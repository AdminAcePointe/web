//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ZionPortal
{
    using System;
    using System.Collections.Generic;
    
    public partial class TimeEntryNote
    {
        public int TimeEntryNotesID { get; set; }
        public int NoteTypeID { get; set; }
        public int TimeEntryID { get; set; }
        public string Notes { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
    
        public virtual NoteType NoteType { get; set; }
        public virtual NoteType NoteType1 { get; set; }
        public virtual TimeEntry TimeEntry { get; set; }
    }
}
