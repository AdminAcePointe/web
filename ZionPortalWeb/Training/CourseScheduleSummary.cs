//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ZionPortal
{
    using System;
    using System.Collections.Generic;
    
    public partial class CourseScheduleSummary
    {
        public int CourseID { get; set; }
        public string CourseName { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<int> CouseDuration { get; set; }
        public int CourseScheduleID { get; set; }
        public string ScheduleStatus { get; set; }
        public Nullable<System.DateTime> CouseScheduleDate { get; set; }
        public Nullable<System.TimeSpan> CourseStartTime { get; set; }
        public Nullable<System.TimeSpan> CourseEndTime { get; set; }
        public int Upcoming { get; set; }
        public int Completed { get; set; }
        public Nullable<int> RoomCapacity { get; set; }
        public Nullable<int> RegisteredStudents { get; set; }
        public int IsClassAvailable { get; set; }
    }
}
