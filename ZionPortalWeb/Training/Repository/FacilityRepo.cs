﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using ZionPortal.Dto;

namespace ZionPortal.Repository
{
    public class FacilityRepo
    {
        private ZionEntities _context;
        const string _user = "AppUser";

        public FacilityRepo(ZionEntities context)
        {
            _context = context;
        }

        public bool Add(Facility model)
        {
            model.CreatedBy = _user;
            model.CreatedDate = DateTime.Now;
            model.ModifiedBy = _user;
            model.ModifiedDate = DateTime.Now;

            _context.Facilities.Add(model);
            _context.SaveChanges();

            return true;
        }

        public IEnumerable<Facility> Get()
        {
            return _context.Facilities.Where(i => i.IsDeleted == false).Include(r => r.Rooms).ToList();
        }

        public Facility Get(int id)
        {
            return Get().FirstOrDefault(a => a.FacilityID == id);
        }
        
        public bool Update(Facility model)
        {
            var dbo = Get(model.FacilityID);
            if (dbo == null)
                throw new KeyNotFoundException("Not Found");

            dbo.FacilityName = model.FacilityName;
            dbo.AddressLine1 = model.AddressLine1;
            dbo.AddressLine2 = model.AddressLine2;
            dbo.Phone = model.Phone;
            dbo.City = model.City;
            dbo.State = model.State;
            dbo.PostalCode = model.PostalCode;
            dbo.ModifiedBy = _user;
            dbo.ModifiedDate = DateTime.Now;

            _context.SaveChanges();

            return true;
        }

        public bool Delete(Facility model)
        {
            return Delete(model.FacilityID);
        }

        public bool Delete(int id)
        {
            var dbo = Get(id);
            if (dbo == null)
                throw new KeyNotFoundException("Not Found");
          
            var c = dbo.Rooms.Count();
            if (c > 0)
            {
                dbo.IsDeleted = true;
            
                _context.SaveChanges();
                return true;
            }
           
            _context.Facilities.Remove(dbo);
            _context.SaveChanges();

            return true;
        }

        public Int32 facilitiesCount()
        {
            return _context.Facilities.Where(i => i.IsDeleted == false).Include(r => r.Rooms).Count();
        }

        public Int32 roomCount()
        {
            return _context.Rooms.Count();
        }
    }
}