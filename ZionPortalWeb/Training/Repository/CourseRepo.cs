﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using ZionPortal.Dto;


namespace ZionPortal.Repository
{
    public class CourseRepo
    {
        private ZionEntities _context;
        private const string _user = "AppUser";

        public CourseRepo(ZionEntities context)
        {
            _context = context;
        }

        public bool Add(Course model)
        {
           

            var newCourse = new Course();
            newCourse.SubcategoryID = model.CourseSubcategory.SubcategoryID;
            newCourse.CourseCode = model.CourseCode;
            newCourse.CourseName = model.CourseName;
            newCourse.CourseDescription = model.CourseDescription;
            newCourse.Pass_Fail = model.Pass_Fail;
            newCourse.Duration = model.Duration;
            newCourse.CoursePrice = model.CoursePrice;
            newCourse.CreatedBy = _user;
            newCourse.CreatedDate = DateTime.Now;
            newCourse.ModifiedBy = _user;
            newCourse.ModifiedDate = DateTime.Now;
            newCourse.IsDeleted = false;
            newCourse.CertificateExpirationDays = model.CertificateExpirationDays;


            _context.Courses.Add(newCourse);
            
          
            return Save();
        }

        public IEnumerable<Course> Get()
        {

            return _context.Courses.Where(a => a.IsDeleted == false).ToList();
        }

     
        public Course Get(int id)
        {
            return _context.Courses.Where(a => a.IsDeleted == false ).FirstOrDefault(a => a.CourseID == id);
        }

        public bool Update(Course model)
        {
            var dbo = Get(model.CourseID);
            if (dbo == null)
                throw new KeyNotFoundException("Not Found");
            dbo.CourseCode = model.CourseCode;
            dbo.CourseName = model.CourseName;
            dbo.CourseDescription = model.CourseDescription;
            dbo.CoursePrice = model.CoursePrice;
            dbo.Duration = model.Duration;
            dbo.Pass_Fail = model.Pass_Fail;
            dbo.SubcategoryID = model.CourseSubcategory.SubcategoryID;
            dbo.ModifiedBy = _user;
            dbo.ModifiedDate = DateTime.Now;
            dbo.CertificateExpirationDays = model.CertificateExpirationDays;

            _context.SaveChanges();

            return true;
        }


        public bool Delete(CourseDto model)
        {
            return Delete(model.CourseId);
        }

        public bool Delete(int id)
        {
            var dbo = Get(id);
            dbo.IsDeleted = true;
            dbo.ModifiedBy = _user;
            dbo.ModifiedDate = DateTime.Now;
            _context.SaveChanges();

            return true;
        }


        public string CourseCardCompleted()
        {
            var b = _context.CourseScheduleSummaries.FirstOrDefault();
            if (b != null)
                return _context.CourseScheduleSummaries.Sum(i => i.Completed).ToString();

            return "0";
        }

  
        public string CourseCardUpcoming()
        {

            var b = _context.CourseScheduleSummaries.FirstOrDefault();
            if (b != null)
                return _context.CourseScheduleSummaries.Sum(i => i.IsClassAvailable).ToString();

            return "0";
    
        }

        public string CourseCardAvailable()
        {
            var b = _context.CourseScheduleSummaries.FirstOrDefault();
            if (b != null)
            return _context.CourseScheduleSummaries.Sum(i => i.IsClassAvailable).ToString();

            return "0";
        }
        private bool Save()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {

                foreach (DbEntityValidationResult item in e.EntityValidationErrors)

                {
                    DbEntityEntry entry = item.Entry;

                    string entityTypeName = entry.Entity.GetType().Name;
                    foreach (DbValidationError subItem in item.ValidationErrors)
                    {
                        string message = string.Format("Error '{0}' occurred in {1} at {2}",

                        subItem.ErrorMessage, entityTypeName, subItem.PropertyName);

                        //  Console.WriteLine(message);
                    }

                }
                return false;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return false;
            }
            return true;
        }

     
    }
}