﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using ZionPortal.Dto;

namespace ZionPortal.Repository
{
    public class RoomRepo
    {
        private ZionEntities _context;
        private FacilityRepo _facilityRepo;
        const string _user = "AppUser";

        public RoomRepo(ZionEntities context)
        {
            _context = context;
            _facilityRepo = new FacilityRepo(context);
        }

        public bool Add(Room model)
        {

            //var fac = _context.Facilities.Where(i => Select(i => i.FacilityID);
            //Int32 f = fac.FirstOrDefault();
            var room = new Room();

            room.FacilityID = model.Facility.FacilityID;
            room.RoomName = model.RoomName;
            room.RoomCapacity = model.RoomCapacity;
            room.CreatedBy = _user;
            room.CreatedDate = DateTime.Now;
            room.ModifiedBy = _user;
            room.ModifiedDate = DateTime.Now;
            room.Facility = null;
            _context.Rooms.Add(room);
            return Save();

        }

        public IEnumerable<Room> Get()
        {
            return _context.Rooms.ToList();
        }

        public Room Get(int id)
        {
            return _context.Rooms.FirstOrDefault(a => a.RoomID == id);
        }

        public bool Update(Room model)
        {
            var dbo = Get(model.RoomID);
            if (dbo == null)
                throw new KeyNotFoundException("Not Found");

            dbo.FacilityID = model.Facility.FacilityID; ;
            dbo.RoomName = model.RoomName;
            dbo.RoomCapacity = model.RoomCapacity;
            dbo.ModifiedBy = _user;
            dbo.ModifiedDate = DateTime.Now;

            _context.SaveChanges();

            return true;
        }

        public bool Delete(Room model)
        {
            return Delete(model.RoomID);
        }

        public bool Delete(int id)
        {
            var dbo = Get(id);
            if (dbo == null)
                throw new KeyNotFoundException("Not Found");

            _context.Rooms.Remove(dbo);
            _context.SaveChanges();

            return true;
        }

        public IEnumerable<vwRoomAvailable> GetAvailable()
        {
            return _context.vwRoomAvailables.ToList();
        }

        public IEnumerable<vwRoomUnavailable> GetUnavailable()
        {
            return _context.vwRoomUnavailables.ToList();
        }
        private bool Save()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {

                foreach (DbEntityValidationResult item in e.EntityValidationErrors)

                {
                    DbEntityEntry entry = item.Entry;

                    string entityTypeName = entry.Entity.GetType().Name;
                    foreach (DbValidationError subItem in item.ValidationErrors)
                    {
                        string message = string.Format("Error '{0}' occurred in {1} at {2}",

                        subItem.ErrorMessage, entityTypeName, subItem.PropertyName);

                        //  Console.WriteLine(message);
                    }

                }
                return false;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return false;
            }
            return true;
        }
    }
}