﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using ZionPortal.Dto;

namespace ZionPortal.Repository
{
    public class PaymentRepo
    {
        private ZionEntities _context;
        private PaymentTypeRepo _paymentTypeRepo;
        const string _user = "AppUser";

        public PaymentRepo(ZionEntities context)
        {
            _context = context;
            _paymentTypeRepo = new PaymentTypeRepo(context);            
        }

        public bool Add(Payment model)
        {
            model.CreatedBy = _user;
            model.CreatedDate = DateTime.Now;
            model.ModifiedBy = _user;
            model.ModifiedDate = DateTime.Now;

            _context.Payments.Add(model);
            _context.SaveChanges();

            return true;
        }

        public IEnumerable<Payment> Get()
        {
            return _context.Payments.ToList();
        }

        public Payment Get(int id)
        {
            return _context.Payments.FirstOrDefault(p => p.PaymentID == id);
        }

        public bool Update(Payment model)
        {
            var dbo = Get(model.PaymentID);
            if (dbo == null)
                throw new KeyNotFoundException("Not Found");

            _paymentTypeRepo.Update(model.PaymentType);

            dbo.PaymentAmount = model.PaymentAmount;
            dbo.PaymentDate = model.PaymentDate;
            dbo.TransactionId = model.TransactionId;
            dbo.ModifiedBy = _user;
            dbo.ModifiedDate = DateTime.Now;

            return true;
        }

        public bool Delete(Payment model)
        {
            return Delete(model.PaymentID);
        }

        public bool Delete(int id)
        {
            var dbo = Get(id);
            if (dbo == null)
                throw new KeyNotFoundException("Not Found");

            _context.Payments.Remove(dbo);
            _context.SaveChanges();

            return true;
        }
    }
}