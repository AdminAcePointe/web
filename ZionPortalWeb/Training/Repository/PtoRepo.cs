﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using ZionPortal.Dto;

namespace ZionPortal.Repository
{
    public class PtoRepo
    {

        private ZionEntities _context;

        private static string _user = "AppUser";

        public PtoRepo(ZionEntities context)
        {
            _context = context;
            _context.Configuration.LazyLoadingEnabled = false;
        }

        
            public List<PtoRequest> getPtoRequestByEmpId(int EmpID)
        {
           
             List<PTORequest> EmpPtoRequest = _context.PTORequests.Where(i => i.EmployeeID == EmpID).ToList();
            List<PtoRequest> requestList = new List<PtoRequest>();
            foreach (PTORequest req in EmpPtoRequest)
            {
                PtoRequest fillUi = new PtoRequest();
                int balance = getPTOBalance(req.PTOTypeID, req.EmployeeID);
                string ptoName = _context.PTOTypes.FirstOrDefault(i => i.PTOTypeID == req.PTOTypeID).PTOType1;
                fillUi.text = ptoName + " - " + req.PTOHours + "Hrs";
                fillUi.ptoHrRequested = req.PTOHours;
                fillUi.employeeID = req.EmployeeID;
                fillUi.startDate = Convert.ToDateTime(req.PTOStartDate).Date.ToString("yyyy-MM-dd hh:mm tt");
                fillUi.endDate = Convert.ToDateTime(req.PTOEndDate).Date.ToString("yyyy-MM-dd hh:mm tt");
                fillUi.PtoRequestId = req.PTORequestID;
                fillUi.PTOType = req.PTOType.PTOType1;
                fillUi.PTOtypeId = req.PTOTypeID;
                var y = _context.PTORequestNotes.FirstOrDefault(i => i.PTORequestID == req.PTORequestID);
                if (y != null)
                {
                    fillUi.Notes = y.PTONotes;
                    fillUi.PTORequestNotesID = y.PTORequestNotesID;
                   
                }
                
                requestList.Add(fillUi);
            }
            return requestList;
        }

        public List<vw_PTORequestBalance> getEmpRequestByManager(int managerId)
        {
            return _context.vw_PTORequestBalance.Where(i => i.ManagerID == managerId).ToList();
        }

        public bool ptoApproval(vw_PTORequestBalance ptoReq)
        {
            SupPtoApproval(ptoReq);
            return  SupPtoNote(ptoReq);
            
        }
        
        private bool SupPtoApproval(vw_PTORequestBalance PtoReq)
        {
            bool myBool = false;
            bool prevApproved = false;
     
            int PrevaprovalStatusId = 0;
            int CuraprovalStatusId = 0;

            if (PtoReq.PTOApprovalID == null && PtoReq.ApprovalStatus != null)
            {
                //Insert
                PTOApproval approval = new PTOApproval();
                approval.PTORequestID = PtoReq.PTORequestID;
                approval.PTODate = PtoReq.ClockDate;
                PrevaprovalStatusId = approval.ApprovalStatusID;
                approval.ApprovalStatusID = _context.ApprovalStatusTypes.FirstOrDefault(i => (i.ApprovalStatusTypeDesc == PtoReq.ApprovalStatus)).ApprovalStatusID;
                CuraprovalStatusId = approval.ApprovalStatusID;
                approval.ApproverEmployeeID = Convert.ToInt16(PtoReq.ApproverEmployeeID);
                approval.CreatedDate = DateTime.Now;
                approval.ApprovalDate = DateTime.Now;
                approval.CreatedBy = _user;
                approval.ApprovalTypeID = 1;
                approval.ModifiedBy = _user;
                approval.ModifiedDate = DateTime.Now;
                _context.PTOApprovals.Add(approval);
                myBool = Save();
               
            }
            else
            {
               
                //update
                PTOApproval toEdit = _context.PTOApprovals.FirstOrDefault(i => i.PTORequestID == PtoReq.PTORequestID);
                if (toEdit != null)
                {
                    
                    toEdit.ApprovalDate = PtoReq.ApprovalDate;
                    PrevaprovalStatusId = toEdit.ApprovalStatusID;  // grab the value first before setting it below
                    toEdit.ApprovalStatusID = _context.ApprovalStatusTypes.FirstOrDefault(i => (i.ApprovalStatusTypeDesc == PtoReq.ApprovalStatus)).ApprovalStatusID;  
                    CuraprovalStatusId = toEdit.ApprovalStatusID;

                    toEdit.ModifiedBy = _user;
                    toEdit.ModifiedDate = DateTime.Now;
                    toEdit.ApproverEmployeeID = Convert.ToInt16(PtoReq.ApproverEmployeeID);
                    if (toEdit.ApprovalStatusID == 1)
                        prevApproved = true;

                   
                      

                    myBool = Save();
                }
            }

            if (myBool && (PrevaprovalStatusId == 0  || PrevaprovalStatusId == 2)  && CuraprovalStatusId == 1)
            {
                var pto = _context.PTOBalances.FirstOrDefault(i => i.EmployeeID == PtoReq.EmployeeID && i.PTOTypeID == PtoReq.PTOTypeID);
                pto.PTOHoursRemaning = pto.PTOHoursRemaning - PtoReq.RequestedHours;
                pto.ModifiedBy = _user;
                pto.ModifiedDate = DateTime.Now;
                Save();
            }

            if (myBool && (PrevaprovalStatusId == 1 )  && (CuraprovalStatusId == 2 || CuraprovalStatusId == 3))
            {
                var pto = _context.PTOBalances.FirstOrDefault(i => i.EmployeeID == PtoReq.EmployeeID && i.PTOTypeID == PtoReq.PTOTypeID);
       
                pto.ModifiedBy = _user;
                var newval = pto.PTOHoursRemaning + PtoReq.RequestedHours;
                pto.PTOHoursRemaning = newval;
                pto.ModifiedDate = DateTime.Now;
                Save();
            }

            return myBool;

        }

        private bool SupPtoNote(vw_PTORequestBalance ptoReq)
        {

            if (ptoReq.PTORequestNotesID == null && ptoReq.PTONotes != null)
            {
                Int32 typeid;

                if (ptoReq.NoteTypeID == null)
                {
                    typeid = 5;
                }
                else
                {
                    typeid = ptoReq.NoteTypeID.Value;
                };

                // Insert
                PTORequestNote note = new PTORequestNote();
                note.PTONotes = ptoReq.PTONotes;
                note.NoteEnteredByEmployeeID = Convert.ToInt16(ptoReq.ManagerID);
                note.CreatedDate = DateTime.Now;
                note.PTORequestID = ptoReq.PTORequestID;
                note.PTONoteTypeID = typeid;
                note.CreatedBy = _user;
                note.ModifiedBy = _user;
                note.ModifiedDate = DateTime.Now;
                
                _context.PTORequestNotes.Add(note);
                return Save();
            }
            else if (ptoReq.PTORequestNotesID != null && ptoReq.PTONotes != null)
            {
                //Update
                PTORequestNote toEdit = _context.PTORequestNotes.FirstOrDefault(i => i.PTORequestNotesID == ptoReq.PTORequestNotesID);
                toEdit.PTONotes = ptoReq.PTONotes;
                toEdit.ModifiedBy = _user;
                toEdit.ModifiedDate = DateTime.Now;
                toEdit.NoteEnteredByEmployeeID = Convert.ToInt32(ptoReq.ManagerID);

                return Save();
            }
            return false;

        }



        public string ptoBalanceByTypeId(int typeId, int empId)
        {
            int remBalance = _context.PTOBalances.Where(i => i.EmployeeID == empId && i.PTOTypeID == typeId).FirstOrDefault().PTOHoursRemaning;
            int begHrs = _context.PTOBalances.Where(i => i.EmployeeID == empId && i.PTOTypeID == typeId).FirstOrDefault().PTOBeginningBalance;
            return "BegHrs- " + begHrs + " RemHrs " + remBalance;
        }
        public List<PTOBalance> ptotypesBalanceByEmpId(int empId)
        {
            return  _context.PTOBalances.Where(i => i.EmployeeID == empId).ToList();
           
        }

        public int getPTOBalance(int typeId, int empId)
        {
          return  _context.PTOBalances.Where(i => i.EmployeeID == empId && i.PTOTypeID == typeId).FirstOrDefault().PTOHoursRemaning;
        }


    
        public List<PTOBalanceWithTypeDto> ptoBalanceWithTypeByEmpId(int empId)
        {


            var dto = _context.PTOBalances.Where(i => i.EmployeeID == empId).Include(i => i.PTOType)
               .Select(i => new PTOBalanceWithTypeDto() {
                   PTOBalanceID = i.PTOBalanceID
                   , EmployeeID = i.EmployeeID
                   , id = i.PTOTypeID
                   , PTOHoursRemaning = i.PTOHoursRemaning
                   , PTOBeginningBalance = i.PTOBeginningBalance
                   , PTOType = i.PTOType.PTOType1
                   , text = i.PTOType.PTOType1 + " - " + i.PTOHoursRemaning + "hours"
               }).ToList();

           

            return dto;
        }

        //Adds new ptoRequest with or without note
        public bool PtoRequest(pto_note_dto ptoRequest)
        {
            bool result = false;

            PTORequest request = new PTORequest();
           // request.ApprovalStatusID = ptoRequest.ApprovalStatusID;
            request.CreatedBy = _user;
            request.CreatedDate = DateTime.Now;
            request.EmployeeID = ptoRequest.EmployeeID;
            request.ModifiedDate = DateTime.Now;
            request.ModifiedBy = _user;
            request.PTOHours = ptoRequest.PTOHours;
            request.PTOStartDate = ptoRequest.PTOStartDate;
            request.PTOEndDate = ptoRequest.PTOStartDate.AddHours(ptoRequest.PTOHours);
            request.PTOTypeID = ptoRequest.PTOTypeID;
  



            _context.PTORequests.Add(request);
            result = Save();

            //insert new note
            if (ptoRequest.PTONotes != null && ptoRequest.PTORequestNotesID == null)
            {
                PTORequestNote note = new PTORequestNote();
                note.PTORequestID = request.PTORequestID;
                note.PTONotes = ptoRequest.PTONotes;
                note.PTONoteTypeID = _context.NoteTypes.Where(i=> i.NoteType1 == "Employee PTO Request").Select(i=> i.NoteTypeID).FirstOrDefault();
                note.CreatedDate = DateTime.Now;
                note.CreatedBy = _user;
                note.ModifiedDate = DateTime.Now;
                note.ModifiedBy = _user;
                note.NoteEnteredByEmployeeID = request.EmployeeID;
                _context.PTORequestNotes.Add(note);
                result = Save();
            }

            return result;
        }

        //modifies an existing pto Request

        public bool updatePtoRequest(pto_note_dto ptoRequest)
        {
            var isapproved = _context.PTOApprovals.Where(i => i.PTORequestID == ptoRequest.PTORequestID && i.ApprovalTypeID == 1).FirstOrDefault();
            if (isapproved == null)
            {
                var dbo = _context.PTORequests.Where(i => i.PTORequestID == ptoRequest.PTORequestID).FirstOrDefault();

                dbo.PTOTypeID = ptoRequest.PTOTypeID;  
                dbo.PTOHours = ptoRequest.PTOHours;
                dbo.PTOStartDate = Convert.ToDateTime(ptoRequest.PTOStartDate);
                dbo.PTOEndDate = ptoRequest.PTOStartDate.AddHours(ptoRequest.PTOHours);
                dbo.ModifiedBy = _user;
                dbo.ModifiedDate = DateTime.Now;
                Save();

                // Update note if exists
                var note = _context.PTORequestNotes.FirstOrDefault(i => i.PTORequestID == ptoRequest.PTORequestID);

                if (ptoRequest.PTONotes != null && ptoRequest.PTORequestNotesID != null)
                {
                    // Update an existing note
                    note.PTONotes = ptoRequest.PTONotes;
                    note.ModifiedDate = DateTime.Now;
                    note.ModifiedBy = _user;
                    Save();
                }
                else
                //insert new note
                if (ptoRequest.PTONotes != null && ptoRequest.PTORequestNotesID == null)
                {
                    var noteup = new PTORequestNote();
                    noteup.PTORequestID = ptoRequest.PTORequestID;
                    noteup.PTONotes = ptoRequest.PTONotes;
                    noteup.PTONoteTypeID = _context.NoteTypes.Where(i => i.NoteType1 == "Employee PTO Request").Select(i => i.NoteTypeID).FirstOrDefault();
                    noteup.CreatedDate = DateTime.Now;
                    noteup.CreatedBy = _user;
                    noteup.ModifiedDate = DateTime.Now;
                    noteup.ModifiedBy = _user;
                    noteup.NoteEnteredByEmployeeID = ptoRequest.EmployeeID;
                   _context.PTORequestNotes.Add(noteup);
                    Save();
                }
            }
            else
            {
                return false;
            }
            return true;
        }

        private bool Save()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {

                foreach (DbEntityValidationResult item in e.EntityValidationErrors)
                {
                    DbEntityEntry entry = item.Entry;
                    string entityTypeName = entry.Entity.GetType().Name;

                    foreach (DbValidationError subItem in item.ValidationErrors)
                    {
                        string message = string.Format("Error '{0}' occurred in {1} at {2}",
                            subItem.ErrorMessage, entityTypeName, subItem.PropertyName);
                    }

                }
                return false;

            }

            return true;
        }


    }
}
