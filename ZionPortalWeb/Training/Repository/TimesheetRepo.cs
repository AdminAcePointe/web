﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using ZionPortal.Dto;

namespace ZionPortal.Repository
{
    public class TimesheetRepo
    {
        private ZionEntities _context;

        private static string _user = "AppUser";

        public TimesheetRepo(ZionEntities context)
        {
            _context = context;
            _context.Configuration.LazyLoadingEnabled = false;
        }

        #region TimeEntry
        public Int32 AddTimeEntry(TimeEntry model)
        {

        
            model.CreatedBy = _user;
            model.CreatedDate = DateTime.Now;
            model.ModifiedBy = _user;
            model.ModifiedDate = DateTime.Now;

            _context.TimeEntries.Add(model);

          

   

            if (Save())
                return model.TimeEntryID;
            return 0;

        }

        public IEnumerable<TimeEntry> GetTimeEntries()
        {
            return _context.TimeEntries.Include(a=> a.TimeEntryType).ToList();
        }

        public TimeEntry GetTimeEntryById(int id)
        {
            return GetTimeEntries().FirstOrDefault(a => a.TimeEntryID == id);
        }

        public IEnumerable<TimeEntry> GetTimeEntriesWithApproval()
        {
            return _context.TimeEntries
                .Include(a => a.TimeEntryType)
                .Include(a => a.TimeEntryApproval)
                .ToList();
        }

        public IEnumerable<TimeEntry> GetTimeEntriesWithPayroll()
        {
            return _context.TimeEntries
                .Include(a => a.TimeEntryType)
                .Include(a => a.TimeEntryApproval)
                .Include(a => a.Payroll)
                .ToList();
        }

        public TimeEntry GetTimeEntryByEmployeeId(int id)
        {
            return _context.TimeEntries
                .Where(a => a.EmployeeID == id)
                .OrderByDescending(a => a.ClockDateTime)
                .Include(a => a.TimeEntryType)
                .Include(a => a.Employee.Person)
                .Include(a=>a.MemberService)
                .FirstOrDefault();
           
        }

        public List<vw_ServiceDetails> GetLastServiceByEmployeeId(int empID,int serviceID)
        {

            return _context.vw_ServiceDetails.Where(i => i.EmpID == empID && i.MemberServiceId == serviceID).ToList();

        }


        public List<TimeEntry> GetAllTimeEntryByEmployeeId(int id)
        {
            return _context.TimeEntries
                .Where(a => a.EmployeeID == id)
                .OrderByDescending(a => a.ClockDateTime)
                .Include(a => a.TimeEntryType)
                .Include(a => a.Employee.Person)
                .ToList();
        }


        public bool UpdateTimeEntry(TimeEntry model)
        {
            var dbo = GetTimeEntryById(model.TimeEntryID);
            if (dbo == null)
                throw new KeyNotFoundException("Not Found");

            dbo.EmployeeID = model.EmployeeID;
            dbo.TimeEntryImage = model.TimeEntryImage;
            dbo.IPAddress = model.IPAddress;
            dbo.Latitude = model.Latitude;
            dbo.Longitude = model.Longitude;
            dbo.PayrollID = model.PayrollID;
            dbo.TimeEntryApprovalID = model.TimeEntryApprovalID;
            dbo.TimeEntryID = model.TimeEntryID;
            dbo.TimeEntryTypeID = model.TimeEntryTypeID;
            dbo.ModifiedBy = _user;
            dbo.ModifiedDate = DateTime.Now;

            _context.SaveChanges();

            return true;
        }

        public bool DeleteTimeEntry(TimeEntry model)
        {
            return DeleteTimeEntryById(model.TimeEntryID);
        }

        public bool DeleteTimeEntryById(int id)
        {
            var dbo = GetTimeEntryById(id);
            if (dbo == null)
                throw new KeyNotFoundException("Not Found");

            _context.TimeEntries.Remove(dbo);

            return Save();
        }
        #endregion

        #region Time

        public List<vw_Time> vw_TimeSheetByID(int id)
        {

            var t = _context.vw_Time
                .Where(a => a.EmployeeID == id)
                .OrderByDescending(a => a.isoWeekOfYear)
                .OrderByDescending(a => a.ClockDate)
                .ToList();
            return _context.vw_Time
                .Where(a => a.EmployeeID == id)
                .OrderByDescending(a => a.isoWeekOfYear)
                .OrderByDescending(a => a.ClockDate)
                .ToList();
        }


        public List<vw_Time> vw_TimeSheetByManagerID(int id)
        {
            return _context.vw_Time
                .Where(a => a.ManagerID == id)
                .OrderByDescending(a => a.ClockDate)
                .ToList();
        }


        public List<vw_Time> vw_TimeSheetForBilling()
        {
            return _context.vw_Time
                .OrderByDescending(a => a.ClockDate)
                .ToList();
        }



        public List<vw_TimeSheet> vw_AllTimeSheetByID(int id)
        {
            return _context.vw_TimeSheet
                .Where(a => a.EmployeeID == id)
                .OrderBy(a => a.EmployeeID )
                .ToList();
        }


        public List<vw_EmployeeTimesheetDetail> vw_EmployeeTimesheetDetailByDate_EmployeeID(int employeeID , DateTime ClockDate)
        {
            return _context.vw_EmployeeTimesheetDetail
                .Where(a => a.EmployeeID == employeeID &&  a.ClockDate == ClockDate)
                .OrderByDescending(a => a.ClockDate)
                .ToList();
        }





        #endregion

        #region TimeEntryNote
        public IEnumerable<TimeNote> GetTimeEntryNotes()
        {
            return _context.TimeNotes.ToList();
        }

        public IEnumerable<BillingNote> GetBillingTimeEntryNotes()
        {
            return _context.BillingNotes.ToList();
        }

        public TimeNote GetTimeEntryNoteById(int id)
        {
            return GetTimeEntryNotes().FirstOrDefault(a => a.TimeNotesID == id);
        }
        public BillingNote GetTimeEntryNoteBillingById(int id)
        {
            return GetBillingTimeEntryNotes().FirstOrDefault(a => a.BillingNotesID == id);
        }



        private bool NoteEntry(vw_TimeSheetDto model)
        {
            if (model.TimeNotesID == null && model.Notes != null)
            {
                Int32 typeid;

                if (model.NoteTypeID == null)
                {
                    typeid = 2;
                }
                else {
                   typeid = model.NoteTypeID.Value;
                  };

                // Insert
                TimeNote note = new TimeNote();
                note.Notes = model.Notes;
                note.NoteEnteredByEmployeeID =Convert.ToInt16(model.ManagerID);
                note.NoteEntryDate = DateTime.Now;
                note.EmployeeID = model.EmployeeID;
                note.ClockDate = model.ClockDate;
                note.CreatedBy = _user;
                note.CreatedDate = DateTime.Now;
                note.ModifiedBy = _user;
                note.ModifiedDate = DateTime.Now;
                note.NoteTypeID = typeid;



                _context.TimeNotes.Add(note);
                return Save();
            }
            else if (model.TimeNotesID != null && model.Notes != null)
            {
                //Update
                TimeNote toEdit = GetTimeEntryNoteById(Convert.ToInt16(model.TimeNotesID));
                toEdit.Notes = model.Notes;
                toEdit.ModifiedBy = _user;
                toEdit.ModifiedDate = DateTime.Now;
                toEdit.NoteEnteredByEmployeeID =Convert.ToInt16(model.ManagerID);
                toEdit.NoteEntryDate = DateTime.Now;
                return Save();
            }
            return false;
        }

        private bool billingTimeApproval(vw_TimeSheetDto model)
        {
            
            bool myBool = false;
            if (model.BillTimeApprovalID == null && model.BillingApprovalStatus != null)
            {
                //Insert
                BillingApproval approval = new BillingApproval();
                approval.ApprovalStatusID = _context.ApprovalStatusTypes.FirstOrDefault(i => (i.ApprovalStatusTypeDesc == model.BillingApprovalStatus)).ApprovalStatusID;
                approval.ApproverEmployeeID = Convert.ToInt16(model.BillingApproverEmployeeID);
                approval.ClockDate = model.ClockDate;
                approval.CreatedBy = _user;
                approval.ApprovalTypeID = 2;
                approval.ApprovalStatusID = _context.ApprovalStatusTypes.FirstOrDefault(i => (i.ApprovalStatusTypeDesc == model.BillingApprovalStatus)).ApprovalStatusID;
                approval.CreatedDate = DateTime.Now;
                approval.ApprovalDate = DateTime.Now;
                approval.EmployeeID = model.EmployeeID;
                approval.ModifiedBy = _user;
                approval.ModifiedDate = DateTime.Now;
                _context.BillingApprovals.Add(approval);
                myBool = Save();
            }
            else
            {

                //update
                BillingApproval toEdit = _context.BillingApprovals.FirstOrDefault(i => i.BillingApprovalID == model.BillTimeApprovalID);
                if (toEdit != null)
                {
                    toEdit.ApprovalDate = model.BillingApprovalDate;
                    toEdit.ApprovalStatusID = _context.ApprovalStatusTypes.FirstOrDefault(i => (i.ApprovalStatusTypeDesc == model.BillingApprovalStatus)).ApprovalStatusID;
                    toEdit.ModifiedBy = _user;
                    toEdit.ModifiedDate = DateTime.Now;
                    toEdit.ApproverEmployeeID = Convert.ToInt16(model.BillingApproverEmployeeID);
                    myBool = Save();
                }

            }
            return myBool;


        }

        private bool BillingNoteEntry(vw_TimeSheetDto model)
        {
       
            if (model.BillingTimeNotesID == null && model.BillingNotes != null)
            {
                Int32 typeid;

                if (model.BillingNoteTypeID == null)
                {
                    typeid = 3;
                }
                else
                {
                    typeid = model.BillingNoteTypeID.Value;
                };
                // Insert
                BillingNote note = new BillingNote();
                note.Notes = model.BillingNotes;
                note.NoteEnteredByEmployeeID = Convert.ToInt16(model.ManagerID); //TODO: this should be the person that clocked in , not the manager
                note.NoteEntryDate = DateTime.Now;
                note.EmployeeID = model.EmployeeID;
                note.ClockDate = model.ClockDate;
                note.CreatedBy = _user;
                note.CreatedDate = DateTime.Now;
                note.ModifiedBy = _user;
                note.ModifiedDate = DateTime.Now;
                note.NoteTypeID = typeid;

                
                _context.BillingNotes.Add(note);
                return Save();
            }
            else if (model.BillingTimeNotesID != null && model.BillingNotes != null)
            {
                //Update
                BillingNote toEdit =_context.BillingNotes.FirstOrDefault(i=> i.BillingNotesID == model.BillingTimeNotesID);
                toEdit.Notes = model.BillingNotes;
                toEdit.ModifiedBy = _user; 
                toEdit.ModifiedDate = DateTime.Now;
                toEdit.NoteEnteredByEmployeeID = Convert.ToInt16(model.ManagerID); //this should be the person taht clocked in
                toEdit.NoteEntryDate = DateTime.Now;
                return Save();
            }
            return false;
        }

        public bool AddTimeEntryNote(TimeEntryNoteDto model)
        {
            if (model.Notes != "")
                {
                // Insert
                TimeEntryNote note = new TimeEntryNote();
                note.Notes = model.Notes;
                note.NoteTypeID = model.NoteTypeID;
                note.CreatedBy = _user;
                note.CreatedDate = DateTime.Now;
                note.ModifiedBy = _user;
                note.ModifiedDate = DateTime.Now;
                note.TimeEntryID = model.TimeEntryID;
                _context.TimeEntryNotes.Add(note);
                return Save();
            }
            return true;
           
        }

        #endregion
        #region TimeEntryApproval
        public IEnumerable<TimeApproval> GetTimeEntryApprovals()
        {
            return _context.TimeApprovals.ToList();
        }

        public IEnumerable<PTOApproval> GetPTOTimeEntryApprovals()
        {
            return _context.PTOApprovals.ToList();
        }

        public TimeApproval GetTimeEntryApprovalById(int id)
        {
            return GetTimeEntryApprovals().FirstOrDefault(a => a.TimeApprovalID == id);
        }

        public PTOApproval GetPTOApprovalbyId(int id)
        {
            return GetPTOTimeEntryApprovals().FirstOrDefault(a => a.PTOApprovalID == id);
        }

        private bool ApprovalEntry(vw_TimeSheetDto model)
        {
            bool myBool = false;
            if (model.TimeApprovalID == null && model.ApprovalStatus != null)
            {
                //Insert
                TimeApproval approval = new TimeApproval();
                approval.ApprovalStatusID = Convert.ToInt16(model.ApprovalStatusID);
                approval.ApproverEmployeeID = Convert.ToInt16(model.ApproverEmployeeID);
                approval.ClockDate = model.ClockDate;
                approval.CreatedBy = _user;
                approval.ApprovalTypeID = 1;
                approval.ApprovalStatusID =  _context.ApprovalStatusTypes.FirstOrDefault(i => (i.ApprovalStatusTypeDesc == model.ApprovalStatus)).ApprovalStatusID;
                approval.CreatedDate = DateTime.Now;
                approval.ApprovalDate = DateTime.Now;
                approval.EmployeeID = model.EmployeeID;
                approval.ModifiedBy = _user;
                approval.ModifiedDate = DateTime.Now;
                _context.TimeApprovals.Add(approval);
                myBool = Save();
            }
            else
            {

                //update
                TimeApproval toEdit = GetTimeEntryApprovalById(Convert.ToInt16(model.TimeApprovalID));
                if (toEdit != null)
                {
                    toEdit.ApprovalDate = model.ApprovalDate;
                    toEdit.ApprovalStatusID = _context.ApprovalStatusTypes.FirstOrDefault(i => (i.ApprovalStatusTypeDesc == model.ApprovalStatus)).ApprovalStatusID;
                    toEdit.ModifiedBy = _user;
                    toEdit.ModifiedDate = DateTime.Now;
                    toEdit.ApproverEmployeeID = Convert.ToInt16(model.ApproverEmployeeID);
                    myBool = Save();
                }
                
               

            }
            return myBool;

        }
        


        #endregion

        public bool approve_note_changes(vw_TimeSheetDto model)
        {
                ApprovalEntry(model);
                NoteEntry(model);
                BillingNoteEntry(model);
                billingTimeApproval(model);
                return Save();
        }

      


        private bool Save()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {

                foreach (DbEntityValidationResult item in e.EntityValidationErrors)
                {
                    DbEntityEntry entry = item.Entry;
                    string entityTypeName = entry.Entity.GetType().Name;

                    foreach (DbValidationError subItem in item.ValidationErrors)
                    {
                        string message = string.Format("Error '{0}' occurred in {1} at {2}",
                            subItem.ErrorMessage, entityTypeName, subItem.PropertyName);
                    }

                }
                return false;

            }

            return true;
        }
    }
}