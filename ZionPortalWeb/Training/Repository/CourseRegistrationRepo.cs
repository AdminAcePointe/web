﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using ZionPortal.Dto;
using iTextSharp;
using iTextSharp.text.pdf;

namespace ZionPortal.Repository
{
    public class CourseRegistrationRepo
    {
        private ZionEntities _context;
        private const string _user = "AppUser";
        private CourseScheduleRepo _courseScheduleRepo;
        private PaymentRepo _paymentRepo;
        private GradeRepo _GradeRepo;
   


        public CourseRegistrationRepo(ZionEntities context)
        {
            _context = context;
            _courseScheduleRepo = new CourseScheduleRepo(context);
            _paymentRepo = new PaymentRepo(context);
            _GradeRepo = new GradeRepo(context);
        }

        public bool Add(CourseRegistration model)
        {
            model.CreatedBy = _user;
            model.CreatedDate = DateTime.Now;
            model.ModifiedBy = _user;
            model.ModifiedDate = DateTime.Now;

            _context.CourseRegistrations.Add(model);
            _context.SaveChanges();

            return true;
        }

        public IEnumerable<CourseRegistration> Get()
        {
            return _context.CourseRegistrations.ToList();
        }

      

        public CourseRegistration Get(int id)
        {
            return _context.CourseRegistrations.FirstOrDefault(i => i.CourseSchedule.CourseScheduleID == id);
        }

        public CourseRegistration GetByRegistrationID(int id)
        {
            return _context.CourseRegistrations.FirstOrDefault(i => i.CourseRegistrationID == id);
        }

        public bool UnregisterStudent(int CourseregID)
        {
            var courseRegistration =  _context.CourseRegistrations.FirstOrDefault(i => i.CourseRegistrationID == CourseregID);
            if (courseRegistration != null)
            {
                _context.CourseRegistrations.Remove(courseRegistration);
                return Save();
            }
            return false;
        }

        public IEnumerable<CourseRegistration> GetByStudentID(int id)
        {
            _context.Configuration.LazyLoadingEnabled = false;
            return _context.CourseRegistrations.Where(i => i.StudentID == id )
                .Include(s => s.CourseSchedule.Instructor.Person)
                .Include(s => s.CourseSchedule.Course.CourseSubcategory.Category)
                .Include(s => s.CourseSchedule.Room.Facility)
                .Include(s => s.Grade)
                .Include(s => s.Cert)
                .Include(s => s.Student.Person.Addresses)
                .Include(s => s.Student.Person.Phones)
                .Include(s => s.Student.Person.Emails).ToList();
        }

        public IEnumerable<CourseRegistration> GetByCourseScheduleID(int id)
        {
            _context.Configuration.LazyLoadingEnabled = false;
            return _context.CourseRegistrations.Where(i => i.CourseScheduleID == id)
             //.Include(s => s.CourseSchedule.Instructor.Person)
              //  .Include(s => s.CourseSchedule.Course.CourseSubcategory.Category)
                .Include(s => s.CourseSchedule.Room.Facility)
                .Include(s => s.CourseSchedule.Course)
                .Include(s => s.Grade)
                .Include(s => s.Cert)
                .Include(s => s.Student.Person.Addresses)
                .Include(s => s.Student.Person.Phones)
                .Include(s => s.Student.Person.Emails).ToList();
        }

        public bool adminRegistersStudent(int studentId , int CourseScheduleId)
        {
            bool exists = _context.Students.ToList().Exists(i => (i.StudentID == studentId));
            bool alreadyregistered = _context.CourseRegistrations.ToList().Exists(i => (i.StudentID == studentId & i.CourseScheduleID == CourseScheduleId));

            if (alreadyregistered)
                return false;

            if (exists)
            {
                Payment newPayment = new Payment();
                newPayment.PaymentDate = DateTime.Now;
                newPayment.PaymentAmount = 0;
                newPayment.ModifiedDate = DateTime.Now;
                newPayment.ModifiedBy = _user;
                newPayment.PaymentTypeID = 3;
                newPayment.CreatedBy = _user;
                newPayment.CreatedDate = DateTime.Now;
                _context.Payments.Add(newPayment);
                if (Save())
                {
                    int paymentId = newPayment.PaymentID;
                    CourseRegistration reg = new CourseRegistration();
                    reg.PaymentID = paymentId;
                    reg.StudentID = studentId;
                    reg.CourseScheduleID = CourseScheduleId;
                    reg.ModifiedBy = _user;
                    reg.CreatedBy = _user;
                    reg.CreatedDate = DateTime.Now;
                    reg.ModifiedDate = DateTime.Now;

                    _context.CourseRegistrations.Add(reg);
                    return Save();
                }
                return false;
            }
            return false;
        }

        public bool upsertGrade(int studentid,  int courseregistrationid,  int score)
        {

            Grade model = new Grade();
            var dbo = _context.Grades.Where(i => i.StudentID == studentid & i.CourseRegistrationID == courseregistrationid).FirstOrDefault();
            var dbo2 = _context.CourseRegistrations.Where(i => i.StudentID == studentid & i.CourseRegistrationID == courseregistrationid).FirstOrDefault();
            bool s = true;

            if (dbo == null)
            {

                model.StudentID = studentid;
                model.Score = score.ToString();
                model.CourseRegistrationID = courseregistrationid;
                model.CreatedBy = _user;
                model.CreatedDate = DateTime.Now;
                model.ModifiedBy = _user;
                model.ModifiedDate = DateTime.Now;
                model.GradeDescription = "";

                _context.Grades.Add(model);
                s = Save();
               
               int gradeid = model.GradeID;

             
                dbo2.GradeID = gradeid;
                dbo2.ModifiedBy = _user;
                dbo2.ModifiedDate = DateTime.Now;

                s = Save();

            }
            else{ 
            
            dbo.Score = score.ToString();
            dbo.ModifiedBy = _user;
            dbo.ModifiedDate = DateTime.Now;

                //dbo2.GradeID = dbo.GradeID;



                s = Save();

            }

          
            return s;
        }


        public bool generateCert(int id, string baseUrl)
        {
            List<CourseRegistration> dbo = GetByCourseScheduleID(id).ToList();

            generateCert genc = new Dto.generateCert();

            bool res = false;


            string templateform = System.Web.Hosting.HostingEnvironment.MapPath(@"\Certs\Templates\CourseCertTemplatefields.pdf");
                Cert c = new Cert();

            foreach (var item in dbo)
{
               

                genc.studentid = item.Student.StudentID;
                genc.crid = item.CourseRegistrationID;
                genc.studentname = item.Student.Person.FirstName + " " + item.Student.Person.LastName;
                genc.courseName = item.CourseSchedule.Course.CourseName;
                genc.datetaken = item.CourseSchedule.ScheduleDate.ToString();
                genc.duration = item.CourseSchedule.Course.Duration;
                genc.passinggrade = Convert.ToInt32(item.CourseSchedule.Course.Pass_Fail);
              //  genc.instructorName = item.CourseSchedule.Instructor.Person.FirstName + " " + item.CourseSchedule.Instructor.Person.LastName;
                genc.certfilename =  genc.studentname + genc.crid +"_" + genc.courseName + "_" + Convert.ToDateTime(genc.datetaken).ToString("yyyy-MM-dd")+ ".pdf";
                genc.finalfilename = System.Web.Hosting.HostingEnvironment.MapPath(@"\Certs\Student\") + genc.certfilename.Replace(" ", "");
                genc.certfilename = genc.certfilename.Replace(" ", "");
                genc.year = item.CourseSchedule.Course.CertificateExpirationDays/365;
                if (item.Grade == null)
                {
                    genc.score = 0;
                }
                else
                {
                    genc.score = Convert.ToInt32(item.Grade.Score);
                };

                if (genc.score >= genc.passinggrade)
                {
                    //Create pdf doc 
                    fillcert(genc, templateform);
                    //Delete cert if exists
                    res = DeleteCert(item.CourseRegistrationID, item.Student.StudentID);
                    // Insert into cert
                    c.CourseRegistrationID = genc.crid;
                    c.StudentID = genc.studentid;
                    c.CertUrl = baseUrl + "/Certs/Student/" + genc.certfilename;
                    c.CreatedBy = _user;
                    c.CreatedDate = DateTime.Now;
                    c.ModifiedBy = _user;
                    c.ModifiedDate = DateTime.Now;

                    _context.Certs.Add(c);
                    res = Save();

                    // Update certid on Course registration
                    var dbo2 = _context.CourseRegistrations.Where(i => i.StudentID == genc.studentid & i.CourseRegistrationID == genc.crid).FirstOrDefault();
                    int certid = c.CertID;
                    dbo2.CertID = certid;
                    dbo2.ModifiedBy = _user;
                    dbo2.ModifiedDate = DateTime.Now;
                    res = Save();
                } 
                
            }

           
            return res;
        }

        public bool DeleteCert(int courseregistrationid , int studentid)
        {
            UpdateCRCert(courseregistrationid);

            var excert = _context.Certs.Where(i => i.CourseRegistrationID == courseregistrationid & i.StudentID == studentid).FirstOrDefault();
            if (excert != null)
            {

                _context.Certs.Remove(excert);
               return Save();
            }
  

            return true;
        }

        public bool UpdateCRCert(int id)
        {
            var dbo = GetByRegistrationID(id);
            if (dbo == null)
                throw new KeyNotFoundException("Not Found");


            dbo.CertID = null;
            dbo.ModifiedBy = _user;
            dbo.ModifiedDate = DateTime.Now;



            return Save();
        }

        public bool fillcert(generateCert genc, string templateform)
        {
            if (File.Exists(genc.finalfilename))
            {
                File.Delete(genc.finalfilename);
            }
            using (FileStream outFile = new FileStream(genc.finalfilename, FileMode.Create))
            {
                PdfReader pdfReader = new PdfReader(templateform);
                PdfStamper pdfStamper = new PdfStamper(pdfReader, outFile);
                AcroFields fields = pdfStamper.AcroFields;
                fields.SetField("Course", genc.courseName);
                fields.SetField("Student",genc.studentname);
                fields.SetField("Instructor", genc.instructorName);
                fields.SetField("Duration", genc.duration.ToString());
                fields.SetField("Date", genc.datetaken);
                fields.SetField("Year", genc.year.ToString());
                fields.SetField("Year_2", genc.year.ToString());

                pdfStamper.FormFlattening = true;

              
                pdfStamper.Close();
                pdfReader.Close();
            }
            return true;
        }
        public bool Update(CourseRegistration model)
        {
            var dbo = Get(model.CourseRegistrationID);
            if (dbo == null)
                throw new KeyNotFoundException("Not Found");

           // _courseScheduleRepo.Update(model.CourseSchedule);
            _paymentRepo.Update(model.Payment);
          
            dbo.ModifiedBy = _user;
            dbo.ModifiedDate = DateTime.Now;



            return Save();
        }

        public bool Delete(CourseRegistration model)
        {
            return Delete(model.CourseRegistrationID);
        }

        public bool Delete(int id)
        {
            var dbo = Get(id);
            if (dbo == null)
                throw new KeyNotFoundException("Not Found");

            _context.CourseRegistrations.Remove(dbo);
            _context.SaveChanges();

            return true;
        }

        private bool Save()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {

                foreach (DbEntityValidationResult item in e.EntityValidationErrors)

                {
                    DbEntityEntry entry = item.Entry;

                    string entityTypeName = entry.Entity.GetType().Name;
                    foreach (DbValidationError subItem in item.ValidationErrors)
                    {
                        string message = string.Format("Error '{0}' occurred in {1} at {2}",

                        subItem.ErrorMessage, entityTypeName, subItem.PropertyName);

                        //  Console.WriteLine(message);
                    }

                }
                return false;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return false;
            }
            return true;
        }
    }
}