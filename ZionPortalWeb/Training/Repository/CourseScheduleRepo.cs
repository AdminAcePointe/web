﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using ZionPortal.Dto;
using AutoMapper;

namespace ZionPortal.Repository
{
    public class CourseScheduleRepo
    {
        private const string _user = "AppUser";

        private ZionEntities _context;
        private CourseRepo _courseRepo;
        private InstructorRepo _instructorRepo;
        private RoomRepo _roomRepo;

        public CourseScheduleRepo(ZionEntities context)
        {
            _context = context;
            _courseRepo = new CourseRepo(context);
            _instructorRepo = new InstructorRepo(context);
            _roomRepo = new RoomRepo(context);
        }

        public bool Add(CourseScheduleDto model)
        {

            var css = new CourseSchedule();


            css.ScheduleDate = model.ScheduleDate;
            css.StartTime = model.StartTime;
            css.EndTime = model.EndTime;
            css.CourseID = model.Course.CourseId;
            css.InstructorID = model.Instructor.InstructorId;
          
            css.CreatedBy = _user;
            css.CreatedDate = DateTime.Now;
            css.ModifiedBy = _user;
            css.ModifiedDate = DateTime.Now;
            css.Status = "Active";
            css.Isdeleted = false;
            css.AllDay = false;
            css.Priority = null;
          //  css.Instructor = null;
            css.Room = null;
            css.CourseRegistrations = null;
            css.Course = null;
            css.RoomID = model.RoomID;
            
            _context.CourseSchedules.Add(css);
            
            return Save();
        }

        public IEnumerable<CourseSchedule> GetForRegistration()
        {
            var u = _context.CourseSchedules.Where(i => i.Isdeleted == false && i.ScheduleDate >= DateTime.Now).ToList();
            return u;
        }


            public IEnumerable<CourseSchedule> Get()
        {
            return _context.CourseSchedules.Where(i=> i.Isdeleted == false).ToList();  
        }

        public List<CourseScheduleDetailDto> getCourseScheduleByInstructorID(int instructorID)
        {
            _context.Configuration.LazyLoadingEnabled = false;
            var u = _context.CourseSchedules.Include("Room").Include("Course").Where(i => i.Isdeleted == false && i.InstructorID == instructorID).Select(i => new CourseScheduleDetailDto()
            {
                CourseName = i.Course.CourseName,
                CourseID = i.Course.CourseID,
                RoomName = i.Room.RoomName,
                CourseScheduleID = i.CourseScheduleID,
                ScheduleDate = i.ScheduleDate,
                StartTime = i.StartTime,
                EndTime = i.EndTime,
            }).ToList();
            return u;
        }

        public CourseSchedule Get(int id)
        {
           return _context.CourseSchedules.FirstOrDefault(a => a.CourseScheduleID == id & a.Isdeleted == false);
        }

        public bool Update(CourseScheduleDto model)
        {
            var dbo = Get(model.CourseScheduleID);
            if (dbo == null)
                throw new KeyNotFoundException("Not Found");
          
            dbo.ModifiedBy = _user;
            dbo.ModifiedDate = DateTime.Now;
            dbo.CourseID = model.CourseID;
            dbo.StartTime = model.StartTime;
            dbo.EndTime = model.EndTime;
            dbo.ScheduleDate = model.ScheduleDate;
            dbo.RoomID = model.RoomID;
            dbo.InstructorID = model.InstructorID;
            dbo.Room = null;
            dbo.Course = null;
           // dbo.Instructor = null;
            return Save();
        }

        


        public bool Delete(CourseSchedule model)
        {
            return Delete(model.CourseScheduleID);
        }

        public bool Delete(int id)
        {
            var dbo = Get(id);


            if (dbo == null)
                return false;
            dbo.Isdeleted = true;


            return Save();
        }

        public IEnumerable<vwCourseScheduleAvailable> GetAvailable()
        {
            return _context.vwCourseScheduleAvailables.ToList();
        }

        public IEnumerable<vwCourseScheduleCompleted> GetCompleted()
        {
            return _context.vwCourseScheduleCompleteds.ToList();
        }

        public IEnumerable<vwCourseScheduleFull> GetFull()
        {
            return _context.vwCourseScheduleFulls.ToList();
        }

        public IEnumerable<vwCourseScheduleUpcoming> GetUpcoming()
        {
            return _context.vwCourseScheduleUpcomings.ToList();
        }

        private bool Save()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {

                foreach (DbEntityValidationResult item in e.EntityValidationErrors)

                {
                    DbEntityEntry entry = item.Entry;

                    string entityTypeName = entry.Entity.GetType().Name;
                    foreach (DbValidationError subItem in item.ValidationErrors)
                    {
                        string message = string.Format("Error '{0}' occurred in {1} at {2}",

                        subItem.ErrorMessage, entityTypeName, subItem.PropertyName);

                        //  Console.WriteLine(message);
                    }

                }
                return false;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return false;
            }
            return true;
        }
    }
}