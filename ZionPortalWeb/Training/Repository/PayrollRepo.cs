﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;

namespace ZionPortal.Repository
{
    public class PayrollRepo
    {
        private ZionEntities _context;

        private static string _user = "AppUser";

        public PayrollRepo(ZionEntities context)
        {
            _context = context;
            _context.Configuration.LazyLoadingEnabled = false;
        }

        public bool AddPayroll(Payroll model)
        {
            model.CreatedBy = _user;
            model.CreatedDate = DateTime.Now;
            model.ModifiedBy = _user;
            model.ModifiedDate = DateTime.Now;

            _context.Payrolls.Add(model);

            return Save();
        }

        public IEnumerable<Payroll> GetPayrolls()
        {
            return _context.Payrolls.Where(i => i.IsDeleted == false).ToList();
        }

        public Payroll GetPayrollById(int id)
        {
            return GetPayrolls().FirstOrDefault(a => a.PayrollID == id);
        }

        public IEnumerable<Payroll> GetPayrollDetails()
        {
            return _context.Payrolls
                .Where(i => i.IsDeleted == false)
                .Include(p => p.PayrollStatusType)
                .Include(p => p.TimeEntries)
                .ToList();
        }

        public bool UpdatePayroll(Payroll model)
        {
            var dbo = GetPayrollById(model.PayrollID);
            if (dbo == null)
                throw new KeyNotFoundException("Not Found");

            dbo.HREmployeeID = model.HREmployeeID;
            dbo.PayrollApprovalDate = model.PayrollApprovalDate;
            dbo.PayrollStatusID = model.PayrollStatusID;
            dbo.PayrollStatusType = model.PayrollStatusType;
            dbo.ModifiedBy = _user;
            dbo.ModifiedDate = DateTime.Now;

            _context.SaveChanges();

            return true;
        }

        public bool DeletePayroll(Payroll model)
        {
            return DeletePayrollById(model.PayrollID);
        }

        public bool DeletePayrollById(int id)
        {
            var dbo = GetPayrollById(id);
            if (dbo == null)
                throw new KeyNotFoundException("Not Found");

            dbo.IsDeleted = true;
            dbo.DeletedDate = DateTime.Now;

            return Save();
        }

        private bool Save()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {

                foreach (DbEntityValidationResult item in e.EntityValidationErrors)
                {
                    DbEntityEntry entry = item.Entry;
                    string entityTypeName = entry.Entity.GetType().Name;

                    foreach (DbValidationError subItem in item.ValidationErrors)
                    {
                        string message = string.Format("Error '{0}' occurred in {1} at {2}",
                            subItem.ErrorMessage, entityTypeName, subItem.PropertyName);

                        // Write to log
                        // Console.WriteLine(message);
                    }

                }
                return false;

            }

            return true;
        }
    }
}