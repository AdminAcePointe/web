﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using ZionPortal.Dto;
using System.IO;
using System.Globalization;

namespace ZionPortal.Repository
{
    public class PersonRepo
    {
        private ZionEntities _context;
        //private string _user = "AppUser";
        private EmployeeRepo _employeeRepo;

        public PersonRepo(ZionEntities context)
        {
            _context = context;
            _employeeRepo = new EmployeeRepo(context);
        }
        

        public int CreatePerson(vw_PersonDetail ddata, string UpdatedProfilePic)
        {
            
            Person newperson = new Person();
            newperson.FirstName = ddata.FirstName;
            newperson.LastName = ddata.LastName;
            newperson.MiddleName = ddata.MiddleName;
            //newperson.PersonPhoto = ddata.profilePic;
            newperson.PersonPhoto = UpdatedProfilePic != "" ? UpdatedProfilePic : ddata.profilePic;
            newperson.MaritalStatus = ddata.MaritalStatus;
            newperson.SSN = ddata.SSN;
            newperson.Gender = ddata.Gender;
            newperson.Title = ddata.Title;
            string[] dobFromUI = ddata.DateOfBirth.Split('G');
            if (dobFromUI[0] != "")
            {
                newperson.DOB = Convert.ToDateTime(dobFromUI[0]);
            }
            else
            {
                newperson.DOB = Convert.ToDateTime(ddata.DateOfBirth);
            }

            newperson.CreatedBy = "AcePointe_User";
            newperson.CreatedDate = DateTime.Now;
            newperson.ModifiedBy = "AcePointe_User";
            newperson.ModifiedDate = DateTime.Now;


            if (ddata.IsActive != null)
            {
                if (!Convert.ToBoolean(ddata.IsActive))
                {
                    newperson.IsInactive = false;
                }
                else
                {
                    newperson.IsInactive = true;
                }
            }

            _context.People.Add(newperson);

            Save();

            if (ddata.Title != null)
            {
                PersonRole personrole = new PersonRole();
                personrole.PersonID = newperson.PersonID;
                personrole.RoleID = _context.Roles.FirstOrDefault(i => i.RoleName == ddata.Title).RoleID;
                personrole.ModifiedDate = DateTime.Now;
                personrole.ModifiedBy = "APP_USER";
                personrole.CreatedDate = DateTime.Now;
                personrole.CreatedBy = "APP_USER";

                _context.PersonRoles.Add(personrole);

                Save();
            }


            return newperson.PersonID;

        }

        public int CreateEmail(vw_PersonDetail ddata, int PersonID)
        {
            Email em = new Email();
            em.Email1 = ddata.email;
            em.PersonID = PersonID;
            em.CreatedBy = "APP_USER";
            em.CreatedDate = DateTime.Now;
            em.ModifiedBy = "APP_USER";
            em.ModifiedDate = DateTime.Now;

            _context.Emails.Add(em);

            Save();
            return em.EmailID;

        }

        public int CreatePhone(vw_PersonDetail ddata, int PersonID)
        {
            Phone ph = new Phone();
            ph.PhoneNumber = ddata.phoneNumber;
            ph.PersonID = PersonID;
            ph.PhoneTypeID = 2; //TODO Fix Later
            ph.CreatedBy = "APP_USER";
            ph.CreatedDate = DateTime.Now;
            ph.ModifiedBy = "APP_USER";
            ph.ModifiedDate = DateTime.Now;

            _context.Phones.Add(ph);

            Save();
            return ph.PhoneID;

        }

        public int CreateAddress(vw_PersonDetail ddata, int PersonID)
        {
            Address ad = new Address();
            ad.PersonID = PersonID;
            ad.City = ddata.city;
            ad.State = ddata.state;
            ad.PostalCode = ddata.ZipCode;
            ad.AddressLine1 = ddata.addressline1;
            ad.AddressTypeID = 1; //TODO Fix Later
            ad.Country = "USA";
            ad.CreatedBy = "APP_USER";
            ad.CreatedDate = DateTime.Now;
            ad.ModifiedBy = "APP_USER";
            ad.ModifiedDate = DateTime.Now;

            _context.Addresses.Add(ad);

            Save();
            return ad.PersonAddressID;

        }
        //

        public int UpdatePersonForMember(vw_PersonDetail ddata , string UpdatedProfilePic)
        {
            int pid = 0;
            if (ddata.PersonID > 0)
            {

                pid = ddata.PersonID;
                var pers = _context.People.FirstOrDefault(i => i.PersonID == pid);
                pers.FirstName = ddata.FirstName;
                pers.LastName = ddata.LastName;
                pers.MiddleName = ddata.MiddleName;
                //updating photo
                pers.PersonPhoto = UpdatedProfilePic != "" ? UpdatedProfilePic : ddata.profilePic;
                pers.MaritalStatus = ddata.MaritalStatus;
                pers.SSN = ddata.SSN;
                pers.Gender = ddata.Gender;
                pers.Title = ddata.Title;
                string[] dobFromUI = ddata.DateOfBirth.Split('G');
                if (dobFromUI[0] != "")
                {
                    pers.DOB = Convert.ToDateTime(dobFromUI[0]);
                }
                else
                {
                    pers.DOB = Convert.ToDateTime(ddata.DateOfBirth);
                }
                pers.ModifiedBy = "APP_USER";
                pers.ModifiedDate = DateTime.Now;


                // set email
                var Email = _context.Emails.FirstOrDefault(i => i.PersonID == pers.PersonID);
                if (Email != null)
                {
                    Email.Email1 = ddata.email;
                    Email.ModifiedBy = "APP_USER";
                    Email.ModifiedDate = DateTime.Now;
                }
                else
                {
                    int email = CreateEmail(ddata, pid);
                }

                //set address
                var addr = _context.Addresses.FirstOrDefault(i => i.PersonID == pers.PersonID);
                if (addr != null)
                {
                    addr.AddressLine1 = ddata.addressline1;
                    addr.City = ddata.city;
                    addr.State = ddata.state;
                    addr.PostalCode = ddata.ZipCode;
                    addr.ModifiedBy = "APP_USER";
                    addr.ModifiedDate = DateTime.Now;
                }
                else
                {
                    int addressid = CreateAddress(ddata, pid);
                }

                // set phone
                var phone = _context.Phones.FirstOrDefault(i => i.PersonID == pers.PersonID);
                if (phone != null)
                {
                    phone.PhoneNumber = ddata.phoneNumber;
                    phone.ModifiedBy = "APP_USER";
                    phone.ModifiedDate = DateTime.Now;
                }
                else
                {
                    int phoneid = CreatePhone(ddata, pid);
                }
            }

             Save();
            return pid;

        }
        public bool UpdatePerson(vw_PersonDetail ddata, string services , string UpdatedProfilePic)
        {
            var PersonId = ddata.PersonID;
            bool result = false;
            var exists = _context.Employees.Where(i => i.EmployeeID == ddata.EmployeeID);
            if (exists != null)
            {

                int pid = exists.FirstOrDefault().PersonID;
                var pers = _context.People.FirstOrDefault(i => i.PersonID == pid);
                pers.FirstName = ddata.FirstName;
                pers.LastName = ddata.LastName;
                pers.MiddleName = ddata.MiddleName;
                //updating photo
                pers.PersonPhoto = UpdatedProfilePic != "" ? UpdatedProfilePic : ddata.profilePic;
                //pers.PersonPhoto = SavePhotoToServer(ddata,file)
                pers.MaritalStatus = ddata.MaritalStatus;
                pers.SSN = ddata.SSN;
                pers.Gender = ddata.Gender;
                pers.Title = ddata.Title;
                string[] dobFromUI = ddata.DateOfBirth.Split('G');
                if (dobFromUI[0] != "")
                {
                    pers.DOB = Convert.ToDateTime(dobFromUI[0]);
                }
                else
                {
                    pers.DOB = Convert.ToDateTime(ddata.DateOfBirth);
                }
                pers.ModifiedBy = "APP_USER";
                pers.ModifiedDate = DateTime.Now;
                
                // set isactive
                if (ddata.IsActive != null)
            { 
                if (!Convert.ToBoolean(ddata.IsActive))
                {
                    pers.IsInactive = false;
                }
                else
                {
                    pers.IsInactive = true;
                }
                    Save();
            }


                // set email
                var Email = _context.Emails.FirstOrDefault(i => i.PersonID == pers.PersonID);
                if (Email != null)
                {
                    Email.Email1 = ddata.email;
                    Email.ModifiedBy = "APP_USER";
                    Email.ModifiedDate = DateTime.Now;
                }
                else
                {
                    int email = CreateEmail(ddata, PersonId);
                }

                //set address
                var addr = _context.Addresses.FirstOrDefault(i => i.PersonID == pers.PersonID);
                if (addr != null)
                {
                    addr.AddressLine1 = ddata.addressline1;
                    addr.City = ddata.city;
                    addr.State = ddata.state;
                    addr.PostalCode = ddata.ZipCode;
                    addr.ModifiedBy = "APP_USER";
                    addr.ModifiedDate = DateTime.Now;
                }
                else
                {
                    int addressid = CreateAddress(ddata, PersonId);
                }

                // set phone
                var phone = _context.Phones.FirstOrDefault(i => i.PersonID == pers.PersonID);
                if (phone != null)
                {
                    phone.PhoneNumber = ddata.phoneNumber;
                    phone.ModifiedBy = "APP_USER";
                    phone.ModifiedDate = DateTime.Now;
                }
                else
                {
                    int phoneid = CreatePhone(ddata, PersonId);
                }

                //var pay = _context.PayRates.FirstOrDefault(i => i.PersonID == PersonId);
                //if (pay == null)
                //{
                //    PayRate empPay = new PayRate();
                //    if (ddata.payRate != null)
                //    {
                //        empPay.PersonID = PersonId;
                //        empPay.PayRate1 = Convert.ToDecimal(ddata.payRate);
                //        empPay.ModifiedBy = "APP_USER";
                //        empPay.CreatedBy = "APP_USER";
                //        empPay.CreatedDate = DateTime.Now;
                //        empPay.ModifiedDate = DateTime.Now;
                //        if (ddata.EffectiveDate != null)
                //        {
                //            string[] effDateFromUI = ddata.EffectiveDate.Split('G');
                //            if (effDateFromUI[0] != "")
                //            {
                //                empPay.PayRateStartDate = Convert.ToDateTime(effDateFromUI[0]);
                //            }
                //            else
                //            {
                //                empPay.PayRateStartDate = Convert.ToDateTime(ddata.EffectiveDate);
                //            }
                            
                //        }
                //        else
                //        {
                //            empPay.PayRateStartDate = DateTime.Now;
                //        }
                //        empPay.PayRateEndDate = Convert.ToDateTime("01/20/9999");
                //        _context.PayRates.Add(empPay);
                //        Save();
                //    }
                //}
                //else
                //{
                //    pay.PayRate1 = Convert.ToDecimal(ddata.payRate);
                //    pay.ModifiedBy = "APP_USER";
                //    pay.ModifiedDate = DateTime.Now;
                //    pay.PayRateStartDate =Convert.ToDateTime(ddata.EffectiveDate);
                //    Save();
                //}

                // Emergency contact

                if (ddata.PersonEmergencyContactID != null)
                {
                    var emcontact = _context.PersonEmergencyContacts.FirstOrDefault(i => i.PersonID == ddata.PersonID);
                    emcontact.Phone = ddata.ContactPhone;
                    emcontact.Email = ddata.ContactEmail;
                    emcontact.Relationship = ddata.Relationship;
                    emcontact.LastName = ddata.ContactLastName;
                    emcontact.FirstName = ddata.ContactFirstName;
                    emcontact.ModifiedBy = "APP_USER";
                    emcontact.ModifiedDate = DateTime.Now;
                }
                else
                {
                  int pem =   CreatePersonEmergency(ddata, PersonId);
                }

                //update personRole
                var personRole = _context.PersonRoles.FirstOrDefault(i => i.PersonID == PersonId);
                if (personRole != null)
                {
                    int newRoleId = _context.Roles.FirstOrDefault(i => i.RoleName == ddata.Title).RoleID;
                    personRole.RoleID = newRoleId;
                    personRole.ModifiedBy = "APP_USER";
                    personRole.ModifiedDate = DateTime.Now;
                    Save();

                }
                else
                {
                    
                    PersonRole personrole = new PersonRole();
                    personrole.PersonID = PersonId;
                    personrole.RoleID = _context.Roles.FirstOrDefault(i => i.RoleName == ddata.Title).RoleID;
                    personrole.ModifiedDate = DateTime.Now;
                    personrole.ModifiedBy = "APP_USER";
                    personrole.CreatedDate = DateTime.Now;
                    personrole.CreatedBy = "APP_USER";

                    _context.PersonRoles.Add(personrole);

                    Save();

                }
                    _employeeRepo.UpsertInstructor(ddata, PersonId);
                    _employeeRepo.UpsertStudent(ddata, PersonId);
                    _employeeRepo.UpsertEmployeeServices(ddata, services);
                    result = Save();
                    result = UpdateWorkInfo(ddata);
                
               
            }
            return result;

        }

        public int CreatePersonEmergency(vw_PersonDetail ddata, int PersonID)
        {
          
            var pem = _context.PersonEmergencyContacts.FirstOrDefault(i => i.PersonID == PersonID);

            PersonEmergencyContact model = new PersonEmergencyContact();

            if (pem == null)
            {
                model.PersonID = PersonID;
                model.Phone = ddata.ContactPhone;
                model.Email = ddata.ContactEmail;
                model.Relationship = ddata.Relationship;
                model.LastName = ddata.ContactLastName;
                model.FirstName = ddata.ContactFirstName;
                model.ModifiedBy = "APP_USER";
                model.ModifiedDate = DateTime.Now;
                model.CreatedBy = "APP_USER";
                model.CreatedDate = DateTime.Now;
                _context.PersonEmergencyContacts.Add(model);
               Save();
            }
            
            return model.PersonEmergencyContactID;
        }






        public bool UpdateWorkInfo(vw_PersonDetail ddata)
        {

            var emp = _context.Employees.FirstOrDefault(i => i.EmployeeID == ddata.EmployeeID);
            var t =  _context.vw_ManagerDetail.FirstOrDefault(i => i.ManagerFullName == ddata.manager);         
            emp.ManagerID = t.ManagerID;
            int facilityId = _context.Facilities.FirstOrDefault(i => i.FacilityName == ddata.GroupHome).FacilityID;
            emp.FacilityID = facilityId;
            emp.ShiftID = _context.Shifts.FirstOrDefault(i => i.ShiftName == ddata.shift).ShiftID;

            if (ddata.HireDate != null)
            {
                string[] hireDateFromUI = ddata.HireDate.Split('G');
                if (hireDateFromUI[0] != "")
                {
                    emp.HireDate = Convert.ToDateTime(hireDateFromUI[0]);
                }
                else
                {
                    emp.HireDate = Convert.ToDateTime(ddata.HireDate);
                }
            }
            
            emp.ModifiedBy = "APP_USER";
            emp.ModifiedDate = DateTime.Now;
            return Save();

        }
        

            public IEnumerable<Person> Get()
        {
            return _context.People.ToList();
        }

        public Person Get(int id)
        {
            return _context.People.FirstOrDefault(a => a.PersonID == id);
        }

        public bool Exists(PersonDto model)
        {
            var exists = _context.People.FirstOrDefault(p => p.FirstName == model.FirstName & p.LastName == model.LastName & p.DOB == model.DOB);
            if (exists != null)
                return true;

            return false;
        }

        public int Create(PersonDto model)
        {
            if (Exists(model))
                throw new InvalidOperationException("Invalid Entry");

            var per = new Person();

            per.CreatedBy = "APP_USER";
            per.CreatedDate = DateTime.Now;
            per.ModifiedBy = "APP_USER";
            per.ModifiedDate = DateTime.Now;
            per.FirstName = model.FirstName;
            per.LastName = model.LastName;
            per.PersonPhoto = model.PersonPhoto;



            _context.People.Add(per);
            bool s = Save();

            int Personid = per.PersonID;

            s = Save();

            return Personid;
        }

        public bool Update(Person model)
        {
            var dbo = Get(model.PersonID);
            if (dbo == null)
                throw new KeyNotFoundException("Not Found");
            

            dbo.Title = model.Title;
            dbo.FirstName = model.FirstName;
            dbo.MiddleName = model.MiddleName;
            dbo.LastName = model.LastName;
            dbo.Suffix = model.Suffix;
            dbo.DOB = model.DOB;
            dbo.ModifiedBy = "APP_USER";
            dbo.ModifiedDate = DateTime.Now;
            dbo.PersonPhoto = model.PersonPhoto;

            _context.SaveChanges();

            return true;
        }

   

        private bool Save()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {

                foreach (DbEntityValidationResult item in e.EntityValidationErrors)

                {
                    DbEntityEntry entry = item.Entry;

                    string entityTypeName = entry.Entity.GetType().Name;
                    foreach (DbValidationError subItem in item.ValidationErrors)
                    {
                        string message = string.Format("Error '{0}' occurred in {1} at {2}",

                        subItem.ErrorMessage, entityTypeName, subItem.PropertyName);

                        //  Console.WriteLine(message);
                    }

                }
                return false;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return false;
            }
            return true;
        }
    }
}