﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using ZionPortal.Dto;

namespace ZionPortal.Repository
{
    public class PaymentTypeRepo
    {
        private ZionEntities _context;
        const string _user = "AppUser";

        public PaymentTypeRepo(ZionEntities context)
        {
            _context = context;
        }

        public bool Add(PaymentType model)
        {
            model.CreatedBy = _user;
            model.CreatedDate = DateTime.Now;
            model.ModifiedBy = _user;
            model.ModifiedDate = DateTime.Now;

            _context.PaymentTypes.Add(model);
            _context.SaveChanges();

            return true;
        }

        public IEnumerable<PaymentType> Get()
        {
            return _context.PaymentTypes.ToList();
        }

        public PaymentType Get(int id)
        {
            return _context.PaymentTypes.FirstOrDefault(i => i.PaymentTypeID == id);
        }

        public bool Update(PaymentType model)
        {
            var dbo = Get(model.PaymentTypeID);
            if (dbo == null)
                throw new KeyNotFoundException("Not Found");

            dbo.PaymentTypeName = model.PaymentTypeName;
            dbo.PaymentTypeDescription = model.PaymentTypeDescription;
            dbo.ModifiedBy = _user;
            dbo.ModifiedDate = DateTime.Now;

            _context.SaveChanges();

            return true;
        }

        public bool Delete(Payment model)
        {
            return Delete(model.PaymentID);
        }

        public bool Delete(int id)
        {
            var dbo = Get(id);
            if (dbo == null)
                throw new KeyNotFoundException("Not Found");

            _context.PaymentTypes.Remove(dbo);
            _context.SaveChanges();

            return true;
        }
    }
}