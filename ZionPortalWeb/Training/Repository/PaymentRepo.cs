﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using ZionPortal.Dto;

namespace ZionPortal.Repository
{
    public class GradeRepo
    {
        private ZionEntities _context;
        const string _user = "AppUser";

        public GradeRepo(ZionEntities context)
        {
            _context = context;
               
        }

        public bool Add(Grade model)
        {
            model.CreatedBy = _user;
            model.CreatedDate = DateTime.Now;
            model.ModifiedBy = _user;
            model.ModifiedDate = DateTime.Now;

            _context.Grades.Add(model);
            _context.SaveChanges();

            return true;
        }

        public IEnumerable<Grade> Get()
        {
            return _context.Grades.ToList();
        }

        public Grade Get(int id)
        {
            return _context.Grades.FirstOrDefault(p => p.GradeID == id);
        }

        public bool Update(Grade model)
        {
            var dbo = Get(model.GradeID);
            if (dbo == null)
                throw new KeyNotFoundException("Not Found");

         

            dbo.Score = model.Score;
            dbo.GradeDescription = model.GradeDescription;
            dbo.ModifiedBy = _user;
            dbo.ModifiedDate = DateTime.Now;

            return true;
        }

        public bool Delete(Grade model)
        {
            return Delete(model.GradeID);
        }

        public bool Delete(int id)
        {
            var dbo = Get(id);
            if (dbo == null)
                throw new KeyNotFoundException("Not Found");

            _context.Grades.Remove(dbo);
            _context.SaveChanges();

            return true;
        }
    }
}