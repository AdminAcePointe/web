﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using ZionPortal.Dto;

namespace ZionPortal.Repository
{
    public class StudentCourseRepo
    {
        private static List<StudentCourseDto> _studentCourseData;
        private static string _user = "AppUser";

        static StudentCourseRepo()
        {
            _studentCourseData = LoadMockData();
        }

        public bool Create(StudentCourseDto dto)
        {
            dto.StudentCourseId = _studentCourseData.Max(sc => sc.StudentCourseId) + 1;
            dto.CreatedBy = _user;
            dto.CreatedOn = DateTime.Now;
            dto.ModifiedBy = _user;
            dto.ModifiedOn = DateTime.Now;
            _studentCourseData.Add(dto);
            return true;
        }

        public List<StudentCourseDto> GetAllStudentCourses()
        {
            return _studentCourseData;
        }

        public StudentCourseDto GetStudentCourseById(int id)
        {
            return _studentCourseData.Find(sc => sc.StudentCourseId == id);
        }

        public bool Update(StudentCourseDto dto)
        {
            var studentCourseToUpdate = _studentCourseData.Find(sc => sc.StudentCourseId == dto.StudentCourseId);
            if (studentCourseToUpdate == null)
                return false;
            studentCourseToUpdate.StudentId = dto.StudentId;
            studentCourseToUpdate.CourseId = dto.CourseId;
            studentCourseToUpdate.DateStarted = dto.DateStarted;
            studentCourseToUpdate.DateCompleted = dto.DateCompleted;
            studentCourseToUpdate.ModifiedBy = _user;
            studentCourseToUpdate.ModifiedOn = DateTime.Now;
            return true;
        }

        public bool Delete(StudentCourseDto dto)
        {
           return _studentCourseData.Remove(dto);
        }

        private static List<StudentCourseDto> LoadMockData()
        {
            string allText = System.IO.File.ReadAllText(HostingEnvironment.MapPath(@"~/MockData/StudentCourse.js"));
            var data = JsonConvert.DeserializeObject<List<StudentCourseDto>>(allText);
            return data;
        }
    }
}