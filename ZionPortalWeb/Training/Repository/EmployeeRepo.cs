﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using ZionPortal.Dto;

namespace ZionPortal.Repository
{
    public class EmployeeRepo
    {
        private ZionEntities _context;
        //private PersonRepo _personRepo;
        private InstructorRepo _instructorRepo;
        private StudentRepo _studentRepo;
        const string _user = "AppUser";
        
        public EmployeeRepo(ZionEntities context)
        {
            _context = context;
           // _personRepo = new PersonRepo(context);
            _instructorRepo = new InstructorRepo(context);
            _studentRepo = new StudentRepo(context);
        }


        public List<string> getServicesByEmployeeID(int empID)
        {
            return _context.vw_ServiceDetails.Where(i => i.EmpID == empID).Select(i => i.ServiceTypeWithService).ToList();
          
        }

        public List<vw_ServiceDetails> getServicesWithIDByEmployeeID(int empID)
        {
            return _context.vw_ServiceDetails.Where(i => i.EmpID == empID).ToList();

        }


        public List<vw_PayPerServiceDetails> getEmployeePayRates(int empID)
        {
            return _context.vw_PayPerServiceDetails.Where(i => i.employeeid == empID).ToList();

        }


        public List<vw_EmployeeDetails> GetAllEmployeeFullName()
        {
            return _context.vw_EmployeeDetails.ToList();

        }

        public bool UpsertEmployeePayRates( ICollection<vw_PayPerServiceDetails> payInfo, int personID)
        {
            
            bool result = false;
            var payRate = new PayRate();
            List<int> serviceIds = new List<int>();
            if (payInfo != null)
            {
                foreach (var pay in payInfo)
                {

                    int serviceID = _context.MemberServices.FirstOrDefault(p => p.MemberServiceName == pay.memberservicename).MemberServiceID;

                    serviceIds.Add(serviceID);

                    if (_context.vw_PayPerServiceDetails.FirstOrDefault(i => i.employeeid == pay.employeeid && i.memberserviceid == serviceID) != null)
                    {
                        //update
                        int payrateID = _context.vw_PayPerServiceDetails.FirstOrDefault(i => i.employeeid == pay.employeeid && i.memberserviceid == serviceID).payrateid;
                        payRate = _context.PayRates.FirstOrDefault(i => i.PayRateID == payrateID);
                        payRate.MemberServiceID = serviceID;
                        payRate.ModifiedBy = "AcepointeUser";
                        payRate.ModifiedDate = DateTime.Now;
                        payRate.PayRate1 = Convert.ToDecimal(pay.payrate);

                        string[] PayeffDateFromUI = pay.payratestartdate.Split('G');
                        if (PayeffDateFromUI[0] != "")
                        {
                            payRate.PayRateStartDate = Convert.ToDateTime(PayeffDateFromUI[0]);
                        }
                        else
                        {
                            payRate.PayRateStartDate = Convert.ToDateTime(pay.payratestartdate);
                        }

                        result = Save();
                    }
                    else
                    {
                        //insert
                        payRate = new PayRate();

                        string[] PayeffDateFromUI = pay.payratestartdate.Split('G');
                        if (PayeffDateFromUI[0] != "")
                        {
                            payRate.PayRateStartDate = Convert.ToDateTime(PayeffDateFromUI[0]);
                        }
                        else
                        {
                            payRate.PayRateStartDate = Convert.ToDateTime(pay.payratestartdate);
                        }
                        payRate.PayRate1 = Convert.ToDecimal(pay.payrate);
                        //payRate.PayRateStartDate = DateTime.Now;
                        payRate.PayRateEndDate = DateTime.Now.AddYears(20);
                        payRate.PersonID = personID;
                        payRate.MemberServiceID = serviceID;
                        payRate.CreatedBy = "APPUSER";
                        payRate.CreatedDate = DateTime.Now;
                        payRate.ModifiedDate = DateTime.Now;
                        payRate.ModifiedBy = "APPUSER";
                        _context.PayRates.Add(payRate);
                        result = Save();

                    }
                }

            }
            result =  PayRateExistsInDB(personID, serviceIds);

            return result;
        }

       


            public bool UpsertEmployeeServices(vw_PersonDetail model, string services)
        {
            bool result = false;
            List<int> serviceIds = new List<int>();
            string[] ServiceType_Services = services.Split(',');
            foreach (var item in ServiceType_Services)
            {
                string[] servType_serv = item.Split('-');
                if (servType_serv.Count() == 2)
                {
                    string service = servType_serv[1].Trim();
                    int serviceID = _context.MemberServices.FirstOrDefault(p => p.MemberServiceName == service).MemberServiceID;
                    serviceIds.Add(serviceID);
                    var emp_serv = _context.MemberEmployeeServices.FirstOrDefault(i => i.EmployeeID == model.EmployeeID && i.MemberServiceID == serviceID);
                    if (emp_serv == null)
                    {
                        //insert a new service for emp
                        emp_serv = new MemberEmployeeService();
                        emp_serv.MemberServiceID = serviceID;
                        //no member assigned yet
                        emp_serv.MemberID = null;
                        emp_serv.EmployeeID = model.EmployeeID;
                        emp_serv.CreatedBy = "AcepointeUser";
                        emp_serv.CreatedDate = DateTime.Now;
                        emp_serv.ModifiedBy = "AcepointeUser";
                        emp_serv.ModifiedDate = DateTime.Now;
                        _context.MemberEmployeeServices.Add(emp_serv);
                        result = Save();

                    }
                    else
                    {
                        //update - service already exists for employee
                        emp_serv.MemberServiceID = serviceID;
                        emp_serv.MemberID = null;
                        emp_serv.EmployeeID = model.EmployeeID;
                        emp_serv.ModifiedBy = "AcepointeUser";
                        emp_serv.ModifiedDate = DateTime.Now;
                        result = Save();
                    }
                }
            }

            result = ServiceExistsInDB(model.EmployeeID, serviceIds);
                return result;
        }

        private bool ServiceExistsInDB(int empID, List<int> serviceIDSFromUI)
        {
            bool result = false;
            var emp_serv = (_context.MemberEmployeeServices.Where(i => i.EmployeeID == empID)).ToList();
            foreach (var item in emp_serv)
            {
                if(!serviceIDSFromUI.Contains(Convert.ToInt32(item.MemberServiceID)))
                {
                    _context.MemberEmployeeServices.Remove(item);
                    result = Save();
                }
            }
            return result;
        }

        private bool PayRateExistsInDB(int PersonID, List<int> serviceIDSFromUI)
        {
            bool result = false;
            var emp_payrates= (_context.PayRates.Where(i=>i.PersonID == PersonID)).ToList();
            foreach (var item in emp_payrates)
            {
                if (!serviceIDSFromUI.Contains(Convert.ToInt32(item.MemberServiceID)))
                {
                    _context.PayRates.Remove(item);
                    result = Save();
                }
            }
            return result;
        }

        


        public bool UpsertStudent(vw_PersonDetail model, int PersonID)
        {
          var student =  _context.Students.FirstOrDefault(i => i.PersonID == model.PersonID);
            bool result = false;
            //Add new student
            if (model.IsStudent != null)
            {
                if (student == null && Convert.ToBoolean(model.IsStudent))
                {
                  _studentRepo.addStudent(PersonID);
                }
                //update student
                else if (student != null)
                {
                 _studentRepo.UpdateStudent(student, Convert.ToBoolean(!model.IsStudent));
                }
                result =  Save();
            }
           return result;
        }
        public bool UpsertInstructor(vw_PersonDetail model, int PersonID)
        {
            var instructor = _context.Instructors.FirstOrDefault(i => i.PersonID == model.PersonID);
            bool result = false;
            if (model.IsInstructor != null)
            {
                //new instructor
                if (instructor == null && Convert.ToBoolean(model.IsInstructor))
                {
                    result = _instructorRepo.addInstructor(model, PersonID);
                }
                else if (instructor != null)
                {
                   result =  _instructorRepo.UpdateInstructor(instructor, Convert.ToBoolean(!model.IsInstructor));
                }
                
            }
            return result;
        }

            public bool CreateEmployee(vw_PersonDetail model, int PersonID)
        {
           // var Emp = new Employee();

           // Emp.PersonID = PersonID;
           // Emp.ShiftID = _context.Shifts.FirstOrDefault(i => i.ShiftName == model.shift).ShiftID;
           // Emp.FacilityID = _context.Facilities.FirstOrDefault(i => i.FacilityName == model.GroupHome).FacilityID;    
           // Emp.CreatedBy = _user;
           // Emp.CreatedDate = DateTime.Now;
           // Emp.ModifiedBy = _user;
           // Emp.ModifiedDate = DateTime.Now;
           //// Emp.HireDate =Convert.ToDateTime( model.HireDate);
           // string[] hireDateFromUI = model.HireDate.Split('G');
           // if (hireDateFromUI[0] != "")
           // {
           //     Emp.HireDate = Convert.ToDateTime(hireDateFromUI[0]);
           // }
           // else
           // {
           //     Emp.HireDate = Convert.ToDateTime(model.HireDate);
           // }

           // Emp.ManagerID = _context.vw_ManagerDetail.FirstOrDefault(i => i.ManagerFullName == model.manager).ManagerID;
           // _context.Employees.Add(Emp);




           // var pay = _context.PayRates.FirstOrDefault(i => i.PersonID == PersonID);
           // if (pay == null)
           // {
           //     if (model.payRate != null)
           //     {
           //         CreateEmployeePay(model, PersonID);
           //     }
           // }

            return Save();
        }

     

        private bool CreateEmployeePay(vw_PersonDetail model, int PersonID)
        {

            //PayRate empPay = new PayRate();
            //empPay.PersonID = PersonID;
            //empPay.PayRate1 = Convert.ToDecimal(model.payRate);
            //empPay.ModifiedBy = _user;
            //empPay.CreatedBy = _user;
            //empPay.CreatedDate = DateTime.Now;
            //empPay.ModifiedDate = DateTime.Now;

            //string[] effDateFromUI = model.EffectiveDate.Split('G');
            //if (effDateFromUI[0] != "")
            //{
            //    empPay.PayRateStartDate = Convert.ToDateTime(effDateFromUI[0]);
            //}
            //else
            //{
            //    empPay.PayRateStartDate = Convert.ToDateTime(model.EffectiveDate);
            //}



            //empPay.PayRateEndDate = Convert.ToDateTime("01/20/9999");
            //_context.PayRates.Add(empPay);
            return Save();
        }


        private bool Save()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {

                foreach (DbEntityValidationResult item in e.EntityValidationErrors)

                {
                    DbEntityEntry entry = item.Entry;

                    string entityTypeName = entry.Entity.GetType().Name;
                    foreach (DbValidationError subItem in item.ValidationErrors)
                    {
                        string message = string.Format("Error '{0}' occurred in {1} at {2}",

                        subItem.ErrorMessage, entityTypeName, subItem.PropertyName);

                        //  Console.WriteLine(message);
                    }

                }
                return false;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.InnerException.Message);
                return false;
            }
            return true;
        }
    }
}