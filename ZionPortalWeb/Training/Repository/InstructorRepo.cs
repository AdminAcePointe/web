﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using ZionPortal.Dto;

namespace ZionPortal.Repository
{
    public class InstructorRepo
    {
        private ZionEntities _context;
        const string _user = "AppUser";
       
        public InstructorRepo(ZionEntities context)
        {
            _context = context;
            
        }


        //public IEnumerable<Instructor> Get()
        //{
        //    _context.Configuration.LazyLoadingEnabled = false;
        //    return _context.Instructors.Include("Person").Where(i => i.IsDeleted == false).ToList();
        //}
        public Instructor Get(int id)
        {
            return _context.Instructors.FirstOrDefault(i => i.InstructorID == id);
        }

        

        public bool Add(InstructorDto model)
        {
            Int32 pID = 0;

          

            //enforce 1 : 1 relationship with Person
            var exists = _context.Instructors.FirstOrDefault(i => (i.PersonID == model.Person.PersonId));

            if (exists != null)
                throw new InvalidOperationException("Invalid Entry");

            var Inst = new Instructor();

            Inst.PersonID = pID;
            Inst.IsDeleted = false;
            Inst.CreatedBy = _user;
            Inst.CreatedDate = DateTime.Now;
            Inst.ModifiedBy = _user;
            Inst.ModifiedDate = DateTime.Now;

            _context.Instructors.Add(Inst);

            return Save();
        }

        public bool saveSignature(int id , string signature)
        {
            var instructor = _context.Instructors.FirstOrDefault(i => i.InstructorID == id);
            if (instructor != null)
            {
                instructor.signature = signature;
                instructor.ModifiedBy = _user;
                instructor.ModifiedDate = DateTime.Now;
                return Save();

            }
            return false;
            
        }

        public string getSignature(int id)
        {
            var instructor = _context.Instructors.FirstOrDefault(i => i.InstructorID == id);
            if (instructor != null)
            {
                return instructor.signature;
            }
            return "";
        }


        public bool addInstructor(vw_PersonDetail model, int PersonID)
        {
                Instructor inst = new Instructor();
                inst.PersonID = PersonID;
                inst.CreatedBy = _user;
                inst.CreatedDate = DateTime.Now;
                inst.IsDeleted = false;
                inst.ModifiedBy = _user;
                inst.ModifiedDate = DateTime.Now;
                //inst.signature
                _context.Instructors.Add(inst);
            
            return Save();
        }

        public bool UpdateInstructor(Instructor instructor, bool isInstructor)
        {
            instructor.IsDeleted = isInstructor;
            instructor.ModifiedBy = _user;
            instructor.ModifiedDate = DateTime.Now;
            return Save();
        }


    //   public IEnumerable<InstructorDetailsDto> GetToPopulateUI()
    //    {
    //           _context.Configuration.LazyLoadingEnabled = false;
    //        //var p = 

    //           var instructor = _context.Instructors.Where(i => i.IsDeleted == false)
    //          .Select(i => new InstructorDetailsDto()
    //          {
    //              instructorID = i.InstructorID
    //              ,
    //              firstname = i.Person.FirstName
    //              ,
    //              lastname = i.Person.LastName
    //              ,
    //              phone = i.Person.Phones.FirstOrDefault().PhoneNumber
    //              ,
    //              email = i.Person.Emails.FirstOrDefault().Email1
    //              ,
    //              dob = i.Person.DOB
    //              ,
    //              // CourseSchedules = i.CourseSchedules

    //    //          ScheduleDate =   i.CourseSchedules.ElementAt(0).ScheduleDate
    //    //public Nullable<System.TimeSpan> StartTime { get; set; }
    //    //public Nullable<System.TimeSpan> EndTime { get; set; }
    //    //public int RoomID { get; set; }
    //    //public string Room { get; set; }
    //    //public string courseName { get; set; }
    //}).ToList();

        public List<InstructorDetailsDto> Get()
        {
            CourseScheduleRepo _csRepo = new CourseScheduleRepo(_context);
            _context.Configuration.LazyLoadingEnabled = false;
         
            var list = _context.Instructors.Where(i => i.IsDeleted == false)
                .Join(_context.Emails, i => i.PersonID, ii => ii.PersonID, (i, ii) => new
            {
                i,ii
            })
            .Join(_context.Phones, ins => ins.i.PersonID, ph => ph.PersonID, (ins, ph) => new
            {
                ins,ph
            })
           
            .Select(i => new InstructorDetailsDto()
            {
                instructorID = i.ins.i.InstructorID,
                firstname = i.ins.i.Person.FirstName,
                lastname = i.ins.i.Person.LastName,
                dob = i.ins.i.Person.DOB,
                signature = i.ins.i.signature,
                email = i.ins.ii.Email1,
                phone = i.ph.PhoneNumber,
                ////courseSchedules = i.ins.i.CourseSchedules,
                //courseSchedules = _csRepo.getCourseScheduleByInstructorID(i.ins.i.InstructorID),
                //room = i.ins.i.CourseSchedules.FirstOrDefault().Room.RoomName,//.Room.RoomName
                //course = i.ins.i.CourseSchedules.FirstOrDefault().Course.CourseName

            }
           ).ToList();

            return list;
        }

        //        return instructor;
        //    }

        public bool Update(InstructorDetailsDto _instructor)
        {

            var dbo = Get(_instructor.instructorID);
            if (dbo == null)
                throw new KeyNotFoundException("Not Found");

            //enforce 1 : 1 relationship with Person
            var exists = _context.Instructors.FirstOrDefault(i => i.InstructorID != _instructor.instructorID);
            if (exists != null)
                throw new InvalidOperationException("Invalid Entry");

          //  _personRepo.Update(_instructor.Person);

            dbo.ModifiedBy = _user;
            dbo.ModifiedDate = DateTime.Now;

            _context.SaveChanges();

            return true;
        }

        public bool Delete(Instructor model)
        {
            return Delete(model.InstructorID);
        }

        public bool Delete(int id)
        {
            var dbo = Get(id);
            if (dbo == null)
                throw new KeyNotFoundException("Not Found");
            dbo.IsDeleted = true;
            // _context.Instructors.Remove(dbo);


            return Save();
        }

        public IEnumerable<vwInstructorAvailable> GetAvailable()
        {
            return _context.vwInstructorAvailables.ToList();
        }

        public IEnumerable<vwInstructorUnavailable> GetUnavailable()
        {
            return _context.vwInstructorUnavailables.ToList();
        }

        public IEnumerable<vwInstructorEmployee> GetEmployee()
        {
            return _context.vwInstructorEmployees.ToList();
        }

        private bool Save()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {

                foreach (DbEntityValidationResult item in e.EntityValidationErrors)

                {
                    DbEntityEntry entry = item.Entry;

                    string entityTypeName = entry.Entity.GetType().Name;
                    foreach (DbValidationError subItem in item.ValidationErrors)
                    {
                        string message = string.Format("Error '{0}' occurred in {1} at {2}",

                        subItem.ErrorMessage, entityTypeName, subItem.PropertyName);

                        //  Console.WriteLine(message);
                    }

                }
                return false;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return false;
            }
            return true;
        }
    }
}