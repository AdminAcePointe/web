﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using ZionPortal.Dto;

namespace ZionPortal.Repository
{
    public class AddressRepo
    {
        private static List<AddressDto> _addressData;
        private static string _user = "AppUser";
        static AddressRepo()
        {
            _addressData = LoadMockData();
        }

        private bool Create(AddressDto dto)
        {
            dto.AddressId = _addressData.Max(a => a.AddressId) + 1;
            dto.CreatedBy = _user;
            dto.CreatedOn = DateTime.Now;
            dto.ModifiedBy = _user;
            dto.ModifiedOn = DateTime.Now;

            _addressData.Add(dto);
            return true;
        }

        public List<AddressDto> GetAllAddresses()
        {
            return _addressData;
        }

        public AddressDto GetAddressById(int id)
        {
            return _addressData.Find(a => a.AddressId == id);
        }

        public AddressDto GetAddressByPersonId(int id)
        {
            return _addressData.Find(a => a.PersonId == id);
        }

        public bool Update(AddressDto dto)
        {
            var AddressInDb = _addressData.Find(i => i.AddressId == dto.AddressId);
            if (AddressInDb == null)
                return false;
            AddressInDb.AddressLine1 = dto.AddressLine1;
            AddressInDb.AddressLine2 = dto.AddressLine2;
            AddressInDb.City = dto.City;
            AddressInDb.Country = dto.Country;
            AddressInDb.PersonId = dto.PersonId;
            AddressInDb.State = dto.State;
            AddressInDb.Zip = dto.Zip;
            AddressInDb.ModifiedBy = _user;
            AddressInDb.ModifiedOn = DateTime.Now;
            return true;
        }

        public bool Delete(AddressDto dto)
        {
            _addressData.Remove(dto);
            return true;
        }

        private static List<AddressDto> LoadMockData()
        {
            string allText = System.IO.File.ReadAllText(HostingEnvironment.MapPath(@"~/MockData/Address.js"));
            var data = JsonConvert.DeserializeObject<List<AddressDto>>(allText);
            return data;
        }
    }
}