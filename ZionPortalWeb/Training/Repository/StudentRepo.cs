﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using ZionPortal.Dto;

namespace ZionPortal.Repository
{
    public class StudentRepo
    {
        private ZionEntities _context;
       // private PersonRepo _personRepo;

        private static string _user = "AppUser";


        

   
        public StudentRepo(ZionEntities context)
        {
            _context = context;
          //  _personRepo = new PersonRepo(context);
        }

        public bool addStudent(int PersonID) {
            Student stud = new Student();
            stud.IsDeleted = false;
            stud.CreatedBy = _user;
            stud.CreatedDate = DateTime.Now;
            stud.ModifiedBy = _user;
            stud.ModifiedDate = DateTime.Now;
            stud.PersonID = PersonID;
            _context.Students.Add(stud);
            return Save();
        }

        public bool UpdateStudent(Student student , bool isStudent)
        {
            student.IsDeleted = isStudent;
            student.ModifiedBy = _user;
            student.ModifiedDate = DateTime.Now;
            return Save();
        }



        //    public bool Add(StudentDto model)
        //{
        //    //person
        //    Int32 personID = _personRepo.Create(model.Person);
        //    model.IsDeleted = false;
        //    model.CreatedBy = _user;
        //    model.CreatedDate = DateTime.Now;
        //    model.ModifiedBy = _user;
        //    model.ModifiedDate = DateTime.Now;

        //    var stu = new Student();
        //    stu.PersonID = personID;
        //    stu.IsDeleted = false;
        //    stu.CreatedBy = _user;
        //    stu.CreatedDate = DateTime.Now;
        //    stu.ModifiedBy = _user;
        //    stu.ModifiedDate = DateTime.Now;

        //    _context.Students.Add(stu);

        //    return Save();
        //}

        public IEnumerable<StudentDetailsDto> Get()
        {
            _context.Configuration.LazyLoadingEnabled = false;
           
            var student = _context.Students.Where(i => i.IsDeleted == false)
                .Include(i => i.Person.Addresses)
                .Include(i => i.Person.Phones)
                .Include(i => i.Person.Emails)
               .Select(i => new StudentDetailsDto()
               {
                   studentID = i.StudentID
                   ,
                   firstname = i.Person.FirstName
                   ,
                   lastname = i.Person.LastName
                   ,
                   phone = i.Person.Phones.FirstOrDefault().PhoneNumber
                   ,
                   email = i.Person.Emails.FirstOrDefault().Email1
                   ,
                   dob = i.Person.DOB
                   ,
                   CourseRegistrations =  i.CourseRegistrations
               }).ToList();

            return student;
        }

        public Student Get(int id)
        {
            return _context.Students.Where(i => i.IsDeleted == false).FirstOrDefault(i => i.StudentID == id);
        }

        public string studentCardCount()
        {
            var b = _context.vw_StudentSummary.Count();
            if ( b > 0 )

            return  _context.vw_StudentSummary.Where( i => i.CourseRegistrationID > 0).Select(i=>i.StudentID).Distinct().Count().ToString();

            return "0";
        }

        public string studentCardExpiredCerts()
        {

            var b = _context.vw_StudentSummary.Count();
            if (b > 0)

                return _context.vw_StudentSummary.Sum(i => i.CertExpired).ToString();

            return "0";
        }
        public string studentCardNewCount()
        {
            var currmonth = DateTime.Now.Month;

            var b = _context.vw_StudentSummary.Count();
            if (b > 0)
                return _context.vw_StudentSummary.Sum(i => i.NewStudentCurrentMonth).ToString();

            return "0"; 
        }

        //public bool Update(Student model)
        //{
        //    var dbo = Get(model.StudentID);
        //    if (dbo == null)
        //        throw new KeyNotFoundException("Not Found");


        //    var exists = _context.Students.FirstOrDefault(i => (i.PersonID == model.Person.PersonID) & (i.StudentID != model.StudentID));
        //    if (exists != null)
        //        throw new InvalidOperationException("Invalid Entry");

        //    _personRepo.Update(model.Person);
        //    // update Person
        //    dbo.ModifiedBy = _user;
        //    dbo.ModifiedDate = DateTime.Now;

        //    _context.SaveChanges();

        //    return true;
        //}

        //public bool Delete(Student model)
        //{
        //    return Delete(model.StudentID);
        //}

        [System.Web.Mvc.HttpDelete]
        public bool Delete(int id)
        {
            var dbo = Get(id);
            if (dbo == null)
                throw new KeyNotFoundException("Not Found");

            dbo.IsDeleted = true;
            dbo.ModifiedBy = _user;
            dbo.ModifiedDate = DateTime.Now;

            _context.SaveChanges();

            return true;
        }

        private bool Save()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {

                foreach (DbEntityValidationResult item in e.EntityValidationErrors)

                {
                    DbEntityEntry entry = item.Entry;

                    string entityTypeName = entry.Entity.GetType().Name;
                    foreach (DbValidationError subItem in item.ValidationErrors)
                    {
                        string message = string.Format("Error '{0}' occurred in {1} at {2}",

                        subItem.ErrorMessage, entityTypeName, subItem.PropertyName);

                        //  Console.WriteLine(message);
                    }

                }
                return false;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return false;
            }
            return true;
        }
    }
}