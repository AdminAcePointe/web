﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using ZionPortal.Dto;

namespace ZionPortal.Repository
{
    public class WidgetCourseSchedulerRepo
    {
        private const string _user = "AppUSer";
        private Random random = new Random();

        private ZionEntities _context = new ZionEntities();
        private CourseRepo _courseRepo;
        private CourseScheduleRepo _courseScheduleRepo;
        private InstructorRepo _instructorRepo;
        private FacilityRepo _facilityRepo;
        private RoomRepo _roomRepo;

        public WidgetCourseSchedulerRepo()
        {
            _courseRepo = new CourseRepo(_context);
            _courseScheduleRepo = new CourseScheduleRepo(_context);
            _instructorRepo = new InstructorRepo(_context);
            _facilityRepo = new FacilityRepo(_context);
            _roomRepo = new RoomRepo(_context);
        }

        public List<WidgetCourseSchedulerDto> Get()
        {

            var u = _courseScheduleRepo.Get().Select(i => new WidgetCourseSchedulerDto
            {
                CourseScheduleId = i.CourseScheduleID,
                Text = i.Course.CourseName,
                Description = i.Course.CourseDescription,
                FacilityId = i.Room.Facility.FacilityID,
                FacilityName = i.Room.Facility.FacilityName,
                RoomId = i.Room.RoomID,
                RoomName = i.Room.RoomName,
                InstructorId = i.Instructor.InstructorID,
                InstructorName = null,
                CourseId = i.Course.CourseID,
                CoursePrice = i.Course.CoursePrice,
                ScheduleDate = Convert.ToDateTime(i.ScheduleDate),
                StartTime = i.StartTime,
                EndTime = i.EndTime,
                //StartDate =Convert.ToDateTime(i.ScheduleDate) + " " + i.StartTime.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) + " " + i.StartTime,
                //StartDate = Convert.ToDateTime(i.ScheduleDate).ToString("dd/MM/yyyy hh:mm tt", CultureInfo.InvariantCulture),

                StartDate = Convert.ToDateTime(i.ScheduleDate).Date.Add( i.StartTime.Value).ToString("yyyy-MM-dd hh:mm tt"),
                //EndDate = Convert.ToDateTime(i.ScheduleDate).ToShortDateString(),//.Date.Add((i.StartTime.HasValue ? i.EndTime.Value : new TimeSpan(0))).ToString("yyyy-MM-ddThh:mm"),
                EndDate = Convert.ToDateTime(i.ScheduleDate).Date.Add(i.EndTime.Value).ToString("yyyy-MM-dd hh:mm tt"),


                AllDay = false,
               Isdeleted = false,
               Priority = 1,
               //RecurrenceRule = i.RecurrenceRule,
               //RecurrenceException = i.RecurrenceException
            }).ToList();
            return u;
        }

        public WidgetCourseSchedulerDto Get(int id)
        {
            var y = Get();
            var t = Get().Find(i => i.CourseScheduleId == id & i.Isdeleted == false);
            return Get().Find(i => i.CourseScheduleId == id & i.Isdeleted == false);
        }

        public List <WidgetRoomDto> GetRoom()
        {

            return _roomRepo.Get().Select(i => new WidgetRoomDto
            {
                Id = i.RoomID,
                Text = i.RoomName,
                Color = AssignRandomColors()
            }).ToList();
        }

        public List <WidgetFacilityDto> GetFacility()
        {
            return _facilityRepo.Get().Select(i => new WidgetFacilityDto
            {
                Id = i.FacilityID,
                Text = i.FacilityName,
                Color = AssignRandomColors()
            }).ToList();
        }

        public List<WidgetInstructorDto> GetInstructor()
        {
            return _instructorRepo.Get().Select(i => new WidgetInstructorDto
            {
                Id = i.instructorID,
                Text = i.firstname + ' ' + i.lastname,
                Color = AssignRandomColors()
            }).ToList();
        }

        public List<WidgetCourseDto> GetCourse()
        {
            return _courseRepo.Get().Select(i => new WidgetCourseDto
            {
                Id = i.CourseID,
                Text = i.CourseName + " (" + i.CourseCode + ")" + " - $" + i.CoursePrice,
                Color = AssignRandomColors()
            }).ToList();
        }

        public bool Add(WidgetCourseSchedulerDto dto)
        {
            DateTime sdt = DateTime.Parse(dto.StartDate);

            DateTime edt = DateTime.Parse(dto.EndDate);


            var cs = new CourseScheduleDto()
            {
                Course = Mapper.Map<CourseDto>(_courseRepo.Get(dto.CourseId)),
                ScheduleDate = sdt,
                StartTime = sdt.TimeOfDay,
                EndTime = edt.TimeOfDay,
                Instructor = Mapper.Map<InstructorDto>(_instructorRepo.Get(dto.InstructorId)),
                Room = Mapper.Map<RoomDto>(_roomRepo.Get(dto.RoomId)),
                AllDay = dto.AllDay,
                Priority = dto.Priority,
                // RecurrenceRule = dto.RecurrenceRule,
                // RecurrenceException = dto.RecurrenceException,
                CreatedBy = _user,
                CreatedDate = DateTime.Now,
                ModifiedBy = _user,
                Isdeleted = false,
                ModifiedDate = DateTime.Now
               
            };

            //ensure that room belongs to facility
            var isValidRoom = IsValidRoom(cs, dto);

            if (isValidRoom)
                cs.RoomID = dto.RoomId;
                _courseScheduleRepo.Add(cs);

            return isValidRoom;
        }
        
        public bool Update(WidgetCourseSchedulerDto dto)
        {
            // update course
            // update instructor
            // update room

            var dbo = _courseScheduleRepo.Get(dto.CourseScheduleId);
            if (dbo != null)
            {
                var cs = Mapper.Map<CourseScheduleDto>(dbo);
                cs.Course = Mapper.Map<CourseDto>(_courseRepo.Get(dto.CourseId));
                cs.ScheduleDate = dto.ScheduleDate; //TODO: Oblitriate this field from the DB
                cs.StartTime = dto.StartTime;
                cs.EndTime = dto.EndTime;
                cs.Instructor = Mapper.Map<InstructorDto>(_instructorRepo.Get(dto.InstructorId));
                cs.Room = Mapper.Map<RoomDto>(_roomRepo.Get(dto.RoomId));
                cs.AllDay = dto.AllDay;
                cs.Priority = dto.Priority;
                //RecurrenceRule = dto.RecurrenceRule,
                //RecurrenceException = dto.RecurrenceException

                //ensure that room belongs to facility
                var isValidRoom = IsValidRoom(cs, dto);

                //if (isValidRoom)
                    _courseScheduleRepo.Update(cs);

                return true;
            }

            return false;
        }
       
        public bool Delete(int id)
        {
            return _courseScheduleRepo.Delete(id);
        }

        private bool IsValidRoom(CourseScheduleDto a, WidgetCourseSchedulerDto b)
        {
            return a.Room.FacilityId == Mapper.Map<FacilityDto>(_facilityRepo.Get(b.FacilityId)).FacilityId;
        }
        
        private string AssignRandomColors()
        {
            Array values = Enum.GetValues(typeof(eColors));
            
            var color = (eColors)values.GetValue(random.Next(values.Length));
            return color.ToString();
        }

        enum eColors
        {
            //white,
            //silver,
            gray,
            black,
            red,
            maroon,
            olive,
            green,
            teal,
            blue,
            navy,
            fuchsia,
            purple
        }
    }
}