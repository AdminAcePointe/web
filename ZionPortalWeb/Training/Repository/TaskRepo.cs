﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZionPortal.Dto;

namespace ZionPortal.Repository
{
    public class TaskRepo
    {
        ZionEntities _context;
        private const string _user = "AppUser";

        public TaskRepo(ZionEntities context)
        {
            _context = context;
        }

        public Task Add(Task model)
        {
            model.PersonId = 1; //TODO: Fix this once you implement Authentication.

            model.CreatedBy = _user;
            model.CreatedDate = DateTime.Now;
            model.ModifiedBy = _user;
            model.ModifiedDate = DateTime.Now;

            _context.Tasks.Add(model);
            var result = _context.SaveChanges();
            return model;
        }

        public IEnumerable<Task> Get()
        {
            return _context.Tasks.ToList();
        }

        public Task Get(int id)
        {
            return _context.Tasks.FirstOrDefault(a => a.TaskId == id);
        }

        public bool Update(Task model)
        {
            var dbo = Get(model.TaskId);
            if (dbo == null)
                throw new KeyNotFoundException("Not Found");

            //var personDbo = _context.People.Find(model.PersonId);
            if(dbo == null)
                throw new KeyNotFoundException("Not Found");

            dbo.Content = model.Content;
            dbo.PersonId = model.PersonId;
            dbo.IsComplete = model.IsComplete;

            dbo.ModifiedBy = _user;
            dbo.ModifiedDate = DateTime.Now;
            return (Save());
        }

        public bool Delete(int id)
        {
            var dbo = Get(id);
            if (dbo == null)
                throw new KeyNotFoundException("Not Found");

            _context.Tasks.Remove(dbo);

            return (Save());
        }

        private bool Save()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return false;
            }

            return true;
        }
    }
}