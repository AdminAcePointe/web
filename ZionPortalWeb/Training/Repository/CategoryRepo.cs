﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZionPortal.Repository
{
    public class CategoryRepo
    {
        private ZionEntities _context;
        private const string _user = "AppUser";

        public CategoryRepo(ZionEntities context)
        {
            _context = context;
        }

        public IEnumerable<Category> Get()
        {
            return _context.Categories.ToList();
        }

        public Category GetCategory(int id)
        {
            return _context.Categories.Where(a=> a.IsDeleted == false).FirstOrDefault(a => a.CategoryID == id);
        }

        public CourseSubcategory GetSubcategory(int id)
        {
            return _context.CourseSubcategories.Where(a => a.IsDeleted == false).FirstOrDefault(a => a.SubcategoryID == id);
        }

        public CourseSubcategory GetSubcategoryByCategoryId(int id)
        {
            return _context.CourseSubcategories.Where(a => a.IsDeleted == false).FirstOrDefault(a => a.CategoryID == id);
        }

        public bool AddCategory(Category model)
        {
            model.CreatedBy = _user;
            model.CreatedDate = DateTime.Now;
            model.ModifiedBy = _user;
            model.ModifiedDate = DateTime.Now;
            model.IsDeleted = false;
            _context.Categories.Add(model);
            _context.SaveChanges();

            return true;
        }

        public bool UpdateCategory(Category model)
        {
            var dbo = GetCategory(model.CategoryID);
            if (dbo == null)
                throw new KeyNotFoundException("Not Found");

            dbo.CategoryName = model.CategoryName;
            dbo.CategoryDescription = model.CategoryDescription;
            dbo.ModifiedBy = _user;
            dbo.ModifiedDate = DateTime.Now;

            _context.SaveChanges();

            return true;
        }

        public bool DeleteCategory(Category model)
        {
            return DeleteCategory(model.CategoryID);
        }

        public bool DeleteCategory(int id)
        {
            var dbo = GetCategory(id);
            if (dbo == null)
                throw new KeyNotFoundException("Not Found");

            dbo.IsDeleted = true;
            _context.SaveChanges();

            return true;
        }

        public bool AddSubcategory(CourseSubcategory model)
        {
            model.CreatedBy = _user;
            model.CreatedDate = DateTime.Now;
            model.ModifiedBy = _user;
            model.ModifiedDate = DateTime.Now;
            model.IsDeleted = false;
            _context.CourseSubcategories.Add(model);
            _context.SaveChanges();

            return true;
        }

        public bool UpdateSubcategory(CourseSubcategory model)
        {
            var dbo = GetSubcategory(model.SubcategoryID);
            if (dbo == null)
                throw new KeyNotFoundException("Not Found");

            dbo.SubcategoryName = model.SubcategoryName;
            dbo.ModifiedBy = _user;
            dbo.ModifiedDate = DateTime.Now;

            _context.SaveChanges();

            return true;
        }

        public bool DeleteSubcategory(CourseSubcategory model)
        {
            return DeleteSubcategory(model.SubcategoryID);
        }

        public bool DeleteSubcategory(int id)
        {
            var dbo = GetSubcategory(id);
            if (dbo == null)
                throw new KeyNotFoundException("Not Found");

            dbo.IsDeleted = true;
            _context.SaveChanges();

            return true;
        }
    }
}