﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZionPortal.Dto;
using System.Data.Entity.Validation;
using System.Data.Entity.Infrastructure;


namespace ZionPortal.Repository
{
    public class PersonDetailRepo
    {

        private ZionEntities _context;
        private PersonRepo _PersonRepo;
        private EmployeeRepo _EmployeeRepo;

        public PersonDetailRepo(ZionEntities context)
        {
            _context = context;
            _PersonRepo = new PersonRepo(_context);
            _EmployeeRepo = new EmployeeRepo(_context);
        }

        public List<vw_ServiceDetails> GetEmployeeServiceInfo(int employeeid)
        {
            _context.Configuration.LazyLoadingEnabled = false;
            return _context.vw_ServiceDetails.Where(i => i.EmpID == employeeid ).ToList();

        }

        public List<vw_ServiceDetails> GetEmployeeServiceTypeInfo(int employeeid)
        {
            _context.Configuration.LazyLoadingEnabled = false;
            return _context.vw_ServiceDetails.Where(i => i.EmpID == employeeid).ToList();

        }


        public vw_PersonDetail GetPersonInfo(int employeeid)
        {
            var pers = _context.vw_PersonDetail.FirstOrDefault(i => i.EmployeeID == employeeid);
           
            return pers;
        }

       
        public bool PostPersonInfo(vw_PersonDetail ddata, int employeeid , string services, string UpdatedProfilePic )
        {
            int PersonID, EmailID, PhoneID, AddressID, PersonEmergencyContactID;
            bool result;
           
            if (ddata.PersonID == 0) // Insert
            {
                // Create new person
                PersonID = _PersonRepo.CreatePerson(ddata, UpdatedProfilePic);
                // Create new person email
                EmailID = _PersonRepo.CreateEmail(ddata,PersonID);
                // Create new person phone
                PhoneID = _PersonRepo.CreatePhone(ddata, PersonID);
                // Create new person address
                AddressID = _PersonRepo.CreateAddress(ddata, PersonID);
                // Create new person emergency contact
                PersonEmergencyContactID = _PersonRepo.CreatePersonEmergency(ddata, PersonID);               
                // Create new employee record
                result = _EmployeeRepo.CreateEmployee(ddata, PersonID);
                // Create / Update Instructor
                result = _EmployeeRepo.UpsertInstructor(ddata, PersonID);
                //  Create / Update Student
                result = _EmployeeRepo.UpsertStudent(ddata, PersonID);
                //Create new member records
               // result = _EmployeeRepo.UpsertEmployeeMembers(ddata, members);
                // Create new service records
                result = _EmployeeRepo.UpsertEmployeeServices(ddata, services);
                //update or insert pay for employee
               // result = _EmployeeRepo.UpsertEmployeePayRates(ddata,)
            }
            else // update
            {
                result = _PersonRepo.UpdatePerson(ddata, services, UpdatedProfilePic);
            }           
            return result;
        }

        public bool postEmployeePayRates(ICollection<vw_PayPerServiceDetails>  payInfo , int personID)
        {
            return _EmployeeRepo.UpsertEmployeePayRates(payInfo, personID);
           // return true;

        }

        public List<vw_PersonDetail> GetPersonInfoByManagerID(int managerID)
        {
            return _context.vw_PersonDetail.ToList();
        }

        public List<Role> GetRoles()
        {
            return  _context.Roles.Where(i=>i.RoleName != "Student" && i.RoleName != "Instructor" ).ToList();
          
        }

        public List<Shift> GetShifts()
        {
            
            var y =  _context.Shifts.ToList();
            return y;
        }

        public List<Facility> GetGroupHomes()
        {
            return _context.Facilities.ToList();

        }

        public List<vw_ManagerDetail> GetManagers()
        {
            return _context.vw_ManagerDetail.ToList();
        }

        public string[] GetStates()
        {
            string[] states = { "AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "FL", "GA", "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY" };
            return states;
        }

        public string[] GetGender()
        {
            string[] gender = {"Female", "Male", "Other" };
            return gender;
        }

        public string[] GetRelationships()
        {
            string[] relationships = { "Spouse", "Son", "Daughter","Sibling","Relative" , "Friend"};
            return relationships;
        }

        public string[] GetMaritalStatus()
        {
            string[] status = { "Single","Married", "Divorced", "Separated" };
            return status;

        }


        private bool Save()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {

                foreach (DbEntityValidationResult item in e.EntityValidationErrors)
                {
                    DbEntityEntry entry = item.Entry;
                    string entityTypeName = entry.Entity.GetType().Name;

                    foreach (DbValidationError subItem in item.ValidationErrors)
                    {
                        string message = string.Format("Error '{0}' occurred in {1} at {2}",
                            subItem.ErrorMessage, entityTypeName, subItem.PropertyName);
                    }

                }
                return false;

            }

            return true;
        }






    }
}