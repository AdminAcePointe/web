﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZionPortal.Dto;

namespace ZionPortal.Repository
{
    public class LocationRepo
    {
        private ZionEntities _context;
        private List<vw_CityStateCountry> locations;

        public LocationRepo()
        {
            _context = new ZionEntities();
            locations = _context.vw_CityStateCountry.ToList();
        }

        public List<vw_CityStateCountry> GetLocations()
        {
            return _context.vw_CityStateCountry.ToList();
          
                
        }

        public LocationDto GetLocationByCityID(int cityID)
        {
            var location = locations.Find(i => i.cityID == cityID);
            if (location != null)
            {
                return composeLocation(location);
            }
            return null;

        }

        public LocationDto GetLocationByStateID(int stateID)
        {
            var location = locations.Find(i => i.stateID == stateID);
            if (location != null)
            {
               return  composeLocation(location);
            }
            return null;

        }

        private LocationDto composeLocation(vw_CityStateCountry location)
        {
            if (location != null)
            {
                LocationDto myLoc = new LocationDto();
                myLoc.cityID = location.cityID;
                myLoc.cityName = location.cityName;
                myLoc.stateID = location.stateID;
                myLoc.stateName = location.stateName;
                return myLoc;
            }
            return null;

        }

        public LocationDto getDistinctState(int stateID)
        {
            var location = locations.Find(i => i.stateID == stateID);
            if (location != null)
            {
                LocationDto myLoc = new LocationDto();
                myLoc.cityID = location.cityID;
                myLoc.cityName = location.cityName;
                myLoc.stateID = location.stateID;
                myLoc.stateName = location.stateName;
                return myLoc;
            }
            return null;

        }


        public List<vw_CityStateCountry> getDistinctState2(int stateID)
        {
            return _context.vw_CityStateCountry.Where(i => i.stateID == stateID).Take(1).ToList();


        }

    }
}