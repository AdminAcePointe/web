﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using ZionPortal.Dto;

namespace ZionPortal.Repository
{
    public class MemberRepo
    {
        private ZionEntities _context;
        private PersonRepo _PersonRepo;

        public MemberRepo(ZionEntities context)
        {
            _context = context;
            _PersonRepo = new PersonRepo(_context);
        }

        public List<vw_ServiceDetails> GetMembersWithServices()
        {

            return _context.vw_ServiceDetails.ToList();
        }

        public List<vw_ServiceDetails> GetMemberServices(int memberID)
        {
            var u = _context.vw_ServiceDetails.Where(i => i.MemberID == memberID).ToList();
            return u;
        }

        public List<vw_MemberDetail> GetAllMembers()
        {
            return _context.vw_MemberDetail.ToList();
        }
       
        public bool AddNewMember(MemberUIDetailsDTO ddata, int memberID, string UpdatedProfilePic)
        {
            bool result = false;
            if (ddata != null)
            {
                vw_PersonDetail personDetails = new vw_PersonDetail();
                personDetails.addressline1 = ddata.ddata.addressline1;
                personDetails.city = ddata.ddata.city;
                //personDetails.SSN = ddata.ddata.SSN;
                personDetails.state = ddata.ddata.state;
                personDetails.ZipCode = ddata.ddata.ZipCode;
                personDetails.email = ddata.ddata.email;
                personDetails.FirstName = ddata.ddata.FirstName;
                personDetails.LastName = ddata.ddata.LastName;
                personDetails.MaritalStatus = ddata.ddata.MaritalStatus;
                personDetails.MiddleName = ddata.ddata.MiddleName;
                personDetails.PersonID = ddata.ddata.PersonID;
                personDetails.phoneNumber = ddata.ddata.phoneNumber;
                personDetails.DateOfBirth = ddata.ddata.DateOfBirth;
                personDetails.Gender = ddata.ddata.Gender;

                int personID = PostMemberAsPerson(personDetails, UpdatedProfilePic);

                //member details
                int memberID_ = PostMemberDetails(ddata.ddata, personID);

                //member services
                if (ddata.vw_ServiceDetails != null)
                {
                    result = UpsertMemberServices(ddata.vw_ServiceDetails.ToList(), memberID_);
                }

                //member emergency contacts
                if (ddata.PersonEmergencyContactDto != null)
                {
                    result = UpsertMemberEmergercyContact(ddata.PersonEmergencyContactDto.ToList(), memberID_);
                }
                
            }
            return result;
        }

        public List<String> GetMemberStatuses()
        {
            return _context.MemberStatus.ToList().Select(i => i.MemberStatus).ToList();
        }

        public vw_MemberDetail GetMemberInfo(int memberID)
        {
           return _context.vw_MemberDetail.FirstOrDefault(i => i.memberID == memberID);

        }
        

        public int PostMemberAsPerson(vw_PersonDetail ddata, String UpdatedProfilePic)
        {
            int PersonID = 0, EmailID, PhoneID, AddressID;
            
            if (ddata.PersonID == 0) // Insert
            {
                // Create new person
                PersonID = _PersonRepo.CreatePerson(ddata, UpdatedProfilePic);
                // Create new person email
                EmailID = _PersonRepo.CreateEmail(ddata, PersonID);
                // Create new person phone
                PhoneID = _PersonRepo.CreatePhone(ddata, PersonID);
                // Create new person address
                AddressID = _PersonRepo.CreateAddress(ddata, PersonID);
            }
            else // update
            {
                PersonID = _PersonRepo.UpdatePersonForMember(ddata, UpdatedProfilePic);
            }
            return PersonID;
        }

        public bool UpsertMemberServices(List<vw_ServiceDetails> memberServices, int memberID)
        {
            bool result = false;
            var UI_allMemberServiceEmpID = memberServices.Select(i => i.MemberEmployeeService).ToList();
            if (memberServices != null)
            {
                UI_allMemberServiceEmpID.RemoveAll(i => i == 0);
                foreach (var serviceDetail in memberServices)
                {
                    //new service 
                    if (serviceDetail.MemberEmployeeService == 0)
                    {
                        MemberEmployeeService newService = new MemberEmployeeService();
                        newService.MemberServiceID = serviceDetail.MemberServiceId;
                        newService.EmployeeID = serviceDetail.EmpID;
                        newService.CreatedDate = DateTime.Now;
                        newService.CreatedBy = "AppUSer";
                        newService.ModifiedBy = "AppUSer";
                        newService.ModifiedDate = DateTime.Now;
                        newService.MemberID = memberID;


                        _context.MemberEmployeeServices.Add(newService);
                        result = Save();

                        int memberEmpServiceID = newService.MemberEmployeeService1;
                        UI_allMemberServiceEmpID.Add(memberEmpServiceID);
                    }

                    else //update
                    {
                        var dbo =_context.MemberEmployeeServices.FirstOrDefault(i => i.MemberEmployeeService1 == serviceDetail.MemberEmployeeService);
                        dbo.MemberServiceID = serviceDetail.MemberServiceId;
                        dbo.EmployeeID = serviceDetail.EmpID;
                        dbo.ModifiedBy = "AppUSer";
                        dbo.ModifiedDate = DateTime.Now;
                        dbo.MemberID = memberID;

                        result = Save();

                    }

                }
            }

            var DB_allEmployeeMemberServiceID = _context.MemberEmployeeServices.Where(i => i.MemberID == memberID).Select(i => i.MemberEmployeeService1).ToList();


            foreach (var i in DB_allEmployeeMemberServiceID)
            {
                if (!UI_allMemberServiceEmpID.Contains(i))
                {
                    //service has been deleted from the ui
                    var deletedMemberEmployeeServices = _context.MemberEmployeeServices.FirstOrDefault(u => u.MemberEmployeeService1 == i);
                    _context.MemberEmployeeServices.Remove(deletedMemberEmployeeServices);
                    result = Save();
                }
            }

            return result;
        }




            public bool UpsertMemberEmergercyContact(List<PersonEmergencyContactDto> emergencyContact , int memberID)
        {
            bool result = false;
            int personID = _context.Members.FirstOrDefault(i => i.MemberID == memberID).PersonID;
            var UI_allContactsID = emergencyContact.Select(i => i.PersonEmergencyContactID).ToList();

            if (emergencyContact != null)
            {

                UI_allContactsID.RemoveAll(i => i == 0);

                foreach (var contact in emergencyContact)
                {
                    //new
                    if (contact.PersonEmergencyContactID == 0)
                    {
                        PersonEmergencyContact newContact = new PersonEmergencyContact();
                        newContact.PersonID = personID;
                        newContact.FirstName = contact.FirstName;
                        newContact.LastName = contact.LastName;
                        newContact.Phone = contact.Phone;
                        newContact.Email = contact.Email;
                        newContact.Relationship = contact.Relationship;
                        newContact.CreatedBy = "AppUser";
                        newContact.ModifiedBy = "AppUser";
                        newContact.ModifiedDate = DateTime.Now;
                        newContact.CreatedDate = DateTime.Now;


                         _context.PersonEmergencyContacts.Add(newContact);
                        result = Save();

                        int emergencyContactID = newContact.PersonEmergencyContactID;
                        UI_allContactsID.Add(emergencyContactID);

                        
                    }
                    else
                    {   //update
                        var oldContact = _context.PersonEmergencyContacts.FirstOrDefault(i => i.PersonEmergencyContactID == contact.PersonEmergencyContactID);
                        oldContact.Phone = contact.Phone;
                        oldContact.Email = contact.Email;
                        oldContact.FirstName = contact.FirstName;
                        oldContact.LastName = contact.LastName;
                        oldContact.ModifiedBy = "AppUser";
                        oldContact.ModifiedDate = DateTime.Now;
                        oldContact.Relationship = contact.Relationship;

                        result = Save();
                    }

                }


                var DB_allContactsID = _context.PersonEmergencyContacts.Where(i => i.PersonID == personID).Select(i => i.PersonEmergencyContactID).ToList();
                


                foreach (var i in DB_allContactsID)
                {
                    if (!UI_allContactsID.Contains(i))
                    {
                        //emergency contact has been deleted from the ui
                        var deletedContact = _context.PersonEmergencyContacts.FirstOrDefault(u => u.PersonEmergencyContactID == i);
                        _context.PersonEmergencyContacts.Remove(deletedContact);
                        result = Save();
                    }
                }
                

            }
            return result;
        }





            public int PostMemberDetails(vw_MemberDetail ddata, int personID)
        {
            bool result = false;
            if (ddata.memberID <= 0) // insert
            {
                //new member
                Member newMember = new Member();
                newMember.AHCCCSID = ddata.Ahcccsid;
                newMember.ASSISTID = ddata.assistid;
                newMember.CreatedBy = "AppUSer";
                newMember.CreatedDate = DateTime.Now;
                newMember.FacilityID = _context.Facilities.FirstOrDefault(i => i.FacilityName == ddata.GroupHome).FacilityID;
                newMember.MemberStatusID = _context.MemberStatus.FirstOrDefault(i => i.MemberStatus == ddata.MemberStatus).MemberStatusID;
                newMember.ModifiedBy = "AppUSer";
                newMember.ModifiedDate = DateTime.Now;
                newMember.StatusDate = DateTime.Now;
                newMember.PersonID = personID;
                newMember.IsInactive = false;

                _context.Members.Add(newMember);
                Save();
                return newMember.MemberID;
            }
            else
            {
                //update
                var member = _context.Members.FirstOrDefault(i => i.MemberID == ddata.memberID);
                member.AHCCCSID = ddata.Ahcccsid;
                member.ASSISTID = ddata.assistid;
                member.FacilityID = _context.Facilities.FirstOrDefault(i => i.FacilityName == ddata.GroupHome).FacilityID;

                if (member.MemberStatusID != _context.MemberStatus.FirstOrDefault(i => i.MemberStatus == ddata.MemberStatus).MemberStatusID)
                {
                    //Status changed
                    member.StatusDate = DateTime.Now;
                }
                member.MemberStatusID = _context.MemberStatus.FirstOrDefault(i => i.MemberStatus == ddata.MemberStatus).MemberStatusID;
                member.ModifiedBy = "AppUSer";
                member.ModifiedDate = DateTime.Now;
                result = Save();
                return member.MemberID;
            }


            
        }



        public List<PersonEmergencyContact> GetEmergencyContacts(int memberID)
        {
            if (_context.Members.FirstOrDefault(i => i.MemberID == memberID) != null)
            {

                int personID = _context.Members.FirstOrDefault(i => i.MemberID == memberID).PersonID;

                if (_context.PersonEmergencyContacts.Where(i => i.PersonID == personID) != null)
                {
                    var emergencyContact = _context.PersonEmergencyContacts.Where(i => i.PersonID == personID).ToList();
                    return emergencyContact;
                }
            }

            return null;
        }





        private bool Save()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {

                foreach (DbEntityValidationResult item in e.EntityValidationErrors)

                {
                    DbEntityEntry entry = item.Entry;

                    string entityTypeName = entry.Entity.GetType().Name;
                    foreach (DbValidationError subItem in item.ValidationErrors)
                    {
                        string message = string.Format("Error '{0}' occurred in {1} at {2}",

                        subItem.ErrorMessage, entityTypeName, subItem.PropertyName);

                        //  Console.WriteLine(message);
                    }

                }
                return false;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return false;
            }
            return true;
        }



    }
}