﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZionPortal.Repository
{
    public class MemberServicesRepo
    {
        private ZionEntities _context;

        public MemberServicesRepo(ZionEntities context)
        {
            _context = context;
        }

        public List<MemberServiceType> getServicesType()
        {
            return _context.MemberServiceTypes.ToList();

        }

      

        public List<vw_Concat_ServiceType_Service> getServices()
        {

            return _context.vw_Concat_ServiceType_Service.ToList();

        }

        public List<MemberService> getMemberService()
        {
            _context.Configuration.LazyLoadingEnabled = false;
            return _context.MemberServices.ToList();

        }

        public List<MemberService> getServicesByServiceTypeId(int ServiceTypeId)
        {
            return _context.MemberServices.Where(i => i.MemberServiceTypeID == ServiceTypeId).ToList();

        }

        public List<vw_ServiceDetails> getMembersAssignedToEmployee(int EmployeeID)
        {
            return _context.vw_ServiceDetails.Where(i => i.EmpID == EmployeeID).ToList();

        }
    }
}