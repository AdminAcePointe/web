﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZionPortal.Dto;
namespace ZionPortal.Repository
{
    public class AnnouncementRepo
    {
        ZionEntities _context;
        private const string _user = "AppUser";

        public AnnouncementRepo(ZionEntities context)
        {
            _context = context;
        }

        public Announcement Add(Announcement model)
        {
            model.CreatedBy = _user;
            model.CreatedDate = DateTime.Now;
            model.ModifiedBy = _user;
            model.ModifiedDate = DateTime.Now;

            _context.Announcements.Add(model);
            var result =_context.SaveChanges();
            return model;
        }
        
        public IEnumerable<Announcement> Get()
        {
            return _context.Announcements.ToList();
        }

        public Announcement Get(int id)
        {
            return _context.Announcements.FirstOrDefault(a => a.AnnouncementId == id);
        }

        public bool Update(Announcement model)
        {
            var dbo = Get(model.AnnouncementId);
            if (dbo == null)
                throw new KeyNotFoundException("Not Found");

            Mapper.Map(model,dbo);
            dbo.ModifiedBy = _user;
            dbo.ModifiedDate = DateTime.Now;
            return(Save());
        }

        public bool Delete(int id)
        {
            var dbo = Get(id);
            if (dbo == null)
                throw new KeyNotFoundException("Not Found");

            _context.Announcements.Remove(dbo);

            return (Save());
        }

        private bool Save()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return false;
            }

            return true;
        }
    }
}