﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using ZionPortal.Dto;
using System.Data.Entity.Validation;
using System.Data.Entity.Infrastructure;

namespace ZionPortal.Repository
{
    public class AuthRepo
    {
        private ZionEntities _context;
        private static List<PersonSecurable> _PersonSecurable;
        private static List<WebSession> webSessions;
        public AuthRepo()
        {
            _context = new ZionEntities();
            _PersonSecurable = _context.PersonSecurables.ToList();
        }
      
        public VerificationDetailsDto Authenticate(LoginInfoDto dto)
        {

            string encPwd = encryptpw(dto.password);
            var exists = _PersonSecurable.Exists(i => (i.UserName == dto.username && i.Password == encPwd));
            if (exists)
            {
                var user = _PersonSecurable.Find(i => i.UserName == dto.username && i.Password == encPwd);
                if (user != null)
                {
                   
                    VerificationDetailsDto verUser = new VerificationDetailsDto();
                    verUser.Token = generateGUID();
                    verUser.isValid = true;
                    verUser.PersonID = user.PersonID;
                    verUser.firstname = user.Person.FirstName;
                    verUser.lastname = user.Person.LastName;
                    verUser.personPhoto = user.Person.PersonPhoto;
                    int roleID= user.Person.PersonRoles.ElementAt(0).RoleID;
                    var roles = _context.Roles.ToList();
                    string role = roles.Find(i => i.RoleID == roleID).RoleName;
                    verUser.role = role;
                    verUser.employeeID = _context.Employees.Where(i => i.PersonID == user.PersonID).FirstOrDefault().EmployeeID;
                    verUser.permissions = getPermissions(Convert.ToInt32(user.PersonID));
                    bool webSessionRes =  AddWebsession(verUser);
                    return verUser;
                }
                return null;
            }
            else
                return null;
        }

        private List<string> getPermissions(int personID)
        {
            return _context.vw_PermissionDetail.Where(i => i.PersonID == personID).Select(i => i.Module).ToList();
        }

        public bool TokenValidation(string Token)
        {
                webSessions = _context.WebSessions.ToList();
                bool exists =  webSessions.Exists(i => (i.Token == Token));
                if (exists)
                {
                    var user = webSessions.Find(i => (i.Token == Token));
                    var expirationDateTime = user.ExpirationDate;
                    var dateTimeNow = DateTime.Now;
                    if (dateTimeNow < expirationDateTime)
                        return true;
                    else
                        return false;
                    
                }
                else
                    return false;
        }
        
        private bool AddWebsession(VerificationDetailsDto details)
        {
            if (details != null)
            {
                try
                {
                    WebSession session = new WebSession();
                    session.PersonID = details.PersonID;
                    session.ExpirationDate = details.ExpirationDate;
                    session.Token = details.Token;
                    session.CreatedBy = details.CreatedBy;
                    session.CreatedDate = details.CreatedDate;
                    _context.WebSessions.Add(session);
                    var result = Save();
                    return result;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    return false;
                }
            }
            return false;
        }

        private string generateGUID()
        {
            string token = Guid.NewGuid().ToString();
            return token.Substring(0, 24);
            
        }
        private static string encryptpw(string pwd)
        {
            byte[] bytes = System.Text.Encoding.Unicode.GetBytes(pwd);
            string encrytpwd = Convert.ToBase64String(bytes);
            return encrytpwd;
        }
        private static string decryptpw(string dbpwd)
        {
            byte[] bytes = Convert.FromBase64String(dbpwd);
            string decrytpwd = System.Text.Encoding.Unicode.GetString(bytes);
            return decrytpwd;
        }

        public string pwdRecovery(string username)
        {
            //first make sure the email exists else return ""
            List<Email> emails = _context.Emails.ToList();
            if (emails.Exists(i => (i.Email1 == username)))
            {
                string temp = genTempPassword(username);
                //save temp
               // _context.PersonSecurables.Find(i=>(i.))
            }
                return "";
        }
        private string genTempPassword(string username)
        {
            string longGuid = Guid.NewGuid().ToString();
            string tempPwd = longGuid.Substring(longGuid.Length - 10);
            return tempPwd;

        }
       

        private bool Save()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {

                foreach (DbEntityValidationResult item in e.EntityValidationErrors)

                {
                    DbEntityEntry entry = item.Entry;

                    string entityTypeName = entry.Entity.GetType().Name;
                    foreach (DbValidationError subItem in item.ValidationErrors)
                    { 
                        string message = string.Format("Error '{0}' occurred in {1} at {2}",

                        subItem.ErrorMessage, entityTypeName, subItem.PropertyName);

                        //  Console.WriteLine(message);
                    }

                }
                return false;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return false;
            }
            return true;
        }
    }
}