﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZionPortal.Dto;

namespace ZionPortal.App_Start
{
    public class MappingProfile: Profile 
    {
        public MappingProfile()
        {
            CreateMap<AnnouncementDto, Announcement>().ReverseMap();
            CreateMap<Announcement, Announcement>();

            CreateMap<TaskDto, Task>().ReverseMap();
            CreateMap<AddressDto, Address>().ReverseMap();
            CreateMap<EmailDto, Email>().ReverseMap();
            CreateMap<PhoneDto, Phone>().ReverseMap();

            CreateMap<CourseDto, Course>().ReverseMap();
            CreateMap<CourseScheduleDto, CourseSchedule>().ReverseMap();
            CreateMap<CourseRegistrationDto, CourseRegistration>().ReverseMap();
            CreateMap<InstructorDto, Instructor>().ReverseMap();
            CreateMap<FacilityDto, Facility>().ReverseMap();
            CreateMap<RoomDto, Room>().ReverseMap();
            CreateMap<PersonDto, Person>().ReverseMap();
            CreateMap<StudentDto, Student>().ReverseMap();
            CreateMap<PaymentDto, Payment>().ReverseMap();
            CreateMap<PaymentTypeDto, PaymentType>().ReverseMap();
            CreateMap<CategoryDto, Category>().ReverseMap();
            CreateMap<CourseSubcategoryDto, CourseSubcategory>().ReverseMap();

            //Timesheet
            CreateMap<TimeEntryDto, TimeEntry>().ReverseMap();
            CreateMap<TimeEntryTypeDto, TimeEntryType>().ReverseMap();
            CreateMap<TimeEntryApprovalDto, TimeEntryApproval>().ReverseMap();
            CreateMap<TimeEntryNoteDto, TimeEntryNote>().ReverseMap();
            CreateMap<ApprovalStatusTypeDto, ApprovalStatusType>().ReverseMap();

            //Payroll
            CreateMap<PayrollDto, Payroll>().ReverseMap();
            CreateMap<PayrollStatusTypeDto, PayrollStatusType>().ReverseMap();
        }
    }
}