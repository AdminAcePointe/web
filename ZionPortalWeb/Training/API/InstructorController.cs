﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ZionPortal.Dto;
using ZionPortal.Repository;
using AutoMapper;
using System.Collections.Generic;

namespace ZionPortal.API
{
    public class InstructorController : ApiController
    {
        private ZionEntities _context;
        private PersonRepo _personRepo;
        private InstructorRepo _repo;

        public InstructorController()
        {
            _context = new ZionEntities();
            _personRepo = new PersonRepo(_context);
            _repo = new InstructorRepo(_context);
        }

        public HttpResponseMessage Get()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.Get().Select(i => Mapper.Map<InstructorDetailsDto>(i)).ToList());
        }

       


        [Route("api/instructor/{id}")]
        public HttpResponseMessage Get(int id)
        {
            return Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<InstructorDto>(_repo.Get(id)));
        }

        public HttpResponseMessage Post(InstructorDto dto)
        {
            if (_repo.Add(dto))
                return Request.CreateResponse(HttpStatusCode.Created);

            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        [HttpGet]
        [Route("api/instructor/savesignature/{id}/{signature}")]
        public HttpResponseMessage Savesignature(int id, string signature)
        {
            if (_repo.saveSignature(id, signature))
                return Request.CreateResponse(HttpStatusCode.Created);

           return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        
        [Route("api/instructor/getsignature/{id}")]
        public HttpResponseMessage Getsignature(int id)
        {
            string signature = _repo.getSignature(id);
            if (signature != "")
             return Request.CreateResponse(HttpStatusCode.OK, signature);

           return Request.CreateResponse(HttpStatusCode.BadRequest);
        }


        [HttpPut]
        [Route("api/updateInstructor")]
        public HttpResponseMessage Put([FromBody] InstructorDetailsDto dto)
        {
            if (_repo.Update(dto))
                return Request.CreateResponse(HttpStatusCode.Accepted);

            return Request.CreateResponse(HttpStatusCode.NotModified);
        }



        public HttpResponseMessage Delete(InstructorDto dto)
        {
            if (_repo.Delete(Mapper.Map<Instructor>(dto)))
                return Request.CreateResponse(HttpStatusCode.Accepted);

            return Request.CreateResponse(HttpStatusCode.NotFound);
        }


        [HttpDelete]
        [Route("api/deleteInstructor/{id}")]
        public HttpResponseMessage Delete(int id)
        {
            if (_repo.Delete(id))
                return Request.CreateResponse(HttpStatusCode.Accepted);

            return Request.CreateResponse(HttpStatusCode.NotFound);
        }

        [Route("api/instructor/available")]
        public HttpResponseMessage GetAvailable()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetAvailable().Select(i => new InstructorDto
            {
                InstructorId = i.InstructorID,
                Person = Mapper.Map<PersonDto>(_personRepo.Get(i.PersonID)),
                CreatedBy = i.CreatedBy,
                CreatedDate = i.CreatedDate,
                ModifiedBy = i.ModifiedBy,
                ModifiedDate = i.ModifiedDate
            }).ToList());
        }

        [Route("api/instructor/available/count")]
        public HttpResponseMessage GetAvailableCount()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetAvailable().Count());
        }

        [Route("api/instructor/unavailable")]
        public HttpResponseMessage GetUnavailable()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetUnavailable().Select(i => new InstructorDto
            {
                InstructorId = i.InstructorID,
                Person = Mapper.Map<PersonDto>(_personRepo.Get(i.PersonID)),
                CreatedBy = i.CreatedBy,
                CreatedDate = i.CreatedDate,
                ModifiedBy = i.ModifiedBy,
                ModifiedDate = i.ModifiedDate
            }).ToList());
        }

        [Route("api/instructor/unavailable/count")]
        public HttpResponseMessage GetUnavailableCount()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetUnavailable().Count());
        }

        [Route("api/instructor/employee")]
        public HttpResponseMessage GetEmployee()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetEmployee().Select(i => new InstructorDto
            {
                InstructorId = i.InstructorID,
                Person = Mapper.Map<PersonDto>(_personRepo.Get(i.PersonID)),
                CreatedBy = i.CreatedBy,
                CreatedDate = i.CreatedDate,
                ModifiedBy = i.ModifiedBy,
                ModifiedDate = i.ModifiedDate
            }).ToList());
        }

        [Route("api/instructor/employee/count")]
        public HttpResponseMessage GetEmployeeCount()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetEmployee().Count());
        }

        [Route("api/instructor/count")]
        public HttpResponseMessage GetInstructorCount()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.Get().Count());
        }
    }
}