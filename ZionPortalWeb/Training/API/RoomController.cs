﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ZionPortal.Dto;
using ZionPortal.Repository;
using AutoMapper;

namespace ZionPortal.API
{
    public class RoomController : ApiController
    {
        private ZionEntities _context;
        private RoomRepo _repo;

        public RoomController()
        {
            _context = new ZionEntities();
            _repo = new RoomRepo(_context);
        }

        public HttpResponseMessage Get()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.Get().Select(i => Mapper.Map<RoomDto>(i)).ToList());
        }

        public HttpResponseMessage Get(int id)
        {
            return Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<RoomDto>(_repo.Get(id)));
        }


        public HttpResponseMessage Post(RoomDto dto)
        {
            if (_repo.Add(Mapper.Map<Room>(dto)))
                return Request.CreateResponse(HttpStatusCode.Created);

            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        public HttpResponseMessage Put(RoomDto dto)
        {
            if (_repo.Update(Mapper.Map<Room>(dto)))
                return Request.CreateResponse(HttpStatusCode.Accepted);

            return Request.CreateResponse(HttpStatusCode.NotModified);
        }

        public HttpResponseMessage Delete(RoomDto dto)
        {
            if (_repo.Delete(Mapper.Map<Room>(dto)))
                return Request.CreateResponse(HttpStatusCode.Accepted);

            return Request.CreateResponse(HttpStatusCode.NotFound);
        }

        public HttpResponseMessage Delete(int id)
        {
            if (_repo.Delete(id))
                return Request.CreateResponse(HttpStatusCode.Accepted);

            return Request.CreateResponse(HttpStatusCode.NotFound);
        }

        [Route("api/room/count")]
        public HttpResponseMessage GetCount()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.Get().Count());
        }

        [Route("api/room/available")]
        public HttpResponseMessage GetAvailable()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetAvailable().Select(r => new RoomDto
            {
                RoomId = r.RoomID,
                FacilityId = r.FacilityID,
                RoomName = r.RoomName,
                RoomCapacity = r.RoomCapacity,
                CreatedBy = r.CreatedBy,
                CreatedDate = r.CreatedDate,
                ModifiedBy = r.ModifiedBy,
                ModifiedDate = r.ModifiedDate
            }).ToList());
        }

        [Route("api/room/available/count")]
        public HttpResponseMessage GetAvailableCount()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetAvailable().Count());
        }

        [Route("api/room/unavailable")]
        public HttpResponseMessage GetUnavailable()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetUnavailable().Select(r => new RoomDto
            {
                RoomId = r.RoomID,
                FacilityId = r.FacilityID,
                RoomName = r.RoomName,
                RoomCapacity = r.RoomCapacity,
                CreatedBy = r.CreatedBy,
                CreatedDate = r.CreatedDate,
                ModifiedBy = r.ModifiedBy,
                ModifiedDate = r.ModifiedDate
            }).ToList());
        }

        [Route("api/room/unavailable/count")]
        public HttpResponseMessage GetUnavailableCount()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetUnavailable().Count());
        }
    }
}
