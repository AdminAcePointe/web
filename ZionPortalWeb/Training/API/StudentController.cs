﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ZionPortal.Dto;
using ZionPortal.Repository;
using AutoMapper;
using Newtonsoft.Json;

namespace ZionPortal.API
{
    public class StudentController : ApiController
    {
        private ZionEntities _context;
        private StudentRepo _repo;
        private StudentRepo _student;

        public StudentController()
        {
            _context = new ZionEntities();
            _repo = new StudentRepo(_context);
            _student = new StudentRepo(_context);
        }

        public HttpResponseMessage Get()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.Get());
        }

        [Route("api/Student/{id}")]
        public HttpResponseMessage Get(int id)
        {
            return Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<StudentDto>(_repo.Get(id)));
        }

        [Route("api/Studentcount")]
        public HttpResponseMessage GetstudentCount()
        {
            return Request.CreateResponse(HttpStatusCode.OK,(_repo.studentCardCount()));
        }

        [Route("api/NewStudentcount")]
        public HttpResponseMessage GetNewstudentCount()
        {
            return Request.CreateResponse(HttpStatusCode.OK, (_repo.studentCardNewCount()));
        }
        [Route("api/ExpiredCertcount")]
        public HttpResponseMessage GetExpiredCertCount()
        {
            return Request.CreateResponse(HttpStatusCode.OK, (_repo.studentCardExpiredCerts()));
        }


        //public HttpResponseMessage Post(StudentDto dto)
        //{
        //    if (_repo.Add(dto))
        //        return Request.CreateResponse(HttpStatusCode.Created);

        //    return Request.CreateResponse(HttpStatusCode.BadRequest);
        //}


        //[Route("api/UpdateStudent")]
        //public HttpResponseMessage Put(StudentDto dto)
        //{
        //    if (_repo.Update(Mapper.Map<Student>(dto)))
        //        return Request.CreateResponse(HttpStatusCode.Accepted);

        //    return Request.CreateResponse(HttpStatusCode.NotModified);
        //}

        //[Route("api/DeleteStudent")]
        //public HttpResponseMessage Delete(StudentDto dto)
        //{
        //    if (_repo.Delete(Mapper.Map<Student>(dto)))
        //        return Request.CreateResponse(HttpStatusCode.Accepted);

        //    return Request.CreateResponse(HttpStatusCode.NotFound);
        //}
        //[Route("api/DeleteStudent/{id}")]
        //public HttpResponseMessage Delete(int id)
        //{
        //    if (_repo.Delete(id))
        //        return Request.CreateResponse(HttpStatusCode.Accepted);

        //    return Request.CreateResponse(HttpStatusCode.NotFound);
        //}

       
    }
}
