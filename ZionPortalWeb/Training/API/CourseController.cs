﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ZionPortal.Dto;
using ZionPortal.Repository;
using AutoMapper;
using System.Collections.Generic;

namespace ZionPortal.API
{
    public class CourseController : ApiController
    {


        private ZionEntities _context;
        private CourseRepo _repo;
        private CategoryRepo _categoryRepo;

        public CourseController()
        {
            _context = new ZionEntities();
            _repo = new CourseRepo(_context);
            _categoryRepo = new CategoryRepo(_context);
        }


// Course
        public HttpResponseMessage Get()
        {

            return Request.CreateResponse(HttpStatusCode.OK, _repo.Get().Select(i => Mapper.Map<CourseDto>(i)).ToList());
        }

        [Route("api/Course/{id}")]
        public HttpResponseMessage Get(int id)
        {
            return Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<CourseDto>(_repo.Get(id)));
        }


        [Route("api/course/addcourse")]
        public HttpResponseMessage Post(CourseDto dto)
        {
            if (_repo.Add(Mapper.Map<Course>(dto)))
                return Request.CreateResponse(HttpStatusCode.Created);

            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        [Route("api/course/updatecourse")]
        public HttpResponseMessage Put(CourseDto dto)
        {
            if (_repo.Update(Mapper.Map<Course>(dto)))
                return Request.CreateResponse(HttpStatusCode.Accepted);

            return Request.CreateResponse(HttpStatusCode.NotModified);
        }

        public HttpResponseMessage Delete(CourseDto dto)
        {
            if (_repo.Delete(dto.CourseId))
                return Request.CreateResponse(HttpStatusCode.Accepted);

            return Request.CreateResponse(HttpStatusCode.NotFound);
        }

        [HttpDelete]
        [Route("api/course/deletecourse/{id}")]
        public HttpResponseMessage Delete(int id)
        {
            if (_repo.Delete(id))
                return Request.CreateResponse(HttpStatusCode.Accepted);

            return Request.CreateResponse(HttpStatusCode.NotFound);
        }


// Category
        [Route("api/course/category")]
        public HttpResponseMessage GetCategory()
        {
            return Request.CreateResponse(HttpStatusCode.OK,

            _categoryRepo.Get().Where( i => i.IsDeleted == false).Select(c => new CategorySubcategoryDto
            {
                CategoryID = c.CategoryID,
                CategoryName = c.CategoryName,
                CategoryDescription = c.CategoryDescription,
                CourseSubcategories = Mapper.Map<ICollection<CourseSubcategory>, ICollection<CourseSubcategoryDto>>(c.CourseSubcategories),
                CreatedBy = c.CreatedBy,
                CreatedDate = c.CreatedDate,
                ModifiedBy = c.ModifiedBy,
                ModifiedDate = c.ModifiedDate
            }));
        }

        [Route("api/course/getCategory/{id}")]
        public HttpResponseMessage GetCategoryByID(int id)
        {
            Category dbo = new Category();
            dbo = _categoryRepo.GetCategory(id);
            return Request.CreateResponse(HttpStatusCode.OK, dbo);
        }

   
        [Route("api/course/addCategory")]
        public HttpResponseMessage PostCategory(Category model)
        {
            if (model != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, _categoryRepo.AddCategory(model));
            }

            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        [Route("api/course/updateCategory")]
        public HttpResponseMessage PutCategory(Category model)
        {
            if (model != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, _categoryRepo.UpdateCategory(model));
            }

            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

  

        [Route("api/course/DeleteCategory/{id}")]
        public HttpResponseMessage DeleteCategory(int id)
        {
            var dbo = new Category();
                dbo = _categoryRepo.GetCategory(id);

            if (dbo != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, _categoryRepo.DeleteCategory(dbo));
            }

            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }



// Sub category

        [Route("api/course/Subcategory")]
        public HttpResponseMessage GetSubCategory()
        {


            return Request.CreateResponse(HttpStatusCode.OK,

            _context.CourseSubcategories.Where( i => i.IsDeleted == false & i.Category.IsDeleted == false).Select(c => new CourseSubcategoryDto
            {
                SubcategoryId = c.SubcategoryID,
                SubcategoryName = c.SubcategoryName,
                CategoryId = c.CategoryID,
                CreatedBy = c.CreatedBy,
                CreatedDate = c.CreatedDate,
                ModifiedBy = c.ModifiedBy,
                ModifiedDate = c.ModifiedDate
            }));
        }



        [Route("api/course/Subcategory/{id}")]
        public HttpResponseMessage GetSubcategory(int id)
        {


            return Request.CreateResponse(HttpStatusCode.OK,

           _context.CourseSubcategories.Where(i => i.IsDeleted == false & i.SubcategoryID == id & i.Category.IsDeleted == false).Select(c => new CourseSubcategoryDto
           {
               SubcategoryId = c.SubcategoryID,
               SubcategoryName = c.SubcategoryName,
               CategoryId = c.CategoryID,
               CreatedBy = c.CreatedBy,
               CreatedDate = c.CreatedDate,
               ModifiedBy = c.ModifiedBy,
               ModifiedDate = c.ModifiedDate
           }));
        }


        [Route("api/course/addsubcategory")]
        public HttpResponseMessage PostSubCategory(CourseSubcategory model)
        {
            if (model != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, _categoryRepo.AddSubcategory(model));
            }

            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        [Route("api/course/updateSubCategory")]
        public HttpResponseMessage PutSubCategory(CourseSubcategory model)
        {
            if (model != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, _categoryRepo.UpdateSubcategory(model));
            }

            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }



        [Route("api/course/DeleteSubCategory/{id}")]
        public HttpResponseMessage DeleteSubCategory(int id)
        {
            var dbo = new CourseSubcategory();
            dbo = _categoryRepo.GetSubcategory(id);

            if (dbo != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, _categoryRepo.DeleteSubcategory(dbo));
            }

            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        // Other stuff

        private CategoryDto catDTOcomposer(Category category)
        {
            if (category != null)
            {
                CategoryDto _category = new CategoryDto();
                _category.CategoryDescription = category.CategoryDescription;
                _category.CategoryID = category.CategoryID;
                _category.CategoryName = category.CategoryName;
                _category.CreatedDate = category.CreatedDate;
                _category.ModifiedDate = category.ModifiedDate;
                _category.CreatedBy = category.CreatedBy;
                _category.ModifiedBy = category.ModifiedBy;
                return _category;
            }
            return null;

        }
        private CategorySubcategoryDto subcatDTOcomposer(CourseSubcategory subCategory)
        {
            if (subCategory != null)
            {
                CategorySubcategoryDto _subCategory = new CategorySubcategoryDto();
                _subCategory.IsDeleted = subCategory.IsDeleted;
                _subCategory.CategoryID = subCategory.CategoryID;
                _subCategory.CategoryName = subCategory.SubcategoryName;
                _subCategory.CreatedDate = subCategory.CreatedDate;
                _subCategory.ModifiedDate = subCategory.ModifiedDate;
                _subCategory.CreatedBy = subCategory.CreatedBy;
                _subCategory.ModifiedBy = subCategory.ModifiedBy;
                return _subCategory;
            }
            return null;

        }

        [Route("api/Course/getCategoryByID/{id}")]
        public HttpResponseMessage getCategoryByID(int id)
        {
            Category cat = _categoryRepo.GetCategory(id);
            return Request.CreateResponse(HttpStatusCode.OK, catDTOcomposer(cat));
        }


        [Route("api/Course/GetSubcategoryByCategoryId/{id}")]
        public HttpResponseMessage GetSubcategoryByCategoryId(int id)
        {
            CourseSubcategory cat = _categoryRepo.GetSubcategoryByCategoryId(id);
            return Request.CreateResponse(HttpStatusCode.OK, subcatDTOcomposer(cat));
        }


      

        [HttpGet]
        [Route("api/CompletedCourseSchedule")]
        public HttpResponseMessage CompletedCourseSchedule()
        {
            return Request.CreateResponse(HttpStatusCode.OK, (_repo.CourseCardCompleted()));
        }

        [HttpGet]
        [Route("api/UpcomingCourseSchedule")]
        public HttpResponseMessage UpcomingCourseSchedule()
        {
            return Request.CreateResponse(HttpStatusCode.OK, (_repo.CourseCardUpcoming()));
        }

        [HttpGet]
        [Route("api/AvailableCourseSchedule")]
        public HttpResponseMessage AvailableCourseSchedule()
        {
            return Request.CreateResponse(HttpStatusCode.OK, (_repo.CourseCardAvailable()));
        }
    }
}
