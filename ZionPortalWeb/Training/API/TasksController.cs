﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ZionPortal.Dto;
using ZionPortal.Repository;

namespace ZionPortal.API
{
    public class TasksController : ApiController
    {
        private TaskRepo _repo;
        private ZionEntities _context;

        public TasksController()
        {
            _context = new ZionEntities();
            _repo = new TaskRepo(_context);
        }

        public HttpResponseMessage Get()
        {
            var result = _repo.Get();
            if (result == null)
                return Request.CreateResponse(HttpStatusCode.NoContent);

            return Request.CreateResponse(HttpStatusCode.OK, result.Select(Mapper.Map<TaskDto>));
        }

        public HttpResponseMessage Get(int id)
        {

            var result = _repo.Get(id);
            if (result == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, new
                {
                    code = (int)HttpStatusCode.NotFound,
                    message = $"Resource id ({id}) does not exist",
                    messageDetails = "The requested resource does not exist"
                });

            return Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<TaskDto>(result));
        }

        public IHttpActionResult Post(TaskDto dto)
        {
            var dbo = _repo.Add(Mapper.Map<Task>(dto));

            IHttpActionResult response;
            HttpResponseMessage responseMsg = new HttpResponseMessage(HttpStatusCode.RedirectMethod);
            responseMsg.Headers.Location = new Uri(Request.RequestUri + "/" + dbo.TaskId);
            response = ResponseMessage(responseMsg);
            return response;
        }

        public HttpResponseMessage Put(TaskDto dto)
        {
            var dbo = _repo.Get(dto.TaskId);

            if (dbo == null)
                return Request.CreateResponse(HttpStatusCode.BadRequest, new
                {
                    code = (int)HttpStatusCode.BadRequest,
                    message = $"Resource id ({dto.TaskId}) does not exist",
                    messageDetails = "An attempt was made to update a resource that does not exist"

                });

            if (_repo.Update(Mapper.Map<Task>(dto)))
                return Request.CreateResponse(HttpStatusCode.OK);

            return Request.CreateResponse(HttpStatusCode.InternalServerError, new
            {
                code = (int)HttpStatusCode.InternalServerError,
                message = $"Resource id ({dto.TaskId}) could not be updated",
                messageDetails = "An internal server error occured while processing a valid request to update a resource"
            });
        }

        public HttpResponseMessage Delete(int id)
        {
            var dbo = _repo.Get(id);

            if (dbo == null)
                return Request.CreateResponse(HttpStatusCode.BadRequest, new
                {
                    code = (int)HttpStatusCode.BadRequest,
                    message = $"Resource id ({id}) does not exist",
                    messageDetails = "An attempt was made to delete a resource that does not exist."

                });

            if (_repo.Delete(id))
                return Request.CreateResponse(HttpStatusCode.OK);

            return Request.CreateResponse(HttpStatusCode.InternalServerError, new
            {
                code = (int)HttpStatusCode.InternalServerError,
                message = $"Resource id ({id}) could not be deleted",
                messageDetails = "An internal server error occured while processing a valid request to delete a resource"

            });
        }
    }
}