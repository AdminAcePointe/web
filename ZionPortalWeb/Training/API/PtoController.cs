﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ZionPortal.Repository;
using ZionPortal.Dto;
using AutoMapper;
using System.Net.Http.Formatting;
using Newtonsoft.Json;

namespace ZionPortal.API
{
    public class PtoController : ApiController
    {

        private ZionEntities _context;
        private PtoRepo _repo;

        public PtoController()
        {
            _context = new ZionEntities();
            _repo = new PtoRepo(_context);
        }

        [HttpGet] 
        [Route("api/getEmpRequestByMagId/{managerId}")]
        public HttpResponseMessage getEmpRequestByMagId(int managerId)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.getEmpRequestByManager(managerId).Select(i => Mapper.Map<PtoRequestBalanceDto>(i)).ToList());
        }

        [HttpPut]
        [Route("api/ptoApproval")]
        public HttpResponseMessage PtoApprovalBySup(vw_PTORequestBalance ptoApproval)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.ptoApproval(ptoApproval));
        }
        




        [HttpGet]
        [Route("api/ptotypesBalanceByEmpId/{id}")]
        public HttpResponseMessage ptotypesBalanceByEmpId(int id)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.ptotypesBalanceByEmpId(id).Select(i => Mapper.Map<PtoBalanceDto>(i)).ToList());
        }

        [HttpGet]
        [Route("api/ptoRequestByEmpId/{empId}")]
        public HttpResponseMessage GetPtoRequestByEmpId(int empId)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.getPtoRequestByEmpId(empId));
        }


        [HttpGet]
        [Route("api/ptoBalanceByType/{typeid}/{empId}")]
        public HttpResponseMessage GetBalanceByType(int typeId, int empId)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.ptoBalanceByTypeId(typeId,empId));
        }

        [HttpGet]
        [Route("api/ptoBalanceByEmployeeID/{empId}")]
        public HttpResponseMessage GetBalanceByEmployeeID(int empId)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.ptoBalanceWithTypeByEmpId(empId));
        }


        [HttpPost]
        [Route("api/RequestNewPto")]
        public HttpResponseMessage RequestNewPto(FormDataCollection formData)
        {
            if (formData != null)
            {
                var r = formData.GetValues("values")[0];
                dynamic data = JsonConvert.DeserializeObject(r);
                string s = data.startDate;
                pto_note_dto dto = new pto_note_dto();
                dto.EmployeeID = data.employeeID;
                dto.PTOTypeID = data.pTOtypeId;
                dto.PTOStartDate = data.startDate;
                dto.PTOEndDate = data.endDate;
                dto.PTOHours = data.ptoHrRequested;
                dto.PTONotes = data.notes;
               

                _repo.PtoRequest(dto);
                return Request.CreateResponse(HttpStatusCode.Created);
            }
            return Request.CreateResponse(HttpStatusCode.Created);
            //return Request.CreateResponse(HttpStatusCode.OK, _repo.PtoRequest(ptoRequest));
        }


        [HttpPut]
        [Route("api/UpdateRequestPto")]
        public HttpResponseMessage UpdateRequestPto(FormDataCollection formData)
        {
            if (formData != null)
            {
                var r = formData.GetValues("values")[0];
                dynamic data = JsonConvert.DeserializeObject(r);
                string s = data.startDate;
                pto_note_dto dto = new pto_note_dto();
                dto.EmployeeID = data.employeeID;
                dto.PTOTypeID = data.pTOtypeId;
                dto.PTOStartDate = data.startDate;
                dto.PTOEndDate = data.endDate;
                dto.PTOHours = data.ptoHrRequested;
                dto.PTONotes = data.notes;
                dto.PTORequestID = data.ptoRequestId;
                dto.PTORequestNotesID = data.ptoRequestNotesID;

                if (_repo.updatePtoRequest(dto))
                {
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
                return Request.CreateResponse(HttpStatusCode.OK);
            }
           
            return Request.CreateResponse(HttpStatusCode.Unauthorized,"Cannot modify PTO. It has been approved");
        }



    }
}
