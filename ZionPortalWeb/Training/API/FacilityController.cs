﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ZionPortal.Dto;
using ZionPortal.Repository;
using AutoMapper;

namespace ZionPortal.API
{
    public class FacilityController : ApiController
    {
        private ZionEntities _context;
        private FacilityRepo _repo;

        public FacilityController()
        {
            _context = new ZionEntities();
            _repo = new FacilityRepo(_context);
        }
        
        public HttpResponseMessage Get()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.Get().Select(i => Mapper.Map<FacilityDto>(i)).ToList());
        }

        public HttpResponseMessage Get(int id)
        {
            return Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<FacilityDto>(_repo.Get(id)));
        }

        public HttpResponseMessage Post(FacilityDto dto)
        {
            if (_repo.Add(Mapper.Map<Facility>(dto)))
                return Request.CreateResponse(HttpStatusCode.Created);

            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        public HttpResponseMessage Put(FacilityDto dto)
        {
            if (_repo.Update(Mapper.Map<Facility>(dto)))
                return Request.CreateResponse(HttpStatusCode.Accepted);

            return Request.CreateResponse(HttpStatusCode.NotModified);
        }

        public HttpResponseMessage Delete(FacilityDto dto)
        {
            if (_repo.Delete(Mapper.Map<Facility>(dto)))
                return Request.CreateResponse(HttpStatusCode.Accepted);

            return Request.CreateResponse(HttpStatusCode.NotFound);
        }

        public HttpResponseMessage Delete(int id)
        {
            if (_repo.Delete(id))
                return Request.CreateResponse(HttpStatusCode.Accepted);

            return Request.CreateResponse(HttpStatusCode.NotFound);
        }

        [Route("api/facility/count")]
        public HttpResponseMessage GetFacilityCount()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.facilitiesCount());
        }

        [Route("api/facility/roomcount")]
        public HttpResponseMessage GetRoomCount()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.roomCount());
        }
    }
}
