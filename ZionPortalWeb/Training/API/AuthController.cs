﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ZionPortal.Dto;
using ZionPortal.Repository;


namespace ZionPortal.API
{
    public class AuthController : ApiController
    {
        public VerificationDetailsDto authenticateCredentials(LoginInfoDto dto)
        {
            if (dto != null)
            {
                AuthRepo repo = new AuthRepo();
                VerificationDetailsDto repoResponse = repo.Authenticate(dto);
                    return repoResponse;
            }
                return null;
           
        }

   
     

        public string pwdRecovery(string username)
        {
            AuthRepo repo = new AuthRepo();
           return  repo.pwdRecovery(username);
        }


        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}