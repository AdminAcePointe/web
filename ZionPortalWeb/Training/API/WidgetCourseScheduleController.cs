﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;
using ZionPortal.Dto;
using ZionPortal.Repository;
using Newtonsoft.Json;

namespace ZionPortal.API
{
    public class WidgetCourseScheduleController : ApiController
    {
        private WidgetCourseSchedulerRepo _repo;
        private CourseScheduleRepo _courseSchrepo;
        private ZionEntities _context = new ZionEntities();

        public WidgetCourseScheduleController()
        {
            _repo = new WidgetCourseSchedulerRepo();
            _courseSchrepo = new CourseScheduleRepo(_context);
        }

        public HttpResponseMessage Get()
        {
            var u = _repo.Get();
            List<testing> p = new List<testing>();
            testing y;
            for (int w = 0; w < u.Count; w++)
            {
                y = new testing();
                y.AllDay = u.ElementAt(w).AllDay;
                y.CourseId = u.ElementAt(w).CourseId;
                y.CoursePrice = Convert.ToDecimal(u.ElementAt(w).CoursePrice);
                y.CourseScheduleId = u.ElementAt(w).CourseScheduleId;
                y.Description = u.ElementAt(w).Description;
                y.EndDate = u.ElementAt(w).EndDate;
                y.EndTime = u.ElementAt(w).EndTime.ToString();
                y.FacilityId = u.ElementAt(w).FacilityId;
                y.FacilityName = u.ElementAt(w).FacilityName;
                y.InstructorId = u.ElementAt(w).InstructorId;
                y.InstructorName = u.ElementAt(w).InstructorName;
                y.Isdeleted = Convert.ToBoolean(u.ElementAt(w).Isdeleted);
                y.Text = u.ElementAt(w).Text;
                y.Priority = u.ElementAt(w).Priority;
                y.RoomId = u.ElementAt(w).RoomId;
                y.RoomName = u.ElementAt(w).RoomName;
                y.ScheduleDate = u.ElementAt(w).ScheduleDate.ToString();
                y.StartTime = u.ElementAt(w).StartTime.ToString();
                y.StartDate = u.ElementAt(w).StartDate.ToString();
                p.Add(y);

            }
           
            //  return Request.CreateResponse(HttpStatusCode.OK, _repo.Get());
            return Request.CreateResponse(HttpStatusCode.OK, p);
        }

        public HttpResponseMessage Get(int id)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.Get(id));
        }

      
        [Route("api/WidgetCourseSchedule/InsertSchedule")]
        public HttpResponseMessage Post(FormDataCollection formData)
        {
            if (formData != null)
            {
                var r = formData.GetValues("values")[0];
                dynamic data = JsonConvert.DeserializeObject(r);
                string s = data.startDate;
                WidgetCourseSchedulerDto dto = new WidgetCourseSchedulerDto();
                dto.CourseId = data.courseId;
                dto.StartDate = data.startDate;
                dto.EndDate = data.endDate;
                dto.FacilityId = data.facilityId;
                dto.RoomId = data.roomId;
                dto.Isdeleted = false;
                dto.InstructorId = data.instructorId;
               
                _repo.Add(dto);
                return Request.CreateResponse(HttpStatusCode.Created);
            }
           return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        [HttpPut]
        [Route("api/WidgetCourseSchedule/UpdateSchedule")]
        public HttpResponseMessage Put( FormDataCollection formData)
        {
            
            if (formData != null)
            {
                var r = formData.GetValues("values")[0];
                dynamic data = JsonConvert.DeserializeObject(r);
                string s = data.startDate;
                DateTime parsedDate = DateTime.Parse(s);
                var pp =   parsedDate.TimeOfDay;

                CourseScheduleDto dto = new CourseScheduleDto();
                string s2 = data.endDate;
                DateTime parsedDate2 = DateTime.Parse(s2);
                var pp2 = parsedDate2.TimeOfDay;
                var pss = parsedDate2.TimeOfDay.ToString("hh\\:mm\\:ss\\.ffffff");

               var timee =  TimeZone.CurrentTimeZone.ToLocalTime(parsedDate2);
                dto.ScheduleDate = parsedDate;
                dto.StartTime = pp;
                dto.EndTime = pp2;





                dto.CourseID = data.courseId;
               // dto.StartTime = data.startTime;
               // dto.EndTime = data.endTime;
               // dto.ScheduleDate = data.startDate;
                dto.RoomID = data.roomId;
                dto.CourseScheduleId = data.courseScheduleId;
                dto.CourseScheduleID = data.courseScheduleId;
                dto.InstructorID = data.instructorId;
                dto.Status = "Active";
                dto.Student = null;
                dto.Priority = null;
                dto.Isdeleted = data.isDeleted;
                dto.Room = null;
                dto.Course = null;
                dto.CreatedBy = data.CreatedBy;
                dto.CreatedDate = data.CreatedDate;
            
               if(_courseSchrepo.Update(dto))
                    return Request.CreateResponse(HttpStatusCode.Accepted);
            }
           
            return Request.CreateResponse(HttpStatusCode.NotModified);
        }

        public HttpResponseMessage Delete(WidgetCourseSchedulerDto dto)
        {
            if (_repo.Delete(dto.CourseScheduleId))
                return Request.CreateResponse(HttpStatusCode.Accepted);

            return Request.CreateResponse(HttpStatusCode.NotFound);
        }


        [HttpDelete]
        [Route("api/WidgetCourseSchedule/DeleteSchedule/{id}")]
        public HttpResponseMessage DeleteSchedule(int id)
        {
            if (_repo.Delete(id))
                return Request.CreateResponse(HttpStatusCode.Accepted);

            return Request.CreateResponse(HttpStatusCode.NotFound);
        }

        public HttpResponseMessage Delete(int id)
        {
            if (_repo.Delete(id))
                return Request.CreateResponse(HttpStatusCode.Accepted);

            return Request.CreateResponse(HttpStatusCode.NotFound);
        }
        
        [HttpGet]
        [Route("api/WidgetCourseSchedule/Resource/Room")]
        public HttpResponseMessage Room()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetRoom());
        }

        [HttpGet]
        [Route("api/WidgetCourseSchedule/Resource/Facility")]
        public HttpResponseMessage Facility()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetFacility());
        }

        [HttpGet]
        [Route("api/WidgetCourseSchedule/Resource/Instructor")]
        public HttpResponseMessage Instructor()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetInstructor());
        }

        [HttpGet]
        [Route("api/WidgetCourseSchedule/Resource/Course")]
        public HttpResponseMessage Course()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetCourse());
        }
    }
}
