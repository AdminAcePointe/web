﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ZionPortal.Dto;
using ZionPortal.Repository;
using AutoMapper;
using Newtonsoft.Json;


namespace ZionPortal.API
{
    public class MemberServicesController : ApiController
    {
        private ZionEntities _context;
        private MemberServicesRepo _repo;
        public MemberServicesController()
        {
            _context = new ZionEntities();
            _repo = new MemberServicesRepo(_context);
        }

        [Route("api/MemberServices/GetMemberService")]
        public HttpResponseMessage GetMemberServices()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.getMemberService());
        }

        [Route("api/MemberServices/GetServices")]
        public HttpResponseMessage Get()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.getServices().Select(i => i.ServiceTypeWithService).ToList());
        }

        [Route("api/MemberServices/GetServicesWithID")]
        public HttpResponseMessage GetServicesWithID()
        {

            return Request.CreateResponse(HttpStatusCode.OK, _repo.getServices());
        }

        [Route("api/MemberServices/GetServiceType")]
        public HttpResponseMessage GetServiceType()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.getServicesType().Select(i => i.ServiceType).ToList());
        }

        [Route("api/MemberServices/GetServicesByServiceTypeID/{id}")]
        public HttpResponseMessage GetServicesBy(int id)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.getServicesByServiceTypeId(id).Select(i => i.MemberServiceName).ToList());
        }

        [Route("api/MemberServices/MemberAssignedToEmployee/{id}")]
        public HttpResponseMessage GetMemebersByEmployeeId(int id)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.getMembersAssignedToEmployee(id).Select(i=>i.memberFullNameWithService).ToList());
        }




    }
}
