﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ZionPortal.Dto;
using ZionPortal.Repository;

namespace ZionPortal.API
{
    public class LocationController : ApiController
    {
        LocationRepo locRepo = new LocationRepo();

        [Route("api/Location/allLocations")]
        public HttpResponseMessage getLocations()
        {

            return Request.CreateResponse(HttpStatusCode.OK, locRepo.GetLocations());
        }

        [Route("api/Location/LocationByCityID/{cityID}")]
        public HttpResponseMessage getLocationByCityID(int cityID)
        {
            return Request.CreateResponse(HttpStatusCode.OK, locRepo.GetLocationByCityID(cityID));
        }

        [Route("api/Location/LocationByStateID/{stateID}")]
        public HttpResponseMessage getLocationByStateID(int stateID)
        {
            return Request.CreateResponse(HttpStatusCode.OK, locRepo.getDistinctState2(stateID));
        }

    }
}
