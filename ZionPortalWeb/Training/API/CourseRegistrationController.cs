﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ZionPortal.Dto;
using ZionPortal.Repository;

namespace ZionPortal.API
{
    public class CourseRegistrationController : ApiController
    {
        private ZionEntities _context;
        private CourseRegistrationRepo _repo;

        public CourseRegistrationController()
        {
            _context = new ZionEntities();
            _repo = new CourseRegistrationRepo(_context);
        }

        public HttpResponseMessage Get()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.Get().Select(i => Mapper.Map<CourseRegistrationDto>(i)).ToList());
        }

        public HttpResponseMessage Get(int id)
        {
            return Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<CourseRegistrationDto>(_repo.Get(id)));
        }


        [Route("api/courseregistration/GetByStudentID/{id}")]
        public HttpResponseMessage GetByStudentID(int id)
        {
            return Request.CreateResponse(HttpStatusCode.OK,(_repo.GetByStudentID(id)).Select(i => Mapper.Map<CourseRegistrationDto>(i)).ToList());
        }

        [HttpGet]
        [Route("api/courseregistration/UnregisterStudent/{id}")]
        public HttpResponseMessage UnregisterStudent(int id)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.UnregisterStudent(id));
        }


        [Route("api/courseregistration/GetByCourseScheduleID/{id}")]
        public HttpResponseMessage GetByCourseScheduleID(int id)
        {
            return Request.CreateResponse(HttpStatusCode.OK, (_repo.GetByCourseScheduleID(id)).Select(i => Mapper.Map<CourseRegistrationDto>(i)).ToList());
        }

        public HttpResponseMessage Post(CourseRegistrationDto dto)
        {
            if (_repo.Add(Mapper.Map<CourseRegistration>(dto)))
                return Request.CreateResponse(HttpStatusCode.Created);

            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        public HttpResponseMessage Put(CourseRegistrationDto dto)
        {
            if (_repo.Update(Mapper.Map<CourseRegistration>(dto)))
                return Request.CreateResponse(HttpStatusCode.Accepted);

            return Request.CreateResponse(HttpStatusCode.NotModified);
        }

        public HttpResponseMessage Delete(CourseRegistrationDto dto)
        {
            if (_repo.Delete(Mapper.Map<CourseRegistration>(dto)))
                return Request.CreateResponse(HttpStatusCode.Accepted);

            return Request.CreateResponse(HttpStatusCode.NotFound);
        }

        public HttpResponseMessage Delete(int id)
        {
            if (_repo.Delete(id))
                return Request.CreateResponse(HttpStatusCode.Accepted);

            return Request.CreateResponse(HttpStatusCode.NotFound);
        }

        [HttpGet]
        [Route("api/courseregistration/registrationByAdmin/{studentId}/{courseScheduleId}")]
        public HttpResponseMessage registerStudentByAdmin(int studentId , int courseScheduleId)
        {
           if(_repo.adminRegistersStudent(studentId, courseScheduleId))
                return Request.CreateResponse(HttpStatusCode.Accepted);

           return Request.CreateResponse(HttpStatusCode.NotFound);
        }


        [HttpPut]
        [Route("api/courseregistration/upsertGrade/{studentid}/{CourseRegistrationID}/{Score}")]
        public HttpResponseMessage upsertGrade(int studentid , int CourseRegistrationID , int Score )
        {

          // int studentid = 1; int CourseRegistrationID = 1003;  int Score = 78;
            if (_repo.upsertGrade(studentid, CourseRegistrationID, Score))
                return Request.CreateResponse(HttpStatusCode.Accepted);

            return Request.CreateResponse(HttpStatusCode.NotModified);
        }


        [HttpGet]
        [Route("api/courseregistration/generateCert/{id}")]
        public HttpResponseMessage generateCert(int id)
        {
            string baseUrl = Url.Request.RequestUri.GetComponents(UriComponents.SchemeAndServer, UriFormat.Unescaped);


            if (_repo.generateCert(id, baseUrl))
                return Request.CreateResponse(HttpStatusCode.Accepted,1);

            return Request.CreateResponse(HttpStatusCode.Accepted, 0);
        }
    }
}
