﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ZionPortal.Dto;
using ZionPortal.Repository;
using AutoMapper;

namespace ZionPortal.API
{
    public class MemberController : ApiController
    {
        private ZionEntities _context;
        private MemberRepo _repo;
        public MemberController()
        {
            _context = new ZionEntities();
            _repo = new MemberRepo(_context);
        }


        [Route("api/Member/GetMembersWithServices")]
        public HttpResponseMessage GetMembersWithServices()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetMembersWithServices().Select(i=>i.memberFullNameWithService).ToList());
        }
        

        [Route("api/Member/GetAllMembers")]
        public HttpResponseMessage GetMembers()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetAllMembers());
        }

        [Route("api/Member/memberDetails/{memberID}")]
        public HttpResponseMessage GetMemberDetails(int memberID)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetMemberInfo(memberID));
        }

        [Route("api/Member/memberServices/{memberID}")]
        public HttpResponseMessage GetMemberServices(int memberID)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetMemberServices(memberID));
        }

        [Route("api/Member/memberEmergencyList/{memberID}")]
        public HttpResponseMessage GetmemberEmergencyList(int memberID)
        {
            if(memberID > 0)
            {
                return Request.CreateResponse(HttpStatusCode.OK, _repo.GetEmergencyContacts(memberID).Select(i => Mapper.Map<PersonEmergencyContactDto>(i)).ToList());
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK);
            }
        }

        [Route("api/MemberStatusList")]
        public HttpResponseMessage GetMemberStatuses()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetMemberStatuses());
        }

        [HttpPost]
        [Route("api/Member/AddNewMember/{memberID}")]
        public HttpResponseMessage AddMember([FromBody] MemberUIDetailsDTO ddata , int  memberID)
        {
          
            return Request.CreateResponse(HttpStatusCode.OK, _repo.AddNewMember(ddata, memberID ,ddata.UpdatedProfilePic));
        }

    }
}
