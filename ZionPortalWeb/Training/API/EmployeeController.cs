﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ZionPortal.Dto;
using ZionPortal.Repository;
using AutoMapper;

namespace ZionPortal.API
{
    public class EmployeeController : ApiController
    {
        private ZionEntities _context;
        private EmployeeRepo _repo;

        public EmployeeController()
        {
            _context = new ZionEntities();
            _repo = new EmployeeRepo(_context);
        }


        [Route("api/Employee/GetSEmployeeServices/{id}")]
        public HttpResponseMessage GetEmployeeServices(int id)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.getServicesByEmployeeID(id));
        }

        [Route("api/Employee/EmployeeServicesWithID/{id}")]
        public HttpResponseMessage GetEmployeeServicesWithID(int id)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.getServicesWithIDByEmployeeID(id).Select(i => Mapper.Map<EmployeeServiceDto>(i)).ToList());
        }

        [HttpGet]
        [Route("api/GetServiceEmployeePayRates/{employeeid}")]
        public HttpResponseMessage GetEmployeePayRates(int employeeid)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.getEmployeePayRates(employeeid));
        }

        //[Route("api/UpsertEmployeePayRates/{model}/{services}")]
        //public HttpResponseMessage UpsertEmployeePayRates(vw_PersonDetail model, string payrates)
        //{
        //    return Request.CreateResponse(HttpStatusCode.OK, _repo.UpsertEmployeePayRates(model, payrates));
        //}

        [HttpGet]
        [Route("api/GetAllEmployeeFullName")]
        public HttpResponseMessage GetAllEmployeeFullName()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetAllEmployeeFullName());
        }



    }
}
