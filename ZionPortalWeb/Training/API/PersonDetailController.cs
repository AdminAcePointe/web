﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ZionPortal.Dto;
using ZionPortal.Repository;
using AutoMapper;

namespace ZionPortal.API
{
    public class PersonDetailController : ApiController
    {

        private ZionEntities _context;
        private PersonDetailRepo _repo;

        public PersonDetailController()
        {
            _context = new ZionEntities();
            _repo = new PersonDetailRepo(_context);
        }

        [HttpGet]
        [Route("api/GetEmployeeServiceType/{employeeID}")]
        public HttpResponseMessage GetEmployeeServiceType(int employeeID)
        {

            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetEmployeeServiceInfo(employeeID).Select(i => i.ServiceType).ToList());
        }

        [HttpGet]
        [Route("api/GetEmployeeService/{employeeID}")]
        public HttpResponseMessage GetEmployeeService(int employeeID)
        {

            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetEmployeeServiceInfo(employeeID).Select(i=>i.ServiceName).ToList());
        }

        [HttpGet]
        [Route("api/personDetail/{personId}")]
        public HttpResponseMessage GetPersonDetailById(int personId)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetPersonInfo(personId));
        }


        [HttpPost]
        [Route("api/postPersonDetail/{employeeID}/{services}")]
        public HttpResponseMessage PostPersonDetailById([FromBody] EmployeeUIDetailsDTO ddata, int employeeID , string services)
        {
            bool res = false;
            if(_repo.PostPersonInfo(ddata.ddata, employeeID, services,ddata.UpdatedProfilePic))
                {
                res = _repo.postEmployeePayRates(ddata.vw_PayPerServiceDetails,ddata.ddata.PersonID);
            }
            return Request.CreateResponse(HttpStatusCode.OK, res);
        }


        [HttpGet]
        [Route("api/personDetailbymanager/{managerID}")]
        public HttpResponseMessage GetPersonDetailByManagergID(int managerID)
        {

            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetPersonInfoByManagerID(managerID));
        }

        [HttpGet]
        [Route("api/roleList/")]
        public HttpResponseMessage GetRoles()
        {

            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetRoles().Select(i =>i.RoleName).ToList());
        }


        [HttpGet]
        [Route("api/shiftList/")]
        public HttpResponseMessage GetShifts()
        {

            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetShifts().Select(i => i.ShiftName).ToList());
        }


        [HttpGet]
        [Route("api/groupHomeList/")]
        public HttpResponseMessage GetGroupHomes()
        {

            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetGroupHomes().Select(i=>i.FacilityName).ToList());
        }


        [HttpGet]
        [Route("api/stateList/")]
        public HttpResponseMessage GetStates()
        {

            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetStates());
        }

        [HttpGet]
        [Route("api/genderList/")]
        public HttpResponseMessage GetGender()
        {

            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetGender());
        }

        [HttpGet]
        [Route("api/relationshipList/")]
        public HttpResponseMessage GetRelationship()
        {

            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetRelationships());
        }

        [HttpGet]
        [Route("api/maritalStatusList/")]
        public HttpResponseMessage GetMaritalStatus()
        {

            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetMaritalStatus());
        }

        [HttpGet]
        [Route("api/managerList/")]
        public HttpResponseMessage GetManagers()
        {

            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetManagers().Select(i => i.ManagerFullName).ToList());
        }



    }
}
