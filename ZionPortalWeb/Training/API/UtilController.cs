﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using ZionPortal.Dto;
using ZionPortal.Repository;

namespace ZionPortal.API
{
    public class UtilController : ApiController
    {


        [Route("api/util/SaveProfilePhoto")]
        public HttpResponseMessage SaveProfilePhoto()
        {
            HttpResponseMessage result = null;
            var httpRequest = HttpContext.Current.Request;
            if (httpRequest.Files.Count > 0)
            {
                var docfiles = new List<string>();
                var finalpath = "";
                foreach (string file in httpRequest.Files)
                {
                    var postedFile = httpRequest.Files[file];
                    var filePath = HttpContext.Current.Server.MapPath("~/img/ProfilePhoto/" + postedFile.FileName);
                    postedFile.SaveAs(filePath);
                    docfiles.Add(filePath);
                    finalpath = filePath;
                }
                result = Request.CreateResponse(HttpStatusCode.Created, finalpath);
            }
            else
            {
                result = Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            return result;
        }
    }
}