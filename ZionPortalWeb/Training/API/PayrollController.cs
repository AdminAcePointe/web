﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ZionPortal.Dto;
using ZionPortal.Repository;
using AutoMapper;

namespace ZionPortal.API
{
    public class PayrollController : ApiController
    {
        private ZionEntities _context;
        private PayrollRepo _repo;

        public PayrollController()
        {
            _context = new ZionEntities();
            _repo = new PayrollRepo(_context);
        }

        public HttpResponseMessage Get()
        {

            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetPayrolls().Select(i => Mapper.Map<PayrollDto>(i)).ToList());
        }

        [Route("api/payroll/{id}")]
        public HttpResponseMessage Get(int id)
        {
            return Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<PayrollDto>(_repo.GetPayrollById(id)));
        }

        [Route("api/payrolldetails/")]
        public HttpResponseMessage GetPayrollDetails()
        {
            return Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<PayrollDto>(_repo.GetPayrollDetails()));
        }

        [Route("api/payroll/add")]
        public HttpResponseMessage Post(PayrollDto dto)
        {
            if (_repo.AddPayroll(Mapper.Map<Payroll>(dto)))
                return Request.CreateResponse(HttpStatusCode.Created);

            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        [Route("api/payroll/update")]
        public HttpResponseMessage Put(CourseDto dto)
        {
            if (_repo.UpdatePayroll(Mapper.Map<Payroll>(dto)))
                return Request.CreateResponse(HttpStatusCode.Accepted);

            return Request.CreateResponse(HttpStatusCode.NotModified);
        }

        public HttpResponseMessage Delete(PayrollDto dto)
        {
            if (_repo.DeletePayroll(Mapper.Map<Payroll>(dto)))
                return Request.CreateResponse(HttpStatusCode.Accepted);

            return Request.CreateResponse(HttpStatusCode.NotFound);
        }

        [HttpDelete]
        [Route("api/payroll/delete/{id}")]
        public HttpResponseMessage Delete(int id)
        {
            if (_repo.DeletePayrollById(id))
                return Request.CreateResponse(HttpStatusCode.Accepted);

            return Request.CreateResponse(HttpStatusCode.NotFound);
        }
    }
}
