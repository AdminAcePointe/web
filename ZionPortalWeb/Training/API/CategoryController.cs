﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ZionPortal.Dto;
using ZionPortal.Repository;
using AutoMapper;


namespace ZionPortal.API
{
    public class CategoryController : ApiController
    {
        private ZionEntities _context;
        private CategoryRepo _repo;

        public CategoryController()
        {
            _context = new ZionEntities();
            _repo = new CategoryRepo(_context);
        }

        [HttpGet]
        [Route("api/category/{id}")]
        public HttpResponseMessage GetCategory(int id)
        {
            return Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<CategoryDto>(_repo.GetCategory(id)));
        }

        [HttpGet]
        [Route("api/subcategory/{id}")]
        public HttpResponseMessage GetSubcategory(int id)
        {
            return Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<CourseSubcategoryDto>(_repo.GetSubcategory(id)));
        }

        [HttpPost]
        [Route("api/category")]
        public HttpResponseMessage PostCategory(CategoryDto dto)
        {
            if (_repo.AddCategory(Mapper.Map<Category>(dto)))
                return Request.CreateResponse(HttpStatusCode.Created);

            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        [HttpPut]
        [Route("api/category")]
        public HttpResponseMessage PutCategory(CategoryDto dto)
        {
            if (_repo.UpdateCategory(Mapper.Map<Category>(dto)))
                return Request.CreateResponse(HttpStatusCode.Accepted);

            return Request.CreateResponse(HttpStatusCode.NotModified);
        }

        [HttpPost]
        [Route("api/subcategory")]
        public HttpResponseMessage PostSubcategory(CourseSubcategoryDto dto)
        {
            if (_repo.AddSubcategory(Mapper.Map<CourseSubcategory>(dto)))
                return Request.CreateResponse(HttpStatusCode.Created);

            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        [HttpPut]
        [Route("api/subcategory")]
        public HttpResponseMessage PutSubcategory(CourseSubcategoryDto dto)
        {
            if (_repo.UpdateSubcategory(Mapper.Map<CourseSubcategory>(dto)))
                return Request.CreateResponse(HttpStatusCode.Accepted);

            return Request.CreateResponse(HttpStatusCode.NotModified);
        }

        [HttpDelete]
        [Route("api/category/{id}")]
        public HttpResponseMessage DeleteCategory(int id)
        {
            if (_repo.DeleteCategory(id))
                return Request.CreateResponse(HttpStatusCode.Accepted);

            return Request.CreateResponse(HttpStatusCode.NotFound);
        }

        [HttpDelete]
        [Route("api/subcategory/{id}")]
        public HttpResponseMessage DeleteSubcategory(int id)
        {
            if (_repo.DeleteSubcategory(id))
                return Request.CreateResponse(HttpStatusCode.Accepted);

            return Request.CreateResponse(HttpStatusCode.NotFound);
        }

        public HttpResponseMessage DeleteCategory(CategoryDto dto)
        {
            if (_repo.DeleteCategory(dto.CategoryID))
                return Request.CreateResponse(HttpStatusCode.Accepted);

            return Request.CreateResponse(HttpStatusCode.NotFound);
        }

        public HttpResponseMessage DeleteSubcategory(CourseSubcategoryDto dto)
        {
            if (_repo.DeleteSubcategory(dto.SubcategoryId))
                return Request.CreateResponse(HttpStatusCode.Accepted);

            return Request.CreateResponse(HttpStatusCode.NotFound);
        }
    }
}
