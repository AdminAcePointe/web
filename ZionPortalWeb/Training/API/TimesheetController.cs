﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ZionPortal.Dto;
using ZionPortal.Repository;
using AutoMapper;

namespace ZionPortal.API
{
    public class TimesheetController : ApiController
    {
        private ZionEntities _context;
        private TimesheetRepo _repo;
        private EmployeeRepo _emp_repo;

        public TimesheetController()
        {
            _context = new ZionEntities();
            _repo = new TimesheetRepo(_context);
            _emp_repo = new EmployeeRepo(_context);
        }


        #region TimeEntry
        [HttpGet]
        [Route("api/timeentry")]
        public HttpResponseMessage GetTimeEntries()
        {

            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetTimeEntries().Select(i => Mapper.Map<TimeEntryDto>(i)).ToList());
        }

        [HttpGet]
        [Route("api/timeentrywithapproval")]
        public HttpResponseMessage GetTimeEntriesWithApproval()
        {

            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetTimeEntriesWithApproval().Select(i => Mapper.Map<TimeEntryDto>(i)).ToList());
        }

        [HttpGet]
        [Route("api/timeentrywithpayroll")]
        public HttpResponseMessage GetTimeEntriesWithPayroll()
        {

            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetTimeEntriesWithPayroll().Select(i => Mapper.Map<TimeEntryDto>(i)).ToList());
        }

        [HttpGet]
        [Route("api/timeentry/{id}")]
        public HttpResponseMessage GetTimeEntry(int id)
        {
            return Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<TimeEntryDto>(_repo.GetTimeEntryById(id)));
        }

        [HttpGet]
        [Route("api/timeentry/employee/{id}")]
        public HttpResponseMessage GetTimeEntryByEmployeeId(int id)
        {
            return Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<TimeEntryDto>(_repo.GetTimeEntryByEmployeeId(id)));
        }

        [HttpGet]
        [Route("api/timeentry/employeeLastService/{id}/{serviceID}")]
        public HttpResponseMessage GetLastServiceByEmployeeId(int id, int serviceID)
        {

            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetLastServiceByEmployeeId(id, serviceID).Select(i => Mapper.Map<EmployeeServiceDto>(i)).ToList());
        }

        [HttpGet]
        [Route("api/timeentry/allemployee/{id}")]
        public HttpResponseMessage GetAllTimeEntryByEmployeeId(int id)
        {
            return Request.CreateResponse(HttpStatusCode.OK,_repo.GetAllTimeEntryByEmployeeId(id).Select(i => Mapper.Map<TimeEntryDto>(i)).ToList());
        }



        [HttpGet]
        [Route("api/time/employee/{id}")]
        public HttpResponseMessage vw_TimeSheetByID(int id)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.vw_TimeSheetByID(id).Select(i => Mapper.Map<vw_TimeSheetDto>(i)).ToList());
        }

        [HttpGet]
        [Route("api/time/employeesPerManager/{id}")]
        public HttpResponseMessage vw_TimeSheetByManager(int id)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.vw_TimeSheetByManagerID(id).Select(i => Mapper.Map<vw_TimeSheetDto>(i)).ToList());
        }

        [HttpGet]
        [Route("api/time/employeesTimeForBilling")]
        public HttpResponseMessage vw_TimeSheetForBilling()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.vw_TimeSheetForBilling().Select(i => Mapper.Map<vw_TimeSheetDto>(i)).ToList());
        }



        [HttpGet]
        [Route("api/time/employeetimeentrydetail/{employeeid}/{date}")]
        public HttpResponseMessage vw_EmployeeTimesheetDetailByDate_EmployeeID(int employeeid,DateTime date)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.vw_EmployeeTimesheetDetailByDate_EmployeeID(employeeid, date).Select(i => Mapper.Map<vw_EmployeeTimesheetDetailDto>(i)).ToList());
        }

        [HttpPost]
        [Route("api/timeentry/add")]
        public Int32 PostTimeEntry([FromBody] TimeEntryDto dto)
        {

            return _repo.AddTimeEntry(Mapper.Map<TimeEntry>(dto));
        }

        [HttpPut]
        [Route("api/timeentry/update")]
        public HttpResponseMessage Put(TimeEntryDto dto)
        {
            if (_repo.UpdateTimeEntry(Mapper.Map<TimeEntry>(dto)))
                return Request.CreateResponse(HttpStatusCode.Accepted);

            return Request.CreateResponse(HttpStatusCode.NotModified);
        }

        public HttpResponseMessage DeleteTimeEntry(TimeEntryDto dto)
        {
            if (_repo.DeleteTimeEntry(Mapper.Map<TimeEntry>(dto)))
                return Request.CreateResponse(HttpStatusCode.Accepted);

            return Request.CreateResponse(HttpStatusCode.NotFound);
        }

        [HttpDelete]
        [Route("api/timeentry/delete/{id}")]
        public HttpResponseMessage DeleteTimeEntry(int id)
        {
            if (_repo.DeleteTimeEntryById(id))
                return Request.CreateResponse(HttpStatusCode.Accepted);

            return Request.CreateResponse(HttpStatusCode.NotFound);
        }
        #endregion

        #region TimeEntryApproval
        [HttpGet]
        [Route("api/timeentryapproval")]
        public HttpResponseMessage GetTimeEntryApprovals()
        {

            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetTimeEntryApprovals().Select(i => Mapper.Map<TimeEntryApprovalDto>(i)).ToList());
        }

        [HttpGet]
        [Route("api/timeentryapproval/{id}")]
        public HttpResponseMessage GetTimeEntryApproval(int id)
        {
            return Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<TimeEntryApprovalDto>(_repo.GetTimeEntryApprovalById(id)));
        }

        #endregion

        #region TimeEntryNote
        [HttpGet]
        [Route("api/timeentrynote")]
        public HttpResponseMessage GetTimeEntryNotes()
        {

            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetTimeEntryNotes().Select(i => Mapper.Map<TimeEntryNoteDto>(i)).ToList());
        }

        [HttpGet]
        [Route("api/timeentrynote/{id}")]
        public HttpResponseMessage GetTimeEntryNote(int id)
        {
            return Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<TimeEntryNoteDto>(_repo.GetTimeEntryNoteById(id)));
        }

      
        [HttpPut]
        [Route("api/approvalnote/update")]
        public HttpResponseMessage PutTimeEntryNote(vw_TimeSheetDto dto)
        {
            if (_repo.approve_note_changes(dto))
                return Request.CreateResponse(HttpStatusCode.Accepted);

            return Request.CreateResponse(HttpStatusCode.NotModified);
        }

        [HttpPost]
        [Route("api/timeentrynote/add")]
        public HttpResponseMessage AddTimeEntryNote(TimeEntryNoteDto dto)
        {
           

            if (_repo.AddTimeEntryNote(dto))
                return Request.CreateResponse(HttpStatusCode.OK);

            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }
        #endregion



        //////////// Will move to a different contoller later
        ///
        [HttpGet]
        [Route("api/ValidateToken/{token}")]
        public HttpResponseMessage validateToken(string token)
        {

            AuthRepo repo = new AuthRepo();
            return Request.CreateResponse(HttpStatusCode.OK, repo.TokenValidation(token));


        }

    }
}
