﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ZionPortal.Dto;
using ZionPortal.Repository;

namespace ZionPortal.API
{
    public class CourseScheduleController : ApiController
    {
        private ZionEntities _context;
        private CourseScheduleRepo _repo;
        private CourseRepo _courseRepo;
        private InstructorRepo _instructorRepo;
        private RoomRepo _roomRepo;

        public CourseScheduleController()
        {
            _context = new ZionEntities();
            _repo = new CourseScheduleRepo(_context);
            _courseRepo = new CourseRepo(_context);
            _instructorRepo = new InstructorRepo(_context);
            _roomRepo = new RoomRepo(_context);
        }

        public HttpResponseMessage Get()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetForRegistration().Select(i => Mapper.Map<CourseScheduleDto>(i)).ToList());
        }

        [Route("api/courseschedule/instructorSchedule/{instructorID}")]
        public HttpResponseMessage getCourseScheduleByInstructorID(int instructorID) {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.getCourseScheduleByInstructorID(instructorID));
        }
        public HttpResponseMessage Get(int id)
        {
            return Request.CreateResponse(HttpStatusCode.OK, Mapper.Map<CourseScheduleDto>(_repo.Get(id)));
        }

        public HttpResponseMessage Post(CourseScheduleDto dto)
        {
            if (_repo.Add(dto))
                return Request.CreateResponse(HttpStatusCode.Created);

            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        public HttpResponseMessage Put(CourseScheduleDto dto)
        {
            if (_repo.Update(dto))
                return Request.CreateResponse(HttpStatusCode.Accepted);

            return Request.CreateResponse(HttpStatusCode.NotModified);
        }

        public HttpResponseMessage Delete(CourseScheduleDto dto)
        {
            if (_repo.Delete(Mapper.Map<CourseSchedule>(dto)))
                return Request.CreateResponse(HttpStatusCode.Accepted);

            return Request.CreateResponse(HttpStatusCode.NotFound);
        }

        public HttpResponseMessage Delete(int id)
        {
            if (_repo.Delete(id))
                return Request.CreateResponse(HttpStatusCode.Accepted);

            return Request.CreateResponse(HttpStatusCode.NotFound);
        }

        [Route("api/courseschedule/available")]
        public HttpResponseMessage GetAvailable()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetAvailable().Select(c => new CourseScheduleDto
            {
                CourseScheduleId = c.CourseScheduleID,
                Course = Mapper.Map<CourseDto>(_courseRepo.Get(c.CourseID)),
                Status = c.Status,
                ScheduleDate = c.ScheduleDate,
                StartTime = c.StartTime,
                EndTime = c.EndTime,
                Instructor = Mapper.Map<InstructorDto>(_instructorRepo.Get(c.InstructorID)),
                Room = Mapper.Map<RoomDto>(_roomRepo.Get(c.RoomID)),
                AllDay = c.AllDay,
                Priority = (int)c.Priority,
                CreatedBy = c.CreatedBy,
                CreatedDate = (DateTime)c.CreatedDate,
                ModifiedBy = c.ModifiedBy,
                ModifiedDate = (DateTime)c.ModifiedDate
            }).ToList());
        }

        [Route("api/courseschedule/available/count")]
        public HttpResponseMessage GetAvailableCount()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetAvailable().Count());
        }

        [Route("api/courseschedule/completed")]
        public HttpResponseMessage GetCompleted()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetCompleted().Select(c => new CourseScheduleDto
            {
                CourseScheduleId = c.CourseScheduleID,
                Course = Mapper.Map<CourseDto>(_courseRepo.Get(c.CourseID)),
                Status = c.Status,
                ScheduleDate = c.ScheduleDate,
                StartTime = c.StartTime,
                EndTime = c.EndTime,
                Instructor = Mapper.Map<InstructorDto>(_instructorRepo.Get(c.InstructorID)),
                Room = Mapper.Map<RoomDto>(_roomRepo.Get(c.RoomID)),
                AllDay = c.AllDay,
                Priority = (int)c.Priority,
                CreatedBy = c.CreatedBy,
                CreatedDate = (DateTime)c.CreatedDate,
                ModifiedBy = c.ModifiedBy,
                ModifiedDate = (DateTime)c.ModifiedDate
            }).ToList());
        }

        [Route("api/courseschedule/completed/count")]
        public HttpResponseMessage GetCompletedCount()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetCompleted().Count());
        }

        [Route("api/courseschedule/full")]
        public HttpResponseMessage GetFull()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetFull().Select(c => new CourseScheduleDto
            {
                CourseScheduleId = c.CourseScheduleID,
                Course = Mapper.Map<CourseDto>(_courseRepo.Get(c.CourseID)),
                Status = c.Status,
                ScheduleDate = c.ScheduleDate,
                StartTime = c.StartTime,
                EndTime = c.EndTime,
                Instructor = Mapper.Map<InstructorDto>(_instructorRepo.Get(c.InstructorID)),
                Room = Mapper.Map<RoomDto>(_roomRepo.Get(c.RoomID)),
                AllDay = c.AllDay,
                Priority = (int)c.Priority,
                CreatedBy = c.CreatedBy,
                CreatedDate = (DateTime)c.CreatedDate,
                ModifiedBy = c.ModifiedBy,
                ModifiedDate = (DateTime)c.ModifiedDate
            }).ToList());
        }

        [Route("api/courseschedule/full/count")]
        public HttpResponseMessage GetFullCount()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetFull().Count());
        }

        [Route("api/courseschedule/upcoming")]
        public HttpResponseMessage GetUpcoming()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetUpcoming().Select(c => new CourseScheduleDto
            {
                CourseScheduleId = c.CourseScheduleID,
                Course = Mapper.Map<CourseDto>(_courseRepo.Get(c.CourseID)),
                Status = c.Status,
                ScheduleDate = c.ScheduleDate,
                StartTime = c.StartTime,
                EndTime = c.EndTime,
                Instructor = Mapper.Map<InstructorDto>(_instructorRepo.Get(c.InstructorID)),
                Room = Mapper.Map<RoomDto>(_roomRepo.Get(c.RoomID)),
                AllDay = c.AllDay,
                Priority = (int)c.Priority,
                CreatedBy = c.CreatedBy,
                CreatedDate = (DateTime)c.CreatedDate,
                ModifiedBy = c.ModifiedBy,
                ModifiedDate = (DateTime)c.ModifiedDate
            }).ToList());
        }

        [Route("api/courseschedule/upcoming/count")]
        public HttpResponseMessage GetUpcomingCount()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetUpcoming().Count());
        }
    }

}
