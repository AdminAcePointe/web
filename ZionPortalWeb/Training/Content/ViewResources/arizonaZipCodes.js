﻿var arizonaaddress = [
	{
	    "longitude": "-112.351835",
	    "zipcode": "85001",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.703967"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85002",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85003",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85004",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85005",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85006",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85007",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-111.953512",
	    "zipcode": "85008",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.446797"
	},
	{
	    "longitude": "-111.969420",
	    "zipcode": "85009",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.447489"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85010",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85011",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85012",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85013",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85014",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.115805",
	    "zipcode": "85015",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.478293"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85016",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85017",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-111.735322",
	    "zipcode": "85018",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.482033"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85019",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85020",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85021",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85022",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85023",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85024",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-111.723635",
	    "zipcode": "85025",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.422621"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85026",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85027",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-111.661390",
	    "zipcode": "85028",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.382744"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85029",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85030",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85031",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85032",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85033",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-111.987383",
	    "zipcode": "85034",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.431304"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85035",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85036",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85037",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85038",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85039",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.066878",
	    "zipcode": "85040",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.367267"
	},
	{
	    "longitude": "-112.112254",
	    "zipcode": "85041",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.379728"
	},
	{
	    "longitude": "",
	    "zipcode": "85042",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": ""
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85043",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-111.972770",
	    "zipcode": "85044",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.338743"
	},
	{
	    "longitude": "-112.122581",
	    "zipcode": "85045",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.302168"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85046",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.066901",
	    "zipcode": "85048",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.316039"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85050",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85051",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85053",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85054",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85055",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85060",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85061",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85062",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85063",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85064",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85065",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85066",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85067",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85068",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85069",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85070",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85071",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85072",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85073",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85074",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85075",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85076",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85077",
	    "zipclass": "unique",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85078",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85079",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85080",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85082",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85085",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85086",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85087",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "new river",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85098",
	    "zipclass": "unique",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85099",
	    "zipclass": "unique",
	    "county": "maricopa",
	    "city": "phoenix",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-111.856967",
	    "zipcode": "85201",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "mesa",
	    "state": "Arizona",
	    "latitude": "+33.440695"
	},
	{
	    "longitude": "-111.804513",
	    "zipcode": "85202",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "mesa",
	    "state": "Arizona",
	    "latitude": "+33.393484"
	},
	{
	    "longitude": "-111.824363",
	    "zipcode": "85203",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "mesa",
	    "state": "Arizona",
	    "latitude": "+33.448876"
	},
	{
	    "longitude": "-111.787678",
	    "zipcode": "85204",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "mesa",
	    "state": "Arizona",
	    "latitude": "+33.397131"
	},
	{
	    "longitude": "-111.732134",
	    "zipcode": "85205",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "mesa",
	    "state": "Arizona",
	    "latitude": "+33.443345"
	},
	{
	    "longitude": "-111.717968",
	    "zipcode": "85206",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "mesa",
	    "state": "Arizona",
	    "latitude": "+33.390148"
	},
	{
	    "longitude": "-111.743444",
	    "zipcode": "85207",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "mesa",
	    "state": "Arizona",
	    "latitude": "+33.443017"
	},
	{
	    "longitude": "-111.663655",
	    "zipcode": "85208",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "mesa",
	    "state": "Arizona",
	    "latitude": "+33.395932"
	},
	{
	    "longitude": "-111.840095",
	    "zipcode": "85210",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "mesa",
	    "state": "Arizona",
	    "latitude": "+33.387296"
	},
	{
	    "longitude": "-111.837345",
	    "zipcode": "85211",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "mesa",
	    "state": "Arizona",
	    "latitude": "+33.466313"
	},
	{
	    "longitude": "-111.635307",
	    "zipcode": "85212",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "mesa",
	    "state": "Arizona",
	    "latitude": "+33.342476"
	},
	{
	    "longitude": "-111.830905",
	    "zipcode": "85213",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "mesa",
	    "state": "Arizona",
	    "latitude": "+33.448407"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85214",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "mesa",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-111.718829",
	    "zipcode": "85215",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "mesa",
	    "state": "Arizona",
	    "latitude": "+33.470724"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85216",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "mesa",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-111.478975",
	    "zipcode": "85217",
	    "zipclass": "standard",
	    "county": "pinal",
	    "city": "apache junction",
	    "state": "Arizona",
	    "latitude": "+33.393398"
	},
	{
	    "longitude": "",
	    "zipcode": "85218",
	    "zipclass": "standard",
	    "county": "pinal",
	    "city": "apache junction",
	    "state": "Arizona",
	    "latitude": ""
	},
	{
	    "longitude": "-111.279538",
	    "zipcode": "85219",
	    "zipclass": "standard",
	    "county": "pinal",
	    "city": "apache junction",
	    "state": "Arizona",
	    "latitude": "+33.361583"
	},
	{
	    "longitude": "-111.535089",
	    "zipcode": "85220",
	    "zipclass": "standard",
	    "county": "pinal",
	    "city": "apache junction",
	    "state": "Arizona",
	    "latitude": "+33.408443"
	},
	{
	    "longitude": "-111.476001",
	    "zipcode": "85221",
	    "zipclass": "po box only",
	    "county": "pinal",
	    "city": "bapchule",
	    "state": "Arizona",
	    "latitude": "+33.225652"
	},
	{
	    "longitude": "-111.753991",
	    "zipcode": "85222",
	    "zipclass": "standard",
	    "county": "pinal",
	    "city": "casa grande",
	    "state": "Arizona",
	    "latitude": "+32.890550"
	},
	{
	    "longitude": "-111.603327",
	    "zipcode": "85223",
	    "zipclass": "standard",
	    "county": "pinal",
	    "city": "arizona city",
	    "state": "Arizona",
	    "latitude": "+32.745169"
	},
	{
	    "longitude": "-111.850620",
	    "zipcode": "85224",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "chandler",
	    "state": "Arizona",
	    "latitude": "+33.298461"
	},
	{
	    "longitude": "-111.824367",
	    "zipcode": "85225",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "chandler",
	    "state": "Arizona",
	    "latitude": "+33.325991"
	},
	{
	    "longitude": "-111.932892",
	    "zipcode": "85226",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "chandler",
	    "state": "Arizona",
	    "latitude": "+33.303777"
	},
	{
	    "longitude": "-111.686171",
	    "zipcode": "85227",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "chandler heights",
	    "state": "Arizona",
	    "latitude": "+33.212186"
	},
	{
	    "longitude": "-111.247908",
	    "zipcode": "85228",
	    "zipclass": "standard",
	    "county": "pinal",
	    "city": "coolidge",
	    "state": "Arizona",
	    "latitude": "+32.904485"
	},
	{
	    "longitude": "-111.705441",
	    "zipcode": "85230",
	    "zipclass": "standard",
	    "county": "pinal",
	    "city": "casa grande",
	    "state": "Arizona",
	    "latitude": "+32.815123"
	},
	{
	    "longitude": "-111.261161",
	    "zipcode": "85231",
	    "zipclass": "standard",
	    "county": "pinal",
	    "city": "eloy",
	    "state": "Arizona",
	    "latitude": "+32.914889"
	},
	{
	    "longitude": "-111.424943",
	    "zipcode": "85232",
	    "zipclass": "standard",
	    "county": "pinal",
	    "city": "florence",
	    "state": "Arizona",
	    "latitude": "+32.982354"
	},
	{
	    "longitude": "-111.815281",
	    "zipcode": "85233",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "gilbert",
	    "state": "Arizona",
	    "latitude": "+33.335401"
	},
	{
	    "longitude": "-111.780712",
	    "zipcode": "85234",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "gilbert",
	    "state": "Arizona",
	    "latitude": "+33.352925"
	},
	{
	    "longitude": "-110.855618",
	    "zipcode": "85235",
	    "zipclass": "po box only",
	    "county": "gila",
	    "city": "hayden",
	    "state": "Arizona",
	    "latitude": "+33.576401"
	},
	{
	    "longitude": "-111.695460",
	    "zipcode": "85236",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "higley",
	    "state": "Arizona",
	    "latitude": "+33.299317"
	},
	{
	    "longitude": "-111.064109",
	    "zipcode": "85237",
	    "zipclass": "standard",
	    "county": "pinal",
	    "city": "kearny",
	    "state": "Arizona",
	    "latitude": "+33.145984"
	},
	{
	    "longitude": "-112.053351",
	    "zipcode": "85239",
	    "zipclass": "standard",
	    "county": "pinal",
	    "city": "maricopa",
	    "state": "Arizona",
	    "latitude": "+32.957645"
	},
	{
	    "longitude": "-111.531036",
	    "zipcode": "85241",
	    "zipclass": "po box only",
	    "county": "pinal",
	    "city": "picacho",
	    "state": "Arizona",
	    "latitude": "+32.689594"
	},
	{
	    "longitude": "-111.656128",
	    "zipcode": "85242",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "queen creek",
	    "state": "Arizona",
	    "latitude": "+33.284844"
	},
	{
	    "longitude": "-111.888824",
	    "zipcode": "85244",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "chandler",
	    "state": "Arizona",
	    "latitude": "+33.321316"
	},
	{
	    "longitude": "-111.340953",
	    "zipcode": "85245",
	    "zipclass": "po box only",
	    "county": "pinal",
	    "city": "red rock",
	    "state": "Arizona",
	    "latitude": "+32.579972"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85246",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "chandler",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-111.714232",
	    "zipcode": "85247",
	    "zipclass": "po box only",
	    "county": "pinal",
	    "city": "sacaton",
	    "state": "Arizona",
	    "latitude": "+33.141592"
	},
	{
	    "longitude": "-111.859308",
	    "zipcode": "85248",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "chandler",
	    "state": "Arizona",
	    "latitude": "+33.250947"
	},
	{
	    "longitude": "-111.770161",
	    "zipcode": "85249",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "chandler",
	    "state": "Arizona",
	    "latitude": "+33.247861"
	},
	{
	    "longitude": "-111.874248",
	    "zipcode": "85250",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "scottsdale",
	    "state": "Arizona",
	    "latitude": "+33.524143"
	},
	{
	    "longitude": "-111.792658",
	    "zipcode": "85251",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "scottsdale",
	    "state": "Arizona",
	    "latitude": "+33.420061"
	},
	{
	    "longitude": "-111.868432",
	    "zipcode": "85252",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "scottsdale",
	    "state": "Arizona",
	    "latitude": "+33.499529"
	},
	{
	    "longitude": "-111.963149",
	    "zipcode": "85253",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "paradise valley",
	    "state": "Arizona",
	    "latitude": "+33.360896"
	},
	{
	    "longitude": "-111.830180",
	    "zipcode": "85254",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "scottsdale",
	    "state": "Arizona",
	    "latitude": "+33.483533"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85255",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "scottsdale",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-111.800330",
	    "zipcode": "85256",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "scottsdale",
	    "state": "Arizona",
	    "latitude": "+33.507744"
	},
	{
	    "longitude": "-111.894039",
	    "zipcode": "85257",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "scottsdale",
	    "state": "Arizona",
	    "latitude": "+33.465646"
	},
	{
	    "longitude": "-111.879806",
	    "zipcode": "85258",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "scottsdale",
	    "state": "Arizona",
	    "latitude": "+33.553346"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85259",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "scottsdale",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85260",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "scottsdale",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85261",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "scottsdale",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-111.915298",
	    "zipcode": "85262",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "scottsdale",
	    "state": "Arizona",
	    "latitude": "+33.407445"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85263",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "rio verde",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-111.677135",
	    "zipcode": "85264",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "fort mcdowell",
	    "state": "Arizona",
	    "latitude": "+33.625163"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85266",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "scottsdale",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85267",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "scottsdale",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85268",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "fountain hills",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85269",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "fountain hills",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85271",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "scottsdale",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-111.957245",
	    "zipcode": "85272",
	    "zipclass": "standard",
	    "county": "pinal",
	    "city": "stanfield",
	    "state": "Arizona",
	    "latitude": "+32.895764"
	},
	{
	    "longitude": "-111.130022",
	    "zipcode": "85273",
	    "zipclass": "standard",
	    "county": "pinal",
	    "city": "superior",
	    "state": "Arizona",
	    "latitude": "+33.180583"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85274",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "mesa",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85275",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "mesa",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85277",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "mesa",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-111.326045",
	    "zipcode": "85278",
	    "zipclass": "standard",
	    "county": "pinal",
	    "city": "apache junction",
	    "state": "Arizona",
	    "latitude": "+32.983653"
	},
	{
	    "longitude": "-111.326045",
	    "zipcode": "85279",
	    "zipclass": "standard",
	    "county": "pinal",
	    "city": "florence",
	    "state": "Arizona",
	    "latitude": "+32.983653"
	},
	{
	    "longitude": "-111.931298",
	    "zipcode": "85280",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "tempe",
	    "state": "Arizona",
	    "latitude": "+33.401395"
	},
	{
	    "longitude": "-111.927219",
	    "zipcode": "85281",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "tempe",
	    "state": "Arizona",
	    "latitude": "+33.432844"
	},
	{
	    "longitude": "-111.929253",
	    "zipcode": "85282",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "tempe",
	    "state": "Arizona",
	    "latitude": "+33.396736"
	},
	{
	    "longitude": "-111.876915",
	    "zipcode": "85283",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "tempe",
	    "state": "Arizona",
	    "latitude": "+33.373723"
	},
	{
	    "longitude": "-111.914127",
	    "zipcode": "85284",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "tempe",
	    "state": "Arizona",
	    "latitude": "+33.343546"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85285",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "tempe",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-111.934865",
	    "zipcode": "85287",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "tempe",
	    "state": "Arizona",
	    "latitude": "+33.428511"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85289",
	    "zipclass": "unique",
	    "county": "maricopa",
	    "city": "tempe",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85290",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "tortilla flat",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-111.429790",
	    "zipcode": "85291",
	    "zipclass": "po box only",
	    "county": "pinal",
	    "city": "valley farms",
	    "state": "Arizona",
	    "latitude": "+33.013502"
	},
	{
	    "longitude": "-110.789035",
	    "zipcode": "85292",
	    "zipclass": "standard",
	    "county": "gila",
	    "city": "winkelman",
	    "state": "Arizona",
	    "latitude": "+33.070467"
	},
	{
	    "longitude": "-111.748791",
	    "zipcode": "85296",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "gilbert",
	    "state": "Arizona",
	    "latitude": "+33.314508"
	},
	{
	    "longitude": "",
	    "zipcode": "85297",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "gilbert",
	    "state": "Arizona",
	    "latitude": ""
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85299",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "gilbert",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85301",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "glendale",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85302",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "glendale",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85303",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "glendale",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85304",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "glendale",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85305",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "glendale",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85306",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "glendale",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85307",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "glendale",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85308",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "glendale",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85309",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "luke afb",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85310",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "glendale",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85311",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "glendale",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85312",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "glendale",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85313",
	    "zipclass": "unique",
	    "county": "maricopa",
	    "city": "glendale",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85318",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "glendale",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85320",
	    "zipclass": "po box only",
	    "county": "maricopa",
	    "city": "aguila",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-111.890713",
	    "zipcode": "85321",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "ajo",
	    "state": "Arizona",
	    "latitude": "+31.970131"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85322",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "arlington",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85323",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "avondale",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.134005",
	    "zipcode": "85324",
	    "zipclass": "standard",
	    "county": "yavapai",
	    "city": "black canyon city",
	    "state": "Arizona",
	    "latitude": "+34.075451"
	},
	{
	    "longitude": "-114.003634",
	    "zipcode": "85325",
	    "zipclass": "po box only",
	    "county": "la paz",
	    "city": "bouse",
	    "state": "Arizona",
	    "latitude": "+33.957820"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85326",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "buckeye",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85327",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "cave creek",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-114.032150",
	    "zipcode": "85328",
	    "zipclass": "standard",
	    "county": "la paz",
	    "city": "cibola",
	    "state": "Arizona",
	    "latitude": "+33.672255"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85329",
	    "zipclass": "po box only",
	    "county": "maricopa",
	    "city": "cashion",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85331",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "cave creek",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.787686",
	    "zipcode": "85332",
	    "zipclass": "po box only",
	    "county": "yavapai",
	    "city": "congress",
	    "state": "Arizona",
	    "latitude": "+34.170305"
	},
	{
	    "longitude": "-113.525199",
	    "zipcode": "85333",
	    "zipclass": "standard",
	    "county": "yuma",
	    "city": "dateland",
	    "state": "Arizona",
	    "latitude": "+32.825705"
	},
	{
	    "longitude": "-114.507697",
	    "zipcode": "85334",
	    "zipclass": "standard",
	    "county": "la paz",
	    "city": "ehrenberg",
	    "state": "Arizona",
	    "latitude": "+33.617670"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85335",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "el mirage",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-114.774342",
	    "zipcode": "85336",
	    "zipclass": "po box only",
	    "county": "yuma",
	    "city": "gadsden",
	    "state": "Arizona",
	    "latitude": "+32.530155"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85337",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "gila bend",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85338",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "goodyear",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.182248",
	    "zipcode": "85339",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "laveen",
	    "state": "Arizona",
	    "latitude": "+33.335057"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85340",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "litchfield park",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-111.890713",
	    "zipcode": "85341",
	    "zipclass": "po box only",
	    "county": "pima",
	    "city": "lukeville",
	    "state": "Arizona",
	    "latitude": "+31.970131"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85342",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "morristown",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85343",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "palo verde",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-114.268073",
	    "zipcode": "85344",
	    "zipclass": "standard",
	    "county": "la paz",
	    "city": "parker",
	    "state": "Arizona",
	    "latitude": "+33.967712"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85345",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "peoria",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-114.192454",
	    "zipcode": "85346",
	    "zipclass": "po box only",
	    "county": "la paz",
	    "city": "quartzsite",
	    "state": "Arizona",
	    "latitude": "+33.729219"
	},
	{
	    "longitude": "-113.798728",
	    "zipcode": "85347",
	    "zipclass": "standard",
	    "county": "yuma",
	    "city": "roll",
	    "state": "Arizona",
	    "latitude": "+32.752609"
	},
	{
	    "longitude": "-113.662489",
	    "zipcode": "85348",
	    "zipclass": "standard",
	    "county": "la paz",
	    "city": "salome",
	    "state": "Arizona",
	    "latitude": "+33.647970"
	},
	{
	    "longitude": "-114.754999",
	    "zipcode": "85349",
	    "zipclass": "po box only",
	    "county": "yuma",
	    "city": "san luis",
	    "state": "Arizona",
	    "latitude": "+32.540068"
	},
	{
	    "longitude": "-114.606214",
	    "zipcode": "85350",
	    "zipclass": "standard",
	    "county": "yuma",
	    "city": "somerton",
	    "state": "Arizona",
	    "latitude": "+32.612408"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85351",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "sun city",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-113.976313",
	    "zipcode": "85352",
	    "zipclass": "standard",
	    "county": "yuma",
	    "city": "tacna",
	    "state": "Arizona",
	    "latitude": "+32.693448"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85353",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "tolleson",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85354",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "tonopah",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85355",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "waddell",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-114.156058",
	    "zipcode": "85356",
	    "zipclass": "standard",
	    "county": "yuma",
	    "city": "wellton",
	    "state": "Arizona",
	    "latitude": "+32.693461"
	},
	{
	    "longitude": "-113.458002",
	    "zipcode": "85357",
	    "zipclass": "po box only",
	    "county": "la paz",
	    "city": "wenden",
	    "state": "Arizona",
	    "latitude": "+34.042203"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85358",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "wickenburg",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-114.239557",
	    "zipcode": "85359",
	    "zipclass": "po box only",
	    "county": "la paz",
	    "city": "quartzsite",
	    "state": "Arizona",
	    "latitude": "+33.666880"
	},
	{
	    "longitude": "-113.810600",
	    "zipcode": "85360",
	    "zipclass": "po box only",
	    "county": "mohave",
	    "city": "wikieup",
	    "state": "Arizona",
	    "latitude": "+35.747820"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85361",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "wittmann",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.756652",
	    "zipcode": "85362",
	    "zipclass": "po box only",
	    "county": "yavapai",
	    "city": "yarnell",
	    "state": "Arizona",
	    "latitude": "+34.250781"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85363",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "youngtown",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-114.648722",
	    "zipcode": "85364",
	    "zipclass": "standard",
	    "county": "yuma",
	    "city": "yuma",
	    "state": "Arizona",
	    "latitude": "+32.615305"
	},
	{
	    "longitude": "-114.490471",
	    "zipcode": "85365",
	    "zipclass": "standard",
	    "county": "yuma",
	    "city": "yuma",
	    "state": "Arizona",
	    "latitude": "+32.709332"
	},
	{
	    "longitude": "-114.631172",
	    "zipcode": "85366",
	    "zipclass": "standard",
	    "county": "yuma",
	    "city": "yuma",
	    "state": "Arizona",
	    "latitude": "+32.609959"
	},
	{
	    "longitude": "-114.404216",
	    "zipcode": "85367",
	    "zipclass": "standard",
	    "county": "yuma",
	    "city": "yuma",
	    "state": "Arizona",
	    "latitude": "+32.656575"
	},
	{
	    "longitude": "-114.074901",
	    "zipcode": "85369",
	    "zipclass": "standard",
	    "county": "yuma",
	    "city": "yuma",
	    "state": "Arizona",
	    "latitude": "+32.751632"
	},
	{
	    "longitude": "-114.390171",
	    "zipcode": "85371",
	    "zipclass": "standard",
	    "county": "la paz",
	    "city": "poston",
	    "state": "Arizona",
	    "latitude": "+34.031791"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85372",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "sun city",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85373",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "sun city",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85374",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "surprise",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85375",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "sun city west",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85376",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "sun city west",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85377",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "carefree",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85378",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "surprise",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85379",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "surprise",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85380",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "peoria",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85381",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "peoria",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85382",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "peoria",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "",
	    "zipcode": "85383",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "peoria",
	    "state": "Arizona",
	    "latitude": ""
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85385",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "peoria",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85387",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "surprise",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-112.187170",
	    "zipcode": "85390",
	    "zipclass": "standard",
	    "county": "maricopa",
	    "city": "wickenburg",
	    "state": "Arizona",
	    "latitude": "+33.276539"
	},
	{
	    "longitude": "-110.868076",
	    "zipcode": "85501",
	    "zipclass": "standard",
	    "county": "gila",
	    "city": "globe",
	    "state": "Arizona",
	    "latitude": "+33.476884"
	},
	{
	    "longitude": "-110.812680",
	    "zipcode": "85502",
	    "zipclass": "standard",
	    "county": "gila",
	    "city": "globe",
	    "state": "Arizona",
	    "latitude": "+33.421919"
	},
	{
	    "longitude": "-109.785973",
	    "zipcode": "85530",
	    "zipclass": "po box only",
	    "county": "graham",
	    "city": "bylas",
	    "state": "Arizona",
	    "latitude": "+33.038678"
	},
	{
	    "longitude": "-109.787778",
	    "zipcode": "85531",
	    "zipclass": "po box only",
	    "county": "graham",
	    "city": "central",
	    "state": "Arizona",
	    "latitude": "+32.869243"
	},
	{
	    "longitude": "-110.814893",
	    "zipcode": "85532",
	    "zipclass": "po box only",
	    "county": "gila",
	    "city": "claypool",
	    "state": "Arizona",
	    "latitude": "+33.415409"
	},
	{
	    "longitude": "-109.255872",
	    "zipcode": "85533",
	    "zipclass": "standard",
	    "county": "greenlee",
	    "city": "clifton",
	    "state": "Arizona",
	    "latitude": "+32.991251"
	},
	{
	    "longitude": "-109.215723",
	    "zipcode": "85534",
	    "zipclass": "standard",
	    "county": "greenlee",
	    "city": "duncan",
	    "state": "Arizona",
	    "latitude": "+32.881003"
	},
	{
	    "longitude": "-109.897401",
	    "zipcode": "85535",
	    "zipclass": "standard",
	    "county": "graham",
	    "city": "eden",
	    "state": "Arizona",
	    "latitude": "+32.975112"
	},
	{
	    "longitude": "-109.971733",
	    "zipcode": "85536",
	    "zipclass": "po box only",
	    "county": "graham",
	    "city": "fort thomas",
	    "state": "Arizona",
	    "latitude": "+33.033314"
	},
	{
	    "longitude": "-110.966562",
	    "zipcode": "85539",
	    "zipclass": "standard",
	    "county": "gila",
	    "city": "miami",
	    "state": "Arizona",
	    "latitude": "+33.528204"
	},
	{
	    "longitude": "-109.326923",
	    "zipcode": "85540",
	    "zipclass": "standard",
	    "county": "greenlee",
	    "city": "morenci",
	    "state": "Arizona",
	    "latitude": "+33.054062"
	},
	{
	    "longitude": "-111.075088",
	    "zipcode": "85541",
	    "zipclass": "standard",
	    "county": "gila",
	    "city": "payson",
	    "state": "Arizona",
	    "latitude": "+33.864471"
	},
	{
	    "longitude": "-110.860653",
	    "zipcode": "85542",
	    "zipclass": "standard",
	    "county": "gila",
	    "city": "peridot",
	    "state": "Arizona",
	    "latitude": "+33.741518"
	},
	{
	    "longitude": "-109.775698",
	    "zipcode": "85543",
	    "zipclass": "standard",
	    "county": "graham",
	    "city": "pima",
	    "state": "Arizona",
	    "latitude": "+32.878042"
	},
	{
	    "longitude": "-111.396855",
	    "zipcode": "85544",
	    "zipclass": "standard",
	    "county": "gila",
	    "city": "pine",
	    "state": "Arizona",
	    "latitude": "+34.320445"
	},
	{
	    "longitude": "-111.005928",
	    "zipcode": "85545",
	    "zipclass": "standard",
	    "county": "gila",
	    "city": "roosevelt",
	    "state": "Arizona",
	    "latitude": "+33.614316"
	},
	{
	    "longitude": "-109.701631",
	    "zipcode": "85546",
	    "zipclass": "standard",
	    "county": "graham",
	    "city": "safford",
	    "state": "Arizona",
	    "latitude": "+32.813612"
	},
	{
	    "longitude": "-111.287750",
	    "zipcode": "85547",
	    "zipclass": "standard",
	    "county": "gila",
	    "city": "payson",
	    "state": "Arizona",
	    "latitude": "+34.257457"
	},
	{
	    "longitude": "-109.752196",
	    "zipcode": "85548",
	    "zipclass": "standard",
	    "county": "graham",
	    "city": "safford",
	    "state": "Arizona",
	    "latitude": "+32.797009"
	},
	{
	    "longitude": "-110.491898",
	    "zipcode": "85550",
	    "zipclass": "po box only",
	    "county": "gila",
	    "city": "san carlos",
	    "state": "Arizona",
	    "latitude": "+33.289447"
	},
	{
	    "longitude": "-109.696449",
	    "zipcode": "85551",
	    "zipclass": "po box only",
	    "county": "graham",
	    "city": "solomon",
	    "state": "Arizona",
	    "latitude": "+32.842769"
	},
	{
	    "longitude": "-109.746133",
	    "zipcode": "85552",
	    "zipclass": "standard",
	    "county": "graham",
	    "city": "thatcher",
	    "state": "Arizona",
	    "latitude": "+32.850436"
	},
	{
	    "longitude": "-110.860653",
	    "zipcode": "85553",
	    "zipclass": "po box only",
	    "county": "gila",
	    "city": "tonto basin",
	    "state": "Arizona",
	    "latitude": "+33.741518"
	},
	{
	    "longitude": "-110.860653",
	    "zipcode": "85554",
	    "zipclass": "po box only",
	    "county": "gila",
	    "city": "young",
	    "state": "Arizona",
	    "latitude": "+33.741518"
	},
	{
	    "longitude": "-111.890713",
	    "zipcode": "85601",
	    "zipclass": "po box only",
	    "county": "pima",
	    "city": "arivaca",
	    "state": "Arizona",
	    "latitude": "+31.970131"
	},
	{
	    "longitude": "-110.262611",
	    "zipcode": "85602",
	    "zipclass": "standard",
	    "county": "cochise",
	    "city": "benson",
	    "state": "Arizona",
	    "latitude": "+32.035885"
	},
	{
	    "longitude": "-109.819216",
	    "zipcode": "85603",
	    "zipclass": "standard",
	    "county": "cochise",
	    "city": "bisbee",
	    "state": "Arizona",
	    "latitude": "+31.445577"
	},
	{
	    "longitude": "-109.706426",
	    "zipcode": "85605",
	    "zipclass": "po box only",
	    "county": "cochise",
	    "city": "bowie",
	    "state": "Arizona",
	    "latitude": "+31.891434"
	},
	{
	    "longitude": "-109.835893",
	    "zipcode": "85606",
	    "zipclass": "standard",
	    "county": "cochise",
	    "city": "cochise",
	    "state": "Arizona",
	    "latitude": "+32.043893"
	},
	{
	    "longitude": "-109.866187",
	    "zipcode": "85607",
	    "zipclass": "standard",
	    "county": "cochise",
	    "city": "douglas",
	    "state": "Arizona",
	    "latitude": "+31.604081"
	},
	{
	    "longitude": "-109.596038",
	    "zipcode": "85608",
	    "zipclass": "standard",
	    "county": "cochise",
	    "city": "douglas",
	    "state": "Arizona",
	    "latitude": "+31.415114"
	},
	{
	    "longitude": "-109.818047",
	    "zipcode": "85609",
	    "zipclass": "po box only",
	    "county": "cochise",
	    "city": "dragoon",
	    "state": "Arizona",
	    "latitude": "+32.040753"
	},
	{
	    "longitude": "-109.622175",
	    "zipcode": "85610",
	    "zipclass": "standard",
	    "county": "cochise",
	    "city": "elfrida",
	    "state": "Arizona",
	    "latitude": "+31.737711"
	},
	{
	    "longitude": "-110.563551",
	    "zipcode": "85611",
	    "zipclass": "standard",
	    "county": "santa cruz",
	    "city": "elgin",
	    "state": "Arizona",
	    "latitude": "+31.609653"
	},
	{
	    "longitude": "-110.318682",
	    "zipcode": "85613",
	    "zipclass": "standard",
	    "county": "cochise",
	    "city": "fort huachuca",
	    "state": "Arizona",
	    "latitude": "+31.559131"
	},
	{
	    "longitude": "-110.955483",
	    "zipcode": "85614",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "green valley",
	    "state": "Arizona",
	    "latitude": "+31.838223"
	},
	{
	    "longitude": "-110.158573",
	    "zipcode": "85615",
	    "zipclass": "standard",
	    "county": "cochise",
	    "city": "hereford",
	    "state": "Arizona",
	    "latitude": "+31.411921"
	},
	{
	    "longitude": "-110.248009",
	    "zipcode": "85616",
	    "zipclass": "standard",
	    "county": "cochise",
	    "city": "huachuca city",
	    "state": "Arizona",
	    "latitude": "+31.606087"
	},
	{
	    "longitude": "-109.843770",
	    "zipcode": "85617",
	    "zipclass": "standard",
	    "county": "cochise",
	    "city": "mc neal",
	    "state": "Arizona",
	    "latitude": "+31.512083"
	},
	{
	    "longitude": "-110.695920",
	    "zipcode": "85618",
	    "zipclass": "standard",
	    "county": "pinal",
	    "city": "mammoth",
	    "state": "Arizona",
	    "latitude": "+32.703898"
	},
	{
	    "longitude": "-110.760458",
	    "zipcode": "85619",
	    "zipclass": "po box only",
	    "county": "pima",
	    "city": "mount lemmon",
	    "state": "Arizona",
	    "latitude": "+32.376306"
	},
	{
	    "longitude": "-109.919794",
	    "zipcode": "85620",
	    "zipclass": "po box only",
	    "county": "cochise",
	    "city": "naco",
	    "state": "Arizona",
	    "latitude": "+31.385182"
	},
	{
	    "longitude": "-110.881900",
	    "zipcode": "85621",
	    "zipclass": "standard",
	    "county": "santa cruz",
	    "city": "nogales",
	    "state": "Arizona",
	    "latitude": "+31.463270"
	},
	{
	    "longitude": "-110.932803",
	    "zipcode": "85622",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "green valley",
	    "state": "Arizona",
	    "latitude": "+31.853334"
	},
	{
	    "longitude": "-110.850082",
	    "zipcode": "85623",
	    "zipclass": "standard",
	    "county": "pinal",
	    "city": "oracle",
	    "state": "Arizona",
	    "latitude": "+32.642130"
	},
	{
	    "longitude": "-110.675964",
	    "zipcode": "85624",
	    "zipclass": "standard",
	    "county": "santa cruz",
	    "city": "patagonia",
	    "state": "Arizona",
	    "latitude": "+31.523845"
	},
	{
	    "longitude": "-109.896053",
	    "zipcode": "85625",
	    "zipclass": "standard",
	    "county": "cochise",
	    "city": "pearce",
	    "state": "Arizona",
	    "latitude": "+31.766243"
	},
	{
	    "longitude": "-109.611545",
	    "zipcode": "85626",
	    "zipclass": "po box only",
	    "county": "cochise",
	    "city": "pirtleville",
	    "state": "Arizona",
	    "latitude": "+31.357528"
	},
	{
	    "longitude": "-110.290892",
	    "zipcode": "85627",
	    "zipclass": "po box only",
	    "county": "cochise",
	    "city": "pomerene",
	    "state": "Arizona",
	    "latitude": "+32.049456"
	},
	{
	    "longitude": "-110.909305",
	    "zipcode": "85628",
	    "zipclass": "standard",
	    "county": "santa cruz",
	    "city": "nogales",
	    "state": "Arizona",
	    "latitude": "+31.531998"
	},
	{
	    "longitude": "-110.895188",
	    "zipcode": "85629",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "sahuarita",
	    "state": "Arizona",
	    "latitude": "+31.932883"
	},
	{
	    "longitude": "-110.170442",
	    "zipcode": "85630",
	    "zipclass": "standard",
	    "county": "cochise",
	    "city": "saint david",
	    "state": "Arizona",
	    "latitude": "+31.891828"
	},
	{
	    "longitude": "-110.599242",
	    "zipcode": "85631",
	    "zipclass": "standard",
	    "county": "pinal",
	    "city": "san manuel",
	    "state": "Arizona",
	    "latitude": "+32.620892"
	},
	{
	    "longitude": "-109.754263",
	    "zipcode": "85632",
	    "zipclass": "standard",
	    "county": "cochise",
	    "city": "san simon",
	    "state": "Arizona",
	    "latitude": "+31.880077"
	},
	{
	    "longitude": "-111.890713",
	    "zipcode": "85633",
	    "zipclass": "po box only",
	    "county": "pima",
	    "city": "sasabe",
	    "state": "Arizona",
	    "latitude": "+31.970131"
	},
	{
	    "longitude": "-110.702027",
	    "zipcode": "85634",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "sells",
	    "state": "Arizona",
	    "latitude": "+31.974033"
	},
	{
	    "longitude": "-109.997623",
	    "zipcode": "85635",
	    "zipclass": "standard",
	    "county": "cochise",
	    "city": "sierra vista",
	    "state": "Arizona",
	    "latitude": "+31.810649"
	},
	{
	    "longitude": "-110.280111",
	    "zipcode": "85636",
	    "zipclass": "standard",
	    "county": "cochise",
	    "city": "sierra vista",
	    "state": "Arizona",
	    "latitude": "+31.668685"
	},
	{
	    "longitude": "-110.618814",
	    "zipcode": "85637",
	    "zipclass": "standard",
	    "county": "santa cruz",
	    "city": "sonoita",
	    "state": "Arizona",
	    "latitude": "+31.673105"
	},
	{
	    "longitude": "-110.076564",
	    "zipcode": "85638",
	    "zipclass": "standard",
	    "county": "cochise",
	    "city": "tombstone",
	    "state": "Arizona",
	    "latitude": "+31.671060"
	},
	{
	    "longitude": "-111.890713",
	    "zipcode": "85639",
	    "zipclass": "po box only",
	    "county": "pima",
	    "city": "topawa",
	    "state": "Arizona",
	    "latitude": "+31.970131"
	},
	{
	    "longitude": "-111.051279",
	    "zipcode": "85640",
	    "zipclass": "standard",
	    "county": "santa cruz",
	    "city": "tumacacori",
	    "state": "Arizona",
	    "latitude": "+31.616167"
	},
	{
	    "longitude": "-110.705269",
	    "zipcode": "85641",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "vail",
	    "state": "Arizona",
	    "latitude": "+32.002669"
	},
	{
	    "longitude": "-109.886546",
	    "zipcode": "85643",
	    "zipclass": "standard",
	    "county": "cochise",
	    "city": "willcox",
	    "state": "Arizona",
	    "latitude": "+32.070050"
	},
	{
	    "longitude": "-109.878211",
	    "zipcode": "85644",
	    "zipclass": "standard",
	    "county": "cochise",
	    "city": "willcox",
	    "state": "Arizona",
	    "latitude": "+32.300690"
	},
	{
	    "longitude": "-111.098620",
	    "zipcode": "85645",
	    "zipclass": "standard",
	    "county": "santa cruz",
	    "city": "amado",
	    "state": "Arizona",
	    "latitude": "+31.672252"
	},
	{
	    "longitude": "-111.066245",
	    "zipcode": "85646",
	    "zipclass": "standard",
	    "county": "santa cruz",
	    "city": "tubac",
	    "state": "Arizona",
	    "latitude": "+31.593758"
	},
	{
	    "longitude": "-111.075695",
	    "zipcode": "85648",
	    "zipclass": "standard",
	    "county": "santa cruz",
	    "city": "rio rico",
	    "state": "Arizona",
	    "latitude": "+31.508436"
	},
	{
	    "longitude": "-110.215304",
	    "zipcode": "85650",
	    "zipclass": "standard",
	    "county": "cochise",
	    "city": "sierra vista",
	    "state": "Arizona",
	    "latitude": "+31.489157"
	},
	{
	    "longitude": "-111.113178",
	    "zipcode": "85652",
	    "zipclass": "po box only",
	    "county": "pima",
	    "city": "cortaro",
	    "state": "Arizona",
	    "latitude": "+32.420055"
	},
	{
	    "longitude": "-111.159344",
	    "zipcode": "85653",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "marana",
	    "state": "Arizona",
	    "latitude": "+32.442979"
	},
	{
	    "longitude": "-111.890713",
	    "zipcode": "85654",
	    "zipclass": "po box only",
	    "county": "pima",
	    "city": "rillito",
	    "state": "Arizona",
	    "latitude": "+31.970131"
	},
	{
	    "longitude": "-109.754263",
	    "zipcode": "85655",
	    "zipclass": "standard",
	    "county": "cochise",
	    "city": "douglas",
	    "state": "Arizona",
	    "latitude": "+31.880077"
	},
	{
	    "longitude": "-110.909305",
	    "zipcode": "85662",
	    "zipclass": "standard",
	    "county": "santa cruz",
	    "city": "nogales",
	    "state": "Arizona",
	    "latitude": "+31.531998"
	},
	{
	    "longitude": "-109.754263",
	    "zipcode": "85670",
	    "zipclass": "standard",
	    "county": "cochise",
	    "city": "fort huachuca",
	    "state": "Arizona",
	    "latitude": "+31.880077"
	},
	{
	    "longitude": "-109.754263",
	    "zipcode": "85671",
	    "zipclass": "standard",
	    "county": "cochise",
	    "city": "sierra vista",
	    "state": "Arizona",
	    "latitude": "+31.880077"
	},
	{
	    "longitude": "-110.970869",
	    "zipcode": "85701",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+32.217975"
	},
	{
	    "longitude": "-111.890713",
	    "zipcode": "85702",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+31.970131"
	},
	{
	    "longitude": "-111.890713",
	    "zipcode": "85703",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+31.970131"
	},
	{
	    "longitude": "-111.100062",
	    "zipcode": "85704",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+32.395222"
	},
	{
	    "longitude": "-110.900395",
	    "zipcode": "85705",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+32.294945"
	},
	{
	    "longitude": "-110.879684",
	    "zipcode": "85706",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+32.093314"
	},
	{
	    "longitude": "-110.875093",
	    "zipcode": "85707",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+32.169577"
	},
	{
	    "longitude": "-110.874973",
	    "zipcode": "85708",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+32.187559"
	},
	{
	    "longitude": "-110.897966",
	    "zipcode": "85709",
	    "zipclass": "unique",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+32.200813"
	},
	{
	    "longitude": "-110.789340",
	    "zipcode": "85710",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+32.213926"
	},
	{
	    "longitude": "-110.883744",
	    "zipcode": "85711",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+32.214075"
	},
	{
	    "longitude": "-110.883744",
	    "zipcode": "85712",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+32.253714"
	},
	{
	    "longitude": "-110.945048",
	    "zipcode": "85713",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+32.192676"
	},
	{
	    "longitude": "-110.934945",
	    "zipcode": "85714",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+32.123064"
	},
	{
	    "longitude": "-110.819977",
	    "zipcode": "85715",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+32.251875"
	},
	{
	    "longitude": "-110.923180",
	    "zipcode": "85716",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+32.244250"
	},
	{
	    "longitude": "-111.890713",
	    "zipcode": "85717",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+31.970131"
	},
	{
	    "longitude": "-110.918980",
	    "zipcode": "85718",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+32.311724"
	},
	{
	    "longitude": "-110.947541",
	    "zipcode": "85719",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+32.247175"
	},
	{
	    "longitude": "-111.890713",
	    "zipcode": "85720",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+31.970131"
	},
	{
	    "longitude": "-110.949996",
	    "zipcode": "85721",
	    "zipclass": "unique",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+32.233761"
	},
	{
	    "longitude": "-111.890713",
	    "zipcode": "85722",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+31.970131"
	},
	{
	    "longitude": "-111.890713",
	    "zipcode": "85723",
	    "zipclass": "unique",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+31.970131"
	},
	{
	    "longitude": "-110.944343",
	    "zipcode": "85724",
	    "zipclass": "unique",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+32.240571"
	},
	{
	    "longitude": "-111.890713",
	    "zipcode": "85725",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+31.970131"
	},
	{
	    "longitude": "-110.945346",
	    "zipcode": "85726",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+32.202726"
	},
	{
	    "longitude": "-111.890713",
	    "zipcode": "85728",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+31.970131"
	},
	{
	    "longitude": "-110.772728",
	    "zipcode": "85730",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+32.159829"
	},
	{
	    "longitude": "-110.708174",
	    "zipcode": "85731",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+32.088034"
	},
	{
	    "longitude": "-110.712250",
	    "zipcode": "85732",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+32.084775"
	},
	{
	    "longitude": "-111.890713",
	    "zipcode": "85733",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+31.970131"
	},
	{
	    "longitude": "-110.935337",
	    "zipcode": "85734",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+32.065082"
	},
	{
	    "longitude": "-110.976006",
	    "zipcode": "85735",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+32.440968"
	},
	{
	    "longitude": "-111.890713",
	    "zipcode": "85736",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+31.970131"
	},
	{
	    "longitude": "-110.984499",
	    "zipcode": "85737",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+32.435158"
	},
	{
	    "longitude": "-111.890713",
	    "zipcode": "85738",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "catalina",
	    "state": "Arizona",
	    "latitude": "+31.970131"
	},
	{
	    "longitude": "-110.892213",
	    "zipcode": "85739",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+32.465013"
	},
	{
	    "longitude": "-111.890713",
	    "zipcode": "85740",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+31.970131"
	},
	{
	    "longitude": "-111.058754",
	    "zipcode": "85741",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+32.340073"
	},
	{
	    "longitude": "-111.065029",
	    "zipcode": "85742",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+32.407562"
	},
	{
	    "longitude": "-111.065869",
	    "zipcode": "85743",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+32.333438"
	},
	{
	    "longitude": "-111.890713",
	    "zipcode": "85744",
	    "zipclass": "unique",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+31.970131"
	},
	{
	    "longitude": "-110.869052",
	    "zipcode": "85745",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+32.215967"
	},
	{
	    "longitude": "-111.890713",
	    "zipcode": "85746",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+31.970131"
	},
	{
	    "longitude": "-110.727183",
	    "zipcode": "85747",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+32.098362"
	},
	{
	    "longitude": "-110.738517",
	    "zipcode": "85748",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+32.213246"
	},
	{
	    "longitude": "-110.746670",
	    "zipcode": "85749",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+32.263975"
	},
	{
	    "longitude": "-110.840422",
	    "zipcode": "85750",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+32.297374"
	},
	{
	    "longitude": "-110.714678",
	    "zipcode": "85751",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+32.161972"
	},
	{
	    "longitude": "-111.890713",
	    "zipcode": "85752",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+31.970131"
	},
	{
	    "longitude": "-111.890713",
	    "zipcode": "85754",
	    "zipclass": "standard",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+31.970131"
	},
	{
	    "longitude": "-111.890713",
	    "zipcode": "85775",
	    "zipclass": "unique",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+31.970131"
	},
	{
	    "longitude": "-110.859106",
	    "zipcode": "85777",
	    "zipclass": "unique",
	    "county": "pima",
	    "city": "tucson",
	    "state": "Arizona",
	    "latitude": "+32.071764"
	},
	{
	    "longitude": "-110.032025",
	    "zipcode": "85901",
	    "zipclass": "standard",
	    "county": "navajo",
	    "city": "show low",
	    "state": "Arizona",
	    "latitude": "+34.570811"
	},
	{
	    "longitude": "-110.035185",
	    "zipcode": "85902",
	    "zipclass": "standard",
	    "county": "navajo",
	    "city": "show low",
	    "state": "Arizona",
	    "latitude": "+34.298092"
	},
	{
	    "longitude": "-110.288704",
	    "zipcode": "85911",
	    "zipclass": "standard",
	    "county": "navajo",
	    "city": "cibecue",
	    "state": "Arizona",
	    "latitude": "+35.285746"
	},
	{
	    "longitude": "-110.203073",
	    "zipcode": "85912",
	    "zipclass": "standard",
	    "county": "navajo",
	    "city": "white mountain lake",
	    "state": "Arizona",
	    "latitude": "+34.266588"
	},
	{
	    "longitude": "-109.240647",
	    "zipcode": "85920",
	    "zipclass": "standard",
	    "county": "apache",
	    "city": "alpine",
	    "state": "Arizona",
	    "latitude": "+34.177052"
	},
	{
	    "longitude": "-109.271169",
	    "zipcode": "85922",
	    "zipclass": "standard",
	    "county": "greenlee",
	    "city": "blue",
	    "state": "Arizona",
	    "latitude": "+33.102464"
	},
	{
	    "longitude": "-110.288704",
	    "zipcode": "85923",
	    "zipclass": "po box only",
	    "county": "navajo",
	    "city": "clay springs",
	    "state": "Arizona",
	    "latitude": "+35.285746"
	},
	{
	    "longitude": "-109.742654",
	    "zipcode": "85924",
	    "zipclass": "standard",
	    "county": "apache",
	    "city": "concho",
	    "state": "Arizona",
	    "latitude": "+34.468787"
	},
	{
	    "longitude": "-109.293952",
	    "zipcode": "85925",
	    "zipclass": "standard",
	    "county": "apache",
	    "city": "eagar",
	    "state": "Arizona",
	    "latitude": "+34.107708"
	},
	{
	    "longitude": "-110.010612",
	    "zipcode": "85926",
	    "zipclass": "po box only",
	    "county": "navajo",
	    "city": "fort apache",
	    "state": "Arizona",
	    "latitude": "+34.201164"
	},
	{
	    "longitude": "-109.522950",
	    "zipcode": "85927",
	    "zipclass": "standard",
	    "county": "apache",
	    "city": "greer",
	    "state": "Arizona",
	    "latitude": "+35.237487"
	},
	{
	    "longitude": "-110.588287",
	    "zipcode": "85928",
	    "zipclass": "standard",
	    "county": "navajo",
	    "city": "heber",
	    "state": "Arizona",
	    "latitude": "+34.409980"
	},
	{
	    "longitude": "-109.972598",
	    "zipcode": "85929",
	    "zipclass": "standard",
	    "county": "navajo",
	    "city": "lakeside",
	    "state": "Arizona",
	    "latitude": "+34.175198"
	},
	{
	    "longitude": "-109.853192",
	    "zipcode": "85930",
	    "zipclass": "po box only",
	    "county": "apache",
	    "city": "mcnary",
	    "state": "Arizona",
	    "latitude": "+34.075359"
	},
	{
	    "longitude": "-111.275860",
	    "zipcode": "85931",
	    "zipclass": "standard",
	    "county": "coconino",
	    "city": "forest lakes",
	    "state": "Arizona",
	    "latitude": "+35.537441"
	},
	{
	    "longitude": "-109.522950",
	    "zipcode": "85932",
	    "zipclass": "po box only",
	    "county": "apache",
	    "city": "nutrioso",
	    "state": "Arizona",
	    "latitude": "+35.237487"
	},
	{
	    "longitude": "-110.560023",
	    "zipcode": "85933",
	    "zipclass": "po box only",
	    "county": "navajo",
	    "city": "overgaard",
	    "state": "Arizona",
	    "latitude": "+34.408619"
	},
	{
	    "longitude": "-110.288704",
	    "zipcode": "85934",
	    "zipclass": "po box only",
	    "county": "navajo",
	    "city": "pinedale",
	    "state": "Arizona",
	    "latitude": "+35.285746"
	},
	{
	    "longitude": "-109.935556",
	    "zipcode": "85935",
	    "zipclass": "standard",
	    "county": "navajo",
	    "city": "pinetop",
	    "state": "Arizona",
	    "latitude": "+34.141314"
	},
	{
	    "longitude": "-109.481543",
	    "zipcode": "85936",
	    "zipclass": "standard",
	    "county": "apache",
	    "city": "saint johns",
	    "state": "Arizona",
	    "latitude": "+34.497709"
	},
	{
	    "longitude": "-110.110981",
	    "zipcode": "85937",
	    "zipclass": "standard",
	    "county": "navajo",
	    "city": "snowflake",
	    "state": "Arizona",
	    "latitude": "+34.521702"
	},
	{
	    "longitude": "-109.285553",
	    "zipcode": "85938",
	    "zipclass": "standard",
	    "county": "apache",
	    "city": "springerville",
	    "state": "Arizona",
	    "latitude": "+34.118938"
	},
	{
	    "longitude": "-110.002763",
	    "zipcode": "85939",
	    "zipclass": "po box only",
	    "county": "navajo",
	    "city": "taylor",
	    "state": "Arizona",
	    "latitude": "+34.314937"
	},
	{
	    "longitude": "-109.522950",
	    "zipcode": "85940",
	    "zipclass": "po box only",
	    "county": "apache",
	    "city": "vernon",
	    "state": "Arizona",
	    "latitude": "+35.237487"
	},
	{
	    "longitude": "-109.993738",
	    "zipcode": "85941",
	    "zipclass": "po box only",
	    "county": "navajo",
	    "city": "whiteriver",
	    "state": "Arizona",
	    "latitude": "+33.802071"
	},
	{
	    "longitude": "-110.080632",
	    "zipcode": "85942",
	    "zipclass": "standard",
	    "county": "navajo",
	    "city": "woodruff",
	    "state": "Arizona",
	    "latitude": "+34.647454"
	},
	{
	    "longitude": "-111.597853",
	    "zipcode": "86001",
	    "zipclass": "standard",
	    "county": "coconino",
	    "city": "flagstaff",
	    "state": "Arizona",
	    "latitude": "+35.932116"
	},
	{
	    "longitude": "-112.052427",
	    "zipcode": "86002",
	    "zipclass": "standard",
	    "county": "coconino",
	    "city": "flagstaff",
	    "state": "Arizona",
	    "latitude": "+35.630842"
	},
	{
	    "longitude": "-112.052427",
	    "zipcode": "86003",
	    "zipclass": "standard",
	    "county": "coconino",
	    "city": "flagstaff",
	    "state": "Arizona",
	    "latitude": "+35.630842"
	},
	{
	    "longitude": "-111.324353",
	    "zipcode": "86004",
	    "zipclass": "standard",
	    "county": "coconino",
	    "city": "flagstaff",
	    "state": "Arizona",
	    "latitude": "+35.610905"
	},
	{
	    "longitude": "-112.052427",
	    "zipcode": "86011",
	    "zipclass": "unique",
	    "county": "coconino",
	    "city": "flagstaff",
	    "state": "Arizona",
	    "latitude": "+35.630842"
	},
	{
	    "longitude": "-112.052427",
	    "zipcode": "86015",
	    "zipclass": "standard",
	    "county": "coconino",
	    "city": "bellemont",
	    "state": "Arizona",
	    "latitude": "+35.630842"
	},
	{
	    "longitude": "-112.052427",
	    "zipcode": "86016",
	    "zipclass": "po box only",
	    "county": "coconino",
	    "city": "gray mountain",
	    "state": "Arizona",
	    "latitude": "+35.630842"
	},
	{
	    "longitude": "-111.640991",
	    "zipcode": "86017",
	    "zipclass": "standard",
	    "county": "coconino",
	    "city": "munds park",
	    "state": "Arizona",
	    "latitude": "+34.941220"
	},
	{
	    "longitude": "-111.950030",
	    "zipcode": "86018",
	    "zipclass": "standard",
	    "county": "coconino",
	    "city": "parks",
	    "state": "Arizona",
	    "latitude": "+35.256347"
	},
	{
	    "longitude": "-112.052427",
	    "zipcode": "86020",
	    "zipclass": "po box only",
	    "county": "coconino",
	    "city": "cameron",
	    "state": "Arizona",
	    "latitude": "+35.630842"
	},
	{
	    "longitude": "-112.978914",
	    "zipcode": "86021",
	    "zipclass": "po box only",
	    "county": "mohave",
	    "city": "colorado city",
	    "state": "Arizona",
	    "latitude": "+36.974658"
	},
	{
	    "longitude": "-112.524696",
	    "zipcode": "86022",
	    "zipclass": "standard",
	    "county": "coconino",
	    "city": "fredonia",
	    "state": "Arizona",
	    "latitude": "+36.966490"
	},
	{
	    "longitude": "-112.130937",
	    "zipcode": "86023",
	    "zipclass": "po box only",
	    "county": "coconino",
	    "city": "grand canyon",
	    "state": "Arizona",
	    "latitude": "+35.942057"
	},
	{
	    "longitude": "-112.052427",
	    "zipcode": "86024",
	    "zipclass": "standard",
	    "county": "coconino",
	    "city": "happy jack",
	    "state": "Arizona",
	    "latitude": "+35.630842"
	},
	{
	    "longitude": "-110.089544",
	    "zipcode": "86025",
	    "zipclass": "standard",
	    "county": "navajo",
	    "city": "holbrook",
	    "state": "Arizona",
	    "latitude": "+34.776828"
	},
	{
	    "longitude": "-109.522950",
	    "zipcode": "86028",
	    "zipclass": "standard",
	    "county": "apache",
	    "city": "petrified forest natl pk",
	    "state": "Arizona",
	    "latitude": "+35.237487"
	},
	{
	    "longitude": "-110.288704",
	    "zipcode": "86029",
	    "zipclass": "standard",
	    "county": "navajo",
	    "city": "sun valley",
	    "state": "Arizona",
	    "latitude": "+35.285746"
	},
	{
	    "longitude": "-110.288704",
	    "zipcode": "86030",
	    "zipclass": "po box only",
	    "county": "navajo",
	    "city": "hotevilla",
	    "state": "Arizona",
	    "latitude": "+35.285746"
	},
	{
	    "longitude": "-110.288704",
	    "zipcode": "86031",
	    "zipclass": "standard",
	    "county": "navajo",
	    "city": "indian wells",
	    "state": "Arizona",
	    "latitude": "+35.285746"
	},
	{
	    "longitude": "-110.337348",
	    "zipcode": "86032",
	    "zipclass": "po box only",
	    "county": "navajo",
	    "city": "joseph city",
	    "state": "Arizona",
	    "latitude": "+34.981359"
	},
	{
	    "longitude": "-110.288704",
	    "zipcode": "86033",
	    "zipclass": "po box only",
	    "county": "navajo",
	    "city": "kayenta",
	    "state": "Arizona",
	    "latitude": "+35.285746"
	},
	{
	    "longitude": "-110.288704",
	    "zipcode": "86034",
	    "zipclass": "po box only",
	    "county": "navajo",
	    "city": "keams canyon",
	    "state": "Arizona",
	    "latitude": "+35.285746"
	},
	{
	    "longitude": "-112.052427",
	    "zipcode": "86035",
	    "zipclass": "po box only",
	    "county": "coconino",
	    "city": "leupp",
	    "state": "Arizona",
	    "latitude": "+35.630842"
	},
	{
	    "longitude": "-111.449889",
	    "zipcode": "86036",
	    "zipclass": "standard",
	    "county": "coconino",
	    "city": "marble canyon",
	    "state": "Arizona",
	    "latitude": "+36.922976"
	},
	{
	    "longitude": "-111.463026",
	    "zipcode": "86038",
	    "zipclass": "standard",
	    "county": "coconino",
	    "city": "mormon lake",
	    "state": "Arizona",
	    "latitude": "+34.908287"
	},
	{
	    "longitude": "-110.288704",
	    "zipcode": "86039",
	    "zipclass": "po box only",
	    "county": "navajo",
	    "city": "kykotsmovi village",
	    "state": "Arizona",
	    "latitude": "+35.285746"
	},
	{
	    "longitude": "-111.502009",
	    "zipcode": "86040",
	    "zipclass": "standard",
	    "county": "coconino",
	    "city": "page",
	    "state": "Arizona",
	    "latitude": "+36.910804"
	},
	{
	    "longitude": "-110.288704",
	    "zipcode": "86042",
	    "zipclass": "po box only",
	    "county": "navajo",
	    "city": "polacca",
	    "state": "Arizona",
	    "latitude": "+35.285746"
	},
	{
	    "longitude": "-110.288704",
	    "zipcode": "86043",
	    "zipclass": "po box only",
	    "county": "navajo",
	    "city": "second mesa",
	    "state": "Arizona",
	    "latitude": "+35.285746"
	},
	{
	    "longitude": "-112.052427",
	    "zipcode": "86044",
	    "zipclass": "standard",
	    "county": "coconino",
	    "city": "tonalea",
	    "state": "Arizona",
	    "latitude": "+35.630842"
	},
	{
	    "longitude": "-111.241773",
	    "zipcode": "86045",
	    "zipclass": "po box only",
	    "county": "coconino",
	    "city": "tuba city",
	    "state": "Arizona",
	    "latitude": "+36.132723"
	},
	{
	    "longitude": "-112.366031",
	    "zipcode": "86046",
	    "zipclass": "standard",
	    "county": "coconino",
	    "city": "williams",
	    "state": "Arizona",
	    "latitude": "+35.915563"
	},
	{
	    "longitude": "-110.350610",
	    "zipcode": "86047",
	    "zipclass": "standard",
	    "county": "navajo",
	    "city": "winslow",
	    "state": "Arizona",
	    "latitude": "+34.633764"
	},
	{
	    "longitude": "-112.052427",
	    "zipcode": "86052",
	    "zipclass": "standard",
	    "county": "coconino",
	    "city": "north rim",
	    "state": "Arizona",
	    "latitude": "+35.630842"
	},
	{
	    "longitude": "-111.451826",
	    "zipcode": "86053",
	    "zipclass": "standard",
	    "county": "coconino",
	    "city": "kaibito",
	    "state": "Arizona",
	    "latitude": "+36.910554"
	},
	{
	    "longitude": "-110.655526",
	    "zipcode": "86054",
	    "zipclass": "standard",
	    "county": "navajo",
	    "city": "shonto",
	    "state": "Arizona",
	    "latitude": "+36.593079"
	},
	{
	    "longitude": "-112.567165",
	    "zipcode": "86301",
	    "zipclass": "standard",
	    "county": "yavapai",
	    "city": "prescott",
	    "state": "Arizona",
	    "latitude": "+34.620826"
	},
	{
	    "longitude": "-112.491528",
	    "zipcode": "86302",
	    "zipclass": "standard",
	    "county": "yavapai",
	    "city": "prescott",
	    "state": "Arizona",
	    "latitude": "+34.574921"
	},
	{
	    "longitude": "-112.378309",
	    "zipcode": "86303",
	    "zipclass": "standard",
	    "county": "yavapai",
	    "city": "prescott",
	    "state": "Arizona",
	    "latitude": "+34.495790"
	},
	{
	    "longitude": "-112.490660",
	    "zipcode": "86304",
	    "zipclass": "standard",
	    "county": "yavapai",
	    "city": "prescott",
	    "state": "Arizona",
	    "latitude": "+34.596680"
	},
	{
	    "longitude": "-112.208914",
	    "zipcode": "86305",
	    "zipclass": "standard",
	    "county": "yavapai",
	    "city": "prescott",
	    "state": "Arizona",
	    "latitude": "+34.683235"
	},
	{
	    "longitude": "-112.307777",
	    "zipcode": "86312",
	    "zipclass": "standard",
	    "county": "yavapai",
	    "city": "prescott valley",
	    "state": "Arizona",
	    "latitude": "+34.668291"
	},
	{
	    "longitude": "-112.397730",
	    "zipcode": "86313",
	    "zipclass": "standard",
	    "county": "yavapai",
	    "city": "prescott",
	    "state": "Arizona",
	    "latitude": "+34.706724"
	},
	{
	    "longitude": "-112.262805",
	    "zipcode": "86314",
	    "zipclass": "standard",
	    "county": "yavapai",
	    "city": "prescott valley",
	    "state": "Arizona",
	    "latitude": "+34.627778"
	},
	{
	    "longitude": "-112.263974",
	    "zipcode": "86320",
	    "zipclass": "po box only",
	    "county": "yavapai",
	    "city": "ash fork",
	    "state": "Arizona",
	    "latitude": "+34.970209"
	},
	{
	    "longitude": "-113.164742",
	    "zipcode": "86321",
	    "zipclass": "standard",
	    "county": "yavapai",
	    "city": "bagdad",
	    "state": "Arizona",
	    "latitude": "+34.583624"
	},
	{
	    "longitude": "-111.809798",
	    "zipcode": "86322",
	    "zipclass": "standard",
	    "county": "yavapai",
	    "city": "camp verde",
	    "state": "Arizona",
	    "latitude": "+34.569687"
	},
	{
	    "longitude": "-112.171623",
	    "zipcode": "86323",
	    "zipclass": "standard",
	    "county": "yavapai",
	    "city": "chino valley",
	    "state": "Arizona",
	    "latitude": "+34.755208"
	},
	{
	    "longitude": "-112.132220",
	    "zipcode": "86324",
	    "zipclass": "standard",
	    "county": "yavapai",
	    "city": "clarkdale",
	    "state": "Arizona",
	    "latitude": "+34.659484"
	},
	{
	    "longitude": "-111.927746",
	    "zipcode": "86325",
	    "zipclass": "standard",
	    "county": "yavapai",
	    "city": "cornville",
	    "state": "Arizona",
	    "latitude": "+34.735388"
	},
	{
	    "longitude": "-112.043547",
	    "zipcode": "86326",
	    "zipclass": "standard",
	    "county": "yavapai",
	    "city": "cottonwood",
	    "state": "Arizona",
	    "latitude": "+34.643596"
	},
	{
	    "longitude": "-112.284396",
	    "zipcode": "86327",
	    "zipclass": "standard",
	    "county": "yavapai",
	    "city": "dewey",
	    "state": "Arizona",
	    "latitude": "+34.556520"
	},
	{
	    "longitude": "-112.252340",
	    "zipcode": "86329",
	    "zipclass": "po box only",
	    "county": "yavapai",
	    "city": "humboldt",
	    "state": "Arizona",
	    "latitude": "+34.518883"
	},
	{
	    "longitude": "-112.397730",
	    "zipcode": "86330",
	    "zipclass": "standard",
	    "county": "yavapai",
	    "city": "iron springs",
	    "state": "Arizona",
	    "latitude": "+34.706724"
	},
	{
	    "longitude": "-112.108626",
	    "zipcode": "86331",
	    "zipclass": "po box only",
	    "county": "yavapai",
	    "city": "jerome",
	    "state": "Arizona",
	    "latitude": "+34.748589"
	},
	{
	    "longitude": "-112.657865",
	    "zipcode": "86332",
	    "zipclass": "standard",
	    "county": "yavapai",
	    "city": "kirkland",
	    "state": "Arizona",
	    "latitude": "+34.443950"
	},
	{
	    "longitude": "-112.163493",
	    "zipcode": "86333",
	    "zipclass": "standard",
	    "county": "yavapai",
	    "city": "mayer",
	    "state": "Arizona",
	    "latitude": "+34.414534"
	},
	{
	    "longitude": "-112.537368",
	    "zipcode": "86334",
	    "zipclass": "standard",
	    "county": "yavapai",
	    "city": "paulden",
	    "state": "Arizona",
	    "latitude": "+34.805120"
	},
	{
	    "longitude": "-111.796517",
	    "zipcode": "86335",
	    "zipclass": "standard",
	    "county": "yavapai",
	    "city": "rimrock",
	    "state": "Arizona",
	    "latitude": "+34.641610"
	},
	{
	    "longitude": "-111.279927",
	    "zipcode": "86336",
	    "zipclass": "standard",
	    "county": "coconino",
	    "city": "sedona",
	    "state": "Arizona",
	    "latitude": "+34.756885"
	},
	{
	    "longitude": "-112.875402",
	    "zipcode": "86337",
	    "zipclass": "po box only",
	    "county": "yavapai",
	    "city": "seligman",
	    "state": "Arizona",
	    "latitude": "+35.331088"
	},
	{
	    "longitude": "-112.397730",
	    "zipcode": "86338",
	    "zipclass": "po box only",
	    "county": "yavapai",
	    "city": "skull valley",
	    "state": "Arizona",
	    "latitude": "+34.706724"
	},
	{
	    "longitude": "-111.728572",
	    "zipcode": "86339",
	    "zipclass": "standard",
	    "county": "coconino",
	    "city": "sedona",
	    "state": "Arizona",
	    "latitude": "+34.907188"
	},
	{
	    "longitude": "-112.397730",
	    "zipcode": "86340",
	    "zipclass": "standard",
	    "county": "yavapai",
	    "city": "sedona",
	    "state": "Arizona",
	    "latitude": "+34.706724"
	},
	{
	    "longitude": "-111.767880",
	    "zipcode": "86341",
	    "zipclass": "standard",
	    "county": "yavapai",
	    "city": "sedona",
	    "state": "Arizona",
	    "latitude": "+34.776648"
	},
	{
	    "longitude": "-111.787154",
	    "zipcode": "86342",
	    "zipclass": "standard",
	    "county": "yavapai",
	    "city": "lake montezuma",
	    "state": "Arizona",
	    "latitude": "+34.641736"
	},
	{
	    "longitude": "-112.397730",
	    "zipcode": "86343",
	    "zipclass": "standard",
	    "county": "yavapai",
	    "city": "crown king",
	    "state": "Arizona",
	    "latitude": "+34.706724"
	},
	{
	    "longitude": "-111.684752",
	    "zipcode": "86351",
	    "zipclass": "standard",
	    "county": "coconino",
	    "city": "sedona",
	    "state": "Arizona",
	    "latitude": "+35.032371"
	},
	{
	    "longitude": "-114.017106",
	    "zipcode": "86401",
	    "zipclass": "standard",
	    "county": "mohave",
	    "city": "kingman",
	    "state": "Arizona",
	    "latitude": "+35.341456"
	},
	{
	    "longitude": "-114.063651",
	    "zipcode": "86402",
	    "zipclass": "standard",
	    "county": "mohave",
	    "city": "kingman",
	    "state": "Arizona",
	    "latitude": "+35.263249"
	},
	{
	    "longitude": "-114.310294",
	    "zipcode": "86403",
	    "zipclass": "standard",
	    "county": "mohave",
	    "city": "lake havasu city",
	    "state": "Arizona",
	    "latitude": "+34.500556"
	},
	{
	    "longitude": "-114.330704",
	    "zipcode": "86404",
	    "zipclass": "standard",
	    "county": "mohave",
	    "city": "lake havasu city",
	    "state": "Arizona",
	    "latitude": "+34.557533"
	},
	{
	    "longitude": "-113.642712",
	    "zipcode": "86405",
	    "zipclass": "standard",
	    "county": "mohave",
	    "city": "lake havasu city",
	    "state": "Arizona",
	    "latitude": "+35.605301"
	},
	{
	    "longitude": "-114.118970",
	    "zipcode": "86406",
	    "zipclass": "standard",
	    "county": "mohave",
	    "city": "lake havasu city",
	    "state": "Arizona",
	    "latitude": "+34.756714"
	},
	{
	    "longitude": "-113.642712",
	    "zipcode": "86411",
	    "zipclass": "standard",
	    "county": "mohave",
	    "city": "hackberry",
	    "state": "Arizona",
	    "latitude": "+35.605301"
	},
	{
	    "longitude": "-113.843241",
	    "zipcode": "86412",
	    "zipclass": "standard",
	    "county": "mohave",
	    "city": "hualapai",
	    "state": "Arizona",
	    "latitude": "+35.397172"
	},
	{
	    "longitude": "-114.221531",
	    "zipcode": "86413",
	    "zipclass": "standard",
	    "county": "mohave",
	    "city": "golden valley",
	    "state": "Arizona",
	    "latitude": "+35.300240"
	},
	{
	    "longitude": "-114.567333",
	    "zipcode": "86426",
	    "zipclass": "standard",
	    "county": "mohave",
	    "city": "fort mohave",
	    "state": "Arizona",
	    "latitude": "+35.066454"
	},
	{
	    "longitude": "-114.581228",
	    "zipcode": "86427",
	    "zipclass": "standard",
	    "county": "mohave",
	    "city": "fort mohave",
	    "state": "Arizona",
	    "latitude": "+35.004290"
	},
	{
	    "longitude": "-114.538599",
	    "zipcode": "86429",
	    "zipclass": "standard",
	    "county": "mohave",
	    "city": "bullhead city",
	    "state": "Arizona",
	    "latitude": "+35.171497"
	},
	{
	    "longitude": "-114.543299",
	    "zipcode": "86430",
	    "zipclass": "standard",
	    "county": "mohave",
	    "city": "bullhead city",
	    "state": "Arizona",
	    "latitude": "+35.147264"
	},
	{
	    "longitude": "-114.222136",
	    "zipcode": "86431",
	    "zipclass": "po box only",
	    "county": "mohave",
	    "city": "chloride",
	    "state": "Arizona",
	    "latitude": "+35.389575"
	},
	{
	    "longitude": "-114.543288",
	    "zipcode": "86432",
	    "zipclass": "po box only",
	    "county": "mohave",
	    "city": "littlefield",
	    "state": "Arizona",
	    "latitude": "+34.901161"
	},
	{
	    "longitude": "-114.383666",
	    "zipcode": "86433",
	    "zipclass": "po box only",
	    "county": "mohave",
	    "city": "oatman",
	    "state": "Arizona",
	    "latitude": "+35.028492"
	},
	{
	    "longitude": "-113.415346",
	    "zipcode": "86434",
	    "zipclass": "standard",
	    "county": "mohave",
	    "city": "peach springs",
	    "state": "Arizona",
	    "latitude": "+35.540361"
	},
	{
	    "longitude": "-112.052427",
	    "zipcode": "86435",
	    "zipclass": "po box only",
	    "county": "coconino",
	    "city": "supai",
	    "state": "Arizona",
	    "latitude": "+35.630842"
	},
	{
	    "longitude": "-114.482571",
	    "zipcode": "86436",
	    "zipclass": "standard",
	    "county": "mohave",
	    "city": "topock",
	    "state": "Arizona",
	    "latitude": "+34.779071"
	},
	{
	    "longitude": "-113.642712",
	    "zipcode": "86437",
	    "zipclass": "standard",
	    "county": "mohave",
	    "city": "valentine",
	    "state": "Arizona",
	    "latitude": "+35.605301"
	},
	{
	    "longitude": "-113.642712",
	    "zipcode": "86438",
	    "zipclass": "po box only",
	    "county": "mohave",
	    "city": "yucca",
	    "state": "Arizona",
	    "latitude": "+35.605301"
	},
	{
	    "longitude": "-114.619445",
	    "zipcode": "86439",
	    "zipclass": "standard",
	    "county": "mohave",
	    "city": "bullhead city",
	    "state": "Arizona",
	    "latitude": "+35.095848"
	},
	{
	    "longitude": "-114.245399",
	    "zipcode": "86440",
	    "zipclass": "standard",
	    "county": "mohave",
	    "city": "mohave valley",
	    "state": "Arizona",
	    "latitude": "+35.052400"
	},
	{
	    "longitude": "-114.243188",
	    "zipcode": "86441",
	    "zipclass": "standard",
	    "county": "mohave",
	    "city": "dolan springs",
	    "state": "Arizona",
	    "latitude": "+35.269759"
	},
	{
	    "longitude": "-114.323801",
	    "zipcode": "86442",
	    "zipclass": "standard",
	    "county": "mohave",
	    "city": "bullhead city",
	    "state": "Arizona",
	    "latitude": "+35.005498"
	},
	{
	    "longitude": "-114.179228",
	    "zipcode": "86443",
	    "zipclass": "standard",
	    "county": "mohave",
	    "city": "temple bar marina",
	    "state": "Arizona",
	    "latitude": "+35.210634"
	},
	{
	    "longitude": "-114.077519",
	    "zipcode": "86444",
	    "zipclass": "standard",
	    "county": "mohave",
	    "city": "meadview",
	    "state": "Arizona",
	    "latitude": "+35.982167"
	},
	{
	    "longitude": "-113.642712",
	    "zipcode": "86445",
	    "zipclass": "standard",
	    "county": "mohave",
	    "city": "willow beach",
	    "state": "Arizona",
	    "latitude": "+35.605301"
	},
	{
	    "longitude": "-114.591728",
	    "zipcode": "86446",
	    "zipclass": "standard",
	    "county": "mohave",
	    "city": "mohave valley",
	    "state": "Arizona",
	    "latitude": "+34.849946"
	},
	{
	    "longitude": "-109.522950",
	    "zipcode": "86502",
	    "zipclass": "po box only",
	    "county": "apache",
	    "city": "chambers",
	    "state": "Arizona",
	    "latitude": "+35.237487"
	},
	{
	    "longitude": "-109.691925",
	    "zipcode": "86503",
	    "zipclass": "standard",
	    "county": "apache",
	    "city": "chinle",
	    "state": "Arizona",
	    "latitude": "+36.527297"
	},
	{
	    "longitude": "-109.522950",
	    "zipcode": "86504",
	    "zipclass": "po box only",
	    "county": "apache",
	    "city": "fort defiance",
	    "state": "Arizona",
	    "latitude": "+35.237487"
	},
	{
	    "longitude": "-109.522950",
	    "zipcode": "86505",
	    "zipclass": "standard",
	    "county": "apache",
	    "city": "ganado",
	    "state": "Arizona",
	    "latitude": "+35.237487"
	},
	{
	    "longitude": "-109.247759",
	    "zipcode": "86506",
	    "zipclass": "po box only",
	    "county": "apache",
	    "city": "houck",
	    "state": "Arizona",
	    "latitude": "+35.337124"
	},
	{
	    "longitude": "-109.259299",
	    "zipcode": "86507",
	    "zipclass": "standard",
	    "county": "apache",
	    "city": "lukachukai",
	    "state": "Arizona",
	    "latitude": "+36.398903"
	},
	{
	    "longitude": "-109.522950",
	    "zipcode": "86508",
	    "zipclass": "po box only",
	    "county": "apache",
	    "city": "lupton",
	    "state": "Arizona",
	    "latitude": "+35.237487"
	},
	{
	    "longitude": "-110.308125",
	    "zipcode": "86510",
	    "zipclass": "po box only",
	    "county": "navajo",
	    "city": "pinon",
	    "state": "Arizona",
	    "latitude": "+35.962139"
	},
	{
	    "longitude": "-109.308746",
	    "zipcode": "86511",
	    "zipclass": "po box only",
	    "county": "apache",
	    "city": "saint michaels",
	    "state": "Arizona",
	    "latitude": "+35.903962"
	},
	{
	    "longitude": "-109.285537",
	    "zipcode": "86512",
	    "zipclass": "po box only",
	    "county": "apache",
	    "city": "sanders",
	    "state": "Arizona",
	    "latitude": "+35.406729"
	},
	{
	    "longitude": "-109.522950",
	    "zipcode": "86514",
	    "zipclass": "standard",
	    "county": "apache",
	    "city": "teec nos pos",
	    "state": "Arizona",
	    "latitude": "+35.237487"
	},
	{
	    "longitude": "-109.078440",
	    "zipcode": "86515",
	    "zipclass": "po box only",
	    "county": "apache",
	    "city": "window rock",
	    "state": "Arizona",
	    "latitude": "+35.667596"
	},
	{
	    "longitude": "-110.288704",
	    "zipcode": "86520",
	    "zipclass": "po box only",
	    "county": "navajo",
	    "city": "blue gap",
	    "state": "Arizona",
	    "latitude": "+35.285746"
	},
	{
	    "longitude": "-109.522950",
	    "zipcode": "86535",
	    "zipclass": "standard",
	    "county": "apache",
	    "city": "dennehotso",
	    "state": "Arizona",
	    "latitude": "+35.237487"
	},
	{
	    "longitude": "-109.522950",
	    "zipcode": "86538",
	    "zipclass": "standard",
	    "county": "apache",
	    "city": "many farms",
	    "state": "Arizona",
	    "latitude": "+35.237487"
	},
	{
	    "longitude": "-109.522950",
	    "zipcode": "86540",
	    "zipclass": "standard",
	    "county": "apache",
	    "city": "nazlini",
	    "state": "Arizona",
	    "latitude": "+35.237487"
	},
	{
	    "longitude": "-109.522950",
	    "zipcode": "86544",
	    "zipclass": "standard",
	    "county": "apache",
	    "city": "red valley",
	    "state": "Arizona",
	    "latitude": "+35.237487"
	},
	{
	    "longitude": "-109.522950",
	    "zipcode": "86545",
	    "zipclass": "standard",
	    "county": "apache",
	    "city": "rock point",
	    "state": "Arizona",
	    "latitude": "+35.237487"
	},
	{
	    "longitude": "-109.522950",
	    "zipcode": "86547",
	    "zipclass": "standard",
	    "county": "apache",
	    "city": "round rock",
	    "state": "Arizona",
	    "latitude": "+35.237487"
	},
	{
	    "longitude": "-109.522950",
	    "zipcode": "86549",
	    "zipclass": "po box only",
	    "county": "apache",
	    "city": "sawmill",
	    "state": "Arizona",
	    "latitude": "+35.237487"
	},
	{
	    "longitude": "-109.522950",
	    "zipcode": "86556",
	    "zipclass": "standard",
	    "county": "apache",
	    "city": "tsaile",
	    "state": "Arizona",
	    "latitude": "+35.237487"
	}
];