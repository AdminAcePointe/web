﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZionPortal.Dto
{
    public class EFExceptionDto
    {
        public string ExceptionSource { get; set; }
        public string ExceptionMessage { get; set; }
    }
}