﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ZionPortal.Dto
{
    public class TaskDto
    {
        public int TaskId { get; set; }
        [Required]
        [MinLength(3)]
        [MaxLength(500)]
        public string Content { get; set; }
        public int PersonId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public bool IsComplete { get; set; }
    }
}