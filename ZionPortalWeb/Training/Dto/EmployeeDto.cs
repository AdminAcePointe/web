﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZionPortal.Dto
{
    public class EmployeeDto
    {
        public int EmployeeId { get; set; }
        public PersonDto Person { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
    }

    public class EmployeeUIDetailsDTO
    {
        public ICollection<vw_PayPerServiceDetails> vw_PayPerServiceDetails { get; set; }
        public vw_PersonDetail ddata { get; set; }
        public string UpdatedProfilePic  { get; set; }

}
}