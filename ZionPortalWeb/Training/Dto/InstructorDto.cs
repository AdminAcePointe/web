﻿using System;
using System.Collections.Generic;

namespace ZionPortal.Dto
{
    public class InstructorDto
    {
        public int InstructorId { get; set; }

        public int PersonId { get; set; }
        public PersonDto Person { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }

        public Nullable<bool> IsDeleted { get; set; }
        public virtual ICollection<CourseScheduleDto> courseSchedules { get; set; }
    }

    public class InstructorDetailsDto
    {

        public int instructorID { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string signature { get; set; }
        public Nullable<DateTime> dob { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        //public string room { get; set; }
        //public string course { get; set; }
        //public virtual ICollection<CourseSchedule> courseSchedules { get; set; }

    }


}