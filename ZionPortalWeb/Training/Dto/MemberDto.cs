﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZionPortal.Dto
{
    public class MemberDto
    {

    }


    public class MemberUIDetailsDTO
    {
        public ICollection<PersonEmergencyContactDto> PersonEmergencyContactDto { get; set; }
        public ICollection<vw_ServiceDetails> vw_ServiceDetails { get; set; }
        public vw_MemberDetail ddata { get; set; }
        public string UpdatedProfilePic { get; set; }
    }
}