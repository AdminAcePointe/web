﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZionPortal.Dto
{
    public class testing {
        public int CourseScheduleId { get; set; }
        public string Text { get; set; }
        public string Description { get; set; }
        public int FacilityId { get; set; }
        public string FacilityName { get; set; }
        public int RoomId { get; set; }
        public string RoomName { get; set; }
        public int InstructorId { get; set; }
        public string InstructorName { get; set; }
        public int CourseId { get; set; }
        public decimal CoursePrice { get; set; }
        public string ScheduleDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public bool AllDay { get; set; }
        public bool Isdeleted { get; set; }
        public int Priority { get; set; }
        public string RecurrenceRule { get; set; }
        public string RecurrenceException { get; set; }



    }


    public class WidgetCourseSchedulerDto
    {
        public int CourseScheduleId { get; set; }
        public string Text { get; set; }
        public string Description { get; set; }
        public int FacilityId { get; set; }
        public string FacilityName { get; set; }
        public int RoomId { get; set;}
        public string RoomName { get; set; }
        public int InstructorId { get; set; }
        public string InstructorName { get; set; }
        public int CourseId { get; set; }
        public decimal? CoursePrice { get; set; }
        public DateTime? ScheduleDate { get; set; }
        public TimeSpan? StartTime { get; set; }
        public TimeSpan? EndTime { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public bool AllDay { get; set; }
        public Nullable<bool> Isdeleted { get; set; }
        public int Priority { get; set; }
        public string RecurrenceRule { get; set; }
        public string RecurrenceException { get; set; }
    }

    public class WidgetCourseDto
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string Color { get; set; }
    }

    public class WidgetFacilityDto
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string Color { get; set; }
    }

    public class WidgetRoomDto
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string Color { get; set; }
    }

    public class WidgetInstructorDto
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string Color { get; set; }
    }


}