﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZionPortal.Dto
{
    public class CertDto
    {
        public int CertID { get; set; }
        public int CourseRegistrationID { get; set; }
        public int StudentID { get; set; }
        public string CertUrl { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
       
    }
}