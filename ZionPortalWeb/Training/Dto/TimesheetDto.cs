﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZionPortal.Dto
{


    public  class TimeNoteDto
    {
        public int TimeNotesID { get; set; }
        public int EmployeeID { get; set; }
        public System.DateTime ClockDate { get; set; }
        public int NoteEnteredByEmployeeID { get; set; }
        public System.DateTime NoteEntryDate { get; set; }
        public int NoteTypeID { get; set; }
        public string Notes { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
    }

    public class ApprovalStatusTypeDto
    {
        public int ApprovalStatusID { get; set; }
        public string ApprovalStatusTypeDesc { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }

        //public virtual ICollection<TimeEntryApprovalDto> TimeEntryApprovals { get; set; }
    }

    public class TimeEntryDto
    {
        public int TimeEntryID { get; set; }
        public int EmployeeID { get; set; }
        public int TimeEntryTypeID { get; set; }
        public System.DateTime ClockDateTime { get; set; }
        public string TimeEntryImage { get; set; }
        public string IPAddress { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public Nullable<int> TimeEntryApprovalID { get; set; }
        public Nullable<int> PayrollID { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public int ServiceID { get; set; }

        public virtual EmployeeDto Employee { get; set; }
        //public virtual PayrollDto Payroll { get; set; }
        public virtual ICollection<TimeEntryNoteDto> TimeEntryNotes { get; set; }
        public virtual TimeEntryApprovalDto TimeEntryApproval { get; set; }
        public virtual TimeEntryTypeDto TimeEntryType { get; set; }
    }

    public class TimeEntryApprovalDto
    {
        public int TimeEntryApprovalID { get; set; }
        public int ApproverEmployeeID { get; set; }
        public int ApprovalStatusID { get; set; }
        public Nullable<System.DateTime> ApprovalDate { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }

        //public virtual ApprovalStatusTypeDto ApprovalStatusType { get; set; }
        //public virtual ICollection<TimeEntryDto> TimeEntries { get; set; }
    }

    public class TimeEntryNoteDto
    {
        public int TimeEntryNotesID { get; set; }
        public int NoteTypeID { get; set; }
        public int TimeEntryID { get; set; }
        public string Notes { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }

        //public virtual TimeEntryDto TimeEntry { get; set; }
    }

    public class TimeEntryTypeDto
    {
        public int TimeEntryTypeID { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public string TimeEntryTypeDesc { get; set; }

        //public virtual ICollection<TimeEntryDto> TimeEntries { get; set; }
    }

    public class GroupHome
    {
        public string FacilityName;
    }

    public class Role
    {
        public string RoleName;
    }

    public class Shift
    {
        public string ShiftName;
        public DateTime ShiftStartTime;
        public DateTime ShiftEndTime;
    }
}