﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZionPortal.Dto
{
    public class CourseDto
    {
        public int CourseId { get; set; }
        public int CourseCode { get; set; }
        public string CourseName { get; set; }
        public string CourseDescription { get; set; }
        public CourseSubcategoryDto CourseSubcategory { get; set; }
        public float CoursePrice { get; set; }
        public string Pass_Fail { get; set; }
        public int Duration { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public bool IsDeleted { get; set; }

        public Nullable<int> CertificateExpirationDays { get; set; }
    }
}