﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZionPortal.Dto
{
    public class StudentDto
    {
        public int StudentID { get; set; }
        public int PersonID { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public Nullable<bool> IsDeleted { get; set; }


        public virtual ICollection<CourseRegistration> CourseRegistrations { get; set; }
        public virtual Person Person { get; set; }

    }
}