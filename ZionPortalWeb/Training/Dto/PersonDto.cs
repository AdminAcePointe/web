﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZionPortal.Dto
{
    public class PersonDto
    {
        public int PersonId { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Suffix { get; set; }
        public DateTime DOB { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string PersonPhoto { get; set; }

        public Nullable<int> AddressID { get; set; }
        public Nullable<int> PhoneID { get; set; }
        public Nullable<int> EmailID { get; set; }

        public AddressDto Address { get; set; }
        public PhoneDto Phone { get; set; }
        public EmailDto Email { get; set; }

    }

    public class PersonDetailDto
    {
        public int PersonID { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string DateOfBirth { get; set; }
        public string MaritalStatus { get; set; }
        public string Gender { get; set; }
        public string SSN { get; set; }
        public string PersonPhoto { get; set; }
        public string profilePic { get; set; }
        public int EmployeeID { get; set; }
        public Nullable<int> ManagerID { get; set; }
        public string GroupHome { get; set; }
        public string shift { get; set; }
        public Nullable<int> PayRateID { get; set; }
        public Nullable<decimal> payRate { get; set; }
        public string EffectiveDate { get; set; }
        public string Title { get; set; }
        public Nullable<bool> IsInstructor { get; set; }
        public Nullable<bool> IsStudent { get; set; }
        public string phoneNumber { get; set; }
        public string email { get; set; }
        public string addressline1 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string ZipCode { get; set; }
        public string manager { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> InActiveDate { get; set; }
        public string HireDate { get; set; }
        public Nullable<int> PersonEmergencyContactID { get; set; }
        public string ContactFirstName { get; set; }
        public string ContactLastName { get; set; }
        public string Relationship { get; set; }
        public string ContactPhone { get; set; }
        public string ContactEmail { get; set; }

        public Nullable<int> EmpID { get; set; }
        public Nullable<int> MemberID { get; set; }
        public Nullable<int> MemberServId { get; set; }
        public int MemberServiceTypeId { get; set; }
        public Nullable<int> MemberServiceId { get; set; }
        public string ServiceType { get; set; }
        public string ServiceName { get; set; }

    }
}