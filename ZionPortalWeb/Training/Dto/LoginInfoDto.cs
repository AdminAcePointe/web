﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZionPortal.Dto
{
    public class LoginInfoDto
    {
        public string username { get; set; }
        public string password { get; set; }
        public int pID { get; set; }
    }
}