﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZionPortal.Dto
{
    public class PtoBalanceDto
    {
        public int PTOBalanceID { get; set; }
        public int EmployeeID { get; set; }
        public int PTOTypeID { get; set; }
        public int PTOHoursRemaning { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public virtual Employee Employee { get; set; }
        public virtual PTOType PTOType { get; set; }
    }
}