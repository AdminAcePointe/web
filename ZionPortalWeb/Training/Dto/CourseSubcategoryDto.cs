﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZionPortal.Dto
{
    public class CourseSubcategoryDto
    {
        public int SubcategoryId { get; set; }

        public int CategoryId { get; set; }
        public string SubcategoryName { get; set; }

        public CategoryDto Category { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}