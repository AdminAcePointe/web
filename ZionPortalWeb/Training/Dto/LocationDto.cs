﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZionPortal.Dto
{
    public class LocationDto
    {
        public int cityID { get; set; }
        public string cityName { get; set; }
        public int stateID { get; set; }
        public string stateName { get; set; }
    }
}