﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZionPortal.Dto
{
    public class CourseRegistrationDto
    {
        public int CourseRegistrationId { get; set; }
        public int StudentId { get; set; }
        public CourseScheduleDto CourseSchedule { get; set; }
        public PaymentDto Payment { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }

        public CertDto Cert { get; set; }
        public GradeDto Grade { get; set; }
        public StudentDto Student { get; set;  }
    }
}