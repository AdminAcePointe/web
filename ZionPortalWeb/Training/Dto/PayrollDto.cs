﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZionPortal.Dto
{
    public class PayrollDto
    {
        public int PayrollID { get; set; }
        public string HREmployeeID { get; set; }
        public int PayrollStatusID { get; set; }
        public Nullable<System.DateTime> PayrollApprovalDate { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> DeletedDate { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }

        public virtual PayrollStatusTypeDto PayrollStatusType { get; set; }
        public virtual ICollection<TimeEntryDto> TimeEntries { get; set; }
    }

    public class PayrollStatusTypeDto
    {
        public int PayrollStatusID { get; set; }
        public string Createdby { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public string PayrollStatusTypeDesc { get; set; }

        //public virtual ICollection<PayrollDto> Payrolls { get; set; }
    }

}