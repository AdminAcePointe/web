﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZionPortal.Dto
{
    public class VerificationDetailsDto
    {
        public string Token { get; set; }
        public Nullable<int> PersonID { get; set; }
        public DateTime ExpirationDate{ get; set; }
        public bool isValid { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string role { get; set; }
        public int employeeID { get; set; }
        public string personPhoto { get; set; }
        public List<string> permissions { get; set; }

        public VerificationDetailsDto()
        {
            //session expires in 30 minutes
            this.ExpirationDate = DateTime.Now.AddMinutes(30);
            this.CreatedDate = DateTime.Now;
            this.CreatedBy = "AppUser";
            this.firstname = "";
            this.lastname = "";
            this.role = "";
        }

    }
}