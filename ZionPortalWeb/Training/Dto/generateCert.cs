﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZionPortal.Dto
{
    public class generateCert
    {
       public  string studentname { get; set; }
       public int studentid { get; set; }
       public  int crid { get; set; }
       public  string courseName { get; set; }
       public string instructorName { get; set; }
      public  string certfilename { get; set; }
      public   string finalfilename { get; set; }
       public string datetaken { get; set; }
      public  int? duration { get; set; }
      public  int? score { get; set; }
      public  int passinggrade { get; set; }
       public string templateform { get; set; }

        public int? year { get; set; }

        public bool status { get; set; }
    }
}