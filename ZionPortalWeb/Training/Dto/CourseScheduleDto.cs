﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZionPortal.Dto
{
    public class CourseScheduleDto
    {

        public int CourseScheduleID { get; set; }
        public int CourseID { get; set; }
        public string Status { get; set; }
        public Nullable<System.DateTime> ScheduleDate { get; set; }
        public Nullable<System.TimeSpan> StartTime { get; set; }
        public Nullable<System.TimeSpan> EndTime { get; set; }
        public bool AllDay { get; set; }
        public Nullable<int> Priority { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public int InstructorID { get; set; }
        public int RoomID { get; set; }
        public int CourseScheduleId { get; set; }
        public CourseDto Course { get; set; }
        public Nullable<bool> Isdeleted { get; set; }
        public InstructorDto Instructor { get; set; }
        public RoomDto Room { get; set; }

        public StudentDto Student { get; set; }

        //not in database TODO: Include Fields in DB
    }

    public class CourseScheduleDetailDto
    {
    public int CourseScheduleID { get; set; }
    public int CourseID { get; set; }
    public string CourseName { get; set; }
    public string RoomName { get; set; }
    public string Status { get; set; }
    public Nullable<System.DateTime> ScheduleDate { get; set; }
    public Nullable<System.TimeSpan> StartTime { get; set; }
    public Nullable<System.TimeSpan> EndTime { get; set; }
    public bool AllDay { get; set; }
    public Nullable<int> Priority { get; set; }
    public string CreatedBy { get; set; }
    public Nullable<System.DateTime> CreatedDate { get; set; }
    public string ModifiedBy { get; set; }
    public Nullable<System.DateTime> ModifiedDate { get; set; }
    public int InstructorID { get; set; }
    public int RoomID { get; set; }
    
        
}

}