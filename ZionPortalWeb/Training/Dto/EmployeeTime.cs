﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZionPortal.Dto
{
 

    public class vw_TimeSheetDto
    {
        public string Id { get; set; }
        public int EmployeeID { get; set; }
        public System.DateTime ClockDate { get; set; }
        public string WeekPeriod { get; set; }
        public string EmployeeName { get; set; }
        public Nullable<int> TimeApprovalID { get; set; }
        public Nullable<int> ApproverEmployeeID { get; set; }
        public Nullable<System.DateTime> ApprovalDate { get; set; }
        public string ApprovalStatusTypeDesc { get; set; }
        public Nullable<int> ApprovalStatusID { get; set; }
        public string ApprovalStatus { get; set; }
        public string Notes { get; set; }
        public Nullable<decimal> HoursWorked { get; set; }
        public Nullable<int> NoteEnteredByEmployeeID { get; set; }
        public Nullable<System.DateTime> NoteEntryDate { get; set; }
        public Nullable<int> TimeNotesID { get; set; }
        public string NoteEnteredByEmployeeName { get; set; }
        public Nullable<int> BillTimeApprovalID { get; set; }
        public Nullable<int> BillingApproverEmployeeID { get; set; }
        public Nullable<System.DateTime> BillingApprovalDate { get; set; }
        public string BillingApprovalStatusTypeDesc { get; set; }
        public Nullable<int> BillingApprovalStatusID { get; set; }
        public string BillingApprovalStatus { get; set; }
        public string WorkType { get; set; }
        public Nullable<int> NoteTypeID { get; set; }
        public string NoteType { get; set; }
        public Nullable<int> ManagerID { get; set; }
        public byte isoWeekOfYear { get; set; }
        public Nullable<decimal> AdjustedHours { get; set; }
        public Nullable<int> BillingTimeNotesID { get; set; }
        public string BillingNotes { get; set; }
        public Nullable<int> BillingNoteEnteredByEmployeeID { get; set; }
        public string BillingNoteEnteredByEmployeeName { get; set; }
        public Nullable<System.DateTime> BillingNoteEntryDate { get; set; }
        public Nullable<int> BillingNoteTypeID { get; set; }
        public string BillingNoteType { get; set; }
        public Nullable<int> serviceId { get; set; }
        public string servicename { get; set; }
    }

    public class vw_EmployeeTimesheetDetailDto
    {
        public int TimeEntryID { get; set; }
        public int EmployeeID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmployeeName { get; set; }
        public int TimeEntryTypeID { get; set; }
        public string TimeEntryTypeDesc { get; set; }
        public System.DateTime ClockDateTime { get; set; }
        public System.DateTime ClockDate { get; set; }
        public string WeekDayName { get; set; }
        public byte WeekOfMonth { get; set; }
        public byte ISOWeekOfYear { get; set; }
        public byte Month { get; set; }
        public string MonthName { get; set; }
        public byte Quarter { get; set; }
        public string QuarterName { get; set; }
        public int Year { get; set; }
        public string MMYYYY { get; set; }
        public string MonthYear { get; set; }
        public Nullable<System.DateTime> WeekStarting { get; set; }
        public Nullable<System.DateTime> WeekEnding { get; set; }
        public string WeekPeriod { get; set; }
        public string TimeEntryImage { get; set; }
        public string Notes { get; set; }
        public string noteType { get; set; }
        public Nullable<int> NoteTypeID { get; set; }
    }
}