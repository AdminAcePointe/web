﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZionPortal.Dto
{
    public class GradeDto
    {
        public int GradeID { get; set; }
        public int CourseRegistrationID { get; set; }
        public int StudentID { get; set; }
        public string GradeDescription { get; set; }
        public string Score { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
    }
}