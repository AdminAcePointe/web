﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZionPortal.Dto
{
    public class PtoRequestDto
    {
        public int EmployeeID { get; set; }
        public int PTOTypeID { get; set; }
        public System.DateTime PTOStartDate { get; set; }
        public System.DateTime PTOEndDate { get; set; }
        public int PTOHours { get; set; }
        public Nullable<int> ApprovalStatusID { get; set; }
        public Nullable<int> PayrollID { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }

    }
    //ptoDto = ptoNote + ptoRequest +2 strings
    //view_pto_request

    public class pto_note_dto
    {
        public string typeAndHrs;

        public int EmployeeID { get; set; }
        public int PTOTypeID { get; set; }
        public System.DateTime PTOStartDate { get; set; }
        public System.DateTime PTOEndDate { get; set; }
        public int PTOHours { get; set; }
        public Nullable<int> ApprovalStatusID { get; set; }
        public Nullable<int> PayrollID { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        ///Note info below

        public Nullable<int> PTORequestNotesID { get; set; }
        public int PTONoteTypeID { get; set; }
        public int PTORequestID { get; set; }
        public string PTONotes { get; set; }
        public string NoteCreatedBy { get; set; }
        public System.DateTime NoteCreatedDate { get; set; }
        public string NoteModifiedBy { get; set; }
        public System.DateTime NoteModifiedDate { get; set; }
        public int NoteEnteredByEmployeeID { get; set; }
    }

    public class PtoRequest
    {
        public string text;
        public int ptoHrRequested;
        public int employeeID;
        public string startDate;
        public string endDate;
        public int PtoRequestId;
        public String PTOType;
        public int PTOtypeId;
        public string Notes;
        public Nullable<int> PTORequestNotesID;

    }

    public  class PTOBalanceWithTypeDto
    {
        public int PTOBalanceID { get; set; }
        public int EmployeeID { get; set; }
        public int id { get; set; }
        public int PTOHoursRemaning { get; set; }
        public int PTOBeginningBalance { get; set; }
        public string PTOType { get; set; }
        public string text { get; set; }
    }


}