﻿using System;

namespace ZionPortal.Dto
{
    public class PaymentDto
    {
        public int PaymentId { get; set; }
        public float PaymentAmount { get; set; }
        public string TransactionId { get; set; }
        public PaymentTypeDto PaymentType { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}