﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZionPortal.Dto
{
    public class EmployeeServiceDto
    {
        public Nullable<int> EmpID { get; set; }
        public Nullable<int> MemberID { get; set; }
        public int MemberServiceTypeId { get; set; }
        public int MemberServiceId { get; set; }
        public string ServiceType { get; set; }
        public string ServiceName { get; set; }
        public string ServiceTypeWithService { get; set; }
        public string memberFirstName { get; set; }
        public string memberLastName { get; set; }
        public string memberFullName { get; set; }
        public string memberFullNameWithService { get; set; }
    }
}