﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZionPortal.Dto
{
    public class StudentDetailsDto
    {

        public int studentID { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public Nullable<DateTime> dob { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public virtual ICollection<CourseRegistration> CourseRegistrations { get; set; }
    }

    //public class InstructorDetailsDto
    //{

    //    public int instructorID { get; set; }
    //    public string firstname { get; set; }
    //    public string lastname { get; set; }
    //    public Nullable<DateTime> dob { get; set; }
    //    public string email { get; set; }
    //    public string phone { get; set; }

    //    public int CourseScheduleID { get; set; }
    //    public Nullable<System.DateTime> ScheduleDate { get; set; }
    //    public Nullable<System.TimeSpan> StartTime { get; set; }
    //    public Nullable<System.TimeSpan> EndTime { get; set; }
    //    public int RoomID { get; set; }
    //    public string Room { get; set; }
    //    public string courseName { get; set; }
       
    //}
}