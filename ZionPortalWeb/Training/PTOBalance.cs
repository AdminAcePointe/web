//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ZionPortal
{
    using System;
    using System.Collections.Generic;
    
    public partial class PTOBalance
    {
        public int PTOBalanceID { get; set; }
        public int EmployeeID { get; set; }
        public int PTOTypeID { get; set; }
        public int PTOBeginningBalance { get; set; }
        public int PTOHoursRemaning { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
    
        public virtual PTOType PTOType { get; set; }
        public virtual Employee Employee { get; set; }
    }
}
