﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ZionPortal
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class ZionEntities : DbContext
    {
        public ZionEntities()
            : base("name=ZionEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Address> Addresses { get; set; }
        public virtual DbSet<AddressType> AddressTypes { get; set; }
        public virtual DbSet<Alert> Alerts { get; set; }
        public virtual DbSet<Announcement> Announcements { get; set; }
        public virtual DbSet<ApprovalStatusType> ApprovalStatusTypes { get; set; }
        public virtual DbSet<ApproverType> ApproverTypes { get; set; }
        public virtual DbSet<Attendance> Attendances { get; set; }
        public virtual DbSet<BillingApproval> BillingApprovals { get; set; }
        public virtual DbSet<BillingNote> BillingNotes { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Cert> Certs { get; set; }
        public virtual DbSet<city> cities { get; set; }
        public virtual DbSet<Contact> Contacts { get; set; }
        public virtual DbSet<country> countries { get; set; }
        public virtual DbSet<Country1> Countries1 { get; set; }
        public virtual DbSet<Course> Courses { get; set; }
        public virtual DbSet<CourseRegistration> CourseRegistrations { get; set; }
        public virtual DbSet<CourseSchedule> CourseSchedules { get; set; }
        public virtual DbSet<CourseSubcategory> CourseSubcategories { get; set; }
        public virtual DbSet<Email> Emails { get; set; }
        public virtual DbSet<Facility> Facilities { get; set; }
        public virtual DbSet<Grade> Grades { get; set; }
        public virtual DbSet<Group> Groups { get; set; }
        public virtual DbSet<JobApplicant> JobApplicants { get; set; }
        public virtual DbSet<JobApplicantEducation> JobApplicantEducations { get; set; }
        public virtual DbSet<JobApplicantEmployment> JobApplicantEmployments { get; set; }
        public virtual DbSet<JobApplication> JobApplications { get; set; }
        public virtual DbSet<JobPosting> JobPostings { get; set; }
        public virtual DbSet<NewsletterSubscription> NewsletterSubscriptions { get; set; }
        public virtual DbSet<NoteType> NoteTypes { get; set; }
        public virtual DbSet<Notification> Notifications { get; set; }
        public virtual DbSet<NotificationType> NotificationTypes { get; set; }
        public virtual DbSet<Payment> Payments { get; set; }
        public virtual DbSet<PaymentType> PaymentTypes { get; set; }
        public virtual DbSet<PayPeriod> PayPeriods { get; set; }
        public virtual DbSet<PayRate> PayRates { get; set; }
        public virtual DbSet<Payroll> Payrolls { get; set; }
        public virtual DbSet<PayrollStatusType> PayrollStatusTypes { get; set; }
        public virtual DbSet<Permission> Permissions { get; set; }
        public virtual DbSet<PersonEmergencyContact> PersonEmergencyContacts { get; set; }
        public virtual DbSet<PersonRole> PersonRoles { get; set; }
        public virtual DbSet<PersonSecurable> PersonSecurables { get; set; }
        public virtual DbSet<Phone> Phones { get; set; }
        public virtual DbSet<PhoneType> PhoneTypes { get; set; }
        public virtual DbSet<PTOApproval> PTOApprovals { get; set; }
        public virtual DbSet<PTOBalance> PTOBalances { get; set; }
        public virtual DbSet<PTORequest> PTORequests { get; set; }
        public virtual DbSet<PTORequestNote> PTORequestNotes { get; set; }
        public virtual DbSet<PTOType> PTOTypes { get; set; }
        public virtual DbSet<RolePermission> RolePermissions { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<Room> Rooms { get; set; }
        public virtual DbSet<Shift> Shifts { get; set; }
        public virtual DbSet<State> States { get; set; }
        public virtual DbSet<Student> Students { get; set; }
        public virtual DbSet<Task> Tasks { get; set; }
        public virtual DbSet<TimeApproval> TimeApprovals { get; set; }
        public virtual DbSet<TimeEntry> TimeEntries { get; set; }
        public virtual DbSet<TimeEntryApproval> TimeEntryApprovals { get; set; }
        public virtual DbSet<TimeEntryNote> TimeEntryNotes { get; set; }
        public virtual DbSet<TimeEntryType> TimeEntryTypes { get; set; }
        public virtual DbSet<TimeNote> TimeNotes { get; set; }
        public virtual DbSet<WebSession> WebSessions { get; set; }
        public virtual DbSet<CalendarDate> CalendarDates { get; set; }
        public virtual DbSet<CourseScheduleSummary> CourseScheduleSummaries { get; set; }
        public virtual DbSet<vw_BillingNoteDetail> vw_BillingNoteDetail { get; set; }
        public virtual DbSet<vw_BusinessCalendar> vw_BusinessCalendar { get; set; }
        public virtual DbSet<vw_CityStateCountry> vw_CityStateCountry { get; set; }
        public virtual DbSet<vw_employeeManagerDetail> vw_employeeManagerDetail { get; set; }
        public virtual DbSet<vw_EmployeePTODetail> vw_EmployeePTODetail { get; set; }
        public virtual DbSet<vw_EmployeeTimesheetDetail> vw_EmployeeTimesheetDetail { get; set; }
        public virtual DbSet<vw_ManagerDetail> vw_ManagerDetail { get; set; }
        public virtual DbSet<vw_PermissionDetail> vw_PermissionDetail { get; set; }
        public virtual DbSet<vw_PTONoteDetail> vw_PTONoteDetail { get; set; }
        public virtual DbSet<vw_PTORequest> vw_PTORequest { get; set; }
        public virtual DbSet<vw_PTORequestBalance> vw_PTORequestBalance { get; set; }
        public virtual DbSet<vw_StudentSummary> vw_StudentSummary { get; set; }
        public virtual DbSet<vw_Time> vw_Time { get; set; }
        public virtual DbSet<vw_TimeNoteDetail> vw_TimeNoteDetail { get; set; }
        public virtual DbSet<vw_TimeSheet> vw_TimeSheet { get; set; }
        public virtual DbSet<vwCourseScheduleAvailable> vwCourseScheduleAvailables { get; set; }
        public virtual DbSet<vwCourseScheduleCompleted> vwCourseScheduleCompleteds { get; set; }
        public virtual DbSet<vwCourseScheduleFull> vwCourseScheduleFulls { get; set; }
        public virtual DbSet<vwCourseScheduleUpcoming> vwCourseScheduleUpcomings { get; set; }
        public virtual DbSet<vwInstructorAvailable> vwInstructorAvailables { get; set; }
        public virtual DbSet<vwInstructorEmployee> vwInstructorEmployees { get; set; }
        public virtual DbSet<vwInstructorUnavailable> vwInstructorUnavailables { get; set; }
        public virtual DbSet<vwRoomAvailable> vwRoomAvailables { get; set; }
        public virtual DbSet<vwRoomUnavailable> vwRoomUnavailables { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<Person> People { get; set; }
        public virtual DbSet<Instructor> Instructors { get; set; }
        public virtual DbSet<MemberEmployeeService> MemberEmployeeServices { get; set; }
        public virtual DbSet<MemberService> MemberServices { get; set; }
        public virtual DbSet<MemberServiceType> MemberServiceTypes { get; set; }
        public virtual DbSet<vw_PayPerServiceDetails> vw_PayPerServiceDetails { get; set; }
        public virtual DbSet<vw_PersonDetail> vw_PersonDetail { get; set; }
        public virtual DbSet<MemberStatu> MemberStatus { get; set; }
        public virtual DbSet<vw_EmployeeDetails> vw_EmployeeDetails { get; set; }
        public virtual DbSet<vw_ServiceDetails> vw_ServiceDetails { get; set; }
        public virtual DbSet<vw_Concat_ServiceType_Service> vw_Concat_ServiceType_Service { get; set; }
        public virtual DbSet<Member> Members { get; set; }
        public virtual DbSet<vw_MemberDetail> vw_MemberDetail { get; set; }
    }
}
