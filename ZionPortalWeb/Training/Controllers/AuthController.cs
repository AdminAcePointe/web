﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using ZionPortal.API;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using ZionPortal.Dto;

namespace ZionPortal.Controllers
{
    public class AuthController : Controller
    {
         LoginInfoDto loginDetail = new LoginInfoDto();
        [System.Web.Mvc.HttpPost]
        public ActionResult Authenticate([FromBody] string username, string password)
        {

            loginDetail.username = username;
            loginDetail.password = password;
            var authResponse = new  API.AuthController();
            VerificationDetailsDto res =  authResponse.authenticateCredentials(loginDetail);
           if (res != null)
           {
                var json = JsonConvert.SerializeObject(res);
                ViewBag.LoginFailure = "";
                TempData["UserInfo"] = json;
                return RedirectToAction("Index", "Home");
           }
            ViewBag.LoginFailure = "Invalid UserName/Password Combination";
            return View("Login");


        }

   


        public string Authorize()
        {
            return "failure";
        }

        public ActionResult Login()
        {
            return View();
        }

        public ActionResult Logout()
        {
            return View();
        }

        //public ActionResult passwordRecovery([FromBody] string username)
        //{
        //    //UI should do some input validation before calling this method
        //        var authResponse = new API.AuthController();
        //        if (authResponse.pwdRecovery(username) != "")
        //        {
        //            ViewBag.PwdRec = "An Info was sent to the email provided";

        //        }

        //    ViewBag.PwdRec = "What are you doing????";
        //    return View();
        //}
    }
}