﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ZionPortal.Controllers
{
    public class EmployeeController : Controller
    {
        // GET: Employee
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult EmployeeProfile()
        {
            return View();
        }
        public ActionResult ManageEmployee()
        {
            return View();
        }
    }
}