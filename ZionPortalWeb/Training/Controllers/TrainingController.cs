﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ZionPortal.Controllers
{
    public class TrainingController : Controller
    {
        // GET: ZionPortal
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Courses()
        {
            return View();
        }

        public ActionResult TrainingSchedule()
        {
            return View();
        }
        public ActionResult Instructors()
        {
            return View();
        }
        public ActionResult Rooms()
        {
            return View();
        }

        public ActionResult Facilities()
        {
            return View();
        }

        public ActionResult Students()
        {
            return View();
        }
    }
}