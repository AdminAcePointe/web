﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ZionPortal.Controllers
{
    public class MemberController : Controller
    {
        // GET: Member
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult MemberProfile()
        {
            return View();
        }
        public ActionResult ManageMember()
        {
            return View();
        }
        public ActionResult MemberReport()
        {
            return View();
        }
    }
}