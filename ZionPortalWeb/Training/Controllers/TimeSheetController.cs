﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ZionPortal.Controllers
{
    public class TimesheetController : Controller
    {
        // GET: Timesheet
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult EnterTime()
        {
            return View();
        }

        public ActionResult ApproveTime()
        {
            return View();
        }

        public ActionResult ViewTime()
        {
            return View();
        }

        public ActionResult TimesheetBilling()
        {
            return View();
        }
    }
}