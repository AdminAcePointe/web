﻿
facilitycount();
roomcount();

function facilitycount() {
    $.ajax({
        type: "GET",
        url: "/api/facility/count",
        success: function (data) {
            $('#facilities').text(data)
        },
        error: function () {
            $('#facilities').text("Error Occured")
        }
    });
}

function roomcount() {
    $.ajax({
        type: "GET",
        url: "/api/facility/roomcount",
        success: function (data) {
            $('#rooms').text(data)
        },
        error: function () {
            $('#rooms').text("Error Occured")
        }
    });
}



var url = "/api/Room";

var dataStore = new DevExpress.data.CustomStore({
    key: "roomId",
    type: "array",
    loadMode: "raw",
    cacheRawData: false,
    load: function (loadOptions) {
        var d = $.getJSON(url);
        return d;
    },
    byKey: function (key) {
        return $.getJSON(url + "/" + encodeURIComponent(key));
    }, insert: function (values) {
        return $.post(url, values);
    },
    add: function (values) {
        return $.ajax({
            url: url,
            method: "POST",
            data: values
        });
    },
    update: function (key, values) {
        return $.ajax({
            url: url,
            method: "PUT",
            data: values
        });
    },
    remove: function (key) {
        return $.ajax({
            url: url + "/" + encodeURIComponent(key),
            method: "DELETE",
        });
    }
});

var dataSource = new DevExpress.data.DataSource({
    store: dataStore
});

var fdataStore = new DevExpress.data.CustomStore({
    key: "facilityId",
    type: "array",
    loadMode: "raw",
    cacheRawData: false,
    load: function (loadOptions) {
        var d = $.getJSON("/api/facility");
        return d;
    },
    byKey: function (key) {
        return $.getJSON(url + "/" + encodeURIComponent(key));
    }, insert: function (values) {
        return $.post(url, values);
    },
    add: function (values) {
        return $.ajax({
            url: url,
            method: "POST",
            data: values
        });
    },
    update: function (key, values) {
        return $.ajax({
            url: url,
            method: "PUT",
            data: values
        });
    },
    remove: function (key) {
        return $.ajax({
            url: url + "/" + encodeURIComponent(key),
            method: "DELETE",
        });
    }
});

var fdataSource = new DevExpress.data.DataSource({
    store: fdataStore
});

$(function () {

    $("#roomGrid").dxDataGrid({
        dataSource: dataSource,
        editing: {
            mode: "row",
            allowAdding: true,
            allowUpdating: true,
            allowDeleting: true,
            useIcons: true
        },
        onToolbarPreparing: function (e) {
            var dataGrid = e.component;
            e.toolbarOptions.items.unshift({
                location: "after",
                widget: "dxButton",
                options: {
                    icon: "refresh",
                    onClick: function () {
                        dataGrid.refresh();
                    }
                }
            });
        },
        onRowUpdating: function (options) {
            $.extend(options.newData, $.extend(true, {}, options.oldData, options.newData));
        },
        Paging: {
            pageSize: 10
        },
        pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 25, 50, 100]
        },
        allowColumnReordering: true,
        allowColumnResizing: true,
        rowAlternationEnabled: true,
        hoverStateEnabled: true,

        columns: [
                {
                    dataField: "roomId",
                    caption: "ID",
                    width: "10%",
                    allowEditing: false,

    formItem: {
        visible: false
    },
                    allowAdding: false,
                   // width: 80

                },
                        {
                            dataField: "roomName",
                            caption: "Room Name",
                            width: "40%",

                        },
                        {
                            dataField: "roomCapacity",
                            caption: "Room Capacity",
                            width: "20%",

                        }
                    ,
             {
                 dataField: "facility.facilityId",
                 caption: "Facility",
                 width: "30%",
                 validationRules: [{
                     type: "required",
                     message: "The facility is required."
                 }]
                 , lookup: {

        dataSource: function (options) {
            var dataSourceConfiguration = {
                store: fdataStore
            }; 
            try {
                      
              //  dataSourceConfiguration.filter = ['facilityId', '=', options.data.facility.facilityId];
            }
            catch (e) {

            }
            return dataSourceConfiguration;

                        
        },
        valueExpr: "facilityId",
        displayExpr: "facilityName"
                 }
             }],

        filterRow: {
            visible: true
        },
        headerFilter: {
            visible: true
        },
        groupPanel: {
            visible: true
        },
        scrolling: {
            mode: "virtual"
        },
        showBorders: true,


        grouping: {
            autoExpandAll: false
        }
    })
});



