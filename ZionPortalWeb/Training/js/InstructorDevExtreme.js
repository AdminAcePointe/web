﻿var courseID;
var dataFromCell = {};
var instructorID;
var signature;
var courseSchedule;
// Cards
unassignedinstructors();
assignedinstructors();
instructors();
function unassignedinstructors() {
    $.ajax({
        type: "GET",
        url: "/api/instructor/available/count",
        success: function (data) {
            $('#unassignedinstructors').text(data)
        },
        error: function () {
            $('#unassignedinstructors').text("Error Occured")
        }
    });
}

function assignedinstructors() {
    $.ajax({
        type: "GET",
        url: "/api/instructor/unavailable/count",
        success: function (data) {
            $('#assignedinstructors').text(data)
        },
        error: function () {
            $('#assignedinstructors').text("Error Occured")
        }
    });
}

function instructors() {
    $.ajax({
        type: "GET",
        url: "/api/instructor/count",
        success: function (data) {
            $('#instructors').text(data)
        },
        error: function () {
            $('#instructors').text("Error Occured")
        }
    });
}

function savesignature(instructorID, signature) {
    var t = signature.toString().replace('image/jsignature;base30,', '');

    $.ajax({
        type: "GET",
        url: "/api/instructor/savesignature" + "/" + instructorID + "/" + t,
        success: function (data) {
          alert("Signature Saved")
        },
        error: function () {
           alert("Error Occured saving signature")
        }
    });
}

function getsignature(instructorID) {
   

    $.ajax({
        type: "GET",
        async: false,
        url: "/api/instructor/getsignature" + "/" + instructorID,
        success: function (data) {
            signature = 'image/jsignature;base30,' + data
           // alert(signature)
        },
        error: function () {
            alert("Error occured getting signature from the database")
        }
    });
}

    var url = "/api/Instructor";

var InsdataStore = new DevExpress.data.CustomStore({
    key: "instructorID",
    type: "array",
    loadMode: "raw",
    cacheRawData: false,
    load: function (loadOptions) {
        var d = $.getJSON(url);
        return d;
    },
    byKey: function (key) {
        return $.getJSON(url + "/" + encodeURIComponent(key));
    }
    , insert: function (values) {
        return $.post(url, values)
        
    },
    update: function (values) {
        return $.ajax({
            url: "/api/updateInstructor",
            method: "PUT",
            data: values
        });
    },
    remove: function (key) {
        return $.ajax({
            url: "/api/deleteInstructor" + "/" + encodeURIComponent(key),
            method: "DELETE",
        });
    }
});

var InsdataSource = new DevExpress.data.DataSource({
    store: InsdataStore
});

var csid;
var c2dataStore = new DevExpress.data.CustomStore({
    key: "student.personID",
    type: "array",
    loadMode: "raw",
    cacheRawData: false,
    load: function (loadOptions) {
        var d = $.getJSON("/api/courseregistration/GetByCourseScheduleID/" + csid);
        return d;
    },
    byKey: function (key) {
        return $.getJSON("/api/courseregistration/" + encodeURIComponent(key));
    }
   , insert: function (values) {
          return $.post("/api/courseregistration", values);
      },
    add: function (values) {
        return $.ajax({
            url: "/api/courseregistration",
            method: "POST",
            data: values
        });
    },
    update: function (key, values) {
        return $.ajax({
            url : updateurl,
            //url: "/api/courseregistration/",
            method: "PUT",
            data: values
        });
    },
    remove: function (key) {
        return $.ajax({
            url: "/api/Course/courseregistration/" + encodeURIComponent(key),
            method: "DELETE"

        });
    }
});

var c2dataSource = new DevExpress.data.DataSource({
    store: c2dataStore
});


$(function () {

        $("#instructorGrid").dxDataGrid({
            dataSource: InsdataSource,
            editing: {
                mode: "form",
                allowAdding: false,
                allowUpdating: false,
                allowDeleting: false,
                useIcons: true
            },
            onRowExpanding: function (e) {
                e.component.collapseAll(-1);
            },
            onToolbarPreparing: function (e) {
                var dataGrid = e.component;
                e.toolbarOptions.items.unshift({
                    location: "after",
                    widget: "dxButton",
                    options: {
                        icon: "refresh",
                        onClick: function () {
                            dataGrid.refresh();
                        }
                    }
                })
            },
            onRowUpdating: function (options) {
                $.extend(options.newData, $.extend(true, {}, options.oldData, options.newData));
            },
            Paging: {
                pageSize: 10
            },
            pager: {
                showPageSizeSelector: true,
                allowedPageSizes: [10, 25, 50, 100]
            },
            allowColumnReordering: true,
            allowColumnResizing: true,
            rowAlternationEnabled: true,
            hoverStateEnabled: true,

            columns: [{
                dataField: "instructorID",
                caption: "Instr. Id",
                allowGrouping: true,
                allowEditing: false,
                width:"8%",
                formItem: {
                    visible: false
                }


            },
            {
            dataField: "firstname",
                caption: "First Name",
                width: "20%",
            }
            ,
            {
                dataField: "lastname",
                caption: "Last Name",
                width: "20%",
            }
            ,
            {
                dataField: "phone",
                caption: "Phone",
                width: "15%",
            }
           
             ,
            {
                dataField: "email",
                caption: "Email",
                width: "25%",
            },
                 {
                     name: 'Signature'
                                 , dataField: ''
                                , width: "12%"
                                 , allowEditing: false,
                     formItem: {
            visible: false
        }

                                 , caption: 'Signature',
                     cellTemplate: function (container, options) {
                         $("<div />").dxButton({
                             icon: 'doc',
                             onClick: function (e) {

                                 try {
                                     instructorID = options.data.instructorID;
                                     sig = options.data.signature;
                                     var p = $("#sigGrid").dxPopup("instance");
                                     p.option("title", "Manage Signature - " + options.data.firstname + " " + options.data.lastname);
                                     p.show(options.data)
                                 }
                                 catch (e) {

                                     DevExpress.ui.dialog.alert("Error retrieving signature")
                                 }

                             }
                         }).appendTo(container);
                     }
                 }
            ],
            masterDetail: {
                enabled: true,
              
                template: function (container, options) {
                    var d = options.data.courseSchedules
                    var d2 = options.data

                    $("<div>")
                      .addClass("master-detail-caption")
                      .text(d2.firstname + " " + d2.lastname + "'s course schedule(s):")
                      .appendTo(container);

                    $("<div>")
                        .dxDataGrid({
                            dataSource: DevExpress.data.AspNet.createStore({
                                key: "courseScheduleID",
                                loadUrl: "/api/courseschedule/instructorSchedule/" + d2.instructorID,
                            }),
                            allowColumnReordering: true,
                            allowColumnResizing: true,
                            rowAlternationEnabled: true,
                            hoverStateEnabled: true,
                            columns: [{
                                dataField: "courseScheduleID",
                                caption: "ID",
                                width: "5%"
                            },
                            {
                                dataField: "courseName",
                                caption: "Course Name",
                                width: "20%",
                               
                            },
                           
                            {
                                dataField: "roomName",
                                caption: "Training Room",
                                width: "20%",
                            }
                            ,
                            {
                                dataField: "scheduleDate",
                                caption: "Scheduled Date",
                                dataType: "date",
                                width: "15%",
                            }
                            ,
                             {
                                 dataField: "startTime",
                                 caption: "Start Time",
                                 width: "15%",
                             }
                            ,
                             {
                                 dataField: "endTime",
                                 caption: "End Time",
                                 width: "15%",
                             }
                            ,
                             
                            
                             {
                                 name: 'Manage Grades & Certs'
                                 , dataField: ''
                                 , width: "10%"
                                 , caption: 'Grades & Certs',
                                 cellTemplate: function (container, options) {
                                     $("<div />").dxButton({
                                         icon: 'doc',
                                         onClick: function (e) {
                                             courseID = options.data.courseScheduleID
                                            
                                             dataFromCell = options.data;
                                             var p = $("#gradeGrid").dxPopup("instance");
                                             p.option("title", "Manage Grades & Certs - " + options.data.courseName);
                                             p.show(options.data)
                                         }
                                     }).appendTo(container);
                                 }
                             }
                             //, {
                             //    name: 'Cert'
                             //    , dataField: ''
                             //    , caption: 'Upload Certs'
                             //    , width: 100,
                             //    cellTemplate: function (container, options) {
                             //        $("<div />").dxButton({
                             //            icon: 'far fa-paper-plane',
                             //            onClick: function (e) {

                             //                var email = options.data.person.email.email1
                             //                var Course = options.data.courseSchedule.course.courseName
                             //                sessionStorage.setItem("studentemail", email);
                             //                sessionStorage.setItem("CourseName", Course);
                             //                //  alert(p);
                             //                window.location.href = "/Common/ComposeEmail"
                             //                //  $("#popup").dxPopup("instance").show();
                             //                //  $("#popup").option("title", options.data.person.firstName + "Cert");
                             //            }
                             //        }).appendTo(container);
                             //    }
                             //}


                            ],

                            showBorders: false,

                            filterRow: {
                                visible: false
                            },
                            groupPanel: {
                                visible: false
                            },
                            scrolling: {
                                mode: "virtual"
                            },
                        }).appendTo(container);
                }
            },
            filterRow: {
                visible: true
            },
            headerFilter: {
                visible: true
            },
            groupPanel: {
                visible: true
            },
            scrolling: {
                mode: "fixed"
            },
            // height: 600,
            showBorders: true,
           

            grouping: {
                autoExpandAll: false
            }

        });
    });

var score = 1;
var studentID = 1;
var crid = 1;
var updateurl

function geturl(newData){
    score = newData.grade.score;
    studentID = newData.studentId;
    crid  = newData.courseRegistrationId;
    updateurl = "/api/courseregistration/upsertGrade/" + studentID + "/" + crid + "/" + score
    return updateurl
}

$(function () {

    $("#gradeGrid").dxPopup({
        height: "auto",
        width:800,
        visible: false,
        closeOnBackButton: true,
        closeOnOutsideClick: false,
        onShown: function (e) {
            e.element.attr('act', 0);
            //geturl()

            csid = dataFromCell.courseScheduleID;

         
            console.log(dataFromCell.courseScheduleID);
            $("#gradeCertGrid").dxDataGrid({
               
                dataSource:c2dataSource,
              
                editing: {
                    editMode: 'row',
                    editEnabled: true,
                    allowAdding: false,
                    allowUpdating: true,
                    allowDeleting: false
                   ,useIcons: true
                },
                Selection: {
                    mode: "single"
                },
                
                 filterRow: { 
                visible: true
                    },
                onToolbarPreparing: function (e) {
                    var dataGrid = e.component;
                    e.toolbarOptions.items.unshift({
                        location: "after",
                        widget: "dxButton",
                        options: {
                            icon: "refresh",
                            onClick: function () {
                                dataGrid.refresh();
                            }
                        }
                    });
                    e.toolbarOptions.items.unshift({
                        location: "after",
                        widget: "dxButton",
                        options: {
                            text: "Generate Certs",
                            onClick: function (e) {
                           
                                $.ajax({
                                    type: "GET",
                                    url: "/api/courseregistration/generateCert/" + dataFromCell.courseScheduleID,
                                    success: function (data) {
                                        if(data == 1)
                                        {
                                            DevExpress.ui.dialog.alert("Cert generated", "Certs")
                                            dataGrid.refresh();
                                        }
                                        else
                                        {
                                            DevExpress.ui.dialog.alert("Student failed,Cert Not generated")
                                        }
                                    },
                                    error: function (data) {
                                       
                                        DevExpress.ui.dialog.alert("Certs not generated. please contact Administrator", "Error generating certs")
                                    
                                    }
                                });

                            }
                        }
                    });

                },
                onRowUpdating: function (options) {
                    $.extend(options.newData, $.extend(true, {}, options.oldData, options.newData));
                    updateurl = geturl(options.newData);
             
               
                },
                Paging: {
                    pageSize: 10
                },
                pager: {
                    showPageSizeSelector: true,
                    allowedPageSizes: [10, 25, 50, 100],
                    showInfo: true,
                    infoText: "showing page {0} of {1} ({2} )"
                },
                allowColumnReordering: true,
                allowColumnResizing: true,
                rowAlternationEnabled: true,
                hoverStateEnabled: true,

                columns: [
                    {
                        dataField: 'studentId',
                        caption: "Student ID",
                        width: 80,
                        allowEditing: false,
                        formItem: {
                            visible: false
                        }
                    },
                     {
                         dataField: 'student.person.firstName',
                         caption: "First Name",
                         allowEditing: false
                       
                     },
                      {
                          dataField: 'student.person.lastName',
                          caption: "Last Name",
                          allowEditing: false

                      },
                      {
                          dataField: 'grade.score',
                          caption: "Score"

                      }
                      ,
                     {
                         dataField: 'courseSchedule.course.pass_Fail',
                         caption: "Passing Grade",
                         allowEditing: false

                     },
                                        {
                                            dataField: "",
                                            caption: "Status",
                                            calculateCellValue: function (e) {
                                                var stat = "Failed";
                                                try { var g = e.grade.score;} catch (e) { g = null }
                                                if (g == null) { g = 0; stat = "N\A" }
                                                if (g >= parseInt(e.courseSchedule.course.pass_Fail)) {
                                                    stat = "Passed"
                                                }
                                                
                                                return stat;
                                            }
                                        },
                       {
                           name: 'Cert'
                                 , dataField: 'cert.certUrl'
                                , width: 80
                                 , caption: 'View Cert',
                           cellTemplate: function (container, options) {
                               $("<div />").dxButton({
                                   icon: 'doc',
                                   onClick: function (e) {

                                       try {
                                          
                                           window.location.href = options.data.cert.certUrl
                                       }
                                       catch (e) {

                                           DevExpress.ui.dialog.alert("No certs loaded", "Cert not found")
                                       }

                                   }
                               }).appendTo(container);
                           }
                       }

      
       ,
    
                ]
           
                ,
                scrolling: {
                    mode: "virtual"
                }

            });

          
         
       

        },
        contentTemplate: function () {
            return $("<div />").append(
             $("<div id='gradeCertGrid' > "
            )
                );
        }
    });
});



$(function () {

    $("#sigGrid").dxPopup({
        height: 350,
        width:500,
        visible: false,
        closeOnBackButton: true,
        closeOnOutsideClick: false,
        onShown: function (e) {
          
            getsignature(instructorID)
           
           

            $('#signatureGrid').jSignature();
           
            $('#signatureGrid').append("<p><button type='button' id='b' >Clear signature</button> <span></span><button type='button' id='c' >Save </button> </p>");
            try { $('#signatureGrid').jSignature("setData", "data:" + signature) }
            catch{
            }
            $("#b").click(function () {
                $('#signatureGrid').jSignature('reset');
            })

            $("#c").click(function () {
                signature = $('#signatureGrid').jSignature('getData', 'base30');
               savesignature(instructorID, signature)
                $('#signatureGrid').jSignature('reset');
                $('#signatureGrid').jSignature("setData", "data:" + signature)          
            })
        },
        onHiding: function (e) {
            $('#signatureGrid').jSignature('reset');
            $('#signatureGrid').empty();
            // Unbind remaining event handlers.
           //   $('#signatureGrid').unbind('.jSignature');
    },
        contentTemplate: function () {
            return $("<div />").append(
             $("<div id='signatureGrid' >")
                );
        }
    });
});

