﻿
// Cards
upcomingcourses();
completedclasses();
availableclasses();

function upcomingcourses() {
    $.ajax({
        type: "GET",
        url: "/api/courseschedule/upcoming/count",
        success: function (data) {
            $('#upcomingcourses').text(data)
        },
        error: function () {
            $('#upcomingcourses').text("0")
        }
    });
}

function completedclasses() {
    $.ajax({
        type: "GET",
        url: "/api/courseschedule/completed/count",
        success: function (data) {
            $('#completedclasses').text(data)
        },
        error: function () {
            $('#completedclasses').text("0")
        }
    });
}

function availableclasses() {
    $.ajax({
        type: "GET",
        url: "/api/courseschedule/available/count",
        success: function (data) {
            $('#availableclasses').text(data)
        },
        error: function () {
            $('#availableclasses').text("0")
        }
    });
}

    //store
var url = "/api/widgetCourseSchedule";

var test;

apt()
function apt() {
    $.ajax({
        type: "GET",
        url: "/api/widgetCourseSchedule",
        async:false,

        success: function (data) {
            test =  data
        },
        error: function () {
            alert("error")
        }
    });
}
  


    var dataStore = new DevExpress.data.CustomStore({
        key: "courseScheduleId",
        type: "array",
        loadMode: "raw",
        cacheRawData: false,
        load: function (loadOptions) {
            var d = test;
            return d;
        },
        byKey: function (key) {
            return $.getJSON(url + "/" + encodeURIComponent(key));
        }, insert: function (values) {
            return $.post(url, values);
        },
        add: function (values) {
            return $.ajax({
                url: url,
                method: "POST",
                data: values
            });
        },
        update: function (key, values) {
            return $.ajax({
                url: url,
                method: "PUT",
                data: values
            });
        },
        remove: function (key) {
            return $.ajax({
                url: url + "/" + encodeURIComponent(key),
                method: "DELETE",
                //reload
            });
        }
    });

    var dataSource = new DevExpress.data.DataSource({
        store: dataStore
    });

    var resourceRouteUrl = "/api/widgetCourseSchedule/resource/";

    var rooms = {
        store: new DevExpress.data.CustomStore({
            key: "id",
            loadMode: "raw",
            load: function () {
                return $.getJSON(resourceRouteUrl + "room");
            }
        }),
        sort: "text"
    }

    var courses = {
        store: new DevExpress.data.CustomStore({
            key: "id",
            loadMode: "raw",
            load: function (id) {
                return $.getJSON(resourceRouteUrl + "course?" + encodeURIComponent(id));
            }
        }),
        sort: "text"
    }

    var facilities = {
        store: new DevExpress.data.CustomStore({
            key: "id",
            loadMode: "raw",
            load: function () {
                return $.getJSON(resourceRouteUrl + "facility");
            }
        }),
        sort: "text"
    }

    var facliliesDataStore = new DevExpress.data.CustomStore({
        key: "facilityId",
        type: "array",
        loadMode: "raw",
        load: function (loadOptions) {
            var d = $.getJSON("/api/facility");
            return d;
        },
        byKey: function (key) {
            var d = new $.Deferred();
            $.get("/api/facility/" + encodeURIComponent(key))
                .done(function (dataItem) {
                    d.resolve(dataItem);
                });
            return d.promise();
        }
    });

    var roomsDataStore = new DevExpress.data.CustomStore({
        key: "roomId",
        type: "array",
        loadMode: "raw",
        load: function (loadOptions) {
            var d = $.getJSON("/api/room");
            return d;
        }
    });

    var instructors = {
        store: new DevExpress.data.CustomStore({
            key: "id",
            loadMode: "raw",
            load: function () {
                return $.getJSON(resourceRouteUrl + "instructor");
            }
        }),
        sort: "text"
    }


 

    $(function () {
        $("#scheduler").dxScheduler({
            dataSource: DevExpress.data.AspNet.createStore({
                key: "courseScheduleId",
                loadUrl: "/api/widgetCourseSchedule",
                updateUrl: "/api/WidgetCourseSchedule/updateSchedule",
                deleteUrl: "/api/WidgetCourseSchedule/DeleteSchedule",
                insertUrl: "/api/WidgetCourseSchedule/InsertSchedule" ,

                onBeforeSend: function (method, ajaxOptions, xhr) {
                    console.log(method);
                 ;
                    console.log(JSON.stringify(ajaxOptions.data));

                   // xhr.setRequestHeader("test", "tokenhere");
                    if (ajaxOptions.data.key && method == 'load')
                        ajaxOptions.url += "/" + ajaxOptions.data.key;

                    if (ajaxOptions.data.key && method == 'delete')
                        ajaxOptions.url += "/" + ajaxOptions.data.key;

                    if (ajaxOptions.data.key && method == 'update')
                        ajaxOptions.url; //+= "/" + ajaxOptions.data.key;

                    if (ajaxOptions.data.key && method == 'insert') {
                   
                        ajaxOptions.url
                    }

                  //  xhr.setRequestHeader("Authorization", "tokenhere");
                }
            }),
            views: ["day", "week", "workWeek", "month"],
            currentView: "month",
            currentDate: new Date(),
            startDayHour: 8,
            EndDayHour:19,
            height: 600,
            cellDuration: 90,
            startDateExpr: "startDate",
            endDateExpr: "endDate",
            textExpr: "text",
            descriptionExpr: "description",
            editing: {
                allowAdding: true,
                allowDeleting: true,
                allowUpdating: true
            },

            onAppointmentUpdating: function(data){
            }
        ,
           
             onAppointmentFormCreated: function (data) {
                
                var form = data.form;

                //configure the form:
                form.option(
                    "items",
                    [
                    //facility
                      {
                          label: {
                              text: "Facility"
                          },
                          editorType: "dxSelectBox",
                          dataField: "facilityId",
                          editorOptions: {
                              dataSource: facliliesDataStore,
                              placeholder: "Please select a facility...",
                              displayExpr: "facilityName",
                              valueExpr: "facilityId",
                              onValueChanged: function (args) {
                                  var roomEditor = form.getEditor("roomId");
                                  form.getEditor("roomId").instance().option("disabled", false);
                                  roomEditor.getDataSource().filter(["facilityId", "=", args.value]);
                                  roomEditor.getDataSource().load();
                              }
                              ,onContentReady: function (e) {
                                  //console.log(e);
                                  //console.log('facility length ' + e.component._options.displayValue);
                                  roomEditor = form.getEditor("roomId");
                                  if (e.component._options.displayValue == null) {
                                      form.getEditor("roomId").instance().option("disabled", true);
                                  }
                                  else {
                                      form.getEditor("roomId").instance().option("disabled", false);
                                  }
                              }
                          }, validationRules: [{
                              type: "required",
                              message: "You must choose a facility"
                          }],
                          //reload the datasource
                          onInitialized: function (e) {
                              e.component.isFirstLoadFlag = true;
                          },
                          onOpened: function (e) {
                              if (!e.component.isFirstLoadFlag)
                                  e.component.getDataSource().reload();
                              else
                                  e.component.isFirstLoadFlag = false;
                          }
                          
                      },
                    //room
                    {
                        label: {
                            text: "Room"
                        },
                        editorType: "dxSelectBox",
                        dataField: "roomId",
                        editorOptions: {
                            dataSource: roomsDataStore,
                            placeholder: "Please choose a room...",
                            displayExpr: "roomName",
                            valueExpr: "roomId",
                          //  disabled: true,
                            onContentReady: function (e) {
                                
                            }
                        }
                        , validationRules: [{
                            type: "required",
                            message: "You must choose a room"
                        }]
                        //reload the data source
                        , onInitialized: function (e) {
                            e.component.isFirstLoadFlag = true;
                        },
                        onOpened: function (e) {
                            if (!e.component.isFirstLoadFlag)
                                e.component.getDataSource().reload();
                            else
                                e.component.isFirstLoadFlag = false;
                        },
                    }
                    //course
                    ,{
                        label: {
                            text: "Course"
                        },
                        editorType: "dxSelectBox",
                        dataField: "courseId",
                        editorOptions: {
                            dataSource: courses,
                            placeholder: "Please choose a course...",
                            displayExpr: "text",
                            valueExpr: "id",
                            disabled: false,
                            onContentReady: function (e) {
                                
                            }
                        }, validationRules: [{
                            type: "required",
                            message: "A course is required"
                        }]
                        //reload the data source
                        , onInitialized: function (e) {
                            e.component.isFirstLoadFlag = true;
                        },
                        onOpened: function (e) {
                            if (!e.component.isFirstLoadFlag)
                                e.component.getDataSource().reload();
                            else
                                e.component.isFirstLoadFlag = false;
                        },
                    }
                    //Instructor
                    , {
                        label: {
                            text: "Instructor"
                        },
                        editorType: "dxSelectBox",
                        dataField: "instructorId",
                        editorOptions: {
                            dataSource: instructors,
                            placeholder: "Pick an Instructor...",
                            displayExpr: "text",
                            valueExpr: "id",
                            disabled: false,
                            onContentReady: function (e) {

                            }
                        }, validationRules: [{
                            type: "required",
                            message: "Please select a ZionPortal resource"
                        }]
                        //reload the data source
                        , onInitialized: function (e) {
                            e.component.isFirstLoadFlag = true;
                        },
                        onOpened: function (e) {
                            if (!e.component.isFirstLoadFlag)
                                e.component.getDataSource().reload();
                            else
                                e.component.isFirstLoadFlag = false;
                        },
                    }
                    //starttime
                    ,{
                        dataField: "startDate",
                        editorType: "dxDateBox",
                        editorOptions: {
                            width: "100%",
                            type: "datetime"
                        }
                    }
                    ,{
                        dataField: "endDate",
                        editorType: "dxDateBox",
                        editorOptions: {
                            width: "100%",
                            type: "datetime"
                        }
                    }

                    ]);

            }
        })


    });

    //function timeDifference(start, end) {

    //    var firstDate = new Date(start);
    //    var secondDate = new Date(end);
    //    var oneWeek = 7 * 24 * 60 * 60 * 1000;
    //    var oneDay = 24 * 60 * 60 * 1000;
    //    var oneHour = 60 * 60 * 1000;
    //    var oneMinute = 60 * 1000;
    //    var oneSecond = 1000;

    //    var diffWeeks = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneWeek)));
    //    var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
    //    var diffHours = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneHour)));
    //    var diffMinutes = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneMinute)));
    //    var diffSeconds = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneSecond)));

    //    if (diffWeeks !== 0) {
    //        return diffWeeks + (diffWeeks === 1 ? ' week' : ' weeks');
    //    }
    //    if (diffDays !== 0) {
    //        return diffDays + (diffDays === 1 ? ' day' : ' days');
    //    }
    //    if (diffHours !== 0) {
    //        return diffHours + (diffHours === 1 ? ' hour' : ' hours');
    //    }
    //    if (diffMinutes !== 0) {
    //        return diffMinutes + (diffMinutes === 1 ? ' minute' : ' minutes');
    //    }
    //    if (diffSeconds !== 0) {
    //        return diffSeconds + (diffSeconds === 1 ? ' second' : ' seconds');
    //    }

    //}


    //function get_time_duration(earlierDate, laterDate) {
    //    var start = new Date(earlierDate);
    //    var stop = new Date(laterDate);
    //    var oDiff = new Object();


    //    //  Calculate Differences
    //    //  -------------------------------------------------------------------  //
    //    var nTotalDiff = stop.getTime() - start.getTime();

    //    oDiff.days = Math.floor(nTotalDiff / 1000 / 60 / 60 / 24);
    //    nTotalDiff -= oDiff.days * 1000 * 60 * 60 * 24;

    //    oDiff.hours = Math.floor(nTotalDiff / 1000 / 60 / 60);
    //    nTotalDiff -= oDiff.hours * 1000 * 60 * 60;

    //    oDiff.minutes = Math.floor(nTotalDiff / 1000 / 60);
    //    nTotalDiff -= oDiff.minutes * 1000 * 60;

    //    oDiff.seconds = Math.floor(nTotalDiff / 1000);
    //    //  -------------------------------------------------------------------  //

    //    //  Format Duration
    //    //  -------------------------------------------------------------------  //
    //    //  Format Hours
    //    var hourtext = '00';
    //    if (oDiff.days > 0) { hourtext = String(oDiff.days); }
    //    if (hourtext.length == 1) { hourtext = '0' + hourtext };

    //    //  Format Minutes
    //    var mintext = '00';
    //    if (oDiff.minutes > 0) { mintext = String(oDiff.minutes); }
    //    if (mintext.length == 1) { mintext = '0' + mintext };

    //    //  Format Seconds
    //    var sectext = '00';
    //    if (oDiff.seconds > 0) { sectext = String(oDiff.seconds); }
    //    if (sectext.length == 1) { sectext = '0' + sectext };

    //    //  Set Duration
    //    var sDuration = hourtext + ':' + mintext + ':' + sectext;
    //    oDiff.duration = sDuration;
    //    //  -------------------------------------------------------------------  //

    //    return oDiff;
    //}