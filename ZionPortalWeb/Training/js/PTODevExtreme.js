﻿//////////////////////////    PTO Request


//alert(sessionStorage.getItem("employeeid"));

var accrualtype = [{
    PTOtypeId: "1"
    ,Text:"Sick Leave - Balance(28 hours)"
},
    {
        PTOtypeId: "2"
        , Text: "Vacation - Balance(38 hours)"
    },
    {
        PTOtypeId: "3"
        , Text: "Beareavement - Balance(98 hours)"
    }
];


var ptoBalance 

getptoBalance();

function getptoBalance() {
    $.ajax({
        type: "GET",
        url: "/api/ptoBalanceByEmployeeID/" + sessionStorage.getItem("employeeid"),
        async: false,
        success: function (data) {
            ptoBalance = data;
        },
        error: function () {
            alert("Error Occured")
        }
    });
}

//ptoBalance = [{ "ptoBalanceID": 4, "employeeID": 1, "id": 3, "ptoHoursRemaning": 14, "ptoBeginningBalance": 34, "ptoType": "Bereavement", "text": "Bereavement - 14" }, { "ptoBalanceID": 5, "employeeID": 1, "id": 2, "ptoHoursRemaning": 146, "ptoBeginningBalance": 340, "ptoType": "Sick Leave", "text": "Sick Leave - 146" }, { "ptoBalanceID": 6, "employeeID": 1, "id": 1, "ptoHoursRemaning": 34, "ptoBeginningBalance": 0, "ptoType": "Vacation", "text": "Vacation - 34" }]

    $(function () {
        $("#scheduler").dxScheduler({
           // dataSource: data,
            dataSource: DevExpress.data.AspNet.createStore({
                key: "ptoRequestId",
                loadUrl: "/api/ptoRequestByEmpId/" + sessionStorage.getItem("employeeid"),
                updateUrl: "/api/UpdateRequestPto",
                insertUrl: "/api/RequestNewPto" ,

                onBeforeSend: function (method, ajaxOptions, xhr) {
                    console.log(method);
                 ;
                    console.log(JSON.stringify(ajaxOptions.data));

                   // xhr.setRequestHeader("test", "tokenhere");
                    if (ajaxOptions.data.key && method === 'load')
                        ajaxOptions.url  ;

                    if (ajaxOptions.data.key && method === 'delete')
                        ajaxOptions.url += "/" + ajaxOptions.data.key;

                    if (ajaxOptions.data.key && method === 'update')
                        ajaxOptions.url; //+= "/" + ajaxOptions.data.key;

                    if (ajaxOptions.data.key && method === 'insert') {
                   
                        ajaxOptions.url
                    }

                  //  xhr.setRequestHeader("Authorization", "tokenhere");
                }
            }),
            views: ["day", "week", "workWeek", "month"],
            currentView: "month",
            currentDate: new Date(),
            height: 600,
            startDateExpr: "startDate",
            endDateExpr: "endDate",
            textExpr: "text",
            descriptionExpr: "text",
            editing: {
                allowAdding: true,
                allowDeleting: false,
                allowUpdating: true
            }
            ,

            onAppointmentUpdating: function(data){
            }
        ,
           
            onAppointmentFormCreated: function (data) {

                var form = data.form;

             
                
               

               // configure the form:
                form.option(
                    "items",
                    [
                  
                    //Acrrual Type
                     {
                        label: {
                            text: "Accrual Type"
                        },
                        editorType: "dxSelectBox",
                            dataField: "pTOtypeId",
                        editorOptions: {
                            dataSource: ptoBalance,
                            placeholder: "Pick an accrual Type...",
                            displayExpr: "text",
                            valueExpr: "id",
                            disabled: false,
                            onContentReady: function (e) {
                               

                            }
                            ,
                            onValueChanged: function (e) {
                            
                            }
                        }, validationRules: [{
                            type: "required",
                            message: "Please select an accrual Type"
                        }]
                        //reload the data source
                        , onInitialized: function (e) {
                            e.component.isFirstLoadFlag = true;
                        },
                        onOpened: function (e) {
                            if (!e.component.isFirstLoadFlag)
                                e.component.getDataSource().reload();
                            else
                                e.component.isFirstLoadFlag = false;
                            }
                        }

                   
                    //starttime
                    ,{
                        dataField: "startDate",
                        editorType: "dxDateBox",
                        editorOptions: {
                            width: "100%",
                            type: "datetime"
                        }
                    }
                        , {
                            label: {
                                text: "Notes",
                                visible:false

                            },
                            dataField: "endDate",
                            editorType: "dxDateBox",
                            editorOptions: {
                                width: "100%",
                                type: "datetime",
                                visible: false
                            }
                        }
                        , {
                            dataField: "ptoHrRequested",
                            label: {
                                text: "Hours Requested"
                            },
                            editorType: "dxTextBox",
                            editorOptions: {
                                width: "30%"

                            }
                        }
                            , {
                            dataField: "notes",
                            label: {
                                text: "Notes"
                            },
                            placeHolder:"Enter notes (Optional)",
                            editorType: "dxTextArea",
                            editorOptions: {
                                width: "100%",height:"80px"

                            }
                        }
                        , {
                            dataField: "employeeID",
                            label: {
                                text: "empID",
                                visible:false
                            },
                            editorType: "dxTextBox",
                            editorOptions: {
                                width: "100%",
                                value: sessionStorage.getItem("employeeid"),
                                visible: false
                                


                            }
                        }
                        , {
                            dataField: "ptoRequestId",
                            label: {
                                text: "RequestID",
                                visible: false
                            },
                            editorType: "dxTextBox",
                            editorOptions: {
                                width: "100%",
                                 visible: false



                            }
                        }
                        , {
                            dataField: "ptoRequestNotesID",
                            label: {
                                text: "NoteRequestID",
                                visible: false
                            },
                            editorType: "dxTextBox",
                            editorOptions: {
                                width: "100%", visible: false
                             



                            }
                        }
                        
                       

                    

                    ]);

            }
        })


});

/////////////////////// PTO Balance



$(function () {

    $("#viewAccrualsGrid").dxDataGrid({
        dataSource: ptoBalance,
        width:700,
        Selection: {
            mode: "row"
        },
        onToolbarPreparing: function (e) {
            var dataGrid = e.component;
        
        },
        onRowUpdating: function (options) {
            $.extend(options.newData, $.extend(true, {}, options.oldData, options.newData));
        },
       
        allowColumnReordering: false,
        allowColumnResizing: true,
        rowAlternationEnabled: true,
        hoverStateEnabled: true,

        showColumnLines: true,
        showRowLines: false,
        rowAlternationEnabled: true,
        showBorders: true,
        columns: [
            {
                dataField: "ptoBalanceID",
                caption: "ID",
                width: 50
            }
            ,
            {
                dataField: "ptoType",
                caption: "Accrual Type",
                width: 250
            }
            , {
                dataField: "ptoBeginningBalance",
                caption: "Beginning Balance",
                alignment: 'center',
                width: 150

            }
            , {
                dataField: "ptoHoursRemaning",
                caption: "Hours Remaining",
                alignment: 'center',
                width: 150

            }

        


        ],
        filterRow: {
            visible: false
        },
        groupPanel: {
            visible: false
        },
        scrolling: {
            mode: "virtual"
        },
    

      
    });
});

/////////////////////// Approve PTO


//// Approval
var statuses = ["All", "Approved", "Rejected", "Pending Approval"];
var statuses2 = [{ "Approval Status": "Rejected" }, { "Approval Status": "Approved" }, { "Approval Status": "Pending Approval" }];

var appurl = "/api/getEmpRequestByMagId/" + sessionStorage.getItem("employeeid")

var dataStoreApp = new DevExpress.data.CustomStore({
    key: "id",
    type: "array",
    loadMode: "raw",
    cacheRawData: false,

    load: function (loadOptions) {
        var d = $.getJSON(appurl);
        return d;
    },
    byKey: function (key) {
        return $.getJSON("/api/time/employeesPerManager/" + encodeURIComponent(key));
    }
    , insert: function (values) {
        return $.post(viewtimeurl, values);
    },
    update: function (key, values) {
        return $.ajax({
            url: "/api/ptoApproval",
            method: "PUT",
            data: values
        });
    },
});

var dataSourceApp = new DevExpress.data.DataSource({
    store: dataStoreApp

});


// Approval main grid


var statuses = ["All", "Approved", "Rejected", "Pending Approval"];
var statuses2 = [{ "Approval Status": "Rejected" }, { "Approval Status": "Approved" }, { "Approval Status": "Pending Approval" }];



$(function () {

    var approvalTimesheetGrid = $("#approvalTimesheetGrid").dxDataGrid({
        dataSource: dataSourceApp,
        editing: {
            mode: "form",
            allowAdding: false,
            allowUpdating: true,
            allowDeleting: false,
            useIcons: true
        },
        Selection: {
            mode: "single"
        },
        grouping: {
            autoExpandAll: false,
        },
        onContentReady: function (e) {
            e.component.expandRow(e.component.getKeyByRowIndex(0));
            e.component.expandRow(e.component.getKeyByRowIndex(1));
        },
        onToolbarPreparing: function (e) {
            var dataGrid = e.component;
            e.toolbarOptions.items.unshift({
                location: "after",
                widget: "dxButton",
                options: {
                    icon: "refresh",
                    onClick: function () {
                        dataGrid.refresh();
                    }
                }
            })
        },
        onRowUpdating: function (options) {
            $.extend(options.newData, $.extend(true, {}, options.oldData, options.newData));
        }

        ,
        Paging: {
            pageSize: 10
        },
        pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 25, 50, 100],
            showInfo: true,
            infoText: "showing page {0} of {1} ({2} Time entries)"
        },
        allowColumnReordering: true,
        allowColumnResizing: true,
        rowAlternationEnabled: true,
        hoverStateEnabled: true,

        //onCellPrepared: function (e) {
        //    if (e.rowType === "data" && e.column.command === "edit") {
        //        var $links = e.cellElement.find(".dx-link");
        //        if (e.row.data.workType === "PTO")
        //            $links.filter(".dx-link-edit").remove();

        //    }
        //},


        columns: [

            {
                dataField: "employeeName",
                caption: "Employee",
                width: 150
                , allowedit: false,
                allowEditing: false,
                groupIndex: 1,
                sortOrder: "asc",
                sortIndex: 1,
            }
            ,
            {
                dataField: "isoWeekOfYear",
                caption: "week",
                width: 100,
                groupIndex: 0,
                sortOrder: "desc",
                sortIndex: 0,
                allowedit: false,
                allowEditing: false
            }
            ,
            {
                dataField: "weekPeriod",
                caption: "Period",
                width: 100,
                allowedit: false,
                allowEditing: false
            }
            , {
                dataField: "clockDate",
                dataType: "date",
                caption: "Requested Date",
                width: 140,
                alignment: 'center',
                allowedit: false,
                allowEditing: false

            }
            , {
                dataField: "accrualType",
                caption: "Accrual Type",
                width: 120,
                alignment: 'center',
                allowedit: false,
                allowEditing: false
            }
            , {
                dataField: "ptoHoursRemaning",
                caption: "PTO Balance",
                width: 140,
                alignment: 'center',
                allowedit: false,
                allowEditing: false
            }
            , {
                dataField: "requestedHours",
                caption: "Requested Hours",
                width: 140,
                alignment: 'center',
                allowedit: false,
                allowEditing: false


            }
            , {
                dataField: "approvalStatus",
                caption: "Approval Status",
                alignment: 'center',
                width: 150,
                lookup: {
                    dataSource: statuses2,
                    valueExpr: "Approval Status",
                    displayExpr: "Approval Status"
                }
            }
            ,
            {
                dataField: "ptoNotes",
                visible: false,
                formItem: {
                    visible: true
                },
                caption: "Approver Notes"
            }

            , {
                width: 120,
                visible: true,
                formItem: {
                    visible: false
                },
                caption: "Approver Notes",
                alignment: 'center',
                allowedit: true,
                allowEditing: true,
                cellTemplate: function (container, options) {
                    $('<a/>').addClass('dx-link')
                        .text('View')
                        .on('dxclick', function () {
                            $("#approvalnotepopup").dxPopup("instance").show(options.data);
                            $("#approvalnotetxt").dxTextArea("instance").option("value",
                                function () {
                                    if (options.data.ptoNotes !== null) {
                                        return "Note: " + options.data.ptoNotes
                                    }
                                    else return "No notes entered"
                                }

                            );
                            $("#approvalnoteby").dxTextBox("instance").option("value",
                                function () {
                                    if (options.data.noteEnteredByEmployeeName !== null) {
                                        return " *** Notes entered by " + options.data.noteEnteredByEmployeeName + " on " + options.data.noteEntryDate + "***"
                                    }

                                });



                        }).appendTo(container);
                }
            }
         

        ],
        filterRow: {
            visible: true
        },
        groupPanel: {
            visible: true
        },
        scrolling: {
            mode: "virtual"
        },
        grouping: {
            autoExpandAll: false,
        },
        onContentReady: function (e) {
            e.component.expandRow(e.component.getKeyByRowIndex(0));
            e.component.expandRow(e.component.getKeyByRowIndex(1));
        },

        sortByGroupSummaryInfo: [{
            summaryItem: "sum"
        }]
        ,
        summary: {
            groupItems: [{
                column: "PTO Balance",
                name:"PTOBalance",
                summaryType: "sum",
                displayFormat: "{0} hours",
            }
                , {
                    column: "Requested Hours",
                    summaryType: "sum",
                    displayFormat: "{0} requested hours",


                }
                , {
                    name: "AvailableBalance",
                summaryType: "custom",
                displayFormat: "{0} available hours",


            }

             
            ]
            , calculateCustomSummary: function (options) {
                // Calculating "customSummary1"
                if (options.name === "AvailableBalance") {
                    switch (options.summaryProcess) {
                        case "start":
                            options.totalValue = 0;
                            break;
                        case "calculate":
                            {
                                options.totalValue = (options.totalValue + options.value.hoursWorked - options.value.adjustedHours);
                            }
                            break;
                        case "finalize": 
                        {
                                options.totalValue = options.totalValue
                        }

                    }
                }
            }
        }


    }).dxDataGrid("instance");
    $("#selectStatus").dxSelectBox({
        dataSource: statuses,
        value: statuses[3],
        onInitialized: function (data) { approvalTimesheetGrid.filter(["Approval Status", "=", "Pending Approval"]) },
        onValueChanged: function (data) {
            if (data.value === "All")
                approvalTimesheetGrid.clearFilter();
            else
                approvalTimesheetGrid.filter(["Approval Status", "=", data.value]);
        }
    });
});


$("#approvalnotepopup").dxPopup({
    height: 500,
    width: 600,
    visible: false,
    closeOnBackButton: false,
    closeOnOutsideClick: true,
    showTitle: true,

    title: 'Approver Notes',
    contentTemplate: function () {
        return $("<div />").append(
            $("<div id='approvalnotetxt' />")
                .dxTextArea({
                    value: "some text",
                    //readOnly: true
                })).append(
                    $("<div id='approvalnoteby' />")
                        .dxTextBox({
                            value: "some text",
                            readOnly: true
                        })
                );

    }
});