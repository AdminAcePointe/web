﻿//////////////////////////    Member form
var isclicked = 0;
var Member
var ddata
var page = sessionStorage.getItem("page")
var GroupHomes_
var MemberStatus_
var states
var Genders
var relationships
var MaritalStatuses
var MemberServiceInfo;
var MemberEmergencyContacts;
var MemberServices;
var serviceTypeWithServices;
var employeeList;
var propic;


var memberuser = sessionStorage.getItem("memberUser")
isclicked = sessionStorage.getItem("isclicked")

//alert(isclicked)


if (memberuser == null && isclicked == 0 && page == "MemberProfile") {
    location.href = '/Member/ManageMember'

}

sessionStorage.setItem("isclicked", 0)


if (isclicked == 2) {
    memberuser = -1
    sessionStorage.setItem("memberUser", null)
}






if (memberuser != null) {
 
        getMemberDetails();
        getMemberServiceList();
        getMemberEmergencyContactList();

}


getServiceTypeConcatService();
getEmployeeList();
getMemberStatusList();
getGroupHomeList();
getStates()
getGender()
getRelationships()
getMaritalStatuses()



var role = sessionStorage.getItem("userrole");

$(function () {
    $("#form").dxForm({
        formData: Member,
        readOnly: function () {
            if (isclicked == 2) {

                return false
            }
            else return true
        },
        showColonAfterLabel: false,
        labelLocation: "left",
        onContentReady: function () { newempenableform() },
        items: [
            {

                itemType: "group",
                cssClass: "first-group",
                colCount: 3,
                items: [


                    {
                        itemType: "group",
                        items: [
                            {
                                itemType: "group",
                                items: [
                                    {
                                        label: {
                                            text: "personPhoto",
                                            visible: false
                                        },

                                        dataField: "personPhoto",
                                        template: function (data, itemElement) {
                                            $('<img class="form-avatar" id="propic">').attr("src", data.editorOptions.value).appendTo(itemElement);
                                        }
                                    }
                                    ,
                                    {

                                        label: {
                                            text: "personPhoto",
                                            visible: false
                                        },
                                        dataField: "",
                                        template: function (data, itemElement) {
                                            itemElement.append($("<div>").attr("id", "dxfu1").dxFileUploader({
                                                labelText: "",
                                                selectButtonText: "Add/ Change Photo",
                                                showFileList: false,
                                                accept: "image/*",
                                                uploadMode: "instantly",
                                                value: [],
                                                multiple: false,
                                                uploadUrl: "/api/util/SaveProfilePhoto",
                                                onValueChanged: function (e) {
                                                    var files = e.value;

                                                    $.each(files, function (i, file) {
                                                        propic = "../img/ProfilePhoto/" + file.name
                                                        var fileURL = URL.createObjectURL(file);
                                                        $("#propic").attr("src", fileURL)
                                                    })



                                                }
                                            }));
                                        }
                                    }


                                ]
                            }    
                        ]
                    }

                    , {
                        itemType: "group",
                        colSpan: 3,
                        items: [{
                            dataField: "firstName"
                            , validationRules: [{
                                type: "required",
                                message: "First Name is required"
                            }, {
                                type: "pattern",
                                pattern: "^[a-zA-Z]+$",
                                message: "The name should not contain digits"
                            }]
                        }, {
                            dataField: "lastName"
                            , validationRules: [{
                                type: "required",
                                message: "First Name is required"
                            }, {
                                type: "pattern",
                                pattern: "^[a-zA-Z]+$",
                                message: "The name should not contain digits"
                            }]
                        }
                            , {

                            dataField: "dateOfBirth",
                            editorType: "dxDateBox",


                            editorOptions: {
                                width: "100%",
                                displayFormat: 'MM-dd-yyyy'
                            }
                        }
                        ]
                    }
                    , {
                        itemType: "group",
                        colSpan: 3,
                        items: [{
                            dataField: "maritalStatus",
                            editorType: "dxSelectBox",
                            editorOptions: {
                                items: MaritalStatuses
                            }
                        }, {
                            dataField: "gender",
                            editorType: "dxSelectBox",
                            editorOptions: {
                                items: Genders
                            }
                        }
                            , {
                            label: {
                                text: "Assist ID"
                            },
                                dataField: "assistid"
                        }]
                    }
                ]
            }
            , {
                itemType: "group",
                cssClass: "second-group",
                caption: "Personal Information",
                colCount: 2,
                items: [{
                    itemType: "group",
                    items: [{
                        label: {
                            text: "Address"
                        },
                        dataField: "addressline1"
                        , validationRules: [{
                            type: "required",
                            message: "Please enter address"
                        }]
                    }, {
                        dataField: "city"
                        , validationRules: [{
                            type: "required",
                            message: "Please enter city"
                        }]
                    }, {
                        dataField: "email",
                        validationRules: [

                            {
                                type: 'email'
                            }, {
                                type: 'required',
                                message: 'Please enter valid email'
                            }]

                    }]
                }
                    , {
                    itemType: "group",
                    items: [{
                        dataField: "state",
                        editorType: "dxSelectBox",
                        editorOptions: {
                            items: states
                        },

                        validationRules: [{
                            type: 'required',
                            message: 'Please select state'
                        }]
                    }, {
                        dataField: "zipCode"
                        ,
                        validationRules: [

                            {
                                type: "stringLength",
                                min: 5,
                                max: 5,
                                message: "Zip should contain 5 digits"
                            }
                            , {
                                type: 'required',
                                message: 'Please enter valid zip code'
                            }]
                    }, {
                        dataField: "phoneNumber",
                        label: {
                            text: "Phone"
                        },
                        editorOptions: {
                            mask: "+1 (000) 000-0000"
                        }

                    }]
                }
                ]
            }


            , {
                itemType: "group",
                cssClass: "second-group",
                caption: "Related Information",
                colCount: 3,
                items: [
                    {
                        itemType: "group",
                        items: [{
                                dataField: "groupHome",
                                editorType: "dxSelectBox",
                                editorOptions: {
                                    items: GroupHomes_
                                },
                                validationRules: [{
                                    type: 'required',
                                    message: 'Please choose group home'
                                }]
                            }




                        ]
                    } 
                    , {
                        itemType: "group",
                        items: [{
                            dataField: "ahcccsid",
                            label: { text: "AHCCCS ID" },
                            editorType: "dxTextBox",
                            
                            validationRules: [{
                                type: 'required',
                                message: 'Please enter AHCCCS ID'
                            }]
                        }

                        ]
                    }
                    , {
                        itemType: "group",
                        items: [{
                            dataField: "memberStatus",
                            editorType: "dxSelectBox",
                            editorOptions: {
                                items: MemberStatus_
                            },
                            validationRules: [{
                                type: 'required',
                                message: 'Please choose Status'
                            }]
                        }

                        ]
                    } 

                ]
            }
            , {
                itemType: "group",
                cssClass: "second-group",
                items: [

                    {
                        itemType: "group",
                        items: [{
                            itemType: "tabbed",
                            tabPanelOptions: {
                                deferRendering: false
                            },
                            tabs: [
                                {
                                   Caption:"Services",
                                    title: "Assigned Services",
                                    label: "",
                                    colCount: 1,
                                    items: [
                                        {
                                            dataField: "serviceGrid",
                                            


                                            disabled:false,
                                            template: function (data, itemElement) {
                                                itemElement.append("<div id='serviceGrid'>")
                                                    .dxDataGrid({
                                                        dataSource: MemberServices,
                                                    
                                                        editing: {

                                                            allowAdding: true,
                                                            allowUpdating: true,
                                                            allowDeleting: true,
                                                            useIcons: true
                                                        },

                                                        width: "100%",
                                                        onEditorPreparing: function (e) {

                                                        },
                                                        Selection: {
                                                            mode: "single"
                                                        },
                                                        onToolbarPreparing: function (e) {
                                                            var dataGrid = e.component;


                                                        },
                                                        onContentReady: function (e) {
                                                           // e.component.option.disabled = true
                                                        },
                                                        onRowUpdating: function (options) {
                                                            $.extend(options.newData, $.extend(true, {}, options.oldData, options.newData));
                                                        },
                                                        Paging: {
                                                            pageSize: 5
                                                        },
                                                        pager: {
                                                            showPageSizeSelector: true,
                                                            allowedPageSizes: [5, 25, 50, 100],
                                                            showInfo: true,
                                                            infoText: "showing page {0} of {1} ({2} servicess)"
                                                        },
                                                        allowColumnReordering: false,
                                                        allowColumnResizing: false,
                                                        rowAlternationEnabled: false,
                                                        hoverStateEnabled: true,
                                                        showColumnLines: true,
                                                        showRowLines: false,
                                                        showBorders: true,

                                                        columns: [
                                                            {
                                                                dataField: "memberServiceId",
                                                                caption: "ID",
                                                                allowedit: false,
                                                                width: "20%",
                                                                allowEditing: false,
                                                            }
                                                           

                                                            , {
                                                                dataField: "memberServiceId",
                                                                caption: "Service",
                                                                width: "40%",
                                                                validationRules: [{
                                                                    type: "required"
                                                                }],
                                                                lookup: {
                                                                    dataSource: serviceTypeWithServices,

                                                                    valueExpr: "memberServiceID",
                                                                    displayExpr: "serviceTypeWithService"
                                                                }

                                                            },

                                                            {
                                                                dataField: "empID",
                                                                caption: "Assigned Employee",
                                                                width: "40%",
                                                                validationRules: [{
                                                                    type: "required"
                                                                }],
                                                                lookup: {
                                                                    dataSource: employeeList,

                                                                    valueExpr: "empID",
                                                                    displayExpr: "employeeName"
                                                                }
                                                            }
                                                        ],
                                                        filterRow: {
                                                            visible: false
                                                        },
                                                        groupPanel: {
                                                            visible: false
                                                        },
                                                        scrolling: {
                                                            mode: "fixed"
                                                        },


                                                    })
                                            }
                                        }

                                    ]
                                }
                                ,
                                {
                                    colCount: 1,
                                    title: "Contacts",
                                    Caption: "Contacts",
                                    items: [
                                        {
                                            dataField: "contactGrid",
                                            disabled: false,
                                            template: function (data, itemElement) {
                                                itemElement.append("<div id='contactGrid'>")
                                                    .dxDataGrid({
                                                        dataSource: MemberEmergencyContacts,

                                                        editing: {

                                                            allowAdding: true,
                                                            allowUpdating: true,
                                                            allowDeleting: true,
                                                            useIcons: true
                                                        },

                                                        width: "100%",
                                                        onEditorPreparing: function (e) {

                                                        },
                                                        Selection: {
                                                            mode: "single"
                                                        },
                                                        onToolbarPreparing: function (e) {
                                                            var dataGrid = e.component;


                                                        },
                                                        onContentReady: function (e) {
                                                            e.component.option.disabled = true
                                                        },
                                                        onRowUpdating: function (options) {
                                                            $.extend(options.newData, $.extend(true, {}, options.oldData, options.newData));
                                                        },
                                                        Paging: {
                                                            pageSize: 5
                                                        },
                                                        pager: {
                                                            showPageSizeSelector: true,
                                                            allowedPageSizes: [5, 25, 50, 100],
                                                            showInfo: true,
                                                            infoText: "showing page {0} of {1} ({2} contacts)"
                                                        },
                                                        allowColumnReordering: false,
                                                        allowColumnResizing: false,
                                                        rowAlternationEnabled: false,
                                                        hoverStateEnabled: true,
                                                        showColumnLines: true,
                                                        showRowLines: false,
                                                        showBorders: true,

                                                        columns: [
                                                            {

                                                                dataField: "firstName"
                                                                , validationRules: [{
                                                                    type: 'required',
                                                                    width: "20%",
                                                                    message: 'Please enter contact first name'
                                                                }]


                                                            }
                                                            ,
                                                            {

                                                                dataField: "lastName"
                                                                , validationRules: [{
                                                                    type: 'required',
                                                                    width: "20%",
                                                                    message: 'Please enter contact last name'
                                                                }]



                                                            }


                                                            ,
                                                            {

                                                                dataField: "phone"

                                                                ,
                                                                editorOptions: {
                                                                    mask: "+1 (000) 000-0000"
                                                                }
                                                                ,
                                                                width: "20%",
                                                                validationRules: [{
                                                                    type: 'required',
                                                                    message: 'Please enter phone'
                                                                }]


                                                            }
                                                            ,
                                                            {

                                                                dataField: "email"
                                                                ,
                                                                width: "20%",
                                                                validationRules: [
                                                                    {
                                                                        type: "email"
                                                                    }
                                                                    ,
                                                                    
                                                                    {
                                                                        type: 'required',
                                                                        message: 'Please enter contact first name'
                                                                    }]



                                                            }
                                                            ,
                                                            {
                                                                dataField: "relationship",
                                                                editorType: "dxSelectBox",
                                                                width: "20%",
                                                                editorOptions: {
                                                                    items: relationships
                                                                }
                                                                , validationRules: [{
                                                                    type: 'required',
                                                                    message: 'Please choose relationship type'
                                                                }], lookup: {
                                                                    dataSource: relationships,

                                                                    valueExpr: "",
                                                                    displayExpr: ""
                                                                }


                                                            }


                                                          
                                                        ],
                                                        filterRow: {
                                                            visible: false
                                                        },
                                                        groupPanel: {
                                                            visible: false
                                                        },
                                                        scrolling: {
                                                            mode: "fixed"
                                                        },


                                                    })
                                            }
                                        }

                                    ]
                                }
                                //,
                                //{
                                //    title: "Reports",
                                //    colCount: 4,
                                //    items: [

                                //        {
                                //            width: "20%",
                                         
                                //            editorType: "dxButton",
                                //            editorOptions: {
                                //                text: "Activity Log",
                                //                type: "success" 
                                             
                                //            }
                                //        }
                                //        ,{
                                //            width: "20%",

                                //            editorType: "dxButton",
                                //            editorOptions: {
                                //                text: "Medication Log"
                                //                ,
                                //                type: "success" 

                                //            }
                                //        }
                                //       , {
                                //            width: "20%",

                                //            editorType: "dxButton",
                                //            editorOptions: {
                                //                text: "Incident Report"
                                //                ,
                                //                type: "success" 
                                //            }
                                //        }
                                //        , {
                                //            width: "20%",

                                //            editorType: "dxButton",
                                //            editorOptions: {
                                //                text: "Behavioral Report"
                                //                ,
                                //                stylingMode: "text"
                                //                , type: "success" 
                                //            }
                                //        }
                                        

                                //    ]
                                //}
                            ]
                        }]
                    }


                ]
            }
        ]
    });
});
// buttons 

try {
    //form.itemOption("Contacts.contactGrid", "caption", "");
    //form.itemOption("Services.serviceGrid", "caption", "");
    // edit button
    $("#btn").dxButton({
        text: function () {

            if (isclicked == 2) {
                return "Save Profile"
            }
            else return "Edit Profile"
        },
        type: "success",
        onClick: function (e) {
            var t = $("#btn").dxButton("instance");
            var text = $("#btn").text();
            var form = $("#form").dxForm("instance");

            form.option("readOnly", false);
            form.itemOption("Contacts.contactGrid", "disabled", false);
            form.itemOption("Services.serviceGrid", "disabled", false);


            if (text == 'Edit Profile') {
                disableformelements(form)

                ddata = form.option('formData');

                t.option("text", "Save Profile");
            }
            else if (text == 'Save Profile') {

                var result = form.validate();

                ddata = form.option('formData');

                if (result.isValid) {
                    postMemberDetails();
                }
                else {
                    DevExpress.ui.notify({
                        message: "please fill out all fields"
                        , position: "{my:'center', at:'center'}"
                        , type: "error"
                        , displayTime: 5000
                    });
                }




            }
            // form.option("readOnly", !form.option("readOnly"));

        },

    });

    if (role == "Administrator") {
        $("#btn").dxButton("instance").option("visible", true)
    }
    else {
        $("#btn").dxButton("instance").option("visible", false)
    }
    // Report button
    $("#reportbtn").dxButton({
        text: "Reports",
        type: "default",
        onClick: function (e) {
             location.href = "/Member/MemberReport"
        },

    });
}
catch{
}



var url = "/api/Member/GetAllMembers/";
var dataStore = new DevExpress.data.CustomStore({
    key: "memberID",
    type: "array",
    loadMode: "raw",
    cacheRawData: false,

    load: function (loadOptions) {
        var d = $.getJSON(url);
        return d;
    },
    byKey: function (key) {
        return $.getJSON(url + "/" + encodeURIComponent(key));

    }
});

var dataSource = new DevExpress.data.DataSource({
    store: dataStore

});

//--------------------------Functions ------------------------------------------

function newempenableform() {

    if (role == 'Administrator' && isclicked == 2) {

      
        form2 = $("#form").dxForm("instance");


        form2.option("readOnly", false);

        var status = false

        form2.getEditor("firstName").option("disabled", status);
        form2.getEditor("lastName").option("disabled", status);
        form2.getEditor("maritalStatus").option("disabled", status);
        form2.getEditor("gender").option("disabled", status);
        form2.getEditor("dateOfBirth").option("disabled", status);
        form2.getEditor("groupHome").option("disabled", status);
        form.itemOption("Services.serviceGrid", "disabled", false);
        form.itemOption("Contacts.contactGrid", "disabled", false);

    }


}

function enablegrid() {
    if (isclicked == 0) {
        form = $("#form").dxForm("instance");
        form.itemOption("Services.serviceGrid", "disabled", false);
        form.itemOption("Contacts.contactGrid", "disabled", false);
    }
}

function disableformelements(form) {
  
    if (role != "Administrator") {
        form.itemOption("Services.serviceGrid", "disabled", true);
        form.itemOption("Contacts.contactGrid", "disabled", true);
        form.getEditor("firstName").option("disabled", true);
        form.getEditor("lastName").option("disabled", true);
        form.getEditor("maritalStatus").option("disabled", true);
        form.getEditor("gender").option("disabled", true);
        form.getEditor("dateOfBirth").option("disabled", true);
        form.getEditor("accessID").option("disabled", true);
        form.getEditor("ssn").option("disabled", true);
        form.getEditor("groupHome").option("disabled", true);
        form.getEditor("memberStatus").option("disabled", true);
    }

}




function postMemberDetails() {
    var form = $("#form").dxForm("instance");
    var t = $("#btn").dxButton("instance");
    console.log(ddata);
    $.ajax({
        type: "POST",
        data: {
            'ddata': ddata, 'vw_ServiceDetails': MemberServices, 'PersonEmergencyContactDto': MemberEmergencyContacts, 'UpdatedProfilePic': propic
        },
        url: "/api/Member/AddNewMember/" + memberuser,
        async: false,
        success: function (data) {
            DevExpress.ui.dialog.alert("Member profile saved successfully!!!", "Update", true);
            form.option("readOnly", true);
            t.option("text", "Edit Profile");
         

        },
        error: function () {
            DevExpress.ui.dialog.alert("Error occured updating Member profile data. Please try again", "Error")

            t.option("text", "Save Profile");
            form.option("readOnly", false);
        }
    });
}

$(function () {

    $("#memberGrid").dxDataGrid({
        dataSource: dataSource,
        editing: {
            mode: "popup",
            allowAdding: false,
            allowUpdating: false,
            allowDeleting: false,
            useIcons: true
        },
         searchPanel: {
            visible: true,
            width: 300,
            placeholder: "Search..."
        },

        onEditorPreparing: function (e) {

        },
        Selection: {
            mode: "single"
        },
        onToolbarPreparing: function (e) {
            var dataGrid = e.component;
            e.toolbarOptions.items.unshift({
                location: "after",
                widget: "dxButton",
                options: {
                    icon: "refresh",
                    onClick: function () {
                        dataGrid.refresh();
                    }
                }
            });
            e.toolbarOptions.items.unshift({
                location: "after",
                widget: "dxButton",
                options: {
                    text: "Add Member",
                    onClick: function () {
                        sessionStorage.setItem("isclicked", 2);
                        //   alert(sessionStorage.getItem("isclicked"));
                        window.location = "/Member/MemberProfile";

                    }
                }
            });

        },
        onRowUpdating: function (options) {
            $.extend(options.newData, $.extend(true, {}, options.oldData, options.newData));
        },
        Paging: {
            pageSize: 10
        },
        pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 25, 50, 100],
            showInfo: true,
            infoText: "showing page {0} of {1} ({2} Members)"
        },
        allowColumnReordering: true,
        allowColumnResizing: true,
        rowAlternationEnabled: true,
        hoverStateEnabled: true,

        columns: [
            {
                dataField: "memberID",
                caption: "Member ID",
                allowedit: false,
                width: '8%',
                allowEditing: false,
            }

            , {
                dataField: "firstName",
                caption: "First Name",
                width: '20%'
            }, {
                dataField: "lastName",
                caption: "Last Name",
                width: '20%'

            }
            ,

            {
                dataField: "gender",
                caption: "Gender",
                width: '8%'

            }
            ,

            {
                dataField: "ahcccsid",
                caption: "Access ID",
                width: '8%'

            }
            
            ,
            {
                dataField: "groupHome",
                caption: "Home",
                width: '15%',
                groupIndex: 0,
                fixed: true
            },

            {
                dataField: "memberStatus",
                caption: "Status",
                dataType: "string",
                width: '15%'

            },
            {
                dataField: "statusDate",
                caption: "Status Date",
                width: '10%',
                dataType: 'date'

            }

            , {
                dataField: "memberID",
                caption: "Action",
                alignment: "center",
                width: '5%'
                , cellTemplate: function (container, options) {
                    $('<a/>').addClass('dx-link')
                        .text('View')
                        .on('dxclick', function () {
                            sessionStorage.setItem("memberUser", options.value);
                            sessionStorage.setItem("isclicked", 1);
                            window.location = "/Member/MemberProfile";
                        }).appendTo(container);
                }
            }



        ],
        filterRow: {
            visible: true
        },
        groupPanel: {
            visible: true
        },
        scrolling: {
            mode: "fixed"
        },


    });
});

function tb() {


    $("#MemberServiceInfo2").dxTagBox({
        dataSource: new DevExpress.data.ArrayStore({
            data: MemberServiceInfo,
            key: "empID"
        }),
        value: MemberServiceInfo,
        displayExpr: "serviceType",
        valueExpr: "empID",
    });
};


function disenableEdit() {
    if (role == "Administrator") {
        CanEditGrid = true;
    }
    else { CanEditGrid = false; }
}

function getMemberDetails() {
    if(memberuser > 0)
    {
        $.ajax({
            type: "GET",
            url: "/api/Member/memberDetails/" + memberuser,
            async: false,
            success: function (data) {
                Member = data;
            },
            error: function () {
                DevExpress.ui.dialog.alert("Error Occured retrieving Member details", "Member Details Error")
            }
        });
    }
}
function getServiceTypeConcatService() {
    $.ajax({
        type: "GET",
        url: "/api/MemberServices/GetServicesWithID",
        async: false,
        success: function (data) {
            serviceTypeWithServices = data;
        },
        error: function () {
            DevExpress.ui.dialog.alert("Error Occured retrieving Member details", "Concat Service Error")
        }
    });
}
function getEmployeeList() {
    $.ajax({
        type: "GET",
        url: "/api/GetAllEmployeeFullName",
        async: false,
        success: function (data) {
            employeeList = data;
        },
        error: function () {
            alert("Error Occured retrieving List of employees");
        }
    });
}
function getMemberEmergencyContactList() {
    if (memberuser > 0) {
        $.ajax({
            type: "GET",
            url: "/api/Member/memberEmergencyList/" + memberuser,
            async: false,
            success: function (data) {
                MemberEmergencyContacts = data;
            },
            error: function () {
                alert("Error Occured retrieving Member emergency list")
            }
        });
    }
}
function getMemberServiceList() {
    if (memberuser > 0) {
        $.ajax({
            type: "GET",
            url: "/api/Member/memberServices/" + memberuser,
            async: false,
            success: function (data) {
                MemberServices = data;
            },
            error: function () {
                alert("Error Occured retrieving Member service list")
            }
        })
    };
}
function getMemberStatusList() {
    $.ajax({
        type: "GET",
        url: "/api/MemberStatusList/",
        async: false,
        success: function (data) {
            MemberStatus_ = data;
        },
        error: function () {
            alert("Error Occured retrieving list of member statuses")
        }
    });
}
function getGroupHomeList() {
    $.ajax({
        type: "GET",
        url: "/api/groupHomeList/",
        async: false,
        success: function (data) {
            GroupHomes_ = data;
        },
        error: function () {
            alert("Error Occured retrieving list of group homes")
        }
    });
}
function getStates() {
    $.ajax({
        type: "GET",
        url: "/api/stateList/",
        async: false,
        success: function (data) {
            states = data;
        },
        error: function () {
            alert("Error Occured retrieving list of states")
        }
    });
}
function getGender() {
    $.ajax({
        type: "GET",
        url: "/api/genderList/",
        async: false,
        success: function (data) {
            Genders = data;
        },
        error: function () {
            alert("Error Occured retrieving list of gender")
        }
    });
}
function getMaritalStatuses() {
    $.ajax({
        type: "GET",
        url: "/api/maritalStatusList/",
        async: false,
        success: function (data) {
            MaritalStatuses = data;
        },
        error: function () {
            alert("Error Occured retrieving list of marital statuses")
        }
    });
}
function getRelationships() {
    $.ajax({
        type: "GET",
        url: "/api/relationshipList/",
        async: false,
        success: function (data) {
            relationships = data;
        },
        error: function () {
            alert("Error Occured retrieving list of relationships")
        }
    });
}


$(function () {

    $("#ActivityreportGrid").dxDataGrid({
        dataSource: dataSource,
        editing: {
            mode: "popup",
            allowAdding: false,
            allowUpdating: false,
            allowDeleting: false,
            useIcons: true
        },
      
        onEditorPreparing: function (e) {

        },
        Selection: {
            mode: "single"
        },
        onToolbarPreparing: function (e) {
            var dataGrid = e.component;
            e.toolbarOptions.items.unshift({
                location: "after",
                widget: "dxButton",
                options: {
                    icon: "refresh",
                    onClick: function () {
                        dataGrid.refresh();
                    }
                }
            });
            e.toolbarOptions.items.unshift({
                location: "after",
                widget: "dxButton",
                options: {
                    text: "back to Member Info",
                    onClick: function () {
                   
                        window.location = "/Member/MemberProfile";

                    }
                }
            });
      
            e.toolbarOptions.items.unshift({
                location: "before",
                widget: "dxButton",
                options: {
                    text: "Add New Activity",
                    onClick: function () {

                        window.location = "/Member/MemberProfile";

                    }
                }
            });
        },
        onRowUpdating: function (options) {
            $.extend(options.newData, $.extend(true, {}, options.oldData, options.newData));
        },
        Paging: {
            pageSize: 5
        },
        pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [5,10, 25, 50, 100],
            showInfo: true,
            infoText: "showing page {0} of {1} ({2} Report)"
        },
        allowColumnReordering: true,
        allowColumnResizing: true,
        rowAlternationEnabled: true,
        hoverStateEnabled: true,

        columns: [
            {
                dataField: "memberID",
                caption: "Report ID",
                allowedit: false,
                width: '8%',
                allowEditing: false,
            }
        ,

          
            {
                dataField: "statusDate",
                caption: "Report Date",
                width: '15%',
                dataType: 'date'

            },
              {
                dataField: "memberStatus",
                caption: "Report Entered By",
                dataType: "string",
                width: '25%'

            }
            , {
                dataField: "memberID",
                caption: "Report Details",
                alignment: "center",
                width: '15%'
                , cellTemplate: function (container, options) {
                    $('<a/>').addClass('dx-link')
                        .text('View')
                        .on('dxclick', function () {
                            //sessionStorage.setItem("memberUser", options.value);
                            //sessionStorage.setItem("isclicked", 1);
                            //window.location = "/Member/MemberProfile";
                        }).appendTo(container);
                }
            }

            //, {
            //    dataField: "memberID",
            //    caption: "Member Info",
            //    alignment: "center",
            //    width: '5%'
            //    , cellTemplate: function (container, options) {
            //        $('<a/>').addClass('dx-link')
            //            .text('View')
            //            .on('dxclick', function () {
            //                sessionStorage.setItem("memberUser", options.value);
            //                sessionStorage.setItem("isclicked", 1);
            //                window.location = "/Member/MemberProfile";
            //            }).appendTo(container);
            //    }
            //}



        ],
        filterRow: {
            visible: false
        },
        groupPanel: {
            visible: false
        },
        scrolling: {
            mode: "fixed"
        },


    });
});





