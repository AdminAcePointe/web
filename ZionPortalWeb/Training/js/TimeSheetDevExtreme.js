﻿
var timeentrytype;
var submitType;
var TimeEntryTypeID;
var dto;
var Notedto;
var Name;
var lastclockinout
var employeeTimeSheet
var dataFromCell = {};
var cdate;
var TimeEntryID;
var employeeServices;
var lastServiceID;
var lastServiceDetails;

// Get Time Entry type from api
if(sessionStorage.getItem("employeeid") !== null)
{
    GetRecentTimeEntryType()
}
//Get ip address
ipLookUp()

// Get new Time Entry Type
SetTimeEntryType(timeentrytype);



//getEmployeeTimesheetDetails();


// Events
$("#AddNote").on("click", function () {
    launchNote();
  
})



// functions

function ipLookUp() {
    $.ajax('http://ip-api.com/json')
        .then(
            function success(response) {


                sessionStorage.setItem("lat", response.lat);
                sessionStorage.setItem("long", response.lon);
            },

            function fail(data, status) {
            
                DevExpress.ui.dialog.alert("iplookup failed");
            }
        );
}


function SetTimeEntryType(timeentrytype) {

    if (timeentrytype === "Clock In") {
        GetLastService();
        submitType = "Clock Out"
        TimeEntryTypeID = 2

    }
    else {
        getEmployeeServiceList();
        submitType = "Clock In"
        TimeEntryTypeID = 1
       
    }
}

function getEmployeeServiceList() {
    $.ajax({
        type: "GET",
        async: false,
        url: "/api/Employee/EmployeeServicesWithID/" + sessionStorage.getItem("employeeid"),
        success: function (data) {
            employeeServices = data;
        },
        error: function () {
            DevExpress.ui.dialog.alert("Error retrieving Service List");
        }
    });
}

function AddNote() {

    sessionStorage.setItem("note", $("#note-container").val());
    sessionStorage.setItem("serviceID", $("#ServiceSelectBox").dxSelectBox("instance").option("value"));
    $("#noteGrid").dxPopup('instance').hide()
}



function launchNote() {
           

    try {

        var p = $("#noteGrid").dxPopup("instance");
        p.option("title", "Add Note");
        p.show()
    }
    catch (e) {

        DevExpress.ui.dialog.alert("Error launching Note app")
    }
          
            
}


function launchPhoto() {


    try {

        var p = $("#photoGrid").dxPopup("instance");
        p.option("title", "take Photo");
        p.show()
    }
    catch (e) {

        DevExpress.ui.dialog.alert("Error launching Photo app")
    }

}

/// Dev Extreme
  
    $("#clockinout").dxButton({
        accessKey: null,
        activeStateEnabled: true,
        disabled: false,
        elementAttr: {},
        focusStateEnabled: true,
        height: 50,
        hint: undefined,
        hoverStateEnabled: true,
        onClick: function () {
            launchNote();
               
        },
        onContentReady: null,
        onDisposing: null,
        onInitialized: null,
        onOptionChanged: null,
        rtlEnabled: false,
        tabIndex: 0,
        template: "content",
        text: submitType,
        type: "normal",
        useSubmitBehavior: false,
        validationGroup: undefined,
        visible: true,
        width: 180
    });



$(function () {
    
    $("#photoGrid").dxPopup({
        height: 470,
        width: 500,
        visible: false,
        closeOnBackButton: true,
        closeOnOutsideClick: false,
        onShown: function (e) {
            $('#phGrid').html("");
            $('#phGrid').append(" <div id='camera-container' style='background-color:#f8f9fa;height:200; width:240; '></div><hr style='margin-top:30px' /><p><button class='btn-primary center' style='margin-top:50px' type='button' id='takephoto'>Take Photo</button></p>");
            initphoto();
        },
        contentTemplate: function () {
            return $("<div />").append(
             $("<div id='phGrid' >")
                );
        }
    });
});

$(function () {
    $("#noteGrid").dxPopup({
        height: 450,
        width: 500,
        visible: false,
        closeOnBackButton: true,
        closeOnOutsideClick: false,
        onShown: function (e) {
            $('#ntGrid').html("");
            $('#ntGrid').append(
                $("#ServiceSelectBox").dxSelectBox({
                    dataSource: employeeServices,
                    displayExpr: "serviceName",
                    valueExpr: "memberServiceId",
                    searchEnabled: true
                })
            )
        
            $('#ntGrid').append(" <textarea id='note-container'  class='nttextarea'style=' margin:0 auto;'></textarea><hr /><p><button class='btn-primary center' style='margin:0 auto' type='button' id='subnot'>Submit Note</button></p>");
            
            $("#subnot").on('click', function () {
                if (($("#ServiceSelectBox").dxSelectBox("instance").option("value")) != null && $("#note-container").val() != "") {
                    AddNote();
                    launchPhoto();
                }
                else {
               
                    DevExpress.ui.dialog.alert("Please select service and enter note to proceed");
                }
             

            })
        },
        contentTemplate: function () {
            return $("<div />").append(
             $("<div id='ntGrid' >")
                );
        }
    });
});


function initphoto() {
  



    Webcam.set({
        width: 240,
        height: 240,
        image_format: 'jpeg',
        jpeg_quality: 90
    });
    Webcam.attach('#camera-container');
 
    $('#takephoto').on('click', function (e) { 
        var dataURI  

        Webcam.snap(function (data_uri) {
            dataURI =  data_uri
        } )
        sessionStorage.setItem("imageURL", dataURI);
        sessionStorage.setItem("clockDate", getdatetime());
        createTimeSheetObj();      
        SaveTime();
        createNoteObj();
        SaveNote();
       
        location.reload();
      
       
 

        $("#photoGrid").dxPopup('instance').hide()
      
    });
}

function getdatetime() {
    var today = new Date();
var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
return  date + ' ' + time;

}




function createTimeSheetObj (){
  
    dto = {
        'Latitude': sessionStorage.getItem("lat")
       , 'Longitude': sessionStorage.getItem("long")
       , 'IPAddress': sessionStorage.getItem("ip")
       , 'EmployeeID': sessionStorage.getItem("employeeid")
       , 'TimeEntryTypeID': TimeEntryTypeID
       , 'TimeEntryImage': sessionStorage.getItem("imageURL")
        , 'ClockDateTime': sessionStorage.getItem("clockDate")
        , 'ServiceID': sessionStorage.getItem("serviceID") 
    };
}

function createNoteObj() {
  
    Notedto = {
         'NoteTypeID': "1"
        ,'Notes': sessionStorage.getItem("note")
        ,'TimeEntryID': TimeEntryID
    };
}



function SaveTime() {
    $.ajax({
        type: "POST",
        url: "/api/timeentry/add/" ,
        data: JSON.stringify(dto),
        contentType: "application/json",
        async:false,
        success: function (data) {
            TimeEntryID = JSON.stringify(data);
            //DevExpress.ui.dialog.alert("Time Submitted Succesfully");
        },
        error: function () {
     
            DevExpress.ui.dialog.alert("Error submitting timesheet");
         
        }
    });

}


$("#large-indicator").dxLoadIndicator({
    height: 60,
    width: 60
});


function SaveNote() {
    $.ajax({
        type: "POST",
        url: "/api/timeentrynote/add/",
        data: JSON.stringify(Notedto),
        async: false,
        contentType: "application/json",
        success: function (data) {
          
            DevExpress.ui.dialog.alert("Time Submitted Succesfully");

          
        },
        error: function () {
    
           DevExpress.ui.dialog.alert("Error saving notes");

        }
    });

}

function formatJSDate(dt) {

    return (dt.getMonth() + 1 + '/' + dt.getDate() + '/' + dt.getFullYear() + ' ' + dt.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true }));
}



function GetRecentTimeEntryType() {
    $.ajax({
        type: "GET",
        url: "/api/timeentry/employee/" + sessionStorage.getItem("employeeid"),
        contentType: "application/json",
        async:false,
        success: function (data) {
           // alert(data);
            timeentrytype = data.timeEntryType.timeEntryTypeDesc;
            Name = data.employee.person.firstName + " " + data.employee.person.lastName;
            var formatDate = new Date(data.clockDateTime)
            var date = formatJSDate(formatDate)
            lastclockinout = "Last " + timeentrytype + " Time : " + date
            $('#lastclockinout').text(lastclockinout);
            $('#Name').text(Name);
           
           
            lastServiceID = data.serviceID;
        },
        error: function () {

            DevExpress.ui.dialog.alert("Error retrieving most recent time sheet entry");

        }
    });

}


//better name?
function GetLastService() {
    $.ajax({
        type: "GET",
        url: "/api/timeentry/employeeLastService/" + sessionStorage.getItem("employeeid") + "/" + lastServiceID,
        contentType: "application/json",
        async: false,
        success: function (data) {
            employeeServices = data;
        },
        error: function () {

            DevExpress.ui.dialog.alert("Error retrieving most recent service entry");

        }
    });

}


  
 

  


/////////////////////////////////
var userrole = sessionStorage.getItem("employeeid") 

var viewtimeurl = "/api/time/employee/" + sessionStorage.getItem("employeeid") 
var updatetimeurl = "/api/approvalnote/update"



function getEmployeeTime() {
    $.ajax({
        type: "GET",
        url: viewtimeurl,
        contentType: "application/json",
        async:false,
        success: function (data) {
   
            emptime.push(data)
         
        },
        error: function () {

            DevExpress.ui.dialog.alert("Error retrieving employee timesheet");

        }
    });

}

var dataStore = new DevExpress.data.CustomStore({
    key: "id",
    type: "array",
    loadMode: "raw",
    cacheRawData: false,

    load: function (loadOptions) {
        var d = $.getJSON(viewtimeurl);
        return d;
    },
      byKey: function (key) {
          return $.getJSON("/api/time/employee/" + encodeURIComponent(key));
    }, insert: function (values) {
          return $.post(viewtimeurl, values);
    },
    update: function (key,values) {
        return $.ajax({
            url: updatetimeurl,
            method: "PUT",
            data: values
        });
    },
});

var dataSource = new DevExpress.data.DataSource({
    store: dataStore

});




$(function () {

    $("#viewTimesheetGrid").dxDataGrid({
        dataSource: dataSource,
       
        Selection: {
            mode: "row"
        },
        onToolbarPreparing: function (e) {
            var dataGrid = e.component;
            e.toolbarOptions.items.unshift({
                location: "after",
                widget: "dxButton",
                options: {
                    icon: "refresh",
                    onClick: function () {
                        dataGrid.refresh();
                    }
                }
            })
        },
        onRowUpdating: function (options) {
            $.extend(options.newData, $.extend(true, {}, options.oldData, options.newData));
        },
        Paging: {
            pageSize: 10
        },
        pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 25, 50, 100],
            showInfo: true,
            infoText: "showing page {0} of {1} ({2} Time entries)"
        },
        allowColumnReordering: true,
        allowColumnResizing: true,
        rowAlternationEnabled: true,
        hoverStateEnabled: true,
    

        columns: [
            {
                dataField: "isoWeekOfYear",
                caption: "week",
                width: 100,
                groupIndex: 0,
                sortOrder: "desc",
                sortIndex: 0
            }
            ,
            {
                dataField: "weekPeriod",
                caption: "Period",
                width: 100
            }
            ,{
                dataField: "clockDate",
                dataType: "date",
                caption: "Date",
                width: 100,
                alignment: 'center'
            
            }
            , {
                dataField: "workType",
                caption: "Work Type",
                width: 120,
                alignment: 'center'
            }
            , {
                dataField: "hoursWorked",
                caption: "Hours",
                width: 140,
                alignment: 'center'
            }
            , {
                dataField: "adjustedHours",
                caption: "Adjusted Hours",
                width: 140,
                alignment: 'center'
             

            }
        ,{
            dataField: "approvalStatus",
            caption: "Approval Status",
            alignment: 'center',
            width: 150
            }
            // , {
            //    dataField: "approvalDate",
            //    dataType: "date",
            //    caption: "Approval Date",
            //     width: 150,
            //     alignment: 'center'
            //}
           
            , {
                width: 120,
                caption: "Approval Notes",
                alignment: 'center',
                cellTemplate: function (container, options) {
                    $('<a/>').addClass('dx-link')
                        .text('View')
                        .on('dxclick', function () {
                          
                            $("#notepopup").dxPopup("instance").show(options.data);
                            $("#notetxt").dxTextArea("instance").option("value",
                                function () {
                                    if (options.data.notes !== null) {
                                        return "Note: " + options.data.notes 
                                    }
                                    else return "No notes entered"
                                }
                                
                                );
                            $("#noteby").dxTextBox("instance").option("value",
                                function () {
                                    if (options.data.noteEnteredByEmployeeName !== null) {
                                     return   " *** Notes entered by " + options.data.noteEnteredByEmployeeName + " on " + options.data.noteEntryDate + "***"
                                    }
                                   
                                });

                        }).appendTo(container);
                }
            }
            , {
                dataField: "servicename",
                caption: "Service",
                alignment: 'center',
                width: 100
            }
            , {
                width: 80,
                caption: "Details",
                alignment: 'center',
                cellTemplate: function (container, options) {
                    $('<a/>').addClass('dx-link')
                        .text('details')
                        .on('dxclick', function () {
                           
                            cdate = options.data.clockDate;
                            cdate = cdate.substring(0,10)
                            $("#tspopup").dxPopup("instance").show(options.data);
                       

                        }).appendTo(container);
                }
            }
           



        ],
        filterRow: {
            visible: true
        },
        groupPanel: {
            visible: true
        },
        scrolling: {
            mode: "virtual"
        },
        grouping: {
            autoExpandAll: false,
        },
        onContentReady: function (e) {
            e.component.expandRow(e.component.getKeyByRowIndex(0));
        },
           
        sortByGroupSummaryInfo: [{
            summaryItem: "sum"
        }]
        ,
        summary: {
            groupItems: [{
                column: "Adjusted Hours",
                summaryType: "sum",
                displayFormat: "{0} billable hours",
            }
                , {
                    name: "OvertimeHours",
                    summaryType: "custom",
                    displayFormat: "{0} billable overtime hours",
               
                   
                }
               
                //,  {
                //    column: "Hours",
                //    summaryType: "sum",
                //   // valueFormat: "currency",
                //    displayFormat: "Total : {0}",
                //    showInGroupFooter: true
                //}
                
                //, {
                //    column: "Adjusted Hours",
                //    summaryType: "sum",
                //    // valueFormat: "currency",
                //    displayFormat: "Total : {0}",
                //    showInGroupFooter: true
                //}
            ]
            , calculateCustomSummary: function (options) {
                // Calculating "customSummary1"
                if (options.name == "OvertimeHours") {
                    switch (options.summaryProcess) {
                        case "start":
                            options.totalValue = 0;
                            break;
                        case "calculate":
                            {
                                options.totalValue = (options.totalValue + options.value.adjustedHours);
                            }
                            break;
                        case "finalize": if (options.totalValue > 40) { options.totalValue = options.totalValue - 40 }
                        else {
                            options.totalValue = 0
                        }

                    }
                }
            }
        }

    });
});

$("#notepopup").dxPopup({
    height: 500,
    width: 600,
    visible: false,
    closeOnBackButton: false,
    closeOnOutsideClick: true,
    showTitle: true,
   
    title: 'Approval Notes',
    contentTemplate: function () {
        return $("<div />").append(
            $("<div id='notetxt' />")
                .dxTextArea({
                    value: "some text",
                    readOnly: true
                }))
                .append(
                    $("<div id='noteby' />")
                    .dxTextBox({
                            value: "some text",
                            readOnly: true
                        })
        );
    }
});

// Time sheet details
$("#tspopup").dxPopup({
    height: 500,
    width: 700,
    visible: false,
    closeOnBackButton: false,
    closeOnOutsideClick: true,
    showTitle: true,

    title: 'Time sheet details',

    onShown: function (e) {
       
        console.log(dataFromCell)
        $("#timesheetGrid").dxDataGrid({
            dataSource: DevExpress.data.AspNet.createStore({
                key: "timeEntryID",
                loadUrl: "/api/time/employeetimeentrydetail/" + sessionStorage.getItem('employeeid') + '/' + cdate,
                onBeforeSend: function (request) {
                    request.test = "test";
                }
            }),
            editing: {
                mode: "popup",
                allowAdding: false,
                allowUpdating: false,
                allowDeleting: false,
                useIcons: true
            },
            Selection: {
                mode: "single"
            },
            onToolbarPreparing: function (e) {
                var dataGrid = e.component;


            },
            onRowUpdating: function (options) {
                $.extend(options.newData, $.extend(true, {}, options.oldData, options.newData));
            },
            Paging: {
                pageSize: 10
            },
            pager: {
                showPageSizeSelector: true,
                allowedPageSizes: [10, 25, 50, 100],
                showInfo: true,
                infoText: "showing page {0} of {1} ({2} time entries)"
            },
            allowColumnReordering: true,
            allowColumnResizing: true,
            rowAlternationEnabled: true,
            hoverStateEnabled: true,
            filterRow: {
                visible: false
            },
            groupPanel: {
                visible: false
            },
            scrolling: {
                mode: "virtual"
            },
            columns: [
              

                {
                    dataField: 'timeEntryTypeDesc',
                    caption: "Entry Type",
                    width: 100,
                }
            ,
                {
                    dataField: 'clockDateTime',
                    dataType: 'datetime',
                    format: 'MM-dd-YYYY hh:mm:ss a',
                    caption: "Datetime",
                    width: 200
                }
                ,
                {
                    dataField: 'notes',
                    width: 180
                }
                 , 
           {
               caption: "Picture",
               dataField: "timeEntryImage",
               cellTemplate: function (container, options) {
                   container.addClass("imgthumb");
                   $("<div class='imgthumb'>")
                       .append($("<img>", { "src": options.value }))
                       .appendTo(container);
               }
               , width: 150

           }
            ]

        });



    },
    onHiding: function (e) {

    },
    contentTemplate: function () {
        return $("<div />").append(
            $("<div id='timesheetGrid' >")
        );
    }
    //contentTemplate: function () {
    //    return $("<div class='imgthumb'>")
    //        .append($("<img>", { "src": options.value }))
    //}
         

});



//// Approval
var statuses = ["All", "Approved", "Rejected", "Pending Approval"]; 
var statuses2 = [{ "Approval Status": "Rejected" }, { "Approval Status": "Approved" }, { "Approval Status": "Pending Approval" }]; 

var appurl = "/api/time/employeesPerManager/" + sessionStorage.getItem("employeeid") 

var dataStoreApp = new DevExpress.data.CustomStore({
    key: "id",
    type: "array",
    loadMode: "raw",
    cacheRawData: false,

    load: function (loadOptions) {
        var d = $.getJSON(appurl);
        return d;
    },
    byKey: function (key) {
        return $.getJSON("/api/time/employeesPerManager/" + encodeURIComponent(key));
    }
    , insert: function (values) {
        return $.post(viewtimeurl, values);
    },
    update: function (key, values) {
        return $.ajax({
            url: updatetimeurl,
            method: "PUT",
            data: values
        });
    },
});

var dataSourceApp = new DevExpress.data.DataSource({
    store: dataStoreApp

});


// Approval main grid
$(function () {

    var approvalTimesheetGrid =  $("#approvalTimesheetGrid").dxDataGrid({
        dataSource: dataSourceApp,
        editing: {
            mode: "form",
            allowAdding: false,
            allowUpdating: true,
            allowDeleting: false,
            useIcons: true
        },
        Selection: {
            mode: "single"
        },
        grouping: {
            autoExpandAll: false,
        },
        onContentReady: function (e) {
            e.component.expandRow(e.component.getKeyByRowIndex(0));
            e.component.expandRow(e.component.getKeyByRowIndex(1));
        },
        onToolbarPreparing: function (e) {
            var dataGrid = e.component;
            e.toolbarOptions.items.unshift({
                location: "after",
                widget: "dxButton",
                options: {
                    icon: "refresh",
                    onClick: function () {
                        dataGrid.refresh();
                    }
                }
            })
        },
        onRowUpdating: function (options) {
            $.extend(options.newData, $.extend(true, {}, options.oldData, options.newData));
        }
 
        ,
        Paging: {
            pageSize: 10
        },
        pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 25, 50, 100],
            showInfo: true,
            infoText: "showing page {0} of {1} ({2} Time entries)"
        },
        allowColumnReordering: true,
        allowColumnResizing: true,
        rowAlternationEnabled: true,
        hoverStateEnabled: true,

        onCellPrepared: function (e) {
            if (e.rowType === "data" && e.column.command === "edit") {
                var $links = e.cellElement.find(".dx-link");
                if (e.row.data.workType == "PTO")
                    $links.filter(".dx-link-edit").remove();
                if (e.row.data.billingApprovalStatus == "Approved")
                    $links.filter(".dx-link-edit").remove();
                if (e.row.data.billingApprovalStatus == "Rejected")
                    $links.filter(".dx-link-edit").remove();

            }
        },
       

        columns: [
           
            {
                dataField: "employeeName",
                caption: "Employee",
                width: 150
                ,allowedit: false,
                allowEditing: false,
                groupIndex: 1,
                sortOrder: "asc",
                sortIndex: 1,
            }
        ,
             {
                dataField: "isoWeekOfYear",
                caption: "week",
                width: 100,
                groupIndex: 0,
                sortOrder: "desc",
                 sortIndex: 0,
                  allowedit: false,
            allowEditing: false
            }
            ,
            {
                dataField: "weekPeriod",
                caption: "Period",
                width: 100,
                allowedit: false,
                allowEditing: false
            }
            , {
                dataField: "clockDate",
                dataType: "date",
                caption: "Date",
                width: 100,
                alignment: 'center',
                allowedit: false,
                allowEditing: false

            }
            , {
                dataField: "workType",
                caption: "Work Type",
                width: 120,
                alignment: 'center',
                allowedit: false,
                allowEditing: false
            }
            , {
                dataField: "servicename",
                caption: "Service",
                alignment: 'center',
                width: 100
            }
            , {
                dataField: "hoursWorked",
                caption: "Hours",
                width: 140,
                alignment: 'center',
                allowedit: false,
                allowEditing: false
            }
            , {
                dataField: "adjustedHours",
                caption: "Adjusted Hours",
                width: 140,
                alignment: 'center',
                allowedit: false,
                allowEditing: false


            }
            , {
            dataField: "approvalStatus",
                caption: "Approval Status",
                alignment: 'center',
                width: 150,
            lookup: {
                dataSource: statuses2,
                valueExpr: "Approval Status",
                displayExpr: "Approval Status"
            }
        }
            , 
            {
                dataField: "notes",
                visible: false,
                formItem: {
                    visible: true
                },
                caption: "Approver Notes"
            }
      
            , {
                width: 120,
                visible: true,
                formItem: {
                    visible: false
                },
            caption: "Approver Notes",
                alignment: 'center',
                allowedit: true,
                allowEditing: true,
            cellTemplate: function (container, options) {
                $('<a/>').addClass('dx-link')
                    .text('View')
                    .on('dxclick', function () {
                        $("#approvalnotepopup").dxPopup("instance").show(options.data);
                        $("#approvalnotetxt").dxTextArea("instance").option("value",
                            function () {
                                if (options.data.notes !== null) {
                                    return "Note: " + options.data.notes
                                }
                                else return "No notes entered"
                            }

                        );
                        $("#approvalnoteby").dxTextBox("instance").option("value",
                            function () {
                                if (options.data.noteEnteredByEmployeeName !== null) {
                                    return " *** Notes entered by " + options.data.noteEnteredByEmployeeName + " on " + options.data.noteEntryDate + "***"
                                }

                            });
                      
                  

                    }).appendTo(container);
            }
        }
            , {
            width: 120,
                caption: "Details",
                formItem: { visible: false },
                alignment: 'center',
                
            cellTemplate: function (container, options) {
                $('<a/>').addClass('dx-link')
                    .text('details')
                    .on('dxclick', function () {
                        cdate = options.data.clockDate;
                        cdate = cdate.substring(0, 10)
                        $("#aptspopup").dxPopup("instance").show(options.data);


                    }).appendTo(container);
            }
        }
          
        ],
        filterRow: {
            visible: true
        },
        groupPanel: {
            visible: true
        },
        scrolling: {
            mode: "virtual"
        },
        grouping: {
            autoExpandAll: false,
        },
        onContentReady: function (e) {
            e.component.expandRow(e.component.getKeyByRowIndex(0));
            e.component.expandRow(e.component.getKeyByRowIndex(1));
        },

        sortByGroupSummaryInfo: [{
            summaryItem: "sum"
        }]
        ,
        summary: {
            groupItems: [{
                column: "Adjusted Hours",
                summaryType: "sum",
                displayFormat: "{0} billable hours",
            }
                , {
                name: "OvertimeHours",
                summaryType: "custom",
                displayFormat: "{0} billable overtime hours",


            }

            //    , {
            //    column: "Hours",
            //    summaryType: "sum",
            //    // valueFormat: "currency",
            //    displayFormat: "Total : {0}",
            //    showInGroupFooter: true
            //}

            //    , {
            //    column: "Adjusted Hours",
            //    summaryType: "sum",
            //    // valueFormat: "currency",
            //    displayFormat: "Total : {0}",
            //    showInGroupFooter: true
            //}
            ]
            , calculateCustomSummary: function (options) {
                // Calculating "customSummary1"
                if (options.name == "OvertimeHours") {
                    switch (options.summaryProcess) {
                        case "start":
                            options.totalValue = 0;
                            break;
                        case "calculate":
                            {
                                options.totalValue = (options.totalValue + options.value.adjustedHours);
                            }
                            break;
                        case "finalize": if (options.totalValue > 40) { options.totalValue = options.totalValue - 40 }
                        else {
                            options.totalValue = 0
                        }

                    }
                }
            }
        }


    }).dxDataGrid("instance");
    $("#selectStatus").dxSelectBox({
        dataSource: statuses,
        value: statuses[3],
        onInitialized: function (data) { approvalTimesheetGrid.filter(["Approval Status", "=", "Pending Approval"]) },
        onValueChanged: function (data) {
            if (data.value === "All")
                approvalTimesheetGrid.clearFilter();
            else
                approvalTimesheetGrid.filter(["Approval Status", "=", data.value]);
        }
    });
});

$("#approvalnotepopup").dxPopup({
    height: 500,
    width: 600,
    visible: false,
    closeOnBackButton: false,
    closeOnOutsideClick: true,
    showTitle: true,

    title: 'Approver Notes',
    contentTemplate: function () {
        return $("<div />").append(
            $("<div id='approvalnotetxt' />")
                .dxTextArea({
                    value: "some text",
                    //readOnly: true
                })).append(
                    $("<div id='approvalnoteby' />")
                        .dxTextBox({
                            value: "some text",
                            readOnly: true
                        })
                );
           
    }
});


$("#aptspopup").dxPopup({
    height: 500,
    width: 500,
    visible: false,
    closeOnBackButton: false,
    closeOnOutsideClick: true,
    showTitle: true,

    title: 'Time sheet details',

    onShown: function (e) {

        $("#aptimesheetGrid").dxDataGrid({
            dataSource: DevExpress.data.AspNet.createStore({
                key: "timeEntryID",
                loadUrl: "/api/time/employeetimeentrydetail/" + sessionStorage.getItem('employeeid') + '/' + cdate,
                onBeforeSend: function (request) {
                    request.test = "test";
                }
            }),
            editing: {
                mode: "popup",
                allowAdding: false,
                allowUpdating: false,
                allowDeleting: false,
                useIcons: true
            },
            Selection: {
                mode: "single"
            },
            onToolbarPreparing: function (e) {
                var dataGrid = e.component;


            },
            onRowUpdating: function (options) {
                $.extend(options.newData, $.extend(true, {}, options.oldData, options.newData));
            },
            Paging: {
                pageSize: 10
            },
            pager: {
                showPageSizeSelector: true,
                allowedPageSizes: [10, 25, 50, 100],
                showInfo: true,
                infoText: "showing page {0} of {1} ({2} time entries)"
            },
            allowColumnReordering: true,
            allowColumnResizing: true,
            rowAlternationEnabled: true,
            hoverStateEnabled: true,
            filterRow: {
                visible: false
            },
            groupPanel: {
                visible: false
            },
            scrolling: {
                mode: "fixed"
            },
            columns: [
               

                {
                    dataField: 'timeEntryTypeDesc',
                    caption: "Entry Type",
                    width: 100,
                },
                {
                    dataField: 'clockDateTime',
                    dataType: 'datetime',
                    format: 'MM-dd-YYYY hh:mm:ss a',
                    width: 150
                }
                ,
                {
                    caption: "Picture",
                    dataField: "timeEntryImage",
                    cellTemplate: function (container, options) {
                        container.addClass("imgthumb");
                        $("<div class='imgthumb'>")
                            .append($("<img>", { "src": options.value }))
                            .appendTo(container);
                    }
                    , width: 150

                }

             
            ]

        });



    },
    onHiding: function (e) {

    },
    contentTemplate: function () {
        return $("<div />").append(
            $("<div id='aptimesheetGrid' >")
        );
    }
    //contentTemplate: function () {
    //    return $("<div class='imgthumb'>")
    //        .append($("<img>", { "src": options.value }))
    //}


});


// Billing main grid
var billurl = "/api/time/employeesTimeForBilling" 
var dataStoreBill = new DevExpress.data.CustomStore({
    key: "id",
    type: "array",
    loadMode: "raw",
    cacheRawData: false,

    load: function (loadOptions) {
        var d = $.getJSON(billurl);
        return d;
    },
    byKey: function (key) {
        return $.getJSON("/api/time/employeesPerManager/" + encodeURIComponent(key));
    }
    , insert: function (values) {
        return $.post(viewtimeurl, values);
    },
    update: function (key, values) {
        return $.ajax({
            url: updatetimeurl,
            method: "PUT",
            data: values
        });
    },
});

var dataSourceBill = new DevExpress.data.DataSource({
    store: dataStoreBill

});
$(function () {

    var BillingTimesheetGrid = $("#BillingTimesheetGrid").dxDataGrid({
        dataSource: dataSourceBill,
        editing: {
            mode: "form",
            allowAdding: false,
            allowUpdating: true,
            allowDeleting: false,
            useIcons: true
        },
        grouping: {
            autoExpandAll: false,
        },
        onContentReady: function (e) {
            e.component.expandRow(e.component.getKeyByRowIndex(0));
            e.component.expandRow(e.component.getKeyByRowIndex(1));
          
        },
        selection: {
            mode: "multiple"
        },
        "export": {
            enabled: true,
            fileName: "Billing File",
            allowExportSelectedData: true
        },
        onCellPrepared: function (e) {
            if (e.rowType === "data" && e.column.command === "edit") {
                var $links = e.cellElement.find(".dx-link");
                if (e.row.data.workType == "PTO")
                    $links.filter(".dx-link-edit").remove();

            }
        },
        onToolbarPreparing: function (e) {
            var dataGrid = e.component;
            e.toolbarOptions.items.unshift({
                location: "after",
                widget: "dxButton",
                options: {
                    icon: "refresh",
                    onClick: function () {
                        dataGrid.refresh();
                    }
                }
            })
        },
        onRowUpdating: function (options) {
            $.extend(options.newData, $.extend(true, {}, options.oldData, options.newData));
        },
        Paging: {
            pageSize: 10
        },
        pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 25, 50, 100],
            showInfo: true,
            infoText: "showing page {0} of {1} ({2} Time entries)"
        },
        allowColumnReordering: true,
        allowColumnResizing: true,
        rowAlternationEnabled: true,
        hoverStateEnabled: true,




        columns: [

            {
                dataField: "employeeName",
                caption: "Employee",
                width: 150
                , allowedit: false,
                allowEditing: false,
                groupIndex: 1,
                sortOrder: "asc",
                sortIndex: 1,
            }
            ,
            {
                dataField: "isoWeekOfYear",
                caption: "week",
                width: 100,
                groupIndex: 0,
                sortOrder: "desc",
                sortIndex: 0,
                allowedit: false,
                allowEditing: false
            }
            //,
            //{
            //    dataField: "weekPeriod",
            //    caption: "Period",
            //    width: 100,
            //    alignment: 'left',
            //    groupIndex: 1,
            //    sortOrder: "desc",
            //    sortIndex: 1,
            //    allowedit: false,
            //    allowEditing: false
            //}
            , {
                dataField: "clockDate",
                dataType: "date",
                caption: "Date",
                width: 100,
                alignment: 'center',
                allowedit: false,
                allowEditing: false

            }
            , {
                dataField: "servicename",
                caption: "Service",
                alignment: 'center',
                width: 100
            }
            , {
                dataField: "workType",
                caption: "Work Type",
                width: 100,
                alignment: 'center',
                allowedit: false,
                allowEditing: false
            }
            , {
                dataField: "hoursWorked",
                caption: "Hours",
                width: 80,
                alignment: 'center',
                allowedit: false,
                allowEditing: false
            }
            , {
                dataField: "adjustedHours",
                caption: "Adj. Hours",
                width: 100,
                alignment: 'center',
                allowedit: false,
                allowEditing: false


            }
          
            , {
                dataField: "approvalStatus",
                caption: "Sup. Approval",
                width: 110,
                allowedit: false,

                allowEditing: false,
                lookup: {
                    dataSource: statuses2,
                    valueExpr: "Approval Status",
                    displayExpr: "Approval Status"
                }
            }
            , {
                width: 85,
                dataField: "notes",
                caption: "Sup. Notes",
                alignment: 'center',
                allowedit: false,
                allowEditing: false,
                cellTemplate: function (container, options) {
                    $('<a/>').addClass('dx-link')
                        .text('View')
                        .on('dxclick', function () {
                            $("#approvalnotepopup").dxPopup("instance").show(options.data);
                            $("#approvalnotetxt").dxTextArea("instance").option("value",
                                function () {
                                    if (options.data.notes !== null) {
                                        return "Note: " + options.data.notes
                                    }
                                    else return "No notes entered"
                                }

                            );
                            $("#approvalnoteby").dxTextBox("instance").option("value",
                                function () {
                                    if (options.data.noteEnteredByEmployeeName !== null) {
                                        return " *** Notes entered by " + options.data.noteEnteredByEmployeeName + " on " + options.data.noteEntryDate + "***"
                                    }

                                });



                        }).appendTo(container);
                }
            }
            , {
                width: 70,
                caption: "Details",
                formItem: { visible: false },
                alignment: 'center',

                cellTemplate: function (container, options) {
                    $('<a/>').addClass('dx-link')
                        .text('details')
                        .on('dxclick', function () {
                            cdate = options.data.clockDate;
                            cdate = cdate.substring(0, 10)
                            $("#aptspopup").dxPopup("instance").show(options.data);


                        }).appendTo(container);
                }
            }

            , {
                dataField: "billingApprovalStatus",
                caption: "Bill. Approval",
                width: 100,
                lookup: {
                    dataSource: statuses2,
                    valueExpr: "Approval Status",
                    displayExpr: "Approval Status"
                }
            }
            
            , {
                width: 100,
                dataField: "billingNotes",
                caption: "Bill. Notes",
                alignment: 'center',
                allowedit: true,
                allowEditing: true,
                cellTemplate: function (container, options) {
                    $('<a/>').addClass('dx-link')
                        .text('View')
                        .on('dxclick', function () {
                            $("#billingnotepopup").dxPopup("instance").show(options.data);
                            $("#billnotetxt").dxTextArea("instance").option("value",
                                function () {
                                    if (options.data.billingNotes !== null) {
                                        return "Note: " + options.data.billingNotes
                                    }
                                    else return "No notes entered"
                                }

                            );
                            $("#billnoteby").dxTextBox("instance").option("value",
                                function () {
                                    if (options.data.billingNoteEnteredByEmployeeName !== null) {
                                        return " *** Notes entered by " + options.data.billingNoteEnteredByEmployeeName + " on " + options.data.billingNoteEntryDate + "***"
                                    }

                                });



                        }).appendTo(container);
                }
            }



        ],
        filterRow: {
            visible: true
        },
        groupPanel: {
            visible: true
        },
        scrolling: {
            mode: "virtual"
        },
        grouping: {
            autoExpandAll: false,
        },
        sortByGroupSummaryInfo: [{
            summaryItem: "sum"
        }]
        ,
        summary: {
            groupItems: [{
                column: "Adj. Hours",
                summaryType: "sum",
                displayFormat: "{0} billable hours",
            }
                , {
                name: "OvertimeHours",
                summaryType: "custom",
                displayFormat: "{0} billable overtime hours",


            }

            
            ]
            , calculateCustomSummary: function (options) {
                // Calculating "customSummary1"
                if (options.name == "OvertimeHours") {
                    switch (options.summaryProcess) {
                        case "start":
                            options.totalValue = 0;
                            break;
                        case "calculate":
                            {
                                options.totalValue = (options.totalValue + options.value.adjustedHours);
                            }
                            break;
                        case "finalize": if (options.totalValue > 40) { options.totalValue = options.totalValue - 40 }
                        else {
                            options.totalValue = 0
                        }

                    }
                }
            }
        }


    }).dxDataGrid("instance");
    $("#BillingselectStatus").dxSelectBox({
        dataSource: statuses,
        value: statuses[1],
        onInitialized: function (data) { BillingTimesheetGrid.filter(["Bill. Approval", "=", "Approved"]) },
        onValueChanged: function (data) {
            if (data.value == "All")
                BillingTimesheetGrid.clearFilter();
            else
                BillingTimesheetGrid.filter(["Bill. Approval", "=", data.value]);
        }
    });
});


$("#billingnotepopup").dxPopup({
    height: 500,
    width: 600,
    visible: false,
    closeOnBackButton: false,
    closeOnOutsideClick: true,
    showTitle: true,

    title: 'Billing Notes',
    contentTemplate: function () {
        return $("<div />").append(
            $("<div id='billnotetxt' />")
                .dxTextArea({
                    value: "some text",
                    readOnly: true
                })).append(
                    $("<div id='billnoteby' />")
                        .dxTextBox({
                            value: "some text",
                            readOnly: true
                        })
                );

    }
});


$("#billingpopup").dxPopup({
    height: 500,
    width: 500,
    visible: false,
    closeOnBackButton: false,
    closeOnOutsideClick: true,
    showTitle: true,

    title: 'Time sheet details',

    onShown: function (e) {

        $("#aptimesheetGrid").dxDataGrid({
            dataSource: DevExpress.data.AspNet.createStore({
                key: "timeEntryID",
                loadUrl: "/api/time/employeetimeentrydetail/" + sessionStorage.getItem('employeeid') + '/' + cdate,
                onBeforeSend: function (request) {
                    request.test = "test";
                }
            }),
            editing: {
                mode: "popup",
                allowAdding: false,
                allowUpdating: false,
                allowDeleting: false,
                useIcons: true
            },
            Selection: {
                mode: "single"
            },
            onToolbarPreparing: function (e) {
                var dataGrid = e.component;


            },
            onRowUpdating: function (options) {
                $.extend(options.newData, $.extend(true, {}, options.oldData, options.newData));
            },
            Paging: {
                pageSize: 10
            },
            pager: {
                showPageSizeSelector: true,
                allowedPageSizes: [10, 25, 50, 100],
                showInfo: true,
                infoText: "showing page {0} of {1} ({2} time entries)"
            },
            allowColumnReordering: true,
            allowColumnResizing: true,
            rowAlternationEnabled: true,
            hoverStateEnabled: true,
            filterRow: {
                visible: false
            },
            groupPanel: {
                visible: false
            },
            scrolling: {
                mode: "fixed"
            },
            columns: [


                {
                    dataField: 'timeEntryTypeDesc',
                    caption: "Entry Type",
                    width: 100,
                },
                {
                    dataField: 'clockDateTime',
                    dataType: 'datetime',
                    format: 'MM-dd-YYYY hh:mm:ss a',
                    width: 150
                }
                ,
                {
                    caption: "Picture",
                    dataField: "timeEntryImage",
                    cellTemplate: function (container, options) {
                        container.addClass("imgthumb");
                        $("<div class='imgthumb'>")
                            .append($("<img>", { "src": options.value }))
                            .appendTo(container);
                    }
                    , width: 150

                }


            ]

        });



    },
    onHiding: function (e) {

    },
    contentTemplate: function () {
        return $("<div />").append(
            $("<div id='aptimesheetGrid' >")
        );
    }
    //contentTemplate: function () {
    //    return $("<div class='imgthumb'>")
    //        .append($("<img>", { "src": options.value }))
    //}


});

