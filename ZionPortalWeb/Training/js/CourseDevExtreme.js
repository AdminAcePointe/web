﻿//validatetoken()
// Cards
upcoming();
available();
completed();

sessionStorage.setItem("token","gupicagp")

var token = sessionStorage.getItem("token")
//function validatetoken() {
//    $.ajax({
//        type: "GET",
//        url: "/api/auth/ValidateToken",
//        data: sessionStorage.getItem("token"),
//        success: function (data) {
//            alert("yay");
//        },
//        error: function () {
//            alert("nayyyy")
//        }
//    });
//}



function upcoming() {
    $.ajax({
        type: "GET",
        url: "/api/UpcomingCourseSchedule",
        success: function (data) {
            $('#upcomingclasses').text(data)
        },
        error: function () {
            $('#upcomingclasses').text("Error Occured")
        }
    });
}

function available() {
    $.ajax({
        type: "GET",
        url: "/api/AvailableCourseSchedule",
        success: function (data) {
            $('#availableclasses').text(data)
        },
        error: function () {
            $('#availableclasses').text("Error Occured")
        }
    });
}

function completed() {
    $.ajax({
        type: "GET",
        url: "/api/CompletedCourseSchedule",
        success: function (data) {
            $('#completedclasses').text(data)
        },
        error: function () {
            $('#completedclasses').text("Error Occured")
        }
    });
}





var url = "/api/Course";


var c2dataStore = new DevExpress.data.CustomStore({
    key: "subcategoryId",
    type: "array",
    loadMode: "raw",
    cacheRawData: false,
    load: function (loadOptions) {
        var d = $.getJSON("/api/Course/SubCategory");
        return d;
    },
      byKey: function (key) {
          return $.getJSON("/api/Course/SubCategory/" + encodeURIComponent(key));
      }
      , insert: function (values) {
          return $.post("/api/Course/addsubCategory", values);
      },
      add: function (values) {
          return $.ajax({
              url: "/api/Course/addsubCategory",
              method: "POST",
              data: values
          });
      },
      update: function (key, values) {
          return $.ajax({
              url: "/api/Course/updatesubcategory",
              method: "PUT",
              data: values
          });
      },
      remove: function (key) {
          return $.ajax({
              url: "/api/Course/deletesubcategory/" + encodeURIComponent(key),
              method: "DELETE"

          });
      }
});

var c2dataSource = new DevExpress.data.DataSource({
    store: c2dataStore
});

var cdataStore = new DevExpress.data.CustomStore({
    key: "categoryID",
    type: "array",
    loadMode: "raw",
    cacheRawData: false,
    load: function (loadOptions) {
        var d = $.getJSON("/api/Course/Category");
        return d;
    }
    , insert: function (values) {
        return $.post("/api/Course/addCategory", values);
    },
    add: function (values) {
        return $.ajax({
            url: "/api/Course/addCategory",
            method: "POST",
            data: values
        });
    },
    update: function (key, values) {
        return $.ajax({
            url: "/api/Course/updatecategory",
            method: "PUT",
            data: values
        });
    },
    remove: function (key) {
        return $.ajax({
            url: "/api/Course/deletecategory/" + encodeURIComponent(key),
            method: "DELETE"

        });
    }
});

var cdataSource = new DevExpress.data.DataSource({
    store: cdataStore
});



var dataStore = new DevExpress.data.CustomStore({
    key: "courseId",
    type: "array",
    loadMode: "raw",
    cacheRawData: false,

    load: function (loadOptions) {
        var d = $.getJSON(url);
        return d;
    },
    byKey: function (key) {
        return $.getJSON(url + "/" + encodeURIComponent(key));
    }

    , insert: function (values) {
        return $.post("/api/Course/addCourse", values);
    },
    add: function (values) {
        return $.ajax({
            url: "/api/Course/addCourse",
            method: "POST",
            data: values
        });
    },
    update: function (key, values) {
        return $.ajax({
            url: "/api/Course/updateCourse",
            method: "PUT",
            data: values
        });
    },
    remove: function (key) {
        return $.ajax({
            url: "/api/Course/deleteCourse/" + encodeURIComponent(key),
            method: "DELETE"
     
        });
    }
});

var dataSource = new DevExpress.data.DataSource({
    store: dataStore
  
});



    
$(function () {
           
        $("#courseGrid").dxDataGrid({
            dataSource: dataSource,
           
            editing: {
                mode: "popup",
                allowAdding: true,
                allowUpdating: true,
                allowDeleting: true,
                useIcons: true
            },
            
            onEditorPreparing: function(e) {
                if(e.parentType === "dataRow" && e.dataField === "subcategoryID") {
                    e.editorOptions.disabled = (typeof e.row.data.courseSubcategory.category.categoryID !== "number");
                }
            },
            Selection: {
                mode: "single"
            },
            onToolbarPreparing: function (e) {
                var dataGrid = e.component;
                e.toolbarOptions.items.unshift({
                    location: "after",
                    widget: "dxButton",
                    options: {
                        icon: "refresh",
                        onClick: function () {
                            dataGrid.refresh();
                        }
                    }
                });
                e.toolbarOptions.items.unshift({
                    location: "after",
                    widget: "dxButton",
                    options: {
                        text:"Manage Category",
                        onClick: function () {
                              $("#catGrid").dxPopup("instance").show();
                         
                        }
                    }
                });
                e.toolbarOptions.items.unshift({
                    location: "after",
                    widget: "dxButton",
                    options: {
                        text: "Manage SubCategory",
                        onClick: function () {
                            $("#subcatGrid").dxPopup("instance").show();

                        }
                    }
                });
            },
            onRowUpdating: function (options) {
                $.extend(options.newData, $.extend(true, {}, options.oldData, options.newData));
            },
            Paging: {
                pageSize: 10
            },
            pager: {
                showPageSizeSelector: true,
                allowedPageSizes: [10, 25, 50, 100],
                showInfo: true,
                 infoText: "showing page {0} of {1} ({2} courses)"
            },
            allowColumnReordering: true,
            allowColumnResizing: true,
            rowAlternationEnabled: true,
            hoverStateEnabled: true,
            onEditorPreparing: function (e) {
                if (e.dataField === "courseDescription")
                    e.editorName = "dxTextArea";
            
            },
           
            columns: [{
                dataField: "courseId",
                caption: "ID",
                allowedit:false,
                width: "5%",
                allowEditing: false,
            }, {
                dataField: "courseCode",
                dataType : "number",
                caption: "Code",
                width: "5%",
                validationRules: [{
                    type: "required",
                    message: "The Course Code is required."
                }]
            }, {
                dataField: "courseName",
                caption: "Name",
                width: "15%",
                validationRules: [{
                    type: "stringLength",
                    message: "The Course name must be between 5 and 100 characters.",
                    min: 5,
                    max: 120
                }]
            }, {
                dataField: "courseDescription",
                caption: "Description",
                width: "17%",
                validationRules: [{
                    type: "stringLength",
                    message: "The field Course Description must be a string with a maximum length of 1500.",
                    max: 1500
                }]
            },
            {
                dataField: "duration",
                caption: "Duration (Hours)",
                width: "11%",
                validationRules: [{
                    type: "range",
                    message: "Duration must be between 0 and 8 hours.",
                    max: 8,
                    min:0
                }]
            },
             {
                 dataField: "coursePrice",
                 caption: "Price",
                 width: "10%",
                 format: function (s) {
                     var formated = (s).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                     return "$" + formated;
                 },
                 editorName: "dxNumberBox",
                 editorOptions: {
                     format: "$ #,##0.00"
                 },

                 allowEditing: true
              
             },
            {
                dataField: "pass_Fail",
                caption: "Grade",
                width: "7%",
                allowEditing: true,
                validationRules: [{
                    type: "range",
                    message: "Passing grade should be between 0 and 100.",
                    max: 100,
                    min:0
                }],
             
            },
            {
                dataField: "certificateExpirationDays",
                caption: "Cert Exp. Days",
                width: "10%",
                allowEditing: true,
                validationRules: [{
                    type: "range",
                    message: " be between 0 and 2000.",
                    max: 2000,
                    min: 0
                }],

            },
              {
                  dataField: "courseSubcategory.category.categoryID",
                  caption: "Category",
                  width: "10%",
                  allowAdding: false,
                  allowUpdating: false,
                  allowDeleting: false,
                    setCellValue: function (rowData, value) {
                        rowData.subcategoryId = null;
                      this.defaultSetCellValue(rowData, value);
                  }
               , lookup: {
                   dataSource: cdataStore,

                   valueExpr: "categoryID",
                   displayExpr: "categoryName"
               }
              }
            ,
            {
                dataField: "courseSubcategory.subcategoryId",
                caption: "Sub Category",
                width: "10%"
                
                , lookup: {

                    dataSource: function (options) {
                        var dataSourceConfiguration = {
                            store: c2dataStore
                        };
                        try {
                      
                            dataSourceConfiguration.filter = ['categoryId', '=', options.data.courseSubcategory.category.categoryID];
                        }
                        catch (e) {

                        }
                        return dataSourceConfiguration;

                        
                    },
                   valueExpr: "subcategoryId",
                   displayExpr: "subcategoryName"
               }

               
            }
            
          
            ],
            filterRow: {
                visible: true
            },
            groupPanel: {
                visible: true
            },
            scrolling: {
                mode: "fixed"
            },

            
        });
    });

$(function () {
    $("#catGrid").dxPopup({
        height:"auto",
        title: "Manage Categories",
        visible: false,
        closeOnBackButton: false,
        closeOnOutsideClick: true,
        onShown: function (e) {
            e.element.attr('act', 0);
            $("#categoryGrid").dxDataGrid({
                dataSource: cdataSource,
                editing: {
                    mode: "popup",
                    allowAdding: true,
                    allowUpdating: true,
                    allowDeleting: true,
                    useIcons: true
                },
                Selection: {
                    mode: "single"
                },
                onToolbarPreparing: function (e) {
                    var dataGrid = e.component;
                    e.toolbarOptions.items.unshift({
                        location: "after",
                        widget: "dxButton",
                        options: {
                            icon: "refresh",
                            onClick: function () {
                                dataGrid.refresh();
                            }
                        }
                    });
                   
                },
                onRowUpdating: function (options) {
                    $.extend(options.newData, $.extend(true, {}, options.oldData, options.newData));
                },
                Paging: {
                    pageSize: 10
                },
                pager: {
                    showPageSizeSelector: true,
                    allowedPageSizes: [10, 25, 50, 100],
                    showInfo: true,
                    infoText: "showing page {0} of {1} ({2} categories)"
                },
                allowColumnReordering: true,
                allowColumnResizing: true,
                rowAlternationEnabled: true,
                hoverStateEnabled: true,
                filterRow: {
                    visible: true
                },
                groupPanel: {
                    visible: false
                },
                scrolling: {
                    mode: "virtual"
                },
                columns: [
                    {
                        dataField: 'categoryID',
                        caption: "ID",
                        width: 100,
                        allowAdding: false,
                        allowEditing: false,
                        formItem: {
                            visible: false
                        }
                    },
                   
                    {
                        dataField: 'categoryName',
                        caption: "Category",
                        width:250
                    },
                     {
                         dataField: 'categoryDescription',
                         caption: "Description",
                     }
                ]

            });

        },
        contentTemplate: function () {
            return $("<div />").append(
             $("<div id='categoryGrid' > "
            )
                );
        }
    });
});

$(function () {
    $("#subcatGrid").dxPopup({
        height: "auto",
        title: "Manage SubCategories",
        visible: false,
        closeOnBackButton: false,
        closeOnOutsideClick: true,
        onShown: function (e) {
            e.element.attr('act', 0);
            $("#subcategoryGrid").dxDataGrid({
                dataSource: c2dataSource,
                editing: {
                    mode: "popup",
                    allowAdding: true,
                    allowUpdating: true,
                    allowDeleting: true,
                    useIcons: true
                },
                Selection: {
                    mode: "single"
                },
                onToolbarPreparing: function (e) {
                    var dataGrid = e.component;
                    e.toolbarOptions.items.unshift({
                        location: "after",
                        widget: "dxButton",
                        options: {
                            icon: "refresh",
                            onClick: function () {
                                dataGrid.refresh();
                            }
                        }
                    });

                },
                onRowUpdating: function (options) {
                    $.extend(options.newData, $.extend(true, {}, options.oldData, options.newData));
                },
                Paging: {
                    pageSize: 10
                },
                pager: {
                    showPageSizeSelector: true,
                    allowedPageSizes: [10, 25, 50, 100],
                    showInfo: true,
                    infoText: "showing page {0} of {1} ({2} sub categories)"
                },
                allowColumnReordering: true,
                allowColumnResizing: true,
                rowAlternationEnabled: true,
                hoverStateEnabled: true,
              
                columns: [
                    {
                        dataField: 'subcategoryId',
                        caption: "ID",
                        width: 80,
                        allowAdding: false,
                        allowEditing: false,
                        formItem: {
                            visible: false
                        }
                    },

                    {
                        dataField: 'subcategoryName',
                        caption: "Sub Category",
                        width: 250,
                        validationRules: [{
                            type: "required"
                        }]
                    },
                     {
                         dataField: 'categoryId',
                         caption: "Category",
                         validationRules: [{
                            type:"required"
                         }],
                          lookup: {
                dataSource: cdataStore,

                valueExpr: "categoryID",
                displayExpr: "categoryName"
                         }
                     }
                ],
                filterRow: {
                    visible: true
                },
                groupPanel: {
                    visible: false
                },
                scrolling: {
                    mode: "virtual"
                }

            });

        },
        contentTemplate: function () {
            return $("<div />").append(
             $("<div id='subcategoryGrid' > "
            )
                );
        }
    });
});







