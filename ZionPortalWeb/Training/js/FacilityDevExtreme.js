﻿var cities;
var states
getcities()
getcitystates()




facilitycount();
roomcount();

function getcities() {
    $.ajax({
        type: "GET",
        url: "/api/Location/LocationByStateID/181",
        async: false,
        success: function (data) {
            states = data;
        },
        error: function () {
            alert("error")
        }
    });
}


function getcitystates() {
    $.ajax({
        type: "GET",
        url: "/api/Location/allLocations",
        async: false,
        success: function (data) {
            cities = data;
      
        },
        error: function () {
            alert("error")
        }
    });
}

//alert(cities)

function facilitycount() {
    $.ajax({
        type: "GET",
        url: "/api/facility/count",
        success: function (data) {
            $('#facilities').text(data)
        },
        error: function () {
            $('#facilities').text("Error Occured")
        }
    });
}

function roomcount() {
    $.ajax({
        type: "GET",
        url: "/api/facility/roomcount",
        success: function (data) {
            $('#rooms').text(data)
        },
        error: function () {
            $('#rooms').text("Error Occured")
        }
    });
}



var url = "/api/Facility";

var dataStore = new DevExpress.data.CustomStore({
    key: "facilityId",
    type: "array",
    loadMode: "raw",
    cacheRawData: false,
    load: function (loadOptions) {
        var d = $.getJSON(url);
        return d;
    },
    byKey: function (key) {
        return $.getJSON(url + "/" + encodeURIComponent(key));
    }, insert: function (values) {
        return $.post(url, values);
    },
    add: function (values) {
        return $.ajax({
            url: url,
            method: "POST",
            data: values
        });
    },
    update: function (key, values) {
        return $.ajax({
            url: url,
            method: "PUT",
            data: values
        });
    },
    remove: function (key) {
        return $.ajax({
            url: url + "/" + encodeURIComponent(key),
            method: "DELETE",
        });
    }
});

var dataSource = new DevExpress.data.DataSource({
    store: dataStore
});





$(function () {

    $("#facilityGrid").dxDataGrid({
        dataSource: dataSource,
        editing: {
            mode: "row",
            allowAdding: true,
            allowUpdating: true,
            allowDeleting: true,
            useIcons: true
        },
        onRowExpanding: function (e) {
            e.component.collapseAll(-1);
        },
        onToolbarPreparing: function (e) {
            var dataGrid = e.component;
            e.toolbarOptions.items.unshift({
                location: "after",
                widget: "dxButton",
                options: {
                    icon: "refresh",
                    onClick: function () {
                        dataGrid.refresh();
                    }
                }
            });
        },
        onRowUpdating: function (options) {
            $.extend(options.newData, $.extend(true, {}, options.oldData, options.newData));
        },
        Paging: {
            pageSize: 10
        },
        pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 25, 50, 100]
        },
        allowColumnReordering: true,
        allowColumnResizing: true,
        rowAlternationEnabled: true,
        hoverStateEnabled: true,
        
        columns: [{
            dataField: "facilityId",
            caption: "ID",
            allowedit: false,
            width: "5%",
            allowEditing: false,
        }, {
            dataField: "facilityName",
            caption: "Facility",
            width: "25%",
            validationRules: [{
                type: "required",
                message: "The facility is required."
            }]
        }, {
            dataField: "phone",
            caption: "Phone",
            width: "15%",
            caption: "Phone",
            format: function formatPhoneNumber(s) {
                var s2 = ("" + s).replace(/\D/g, '');
                var m = s2.match(/^(\d{3})(\d{3})(\d{4})$/);
                return (!m) ? null : "+1" + "(" + m[1] + ") " + m[2] + "-" + m[3];
            },
            mask: "+1 (000) 000-0000",
            editorOptions: {
                mask: "+1 (000) 000-0000",
            }
         
        }
        , {
            dataField: "addressLine1",
            caption: "Address 1",
            width: "25%",
            validationRules: [{
                type: "stringLength",
                message: "The field Address 1 must be a string with a maximum length of 150.",
                max: 150
            }]
        }
        ,
         {
             dataField: "city",
             caption: "City",
             width: "10%",
             lookup: {
                 dataSource: cities,
                 valueExpr: "cityName",
                 displayExpr: "cityName"
             }
         },
          {
             dataField: "state",
              caption: "State",
              width: "10%",
            
             lookup: {
             dataSource: states,
                 valueExpr: "stateName",
             displayExpr: "stateName"
             
             }
          },
           {
               dataField: "postalCode",
               caption: "Zip Code",
                width: "10%"
           }
       
        ],
        masterDetail: {
            enabled: true,
            template: function (container, options) {
            
                var d2 = options.data.rooms

           

                $("<div>")
                    .dxDataGrid({
                        dataSource: d2,
                        allowColumnReordering: true,
                        allowColumnResizing: true,
                        rowAlternationEnabled: true,
                        hoverStateEnabled: true,
                        columns: [{
                            dataField: "roomId",
                            caption: "Room ID",
                            width:80
                           
                        },
                        {
                            dataField: "roomName",
                            caption: "Room Name",
                         
                        },
                        {
                            dataField: "roomCapacity",
                            caption: "Room Capacity",
                            width:130
                         
                        }
                    
                      


                        ],

                        showBorders: false,

                        filterRow: {
                            visible: false
                        },
                        groupPanel: {
                            visible: false
                        },
                        scrolling: {
                            mode: "virtual"
                        },
                    }).appendTo(container);
            }
        },
        filterRow: {
            visible: true
        },
        headerFilter: {
            visible: true
        },
        groupPanel: {
            visible: true
        },
        scrolling: {
            mode: "virtual"
        },
        showBorders: true,


        grouping: {
            autoExpandAll: false
        }
    });
});



