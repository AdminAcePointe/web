﻿//////////////////////////    Employee form
var isclicked = 0;
var employee
var ddata
var page = sessionStorage.getItem("page")
var GroupHomes_
var Shifts_
var Managers
var Titles
var states
var Genders
var relationships
var MaritalStatuses
var MembersWithServices
var MemberServices
var MemberServiceInfo;
var EmployeeMemberServices;
var employeeMembersAssigned;
var employeeServicePayrate;
var MemberServiceList;
var propic;

var employeeuser = sessionStorage.getItem("employeeuser")



isclicked = sessionStorage.getItem("isclicked")

sessionStorage.setItem("isclicked",0)


if (isclicked == 0 || isclicked == null) {
    employeeuser = sessionStorage.getItem("employeeid")
    sessionStorage.setItem("employeeuser",null)
}

if (isclicked == 2) {
    employeeuser = -1
    sessionStorage.setItem("employeeuser", null)
}



//EmployeeProfile
getEmployeeDetails();

function getEmployeeDetails() {
    $.ajax({
        type: "GET",
        url: "/api/personDetail/" + employeeuser,
        async:false,
        success: function (data) {
            employee = data;
        },
        error: function () {
            alert("Error Occured retrieving employee details")
        }
    });
}

getGroupHomeList();

function getGroupHomeList() {
    $.ajax({
        type: "GET",
        url: "/api/groupHomeList/",
        async: false,
        success: function (data) {
            GroupHomes_ = data;
        },
        error: function () {
            alert("Error Occured retrieving list of group homes")
        }
    });
}

getShiftList(); 

function getShiftList() {
    $.ajax({
        type: "GET",
        url: "/api/shiftList/",
        async: false,
        success: function (data) {
            Shifts_ = data;
        },
        error: function () {
            alert("Error Occured retrieving list of shifts")
        }
    });
}

getManagerList()

function getManagerList() { 
    $.ajax({
        type: "GET",
        url: "/api/managerList/",
        async: false,
        success: function (data) {
            Managers = data;
        },
        error: function () {
            alert("Error Occured retrieving list of managers")
        }
    });
}

getRoleList()

function getRoleList() { 
    $.ajax({
        type: "GET",
        url: "/api/roleList/",
        async: false,
        success: function (data) {
            Titles = data;
        },
        error: function () {
            alert("Error Occured retrieving list of roles")
        }
    });
}

getStates() 
function getStates() {
    $.ajax({
        type: "GET",
        url: "/api/stateList/",
        async: false,
        success: function (data) {
            states = data;
        },
        error: function () {
            alert("Error Occured retrieving list of states")
        }
    });
}

getGender()
function getGender() {
    $.ajax({
        type: "GET",
        url: "/api/genderList/",
        async: false,
        success: function (data) {
            Genders = data;
        },
        error: function () {
            alert("Error Occured retrieving list of gender")
        }
    });
}

getRelationships()
function getRelationships() {
    $.ajax({
        type: "GET",
        url: "/api/relationshipList/",
        async: false,
        success: function (data) {
            relationships = data;
        },
        error: function () {
            alert("Error Occured retrieving list of relationships")
        }
    });
}

getMaritalStatuses()
function getMaritalStatuses() {
    $.ajax({
        type: "GET",
        url: "/api/maritalStatusList/",
        async: false,
        success: function (data) {
            MaritalStatuses = data;
        },
        error: function () {
            alert("Error Occured retrieving list of marital statuses")
        }
    });
}

getMembersWithServicesList();

function getMembersWithServicesList() {
    $.ajax({
        type: "GET",
        url: "/api/Member/GetMembersWithServices" ,
        success: function (data) {
            MembersWithServices = data;
        },
        error: function () {
            alert("Error Occured retrieving list of Members")
        }
    });
}



getServicesList();
function getServicesList() {
    $.ajax({
        type: "GET",
        async: false,
        url: "/api/MemberServices/GetServices",
        success: function (data) {
             MemberServices = data;
        },
        error: function () {
            alert("Error Occured retrieving list of services")
        }
    });
}


getEmployeeServiceList();
function getEmployeeServiceList() {
    $.ajax({
        type: "GET",
        url: "/api/Employee/GetSEmployeeServices/" + employeeuser,
        async: false,
        success: function (data) {
            EmployeeMemberServices = data
            
        },
        error: function () {
            alert("Error Occured retrieving list of member Services")
        }
    });
}

getEmployeeMembersList();
function getEmployeeMembersList() {
    $.ajax({
        type: "GET",
        url: "/api/MemberServices/MemberAssignedToEmployee/" + employeeuser,
        async: false,
        success: function (data) {
            employeeMembersAssigned = data

        },
        error: function () {
            alert("Error Occured retrieving list of member Services")
        }
    });
}

GetServiceEmployeePayRates();
function GetServiceEmployeePayRates() {
    $.ajax({
        type: "GET",
        url: "/api/GetServiceEmployeePayRates/" + employeeuser,
        async: false,
        success: function (data) {
            employeeServicePayrate = data

        },
        error: function () {
            alert("Error Occured retrieving list of employee pay rates")
        }
    });
}

GetServiceList();
function GetServiceList() {
    $.ajax({
        type: "GET",
        url: "/api/MemberServices/GetMemberService",
        async: false,
        success: function (data) {
            MemberServiceList = data

        },
        error: function () {
            alert("Error Occured retrieving list of services")
        }
    });
}







var role = sessionStorage.getItem("userrole");
var CanEditPay = false;
if (role == "Administrator") {
    CanEditPay = true;
}

var imgSrc = []


$(function () {
    $("#form").dxForm({
        formData: employee,
        readOnly: function () {
            if (isclicked == 2) {

                return false
            }
            else return true
        },
        showColonAfterLabel: false,
        labelLocation: "left",
        onContentReady: function () { newempenableform()  },
        items: [
            {
            
            itemType: "group",
            cssClass: "first-group",
            colCount: 3,
                items: [

                   
                    {
                        itemType: "group",
                        items: [
                            {
                                label: {
                                    text: "personPhoto",
                                    visible:false
                                },

                                dataField: "personPhoto",
                                template: function (data, itemElement) {
                                    $('<img class="form-avatar" id="propic">').attr("src", data.editorOptions.value).appendTo(itemElement);
                                }
                            }
                        ,
                           {
                                
                               label: {
                                   text: "personPhoto",
                                   visible: false
                               },
                                dataField: "",
                                template: function (data, itemElement) {
                                    itemElement.append($("<div>").attr("id", "dxfu1").dxFileUploader({
                                        labelText: "",
                                        selectButtonText: "Add/ Change Photo",
                                        showFileList: false,
                                        accept: "image/*",
                                        uploadMode: "instantly",
                                        value: [],
                                        multiple: false,
                                        uploadUrl: "/api/util/SaveProfilePhoto",
                                        onValueChanged: function (e) {
                                            var files = e.value;

                                            $.each(files, function (i, file) {
                                                propic = "../img/ProfilePhoto/"+file.name
                                                var fileURL = URL.createObjectURL(file);
                                                $("#propic").attr("src", fileURL)
                                            })
                                            
                                              
                                            
                                        }
                                    }));
                                }
                            }


                        ]
                            }     
                    
                , {
                itemType: "group",
                colSpan: 3,
                items: [{
                    dataField: "firstName"
                      ,validationRules: [{
                        type: "required",
                        message: "First Name is required"
                    }, {
                        type: "pattern",
                        pattern: "^[a-zA-Z]+$",
                        message: "The name should not contain digits"
                    }]
                }, {
                        dataField: "lastName"
                        , validationRules: [{
                            type: "required",
                            message: "First Name is required"
                        }, {
                            type: "pattern",
                            pattern: "^[a-zA-Z]+$",
                            message: "The name should not contain digits"
                        }]
                    }
                    , {
       
                    dataField: "dateOfBirth",
                       editorType: "dxDateBox",
                   
                        
                    editorOptions: {
                        width: "100%",
                        displayFormat: 'MM-dd-yyyy'
                    }
                    }
                ]
                }
                , {
                    itemType: "group",
                    colSpan: 3,
                    items: [{
                        dataField: "maritalStatus",
                        editorType: "dxSelectBox",
                        editorOptions: {
                            items: MaritalStatuses
                        }
                    }, {
                        dataField: "gender",
                        editorType: "dxSelectBox",
                        editorOptions: {
                            items: Genders
                        }
                    }
                        , {
                            label: {
                                text: "SSN"
                            },
                            dataField: "ssn",
                        mode:"password",
                       
                            editorOptions: {
                             //   type: 'numeric'
                        
                        }
                            , validationRules: [
                                {
                                    type: "stringLength",
                                    min: 9,
                                    max: 9,
                                    message: "SSN should contain 9 digits"
                                },
                                {
                            type: "required"
                        }
                                ]
                    }]
                }
            ]
        }
            ,{
            itemType: "group",
                cssClass: "second-group",
                caption: "Personal Information",
                colCount: 2,
                items: [{
                    itemType: "group",
                    items: [{
                        label: {
                            text: "Address"
                        },
                        dataField: "addressline1"
                        , validationRules: [{
                            type: "required",
                            message: "Please enter address"
                        }]
                    }, {
                        dataField: "city"
                        , validationRules: [{
                            type: "required",
                            message: "Please enter city"
                        }]
                    }, {
                        dataField: "email",
                        validationRules: [

                            {
                                type: 'email'
                            }, {
                                type: 'required',
                                message: 'Please enter valid email'
                            }]

                    }]
                }
                    , {
                    itemType: "group",
                    items: [{
                        dataField: "state",
                        editorType: "dxSelectBox",
                        editorOptions: {
                            items: states
                        },

                        validationRules: [{
                            type: 'required',
                            message: 'Please select state'
                        }]
                    }, {
                        dataField: "zipCode"
                        ,
                        validationRules: [

                            {
                                type: "stringLength",
                                min: 5,
                                max: 5,
                                message: "Zip should contain 5 digits"
                            }
                            , {
                                type: 'required',
                                message: 'Please enter valid zip code'
                            }]
                    }, {
                        dataField: "phoneNumber",
                        label: {
                            text: "Phone"
                        },
                        editorOptions: {
                            mask: "+1 (000) 000-0000"
                        }
                       
                    }]
                }
                ]
            }


           , {
                itemType: "group",
                cssClass: "second-group",
                caption: "Work Information",
                colCount: 3,
               items: [
                   {
                    itemType: "group",
                    items: [ 
                         {
                            dataField: "title",
                            editorType: "dxSelectBox",
                            editorOptions: {
                                items: Titles
                            },
                            validationRules: [ {
                                type: 'required',
                                message: 'Please select title'
                            }]
                        }
                       
                        , {
                            dataField: "groupHome",
                            editorType: "dxSelectBox",
                            editorOptions: {
                                items: GroupHomes_
                            },
                            validationRules: [{
                                type: 'required',
                                message: 'Please choose group home'
                            }]
                        }
                      
                       
                       

                    ]
                }
                 , {
                    itemType: "group",
                        items: [
                           
                            {
                                dataField: "manager",
                                editorType: "dxSelectBox",
                                editorOptions: {
                                    items: Managers
                                },
                                
                            validationRules: [{
                                type: 'required',
                                message: 'Please choose manager'
                            }]
                            }
                          ,
                                  {
                                      dataField: "shift",
                                editorType: "dxSelectBox",
                                editorOptions: {
                                    items: Shifts_
                                      },
                                
                            validationRules: [{
                                type: 'required',
                                message: 'Please choose shift'
                            }]
                            }
                    ]
                }
                 , {
                        itemType: "group",
                        items: [
                        
                             {

                                dataField: "hireDate",
                                editorType: "dxDateBox",
                                format:"Date",
                                editorOptions: {
                                    width: "100%",
                                    displayFormat: 'MM-dd-yyyy'
                                }
                            }
                               
                            , {
                                width: "50%",

                                dataField: "isActive",
                                editorType: "dxCheckBox",
                                editorOptions: {
                                    elementAttr: { 'class': 'chk' }
                                }
                                ,
                                validationRules: [{
                                    type: 'required',
                                    message: 'Please select active status'
                                }]
                            }




                        ]
                    }

                ]
            }
            , {
                itemType: "group",
                cssClass: "second-group",
                caption: "Member Related Information",
                colCount: 1,
                items: [{
                    itemType: "group",
                    items: [
                        {
                            dataField: "",
                            label: { text: "Services" },
                            editorType: "dxTagBox",

                            editorOptions: {
                                items: MemberServices,
                                value: EmployeeMemberServices,
                                searchEnabled: true,
                                showClearButton: true,
                                hideSelectedItems: true,
                                showSelectionControls: true,
                                applyValueMode: "useButtons",
                                elementAttr: { 'class': 'servicesTag' }
                            }
                            ,
                            validationRules: [{
                                type: 'required',
                                message: 'Please choose a service'
                            }]
                        }
                    //,
                        //{
                        
                        //    label: { text: "Assigned Members" },
                        //    editorType: "dxTagBox",
                         
                        //    editorOptions: { 
                        //        items: MembersWithServices,
                        //        value: employeeMembersAssigned,

                        //        searchEnabled: true,
                        //        showClearButton: true,
                        //        hideSelectedItems: true,
                        //        showSelectionControls: true,
                        //        applyValueMode: "useButtons",
                        //        elementAttr: { 'class': 'membersTag' }
                                
                        //    }
                          
                        //}
                    ]
                }
               
                ]
            }
               

            , {
                itemType: "group",
                cssClass: "second-group",
              //  caption: "Personal Information",
                //colCount: 3,
                items: [
                   
                    {
                    itemType: "group",
                    items: [{
                        itemType: "tabbed",
                        tabPanelOptions: {
                            deferRendering: false
                        },
                        tabs: [
                            {
                                title: "Emergency Contact",
                                colCount: 3,
                                items: [
                                    {

                                        dataField: "contactFirstName"
                                        , validationRules: [{
                                            type: 'required',
                                            message: 'Please enter contact first name'
                                        }]


                                    }
                                    ,
                                    {

                                        dataField: "contactLastName"
                                        , validationRules: [{
                                            type: 'required',
                                            message: 'Please enter contact last name'
                                        }]



                                    }


                                    ,
                                    {

                                        dataField: "contactPhone"
                                        ,
                                        editorOptions: {
                                            mask: "+1 (000) 000-0000"
                                        }
                                        , validationRules: [{
                                            type: 'required',
                                            message: 'Please enter phone'
                                        }]


                                    }
                                    ,
                                    {

                                        dataField: "contactEmail"
                                        , validationRules: [
                                            {
                                                type: "email"
                                            }
                                            , {
                                                type: 'required',
                                                message: 'Please enter contact first name'
                                            }]



                                    }
                                    ,
                                    {
                                        dataField: "relationship",
                                        editorType: "dxSelectBox",
                                        editorOptions: {
                                            items: relationships
                                        }
                                        , validationRules: [{
                                            type: 'required',
                                            message: 'Please choose relationship type'
                                        }]

                                    }




                                ]
                            }
                            ,
                            {
                            colCount: 1,
                            title: "Pay Information",
                                items: [
                                    {
                                        dataField: "",
                                        template: function (data, itemElement) {
                                            itemElement.append("<div id='payGrid'>")
                                                .dxDataGrid({
                                                    dataSource: employeeServicePayrate,
                                                   
                                                    editing: {

                                                        allowAdding: CanEditPay,
                                                        allowUpdating: CanEditPay,
                                                        allowDeleting: CanEditPay,
                                                        useIcons: true
                                                    },
                                                    

                                                    onEditorPreparing: function (e) {

                                                    },
                                                    Selection: {
                                                        mode: "single"
                                                    },
                                                    onToolbarPreparing: function (e) {
                                                        var dataGrid = e.component;
                                                  

                                                    },
                                                    onContentReady: function (e) {
                                                        e.component.option.disabled = true
                                                    },
                                                    onRowUpdating: function (options) {
                                                        $.extend(options.newData, $.extend(true, {}, options.oldData, options.newData));
                                                    },
                                                    Paging: {
                                                        pageSize: 5
                                                    },
                                                    pager: {
                                                        showPageSizeSelector: true,
                                                        allowedPageSizes: [5, 25, 50, 100],
                                                        showInfo: true,
                                                        infoText: "showing page {0} of {1} ({2} Pay rates)"
                                                    },
                                                    allowColumnReordering: false,
                                                    allowColumnResizing: false,
                                                    rowAlternationEnabled: false,
                                                    hoverStateEnabled: true,
                                                    showColumnLines: true,
                                                    showRowLines: false,
                                                    showBorders:true,

                                                    columns: [
                                                        {
                                                            dataField: "payrateid",
                                                            caption: "ID",
                                                            allowedit: false,
                                                            width: "5%",
                                                            allowEditing: false,
                                                        }

                                                        , {
                                                            dataField: "memberservicename",
                                                            caption: "Service",
                                                            width: "60%",
                                                            validationRules: [{
                                                                type: "required"
                                                            }]
                                                        
                                                              , lookup: {
                                                                  dataSource: MemberServiceList,

                                                                  valueExpr: "memberServiceName",
                                                                  displayExpr: "memberServiceName"
                                                        }
                                                        },
                                                        {
                                                            dataField: "payrate",
                                                            caption: "Pay rate (hourly)",
                                                            width: "15%",
                                                            alignment: "center",
                                                            format: "$ #,##0.00",
                                                            editorOptions: {
                                                                format: "$ #,##0.00"
                                                            },
                                                            validationRules: [{
                                                                type: "required"
                                                            }]

                                                        }
                                                        ,

                                                        {
                                                            dataField: "payratestartdate",
                                                            caption: "Effective Date",
                                                            width: "20%",
                                                            dataType: "date",
                                                            alignment: "center",
                                                            validationRules: [{
                                                                type: "required"
                                                            }]


                                                        }
                                                    ],
                                                    filterRow: {
                                                        visible: false
                                                    },
                                                    groupPanel: {
                                                        visible: false
                                                    },
                                                    scrolling: {
                                                        mode: "fixed"
                                                    },


                                                })
                                        }
                                    }
                             
                            ]
                        }
                        , {
                            title: "Other",
                            colCount: 4,
                                items: [
                                   
                                    {
                                        width: "20%",
                                        dataField: "isInstructor",
                                        editorType: "dxCheckBox",
                                        editorOptions: {
                                            elementAttr: { 'class': 'chk' }
                                        }
                                    }
                                    ,
                                    {
                                        width: "20%",
                                        dataField: "isStudent",
                                        editorType: "dxCheckBox",
                                        editorOptions: {
                                            elementAttr: { 'class': 'chk' }
                                        }
                                    }
                                    , {
                                        dataField: "personID",
                                    editorOptions: {
                                        disabled: true
                                        }
                                    }
                                    , {
                                        dataField: "employeeID"
                                        ,
                                        editorOptions: {
                                            disabled:true
                                        }
                                    }  

                                ]
                        }]
                    }]
                }


                ]
            }
        ]
    });
});



$("#btn").dxButton({
    text: function () {
        
        if (isclicked == 2) {
            return "Save Profile"
        }
        else return "Edit Profile"
    },
    type: "success",
    onClick: function (e) {
        var t = $("#btn").dxButton("instance");
        var text = $("#btn").text();
        var form = $("#form").dxForm("instance");
       

        if (text == 'Edit Profile') {

            form.option("readOnly", false);
     
            
            disableformelements(form) 
            

            ddata = form.option('formData');

            t.option("text", "Save Profile");
        }
        else if (text == 'Save Profile') {
       
            var result = form.validate();

            ddata = form.option('formData');

            if (result.isValid) {
                postEmployeeDetails();
            }
            else {
                DevExpress.ui.notify({
                    message: "please fill out all fields"
                    , position: "{my:'center', at:'center'}"
                    , type: "error"
                    ,displayTime:5000
                });
            }
          



        }
        // form.option("readOnly", !form.option("readOnly"));

    }
});


function disableformelements(form) {

    if (role != "Administrator") {
        form.getEditor("firstName").option("disabled", true);
        form.getEditor("lastName").option("disabled", true);


        form.getEditor("maritalStatus").option("disabled", true);
        form.getEditor("gender").option("disabled", true);
        form.getEditor("dateOfBirth").option("disabled", true);

        form.getEditor("ssn").option("disabled", true);
        form.getEditor("title").option("disabled", true);
        form.getEditor("groupHome").option("disabled", true);

        form.getEditor("manager").option("disabled", true);
        form.getEditor("shift").option("disabled", true);
        form.getEditor("isActive").option("disabled", true);

        form.getEditor("hireDate").option("disabled", true);

    
        $('#payGrid').attr("disabled","disabled")
        //form.getEditor("effectiveDate").option("disabled", true);
        form.getEditor("isStudent").option("disabled", true);

        form.getEditor("isInstructor").option("disabled", true);

        form.getEditor("personID").option("disabled", true);

        form.getEditor("employeeID").option("disabled", true); 

        $(".servicesTag").dxTagBox("instance").option("readOnly", true)
       
    }

}

function enableformelements(form) {
    var status = false

    form.getEditor("firstName").option("disabled", status);
    form.getEditor("lastName").option("disabled", status);


    form.getEditor("maritalStatus").option("disabled", status);
    form.getEditor("gender").option("disabled", status);
    form.getEditor("dateOfBirth").option("disabled", status);

    form.getEditor("ssn").option("disabled", status);
    form.getEditor("title").option("disabled", status);
    form.getEditor("groupHome").option("disabled", status);


    form.getEditor("manager").option("disabled", status);
    form.getEditor("shift").option("disabled", status);
    form.getEditor("isActive").option("disabled", status);

    form.getEditor("hireDate").option("disabled", status);

    //form.getEditor("effectiveDate").option("disabled", status);
    form.getEditor("isStudent").option("disabled", status);

    form.getEditor("isInstructor").option("disabled", status);

    form.getEditor("personID").option("disabled", status);

    form.getEditor("employeeID").option("disabled", status); 

    $(".dx-tag-container").attr("disabled","disabled");

    $(".servicesTag").dxTagBox("instance").option("readOnly", true)
}

function newempenableform() {
   
    if (role == 'Administrator' && isclicked == 2) {

       
            form2 = $("#form").dxForm("instance");
      

        form2.option("readOnly", false);

        var status = false

        form2.getEditor("firstName").option("disabled", status);
        form2.getEditor("lastName").option("disabled", status);


        form2.getEditor("maritalStatus").option("disabled", status);
        form2.getEditor("gender").option("disabled", status);
        form2.getEditor("dateOfBirth").option("disabled", status);

        form2.getEditor("ssn").option("disabled", status);
        form2.getEditor("title").option("disabled", status);
        form2.getEditor("groupHome").option("disabled", status);


        form2.getEditor("manager").option("disabled", status);
        form2.getEditor("shift").option("disabled", status);
        form2.getEditor("isActive").option("disabled", status);

        form2.getEditor("hireDate").option("disabled", status);

        form2.getEditor("payRate").option("disabled", status);
        form2.getEditor("effectiveDate").option("disabled", status);
        form2.getEditor("isStudent").option("disabled", status);

        form2.getEditor("isInstructor").option("disabled", status);

        form2.getEditor("personID").option("disabled", status);

        form2.getEditor("employeeID").option("disabled", status); 

        $(".servicesTag").dxTagBox("instance").option("readOnly", true)
       
    }


}

function postEmployeeDetails() {
    var form = $("#form").dxForm("instance");
    var t = $("#btn").dxButton("instance");
    var selectedServices = $(".servicesTag").dxTagBox("instance").option("selectedItems"); 
 
    var selectedPayRate = employeeServicePayrate

  
    $.ajax({
        type: "POST",
        data: {
            'ddata': ddata, 'vw_PayPerServiceDetails': selectedPayRate,'UpdatedProfilePic':propic
        },
        url: "/api/postPersonDetail/" + employeeuser + "/" + selectedServices,
        async: false,
        success: function (data) {
            DevExpress.ui.dialog.alert("Employe profile saved successfully!!!", "Update", true);
            form.option("readOnly", true);
            t.option("text", "Edit Profile");
            $("#payGrid").dxDataGrid("instance").refresh();
            enableformelements(form) 
          
        },
        error: function () {
            DevExpress.ui.dialog.alert("Error occured updating employee profile data. Please try again", "Error")

            t.option("text", "Save Profile");
            form.option("readOnly", false);
        }
    });
}


///////  

var url = "/api/personDetailbymanager/" + sessionStorage.getItem("employeeid");

var dataStore = new DevExpress.data.CustomStore({
    key: "employeeID",
    type: "array",
    loadMode: "raw",
    cacheRawData: false,

    load: function (loadOptions) {
        var d = $.getJSON(url);
        return d;
    },
    byKey: function (key) {
        return $.getJSON(url + "/" + encodeURIComponent(key));

    }
});

var dataSource = new DevExpress.data.DataSource({
    store: dataStore

});

$(function () {

    $("#employeeGrid")
        .dxDataGrid({
        dataSource: dataSource,
        //columnChooser: {
        //    enabled:true
        //},
        editing: {
            mode: "popup",
            allowAdding: false,
            allowUpdating: false,
            allowDeleting: false,
            useIcons: true
        },

        onEditorPreparing: function (e) {
          
        },
        Selection: {
            mode: "single"
        },
        onToolbarPreparing: function (e) {
            var dataGrid = e.component;
            e.toolbarOptions.items.unshift({
                location: "after",
                widget: "dxButton",
                options: {
                    icon: "refresh",
                    onClick: function () {
                        dataGrid.refresh();
                    }
                }
            });
            e.toolbarOptions.items.unshift({
                location: "after",
                widget: "dxButton",
                options: {
                    text: "Add Employee",
                    onClick: function () {
                        sessionStorage.setItem("isclicked", 2);
                        window.location = "/Employee/EmployeeProfile";

                    }
                }
            });

        },
        onRowUpdating: function (options) {
            $.extend(options.newData, $.extend(true, {}, options.oldData, options.newData));
        },
        Paging: {
            pageSize: 10
        },
        pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [10, 25, 50, 100],
            showInfo: true,
            infoText: "showing page {0} of {1} ({2} employees)"
        },
        allowColumnReordering: true,
        allowColumnResizing: true,
        rowAlternationEnabled: true,
        hoverStateEnabled: true,

        columns: [
            {
            dataField: "employeeID",
            caption: "ID",
            allowedit: false,
            width: 45,
            allowEditing: false,
            }
          
            , {
            dataField: "firstName",
            caption: "First Name",
            width: 120
        }, {
            dataField: "lastName",
            caption: "Last Name",
            width: 120

        }
            ,

        {
            dataField: "gender",
            caption: "Gender",
            width: 80

            }
            ,

            {
                dataField: "title",
                caption: "Title",
                width: 120

            }
            ,
            

        {
            dataField: "phoneNumber",
            caption: "Phone",
            width: 120

        }
            ,
        {
            dataField: "groupHome",
            caption: "Home",
            width: 250,
            groupIndex: 0,
            fixed:true
        },
   
        {
            dataField: "shift",
            caption: "shift",
            width: 80

            }
            ,

            {
                dataField: "hireDate",
                caption: "Hire Date",
                width: 100,
                dataType:'date'

            }
            ,

            {
                dataField: "isActive",
                caption: "Active?",
                dataType:"boolean",
                width: 80

            }

            , {
                dataField: "employeeID",
                caption: "Action",
                alignment:"center",
                width: 80
                , cellTemplate: function (container, options) {
                    $('<a/>').addClass('dx-link')
                        .text('View')
                        .on('dxclick', function () {
                            sessionStorage.setItem("employeeuser", options.value);
                            sessionStorage.setItem("isclicked", 1);
                            window.location = "/Employee/EmployeeProfile";
                        }).appendTo(container);
                }
            }
          


        ],
        filterRow: {
            visible: true
        },
        groupPanel: {
            visible: true
        },
        scrolling: {
            mode: "fixed"
        },


    });
});

function tb () {


    $("#MemberServiceInfo2").dxTagBox({
        dataSource: new DevExpress.data.ArrayStore({
            data: MemberServiceInfo,
            key: "empID"
        }),
        value: MemberServiceInfo,
        displayExpr: "serviceType",
        valueExpr: "empID",
    });
};












