﻿
var url = "/api/Student";
var course;
var student;
var scheduleid;
var courseRegId;

// Cards
Studentcount();
NewStudentcount();
ExpiredCertcount();
available();

function Studentcount() {
    $.ajax({
        type: "GET",
        url: "/api/Studentcount",
        success: function (data) {
            $('#registeredstudents').text(data)
        },
        error: function () {
            $('#registeredstudents').text("Error Occured")
        }
    });
}

function NewStudentcount() {
    $.ajax({
        type: "GET",
        url: "/api/NewStudentcount",
        success: function (data) {
            $('#newstudents').text(data)
        },
        error: function () {
            $('#newstudents').text("Error Occured")
        }
    });
}

function ExpiredCertcount() {
    $.ajax({
        type: "GET",
        url: "/api/ExpiredCertCount",
        success: function (data) {
            $('#expiredcerts').text(data)
        },
        error: function () {
            $('#expiredcerts').text("Error Occured")
        }
    });
}

function available() {
    $.ajax({
        type: "GET",
        url: "/api/courseschedule/",
        success: function (data) {
            course = data;
        },
        error: function () {
            alert("Error Occured")
        }
    });
}


function registerstudent() {
    $.ajax({
        type: "GET",
        url: "/api/courseregistration/registrationByAdmin/" + student + "/" + scheduleid,
        success: function (data) {
            DevExpress.ui.dialog.alert("Registration succeeded", "Registration Status")
            
        },
        error: function () {
            DevExpress.ui.dialog.alert("Student already registered for this course", "Registration Status")
        }
    });
}

function unregisterstudent() {
    $.ajax({
        type: "GET",
        url: "/api/courseregistration/UnregisterStudent/" + courseRegId,
        success: function (data) {
            DevExpress.ui.dialog.alert("Student Unregistered", "Status")
        },
        error: function () {
            DevExpress.ui.dialog.alert("Error Unregistering Student", "Status")
        }
    });
}

function refreshGrid() {
    $("#registerGrid").refresh();
}

var StudataStore = new DevExpress.data.CustomStore({
    key: "studentID",
    type: "array",
    loadMode: "raw",
    cacheRawData: false,
    load: function (loadOptions) {
        var d = $.getJSON(url);
        return d;
    },
    byKey: function (key) {
        return $.getJSON(url + "/" + encodeURIComponent(key));
    }, insert: function (values) {
        return $.post(url, values);
    },
    add: function (values) {
        return $.ajax({
            url: url,
            method: "POST",
            data: values
        });
    },
    update: function (key, values) {
        return $.ajax({
            key: "studentID",
            url: "/api/UpdateStudent/",
            method: "PUT",
            data: values
        });
    },
    remove: function (key) {
        return $.ajax({
            url: "/api/DeleteStudent/" + encodeURIComponent(key),
            method: "DELETE"
        });
    }
});

var StudataSource = new DevExpress.data.DataSource({
    store: StudataStore
});






    $(function () {
        
        $("#studentGrid").dxDataGrid({
            dataSource: StudataSource,
            RemoteOperations : false,
            editing: {
                mode: "row",
                allowAdding: false,
                allowUpdating: false,
                allowDeleting: false,
                useIcons: true
            },
            onRowExpanding: function (e) {
                e.component.collapseAll(-1);
            },
            onToolbarPreparing: function (e) {
                var dataGrid = e.component;
                e.toolbarOptions.items.unshift({
                    location: "after",
                    widget: "dxButton",
                    options: {
                        icon: "refresh",
                        onClick: function () {
                            dataGrid.refresh();
                        }
                    }
                });
            },
            onRowUpdating: function (options) {
                $.extend(options.newData, $.extend(true, {}, options.oldData, options.newData));
            },
            columnAutoWidth: true,
            columnResizingMode: "widget",
            paging: {
                pageSize: 10
            },
            pager: {
                showPageSizeSelector: true,
                allowedPageSizes: [5, 10, 20,50,100],
                showInfo: true,
                infoText: "showing page {0} of {1} ({2} students)"
            },
            selection: {
                mode: "single"
            },
       
            searchPanel: {
                visible: true,
                searchVisibleColumnsOnly: true
            },
            headerFilter: {
                allowSearch: true,
                visible: true
            },
            allowColumnReordering: true,
            allowColumnResizing: true,
            rowAlternationEnabled: true,
            hoverStateEnabled: true,

            columns: [{
                dataField: "studentID",
                caption: "Student Id",
                allowGrouping: false,
                allowEditing: false,
                width:"9%"
            },{
                dataField: "firstname",
                caption: "First Name",
                allowSearch: true,
                width: "17%",
                validationRules: [{ type: "required" }]
            }, {
                 dataField: "lastname",
                 width: "18%",
                caption: "Last Name"
            },{
                dataField: "dob",
                caption: "Birth Date",
                dataType: "date",
                allowGrouping: true,
                width: "13%",
            }, {
                dataField: "email",
                caption: "Email",
                width: "25%",
            },
             {
                 dataField: "phone",
                 caption: "Phone",
                 width: "18%",
             }
              
            ],
            filterRow: {
                visible: true
            },
            groupPanel: {
                visible: true
            },
            scrolling: {
                mode: "fixed"
            },
            // height: 600,
            showBorders: true,
            masterDetail: {
                enabled: true,
                template: function (container, options) {
                    var d = options.data.courseRegistrations
                    var d2 = options.data
                  
                    $("<span id='regcourse' style='display:inline-block; margin-bottom:10px;'>")
                     
                      .text(d2.firstname + " " + d2.lastname + "'s registered course details:")
                      .appendTo(container);
                    $("<span id='reg' style='display:inline-block;float:right;margin-bottom:10px;clear:both'>").dxButton({
                        text:"Register for classes",
                        onClick: function (e) {
                                            
                            try {
                                student = options.data.studentID
                                var p = $("#register").dxPopup("instance");
                                p.option("title", "Register for course - " + d2.firstname + " " + d2.lastname);
                                p.show(options.data)
                            }
                            catch (e) {

                                DevExpress.ui.dialog.alert("Nada")
                            }
                                 
                        }
                    }).appendTo(container);
                   $("<div style='min-height:20px;'>").appendTo(container);
                   
                    $("<div>")
                        .dxDataGrid({
                           // dataSource: crdataSource,
                            dataSource: DevExpress.data.AspNet.createStore({
                                loadUrl: "/api/courseregistration/GetByStudentID/" + d2.studentID,
                                  
                                onBeforeSend: function (method, ajaxOptions) {
                                    ajaxOptions.xhrFields = { withCredentials: true };
                                }
                            })
                            ,
                            columns: [{
                                dataField: "courseRegistrationId",
                                caption: "Reg.ID",
                                width: "5%"
                            },
                               {
                                dataField: "courseSchedule.course.courseName",
                                caption: "Course Name",
                                width:"15%"
                            },
                            {
                                dataField: "",
                                caption: "Instructor Name",
                                width: "20%",
                                calculateCellValue: function (e) {
                                    ins = e.courseSchedule.instructor.person.firstName + " " + e.courseSchedule.instructor.person.lastName

                                    return ins;
                                }
                            },
                            
                            ,
                            {
                                dataField: "courseSchedule.room.roomName",
                                caption: "Training Room",
                                width: "10%"
                            }
                            ,
                            {
                                dataField: "courseSchedule.scheduleDate",
                                caption: "Schedule Date",
                                dataType: "date",
                                width:"10%"
                            }
                             ,
                            {
                                dataField: "grade.score",
                                caption: "Grade",
                                 width: "5%"

                            },
                            {
                                dataField: "courseSchedule.course.pass_Fail",
                                caption: "Passing grade",
                                width: "10%"
                            },
                             
                            {
                                dataField: "",
                                caption: "Status",
                                width: "5%",
                                calculateCellValue: function (e) {
                                    var stat = "Failed";
                                    try { var g = e.grade.score } catch (e) { g = null }
                                    if (g == null) { g = 0; stat = "N\A" }
                                    if (g >= e.courseSchedule.course.pass_Fail) {
                                        stat = "Passed"
                                    }
                                   
                                    return stat;
                                }


                                },
                             
                             {
                                 name: 'Cert'
                                 , dataField: 'cert.certUrl'
                                , width:"10%"
                                 , caption: 'View Cert',
                                 cellTemplate: function (container, options) {
                                     $("<div />").dxButton({
                                         icon: 'doc',
                                         onClick: function (e) {
                                            
                                             try {
                                                 window.location.href = options.data.cert.certUrl
                                             }
                                             catch (e) {
                                                
                                                 DevExpress.ui.dialog.alert("No certs generated. Please generate certs","Cert not found")
                                             }
                                 
                                         }
                                     }).appendTo(container);
                                 }
                                },
                                {
                                    dataField: '',
                                    caption: "UnRegister",
                                    width: "10%",
                                    cellTemplate: function (container, options) {
                                        $("<div />").dxButton({
                                            icon: 'doc',
                                            onClick: function (e) {
                                                courseRegId = options.data.courseRegistrationId
                                                unregisterstudent();
                                                $("#studentGrid").dxDataGrid("instance").refresh();

                                            }
                                        }).appendTo(container);
                                    }
                                }
                            ],
                            allowColumnReordering: true,
                            allowColumnResizing: true,
                            rowAlternationEnabled: true,
                            hoverStateEnabled: true,
                           
                         
                               
                            showBorders: false,
                            
                                filterRow: {
                        visible: false
                                },
                    groupPanel: {
                        visible: false
                    },
                    scrolling: {
                        mode: "virtual"
                    },
                        }).appendTo(container);
                }
            },

            grouping: {
                autoExpandAll: false
            }

        });
    });


    $("#popup").dxPopup({
        showTitle: true,
        width: 300,
        height: 250,
        title: 'View / Print Cert',
        contentTemplate: function () {
            return $("<div />").append(
             $("<div id='txt' > <a href= '#'>View Certificate</a> <br><a href= '#'>Print Certificate</a> "
            )
                );
        }
    });

    $(function () {

        $("#register").dxPopup({
            height: 400,
            width: 700,
            visible: false,
            closeOnBackButton: true,
            closeOnOutsideClick: false,
            onShown: function (e) {

                $("#registerGrid").dxDataGrid({
                    dataSource: course,
                    editing: {
                        mode: "popup",
                        allowAdding: false,
                        allowUpdating: false,
                        allowDeleting: false,
                        useIcons: true
                    },
                    Selection: {
                        mode: "single"
                    },
                    onToolbarPreparing: function (e) {
                        var dataGrid = e.component;
                  

                    },
                    onRowUpdating: function (options) {
                        $.extend(options.newData, $.extend(true, {}, options.oldData, options.newData));
                    },
                    Paging: {
                        pageSize: 10
                    },
                    pager: {
                        showPageSizeSelector: true,
                        allowedPageSizes: [10, 25, 50, 100],
                        showInfo: true,
                        infoText: "showing page {0} of {1} ({2} courses)"
                    },
                    allowColumnReordering: true,
                    allowColumnResizing: true,
                    rowAlternationEnabled: true,
                    hoverStateEnabled: true,
                    filterRow: {
                        visible: true
                    },
                    groupPanel: {
                        visible: false
                    },
                    scrolling: {
                        mode: "virtual"
                    },
                    columns: [
                        {
                            dataField: 'courseScheduleId',
                            caption: "ID",
                            width: "10%",
                            allowAdding: false,
                            allowEditing: false,
                            formItem: {
                                visible: false
                            }
                        },

                        {
                            dataField: 'course.courseName',
                            caption: "Course",
                            width: "30%",
                        },
                         {
                             dataField: 'scheduleDate',
                             caption: "Date",
                             dataType: "date",
                             width: "15%",
                         }
                         ,
                         {
                             dataField: 'startTime',
                             caption: "Start Time",
                             width: "15%",
                         }
                         ,
                         {
                             dataField: 'endTime',
                             caption: "End Time",
                             width: "15%", 
                         }
                          ,
                         {
                             dataField: '',
                             caption: "Register",
                             wwidth: "15%",
                             cellTemplate: function (container, options) {
                                 $("<div />").dxButton({
                                     icon: 'doc',
                                     onClick: function (e) {

                                       
                                            scheduleid  = options.data.courseScheduleId
                                            registerstudent()
                                           // location.reload()
                                         $("#studentGrid").dxDataGrid("instance").refresh();
                                        

                                     }
                                 }).appendTo(container);
                             }
                         }
                    ]

                });

    

            },
            onHiding: function (e) {
           
            },
            contentTemplate: function () {
                return $("<div />").append(
                 $("<div id='registerGrid' >")
                    );
            }
        });
    });






