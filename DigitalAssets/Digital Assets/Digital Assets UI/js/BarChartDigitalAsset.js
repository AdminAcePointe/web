﻿
//////////////////////////////////////
/// Dimension List
var BarDimChangeVal = null;
var BarMeasureChangeVal = null;
var BarCategory = [
    {
        "ID": 1,
        "Name": "Firm",
        "Category": "Classification"
    }, {
        "ID": 2,
        "Name": "Firm Classification",
        "Category": "Classification"
    }];
var barseries;
var bardata;
var barFilterData =
{
}
    ;

var BarCategoryDS = new DevExpress.data.DataSource({
    store: BarCategory,
    key: "id",
    group: "Category"
});

/// Measure List
var BarMeasure = [
    {
        "ID": 1,
        "Name": "AUM",
        "Category": "Measure"
    }, {
        "ID": 2,
        "Name": "Investment",
        "Category": "Measure"
    }, {
        "ID": 3,
        "Name": "Staff",
        "Category": "Measure"
    }];
var BarMeasureDS = new DevExpress.data.DataSource({
    store: BarMeasure,
    key: "id",
    group: "Category"
});
// Filter 
$("#BarDimension").dxSelectBox({
    dataSource: BarCategoryDS,
    valueExpr: "Name",
   value: "Firm Classification",
    grouped: true,
    displayExpr: "Name",
    placeholder: "Select Category",
    showClearButton: true,
    onValueChanged: function (e) {
        bardata = null;
        barseries = null;
        BarDimChangeVal = e.value
        barFilterData['Dimension'] = BarDimChangeVal;
        barFilterData['Measure'] = BarMeasureChangeVal;
        BarFilter();
        setAxis();
        var b = $("#BarChart").dxChart('instance')
        b.option('dataSource', bardata);
        b.option('series', barseries);

    }
});
$("#BarMeasure").dxSelectBox({
    dataSource: BarMeasureDS,
     value: "Investment",
    valueExpr: "Name",
    grouped: true,
    displayExpr: "Name",
    placeholder: "Select Measure",
    showClearButton: true,
    onValueChanged: function (e) {
        bardata = null
        barseries = null
        BarMeasureChangeVal = e.value
        barFilterData['Dimension'] = BarDimChangeVal;
        barFilterData['Measure'] = BarMeasureChangeVal;

        BarFilter();
        setAxis();
        var b = $("#BarChart").dxChart('instance')
        b.option('dataSource', bardata);
        b.option('series', barseries);
    }
});

// Bar chart
// Get series data
BarFilter();
function BarFilter() {
    refreshBarByFilter();
};
var t = [];
function refreshBarByFilter() {
    $.ajax({
        type: "POST",
        async: false,
        data: barFilterData,
        url: "http://digitalassetsapi.acepointe.com/InvestmentsByFirmClassificationCountry2",
        success: function (response) {
            barseries = JSON.parse(JSON.stringify(response))

        },
        error: function (xhr) {
            // alert("error")
        }
    });
    $.ajax({
        type: "POST",
        async: false,
        data: barFilterData,
        url: "http://digitalassetsapi.acepointe.com/InvestmentsByFirmClassification",
        success: function (response) {
            bardata = JSON.parse(response)
            console.log(bardata);
        },
        error: function (xhr) {
          
        }
  
        
    });

  
}


function setAxis(bardata,) {
    if (BarMeasureChangeVal == null) {
        BarMeasureChangeVal = "Investment"
    }
    //alert(BarMeasureChangeVal);
    return BarMeasureChangeVal
}

$(function () {
    $("#BarChart").dxChart({
        palette: "Green Mist",
        dataSource: bardata,
        commonSeriesSettings: {
            argumentField: "ClassificationLabel",
            type: "stackedBar"
        },
       
        series: barseries,
        valueAxis: [{
            title:"",
            grid: {
                visible: true
            }
        }
        ],
        tooltip: {
            enabled: true,
            shared: true,
            format: {
                type: "largeNumber",
                precision: 1
            },
            customizeTooltip: function (arg) {
                var items = arg.valueText.split("\n"),
                    color = arg.point.getColor();
                $.each(items, function (index, item) {
                    if (item.indexOf(arg.seriesName) === 0) {
                        items[index] = $("<span>")
                            .text(item)
                            .addClass("active")
                            .css("color", color)
                            .prop("outerHTML");
                    }
                });
                return { text: items.join("\n") };
            }
        },
        legend: {
            verticalAlignment: "bottom",
            horizontalAlignment: "center"
        }

    });
});

