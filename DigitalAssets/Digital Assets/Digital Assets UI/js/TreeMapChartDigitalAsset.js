﻿

var TreeDimChangeVal = null;
var TreeMeasureChangeVal = null;

var Treeseries;
var Treedata;
var TreeFilterData = {};
    var treeView, treeView2;

    var syncTreeViewSelection = function (treeView, value) {
        if (!value) {
            treeView.unselectAll();
        } else {
            treeView.selectItem(value);
        }
    };
    var syncTreeViewSelection2 = function (treeView2, value) {
        if (!value) {
            treeView2.unselectAll();
        } else {
            treeView2.selectItem(value);
        }
    };

    var makeAsyncDataSource = function (jsonFile) {
        return new DevExpress.data.CustomStore({
            loadMode: "raw",
            key: "ID",
            load: function () {
                return jsonFile//$.getJSON(jsonFile);
            }
        });
    };
    var makeAsyncDataSource2 = function (jsonFile) {
        return new DevExpress.data.CustomStore({
            loadMode: "raw",
            key: "ID",
            load: function () {
                return jsonFile//$.getJSON(jsonFile);
            }
        });
    };

    var getSelectedItemsKeys = function (items) {
        var result = [];
        items.forEach(function (item) {
            if (item.selected) {
                result.push(item.key);
            }
            if (item.items.length) {
                result = result.concat(getSelectedItemsKeys(item.items));
            }
        });
        return result;
    };
//////////////////////////////////////
/// Dimension List
var TreeCategory = [
    {
        "ID": 1,
        "Name": "Firm",
        "Category": "Classification"
    }, {
        "ID": 2,
        "Name": "Country",
        "Category": "Classification"
    }];
var TreeCategoryDS = new DevExpress.data.DataSource({
    store: TreeCategory,
    key: "id",
    group: "Category"
});

/// Measure List
var TreeMeasure = [
    {
        "ID": 1,
        "Name": "AUM",
        "Category": "Measure"
    }, {
        "ID": 2,
        "Name": "Investment",
        "Category": "Measure"
    }];
var TreeMeasureDS = new DevExpress.data.DataSource({
    store: TreeMeasure,
    key: "id",
    group: "Category"
});
// Filter 
$("#TreeDimension").dxSelectBox({
    dataSource: TreeCategoryDS,
    valueExpr: "Name",
    value: "Firm",
    grouped: true,
    displayExpr: "Name",
    placeholder: "Select Category",
    showClearButton: true,
    onValueChanged: function (e) {
        Treedata = null;
        Treeseries = null;
        TreeDimChangeVal = e.value
        TreeFilterData['Dimension'] = TreeDimChangeVal;
        TreeFilterData['Measure'] = TreeMeasureChangeVal;
        TreeFilter();
        setAxis();
        var b = $("#Top10TreeMap").dxTreeMap('instance')
        b.option('dataSource', Treedata);

    }
});
$("#TreeMeasure").dxSelectBox({
    dataSource: TreeMeasureDS,
    value: "Investment",
    valueExpr: "Name",
    grouped: true,
    displayExpr: "Name",
    placeholder: "Select Measure",
    showClearButton: true,
    onValueChanged: function (e) {
        Treedata = null;
        Treeseries = null;
        TreeMeasureChangeVal = e.value
        TreeFilterData['Dimension'] = TreeDimChangeVal;
        TreeFilterData['Measure'] = TreeMeasureChangeVal;
        TreeFilter();
        setAxis();
        var b = $("#Top10TreeMap").dxTreeMap('instance')
        b.option('dataSource', Treedata);
    }
});

// Tree chart
// Get series data

refreshTreeByFilter();


TreeFilter();
function TreeFilter() {
    refreshTreeByFilter();
};

function refreshTreeByFilter() {
    $.ajax({
        type: "POST",
        async: false,
        data: TreeFilterData,
        url: "http://digitalassetsapi.acepointe.com/getTopTenMeasuresByDimensionTreeMap",
        success: function (response) {
            Treedata = JSON.parse(response)
            console.log(Treedata);
        },
        error: function (xhr) {
            // alert("error")
        }
    });
  

  
}


function setAxis(Treedata,) {
    if (TreeMeasureChangeVal == null) {
        TreeMeasureChangeVal = "Investment"
    }
    //alert(TreeMeasureChangeVal);
    return TreeMeasureChangeVal
}

//////////////////////////////////////
/// Tree Map 


$(function () {

    $("#Top10TreeMap").dxTreeMap({
        dataSource: Treedata,
        size: {
            height: 650
        },
        colorizer: {
            palette: "Green Mist"
        },
        labelField: "name",
        valueField: "value",
        interactWithGroup: true,
        maxDepth: 1,
        onClick: function (e) {
            e.node.drillDown();
        },
        onDrill: function (e) {
            var markup = $("#drill-down-title").empty(),
                node;
            for (node = e.node.getParent(); node; node = node.getParent()) {
                markup.prepend(" > ").prepend($("<span />")
                    .addClass("link")
                    .text(node.label() || " Top 10 ")
                    .data("node", node)
                    .on("dxclick", onLinkClick));
            }
            if (markup.children().length) {
                markup.append(e.node.label());
            }
        },
        tooltip: {
            enabled: true,
            format: "decimal",
            customizeTooltip: function (arg) {
                var data = arg.node.data;

                return {
                    text: ("<span class='value'>" + data.name +
                        "</span><br/>Value: " + arg.valueText)
                };
            }
        },
    });

    function onLinkClick(e) {
        $(e.target).data("node").drillDown();
    }
});

$.ajaxSetup({
    async: false
});

