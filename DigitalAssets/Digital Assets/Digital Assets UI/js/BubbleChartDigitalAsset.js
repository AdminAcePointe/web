﻿

var BubbleDimChangeVal = null;
var BubbleMeasureChangeVal = null;

var Bubbleseries;
var Bubbledata;
var BubbleFilterData = {};

//////////////////////////////////////
/// Dimension List
var BubbleCategory = [
    {
        "ID": 1,
        "Name": "Firm Classification",
        "Category": "Classification"
    }, {
        "ID": 2,
        "Name": "Country",
        "Category": "Classification"
    }];
var BubbleCategoryDS = new DevExpress.data.DataSource({
    store: BubbleCategory,
    key: "id",
    group: "Category"
});

/// Measure List
var BubbleMeasure = [
    {
        "ID": 1,
        "Name": "AUM",
        "Category": "Measure"
    }, {
        "ID": 2,
        "Name": "Investment",
        "Category": "Measure"
    }, {
        "ID": 3,
        "Name": "Staff",
        "Category": "Measure"
    }];
var BubbleMeasureDS = new DevExpress.data.DataSource({
    store: BubbleMeasure,
    key: "id",
    group: "Category"
});
// Filter 
$("#BubbleDimension").dxSelectBox({
    dataSource: BubbleCategoryDS,
    valueExpr: "Name",
     value: "Firm Classification",
    grouped: true,
    displayExpr: "Name",
    placeholder: "Select Category",
    showClearButton: true,
    onValueChanged: function (e) {
        bubbledata = null;
        bubbleseries = null;
        BubbleDimChangeVal = e.value
        BubbleFilterData['Dimension'] = BubbleDimChangeVal;
        BubbleFilterData['Measure'] = BubbleMeasureChangeVal;
        BubbleFilter();
        setAxis();
        var b = $("#BubbleChart").dxChart('instance')
        b.option('dataSource', bubbledata);
        b.option('series', bubbleseries);

    }
});
$("#BubbleMeasure").dxSelectBox({
    dataSource: BubbleMeasureDS,
    value: "Investment",
    valueExpr: "Name",
    grouped: true,
    displayExpr: "Name",
    placeholder: "Select Measure",
    showClearButton: true,
    onValueChanged: function (e) {
        bubbledata = null;
        bubbleseries = null;
        BubbleMeasureChangeVal = e.value
        BubbleFilterData['Dimension'] = BubbleDimChangeVal;
        BubbleFilterData['Measure'] = BubbleMeasureChangeVal;
        BubbleFilter();
        setAxis();
        var b = $("#BubbleChart").dxChart('instance')
        b.option('dataSource', bubbledata);
        b.option('series', bubbleseries);
    }
});

// Bubble chart
// Get series data

refreshBubbleByFilter();


BubbleFilter();
function BubbleFilter() {
    refreshBubbleByFilter();
};

function refreshBubbleByFilter() {
    $.ajax({
        type: "POST",
        async: false,
        data: BubbleFilterData,
        url: "http://digitalassetsapi.acepointe.com/getSeriesByDimensionBubble",
        success: function (response) {
            bubbleseries = JSON.parse(response)
            console.log(bubbleseries);
        },
        error: function (xhr) {
            // alert("error")
        }
    });

    $.ajax({
        type: "POST",
        async: false,
        data: BubbleFilterData,
        url: "http://digitalassetsapi.acepointe.com/getMeasureByDimensionBubble",
        success: function (response) {
            bubbledata = JSON.parse(response)
            console.log(bubbledata);
        },
        error: function (xhr) {
            // alert("error")
        }
    });
  

  // getMeasureByDimensionBubble
}


function setAxis(Bubbledata,) {
    if (BubbleMeasureChangeVal == null) {
        BubbleMeasureChangeVal = "Investment"
    }
    //alert(BubbleMeasureChangeVal);
    return BubbleMeasureChangeVal
}



//// Bubble Chart
$(function () {
    $("#BubbleChart").dxChart({
        dataSource: bubbledata,
        loadingIndicator: {
            show: true
        },
        size: {
            height: 650
        },
        commonSeriesSettings: {
            type: 'bubble'
        },
        title: '',
        tooltip: {
            enabled: true,
            location: "bottom",
            customizeTooltip: function (arg) {
                return {
                    text: 'Filter : ' +arg.seriesName + '<br/>Value : ' + arg.valueText
                };
            }
        },
        argumentAxis: {
            label: {
                customizeText: function () {
                    return this.value;
                }
            }
        },
        valueAxis: {
            label: {
                customizeText: function () {
                    return this.value ;
                }
            }
           
        },
        legend: {
            position: 'outside',
            horizontalAlignment: 'center',
            verticalAlignment: 'bottom',
            border: {
                visible: false
            }
        },
        palette: "Green Mist",
        onSeriesClick: function (e) {
            var series = e.target;
            if (series.isVisible()) {
                series.hide();
            } else {
                series.show();
            }
        },
        //"export": {
        //    enabled: true
        //},
        series: bubbleseries
    });
});
