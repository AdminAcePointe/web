﻿
// Uncheck all switches
$("#sec_registered_1940_act").prop('checked', false);
$("#hiring").prop('checked', false);
$("#exclusively_crypto").prop('checked', false);


// Get default card values
$.getJSON("http://digitalassetsapi.acepointe.com/InvestmentCardSummary", function (data) {
    $.each(data, function (key, val) {
        console.log(JSON.stringify(data))
            $("#" +key).text(val)
       
    });
    console.log(JSON.stringify(data))
});
//Create card object
var data = 
    {
    }
;

// Populate cards
$("#sec_registered_1940_act").on('change', function (e) {
   
    var sec_registered_1940_act
    sec_registered_1940_act = $(this).is(':checked');
   console.log(sec_registered_1940_act)
    if ($(this).is(':checked') == true) {
        data['sec_registered_1940_act'] = 'yes';

    }
    else {
        data['sec_registered_1940_act'] = null;

    }
    console.log(data.sec_registered_1940_act)

// Refresh card
    refreshCardByFilter()

});

$("#exclusively_crypto").on('change', function () {
    var exclusively_crypto
    exclusively_crypto = $(this).is(':checked');
    console.log(exclusively_crypto)
    if ($(this).is(':checked') == true) {
        data['exclusively_crypto'] = 'Y';

    }
    else {
        data['exclusively_crypto'] = null;

    }
    console.log(data.sec_registered_1940_act)

    // Refresh card
    refreshCardByFilter()

});

$("#hiring").on('change', function () {

    var hiring
    hiring = $(this).is(':checked');
    console.log(hiring)
    if ($(this).is(':checked') == true) {
        data['hiring'] = 'Yes';

    }
    else {
        data['hiring'] = null;

    }
    console.log(data.hiring)

    // Refresh card
    refreshCardByFilter()
});


function refreshCardByFilter() {
    $.ajax({
        type: "POST",
        async: false,
        data: data,
        url: "http://digitalassetsapi.acepointe.com/InvestmentCardSummaryFilter",
        success: function (response) {
            $.each(response, function (key, val) {

                $("#" + key).text(val)

            });
            console.log(JSON.stringify(response))
        },
        error: function (xhr) {
            alert("error")
        }
    });
}

//// fixed header
$(window).scroll(function () {
    if ($(window).scrollTop() >= 250) {
        $('.StickyHeader').addClass('fixed-header');
        $('.StickyHeader div').addClass('visible-title');
        $('#switches').addClass('switchesAdd');
    }
    else {
        $('.StickyHeader').removeClass('fixed-header');
        $('.StickyHeader div').removeClass('visible-title');
        $('#switches').removeClass('switchesAdd');

    }
});

// Populate grid
ListDetailsInvestmentFunds();


var test = [{  "longitude": "-3.6951", "latitude": "40.45141", "aum_per_investments_KPI": "", "aum_per_number_of_professional_staff": null, "aum_per_staff_KPI": ""}]

function ListDetailsInvestmentFunds() {
    var noi;
    var aunstaff
    var sec
    var linkedin
    var phone
    var email
    var mainemail
    var url
    $.ajax({
        type: "GET",
       // async: false,
        url: "http://digitalassetsapi.acepointe.com/DetailsInvestmentFunds",
        success: function (response) {
            var parsedJSON = JSON.stringify(response);
            var t = JSON.parse(parsedJSON)
            //t[i].latitude
            var tr;
            for (var i = 0; i < t.length; i++) {
               
                if (t[i].aum_per_number_of_investments == null) {
                    noi = "";
                }
                else {
                    noi = "$" + t[i].aum_per_number_of_investments
                };
                if (t[i].aum_per_number_of_professional_staff == null) {
                    aunstaff = "";
                }
                else {
                    aunstaff = "$" + t[i].aum_per_number_of_professional_staff
                };
                if (t[i].SEC_Registered == "Yes") {
                    sec = "SEC"
                }
                else {
                    sec = ""

                }
                if (t[i].company_linkedin == null) {
                    linkedin = "#";
                }
                else {
                    linkedin =  t[i].company_linkedin
                };
                if (t[i].phone == null) {
                    phone = "#";
                }
                else {
                    phone = "tel:"+t[i].phone
                };
                if (t[i].email == null) {
                    email = "#";
                }
                else {
                    email = "mailto:" + t[i].email
                };
                if (t[i].main_email == null) {
                    mainemail = "#";
                }
                else {
                    mainemail = "mailto:" + t[i].main_email
                };
                if (t[i].url == null) {
                    url = "#";
                }
                else {
                    url = t[i].url
                };
                tr = "<tr  role='row'><td><div class='d-flex align-items-center justify-content-between'><span >" + t[i].firm_name + "</span> <span class='badge pe shadow'>" + t[i].Firm_Type + "</span></div></td > <td ><div class='d-flex align-items-center justify-content-between'><span >" + t[i].primary_contact + "</span><ul class='list-icons m-0'><li><a href='" + phone + "'><i class='icon-phone2'></i></a></li><li ><a href='" + email + "'><i class='icon-envelop4'></i></a></li><li ><a href='" + linkedin + "' target='blank'><i class='icon-linkedin text-blue-800'></i></a></li></ul> </div> </td><td >" + t[i].country + "</td><td > <ul class='d-flex align-items-center justify-content-center list-icons m-0 p-0'><li ><a href='" + phone + "'><i class='icon-phone2'></i></a></li><li ><a href='" + mainemail + "'><i class='icon-envelop4'></i></a></li><li ><a href='" + linkedin + "' target='blank'><i class='icon-linkedin text-blue-800'></i></a></li><li ><a href='" + url + "' target='blank'><i class='icon-link'></i></a></li><li ><a class='btn btn - primary' data-toggle='modal' data-target='#myModal' data-lat='" + t[i].latitude + "' data-lng='" + t[i].longitude +"'><i class='icon-location3 text-green-600'></i></a></li></ul></td><td ><div class='d-flex align-items-center justify-content-between'><span >" + noi + "</span> <span class='marker high'></span></div></td><td ><div class='d-flex align-items-center justify-content-between'><span >" + aunstaff + "</span> <span class='marker high'></span></div></td><td><div class='d-flex align-items-center justify-content-end'><span class='badge bg-green-800 shadow'>" + sec+  "</span></div></td ></tr>"
               // console.log(tr)
                $("#grid").append(tr);
            }
            //$.each(response, function (key, val) {
            //    alert(val)
            ////    $("#" + key).text(val)

            //});
            //console.log(JSON.stringify(response))
        },
        error: function (xhr) {
            alert("error")
        }
    });
}

// ECharts

    // based on prepared DOM, initialize echarts instance
    //var myChart = echarts.init(document.getElementById('main'));

    // specify chart configuration item and data
        //var option = {
//        title: {
//        text: ''
//},
//            tooltip: {},
//            legend: {
//                data: ['AUM'],
//                show: true,
//                type:'scroll'
//},
//            xAxis: {},
//            yAxis: {},
//            series: [{
//        name: 'AUM',
//    type: 'scatter',
//                data: [
//                    [10.0, 8.04],
//                    [8.0, 6.95],
//                    [13.0, 7.58],
//                    [9.0, 8.81],
//                    [11.0, 8.33],
//                    [14.0, 9.96],
//                    [6.0, 7.24],
//                    [4.0, 4.26],
//                    [12.0, 10.84],
//                    [7.0, 4.82],
//                    [5.0, 5.68]
//                ]
//}]
//};

// use configuration item and data specified to show chart
myChart.setOption(option);

var map = null;
var myMarker;
var myLatlng;

function initializeGMap(lat, lng) {
    myLatlng = new google.maps.LatLng(lat, lng);

    var myOptions = {
        zoom: 12,
        zoomControl: true,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

    myMarker = new google.maps.Marker({
        position: myLatlng
    });
    myMarker.setMap(map);
}

// Re-init map before show modal
$('#myModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    initializeGMap(button.data('lat'), button.data('lng'));
    $("#location-map").css("width", "100%");
    $("#map_canvas").css("width", "100%");
});

// Trigger map resize event after modal shown
$('#myModal').on('shown.bs.modal', function () {
    google.maps.event.trigger(map, "resize");
    map.setCenter(myLatlng);
});
//////////////////////////////////////////////////////////////////////
// Sample data

////// Fixed header





