﻿using Digital_Assets_Web.repo;
using System;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Digital_Assets_Web.Controllers
{
    public class DashboardsController : Controller
    {
        private Embed m_embed;
   

        // GET: Dashboards
        //public ActionResult Index()
        //{
        //    return View();
        //}

        public DashboardsController()
        {
            m_embed = new Embed();
       
        }

        public  ActionResult Index()
        {
         
            return View();

        }

        //public async Task<ActionResult> Index(string username, string roles)
        //{
        //    string url = Request.Url.AbsoluteUri;
        //    Uri myUri = new Uri(url);
        //    username = HttpUtility.ParseQueryString(myUri.Query).Get("guid");

        //    var embedResult = await m_embed.EmbedReport(username, roles);
           
        //        return View(m_embed.m_embedConfig2);
            
        //}

        public async Task<object> GetReport(string id)
        {
            string roles = null;
            string url = Request.Url.AbsoluteUri;

            var embedResult = await m_embed.EmbedReport(id, roles);

            var serializer = new JavaScriptSerializer();
           var JsonData = serializer.Serialize(m_embed.m_embedConfig2);


            return JsonData;

        }
    }
}