﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Digital_Assets.API;

namespace Digital_Assets_Web.Controllers
{
    public class AuthController : Controller
    {
        // GET: Auth
        public ActionResult Index()
        {
            return View();
        }

        [Route("SignIn")]
        public ActionResult SignIn()
        {
            return View();
        }

        [Route("ForgotPassword")]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        [Route("SignUp")]
        public ActionResult SignUp()
        {
            return View();
        }

        [Route("ResetPassword")]
        public ActionResult ResetPassword()
        {
            return View();
        }

        [Route("ConfirmEmail/{guid}")]
        public ActionResult ConfirmEmail(string id)
        {

            var EmpResponse = "";
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://digitalassetsapi.acepointe.com/");
                //HTTP GET

                string url = "GetEmailByGuid/" + id;
                var responseTask = client.GetAsync(url);
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {

                    EmpResponse = result.Content.ReadAsStringAsync().Result;



                }
            }
            ViewBag.email = EmpResponse.Replace(@"""",@"").ToString();
            return View();
        }

     
    }
}