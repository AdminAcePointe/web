
/// <reference path="jquery.js" />
/// <reference path="jquery.validate.js" />



// Redirect to signin page 
signinredirect();

// Sign in
$('#btnsignin').click(function () {
    $('#frmSignIn').validate(
        {
            rules: {
                email: {
                    required: true,
                    email:true
                },
                password: {
                    required: true
                }
            }
        
            ,
            errorPlacement: function (error, element) {
                if (element.attr("name") == "email")
                    $(".erroremail").html(error);

                if (element.attr("name") == "password")
                    $(".errorpassword").html(error);
            },
            submitHandler: function (form) {
                var signindata = {};
                signindata['email'] = $("#signinemail").val();
                signindata['pwd'] = $("#signinpwd").val();
            
                $.ajax({
                    type: "POST",
                    async: false,
                    data : signindata,
                    url: "http://digitalassetsapi.acepointe.com/SignIn",
                    success: function (response) {
                        if (response != "-1") {
                            sessionStorage.setItem('LoginToken', response);
                            window.location.href = "/Dashboards/Index";
                        }
                        else {
                            $(".errorsignin").removeClass("hidden");
                            $("#signinemail").val("");
                            $("#signinpwd").val("");
                            sessionStorage.clear();
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $(".errorsignin").removeClass("hidden");
                        console.log(xhr.status + " " + thrownError );
                    }
                });
                return false; // required to block normal submit since you used ajax
            }
        }
    );
 
});


// Sign up
$('#btnsignup').click(function () {
    $('#frmSignUp').validate(
        {
            rules: {
                email: {
                    required: true,
                    email: true
                },
                pwd: {
                    required: true,
                    minlength: 4
                },
                pwd2: {
                    required: true,
                    minlength: 4,
                    equalTo: "#signuppwd"
                },
                firstname: {
                    required:true
                },
                lastname: {
                    required: true
                }
            }

            ,
            errorPlacement: function (error, element) {
                if (element.attr("name") == "email")
                    $(".erroremail").html(error);
                if (element.attr("name") == "pwd")
                    $(".errorpassword").html(error);
                if (element.attr("name") == "pwd2")
                    $(".errorpassword2").html(error);
                if (element.attr("name") == "firstname")
                    $(".errorfirstname").html(error);
                if (element.attr("name") == "lastname")
                    $(".errorlastname").html(error);
            },
            submitHandler: function (form) {
                var signupdata = {};
                signupdata['email'] = $("#signupemail").val();
                signupdata['pwd'] = $("#signuppwd").val();
                signupdata['firstname'] = $("#signupfirstname").val();
                signupdata['lastname'] = $("#signuplastname").val();

                $.ajax({
                    type: "POST",
                    async: false,
                    data: signupdata,
                    url: "http://digitalassetsapi.acepointe.com/SignUp",
                    success: function (response) {
                        
                        if (response == "-2") {
                            $(".errorsignup p").text("Already registered. Please log in with the sign in link below");
                            $(".errorsignup").removeClass("hidden");
                            $("#signupemail").val("");
                            $("#signuppwd").val("");
                            $("#signuppwd2").val("");
                             $("#signupfirstname").val("");
                             $("#signuplastname").val("");
                        }
                        else if (response != "-1") {
                           
                            $(".successsignup p").text("Sign Up successfull.Please check your email to proceed with with confirmation process. ");
                            $(".successsignup").removeClass("hidden");

                            $("#formdatadiv").addClass("hidden");
                        }
                        else {
                            $(".errorsignup").removeClass("hidden");
                            sessionStorage.clear();
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $(".errorsignup").removeClass("hidden");
                        console.log(xhr.status + " " + thrownError);
                    }
                });
                return false; // required to block normal submit since you used ajax
            }
        }
    );

});

//Forgot Password
$('#btnforgotpassword').click(function () {
   
    $('#frmForgotPassword').validate(
        {
            rules: {
                forgotpasswordemail: {
                    required: true,
                    email: true
                }
            }

            ,
            errorPlacement: function (error, element) {
                if (element.attr("name") == "forgotpasswordemail")
                    $(".errorforgotpasswordemail").html(error);

             
            },
            submitHandler: function (form) {
                var forgotpassworddata = {};
                forgotpassworddata['email'] = $("#forgotpasswordemail").val();
               
                $.ajax({
                    type: "POST",
                    async: false,
                    data: forgotpassworddata,
                    url: "http://digitalassetsapi.acepointe.com/ForgotPassword",
                    success: function (response) {
                       
                        if (response == true) {
                            console.log(response)
                            window.location.href = "/Auth/ResetPassword";
                          
                        }
                        else {
                            $(".errorforgotpassword").removeClass("hidden");
                            $("#forgotpasswordemail").val("");
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $(".errorforgotpassword").removeClass("hidden");
                        alert(xhr.status + " " + thrownError)
                        console.log(xhr.status + " " + thrownError);
                    }
                });
                return false; // required to block normal submit since you used ajax
            }
        }
    );

});

// Reset Password
$('#btnresetpassword').click(function () {
    $('#frmresetpassword').validate(
        {
            rules: {
                resetpasswordemail: {
                    required: true,
                    email: true
                },
                resetpasswordtemppassword:{
                    required: true,
                    minlength: 4
                },
                resetpasswordpassword2: {
                    required: true,
                    minlength: 4,
                    equalTo: "#resetpasswordpassword"
                }
            }

            ,
            errorPlacement: function (error, element) {
                if (element.attr("name") == "resetpasswordemail")
                    $(".errorresetpasswordemail").html(error);
                if (element.attr("name") == "resetpasswordtemppassword")
                    $(".errorresetpasswordtemppassword").html(error);
                if (element.attr("name") == "resetpasswordpassword")
                    $(".errorresetpasswordpassword").html(error);
                if (element.attr("name") == "resetpasswordpassword2")
                    $(".errorresetpasswordpassword2").html(error);

            },
            submitHandler: function (form) {
                var resetpassworddata = {};
                resetpassworddata['email'] = $("#resetpasswordemail").val();
                resetpassworddata['temppwd'] = $("#resetpasswordtemppassword").val();
                resetpassworddata['newpwd'] = $("#resetpasswordpassword").val();

                $.ajax({
                    type: "POST",
                    async: false,
                    data: resetpassworddata,
                    url: "http://digitalassetsapi.acepointe.com/ResetPassword",
                    success: function (response) {
                        if (response == "1") {
                            window.location.href = "/Auth/SignIn";

                        }
                        else if (response == "-999") {
                            $(".errorresetpassword").removeClass("hidden").text("Email address not found !!.");
                            $("#resetpasswordemail").val("");
                            $("#resetpasswordtemppassword").val("");
                            $("#resetpasswordpassword").val("");
                        }
                        else {
                            $(".errorresetpassword").removeClass("hidden");
                 
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $(".errorresetpassword").removeClass("hidden");
                        console.log(xhr.status + " " + thrownError);
                    }
                });
                return false; // required to block normal submit since you used ajax
            }
        }
    );

});


// Confirm Email
$('#btnconfirmemail').click(function () {
    $('#frmconfirmemail').validate(
        {
            rules: {
                confirmemailemail: {
                    required: true,
                    email: true
                }
            }

            ,
            errorPlacement: function (error, element) {
                if (element.attr("name") == "confirmemailemail")
                    $(".errorconfirmemailemail").html(error);

            },
            submitHandler: function (form) {
                var confirmemaildata = {};
                confirmemaildata['email'] = $("#confirmemailemail").val();


                $.ajax({
                    type: "POST",
                    async: false,
                    data: confirmemaildata,
                    url: "http://digitalassetsapi.acepointe.com/ConfirmEmail",
                    success: function (response) {
                        if (response == "1") {
                            $(".successconfirmemaild").removeClass("hidden");
                            $("#confirmemailemail").addClass("hidden");
                            $("#confirmPText").text("Email Confirmed !!");
                            $("#btnconfirmemail").addClass("hidden");
                       

                        }
                        else if (response == "-1") {
                            $(".errorconfirmemail").removeClass("hidden").text("Email not confirmed !!.");
                        }
                        else {
                            $(".errorconfirmemail").removeClass("hidden");

                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $(".errorconfirmemail").removeClass("hidden");
                        console.log(xhr.status + " " + thrownError);
                    }
                });
                return false; // required to block normal submit since you used ajax
            }
        }
    );

});
// Functions
function signinredirect() { 
    var page = window.location.pathname;
    var page2 = window.location.href;
   // alert(page2)
    //alert(sessionStorage.getItem('LoginToken') )

    if (window.location.pathname == '/' ) {
        window.location.href = "/Auth/Signin";
      
    }
  
  
}

//$("#default").removeClass("hidden");
//$('#default').css("color", "#b96666")
//$("#rptText").text($('#investment li:first-child a').text());
    


