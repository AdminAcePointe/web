//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Digital_Assets
{
    using System;
    using System.Collections.Generic;
    
    public partial class AUMbyFirmClassificationLabel
    {
        public string ClassificationLabel { get; set; }
        public Nullable<int> Investment { get; set; }
        public Nullable<int> Staff { get; set; }
        public Nullable<decimal> AUM { get; set; }
        public Nullable<decimal> AUM_By_Number_Of_Investments { get; set; }
        public Nullable<decimal> AUM_By_Number_Of_Staff { get; set; }
    }
}
