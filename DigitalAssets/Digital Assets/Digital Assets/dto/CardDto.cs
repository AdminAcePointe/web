﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Digital_Assets.dto
{
    public class CardDto
    {
        public Nullable<int> InvestmentFirmsCount { get; set; }
        public Nullable<decimal> AUMSumBillions { get; set; }
        public Nullable<Decimal> InvestmentsSum { get; set; }
        public Nullable<Decimal> ProfessionalStaffSum { get; set; }
        public Nullable<decimal> aum_investmentsSum { get; set; }
        public Nullable<decimal> Aum_staffSum { get; set; }

    }
    public class CardFilterDto
    {
        public string sec_registered_1940_act { get; set; }
        public string exclusively_crypto { get; set; }
        public string hiring { get; set; }
        public string firm_type { get; set; }
        public string country { get; set; }

    }
    public class MapFilterInvestmentFundsDto
    {
        public Nullable<decimal> Investments { get; set; }
        public Nullable<decimal> aumBillions { get; set; }
        public string country { get; set; }
        public Nullable<decimal> staff { get; set; }

    }
    public class RegulationByCountriesDto
    {
        public string country { get; set; }
        public string regulationRaking { get; set; }

    }

    public class ScatterPlotFilterDto
    {
        public string sec_registered_1940_act { get; set; }
        public string exclusively_crypto { get; set; }
        public string hiring { get; set; }
    }

    public class BarFilterDto
    {
        public string Dimension { get; set; }
        public string Measure { get; set; }
    }

    public class ScatterPlotResultDto
    {
        public string founded_launch_year { get; set; }
        public string firm_type { get; set; }
        public Nullable<int> Investment { get; set; }
        public Nullable<int> Staff { get; set; }
        public Nullable<decimal> AUM { get; set; }
        public string Country { get; set; }
        public string firm_name { get; set; }
        public string city { get; set; }
    }

    public class DetailsInvestmentFundsDto
    {
        public string Firm_Type { get; set; }
        public string firm_name { get; set; }
        public string primary_contact { get; set; }
        public string phone { get; set; }
        public string primary_email { get; set; }
        public string company_linkedin { get; set; }
        public string country { get; set; }
        public string main_email { get; set; }
        public string url { get; set; }
        public string longitude { get; set; }
        public string latitude { get; set; }
        public Nullable<decimal> aum_per_number_of_investments { get; set; }
        public string aum_per_investments_KPI { get; set; }
        public Nullable<decimal> aum_per_number_of_professional_staff { get; set; }
        public string aum_per_staff_KPI { get; set; }
        public string SEC_Registered { get; set; }
    }


    public class AumByCityDto
    {
        public Nullable<decimal> aumBillions { get; set; }
        public string city { get; set; }
    }


    public class AumByCountryDto
    {
        public Nullable<decimal> aumBillions { get; set; }
        public string country { get; set; }
    }

    public class AumByFirmTypeDto
    {
        public Nullable<decimal> aumBillions { get; set; }
        public string firmType { get; set; }
    }

    public class AumByFirmNameDto
    {
        public Nullable<decimal> aumBillions { get; set; }
        public string firmName { get; set; }
    }

    public class InvestmentsByFirmClassificationCountryDto
    {
        public string valueField { get; set; }
    public string name { get; set; }
    public int id { get; set; }
}
}