﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Digital_Assets.dto
{
    public class SecurableDto
    {
        public string firstName { get; set; }
        public string lastName{ get; set; }
        public string email { get; set; }
        public string pwd { get; set; }
    }

    public class SecurablePWDto
    {
        public string newpwd { get; set; }
        public string email { get; set; }
        public string temppwd { get; set; }
    }

    public class SecurableConfirmEmail
    {
        public string email { get; set; }
    }

    public class SignInConfirmationDto
    {
        public string FullName { get; set; }
        public string Firstname { get; set; }
        public string LastName { get; set; }
        public string Token { get; set; }
        public string Email { get; set; }
    }




    public struct EmailParms
    {
        public string From { get; set; }
        public string To { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string Attachment { get; set; }
        public string Emailtype { get; set; }
    }
}