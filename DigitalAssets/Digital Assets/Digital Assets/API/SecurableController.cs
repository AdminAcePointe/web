﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Digital_Assets.Repository;
using Digital_Assets.dto;

namespace Digital_Assets.API
{
    public class SecurableController : ApiController
    {

        private DigitalAssetsEntities2 _context;
        private SecurableRepo _repo;
        public SecurableController()
        {
            _context = new DigitalAssetsEntities2();
            _repo = new SecurableRepo(_context);
        }

        [HttpPost]
        [Route("SignUp")]
        public HttpResponseMessage newSignUp([FromBody] SecurableDto signupdata)

        {

            string result = "-1";
            if (signupdata != null)
            {

                result = _repo.SignUp(signupdata);
            }


            return Request.CreateResponse(HttpStatusCode.OK, result);
        }


        [HttpPost]
        [Route("SignIn")]
        public HttpResponseMessage SignIn([FromBody] SecurableDto signindata )
        {
            SignInConfirmationDto confirmation = new SignInConfirmationDto();
            if (signindata != null)
            {

                confirmation = _repo.SignIn(signindata);
            }


            return Request.CreateResponse(HttpStatusCode.OK, confirmation);
        }

        [HttpPost]
        [Route("ForgotPassword")]
        public HttpResponseMessage PasswordRecovery([FromBody] SecurableDto forgotpassworddata)

        {
            bool result = false;
            if (forgotpassworddata != null)
            {

                result = _repo.PasswordRecovery(forgotpassworddata);
            }


            return Request.CreateResponse(HttpStatusCode.OK, result);


        }

        [HttpPost]
        [Route("ResetPassword")]
        public HttpResponseMessage ResetPassword([FromBody] SecurablePWDto resetpassworddata)

        {
            string result = "-1";
            if (resetpassworddata != null)
            {

                result = _repo.ResetPassword(resetpassworddata);
            }


            return Request.CreateResponse(HttpStatusCode.OK, result);


        }

        [HttpGet, HttpPost]
        [Route("GetEmailByGuid/{id}")]
        public HttpResponseMessage GetEmailByGuid(string id)

        {
            //string guid = "DA04BA5E-B89A-4CA3-BF3B-1F6CC3D1C7AB";
            string result = "-1";
            result = _repo.getEmailByGuid(id);
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [HttpPost]
        [Route("confirmEmail")]
        public HttpResponseMessage confirmEmail([FromBody] SecurableConfirmEmail email)
        {
            string result = _repo.setConfirmation(email);
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

    }
}
