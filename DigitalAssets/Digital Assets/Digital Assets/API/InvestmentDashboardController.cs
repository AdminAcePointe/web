﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Digital_Assets.Repository;
using Digital_Assets.dto;

namespace Digital_Assets.API
{
    public class InvestmentDashboardController : ApiController
    {
        private DigitalAssetsEntities2 _context;
        private InvestmentDashboardRepo _repo;
        private ScatterPlotFilterDto filter2 = new ScatterPlotFilterDto();
        private BarFilterDto Measure = new BarFilterDto();
        public InvestmentDashboardController()
        {
            _context = new DigitalAssetsEntities2();
            _repo = new InvestmentDashboardRepo(_context);
        }

        [HttpGet]
        [Route("InvestmentCardSummary")]
        public HttpResponseMessage Get()
        {
            return Request.CreateResponse(HttpStatusCode.OK,_repo.InvestmentCardSummary() );
        }
        
        [HttpPost]
        [Route("InvestmentCardSummaryFilter")]
        public HttpResponseMessage InvestmentCardSummaryFilter([FromBody] CardFilterDto filter)

        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.InvestmentCardSummaryfilter(filter));
        }
       

        [HttpGet]
        [Route("MapFilterInvestmentFunds")]
        public HttpResponseMessage MapFilterInvestmentFunds()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.MapFilterInvestmentFunds());
        }
      

        [HttpGet]
        [Route("regulatoryLandscape")]
        public HttpResponseMessage regulatoryLandscape()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.regulatoryLandscape());
        }
       

     
// Bar chart
        [HttpPost]
        [Route("InvestmentsByFirmClassification")]
        public HttpResponseMessage getInvestmentsByFirmClassification([FromBody] BarFilterDto Measure)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.InvestmentsByFirmClassification(Measure));
        }

        [HttpPost]
        [Route("InvestmentsByFirmClassificationCountry2")]
        public HttpResponseMessage getInvestmentsByFirmClassificationCountry2([FromBody] BarFilterDto Measure)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.InvestmentsByFirmClassificationCountry2(Measure));
        }

        //End  Bar chart

        // Tree Map
        [HttpPost]
        [Route("getTopTenMeasuresByDimensionTreeMap")]
        public HttpResponseMessage getTopTenMeasuresByDimensionTreeMap([FromBody] BarFilterDto Measure)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.getTopTenMeasuresByDimensionTreeMap(Measure));
        }

        //End  Tree Map

        // Bubble Chart
        [HttpPost]
        [Route("getSeriesByDimensionBubble")]
        public HttpResponseMessage getSeriesByDimensionBubble([FromBody] BarFilterDto Measure)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.getSeriesByDimensionBubble(Measure));
        }

        [HttpPost]
        [Route("getMeasureByDimensionBubble")]
        public HttpResponseMessage getMeasureByDimensionBubble([FromBody] BarFilterDto Measure)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.getMeasureByDimensionBubble(Measure));
        }

        //End  Bubble Chart


        [HttpGet]
        [Route("AUMbyFirmClassification")]
        public HttpResponseMessage getAUMbyFirmClassification()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.AUMbyFirmClassification());
        }

        [HttpGet]
        [Route("AUMbyFirmClassificationLabels")]
        public HttpResponseMessage getAUMbyFirmClassificationLabels()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.AUMbyFirmClassificationLabels());
        }

        [HttpGet]
        [Route("AUMbyFirmClassificationFirm_Type")]
        public HttpResponseMessage getAUMbyFirmClassificationFirm_Type()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.AUMbyFirmClassificationFirm_Type());
        }

        [HttpGet]
        [Route("AUMbyFirmClassificationCountries")]
        public HttpResponseMessage getAUMbyFirmClassificationCountries()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.AUMbyFirmClassificationCountries());
        }
       

        [HttpGet]
        [Route("DetailsInvestmentFunds")]
        public HttpResponseMessage getDetailsInvestmentFunds()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.DetailsInvestmentFunds());
        }

        [HttpGet]
        [Route("TopTenByAUMInvestments")]
        public HttpResponseMessage getTopTenByAUMInvestments()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.TopTenByAUMInvestments());
        } 

        [HttpGet]
        [Route("TopTenAum_per_number_of_professional_staff")]
        public HttpResponseMessage getAum_per_number_of_professional_staff()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.TopTenAum_per_number_of_professional_staff());
        }
        

        [HttpGet]
        [Route("TopTenAumByCountry")]
        public HttpResponseMessage getTopTenaumByCountry()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.TopTenAumByCountry());
        }

        [HttpGet]
        [Route("TopTenAumByCity")]
        public HttpResponseMessage getTopTenaumByCity()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.TopTenAumByCity());
        }

        [HttpGet]
        [Route("TopTenAumByFirmType")]
        public HttpResponseMessage getTopTenAumByFirmType()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.TopTenAumByFirmType());
        }

        [HttpGet]
        [Route("TopTenAumByFirmName")]
        public HttpResponseMessage getTopTenAumByFirmName()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.TopTenAumByFirmName());
        }
    }
}
