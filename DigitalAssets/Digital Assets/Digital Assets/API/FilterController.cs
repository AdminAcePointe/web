﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Digital_Assets.Repository;

namespace Digital_Assets.API
{
    public class FilterController : ApiController
    {
        private DigitalAssetsEntities2 _context;
        private FilterRepo _repo;
        public FilterController()
        {
            _context = new DigitalAssetsEntities2();
            _repo = new FilterRepo(_context);
        }

        [HttpGet]
        [Route("FirmTypes")]
        public HttpResponseMessage GetFirmTypes()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.getFirmTypes());
        }

        [HttpGet]
        [Route("RegulationRanking")]
        public HttpResponseMessage GetRegulationRanking()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.getRegulationRanking());
        }
    }
}
