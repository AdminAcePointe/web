//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Digital_Assets
{
    using System;
    using System.Collections.Generic;
    
    public partial class crypto_investment_firms
    {
        public long id { get; set; }
        public string firm_name { get; set; }
        public string address { get; set; }
        public string address_two { get; set; }
        public string city { get; set; }
        public string state_provnice { get; set; }
        public string zip { get; set; }
        public string country { get; set; }
        public string map { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public string code { get; set; }
        public string phone { get; set; }
        public string fax { get; set; }
        public string url { get; set; }
        public string company_linkedin { get; set; }
        public string crunchbase { get; set; }
        public string contact_title { get; set; }
        public string primary_contact { get; set; }
        public string primary_email { get; set; }
        public string contact_two { get; set; }
        public string contact_two_linkedin { get; set; }
        public string contact_two_email { get; set; }
        public string third_contact { get; set; }
        public string third_contact_linkedin { get; set; }
        public string third_contact_email { get; set; }
        public string forth_contact { get; set; }
        public string forth_contact_linkedin { get; set; }
        public string forth_contact_email { get; set; }
        public string aum_millions { get; set; }
        public Nullable<decimal> aum_billions { get; set; }
        public string twelve_month_aum_change { get; set; }
        public Nullable<int> professional_staff { get; set; }
        public string founded_launch_year { get; set; }
        public string main_email { get; set; }
        public string careers_email { get; set; }
        public string hiring { get; set; }
        public string firm_type { get; set; }
        public string firm_type_two { get; set; }
        public string firm_type_three { get; set; }
        public string firm_type_four { get; set; }
        public string firm_type_five { get; set; }
        public string sec_registered_1940_act { get; set; }
        public string number_of_clients { get; set; }
        public string exclusively_crypto { get; set; }
        public Nullable<int> number_of_investments { get; set; }
        public string investment_1 { get; set; }
        public string investment_2 { get; set; }
        public string investment_3 { get; set; }
        public string investment_4 { get; set; }
        public string investment_5 { get; set; }
        public string investment_6 { get; set; }
        public string investment_7 { get; set; }
        public string investment_8 { get; set; }
        public string investment_9 { get; set; }
        public string investment_10 { get; set; }
        public string investment_11 { get; set; }
        public string investment_12 { get; set; }
        public string investment_13 { get; set; }
        public string investment_14 { get; set; }
        public string investment_15 { get; set; }
        public string investment_16 { get; set; }
        public string investment_17 { get; set; }
        public string investment_18 { get; set; }
        public string investment_20 { get; set; }
        public string form_of_incorporation { get; set; }
        public Nullable<decimal> aum_per_number_of_investments { get; set; }
        public Nullable<decimal> aum_per_number_of_professional_staff { get; set; }
        public string number_of_investments_per_number_of_professional_staff { get; set; }
        public Nullable<System.DateTime> created_at { get; set; }
        public Nullable<System.DateTime> updated_at { get; set; }
    }
}
