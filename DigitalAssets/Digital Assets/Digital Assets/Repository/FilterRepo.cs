﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Digital_Assets.Repository
{
    public class FilterRepo
    {
        private DigitalAssetsEntities2 _context;

        public FilterRepo(DigitalAssetsEntities2 context)
        {
            _context = context;
        }
        public List<string> getFirmTypes()
        {
            var firmTypes = _context.crypto_investment_firms.Select(i => i.firm_type).Distinct().ToList();

            return firmTypes;
        }

        public List<string> getRegulationRanking()
        {
            var RegulationRanking = _context.RegulationByCountries.Select(i => i.RegulationRanking).Distinct().ToList();

            return RegulationRanking;
        }

    }
}