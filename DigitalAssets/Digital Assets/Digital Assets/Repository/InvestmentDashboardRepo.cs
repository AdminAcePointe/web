﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Web.Hosting;
using Digital_Assets.dto;
using System.Collections;
using System.Data;
using System.Linq.Dynamic;
using System.Data.SqlClient;
using System.Web.Mvc;
using System.Text;

namespace Digital_Assets.Repository
{
    public class InvestmentDashboardRepo
    {
        private DigitalAssetsEntities2 _context;
        private CardDto CardDto;
        public InvestmentDashboardRepo(DigitalAssetsEntities2 context)
        {
            CardDto = new CardDto();
            _context = context;

        }

        //Begining of Card Methods
        public CardDto InvestmentCardSummary()
        {

            CardDto.InvestmentFirmsCount = _context.crypto_investment_firms.Count();
            CardDto.AUMSumBillions = _context.crypto_investment_firms.Sum(i => i.aum_billions);
            CardDto.aum_investmentsSum = _context.crypto_investment_firms.Sum(i => i.aum_per_number_of_investments);
            CardDto.InvestmentsSum = _context.crypto_investment_firms.Sum(i => i.number_of_investments);
            CardDto.ProfessionalStaffSum = _context.crypto_investment_firms.Sum(i => i.professional_staff);
            CardDto.Aum_staffSum = _context.crypto_investment_firms.Sum(i => i.aum_per_number_of_professional_staff);

            return CardDto;
        }


        public CardDto InvestmentCardSummaryfilter(CardFilterDto filter)
        {

            var crypto_investment_firms = _context.crypto_investment_firms.Where(i =>
                          i.sec_registered_1940_act == (!string.IsNullOrEmpty(filter.sec_registered_1940_act) ? filter.sec_registered_1940_act : i.sec_registered_1940_act)
                                        &&
                                         i.country == (!string.IsNullOrEmpty(filter.country) == true ? filter.country : i.country)
                                          &&
                                         i.firm_type == (!string.IsNullOrEmpty(filter.firm_type) == true ? filter.firm_type : i.firm_type)
                                          &&
                                         i.hiring == (!string.IsNullOrEmpty(filter.hiring) == true ? filter.hiring : i.hiring)
                                            &&
                                         i.exclusively_crypto == (!string.IsNullOrEmpty(filter.exclusively_crypto) == true ? filter.exclusively_crypto : i.exclusively_crypto)
                                         );

            CardDto.InvestmentFirmsCount = crypto_investment_firms.Count();
            CardDto.AUMSumBillions = crypto_investment_firms.Sum(i => i.aum_billions);
            CardDto.aum_investmentsSum = crypto_investment_firms.Sum(i => i.aum_per_number_of_investments);
            CardDto.InvestmentsSum = crypto_investment_firms.Sum(i => i.number_of_investments);
            CardDto.ProfessionalStaffSum = crypto_investment_firms.Sum(i => i.professional_staff);
            CardDto.Aum_staffSum = crypto_investment_firms.Sum(i => i.aum_per_number_of_professional_staff);

            return CardDto;
        }
        //End of Card Methods

        //Begining of Map Methods
        public List<MapFilterInvestmentFundsDto> MapFilterInvestmentFunds()
        {
            var results = from crypto_investment_firm in _context.crypto_investment_firms
                          group crypto_investment_firm by crypto_investment_firm.country into g
                          select new MapFilterInvestmentFundsDto
                          {
                              country = g.FirstOrDefault().country,
                              aumBillions = g.Sum(o => o.aum_billions),
                              Investments = g.Sum(o => o.number_of_investments),
                              staff = g.Sum(o => o.professional_staff)
                          };
            //Exclude null aumBillion values
            var exclNullaumBillions = results.Where(i => i.aumBillions != null);
            //Exclude null Investments values
            var excNullInvestments = exclNullaumBillions.Where(i => i.Investments != null);

            var resultList = excNullInvestments.ToList();
            return resultList;
        }

        public List<RegulationByCountry> regulatoryLandscape()
        {
            var regulatoryRanking = _context.RegulationByCountries.GroupBy(p => new { p.Country, p.RegulationRanking }).Select(g => g.FirstOrDefault());
            //excludes null countries
            var regulatoryRankingNoNull = regulatoryRanking.Where(o => o.Country != null).ToList();
            return regulatoryRankingNoNull;
        }
        //End of Map Methods

     


        //Begin of Bar chart Methods

        public IEnumerable InvestmentsByFirmClassification (BarFilterDto Measure)
        {
            string DimensionString = null;
            string MeasureString = null;

            if (Measure == null)
            {
                DimensionString = "Firm Classification";
                MeasureString = "Investment";
            }
            else
            {
                DimensionString = Measure.Dimension.ToString();
                MeasureString = Measure.Measure.ToString();
            }



            string ConnectionString = _context.Database.Connection.ConnectionString;
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(ConnectionString);
                builder.ConnectTimeout = 2500;
                SqlConnection con = new SqlConnection(builder.ConnectionString);
                System.Data.Common.DbDataReader sqlReader;
                con.Open();
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "usp_getMeasuresByDimensionsBar";
                cmd.Parameters.Add("@Dimension", SqlDbType.VarChar).Value = DimensionString;
                cmd.Parameters.Add("@Measure", SqlDbType.VarChar).Value = MeasureString;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandTimeout = 0;

                    sqlReader = (System.Data.Common.DbDataReader)cmd.ExecuteReader();
                }
            var dt = new DataTable();
            dt.Load(sqlReader);
            List<DataRow> dr = dt.AsEnumerable().ToList();

            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(dt);
            return JSONString;

            // return _context.usp_getMeasuresByDimensionsBar1(DimensionString, MeasureString).ToList();
        }

        public List<usp_getMeasuresByFirmClassificationCountry2_Result> InvestmentsByFirmClassificationCountry2(BarFilterDto Measure)

        {
            string DimensionString = null;
            string MeasureString = null;

            if (Measure == null)
            {
                DimensionString = "Firm Classification";
                MeasureString = "Investment";
            }
            else
            {
                DimensionString = Measure.Dimension.ToString();
                MeasureString = Measure.Measure.ToString();
            }



            return _context.usp_getMeasuresByFirmClassificationCountry2(DimensionString,MeasureString).ToList();
          
        }
        //End of Bar chart Methods

        //Begin of Tree Map Methods
        public IEnumerable getTopTenMeasuresByDimensionTreeMap(BarFilterDto Measure)
        {
            {
                string DimensionString = null;
                string MeasureString = null;

                if (Measure == null)
                {
                    DimensionString = "Firm";
                    MeasureString = "Investment";
                }
                else
                {
                    DimensionString = Measure.Dimension.ToString();
                    MeasureString = Measure.Measure.ToString();
                }


                StringBuilder JSONString = new StringBuilder();
                string ConnectionString = _context.Database.Connection.ConnectionString;
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(ConnectionString);
                builder.ConnectTimeout = 2500;
                SqlConnection con = new SqlConnection(builder.ConnectionString);
                
                con.Open();
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "usp_getMeasuresByDimensionsTreeMap2";
                    cmd.Parameters.Add("@Dimension", SqlDbType.VarChar).Value = DimensionString;
                    cmd.Parameters.Add("@Measure", SqlDbType.VarChar).Value = MeasureString;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandTimeout = 0;


                    JSONString.Append(cmd.ExecuteScalar().ToString());
                }
                return JSONString.ToString();

                // return _context.usp_getMeasuresByDimensionsBar1(DimensionString, MeasureString).ToList();
            }



        }
        //End of Tree Map Methods

        //Begin of Bubble chart Methods
        public IEnumerable getSeriesByDimensionBubble(BarFilterDto Measure)
        {
            {
                string DimensionString = null;
                string MeasureString = null;

                if (Measure == null)
                {
                    DimensionString = "Firm Classification";
                    MeasureString = "Investment";
                }
                else
                {
                    DimensionString = Measure.Dimension.ToString();
                    MeasureString = Measure.Measure.ToString();
                }


                StringBuilder JSONString = new StringBuilder();
                string ConnectionString = _context.Database.Connection.ConnectionString;
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(ConnectionString);
                builder.ConnectTimeout = 2500;
                SqlConnection con = new SqlConnection(builder.ConnectionString);
                con.Open();
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "usp_getSeriesByDimensionBubble";
                    cmd.Parameters.Add("@Dimension", SqlDbType.VarChar).Value = DimensionString;
                    cmd.Parameters.Add("@Measure", SqlDbType.VarChar).Value = MeasureString;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandTimeout = 0;
                    JSONString.Append(cmd.ExecuteScalar().ToString());
                }
                return JSONString.ToString();

            }

        }

        public IEnumerable getMeasureByDimensionBubble(BarFilterDto Measure)
        {
            {
                string DimensionString = null;
                string MeasureString = null;

                if (Measure == null)
                {
                    DimensionString = "Firm Classification";
                    MeasureString = "Investment";
                }
                else
                {
                    DimensionString = Measure.Dimension.ToString();
                    MeasureString = Measure.Measure.ToString();
                }

                string j = string.Empty;
                StringBuilder JSONString = new StringBuilder();
                string ConnectionString = _context.Database.Connection.ConnectionString;
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(ConnectionString);
                builder.ConnectTimeout = 2500;
                SqlConnection con = new SqlConnection(builder.ConnectionString);
                con.Open();
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "usp_getMeasuresByDimensionsBubble";
                    cmd.Parameters.Add("@Dimension", SqlDbType.VarChar).Value = DimensionString;
                    cmd.Parameters.Add("@Measure", SqlDbType.VarChar).Value = MeasureString;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandTimeout = 0;
                    j = cmd.ExecuteScalar().ToString();
                }
                string json = JsonConvert.DeserializeObject(j).ToString();
                return json;

            }

        }
        //Begin of Bubble chart Methods




        public IEnumerable getTopTenMeasuresByDimensionBubble(BarFilterDto Measure)
        {
            {
                string DimensionString = null;
                string MeasureString = null;

                if (Measure == null)
                {
                    DimensionString = "Firm";
                    MeasureString = "Investment";
                }
                else
                {
                    DimensionString = Measure.Dimension.ToString();
                    MeasureString = Measure.Measure.ToString();
                }


                StringBuilder JSONString = new StringBuilder();
                string ConnectionString = _context.Database.Connection.ConnectionString;
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(ConnectionString);
                builder.ConnectTimeout = 2500;
                SqlConnection con = new SqlConnection(builder.ConnectionString);

                con.Open();
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "usp_getMeasuresByDimensionsBubble";
                    cmd.Parameters.Add("@Dimension", SqlDbType.VarChar).Value = DimensionString;
                    cmd.Parameters.Add("@Measure", SqlDbType.VarChar).Value = MeasureString;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandTimeout = 0;


                    JSONString.Append(cmd.ExecuteScalar().ToString());
                }
                return JSONString.ToString();

                // return _context.usp_getMeasuresByDimensionsBar1(DimensionString, MeasureString).ToList();
            }
        }
            //Begin of grid Methods
            public List<AUMbyFirmClassification> AUMbyFirmClassification()
        {
           return _context.AUMbyFirmClassifications.ToList();
        }

        public List<AUMbyFirmClassificationLabel> AUMbyFirmClassificationLabels()
        {
            return _context.AUMbyFirmClassificationLabels.OrderBy(i => i.ClassificationLabel).ToList();
           
        }

        public List<AUMbyFirmClassificationFirm_Type> AUMbyFirmClassificationFirm_Type()
        {
            return _context.AUMbyFirmClassificationFirm_Type.OrderBy(i=>i.firm_type).ToList();
        }

        public List<AUMbyFirmClassificationCountry> AUMbyFirmClassificationCountries()
        {
            return _context.AUMbyFirmClassificationCountries.ToList();
        }

        public List<DetailsInvestmentFundsDto> DetailsInvestmentFunds()
        {
            var result = from crypto_investment_firms in _context.crypto_investment_firms
                         select new DetailsInvestmentFundsDto
                         {
                             Firm_Type = (
                             crypto_investment_firms.firm_type == "Venture Capital" ? "VC" :
                             crypto_investment_firms.firm_type == "Hedge Fund" ? "HF" :
                             crypto_investment_firms.firm_type == "Private Equity" ? "PE" : ""
                             ),
                             firm_name = crypto_investment_firms.firm_name,
                             primary_contact = crypto_investment_firms.primary_contact,
                             phone = crypto_investment_firms.phone,
                             primary_email = crypto_investment_firms.primary_email,
                             company_linkedin = crypto_investment_firms.company_linkedin,
                             country = crypto_investment_firms.country,
                             main_email = crypto_investment_firms.main_email,
                             url = crypto_investment_firms.url,
                             longitude = crypto_investment_firms.longitude,
                             latitude = crypto_investment_firms.latitude,
                             aum_per_number_of_investments = crypto_investment_firms.aum_per_number_of_investments,
                             aum_per_investments_KPI = "",
                             aum_per_number_of_professional_staff = crypto_investment_firms.aum_per_number_of_professional_staff,
                             aum_per_staff_KPI = "",
                             SEC_Registered = crypto_investment_firms.sec_registered_1940_act

                         };
            var resultList = result.ToList();
                
            return resultList;
        }


        public List<DetailsInvestmentFundsDto> TopTenByAUMInvestments()
        {
               var result = from crypto_investment_firms in _context.crypto_investment_firms
                         select new DetailsInvestmentFundsDto
                         {
                             Firm_Type = 
                             (
                                crypto_investment_firms.firm_type == "Venture Capital" ? "VC" :
                                crypto_investment_firms.firm_type == "Hedge Fund" ? "HF" :
                                crypto_investment_firms.firm_type == "Private Equity" ? "PE" : ""
                             ),
                                firm_name = crypto_investment_firms.firm_name,
                                primary_contact = crypto_investment_firms.primary_contact,
                                phone = crypto_investment_firms.phone,
                                primary_email = crypto_investment_firms.primary_email,
                                company_linkedin = crypto_investment_firms.company_linkedin,
                                country = crypto_investment_firms.country,
                                main_email = crypto_investment_firms.main_email,
                                url = crypto_investment_firms.url,
                                longitude = crypto_investment_firms.longitude,
                                latitude = crypto_investment_firms.latitude,
                                aum_per_number_of_investments = crypto_investment_firms.aum_per_number_of_investments,
                                aum_per_investments_KPI = "",
                                aum_per_number_of_professional_staff = crypto_investment_firms.aum_per_number_of_professional_staff,
                                aum_per_staff_KPI = "",
                                SEC_Registered = crypto_investment_firms.sec_registered_1940_act

                         };

            var resultList = result.OrderByDescending(i => i.aum_per_number_of_investments).Take(10).ToList();

            return resultList;
        }

        public List<DetailsInvestmentFundsDto> TopTenAum_per_number_of_professional_staff()
        {
            var result = from crypto_investment_firms in _context.crypto_investment_firms
                         select new DetailsInvestmentFundsDto
                         {
                             Firm_Type =
                             (
                                crypto_investment_firms.firm_type == "Venture Capital" ? "VC" :
                                crypto_investment_firms.firm_type == "Hedge Fund" ? "HF" :
                                crypto_investment_firms.firm_type == "Private Equity" ? "PE" : ""
                             ),
                             firm_name = crypto_investment_firms.firm_name,
                             primary_contact = crypto_investment_firms.primary_contact,
                             phone = crypto_investment_firms.phone,
                             primary_email = crypto_investment_firms.primary_email,
                             company_linkedin = crypto_investment_firms.company_linkedin,
                             country = crypto_investment_firms.country,
                             main_email = crypto_investment_firms.main_email,
                             url = crypto_investment_firms.url,
                             longitude = crypto_investment_firms.longitude,
                             latitude = crypto_investment_firms.latitude,
                             aum_per_number_of_investments = crypto_investment_firms.aum_per_number_of_investments,
                             aum_per_investments_KPI = "",
                             aum_per_number_of_professional_staff = crypto_investment_firms.aum_per_number_of_professional_staff,
                             aum_per_staff_KPI = "",
                             SEC_Registered = crypto_investment_firms.sec_registered_1940_act

                         };

            var resultList = result.OrderByDescending(i => i.aum_per_number_of_professional_staff).Take(10).ToList();

            return resultList;
        }

        
        public List<AumByCountryDto> TopTenAumByCountry()
        {
            var results = from crypto_investment_firm in _context.crypto_investment_firms
                          group crypto_investment_firm by crypto_investment_firm.country into g
                          select new AumByCountryDto
                          {
                              country = g.FirstOrDefault().country,
                              aumBillions = g.Sum(o => o.aum_billions)
                          };
            var resultList = results.OrderByDescending(i => i.aumBillions).Take(10).ToList();
            return resultList;
        }
        

        public List<AumByCityDto> TopTenAumByCity()
        {
            var results = from crypto_investment_firm in _context.crypto_investment_firms
                          group crypto_investment_firm by crypto_investment_firm.city into g
                          select new AumByCityDto
                          {
                              city = g.FirstOrDefault().city,
                              aumBillions = g.Sum(o => o.aum_billions)
                          };
            var resultList = results.OrderByDescending(i => i.aumBillions).Take(10).ToList();
            return resultList;
        }

        public List<AumByFirmTypeDto> TopTenAumByFirmType()
        {
            var results = from crypto_investment_firm in _context.crypto_investment_firms
                          group crypto_investment_firm by crypto_investment_firm.firm_type into g
                          select new AumByFirmTypeDto
                          {
                              firmType = g.FirstOrDefault().city,
                              aumBillions = g.Sum(o => o.aum_billions)
                          };
            var resultList = results.OrderByDescending(i => i.aumBillions).Take(10).ToList();
            return resultList;
        }

        public List<AumByFirmNameDto> TopTenAumByFirmName()
        {
            var results = from crypto_investment_firm in _context.crypto_investment_firms
                          group crypto_investment_firm by crypto_investment_firm.firm_name into g
                          select new AumByFirmNameDto
                          {
                              firmName = g.FirstOrDefault().city,
                              aumBillions = g.Sum(o => o.aum_billions)
                          };
            var resultList = results.OrderByDescending(i => i.aumBillions).Take(10).ToList();
            return resultList;
        }
    }
}