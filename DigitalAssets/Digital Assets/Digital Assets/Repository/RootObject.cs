﻿using System.Collections.Generic;

namespace Digital_Assets.Repository
{
    public class Item2
    {
        public string name { get; set; }
        public double? value { get; set; }
    }

    public class Item
    {
        public string name { get; set; }
        public List<Item2> items { get; set; }
    }

    public class RootObject
    {
        public string name { get; set; }
        public List<Item> items { get; set; }
    }
}