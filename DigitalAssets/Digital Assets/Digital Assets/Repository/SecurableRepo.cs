﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Web.Hosting;
using Digital_Assets.dto;
using System.Collections;
using System.Data;
using System.Linq.Dynamic;
using System.Data.SqlClient;
using System.Web.Mvc;
using System.Net;
using System.Text;
using System.Net.Mail;

namespace Digital_Assets.Repository
{
    public class SecurableRepo
    {

        private DigitalAssetsEntities2 _context;
        public SecurableRepo(DigitalAssetsEntities2 context)
        {
            _context = context;

        }
        
        public string getEmailByGuid(string guid) {
            StringBuilder JSONString = new StringBuilder();
            string ConnectionString = _context.Database.Connection.ConnectionString;
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(ConnectionString);
            builder.ConnectTimeout = 2500;
            SqlConnection con = new SqlConnection(builder.ConnectionString);
            con.Open();

            SqlCommand com = new SqlCommand("SELECT [SECURABLE].getUserEmailByGuid(@guid)", con);
            com.Parameters.Add("@guid", SqlDbType.VarChar).Value = guid;
            string email = Convert.ToString(com.ExecuteScalar());

            return email;
        }
        public string setConfirmation(SecurableConfirmEmail email)
        {
            StringBuilder JSONString = new StringBuilder();
            string ConnectionString = _context.Database.Connection.ConnectionString;
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(ConnectionString);
            builder.ConnectTimeout = 2500;
            SqlConnection con = new SqlConnection(builder.ConnectionString);
            con.Open();
            using (SqlCommand cmd = con.CreateCommand())
            {
                cmd.CommandText = "securable.setIsConfirmedByEmail";
                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = email.email;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;

                JSONString.Append(cmd.ExecuteScalar().ToString());
            }
            string result = JSONString.ToString();

            return result;
        }


        public SignInConfirmationDto SignIn(SecurableDto signIn)
        {
            SignInConfirmationDto confirmation = new SignInConfirmationDto();
            confirmation.Token = "-1";
            confirmation.Email = null;
            confirmation.Firstname = null;
            confirmation.FullName = null;
            confirmation.LastName = null;
            string returnedValue = "-1";

            if (signIn != null)
            {

                StringBuilder JSONString = new StringBuilder();
                string ConnectionString = _context.Database.Connection.ConnectionString;
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(ConnectionString);
                builder.ConnectTimeout = 2500;
                SqlConnection con = new SqlConnection(builder.ConnectionString);
                con.Open();
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "securable.ValidateUserCredentials";
                    cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = signIn.email;
                    cmd.Parameters.Add("@pwd", SqlDbType.VarChar).Value = encryptpw(signIn.pwd);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandTimeout = 0;

                    JSONString.Append(cmd.ExecuteScalar().ToString());
                    returnedValue = JSONString.ToString();
                    if (returnedValue != "-1")
                    {
                        using (SqlCommand cmd2 = con.CreateCommand())
                        {
                            StringBuilder JSONString2 = new StringBuilder();
                            cmd2.CommandText = "securable.getUserInfoByEmail";
                            cmd2.Parameters.Add("@email", SqlDbType.VarChar).Value = signIn.email;
                            cmd2.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd2.CommandTimeout = 0;
                            SqlDataReader reader = cmd2.ExecuteReader();
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    confirmation.FullName = reader.GetString(0);
                                    confirmation.Firstname = reader.GetString(1);
                                    confirmation.LastName = reader.GetString(2);
                                    confirmation.Email = reader.GetString(3);
                                    confirmation.Token = returnedValue;
                                }
                            }

                        }
                    }
                    //-1 means not successful
                    

                }

            }

            return confirmation;
        }

        public string SignUp(SecurableDto signUp)
        {
            StringBuilder JSONString = new StringBuilder();
            string guid = null;
            string ConnectionString = _context.Database.Connection.ConnectionString;
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(ConnectionString);
            builder.ConnectTimeout = 2500;
            SqlConnection con = new SqlConnection(builder.ConnectionString);
            con.Open();

            SqlCommand com = new SqlCommand("SELECT securable.fn_emailExists(@Email)", con);
            com.Parameters.Add("@email", SqlDbType.VarChar).Value = signUp.email;
            bool emailExists = Convert.ToBoolean(com.ExecuteScalar());

            if (!emailExists) //Email does not exists
            {
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "securable.UserSignUp";
                    cmd.Parameters.Add("@firstname", SqlDbType.VarChar).Value = signUp.firstName;
                    cmd.Parameters.Add("@lastname", SqlDbType.VarChar).Value = signUp.lastName;
                    cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = signUp.email;
                    cmd.Parameters.Add("@pwd", SqlDbType.VarChar).Value = encryptpw(signUp.pwd);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandTimeout = 0;

                    JSONString.Append(cmd.ExecuteScalar().ToString());
                }
                guid = JSONString.ToString();
            }
            else
            {
                guid = "-2";
                return guid;
            }
            bool isloginSuccessful = false;
            if (guid != null)
            {
                string url = "http://digitalassets.acepointe.com/Auth/Confirmemail/" + guid;
                isloginSuccessful = confirmUserEmail(signUp.firstName, signUp.lastName, "Thanks for signing up to our website <br> Please confirm your email by clicking the link below <br><br><a href='" + url + "'>Confirm email</a><br><br> Thanks<br> Digital Assets", "Digital Assets - Thanks for signing up",signUp.email);

            }
            if (!isloginSuccessful)
            {
                guid = "-1";
            }
            return guid;

        }

        public bool PasswordRecovery(SecurableDto forgotPasswordEmail)
        {
            StringBuilder JSONString = new StringBuilder();
            string ConnectionString = _context.Database.Connection.ConnectionString;
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(ConnectionString);
            builder.ConnectTimeout = 2500;
            SqlConnection con = new SqlConnection(builder.ConnectionString);
            con.Open();

            SqlCommand com = new SqlCommand("SELECT securable.fn_emailExists(@Email)", con);
            com.Parameters.Add("@email", SqlDbType.VarChar).Value = forgotPasswordEmail.email;
            bool emailExists = Convert.ToBoolean(com.ExecuteScalar());

            if (emailExists)
            {
                //PASSWORDRECOVERY
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "securable.getTemporaryPassword";
                    cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = forgotPasswordEmail.email;
                  
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandTimeout = 0;

                    JSONString.Append(cmd.ExecuteScalar().ToString());
                }
                string tempPwd = JSONString.ToString();
                //send recovery email to user
                if (tempPwd != "")
                {
                    string url = "http://digitalassets.acepointe.com/Auth/ResetPassword/";
                    confirmUserEmail("FirstName", "Lastname", "We see that you are trying to reset your password <br> <br>Here is your temporary password: " + tempPwd + "<br>Please reset your password using the link below <br><br><a href='" + url + "'>Reset Password</a><br><br> Thanks<br> Digital Assets" , "Digital Assets - Password Reset", forgotPasswordEmail.email);
                }
            }


            return true;//always return true no matter what....Tell user -> if the email exists then a recovery password has been sent
        }

        public string ResetPassword(SecurablePWDto resetPassword)
        {
            StringBuilder JSONString = new StringBuilder();
            string result = null;
            string ConnectionString = _context.Database.Connection.ConnectionString;
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(ConnectionString);
            builder.ConnectTimeout = 2500;
            SqlConnection con = new SqlConnection(builder.ConnectionString);
            con.Open();

            SqlCommand com = new SqlCommand("SELECT securable.fn_emailExists(@Email)", con);
            com.Parameters.Add("@email", SqlDbType.VarChar).Value = resetPassword.email;
            bool emailExists = Convert.ToBoolean(com.ExecuteScalar());

            if (emailExists) //Email exists
            {
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = "securable.ResetPassword";
                    cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = resetPassword.email;
                    cmd.Parameters.Add("@temppwd", SqlDbType.VarChar).Value = resetPassword.temppwd;
                    cmd.Parameters.Add("@newpwd", SqlDbType.VarChar).Value = encryptpw(resetPassword.newpwd);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandTimeout = 0;

                    JSONString.Append(cmd.ExecuteScalar().ToString());
                }
                result = JSONString.ToString();
            }
            else
            {
                result = "-999"; // email not found
                return result;
            }
            bool passwordchanged = false;

            if (result == "1")
            {
                passwordchanged = confirmUserEmail("Dear", "User" , "Your password has been changed successfully. <br> Please log in using your new password", "Digital Assets - Password Change", resetPassword.email);

            }
            
           
            return result;

        }



        //Private methods
        private bool confirmUserEmail(string fn,string ln, string msg ,string subject,string email)
        {
            
            EmailParms emailParmObj = new EmailParms();
            emailParmObj.From = "admin@acepointe.com";
            emailParmObj.Subject = subject;
            emailParmObj.To = email;
            emailParmObj.Body = msg;

            try
            {
                using (MailMessage mail = new MailMessage(emailParmObj.From, emailParmObj.To))
                {
                   

                    mail.Subject = emailParmObj.Subject;
                    mail.Body = emailParmObj.Body;
                    mail.IsBodyHtml = true;
                  
                        SmtpClient client = new SmtpClient
                        {
                            Host = "relay-hosting.secureserver.net",
                            Credentials = new NetworkCredential("admin@acepointe.com", "Minda2017!!1"),
                            Port = 25

                        };
                    client.Send(mail);

                   
                }
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);

            }


        }
        private static string encryptpw(string pwd)
        {
            byte[] bytes = System.Text.Encoding.Unicode.GetBytes(pwd);
            string encrytpwd = Convert.ToBase64String(bytes);
            return encrytpwd;
        }
        private static string decryptpw(string dbpwd)
        {
            byte[] bytes = Convert.FromBase64String(dbpwd);
            string decrytpwd = System.Text.Encoding.Unicode.GetString(bytes);
            return decrytpwd;
        }
    
     
        //End of private methods

    }
}