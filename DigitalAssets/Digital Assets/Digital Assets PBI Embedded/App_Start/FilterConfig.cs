﻿using System.Web;
using System.Web.Mvc;

namespace Digital_Assets_PBI_Embedded
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
