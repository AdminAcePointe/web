﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Stripe;
using EKCT.dto;

namespace EKCT.Repo
{
    public class PaymentRepo
    {

        EKCT_DevEntities2 _context;
        private StudentRegistrationRepo _regrepo;
        private Common _commonrepo;
        const string _user = "AppUser";
        public PaymentRepo(EKCT_DevEntities2 context)
        {
            _context = context;
            _regrepo = new StudentRegistrationRepo(_context);
            _commonrepo = new Common(_context);

        }
        stripeResult result = new stripeResult();

        public stripeResult charge(Registrationdto Registration)
        {
            int register = 0;
            StripeConfiguration.ApiKey = "sk_live_KSjejbIoaQ2RaGVDQoIBxfeW00dg0Ty9Zp";

            var token = Registration.token;
            if (token != "paypal")
            {
                result = StripePayment(register, token, Registration);
            }
            else
            {
                result = PaypalPayment(register, token, Registration);
            }



            return result;
        }

        public int InsertPayment(Charge charge, int RegistrationID)
        {
            int result = 0;
           
            try
            {
             
                var reg = _context.Registrations.FirstOrDefault(i => i.RegistrationID == RegistrationID);

                int paymentType = 4;
                if (charge.PaymentMethodDetails != null)
                {
                    paymentType = 3;
                }

                int paymentMethod = 2;
                if (charge.PaymentMethodDetails != null)
                {
                    paymentMethod = 1;
                }



                if (reg != null)
                {
                    Payment pay = new Payment {
                        RegistrationID = RegistrationID,
                        PaymentDate = DateTime.Now,
                        Amount = charge.Amount,
                        PaymentmethodID = paymentMethod,
                        PaymenttypeID = paymentType,
                        CreatedDate = DateTime.Now,
                        ModifiedDate = DateTime.Now,
                        ModifiedBy = _user,
                        CreatedBy = _user

                };
                     _context.Payments.Add(pay);
                    if (_commonrepo.DbSave(_context))
                    { result = pay.PaymentID; }
                }
                else
                {
                    result = -4; // No registration record
                }

            }
            catch (Exception ex)
            {
          
                result = -5; // Error with inserting payment record
            }



            return result;
        }

        public stripeResult StripePayment(int register, string token, Registrationdto Registration)
        {
            var options = new ChargeCreateOptions
            {
                Amount = Registration.amount,
                Currency = "usd",
                Description = Registration.PaymentDescription,
                Source = token

            };
            var service = new ChargeService();
            try
            {
                Charge charge = service.Create(options);
                result.payment_status = charge.Paid;
                result.failure_code = charge.FailureCode;
                result.failure_message = charge.FailureMessage;

                if (charge.Paid)
                {
                    register = _regrepo.RegisterStudent(Registration);
                    result.registration_status = register;
                    if (register > 0)
                    {
                        result.registration_id = register;
                        charge.Amount = charge.Amount / 100;
                        result.paymentinsert_status = InsertPayment(charge, register);
                    }
                }

            }
            catch
            {
                result.payment_status = false;
                result.failure_code = "Error processing card. Please try later";
                result.failure_message = "Error processing card. Please try later";
            }
            return result;

        }


        public stripeResult PaypalPayment(int register, string token, Registrationdto Registration)
        {
           
         
            try
            {
          
              

              
                    register = _regrepo.RegisterStudent(Registration);
                    result.registration_status = register;
                    if (register > 0)
                    {
                    result.payment_status = true;
                    result.failure_code = null;
                    result.failure_message = null;
                    result.registration_id = register;
                    Charge charge = new Charge
                    {
                        Amount = Registration.amount/100,
                        Currency = "usd",
                        Description = Registration.PaymentDescription
                    };
                        result.paymentinsert_status = InsertPayment(charge, register);
                    }
                

            }
            catch
            {
                result.payment_status = false;
                result.failure_code = "Error processing payment with PayPal. Please try later";
                result.failure_message = "Error processing payment with PayPal. Please try later";
            }
            return result;

        }
    }
}