﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using EKCT.dto;
using System.Net;
using System.Net.Mime;

namespace EKCT.Repo
{
    public class EmailRepo
    {

        EKCT_DevEntities2 _context;

        const string _user = "AppUser";
        public EmailRepo(EKCT_DevEntities2 context)
        {
            _context = context;
        }

        public int SendEmailApplicant(EmailDto email)
        {

            EmailDto emailParmObj = new EmailDto();

            emailParmObj.email = email.email;
            emailParmObj.EmailFrom = email.EmailFrom;
            emailParmObj.firstName = email.firstName;
            emailParmObj.lastName = email.lastName;
            emailParmObj.Subject = email.Subject;
            emailParmObj.HTMLBody = email.HTMLBody;
            emailParmObj.resumePath = HttpContext.Current.Server.MapPath(email.resumePath);


            try
            {
                using (MailMessage mail = new MailMessage(emailParmObj.EmailFrom, emailParmObj.email))
                {
                    mail.Subject = emailParmObj.Subject;
                    mail.Body = emailParmObj.HTMLBody;
                    mail.IsBodyHtml = true;
                    SmtpClient client = new SmtpClient();
                    client.Send(mail);
                }
                return 1;
            }
            catch (Exception ex)
            {
                Common common_ = new Common(_context);
                ErrorLogDto errorDto = new ErrorLogDto();
                errorDto = common_.SetError("EmailRepo", DateTime.Now, ex.Message, ex.Source, _user);
                common_.LogError(errorDto);
                throw (ex);

            }


        }

        public int SendRegistrationEmail(EmailDto email)
        {

            EmailDto emailParmObj = new EmailDto();

            emailParmObj.email = email.email;
            emailParmObj.EmailFrom = email.EmailFrom;
            emailParmObj.firstName = email.firstName;
            emailParmObj.lastName = email.lastName;
            emailParmObj.Subject = email.Subject;
            emailParmObj.HTMLBody = email.HTMLBody;
            emailParmObj.resumePath = HttpContext.Current.Server.MapPath(email.resumePath);
            try
            {
                using (MailMessage mail = new MailMessage(emailParmObj.EmailFrom, emailParmObj.email))
                {
                    mail.Subject = emailParmObj.Subject;
                    mail.Body = emailParmObj.HTMLBody;
                    mail.IsBodyHtml = true;
                  
                    SmtpClient client = new SmtpClient();

                    client.Send(mail);
                }
                return 1;
            }
            catch (Exception ex)
            {
                Common common_ = new Common(_context);

                ErrorLogDto errorDto = new ErrorLogDto();
                errorDto = common_.SetError("EmailRepo", DateTime.Now, ex.Message, ex.Source, _user);
                common_.LogError(errorDto);
                throw (ex);

            }


        }
        
        public int SendEmailAdmin(AdminEmailDetailDto email)
        {

            EmailDto emailParmObj = new EmailDto();

            emailParmObj.email = email.EmailTo;
            emailParmObj.EmailFrom = email.EmailFrom;
            emailParmObj.firstName = email.FirstName;
            emailParmObj.lastName = email.LastName;
            emailParmObj.Subject = email.Subject;
            emailParmObj.HTMLBody = email.FinalBody;
            emailParmObj.resumePath = HttpContext.Current.Server.MapPath(email.AttachmentFilePath);
            try
            {
                using (MailMessage mail = new MailMessage(emailParmObj.EmailFrom, emailParmObj.email))
                {
                    mail.Subject = emailParmObj.Subject;
                    mail.Body = emailParmObj.HTMLBody;
                    mail.IsBodyHtml = true;

                    Attachment data = new Attachment(emailParmObj.resumePath, MediaTypeNames.Application.Octet);
                    mail.Attachments.Add(data);

                    SmtpClient client = new SmtpClient();

                    client.Send(mail);


                }
                return 1;
            }
            catch (Exception ex)
            {
                Common common_ = new Common(_context);

                ErrorLogDto errorDto = new ErrorLogDto();
                errorDto = common_.SetError("EmailRepo", DateTime.Now, ex.Message, ex.Source, _user);
                common_.LogError(errorDto);
                throw (ex);

            }


        }
        // Registration
        public int SendEmailApplicantComplete(EmailDto email)
        {

            EmailDto emailParmObj = new EmailDto();

            emailParmObj.email = email.email;
            emailParmObj.EmailFrom = email.EmailFrom;
            emailParmObj.Subject = email.Subject;
            emailParmObj.HTMLBody = email.HTMLBody;


            try
            {
                using (MailMessage mail = new MailMessage(emailParmObj.EmailFrom, emailParmObj.email))
                {
                    mail.Subject = emailParmObj.Subject;
                    mail.Body = emailParmObj.HTMLBody;
                    mail.IsBodyHtml = true;
                    mail.Bcc.Add(emailParmObj.EmailFrom);
                    SmtpClient client = new SmtpClient();
                    client.Send(mail);
                }
                return 1;
            }
            catch (Exception ex)
            {
                Common common_ = new Common(_context);
                ErrorLogDto errorDto = new ErrorLogDto();
                errorDto = common_.SetError("EmailRepo", DateTime.Now, ex.Message, ex.Source, _user);
                common_.LogError(errorDto);
                throw (ex);

            }


        }
    }
}