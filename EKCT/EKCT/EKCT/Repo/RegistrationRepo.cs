﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using EKCT.dto;
using EKCT.Repo;

namespace EKCT.Repo
{
    public class RegistrationRepo
    {

        EKCT_DevEntities2 _context;
        private Common _Commonrepo;
        const string _user = "AppUser";
        public RegistrationRepo(EKCT_DevEntities2 context)
        {
            _context = context;
            _Commonrepo = new Common(_context);
           
        }
      

        public int signUp(SignUpDto signUp)
        {
            int appID = 0; // failure
            var d = _context.Applicants.FirstOrDefault(i => i.Email == signUp.email);

            // User already took test
            if (d != null)
            {
                int _applicantid = d.ApplicantID;
                if (_context.ApplicantAssessments.Any(u => (u.ApplicantID == _applicantid)))
                {
                    appID = -1;
                    return appID;
                }
            }

            // Not authorized
            if (!_context.AssessmentApprovals.Any(u => (u.Email == signUp.email) && u.ExpirationDate > DateTime.Now))
            {
                appID = -9999;
                return appID;
            }

            // check if applicant already exists

            if (d != null)
            {
                appID = d.ApplicantID;
                return appID;
            }

           

            if (signUp != null)
            {
                Applicant applicant = new Applicant();
                applicant.FirstName = signUp.firstName;
                applicant.LastName = signUp.lastName;
                applicant.CreatedBy = _user;
                applicant.CreatedDate = DateTime.Now;
                applicant.ModifiedBy = _user;
                applicant.ModifiedDate = DateTime.Now;
                applicant.IsInactive = false;
                applicant.Password = "";
                applicant.Phone = signUp.PhoneNo;
                applicant.Gender = "";
                applicant.Email = signUp.email;
                applicant.ReferalSource = signUp.referalSource;
                applicant.ResumePath = signUp.resumePath.Replace(@"C:\fakepath\", @"\resumes\");
                appID = _context.Applicants.Add(applicant).ApplicantID;

                 {
                   _Commonrepo.DbSave(_context);
                    appID = applicant.ApplicantID;               

                }
              

            }
            return appID;




        }

        public List<CountryAbbrev> getCountries()
        {
            
            var countries = _context.Countries.Select(l => new CountryAbbrev
            {
                CountryName= l.CountryName ,
                Abbrev = l.ISO_Code,
                 CountryID = l.CountryID
            } ).ToList();
            return countries;

        }

        public List<StateAbbrev> getStatesByCountryID(int countryID)
        {

            var countries = _context.StateProvinces.Where(o=>o.CountryID == countryID).Select(l =>new StateAbbrev {
                 State = l.StateProvinceName,
                 Abbrev = l.StateAbbreviation ,
                 CountryID = (int)l.CountryID,
                 StateID = l.ID
            }).ToList();
            return countries;

        }
    }
}