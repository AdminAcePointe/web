﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using EKCT.dto;
using System.IO;
using Newtonsoft.Json;
using System.Globalization;
using System.Data.Entity.SqlServer;
using System.Data.Entity.Core.Objects;

namespace EKCT.Repo
{
    public class StudentRegistrationRepo
    {

        EKCT_DevEntities2 _context;
        private Common _Commonrepo;
        const string _user = "AppUser";
        public StudentRegistrationRepo(EKCT_DevEntities2 context)
        {
            _context = context;
            _Commonrepo = new Common(_context);

        }


        public int RegisterStudent(Registrationdto Applicant)
        {
            int result = 0;
            // Update applicant info
          result =   UpdateApplicantDetails(Applicant);

            if (result == 1)
            {
                // Create new student record
                result = AddNewStudent(Applicant);

                if (result > 0)
                {
                    // Create new registration record
                    result = AddRegistration(Applicant, result);
                }
            }

          return result;

        }

        public ApplicantDto GetApplicantDetails(int id)
        {
            
            ApplicantDto App = new ApplicantDto();
            var Applicant =  _context.Applicants.FirstOrDefault(i => i.ApplicantID == id);
            if (Applicant != null)
            {
                App.ApplicantID = id;
                App.Email = Applicant.Email;
                App.FirstName = Applicant.FirstName;
                App.LastName = Applicant.LastName;
                App.Phone = Applicant.Phone;
                App.AddressLine1 = Applicant.AddressLine1;
                App.AddressLine2 = Applicant.AddressLine2;
                App.PostalCode = Applicant.PostalCode;
                App.Gender = Applicant.Gender;
                App.City = Applicant.City;
                App.Country =  Applicant.Country;
                App.State = Applicant.State;
                App.MiddleName = Applicant.MiddleName;
            }

            return App;
        }

        public int UpdateApplicantDetails(Registrationdto Applicant)
        {
            int result = 0;
            if (Applicant != null)
            {

                var app = _context.Applicants.FirstOrDefault(i => i.ApplicantID == Applicant.ApplicantID);
                //Applicant exists?
                if (app != null)
                {
                    app.Email = Applicant.Email;
                    app.Phone = Applicant.Phone;
                    app.FirstName = Applicant.FirstName;
                    app.LastName = Applicant.LastName;
                    app.MiddleName = Applicant.MiddleInitial;
                    app.Gender = Applicant.Gender;
                    app.AddressLine1 = Applicant.AddressLine1;
                    app.AddressLine2 = Applicant.AddressLine2;
                    app.City = Applicant.City;
                    app.State = Applicant.state;
                    app.Country = Applicant.Country;
                    app.PostalCode = Applicant.ZipCode;

                    if (_Commonrepo.DbSave(_context))
                    { result = 1; }
                }
                else
                {
                    result = -1; // Applicant info not found
                }


            }
            return result;

        }


        public int AddNewStudent(Registrationdto Applicant)
        {
            int result = 0;
            int newStudentID = 0;
            if (Applicant != null)
            {

                var app = _context.Students.FirstOrDefault(i => i.ApplicantID == Applicant.ApplicantID);
                //applicant exists?
                if (app == null)
                {

                    Student Stu = new Student();
                    Stu.ApplicantID = Applicant.ApplicantID;
                    Stu.CreatedDate = DateTime.Now;
                    Stu.ModifiedDate = DateTime.Now;
                    Stu.ModifiedBy = _user;
                    Stu.CreatedBy = _user;

                     _context.Students.Add(Stu);
                    if (_Commonrepo.DbSave(_context))
                    { newStudentID = Stu.StudentID; }

                }
                else
                {
                    newStudentID = app.StudentID; // Applicant already exists
                }

                
            }
            result = newStudentID;
            return result;

        }

        public int AddRegistration(Registrationdto Applicant , int studentid)
        {
            int result = 0;
            int newRegistration = 0;
            if (Applicant != null)
            {
                // Check if applicant has registered for the same program
                var app = _context.Registrations.FirstOrDefault(i => i.StudentID == studentid  && i.ProgramID == Applicant.ProgramID);
                //applicant exists?
                if (app == null)
                {

                    Registration reg = new Registration();

                    reg.StudentID = studentid;
                    reg.CohortID = Applicant.CohortID;
                    reg.ProgramID = Applicant.ProgramID;
                    reg.CreatedDate = DateTime.Now;
                    reg.ModifiedDate = DateTime.Now;
                    reg.ModifiedBy = _user;
                    reg.CreatedBy = _user;

                    newRegistration = _context.Registrations.Add(reg).StudentID;
                    if (_Commonrepo.DbSave(_context))
                    { newRegistration = reg.RegistrationID; }
                }
                else
                {
                    newRegistration = -3; // Registration already exists
                }


            }
            result = newRegistration;
            return result;

        }

        public ApplicantDto StudentLogin(string email, string pwd)
        {
            if (email != "" && pwd != "")
            {
                var applicant = _context.Applicants.FirstOrDefault(i => i.Email == email);
                if (applicant != null)
                {
                    int stuID =_context.Students.FirstOrDefault(i => i.ApplicantID == applicant.ApplicantID).StudentID;
                    if (stuID != 0)
                    {
                        string encPwd = encryptpw(pwd);
                        if (_context.securables.FirstOrDefault(i => i.StudentID == stuID).Pwd == encPwd)
                        {
                            //Login successful
                            return GetApplicantDetails(applicant.ApplicantID);
                        }
                    }

                }
            }
            return null;

        }

        private static string encryptpw(string pwd)
        {
            byte[] bytes = System.Text.Encoding.Unicode.GetBytes(pwd);
            string encrytpwd = Convert.ToBase64String(bytes);
            return encrytpwd;
        }

        public List<programDetailDto> getProgramById(int id)
        {

            var programs = _context.Programs.Where(i => i.ProgramID == id).Include("ProgramCourses").Include("Cohorts").Select(k => new programDetailDto
            {
                programId = k.ProgramID,
                programName = k.ProgramName,
                description = k.Description,
                price =  k.Price,
                TargettedAudience = k.TargettedAudience,
                TentativeStartDate = SqlFunctions.StringConvert((double)
                  SqlFunctions.DatePart("mm", k.TentativeStartDate)).Trim() + "/" +
              // SqlFunctions.DateName("mm", p.modified) + "/" + MS ERROR?
              SqlFunctions.DateName("dd", k.TentativeStartDate) + "/" +
              SqlFunctions.DateName("yyyy", k.TentativeStartDate),
                ProgramDuration = DbFunctions.DiffMonths(k.TentativeStartDate, k.TentativeEndDate).ToString() + " Months",
                Courses = k.ProgramCourses.Select(b => new ProgramCourseDto
                {
                    CourseID = b.CourseID,
                    CourseName = b.Course.CourseName,
                    CourseWebLink = b.Course.CourseWebLink

                }
                ),
                Cohorts = k.Cohorts.Where(i=> i.IsNextCohort == true && i.IsActive == true).Select(b => new CohortDto
                {
                    CohortID = b.CohortID,
                    CohortName = b.CohortName,
                    MeetingDays = b.MeetingDays,
                    MeetingTime = b.MeetingTime,
                    MeetingDuration = b.MeetingDuration
                }
                )
            }
           );


            return programs.ToList();

        }
        public List<programDto> getPrograms()
        {

            var programs = _context.Programs.Select(k => new programDto
            {
                programId = k.ProgramID,
                code = k.ProgramCode,
                programName = k.ProgramName,
                description = k.Description,
                price = k.Price
            }).ToList();

            return programs;

        }
        private List<JsonDto> LoadJson()
        {
            List<JsonDto> schedule = new List<JsonDto>();
            using (StreamReader r = new StreamReader("json.json"))
            {
                string json = r.ReadToEnd();
                 schedule = JsonConvert.DeserializeObject<List<JsonDto>>(json);
            }
            return schedule;
        }
    }


}
