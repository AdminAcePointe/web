﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using EKCT.dto;
using Newtonsoft.Json;

namespace EKCT.Repo
{
    public class Pre_AssessmentRepo
    {
        EKCT_DevEntities2 _context;

        const string _user = "AppUser";
        public Pre_AssessmentRepo(EKCT_DevEntities2 context)
        {
            _context = context;
        }

        public string getQuestionsOptions()
        {
       
            
          var  qa2 = _context.vw_getQuestions_Options.ToList();

            string json = JsonConvert.SerializeObject(qa2);
            return json;

        }

        public vw_ApplicantScoreDetail GetApplicantScoreDetails(int id)
        {
            vw_ApplicantScoreDetail AppEmail = new vw_ApplicantScoreDetail();
            AppEmail = _context.vw_ApplicantScoreDetail.Where(u => (u.ApplicantID == id)).FirstOrDefault();


            return AppEmail;

        }

        
        public int ConfirmAdminRight(string adminCode)
        {
            if (genCode(adminCode) == 1)
                return 1;
            else
                return 0;

        }

       

        public int AddLeadEmail(string email)
        {
            return addEmail(email);
        }

        private int addEmail(string email)
        {
            if (email != "" || email != null)
            {
                if (_context.AssessmentApprovals.Find().Email == email)
                {
                    _context.AssessmentApprovals.FirstOrDefault(u => u.Email == email).ExpirationDate = DateTime.Now.AddDays(7);
                    if (Save())
                        return 1;
                            return 0;

                }
                else
                {
                    AssessmentApproval approval = new AssessmentApproval();
                    approval.ExpirationDate = DateTime.Now.AddDays(7);
                    approval.Email = email;
                    approval.AssessmentID = 1;
                    approval.CreatedBy = "AcePointeUser";
                    approval.ModifiedBy = "AcePointeUser";
                    approval.CreatedDate = DateTime.Now;
                    approval.ModifiedDate = DateTime.Now;

                    _context.AssessmentApprovals.Add(approval);
                    if(Save())
                      return 1;
                    return 0;
                }
               

            }
            return 0;
        }

        private int genCode(string adminCode)
        {
            string code = DateTime.Now.Date.Month + "Consult" + DateTime.Now.Date.Day;
            if (adminCode == code)
                return 1;
            return 0;
        }
        public int gradePreAssesment(List<StudentResponseDto>responses)
        {
            int studentScore = 0;
            int applicantID = 0;

            ApplicantQuestionAnswerResult appresult = new ApplicantQuestionAnswerResult();

            foreach (var response in responses)
            {
                applicantID = response.ApplicantID;
                // audit
                appresult.ApplicantID = applicantID;
                appresult.Question = response.Question;
                appresult.Answer = response.studentAnswer;
                appresult.Result = "Incorrect"; //TODO.. refactor code
                appresult.CreatedDate = DateTime.Now;
                appresult.ModifiedBy = _user;
                appresult.ModifiedDate = DateTime.Now;
                appresult.CreatedBy = _user;

                string correctAnswer = _context.Options.FirstOrDefault(i => i.QuestionID == response.QuestionID && i.isAnswer.Value).option1;
                if (response.studentAnswer == correctAnswer)
                {
                    studentScore = studentScore + 1;
                    appresult.Result = "Correct";
                }


                _context.ApplicantQuestionAnswerResults.Add(appresult);

                Save();


            }
           

            ApplicantScore score = new ApplicantScore();
            score.ApplicantID = applicantID;
            score.Score = studentScore;
            score.CreatedDate = DateTime.Now;
            score.ModifiedBy = _user;
            score.ModifiedDate = DateTime.Now;
            score.CreatedBy = _user;

          

            _context.ApplicantScores.Add(score);
            Save();

            // Load bridge table
            ApplicantAssessment ApplicantAssessment = new ApplicantAssessment();

            ApplicantAssessment.AssessmentID = 1;  //TODO : Dont hardcode
            ApplicantAssessment.ApplicantID = applicantID;
            ApplicantAssessment.CreatedBy = _user;
            ApplicantAssessment.CreatedDate = DateTime.Now;
            ApplicantAssessment.ModifiedBy = _user;
            ApplicantAssessment.ModifiedDate = DateTime.Now;

            _context.ApplicantAssessments.Add(ApplicantAssessment);
            Save();

            return studentScore;
        }

        private string getTenQuestionOptions(List<int> questionNos)
        {
            // TODO Add assesemnet type and assessment logic
            string assessment = "Data Analytics Aptitude Test";
            List<QuestionAnswerDto> Question_Options = new List<QuestionAnswerDto>();
            QuestionAnswerDto Question_ = null;
            if (questionNos != null)
            {

                foreach (int i in questionNos)
                {
                    Question_ = new QuestionAnswerDto();
                    List<string> _options = new List<string>();
                    Question_.QuestionID = _context.Questions.FirstOrDefault(u => u.QuestionID == i && u.Assessment.AssessmentName == assessment).QuestionID;
                    Question_.question = _context.Questions.FirstOrDefault(u => (u.QuestionID == i) && u.Assessment.AssessmentName == assessment).Question1;
                    var options = _context.Questions.FirstOrDefault(u => (u.QuestionID == i) && u.Assessment.AssessmentName == assessment).Options.ToList();
                    foreach (var option in options)
                    {
                        _options.Add(option.option1.ToString());

                    }
                    Question_.options = _options;
                    Question_Options.Add(Question_);
                }

            }
            string json = JsonConvert.SerializeObject(Question_Options);
            return json;
        }


      

        public int ReviewTest(ReviewTestDto ReviewTest)
        {
            int result = 0;
            vw_AdminDetail adminDetail = new vw_AdminDetail();
            adminDetail = getAdmin();

            var check = _context.RegistrationApprovals.FirstOrDefault(i => i.ApplicantID == ReviewTest.ApplicantID);
            if (check != null)
            {
                result = -1;
            }
            else
            {
                RegistrationApproval regApproval = new RegistrationApproval();
                regApproval.ApprovalAdminID = adminDetail.adminID;
                regApproval.ApplicantID = ReviewTest.ApplicantID;
                regApproval.ApprovalDate = DateTime.Now.Date;
                regApproval.ApplicantScoreID = _context.ApplicantScores.FirstOrDefault(i => i.ApplicantID == ReviewTest.ApplicantID).ApplicantScoreID;
                regApproval.ApprovalStatusTypeID = _context.ApprovalStatusTypes.FirstOrDefault(i => i.ApprovalStatusTypeDesc == ReviewTest.Status).ApprovalStatusTypeID;
                regApproval.AssessmentID = 1;
                regApproval.CourseID = null;
                regApproval.CreatedBy = _user;
                regApproval.ModifiedBy = _user;
                regApproval.CreatedDate = DateTime.Now.Date;
                regApproval.ModifiedDate = DateTime.Now.Date;

                _context.RegistrationApprovals.Add(regApproval);
                if (Save())
                {
                    result = 1;
                }
            }
            // return 0 if  NOT successfull
            return result;

        }

        private vw_AdminDetail getAdmin()
        {
            vw_AdminDetail AdminDetail = new vw_AdminDetail();
             AdminDetail = _context.vw_AdminDetail.FirstOrDefault();
            return AdminDetail;
        }

        private bool Save()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {

                foreach (DbEntityValidationResult item in e.EntityValidationErrors)

                {
                    DbEntityEntry entry = item.Entry;

                    string entityTypeName = entry.Entity.GetType().Name;
                    foreach (DbValidationError subItem in item.ValidationErrors)
                    {
                        string message = string.Format("Error '{0}' occurred in {1} at {2}",

                        subItem.ErrorMessage, entityTypeName, subItem.PropertyName);
                        Common common_ = new Common(_context);

                        ErrorLogDto errorDto = new ErrorLogDto();
                        errorDto = common_.SetError(entityTypeName, DateTime.Now, subItem.ErrorMessage, subItem.PropertyName, _user);
                        common_.LogError(errorDto);
                    }

                }
                return false;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return false;
            }
            return true;
        }

        

    }
}