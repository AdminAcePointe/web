﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using EKCT.dto;
using System.Net;
using System.Net.Mime;
using System.Data.Entity.Validation;
using System.Data.Entity.Infrastructure;

namespace EKCT.Repo
{
    public class AssessmentApprovalRepo
    {

        EKCT_DevEntities2 _context;

        const string _user = "AppUser";
        public AssessmentApprovalRepo(EKCT_DevEntities2 context)
        {
            _context = context;
        }

        public int ValidateAdminCredential(string adminCode)
        {
            if (genCode(adminCode) == 1)
                return 1;
            else
                return 0;

        }

        public int RegisterApplicantAssessment(string email)
        {
            return addEmail(email);
        }

        private int addEmail(string email)
        {
            if (email != "" || email != null)
            {
                if (_context.AssessmentApprovals.Any(i=> i.Email == email))
                {
                   
                    DeleteApplicantDetails( email, 1);
                  _context.AssessmentApprovals.FirstOrDefault(u => u.Email == email).ExpirationDate = DateTime.Now.AddDays(14);
                    if (Save())
                        return 1;
                    return 0;

                }
                else
                {
                    AssessmentApproval approval = new AssessmentApproval();
                    approval.ExpirationDate = DateTime.Now.AddDays(14);
                    approval.Email = email;
                    approval.AssessmentID = 1;
                    approval.AdminID = 4; // TODO. Dont hardcode ID
                    approval.CreatedBy = "AcePointeUser";
                    approval.ModifiedBy = "AcePointeUser";
                    approval.CreatedDate = DateTime.Now;
                    approval.ModifiedDate = DateTime.Now;

                    _context.AssessmentApprovals.Add(approval);
                    if (Save())
                        return 1;
                    return 0;
                }


            }
            return 0;
        }

        private int genCode(string adminCode)
        {
            string code = DateTime.Now.Date.Month + "Consult" + DateTime.Now.Date.Day;
            if (adminCode == code)
                return 1;
            
return 0;
        }

        private int DeleteApplicantDetails(string email, int AssessmentID)
        {
            var appID = _context.Applicants.FirstOrDefault(i => i.Email == email);

            if (appID != null)
            {
                // Applicant Score
               var score = _context.ApplicantScores.FirstOrDefault(i => i.ApplicantID == appID.ApplicantID);
                if (score != null)
                {
                 var applicantscore =   _context.ApplicantScores.FirstOrDefault(i => i.ApplicantScoreID == score.ApplicantScoreID);
                    _context.ApplicantScores.Remove(applicantscore);
                  
                }

                // applicantassessment
                var applicantass = _context.ApplicantAssessments.FirstOrDefault(i => i.ApplicantID == appID.ApplicantID && i.AssessmentID == 1);
                if (applicantass != null)
                {
                    var applicantassessment = _context.ApplicantAssessments.FirstOrDefault(i => i.ApplicantAssessmentID == applicantass.ApplicantAssessmentID && i.AssessmentID == 1);
                    _context.ApplicantAssessments.Remove(applicantassessment);
                   
                }

                // ApplicantQuestionAnswer
                var ApplicantQuestionAnswerResult = _context.ApplicantQuestionAnswerResults.FirstOrDefault(i => i.ApplicantID == appID.ApplicantID);
                if (ApplicantQuestionAnswerResult != null)
                {
                    var ApplicantQuestionAnswer = _context.ApplicantQuestionAnswerResults.FirstOrDefault(i => i.ApplicantID == ApplicantQuestionAnswerResult.ApplicantID);
                    _context.ApplicantQuestionAnswerResults.Remove(ApplicantQuestionAnswer);

                }

                // registrationapproval
                var registrationapproval = _context.RegistrationApprovals.FirstOrDefault(i => i.ApplicantID == appID.ApplicantID && i.AssessmentID == 1);
                if (registrationapproval != null)
                {
                    var registrationapp = _context.RegistrationApprovals.FirstOrDefault(i => i.ApplicantID == registrationapproval.ApplicantID && i.AssessmentID == 1);
                    _context.RegistrationApprovals.Remove(registrationapp);

                }
            }

            if (Save())
                return 1;
            return 0;

         


        }

        private bool Save()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {

                foreach (DbEntityValidationResult item in e.EntityValidationErrors)

                {
                    DbEntityEntry entry = item.Entry;

                    string entityTypeName = entry.Entity.GetType().Name;
                    foreach (DbValidationError subItem in item.ValidationErrors)
                    {
                        string message = string.Format("Error '{0}' occurred in {1} at {2}",

                        subItem.ErrorMessage, entityTypeName, subItem.PropertyName);
                        Common common_ = new Common(_context);

                        ErrorLogDto errorDto = new ErrorLogDto();
                        errorDto = common_.SetError(entityTypeName, DateTime.Now, subItem.ErrorMessage, subItem.PropertyName, _user);
                        common_.LogError(errorDto);
                    }

                }
                return false;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return false;
            }
            return true;
        }

    }
}