﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using EKCT.dto;
namespace EKCT.Repo
{
    public class Common
    {

        EKCT_DevEntities2 _context;

        const string _user = "AppUser";
        public Common(EKCT_DevEntities2 context)
        {
            _context = context;
        }
        public ErrorLogDto SetError(string entityTypeName, DateTime date, string ErrorMessage, string PropertyName, string _user)
        {
            ErrorLogDto errorDto = new ErrorLogDto();
            errorDto.Application = entityTypeName;
            errorDto.CreatedDate = date;
            errorDto.ErrorMessage = ErrorMessage;
            errorDto.Trace = PropertyName;
            errorDto.user = _user;
            return errorDto;
        }

        public bool LogError(ErrorLogDto Error)
        {
            if (Error != null)
            {
                ErrorLog error_ = new ErrorLog();
                error_.ErrorMessage = Error.ErrorMessage;
                error_.Application = Error.Application;
                error_.CreatedDate = error_.CreatedDate;
                error_.user = Error.user;
                error_.Trace = Error.Trace;

                if (DbSave(_context))
                    return true;
            }
            return false;
        }


        public bool DbSave(EKCT_DevEntities2 _context)
        {
            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {

                foreach (DbEntityValidationResult item in e.EntityValidationErrors)

                {
                    DbEntityEntry entry = item.Entry;

                    string entityTypeName = entry.Entity.GetType().Name;
                    foreach (DbValidationError subItem in item.ValidationErrors)
                    {
                        string message = string.Format("Error '{0}' occurred in {1} at {2}",

                        subItem.ErrorMessage, entityTypeName, subItem.PropertyName);

                        ErrorLogDto errorDto = new ErrorLogDto();
                        errorDto = SetError(entityTypeName, DateTime.Now, subItem.ErrorMessage, subItem.PropertyName, _user);
                        LogError(errorDto);
                    }

                }
                return false;

            }
            catch (Exception e)
            {
                ErrorLogDto errorDto = new ErrorLogDto();
                errorDto = SetError(e.Source, DateTime.Now, e.InnerException.Message, e.StackTrace, _user);
                LogError(errorDto);
                return false;
            }
            return true;
        }

    }
    
}