//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EKCT
{
    using System;
    using System.Collections.Generic;
    
    public partial class RegistrationApproval
    {
        public int RegistrationApprovalID { get; set; }
        public Nullable<int> AssessmentID { get; set; }
        public int ApplicantID { get; set; }
        public int ApplicantScoreID { get; set; }
        public int ApprovalAdminID { get; set; }
        public int ApprovalStatusTypeID { get; set; }
        public Nullable<System.DateTime> ApprovalDate { get; set; }
        public Nullable<int> CourseID { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
    
        public virtual Admin Admin { get; set; }
        public virtual Applicant Applicant { get; set; }
        public virtual ApprovalStatusType ApprovalStatusType { get; set; }
    }
}
