﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EKCT.Repo;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using Newtonsoft.Json;
using EKCT.dto;
using System.Web.Http.Cors;


namespace EKCT.API
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PaymentController : ApiController
    {

        private PaymentRepo _repo;
        private EKCT_DevEntities2 _context;

        public PaymentController()
        {
            _context = new EKCT_DevEntities2();
            _repo = new PaymentRepo(_context);
        }

        [HttpPost]
        [Route("Makepayment")]
        public HttpResponseMessage Makepayment(Registrationdto Registration)
        {
           
            JsonSerializer serializer = new JsonSerializer();
            serializer.NullValueHandling = NullValueHandling.Ignore;

            string output = JsonConvert.SerializeObject(_repo.charge(Registration));

            return Request.CreateResponse(HttpStatusCode.OK, output);



        }
    }
}
