﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EKCT.Repo;
using EKCT.dto;
using System.Web.Http.Cors;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.IO;

namespace EKCT.API
{

    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class RegistrationController : ApiController
    {

        private EKCT_DevEntities2 _context;
        private RegistrationRepo _repo;

        public RegistrationController()
        {

            _context = new EKCT_DevEntities2();
            _repo = new RegistrationRepo(_context);
        }


        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("registration")]
        public HttpResponseMessage studentRegistration(SignUpDto dto) 
        {

            return Request.CreateResponse(HttpStatusCode.Created, _repo.signUp(dto));
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("getCountries")]
        public HttpResponseMessage getCountries()
        {

            return Request.CreateResponse(HttpStatusCode.Created, _repo.getCountries());
        }


        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("getStateByCountry/{id}")]
        public HttpResponseMessage getStateCode(int id)
        {

            return Request.CreateResponse(HttpStatusCode.Created, _repo.getStatesByCountryID(id));
        }
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("uploadfiles")]
        public HttpResponseMessage UploadFiles()

        {
            int result = 0; // failure
            if (HttpContext.Current.Request.Files.AllKeys.Any())
            {
            
                var httpPostedFile = HttpContext.Current.Request.Files["UploadedImage"];
                if (httpPostedFile != null)
                {
                 
                    var fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Resumes"), httpPostedFile.FileName);

                    // Save the uploaded file to "UploadedFiles" folder
                    httpPostedFile.SaveAs(fileSavePath);
                    result = 1; // success
                }
            }
            return Request.CreateResponse(HttpStatusCode.Created,result);
        }
    }
}
