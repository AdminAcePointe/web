﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EKCT.Repo;
using EKCT.dto;
using System.Web.Http.Cors;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.IO;

namespace EKCT.API
{

    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class StudentRegistrationController : ApiController
    {

        private EKCT_DevEntities2 _context;
        private StudentRegistrationRepo _repo;

        public StudentRegistrationController()
        {
            _context = new EKCT_DevEntities2();
            _repo = new StudentRegistrationRepo(_context);
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("GetApplicantDetails/{id}")]
        public HttpResponseMessage GetApplicantDetails(int id)
        {
           
            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetApplicantDetails(id));
        }


   

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("Login")]
        public HttpResponseMessage StudentLogin(string email , string pwd)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.StudentLogin(email, pwd));
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("GetPrograms")]
        public HttpResponseMessage GetPrograms()
        {
            
            return Request.CreateResponse(HttpStatusCode.OK, _repo.getPrograms());

        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("GetProgramById/{id}")]
        public HttpResponseMessage GetProgramByID(int Id)
          {

            return Request.CreateResponse(HttpStatusCode.OK, _repo.getProgramById(Id));

        }


    }
}
