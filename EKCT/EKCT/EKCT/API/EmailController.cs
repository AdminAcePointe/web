﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EKCT.Repo;
using EKCT.dto;
using System.Web.Http.Cors;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;

namespace EKCT.API
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class EmailController : ApiController
    {
        

        private EKCT_DevEntities2 _context;
        private EmailRepo _repo;

        public EmailController()
        {
            _context = new EKCT_DevEntities2();
            _repo = new EmailRepo(_context);
        }

 // assessments
        [HttpGet]
        [Route("SendEmailApplicant/{id}")]
        public HttpResponseMessage SendEmailApplicant(int id)
        {
            vw_ApplicantScoreDetail AppEmail = new vw_ApplicantScoreDetail();
            AppEmail = _context.vw_ApplicantScoreDetail.Where(u => (u.ApplicantID == id)).FirstOrDefault();

         

            if (AppEmail != null)
            {
                EmailDto email = new EmailDto();

            email.EmailFrom = AppEmail.EmailFrom;
            email.email = AppEmail.EmailTo;
            email.firstName = AppEmail.FirstName;
            email.lastName = AppEmail.LastName;
            email.Subject = AppEmail.Subject;
            email.resumePath = AppEmail.AttachmentFilePath;
            email.HTMLBody = AppEmail.FinalBody;

        
                return Request.CreateResponse(HttpStatusCode.OK, _repo.SendEmailApplicant(email));
                
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest, 0);

        }

        [HttpGet]
        [Route("SendRegistrationEmail/{id}")]
        public HttpResponseMessage SendRegistrationEmail(int id)
        {
            vw_ApplicantRegistrationEmail AppEmail = new vw_ApplicantRegistrationEmail();
            AppEmail = _context.vw_ApplicantRegistrationEmail.Where(u => (u.ApplicantID == id)).FirstOrDefault();



            if (AppEmail != null)
            {
                EmailDto email = new EmailDto();

                email.EmailFrom = AppEmail.EmailFrom;
                email.email = AppEmail.EmailTo;
                email.firstName = AppEmail.FirstName;
                email.lastName = AppEmail.LastName;
                email.Subject = AppEmail.Subject;
                email.resumePath = AppEmail.AttachmentFilePath;
                email.HTMLBody = AppEmail.FinalBody;


                return Request.CreateResponse(HttpStatusCode.OK, _repo.SendRegistrationEmail(email));

            }
            return Request.CreateResponse(HttpStatusCode.BadRequest, 0);

        }


        [HttpGet]
        [Route("SendEmailAdmin/{id}")]
        public HttpResponseMessage SendEmailAdmin(int id)
        {
            vw_AdminEmailDetail AppEmail = new vw_AdminEmailDetail();
            AppEmail = _context.vw_AdminEmailDetail.Where(u => (u.ApplicantID == id)).FirstOrDefault();



            if (AppEmail != null)
            {
                AdminEmailDetailDto email = new AdminEmailDetailDto();

                email.EmailFrom = AppEmail.EmailFrom;
                email.EmailTo = AppEmail.AdminEmail;
                email.FirstName = AppEmail.FirstName;
                email.LastName = AppEmail.LastName;
                email.Subject = AppEmail.Subject;
                email.AttachmentFilePath = AppEmail.AttachmentFilePath;
                email.FinalBody = AppEmail.FinalBody;


                return Request.CreateResponse(HttpStatusCode.OK, _repo.SendEmailAdmin(email));

            }
            return Request.CreateResponse(HttpStatusCode.BadRequest, 0);

        }


  // registration

        [HttpGet]
        [Route("SendEmailApplicantRegistration/{id}")]
        public HttpResponseMessage SendEmailApplicantRegistration(int id)
        {
            vw_ApplicantRegistrationCompleteEmail AppEmail = new vw_ApplicantRegistrationCompleteEmail();
            AppEmail = _context.vw_ApplicantRegistrationCompleteEmail.Where(u => (u.RegistrationID == id)).FirstOrDefault();



            if (AppEmail != null)
            {
                EmailDto email = new EmailDto();

                email.EmailFrom = AppEmail.EmailFrom;
                email.email = AppEmail.EmailTo;
                email.Subject = AppEmail.Subject;
                email.HTMLBody = AppEmail.FinalBody;


                return Request.CreateResponse(HttpStatusCode.OK, _repo.SendEmailApplicantComplete(email));

            }
            return Request.CreateResponse(HttpStatusCode.BadRequest, 0);

        }
    }
}
