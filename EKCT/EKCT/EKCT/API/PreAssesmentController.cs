﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EKCT.Repo;
using EKCT.dto;
using System.Web.Http.Cors;

namespace EKCT.API
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PreAssesmentController : ApiController
    {

        private EKCT_DevEntities2 _context;
        private Pre_AssessmentRepo _repo;

        public PreAssesmentController()
        {
            _context = new EKCT_DevEntities2();
            _repo = new Pre_AssessmentRepo(_context);
        }

        
        [HttpPost]
        [Route("ReviewTest")]
        public HttpResponseMessage ReviewTest(ReviewTestDto ReviewTest)
        {
          
            return Request.CreateResponse(HttpStatusCode.OK , _repo.ReviewTest(ReviewTest));
        }

        //get random 10 questions for student
        [HttpGet]
        [Route("GetQuestionsOptions")]
        public HttpResponseMessage GetQuestionsOptions()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.getQuestionsOptions());
        }


        
        [HttpGet]
        [Route("GetApplicantScoreDetails/{id}")]
        public HttpResponseMessage GetApplicantScoreDetails(int id)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.GetApplicantScoreDetails(id));
        }

        [HttpGet]
        [Route("AdminRight/{id}")]
        public HttpResponseMessage ConfirmAdminRight(string adminCode)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.ConfirmAdminRight(adminCode));
        }

        [HttpGet]
        [Route("LeadEmail/{email}")]
        public HttpResponseMessage AddLeadEmail(string email)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.AddLeadEmail(email));
        }



        [HttpPost]
        [Route("GradePreAssesment")]
        public HttpResponseMessage Grading(List<StudentResponseDto> response)//FromBody StudentResponseDto response
        {

            //List<StudentResponseDto> responses = new List<StudentResponseDto>();
            //responses.Add(response);
            if (response != null)
                return Request.CreateResponse(HttpStatusCode.OK, _repo.gradePreAssesment(response));
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest,9999);
         }
    }
}
