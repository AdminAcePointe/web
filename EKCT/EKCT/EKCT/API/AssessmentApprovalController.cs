﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EKCT.Repo;
using EKCT.dto;
using System.Web.Http.Cors;


namespace EKCT.API
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AssessmentApprovalController : ApiController
    {

      

        private EKCT_DevEntities2 _context;
        private AssessmentApprovalRepo _repo;

        public AssessmentApprovalController()
        {
            _context = new EKCT_DevEntities2();
            _repo = new AssessmentApprovalRepo(_context);
        }


        [HttpGet]
        [Route("ValidateAdminCredential/{adminCode}")]
        public HttpResponseMessage ValidateAdminCredential(string adminCode)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.ValidateAdminCredential(adminCode));
        }

        [HttpGet]
        [Route("RegisterApplicantAssessment/{email}")]
        public HttpResponseMessage RegisterApplicantAssessment(string email)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _repo.RegisterApplicantAssessment(email));
        }

    }
}
