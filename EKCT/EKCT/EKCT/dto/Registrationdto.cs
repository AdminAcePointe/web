﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EKCT.dto
{
    

    public class Registrationdto
    {
        public int ApplicantID;
        public string FirstName;
        public string LastName;
        public string Phone;
        public string Email;
        public string AddressLine1;
        public string AddressLine2;
        public string City;
        public string Country;
        public string Gender;
        public string MiddleInitial;
        public string ZipCode;
        public int ProgramID;
        public string PaymentDescription;
        public string token;
        public long amount;
        public string state;
        public int CohortID;
    }
}