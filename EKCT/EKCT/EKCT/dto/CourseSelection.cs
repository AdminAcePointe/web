﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace EKCT.dto
{
    public class programDto
    {
        public int programId { get; set; }
        public string code { get; set; }
        public string programName { get; set; }
        public decimal price { get; set; }
        public string description { get; set; }

    }

    public class programDetailDto
    {
        public int programId { get; set; }
        public string code { get; set; }
        public string programName { get; set; }
        public decimal price { get; set; }
        public string description { get; set; }
        public string TargettedAudience { get; set; }
        public string TentativeStartDate { get; set; }
        public  IEnumerable<ProgramCourseDto> Courses { get; set; }
        public IEnumerable<CohortDto> Cohorts { get; set; }
        public string ProgramDuration { get; set; }
    }

    public class ProgramCourseDto
    {
        public int CourseID { get; set; }
        public string CourseName { get; set; }
        public string Duration { get; set; }
        public string CourseWebLink { get; set; }
    }

    public class CourseDto
    {
        public int CourseID { get; set; }
        public Nullable<int> PrerequisiteID { get; set; }
        public string CourseName { get; set; }
        public string CourseCode { get; set; }
        public string Description { get; set; }
        public string Duration { get; set; }
        public decimal Price { get; set; }
       
    }

    public class CohortDto
    {
        public int CohortID { get; set; }
        public string CohortName { get; set; }
        public string MeetingDays { get; set; }
        public string MeetingTime { get; set; }
        public string MeetingDuration { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsNextCohort { get; set; }
    }
}