﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EKCT.dto
{
    public class SignUpDto
    {
        public string firstName;
        public string lastName;
        public string email;
        public string resumePath;
        public string PhoneNo;
        public string referalSource;
        public string gender;

    }

    public class ApplicantAssessmentDto
    {
        public int ApplicantAssessmentID;
        public Nullable<int> AssessmentID;
        public Nullable<int> ApplicantID;
        public string CreatedBy;
        public System.DateTime CreatedDate;
        public string ModifiedBy;
        public Nullable<System.DateTime> ModifiedDate;

    }

    public class ApplicantDto
    {
        public int ApplicantID;
        public string FirstName;
        public string LastName;
        public string Phone;
        public string Email;
        public string AddressLine1;
        public string AddressLine2;
        public string City;
        public string Country;
        public string Gender;
        public string MiddleName;
        public string PostalCode;
        public string State;
    }

    public class Cohort
    {
        public string Cohort_id;
        public string Cohort_name;
        public string Tentative_Start_Date;
        public string Tentative_End_Date;
    }


        public class JsonDto
    {
       public string Program_id;
        public string Program_name;
        public string Price;
        public string Description;
        public List<Cohort> Cohort;
    
    }

}