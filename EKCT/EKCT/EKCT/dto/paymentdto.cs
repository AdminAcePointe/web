﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EKCT.dto
{
    
    public class stripeResult
    {
        public bool payment_status { get; set; }
        public string failure_code { get; set; }
        public string failure_message { get; set; }
        public int registration_status { get; set; }
        public int paymentinsert_status { get; set; }
        public int registration_id { get; set; }

    }
}