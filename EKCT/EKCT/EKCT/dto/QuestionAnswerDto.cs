﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EKCT.dto
{
    public class QuestionAnswerDto
    {
        public int QuestionID;
        public string question;
        public List<string> options;

    }

    public class StudentResponseDto
    {
        public int ApplicantID;
        public int QuestionID;
        public string Question;
        public string studentAnswer;

    }

    public  class QuestionsOptionsDto
    {
        public int id { get; set; }
        public int QuestionID { get; set; }
        public string QuestionType { get; set; }
        public Nullable<int> AssessmentID { get; set; }
        public string Question { get; set; }
        public string QuestionHTML { get; set; }
        public string SummaryHTML { get; set; }
        public string OptionHTML { get; set; }
    }

    public class StateAbbrev
    {
        public string State { get; set; }
        public string Abbrev { get; set; }
        public int StateID { get; set; }
        public int CountryID { get; set; }
    }
    public class CountryAbbrev
    {
        public string CountryName { get; set; }
        public string Abbrev { get; set; }
        public int CountryID { get; set; }
    }
   
}