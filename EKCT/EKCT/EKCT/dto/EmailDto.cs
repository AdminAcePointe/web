﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EKCT.dto
{
    public class EmailDto
    {
        public string firstName;
        public string lastName;
        public string email;
        public string resumePath;
        public string Subject;
        public string HTMLBody;
        public string EmailFrom;
    }

    public class AdminDto
    {
        public string firstName;
        public string lastName;
        public string email;
    }

    public class ApplicantEmailDetailDto
    {
        public int ApplicantID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailTo { get; set; }
        public string Phone { get; set; }
        public string AttachmentFilePath { get; set; }
        public int ApplicantScore { get; set; }
        public string ScorePct { get; set; }
        public Nullable<int> PassScore { get; set; }
        public string Result { get; set; }
        public string Subject { get; set; }
        public string FinalBody { get; set; }
        public string EmailFrom { get; set; }
    }

    public  class AdminEmailDetailDto
    {
        public int ApplicantID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string AdminFirstName { get; set; }
        public string AdminLastName { get; set; }
        public string AdminEmail { get; set; }
        public string EmailTo { get; set; }
        public string Phone { get; set; }
        public string AttachmentFilePath { get; set; }
        public int ApplicantScore { get; set; }
        public string ScorePct { get; set; }
        public Nullable<int> PassScore { get; set; }
        public string Result { get; set; }
        public string Subject { get; set; }
        public string FinalBody { get; set; }
        public string EmailFrom { get; set; }
    }
}
