	/*  Wizard */
	jQuery(function ($) {
		"use strict";
		//  progress bar
		$("#progressbar").progressbar();
		$("#wizard_container").wizard({
			afterSelect: function (event, state) {
				$("#progressbar").progressbar("value", state.percentComplete);
				$("#location").text("(" + state.stepsComplete + "/" + state.stepsPossible + ")");
			}
		});
		// Validate select
	
	});
	if (sessionStorage.getItem("testcompleted") === null) {
	    sessionStorage.setItem("testcompleted", 0)
	    sessionStorage.setItem("email", "")
	    sessionStorage.setItem("name", "")
	    sessionStorage.setItem("resume", "")
	}

/* File upload validate size and file type - For details: https://github.com/snyderp/jquery.validate.file*/

// Summary 
function getVals(formControl, controlType) {
	switch (controlType) {

		case 'question_1':
			// Get the value for a radio
			var value = $(formControl).val();
			$("#question_1").text(value);
			break;

		case 'question_2':
			// Get name for set of checkboxes
			var checkboxName = $(formControl).attr('name');

			// Get all checked checkboxes
			var value = [];
			$("input[name*='" + checkboxName + "']").each(function () {
				// Get all checked checboxes in an array
				if (jQuery(this).is(":checked")) {
					value.push($(this).val());
				}
			});
			$("#question_2").text(value.join(", "));
			break;

		case 'question_3':
			// Get the value for a radio
			var value = $(formControl).val();
			$("#question_3").text(value);
			break;

	    case 'question_4':
	        // Get the value for a radio
	        var value = $(formControl).val();
	        $("#question_4").text(value);
	        break;


	    case 'question_5':
	        // Get the value for a radio
	        var value = $(formControl).val();
	        $("#question_5").text(value);
	        break;


	    case 'question_6':
	        // Get the value for a radio
	        var value = $(formControl).val();
	        $("#question_6").text(value);
	        break;


	    case 'question_7':
	        // Get the value for a radio
	        var value = $(formControl).val();
	        $("#question_7").text(value);
	        break;



	    case 'question_8':
	        // Get the value for a radio
	        var value = $(formControl).val();
	        $("#question_8").text(value);
	        break;



	    case 'question_9':
	        // Get the value for a radio
	        var value = $(formControl).val();
	        $("#question_9").text(value);
	        break;


	    case 'question_10':
	        // Get the value for a radio
	        var value = $(formControl).val();
	        $("#question_10").text(value);
	        break;


		case 'additional_message':
			// Get the value for a textarea
			var value = $(formControl).val();
			$("#additional_message").text(value);
			break;
	}
}

$("#starttest").click(function () {
    window.location = "/Test";
})


$("#reviewtest").click(function () {
    var status = getreviewoption();
    var appID = $("#appID").text();
    var ReviewStatus = {
                            ApplicantID: appID,
                            Status: status,
    }

    var ReviewStatusx = JSON.stringify(ReviewStatus)
    reviewtest(ReviewStatusx, appID)
   
})

// get review options
function getreviewoption() {
    var decision = 0;
    if ($('input[value="Approved"]').is(':checked')) {
        decision = $('input[value="Approved"]').val();
    }
    else if ($('input[value="Rejected"]').is(':checked')) {
        decision = $('input[value="Rejected"]').val();
    }
    else {
        $.confirm({
            columnClass: 'col-md-4',
            containerFluid: true,
            title: 'Review Applicant Test<br><br>',
            content: 'Pleae select one of the options (Approve or Reject)',
            type: 'red',
            typeAnimated: true,

            buttons: {
                close: {
                    text: 'Close',
                    btnClass: 'btn-red'
                }
            }
        });
    }
    return decision;
};

function starttimer() {
    var timer2 = "15:01";
    var interval = setInterval(function () {


        var timer = timer2.split(':');
        //by parsing integer, I avoid all extra string processing
        var minutes = parseInt(timer[0], 10);
        var seconds = parseInt(timer[1], 10);
        --seconds;
        minutes = (seconds < 0) ? --minutes : minutes;
        if (minutes < 0) clearInterval(interval);
        seconds = (seconds < 0) ? 59 : seconds;
        seconds = (seconds < 10) ? '0' + seconds : seconds;
        //minutes = (minutes < 10) ?  minutes : minutes;

        timer2 = minutes + ':' + seconds;
        if (minutes == 0 && seconds == 1) {
            $.confirm({
                columnClass: 'col-md-4',
                containerFluid: true,
                title: 'Time Elapsed !!<br><br>',
                content: 'You have exceeded the time limit for this test.<br><br> Please reach out to us at admissions@eklipseconsult.com for comments / questions',
                type: 'blue',
                typeAnimated: true,
                
                buttons: {
                    close: {
                        text: 'Close',
                        btnClass: 'btn-blue',
                        action: function () {
                            window.location.href = 'https://www.eklipseconsult.com';
                            sessionStorage.setItem("testcompleted", 1)
                        }
                    }
                }
            });


        }
        else {
            $('.countdown').html('Time Left - ' + minutes + ':' + seconds);
        }
    }, 1000);
}
// privacy
$(function () {

    // Initialize form validation on the registration form.
    // It has the name attribute "registration"
    $("form[name='wrapped']").validate({
        // Specify validation rules
        rules: {
            
            terms: {
                required: true
            }
        },
        // Specify validation error messages
        messages: {
            terms:"Please accept our privacy policy"
        },
        errorPlacement: function (error, element) {
            if (element.is('select:hidden')) {
                error.insertAfter(element.next('.nice-select'));
            } else {
                error.insertAfter(element);
            }
        },
        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            $("#loader_form").fadeIn();
            window.location = "/signup"
            $("#loader_form").fadeOut();
        }
    });
});
// sign up
$(function () {
    $("#loader_form").fadeIn();
    $("form[name='wrappedsignup']").validate({
        // Specify validation rules
        rules: {
            fileupload: {
                               fileType: {
                                   types: ["doc", "docx", "pdf", "application/msword", "application/vnd.openxmlformats - officedocument.wordprocessingml.document"]
                               },
                               maxFileSize: {
                                   "unit": "KB",
                                   "size": 10000
                               },
                               minFileSize: {
                                   "unit": "KB",
                                   "size": "2"
                               }
                           },
            firstname: "required",
            lastname: "required",
            email: {
                required: true,
                // Specify that email should be validated
                // by the built-in "email" rule
                email: true
            },
            select: {
                               required: true
                           },
                           phone: {
                               number: true,
                               digits: true,
                               minlength: 10
                           }
        },
        // Specify validation error messages
        messages: {
            firstname: "Please enter your firstname",
            lastname: "Please enter your lastname",
            phone:"Please enter valid phone",
            email: "Please enter a valid email address"
        },
        errorPlacement: function (error, element) {
                       if (element.is('select:hidden')) {
                           error.insertAfter(element.next('.nice-select'));
                       } else {
                           error.insertAfter(element);
                       }
                   },
        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            $("#loader_form").fadeIn();
                   var data = new FormData();
                   var files = $("#fileupload").get(0).files;
                   // Add the uploaded image content to the form data collection
                   if (files.length > 0) {
                       data.append("UploadedImage", files[0]);
                   }

                   var dataobj = {

                       email: $('[name=email]').val(),
                       firstName: $('[name=firstname]').val(),
                       gender: "Male",
                       lastName: $('[name=lastname]').val(),
                       referalSource: $('#referralsource').val(),
                       PhoneNo: $('[name=phone]').val(),
                       resumePath: $('[name=fileupload]').val()
                   }
                   var datax = JSON.stringify(dataobj)
                   sessionStorage.setItem("email", $('[name=email]').val())
                   sessionStorage.setItem("name", $('[name=firstname]').val() + " " + $('[name=lastname]').val())
                   sessionStorage.setItem("resume", "~/resume/" + $('[name=fileupload]').val())

                   uploadresume(data, datax);
                   $("#loader_form").fadeIn();
              
        }
    });
    $("#loader_form").fadeOut();
});
// submit test
$(function () {
    $("form[name='testsubmit']").validate({
        // Specify validation rules
        rules: {

            terms: {
                required: true
            }
        },
       
     
        submitHandler: function (form) {
            $('#wait').css("display", "block")
            $("#sub").attr('disabled', 'disabled');
            var json = []
            $(".summary ul li").each(function () {
                var ApplicantID = 1;
                var Question = $(this).children("h5").text();
                var QuestionID  = $(this).children("p").attr("qid");
                var studentAnswer = $(this).children("p").text();
                item = {}
                item["ApplicantID"] = sessionStorage.getItem("applicantid");
                item["QuestionID"] = QuestionID;
                item["Question"] = Question;
                item["studentAnswer"] = studentAnswer;
                json.push(item);
            });

         
            var result =  JSON.stringify(json)
            sendresult(result);
 
        }
    });
});

function reviewtest(ReviewStatus,id) {

    $.ajax({
        type: "POST",
        async: false,
        dataType: "json",
        data: ReviewStatus,
        traditional: true,
        contentType: "application/json; charset=utf-8",
        url: "https://api.eklipseconsult.com/ReviewTest",///ekctassessments.acepointe.com",
        success: function (response) {
            if (response == 1) {
                sendRegistrationEmail(id)
            }
            else if (response == -1) {
                $.confirm({
                    columnClass: 'col-md-4',
                    containerFluid: true,
                    title: 'Already Reviewed<br><br>',
                    content: 'Applicant Review has been previously completed. ',
                    type: 'orange',
                    typeAnimated: true,

                    buttons: {
                        close: {
                            text: 'Close',
                            btnClass: 'btn-orange'
                        }
                    }
                });
            }
            else {
                $.confirm({
                    columnClass: 'col-md-4',
                    containerFluid: true,
                    title: 'Error<br><br>',
                    content: 'Error sending email to Applicant',
                    type: 'red',
                    typeAnimated: true,

                    buttons: {
                        close: {
                            text: 'Close',
                            btnClass: 'btn-red'
                        }
                    }
                });
            }
            
            
            //window.location = "/result/" + response
        },
        error: function (xhr, ajaxOptions, thrownError) {

            $.confirm({
                columnClass: 'col-md-4',
                containerFluid: true,
                title: 'Error<br><br>',
                content: 'There was an error reviewing test. Please try again later<br><br>' + xhr + "  "+thrownError ,
                type: 'red',
                typeAnimated: true,

                buttons: {
                    close: {
                        text: 'Close',
                        btnClass: 'btn-red'
                    }
                }
            });
        }
    });

}


function sendresult(result) {
    $("#loader_form").css("display", "block");

    $.ajax({
        type: "POST",
        async: false,
        dataType: "json",
        data: result,
        traditional:true,
        contentType: "application/json; charset=utf-8",
        url: "https://api.eklipseconsult.com/GradePreAssesment",///api.eklipseconsult.com",
        success: function (response) {
            sendapplicantEmail(result, response)
            //window.location = "/result/" + response
        },
        error: function (xhr, ajaxOptions, thrownError) {

            $.confirm({
                columnClass: 'col-md-4',
                containerFluid: true,
                title: 'Error<br><br>',
                content: 'There was an error grading the test. Please try again later',
                type: 'red',
                typeAnimated: true,

                buttons: {
                    close: {
                        text: 'Close',
                        btnClass: 'btn-red'
                    }
                }
            });
        }
    });
    $("#loader_form").css("display", "none");
}

function sendapplicantEmail(result, response1) {
   var id =  sessionStorage.getItem("applicantid")
    $.ajax({
        type: "GET",
        async: false,
        dataType: "json",
        traditional: true,
        contentType: "application/json; charset=utf-8",
        url: "https://api.eklipseconsult.com/SendEmailApplicant"+"/" + id,///GradePreAssesment",
        success: function (response) {
            if (response == 1) {
                sendadminEmail(id, response1);
            }
            else {
                $.confirm({
                    columnClass: 'col-md-4',
                    containerFluid: true,
                    title: 'Error<br><br>',
                    content: 'There was an error sending email. Please try again later',
                    type: 'red',
                    typeAnimated: true,

                    buttons: {
                        close: {
                            text: 'Close',
                            btnClass: 'btn-red'
                        }
                    }
                });
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {

            $.confirm({
                columnClass: 'col-md-4',
                containerFluid: true,
                title: 'Error<br><br>',
                content: xhr + " "+ thrownError,
                type: 'red',
                typeAnimated: true,

                buttons: {
                    close: {
                        text: 'Close',
                        btnClass: 'btn-red'
                    }
                }
            });
        }
    });

}

function sendRegistrationEmail(id) {

    $.ajax({
        type: "GET",
        async: false,
        dataType: "json",
        traditional: true,
        contentType: "application/json; charset=utf-8",
        url: "https://api.eklipseconsult.com/SendRegistrationEmail/"  + id,///GradePreAssesment",
        success: function (response) {
            if (response == 1) {
                $.confirm({
                    columnClass: 'col-md-4',
                    containerFluid: true,
                    title: 'Success<br><br>',
                    content: 'Applicant Status have been updated accordingly.<br><br>Email with registration link has been sent to approved applicant',
                    type: 'blue',
                    typeAnimated: true,

                    buttons: {
                        close: {
                            text: 'Close',
                            btnClass: 'btn-blue',
                            action: function () {
                                window.location.href = 'https://www.eklipseconsult.com';
                            }
                        }
                    }
                });
            }
            else {
                $.confirm({
                    columnClass: 'col-md-4',
                    containerFluid: true,
                    title: 'Error<br><br>',
                    content: 'There was an error sending email. Please try again later',
                    type: 'red',
                    typeAnimated: true,

                    buttons: {
                        close: {
                            text: 'Close',
                            btnClass: 'btn-red'
                        }
                    }
                });
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {

            $.confirm({
                columnClass: 'col-md-4',
                containerFluid: true,
                title: 'Error<br><br>',
                content: xhr + " " + thrownError,
                type: 'red',
                typeAnimated: true,

                buttons: {
                    close: {
                        text: 'Close',
                        btnClass: 'btn-red'
                    }
                }
            });
        }
    });

}
function sendadminEmail(id, response1) {
    var id = sessionStorage.getItem("applicantid")
    $.ajax({
        type: "GET",
        async: false,
        dataType: "json",
        traditional: true,
        contentType: "application/json; charset=utf-8",
        url: "https://api.eklipseconsult.com/SendEmailAdmin" + "/" + id,///GradePreAssesment",
        success: function (response) {
            if (response == 1) {
                window.location = "/result/" + response1
            }
            else {
                $.confirm({
                    columnClass: 'col-md-4',
                    containerFluid: true,
                    title: 'Error<br><br>',
                    content: 'There was an error sending email. Please try again later',
                    type: 'red',
                    typeAnimated: true,

                    buttons: {
                        close: {
                            text: 'Close',
                            btnClass: 'btn-red'
                        }
                    }
                });
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {

            $.confirm({
                columnClass: 'col-md-4',
                containerFluid: true,
                title: 'Error<br><br>',
                content: xhr + " " + thrownError,
                type: 'red',
                typeAnimated: true,

                buttons: {
                    close: {
                        text: 'Close',
                        btnClass: 'btn-red'
                    }
                }
            });
        }
    });

}
function signup(datax) {
    $("#loader_form").css("display", "block");
    $.ajax({
        type: "POST",
        async: false,
        dataType: "json",
        data: datax,
        contentType: "application/json; charset=utf-8",
        url: "https://api.eklipseconsult.com/registration", // api.eklipseconsult.com
        success: function (response) {
            if (response > 0) {
                sessionStorage.setItem("applicantid",response)
                window.location = "/Instructions"
            }
            else if (response == -9999) {
                $.confirm({
                    columnClass: 'col-md-4',
                    containerFluid: true,
                    title: 'Unauthorized !!<br><br>',
                    content: 'You are not authorized to take this test.<br><br> Please reach out to us at admissions@eklipseconsult.com for more info',
                    type: 'red',
                    typeAnimated: true,

                    buttons: {
                        close: {
                            text: 'Close',
                            btnClass: 'btn-red',
                            action: function () {
                                window.location.href = 'https://www.eklipseconsult.com';
                            }
                        }
                    }
                });

              
            }
            else if (response == -1) {
                $.confirm({
                    columnClass: 'col-md-4',
                    containerFluid: true,
                    title: 'Test previously taken !!<br><br>',
                    content: 'You have already taken the test.<br><br> Please reach out to us at admissions@eklipseconsult.com for more info',
                    type: 'red',
                    typeAnimated: true,

                    buttons: {
                        close: {
                            text: 'Close',
                            btnClass: 'btn-red',
                            action: function () {
                                window.location.href = 'https://www.eklipseconsult.com';
                            }
                        }
                    }
                });
            }
            else {
                $.confirm({
                    columnClass: 'col-md-4',
                    containerFluid: true,
                    title: 'Technical error !!<br><br>',
                    content: 'There was an issue registering you for the test.<br><br> Please reach out to us at admissions@eklipseconsult.com for more info',
                    type: 'red',
                    typeAnimated: true,

                    buttons: {
                        close: {
                            text: 'Close',
                            btnClass: 'btn-red',
                            action: function () {
                                window.location = "/"
                            }
                        }
                    }
                });
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {

            console.log(xhr.status + " " + thrownError);
        }
    });
    $("#loader_form").css("display", "none");
 
}
function uploadresume(data, datax) {
    $("#loader_form").css("display","block");
    $.ajax({
        type: "POST",
        cache: false,
        processData: false,
        contentType: false,
        data: data,
        url: "https://api.eklipseconsult.com/uploadfiles",
        success: function (response) {

            if (response == 1) {
                signup(datax);
            }
            else {
                $.confirm({
                    columnClass: 'col-md-4',
                    containerFluid: true,
                    title: 'Resume Upload Error !!<br><br>',
                    content: 'there is an issue upload your resume .<br><br> Please reach out to us at admissions@eklipseconsult.com for more info',
                    type: 'red',
                    typeAnimated: true,

                    buttons: {
                        close: {
                            text: 'Close',
                            btnClass: 'btn-red',
                            action: function () {
                                window.location.href = 'https://www.eklipseconsult.com';
                            }
                        }
                    }
                });
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
               console.log(xhr.status + " " + thrownError);
        }
    });
    $("#loader_form").css("display", "none");
}

$(function () {
    $("form[name='RegApplicant']").validate({
        // Specify validation rules
        rules: {

            email: {
                required: true,
                email:true
            }
        },


        submitHandler: function (form) {
       
            var email = $('#valemail').val();
            $("#loader_form").fadeIn();
            RegisterApplicant(email);
            $("#loader_form").fadeOut();
        }
    });
});

$("#starttest").click(function () {
    $("#loader_form").fadeIn();
})





var url = window.location.href;
var page = url.substr(url.lastIndexOf('/') + 1);
var host = window.location.host;

if (url.indexOf('https://' + host + '/Test') != -1) {
    $("#loader_form").fadeIn();
    var tt = sessionStorage.getItem("testcompleted")
    $("#loader_form").fadeOut();
    if (tt != 1) {
       
        getquestions();

        starttimer();
    
    }
    else {
        $.confirm({
            columnClass: 'col-md-4',
            containerFluid: true,
            title: 'Test previously taken !!<br><br>',
            content: 'you have already taken the test..<br><br> Please reach out to us at Assessments@eklipseconsult.com for more info',
            type: 'red',
            typeAnimated: true,

            buttons: {
                close: {
                    text: 'Close',
                    btnClass: 'btn-red',
                    action: function () {
                        window.location.href = "https://www.eklipseconsult.com"
                    }
                }
            }
        });
    }
}

checkifsignup(page);
RegisterApplicantValidate(page)



function checkifsignup(page) {
    if (page == "Test" || page == "Instructions" || page == "result") {
        if (sessionStorage.getItem("applicantid") < 1) {
            window.location = '/'
        }
    }

}

function RegisterApplicantValidate(page) {
   var p = page.toLowerCase()
    if (p == "registerapplicant") {
      
        $.confirm({
            title: 'Validate User',
            content: '' +
            '<form action="" name= "" class="formName">' +
            '<div class="form-group"><input type="password" name="password" class="form-control required inputbottomborder valpwd" placeholder="Passcode"></div> ' +
            '<label class="hidden pwderror" style="text-align: center;color: red; display: none">Invalid Password</label>' +
            '</form>',
            buttons: {
                formSubmit: {
                    text: 'Submit',
                    btnClass: 'btn-blue',
                    action: function () {
                        var name = this.$content.find('.valpwd').val();
                        if (!name) {
                            $.alert('Please provide password');
                            return false;
                        }
                        ValidateApplicantRegistration(name);
                        var response = sessionStorage.getItem("valresponse")
                        if (response == 1) {
                            $(".regApp").css('display', 'block');
                            this.close();
                        }
                        else {
                            var errorpwd = this.$content.find('.pwderror').css('display', 'block');
                            return false;
                        }
                   
                    }
                },
                cancel: function () {
                   window.location = "https://www.eklipseconsult.com"
                },
            },
            onContentReady: function () {
                // bind to events
                var jc = this;
                this.$content.find('form').on('submit', function (e) {
                    // if the user submits the form by pressing enter in the field.
                    e.preventDefault();
                    jc.$$formSubmit.trigger('click'); // reference the button and click it
                });
            }
        });
        //$.confirm({
        //    columnClass: 'col-md-4',
        //    containerFluid: true,
        //    title: 'Enter Passcode<br><br>',
        //    content: '<div class="form-group"><input type="password" name="password" class="form-control required inputbottomborder" placeholder="Passcode"></div> ',
        //    type: 'blue',
        //    typeAnimated: true,
       

        //    buttons: {
        //        Submit: {
        //            text: 'Submit',
        //            btnClass: 'btn-blue',
        //            action: function () {
        //                var pw = $("input[name*='password']").val();
        //                alert(pw)
        //                return;
        //            }
        //        }
        //    }
        //});
        
    }

}

function ValidateApplicantRegistration(email) {

    $.ajax({
        type: "GET",
        async: false,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        url: "https://api.eklipseconsult.com/ValidateAdminCredential" + "/" + email, // api.eklipseconsult.com
        success: function (response) {
            sessionStorage.setItem("valresponse",response);
        },
        error: function (xhr, ajaxOptions, thrownError) {

            console.log(xhr.status + " " + thrownError);
        }
    });
}

function RegisterApplicant(email) {

    $.ajax({
        type: "GET",
        async: false,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        url: "https://api.eklipseconsult.com/RegisterApplicantAssessment" + "/" + email + "/", // api.eklipseconsult.com
        success: function (response) {
            if (response == 1) {
                $.confirm({
                    columnClass: 'col-md-4',
                    containerFluid: true,
                    title: 'Applicant Registration<br><br>',
                    content: 'Applicant Added / Reactivated successfully',
                    type: 'blue',
                    typeAnimated: true,

                    buttons: {
                        close: {
                            text: 'Close',
                            btnClass: 'btn-blue'
                            //,action: function () {
                            //    window.location = 'https://www.eklipseconsult.com';
                               
                            //}
                        }
                    }
                });
               
            }
            else {
                $.confirm({
                    columnClass: 'col-md-4',
                    containerFluid: true,
                    title: 'Error<br><br>',
                    content: 'There was an error adding applicant. Please try again later',
                    type: 'red',
                    typeAnimated: true,

                    buttons: {
                        close: {
                            text: 'Close',
                            btnClass: 'btn-red'
                        }
                    }
                });
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {

            console.log(xhr.status + " " + thrownError);
        }
    });
}

if (url.indexOf('http://' + host + '/result') != -1) {

    sessionStorage.setItem("testcompleted",1)
}


   
function getquestions() {
    $.ajax({
        type: "GET",
        async: false,
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        url: "https://api.eklipseconsult.com/GetQuestionsOptions",
        success: function (response) {
            var data = JSON.parse(response)
            var QuestionHTML
            var OptionHTML
            var SummaryHTML
            var list = ""
            var summary = '<div class="submit step"><br /> <br /><h1 class="main_question question" style="text-align:center;color:#090b1a;margin-bottom:50px;margin-top:30px;font-size:2.5rem" >Review and Submit test</h1><div class="summary"><ul>'

            $.each(data, function (index, value) {
                QuestionHTML = value.QuestionHTML
                OptionHTML = value.OptionHTML
                SummaryHTML = value.SummaryHTML
                list = list + ('  <div class="step"><br /><br />' + QuestionHTML + '<br />' + OptionHTML + '</div>')
                summary = summary + SummaryHTML

            });
            summary = summary + '</ul></div> </div>'
                             

            $(".steploop").html(list + summary)
 

        },
        error: function (xhr, ajaxOptions, thrownError) {

            console.log(xhr.status + " " + thrownError);
        }
    });
}

// 