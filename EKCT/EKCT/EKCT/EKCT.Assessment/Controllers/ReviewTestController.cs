﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace EKCT.Assessment.Controllers
{
    public class ReviewTestController : Controller
    {
        // GET: ReviewTest
        public ActionResult Index()
        {
            return View();
        }
        public async Task<ActionResult> ApproveRejectTest(int id)
        {
            ApplicantEmailDetailDto appDetail = new ApplicantEmailDetailDto();

            appDetail = await getApplicantDetail(id);

            ViewBag.AppID = appDetail.ApplicantID;
            ViewBag.FirstLastName = appDetail.FirstName + " " + appDetail.LastName;
            ViewBag.Email = appDetail.EmailTo;
            ViewBag.Phone = appDetail.Phone;
            ViewBag.Score = appDetail.ScorePct;
            ViewBag.Result = appDetail.Result;

            return View("ReviewTest");
        }

        public async Task<ApplicantEmailDetailDto> getApplicantDetail(int id)
        {

            ApplicantEmailDetailDto product = new ApplicantEmailDetailDto();


            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://api.eklipseconsult.com/");
            //HTTP GET
            var responseTask = client.GetAsync("GetApplicantScoreDetails/" + id);
           // responseTask.Wait();

            var result = responseTask.Result;
            if (result.IsSuccessStatusCode)
            {
                var readTask = result.Content.ReadAsAsync<ApplicantEmailDetailDto>();
                readTask.Wait();

                product = readTask.Result;
            }
            
            return product;
        }



        public class ApplicantEmailDetailDto
        {
            public int ApplicantID { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string EmailTo { get; set; }
            public string Phone { get; set; }
            public string AttachmentFilePath { get; set; }
            public int ApplicantScore { get; set; }
            public string ScorePct { get; set; }
            public Nullable<int> PassScore { get; set; }
            public string Result { get; set; }
            public string Subject { get; set; }
            public string FinalBody { get; set; }
            public string EmailFrom { get; set; }
        }


    }
}