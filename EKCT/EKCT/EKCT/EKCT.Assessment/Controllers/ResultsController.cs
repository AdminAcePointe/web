﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EKCT.Assessment.Controllers
{
    public class ResultsController : Controller
    {
        // GET: Results
        public ActionResult result(int id)
        {
            string view = "failure";
         


            if (id >= 7)
            {
                view = "success";
            }

            TempData["ResultMessage"] = Convert.ToString(id*10) + "%";
                return View(view);
        }
    }
}