﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace EKCT.Assessment
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
name: "RegisterApplicant",
url: "RegisterApplicant/{id}",
defaults: new { controller = "ApplicantRegistration", action = "RegisterApplicant", id = UrlParameter.Optional }
);

            routes.MapRoute(
    name: "approverejecttest",
    url: "approverejecttest/{id}",
    defaults: new { controller = "ReviewTest", action = "ApproveRejectTest", id = UrlParameter.Optional }
);

            routes.MapRoute(
    name: "result",
    url: "result/{id}",
    defaults: new { controller = "results", action = "result", id = UrlParameter.Optional }
);
            routes.MapRoute(
        name: "Test",
        url: "test/{id}",
        defaults: new { controller = "Test", action = "Test", id = UrlParameter.Optional }
    );
            routes.MapRoute(
           name: "instructions",
           url: "instructions/{id}",
           defaults: new { controller = "instructions", action = "instructions", id = UrlParameter.Optional }
       );
            routes.MapRoute(
             name: "signup",
             url: "signup/{id}",
             defaults: new { controller = "SignUp", action = "signup", id = UrlParameter.Optional }
         );

            routes.MapRoute(
                name: "Disclaimer",
                url: "disclaimer/{id}",
                defaults: new { controller = "disclaimer", action = "disclaimer", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
