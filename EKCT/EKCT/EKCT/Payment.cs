//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EKCT
{
    using System;
    using System.Collections.Generic;
    
    public partial class Payment
    {
        public int PaymentID { get; set; }
        public int PaymenttypeID { get; set; }
        public int RegistrationID { get; set; }
        public int PaymentmethodID { get; set; }
        public decimal Amount { get; set; }
        public System.DateTime PaymentDate { get; set; }
        public Nullable<decimal> RefundAmount { get; set; }
        public Nullable<System.DateTime> RefundDate { get; set; }
        public string RefundedBy { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    
        public virtual PaymentMethod PaymentMethod { get; set; }
        public virtual PaymentType PaymentType { get; set; }
        public virtual Registration Registration { get; set; }
    }
}
