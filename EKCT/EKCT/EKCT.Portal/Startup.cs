﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(EKCT.Portal.Startup))]

namespace EKCT.Portal
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
