﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EKCT.FE.Controllers
{
    public class CourseRegistrationController : Controller
    {
        // GET: CourseRegistration
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CourseRegistration(int? id)
        {
           
            ViewBag.Id = id;
            return View("CourseRegistration");
        }

        public ActionResult CourseRegistrationCompletion(int id)
        {

          
            return View("CourseRegistrationCompletion");
        }
    }
}