﻿$(document).ready(function () {

var url = window.location.href;
var page = url.substr(url.lastIndexOf('/') + 1);
var host = window.location.host;
var id = page;
 

getProgram();
getCountries();
getApplicantDetails(page);
stripe();
$(".pppayment").css("display", "none")
/////////////////////////////////////////////////////////
// DOM Manipulation
$(".switch").click(function () {
    var id = $(this).attr("id")
    if (id == "paymentcard") {
        $(".cardpayment").css("visibility", "visible")
        $("#paymentcard").css("backgroundColor", "cornsilk")
        $(".pppayment").css("display", "none")
        $("#paymentpp").css("backgroundColor", "whitesmoke")
      
        card
        return
    }
    else {
        $(".cardpayment").css("visibility", "hidden")
        $("#paymentcard").css("backgroundColor", "whitesmoke")
        $(".pppayment").css("display", "block")
        $("#paymentpp").css("backgroundColor", "cornsilk")
        return
    }

})

$("#Country").on('change', function () {
    var CountryID = $(this).val();
 
    if (CountryID == "") {
        $("#State").html('<option value="" selected>Select State</option>');
    }
    else {

        getStateByCountryID(CountryID)
    }
});

$("#Program").on('change', function () {
    $("#programcontent").css("visibility", "hidden");
    var pid = $("#Program").val();
   
    sessionStorage.setItem("ProgramID", pid);
    getProgramById(pid);
    $("#programcontent").css("visibility", "visible");
});

$(".switch").css("backgroundColor", "whitesmoke")
$("#paymentcard").css("backgroundColor", "cornsilk")
$(".radio-input").css("fontWeight", "900")


/////////////////////////////////////////////////////////
    // Functions


// Load Applicant details
function getApplicantDetails(id) {
  
    if (Math.floor(id) == id && $.isNumeric(id)) {
        $("#loader_form").css("display", "none");
        $.ajax({
            type: "GET",
            async: false,
            dataType: "json",
            traditional: true,
            contentType: "application/json; charset=utf-8",
            url: "https://api.eklipseconsult.com/GetApplicantDetails/" + id,///api.eklipseconsult.com",
            success: function (response) {
                if ( response.FirstName != null) {
                    $("#FirstName").val(response.FirstName);
                    $("#LastName").val(response.LastName);
                    $("#MiddleName").val(response.MiddleName);
                    $("#Email").val(response.Email);
                    $("#Phone").val(response.Phone);
                    $("#Address1").val(response.AddressLine1);
                    $("#PostalCode").val(response.PostalCode);
                    $("#Address2").val(response.AddressLine2);
                    $("#City").val(response.City);
                    $("#State").val(response.StateCode);
                    $("#Country").val(response.Country);
                    $("#Gender").val(response.Gender);
                    $("#loader_form").css("display", "none");

                    var CountryID = response.Country;
                    if (CountryID == null) {
                        $("#State").html('<option value="" selected>Select State</option>');
                    }
                    else {

                        getStateByCountryID(CountryID)
                    }
                }
                else {
              
                        $.confirm({
                            columnClass: 'col-md-4',
                            containerFluid: true,
                            title: 'Not Eligible <br><br>',
                            content: 'You are not eligible to register for the program <br><br> Please reach out to us at admissions@eklipseconsult.com ',
                            type: 'red',
                            typeAnimated: true,

                            buttons: {
                                close: {
                                    text: 'Close',
                                    btnClass: 'btn-red',
                                    action: function () {
                                        window.location.replace('https://www.eklipseconsult.com');

                                    }
                                }
                            }
                        });
                  
                }



                //window.location = "/result/" + response
            },
            error: function (xhr, ajaxOptions, thrownError) {
             
                $.confirm({
                    columnClass: 'col-md-4',
                    containerFluid: true,
                    title: 'Error<br><br>',
                    content: 'There was an error registering for the program.<br><br> Please try again later or contact us at admissions@eklipseconsult.com' ,
                    type: 'red',
                    typeAnimated: true,

                    buttons: {
                        close: {
                            text: 'Close',
                            btnClass: 'btn-red',
                            action: function () {
                                window.location.replace('https://www.eklipseconsult.com');

                            }
                        }
                    }
                });
            }
        });
    }
    else {
        $.confirm({
                    columnClass: 'col-md-4',
                    containerFluid: true,
                    title: 'Not Authorized<br><br>',
                    content: 'You are not authorized to register for the program<br><br> Please contact the admissions team at admissions@eklipseconsult.com for further details ',
                    type: 'red',
                    typeAnimated: true,

                    buttons: {
                        close: {
                            text: 'Close',
                            btnClass: 'btn-red',
                            action: function () {
                                window.location.replace('https://www.eklipseconsult.com');

                            }
                        }
                    }
                });
    }

}


// Get countries
function getCountries() {
    $.ajax({
        type: "GET",
        async: false,
        dataType: "json",
        traditional: true,
        contentType: "application/json; charset=utf-8",
        url: "https://api.eklipseconsult.com/getCountries",///ekctassessments.acepointe.com",
        success: function (response) {
          
            var option = '<option value="" selected>Select country</option>'
            var options = option
            for (var i = 0; i < response.length; i++) {
               // var item = response[i].CountryName;
                options = options + '<option value="' + response[i].CountryID + '">' + response[i].CountryName + '</option>';
               
            }
         
            $("#Country").html(options);
            
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $.confirm({
                columnClass: 'col-md-4',
                containerFluid: true,
                title: 'Error - Countries<br><br>',
                content: 'There was an error retreiving the list of countries. Please try again<br><br>' + xhr + "  " + thrownError,
                type: 'red',
                typeAnimated: true,

                buttons: {
                    close: {
                        text: 'Close',
                        btnClass: 'btn-red'
                    }
                }
            });
        }
    });

}

// Getstate by countryid
function getStateByCountryID(CountryID) {
    $.ajax({
        type: "GET",
        async: false,
        dataType: "json",
        traditional: true,
        contentType: "application/json; charset=utf-8",
        url: "https://api.eklipseconsult.com/getStateByCountry/" + CountryID,
        success: function (response) {

            var option = '<option value="" selected>Select State</option>'

            for (var i = 0; i < response.length - 1; i++) {
                // var item = response[i].CountryName;
                option = option + '<option value="' + response[i].StateID + '">' + response[i].State + '</option>';

            }

            $("#State").html(option);
        },
        error: function (xhr, ajaxOptions, thrownError) {

            $.confirm({
                columnClass: 'col-md-4',
                containerFluid: true,
                title: 'Error - States<br><br>',
                content: 'There was an error retreiving the list of states. Please try again<br><br>' + xhr + "  " + thrownError,
                type: 'red',
                typeAnimated: true,

                buttons: {
                    close: {
                        text: 'Close',
                        btnClass: 'btn-red'
                    }
                }
            });
        }
    });

}

// Create our number formatter.


// Get Program Details 
function getProgramById(ProgramID) {

    $("#ProgramTitle").text("")
    $("#ProgramDesc").text("")
    $("#startdate").html("")
    $("#price").html("")
    $("#audience").html("")
    $("#duration").html("")
    $("#courses").html("")
    $("#meetingdays").html("")
    $("#meetinghours").html("")


    $.ajax({
        type: "GET",
        async: false,
        dataType: "json",
        traditional: true,
        contentType: "application/json; charset=utf-8",
        url: "https://api.eklipseconsult.com/GetProgramById/" + ProgramID,
        success: function (response) {
            var formatter = new Intl.NumberFormat('en-US', {
                style: 'currency',
                currency: 'USD',
            });
            var p = response
            var courses = 'Courses offered as part of this program include '
            $.each(p, function (index, value) {
                $("#ProgramTitle").text(value.programName + " Track")
                $("#ProgramDesc").text(value.description)
                $("#startdate").html('<b >Program Start Date :</b> ' + value.TentativeStartDate)
                $("#programduration").html('<b >Program Duration :</b> ' + value.ProgramDuration)
                $("#price").html('<b >Program Price :</b> ' + formatter.format(value.price))
                $("#audience").html('<b >Target Audience :</b> ' + value.TargettedAudience)
                $("#meetingtimes").html('<b >Meeting Hours :</b> ' + value.Cohorts[0].MeetingTime)
              
                $("#meetingdays").html('<b >Meeting Days :</b> ' + value.Cohorts[0].MeetingDays)
                $("#meetinghours").html('<b >Meeting Duration :</b> ' + value.Cohorts[0].MeetingDuration)
                $("#Cohort").html('<b >Cohort :</b> ' + value.Cohorts[0].CohortName)
                sessionStorage.setItem("cohortid", value.Cohorts[0].CohortID);
                $.each(value.Courses, function (index, value) {
           
                    courses = courses + '<a  href="' + value.CourseWebLink + '"  target="_blank"  style="text-decoration:underline;color:inherit"><i>' + value.CourseName + '</i></a>,';

                })
                
            });

            $("#courses").html(courses)

        },
        error: function (xhr, ajaxOptions, thrownError) {
            $.confirm({
                columnClass: 'col-md-4',
                containerFluid: true,
                title: 'Error - Courses<br><br>',
                content: 'There was an error retreiving the list of courses. Please try again<br><br>' + xhr + "  " + thrownError,
                type: 'red',
                typeAnimated: true,

                buttons: {
                    close: {
                        text: 'Close',
                        btnClass: 'btn-red'
                    }
                }
            });
        }
    });

}


// Get Program Details 
function getProgram() {

    $.ajax({
        type: "GET",
        async: false,
        dataType: "json",
        traditional: true,
        contentType: "application/json; charset=utf-8",
        url: "https://api.eklipseconsult.com/getPrograms/",
        success: function (response) {

 
           var option ='<option value="" selected>Select Program</option>'
       var data = response
           $.each(data, function (index, value) {
 
               option = option + '<option value="' + value.programId +'">' + value.programName+ '</option>'

           });
     
                $("#Program").html(option);

        },
        error: function (xhr, ajaxOptions, thrownError) {
       
            $.confirm({
                columnClass: 'col-md-4',
                containerFluid: true,
                title: 'Error - Programs<br><br>',
                content: 'There was an error retreiving programs list. Please try again later<br><br>' + xhr + "  " + thrownError,
                type: 'red',
                typeAnimated: true,

                buttons: {
                    close: {
                        text: 'Close',
                        btnClass: 'btn-red'
                    }
                }
            });
        }
    });

}




//Stripe
function stripe() {
    $("#loader_form").css("display", "none");
    var stripe = Stripe('pk_test_3lz8Dc1sOfgBaVAYDPZbCyw300J7ON34Yj');
    var elements = stripe.elements();

    var elementStyles = {
        base: {
            color: '#555',
            fontWeight: 500,
            fontFamily: 'Roboto Condensed, Arial, sans-serif',
            fontSize: '20px',
            fontSmoothing: 'antialiased',
      

            '::placeholder': {
                color: '#CFD7DF',
            },
            ':-webkit-autofill': {
                color: '#e39f48',
            },

        },

        invalid: {
            color: '#E25950',

            '::placeholder': {
                color: '#FFCCA5',
            },
        },
    };

    var elementClasses = {
        focus: 'focused',
        empty: 'empty',
        invalid: 'invalid',
    };


    var stripe = Stripe('pk_live_SxoM54ZlotXJS8St7pR2ZVJy00S6vutl4t');
    var elements = stripe.elements();


    var cardNumberElement = elements.create('cardNumber', {
        'placeholder': '',
        'style': elementStyles
    });
    cardNumberElement.mount('#cardnumber');

    var cardExpiryElement = elements.create('cardExpiry', {
        'style': elementStyles
    });
    cardExpiryElement.mount('#cardexpiration', {
        'placeholder': '',
        'style': elementStyles
    });

    var cardCvcElement = elements.create('cardCvc', {
        'placeholder': '',
        'style': elementStyles
    });
    cardCvcElement.mount('#card-cvc-element');


    function setOutcome(result) {
        var successElement = document.querySelector('.success');
        var errorElement = document.querySelector('.error');
        successElement.classList.remove('visible');
        errorElement.classList.remove('visible');

        if (result.token) {
            // In this example, we're simply displaying the token
           
            var am = document.getElementById('amount').value
            var token = result.token.id
            stripeTokenHandler(token, am)
        } else if (result.error) {
            errorElement.textContent = result.error.message;
            errorElement.classList.add('visible');
            $("#loader_form").css("display", "none");
            return;
        }
    }

    cardNumberElement.on('change', function (event) {
        var successElement = document.querySelector('.success');
        var errorElement = document.querySelector('.error');
        successElement.classList.remove('visible');
        errorElement.classList.remove('visible');
        setOutcome(event);
    });

    document.querySelector('form').addEventListener('submit', function (e) {
        e.preventDefault();
        var name = document.getElementById('cardholder').value
        var options = {
            name: name,
            amount: document.getElementById('amount').value
        };
        if (!name) {
            var errorElement = document.querySelector('.error');
            errorElement.textContent = "Please enter Cardholder Name";
            errorElement.classList.add('visible');
            $("#loader_form").css("display", "none");
            return
        }

        stripe.createToken(cardNumberElement, options).then(setOutcome);
    });

    function getRegistrationCompleteEmail(regid) {


        $.ajax({
            type: "GET",
            async: false,
            dataType: "json",
            traditional: true,
            contentType: "application/json; charset=utf-8",
            url: "https://api.eklipseconsult.com/SendEmailApplicantRegistration/" + regid,
            success: function (response) {

            },
            error: function (xhr, ajaxOptions, thrownError) {
                $.confirm({
                    columnClass: 'col-md-4',
                    containerFluid: true,
                    title: 'Error<br><br>',
                    content: 'There was an error sending registration email. Please notify us at admissions@eklipseconsult.com about this error<br><br>' + xhr + "  " + thrownError,
                    type: 'red',
                    typeAnimated: true,

                    buttons: {
                        close: {
                            text: 'Close',
                            btnClass: 'btn-red'
                        }
                    }
                });
            }
        });

    }

    function stripeTokenHandler(token, amount) {
        var form = document.querySelector('form');
        var RegistrationDetails = {

           
            FirstName: $('[name=firstname]').val(),
            MiddleInitial: $('[name=MiddleInitial]').val(),
            LastName: $('[name=lastname]').val(),
            Gender: $('[name=gender]').val(),
            Email: $('[name=email]').val(),
            Phone: $('[name=phone]').val(),
            AddressLine1: $('[name=address]').val(),
            AddressLine2: $('[name=address2]').val(),
            City: $('[name=city]').val(),
            State: $('[name=state]').val(),
            Country: $('[name=country]').val(),
            ZipCode: $('[name=PostalCode]').val(),
            ProgramID: sessionStorage.getItem("ProgramID"),
            ApplicantId: id,
            PaymentDescription: "Student Registration Fee",
            token: token,
            amount: amount,
            CohortID : sessionStorage.getItem("cohortid")
        }
        var Registration = JSON.stringify(RegistrationDetails)

        $.ajax({
            type: "POST",
            async: false,
            dataType: "json",
            data: Registration,
            traditional: true,
            contentType: "application/json; charset=utf-8",
            url: "https://api.eklipseconsult.com/Makepayment",///ekctassessments.acepointe.com",
            success: function (response) {
                var p = JSON.parse(response)
            if (p.payment_status != true && (p.registration_status == 0 || p.registration_status > 0 )) {
                $.confirm({
                    columnClass: 'col-md-4',
                    containerFluid: true,
                    title: 'Card Processing Error<br><br>',
                    content: 'There was an error processing your payment.<br><br>' + 'Message<br>' + p.failure_message,
                    type: 'red',
                    typeAnimated: true,

                    buttons: {
                        close: {
                            text: 'Close',
                            btnClass: 'btn-red'
                        }
                    }
                });
                $("#loader_form").css("display", "none");
            }
          if (p.payment_status == true && p.registration_status > 0) {
         
              var regid = p.registration_id
              getRegistrationCompleteEmail(regid)
              window.location.href = "/CourseRegistrationCompletion/" + id
          }
          if ( p.registration_status < 0) {
              $.confirm({
                  columnClass: 'col-md-4',
                  containerFluid: true,
                  title: 'Already registered<br><br>',
                  content: 'Our records show that you have already registered for the program.<br><br>Please contact us at admissions@eklipseconsult.com for the next steps' ,
                  type: 'blue',
                  typeAnimated: true,

                  buttons: {
                      close: {
                          text: 'Close',
                          btnClass: 'btn-blue',
                          action: function () {
                              window.location.replace('https://www.eklipseconsult.com');

                          }
                      }
                  }
              });
              $("#loader_form").css("display", "none");
          }
         
                $("#loader_form").css("display", "none");
           
            },
            error: function (xhr, ajaxOptions, thrownError) {
            
                $.confirm({
                    columnClass: 'col-md-4',
                    containerFluid: true,
                    title: 'Card payment Error<br><br>',
                    content: 'There was an error registering you for the program .<br><br>'+ 'Please contact us at admissions@eklipseconsult.com to rectify the issue',
                    type: 'red',
                    typeAnimated: true,

                    buttons: {
                        close: {
                            text: 'Close',
                            btnClass: 'btn-red'
                        }
                    }
                });
                $("#loader_form").css("display", "none");
            }
        });
        $("#loader_form").css("display", "none");
    }


    sessionStorage.setItem("paypal.status", null)
    paypal.Buttons({
        style: {
            shape: 'rect',
            color: 'gold',
            layout: 'vertical',
            label: 'paypal',

        },
        createOrder: function (data, actions) {
            return actions.order.create({
                purchase_units: [{
                    amount: {
                        value: '100'
                    }
                }]
            });
        },
        onApprove: function (data, actions) {
            return actions.order.capture().then(function (details) {
                
                sessionStorage.setItem("paypal.status", details.status)
                var am = document.getElementById('amount').value
                paypalTokenHandler(am)
            });
        }

    }).render('#paypal-button-container');


    function paypalTokenHandler(amount) {
        var form = document.querySelector('form');
        var RegistrationDetails = {


            FirstName: $('[name=firstname]').val(),
            MiddleInitial: $('[name=MiddleInitial]').val(),
            LastName: $('[name=lastname]').val(),
            Gender: $('[name=gender]').val(),
            Email: $('[name=email]').val(),
            Phone: $('[name=phone]').val(),
            AddressLine1: $('[name=address]').val(),
            AddressLine2: $('[name=address2]').val(),
            City: $('[name=city]').val(),
            State: $('[name=state]').val(),
            Country: $('[name=country]').val(),
            ZipCode: $('[name=PostalCode]').val(),
            ProgramID: sessionStorage.getItem("ProgramID"),
            ApplicantId: id,
            PaymentDescription: "Student Registration Fee",
            amount: amount,
            token: "paypal",
            CohortID: sessionStorage.getItem("cohortid")
        }
        var Registration = JSON.stringify(RegistrationDetails)

        $.ajax({
            type: "POST",
            async: false,
            dataType: "json",
            data: Registration,
            traditional: true,
            contentType: "application/json; charset=utf-8",
            url: "https://api.eklipseconsult.com/Makepayment",///ekctassessments.acepointe.com",
            success: function (response) {
                var p = JSON.parse(response)
           
                if (p.payment_status != true && (p.registration_status == 0 || p.registration_status > 0)) {
                    $.confirm({
                        columnClass: 'col-md-4',
                        containerFluid: true,
                        title: 'Card Processing Error<br><br>',
                        content: 'There was an error processing your payment.<br><br>' + 'Message<br>' + p.failure_message,
                        type: 'red',
                        typeAnimated: true,

                        buttons: {
                            close: {
                                text: 'Close',
                                btnClass: 'btn-red'
                            }
                        }
                    });
                    $("#loader_form").css("display", "none");
                }
                if (p.payment_status == true && p.registration_status > 0) {

                    var regid = p.registration_id
                    getRegistrationCompleteEmail(regid)
                    window.location.href = "/CourseRegistrationCompletion/" + id
                }
                if (p.registration_status < 0) {
                    $.confirm({
                        columnClass: 'col-md-4',
                        containerFluid: true,
                        title: 'Already registered<br><br>',
                        content: 'Our records show that you have already registered for the program.<br><br>Please contact us at admissions@eklipseconsult.com for the next steps',
                        type: 'blue',
                        typeAnimated: true,

                        buttons: {
                            close: {
                                text: 'Close',
                                btnClass: 'btn-blue',
                                action: function () {
                                    window.location.replace('https://www.eklipseconsult.com');

                                }
                            }
                        }
                    });
                    $("#loader_form").css("display", "none");
                }

                $("#loader_form").css("display", "none");

            },
            error: function (xhr, ajaxOptions, thrownError) {

                $.confirm({
                    columnClass: 'col-md-4',
                    containerFluid: true,
                    title: 'Card payment Error<br><br>',
                    content: 'There was an error registering you for the program .<br><br>' + 'Please contact us at admissions@eklipseconsult.com to rectify the issue',
                    type: 'red',
                    typeAnimated: true,

                    buttons: {
                        close: {
                            text: 'Close',
                            btnClass: 'btn-red'
                        }
                    }
                });
                $("#loader_form").css("display", "none");
            }
        });
        $("#loader_form").css("display", "none");
    }
}

 
})

