﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EKCT.FE.Startup))]
namespace EKCT.FE
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
