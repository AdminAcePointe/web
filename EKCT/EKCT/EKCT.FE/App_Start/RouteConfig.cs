﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace EKCT.FE
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
               name: "CourseRegistration",
               url: "CourseRegistration/{id}",
               defaults: new { controller = "CourseRegistration", action = "CourseRegistration", id = UrlParameter.Optional }
           );


            routes.MapRoute(
               name: "CourseRegistrationCompletion",
               url: "CourseRegistrationCompletion/{id}",
               defaults: new { controller = "CourseRegistration", action = "CourseRegistrationCompletion", id = UrlParameter.Optional }
           );
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
