﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

namespace SampleWeb
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnParse_Click(object sender, EventArgs e)
        {
            if (fileUpload.HasFile)
            {
                
                fileUpload.SaveAs(HttpContext.Current.Server.MapPath("Input/" + fileUpload.FileName));

                CVParseAPI.CVParseAPI api = new CVParseAPI.CVParseAPI();
                FileStream fs = new FileStream(HttpContext.Current.Server.MapPath("Input/" + fileUpload.FileName), FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                string xmlString = api.ParseResume(bytes, fileUpload.FileName, txtAPIkey.Text, txtPassword.Text);
                File.WriteAllText(HttpContext.Current.Server.MapPath("Output/output.xml"), xmlString);

                XDocument xDocument = XDocument.Parse(xmlString);            
                txtResults.Text = xDocument.ToString();

            }
        }

        protected void btnParseFTXML_Click(object sender, EventArgs e)
        {
            if (fileUpload.HasFile)
            {

                fileUpload.SaveAs(HttpContext.Current.Server.MapPath("Input/" + fileUpload.FileName));

                CVParseAPI.CVParseAPI api = new CVParseAPI.CVParseAPI();
                FileStream fs = new FileStream(HttpContext.Current.Server.MapPath("Input/" + fileUpload.FileName), FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                string xmlString = api.ParseResumeFTXML(bytes, fileUpload.FileName, txtAPIkey.Text, txtPassword.Text);
                File.WriteAllText(HttpContext.Current.Server.MapPath("Output/output.xml"), xmlString);

                XDocument xDocument = XDocument.Parse(xmlString);
                txtResults.Text = xDocument.ToString();

            }
        }
    }
}