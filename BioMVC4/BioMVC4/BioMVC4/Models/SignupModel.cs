﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.Mvc;
using System.Data.Entity.Validation;
using System.Text;

namespace BioMVC4.Models
{




    public class SignupModel
    {
        public string DBUser = "BioUser";

        public string SetLogin(string email, string password)
        {
            //TODO: Add new user in database
            return null;
        }

        public static bool CheckIfEmailExists(string email)
        {
            var db = new BioDataEntities();
            var exists = db.MemberSecurables.AsEnumerable().Any(row => email == row.MemberEmail);
            if (exists)
            {
                return true; //"This email is already in use.";
            }
            return false; //"Email is available";
        }

        public static bool AddMember(FormCollection fc, out int _NewMemberID, out string _result)
        {
     
        string DBUser = "BioUser";
        string emailresult;
        _NewMemberID = 0;
        
            // check email existence before proceeding
            if (CheckIfEmailExists(fc["email"]))
            {
                _result = "Email already exists.. Please use a different email or login with current email";
                return true;
            }
            else
            {
                var db = new BioDataEntities();

                // Member
                Member m = new Member
                {
                    MemberFirstName = fc["firstname"],
                    MemberLastName = fc["lastname"],
                    Memberdob = fc["dob"],
                    CreatedBy = DBUser,
                    CreatedDate = DateTime.Now,
                    ModifiedBy = DBUser,
                    ModifiedDate = DateTime.Now
                };

               

                try
                {
                    db.Members.Add(m);
                    db.SaveChanges();
                    _NewMemberID = m.MemberID;

                    // Member credentials
                    MemberSecurable ms = new MemberSecurable
                    {
                        MemberID = _NewMemberID,
                        MemberEmail = fc["email"],
                        Password = LoginModel.Encryptpw(fc["password"]),
                        CreatedDate = DateTime.Now,
                        CreatedBy = DBUser
                    };
                    db.MemberSecurables.Add(ms);                       
                    db.SaveChanges();
                 // Send email
                    emailresult = ContactMember.SendSignUpNotification(fc["firstname"] + " " + fc["lastname"], fc["email"]);
                 
                    _result = "Member Info Added";
                    return true;


                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    _result = "Error occured while adding Member Information. Please try again";
                    return false;
                }

            }

        }

    }

    public class FormattedDbEntityValidationException : Exception
    {
        public FormattedDbEntityValidationException(DbEntityValidationException innerException) :
            base(null, innerException)
        {
        }

        public override string Message
        {
            get
            {
                var innerException = InnerException as DbEntityValidationException;
                if (innerException != null)
                {
                    StringBuilder sb = new StringBuilder();

                    sb.AppendLine();
                    sb.AppendLine();
                    foreach (var eve in innerException.EntityValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().FullName, eve.Entry.State));
                        foreach (var ve in eve.ValidationErrors)
                        {
                            sb.AppendLine(string.Format("-- Property: \"{0}\", Value: \"{1}\", Error: \"{2}\"",
                                ve.PropertyName,
                                eve.Entry.CurrentValues.GetValue<object>(ve.PropertyName),
                                ve.ErrorMessage));
                        }
                    }
                    sb.AppendLine();

                    return sb.ToString();
                }

                return base.Message;
            }
        }
    }
}
