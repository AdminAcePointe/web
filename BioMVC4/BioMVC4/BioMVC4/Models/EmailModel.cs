﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Mail;
using System.IO;
using System.Net;  

namespace BioMVC4.Models
{
    public class EmailModel
    {

        public string SendEmail(EmailParms emailParmObj)
        {
            string result;


            try
            {
                using (MailMessage mail = new MailMessage(emailParmObj.From, emailParmObj.To))
                {
                  mail.Subject = emailParmObj.Subject;
                  mail.Body = emailParmObj.Body;
                  mail.IsBodyHtml = true;
                  SmtpClient smtpClient = new SmtpClient();
                  smtpClient.Send(mail);
                }
                    result = "Message sent";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                result = "Message not succesfull. Please try again later";
           
               
            }
            
            return result;
        }
    }

    public class EmailParms
    {
        public string From { get; set; }
        public string To { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string Attachment { get; set; }
        public string Emailtype { get; set; }
    }
}