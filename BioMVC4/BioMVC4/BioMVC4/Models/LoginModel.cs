﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI.WebControls;


namespace BioMVC4.Models
{
    public class LoginModel
    {
        public int Memberid { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public bool RememberMe { get; set; }


    

        public static string ValidateMember(string email, string pwd)
        {
            
           string result;
           var pwdencr =  LoginModel.Encryptpw(pwd);

            using (BioDataEntities dc = new BioDataEntities())
            {
                var e = dc.MemberSecurables
                    .FirstOrDefault(a => a.MemberEmail.Equals(email) && a.Password.Equals(pwdencr));
                //var currpwd = (from a in dc.MemberSecurables.Where(a => a.MemberEmail == email) select a.Password)
                //    .SingleOrDefault();
                //var rawcurrpwd = Decryptpw(currpwd);
                if (e != null)
                {
                    result = e.MemberID.ToString();
                }
                else
                {
                    result = "0";
                }
            }
           
            return result;
        }

        public static string Encryptpw(string pwd)
        {
            byte[] bytes = System.Text.Encoding.Unicode.GetBytes(pwd);
            string encrytpwd = Convert.ToBase64String(bytes);
            return encrytpwd;
        }

        public static string Decryptpw(string dbpwd)
        {
            byte[] bytes = Convert.FromBase64String(dbpwd);
            string decrytpwd = System.Text.Encoding.Unicode.GetString(bytes);
            return decrytpwd;
        }

        //Action After Login
        public static bool SessionValid()
        {
            var isSessionValid = HttpContext.Current.Session["ActiveMemberID"] != null;

            return isSessionValid;
        }

     

        public static string CreateTempPassword()
        {
            string allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789!@$?_-";
            char[] chars = new char[10];
            Random rd = new Random();

            for (int i = 0; i < 10; i++)
            {
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            }

            return new string(chars);
        }

        public static string InsertTempPassword(string email,string temppwd)
        {
            string result = string.Empty;
            string pwdencr = Encryptpw(temppwd);
            DateTime now = DateTime.Now;

            try
            {
                using (BioDataEntities dc = new BioDataEntities())
                {
                    var e = dc.MemberSecurables
                        .Single(a => a.MemberEmail.Equals(email));

                    if (e != null)
                    { 
                       
                        e.TempPassword = pwdencr;
                        e.Password = null;
                        e.TempPasswordCreateDate = now;
                     
                        dc.SaveChanges();
                        result = "1";
                    }
                    else
                    {
                        result = "Message not succesfull. Please try again later";
                    }
                }

            }

            catch (Exception ex)
            {
                result = "Message not succesfull. Please try again later";
            }
            return result;
        }

        public static string UpdatePassword(string email, string temppwd, string newpwd)
        {
            string result = string.Empty;
            string pwdencr = temppwd == null ? null: Encryptpw(temppwd);
            string newpwdencr = Encryptpw(newpwd);
            DateTime now = DateTime.Now;
            try
            {
                using (BioDataEntities dc = new BioDataEntities())
                {
                    var e = dc.MemberSecurables
                        .FirstOrDefault(a => a.MemberEmail.Equals(email) && a.TempPassword.Equals(pwdencr));

                    //for edit password
                    var passwordtoedit = dc.MemberSecurables.SingleOrDefault(a => a.MemberEmail == email);
                    
                    if (passwordtoedit != null && temppwd == null)
                    {
                        passwordtoedit.TempPassword = null;
                        passwordtoedit.Password = newpwdencr;
                        passwordtoedit.CreatedBy = "BioUser";
                        passwordtoedit.CreatedDate = now;
                        dc.SaveChanges();
                        return "1";
                    }
                    if (e != null)
                    {                      
                        e.TempPassword = null;
                        e.Password = newpwdencr;
                        e.TempPasswordCreateDate = null;
                        dc.SaveChanges();
                        result = "1";
                    }
                    else
                    {
                        result = "0";
                    }
                }

            }

            catch (Exception ex)
            {
                result = "0";
            }
            return result;
        }
    }


}