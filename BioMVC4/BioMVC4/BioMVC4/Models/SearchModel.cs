﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BioMVC4.Models
{
    public class SearchModel
    {
        public static List<SearchUserString_Result> GetSearchResult(string searchstring)
        {
         BioDataEntities bio = new BioDataEntities();
         List<SearchUserString_Result> searchresult = bio.SearchMemberString(searchstring).ToList();

            return searchresult; 
        }
    }
}