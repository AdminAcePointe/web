﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;

namespace BioMVC4.Models
{
    public class ContactMember
    {
        public static string SMTPSender = @"info@acepointe.com";
        public static string AdminEmail = @"ayodele.olatunji@eklipseconsult.com";
       public static string _subject;
        public static string _body;


        public static string SendResetPasswordEmail(string temppwd, string email)
        {
            string url = HttpContext.Current.Request.Url.Authority + @"\Auth\SetPassword";

            BioDataEntities db = new BioDataEntities();
            List<NewTemplate> signup = (
                                 from s in db.Templates
                                 where s.TemplateType == "Password Reset Notification"
                                 select new NewTemplate { TemplateSubject = s.TemplateSubject, TemplateBody = s.TemplateBody }
                                 ).ToList();

            foreach (var a in signup)
            {
                _subject = a.TemplateSubject;
                _body = a.TemplateBody.Replace("[[email]]", email).Replace("[[temppwd]]", temppwd).Replace("[[url]]", url);
            }
            EmailParms emailParmObj = new EmailParms
            {

                From = SMTPSender,
                To = email,
                Subject = _subject,
                Body = _body               
            };
            EmailModel em = new EmailModel();
            string result = em.SendEmail(emailParmObj);

            return result;
        }
        /**
    * Method to contact member
    * 
    * */
        //Log Input to Database:
        public string SendNotification(int senderID, int receipentID, string subject, string message)
        {
            BioDataEntities db = new BioDataEntities();
            Notification n = new Notification
            {
                SenderID = senderID,
                RecipientID = receipentID,
                NotificationSubject = subject,
                Message = message,
                NotificationDate = DateTime.Now,
                CreatedDate = DateTime.Now,
                CreatedBy = "BioUser",
                ModifiedDate = DateTime.Now,
                ModifiedBy = "BioUser"
            };
       
            string senderName = (from s in db.Members where s.MemberID == senderID select s.MemberFirstName + " " + s.MemberLastName).FirstOrDefault();
            string recepientEmail = (from r in db.MemberSecurables where r.MemberID == receipentID select r.MemberEmail).FirstOrDefault();
            string result;

      
            List<NewTemplate> conmem = (
                                 from s in db.Templates
                                 where s.TemplateType == "Member Contact Notification"
                                 select new NewTemplate { TemplateSubject = s.TemplateSubject, TemplateBody = s.TemplateBody }
                                 ).ToList();

            foreach (var a in conmem)
            {
                _subject = a.TemplateSubject.Replace("[[sendername]]", senderName);
                _body = a.TemplateBody.Replace("[[sendername]]", senderName).Replace("[[subject]]", subject).Replace("[[message]]", message).Replace("[[memberid]]", senderID.ToString());
            }

            try
            { // send email
                EmailParms emailparms = new EmailParms
                {
                    From = SMTPSender,
                    To = recepientEmail,
                    Subject = _subject,
                    Body = _body
                };
                EmailModel em = new EmailModel();
               result =  em.SendEmail(emailparms);

            // write to database
                db.Notifications.Add(n);
                db.SaveChanges();

              
            }
            catch(Exception ex){
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                result = "Message failed. Please try again later";
            }

          

      return result;

        }
        //Send signup email
        public static string SendSignUpNotification( string fullname, string email)
        {
            string result;
            List<NewTemplate> nt = new List<NewTemplate>();
            BioDataEntities db = new BioDataEntities();
            List<NewTemplate> signup = (
                                 from s in db.Templates
                                 where s.TemplateType == "Registration Notification"
                                 select new NewTemplate { TemplateSubject = s.TemplateSubject, TemplateBody = s.TemplateBody }
                                 ).ToList();

            foreach (var a in signup)
            {
                _subject = a.TemplateSubject.Replace("[[fullname]]", fullname);
                _body = a.TemplateBody.Replace("[[email]]", email).Replace("[[fullname]]", fullname);
            }
            EmailParms emailparms = new EmailParms
            {
                From = SMTPSender,
                To = email,
                Subject = _subject,
                Body = _body
            };
            EmailModel em = new EmailModel();
            try
            {
                result = em.SendEmail(emailparms);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                result = "Message failed. Please try again later";
            }
            return result;
        }
        //Send Message to Admin
        public string SendAdminNotification(string subject, string senderName, string senderEmail, string message)
        {
            string result;
              List<NewTemplate> nt = new List<NewTemplate>();
            BioDataEntities db = new BioDataEntities();
             List<NewTemplate> adm = (
                                  from s in db.Templates
                                  where s.TemplateType == "Contact Form Notification"
                                  select new NewTemplate { TemplateSubject = s.TemplateSubject, TemplateBody = s.TemplateBody }
                                  ).ToList();
   
             foreach (var a in adm)
             {
                  _subject = a.TemplateSubject;
                  _body = a.TemplateBody.Replace("[[email]]", senderEmail).Replace("[[message]]", message).Replace("[[Full name]]", senderName).Replace("[[subject]]", subject);
             }
            EmailParms emailparms = new EmailParms
            {
                From = SMTPSender,
                To = AdminEmail,
                Subject = _subject,
                Body = _body
            };
            EmailModel em = new EmailModel();
            try
            {
                 result = em.SendEmail(emailparms);
                
            }
            catch (Exception ex){
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            result = "Message failed. Please try again later";
            }
            return result;
        }

        //Referral
        public string ReferPerson(string firstname, string lastname,string memberid, string emailaddress, string subject, string message)
        {
            string result;
            Int32 memid = Convert.ToInt32(memberid);
            BioDataEntities db = new BioDataEntities();
            string senderName = (from s in db.Members where s.MemberID == memid select s.MemberFirstName + " " + s.MemberLastName).FirstOrDefault();

            List<NewTemplate> reff = (
                                 from s in db.Templates
                                 where s.TemplateType == "Referral Notification"
                                 select new NewTemplate { TemplateSubject = s.TemplateSubject, TemplateBody = s.TemplateBody }
                                 ).ToList();

            foreach (var a in reff)
            {
                _subject = a.TemplateSubject.Replace("[[sendername]]", senderName);
                _body = a.TemplateBody.Replace("[[email]]", emailaddress).Replace("[[message]]", message).Replace("[[sendername]]", senderName).Replace("[[subject]]", subject);
            }
            EmailParms emailparms = new EmailParms
            {
                From = SMTPSender,
                To = emailaddress,
                Subject = _subject,
                Body = _body
            };

            EmailModel em = new EmailModel();

            //Log the information into the db.
            MemberReferral mr = new MemberReferral
            {
                 MemberID =  Convert.ToInt32(memberid)
                ,FirstName = firstname
                ,LastName = lastname
                ,Subject = subject
                ,Message = message
                ,ReferralEmailAddress = emailaddress
                ,CreatedBy = "BioUser"
                ,CreatedDate = DateTime.Now
                ,LastModifiedBy = "BioUser"
                ,LastModifiedDate = DateTime.Now
            };
            try
            {
                db.MemberReferrals.Add(mr);
                db.SaveChanges();
                result = em.SendEmail(emailparms);
               
            }
            catch(Exception ex){

                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                result = "Message failed. Please try again later";
            }
            return result;
        }
    }

    public class NewTemplate
    {
      
        public string TemplateSubject { get; set; }
        public string TemplateBody { get; set; }
   
    }  
    
}