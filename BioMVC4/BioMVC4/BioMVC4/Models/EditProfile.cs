﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.EnterpriseServices;
using System.Runtime.CompilerServices;
using WebGrease.Css.Extensions;

namespace BioMVC4.Models
{
    public class EditProfile
    {
        //Change Password:
        //Change Password:
        public static bool UpdatePassword(string email, string oldpwd, string newpwd, string verifynewpwd,
            out string message)
        {
            try
            {

                //check if password value supplied is different from oldvalue:
                var rawOldPwd = string.IsNullOrEmpty(oldpwd) ? null : LoginModel.Decryptpw(oldpwd);
                if (rawOldPwd == newpwd && newpwd != null)
                {
                    message = "";
                    return true;
                }

                if (newpwd == verifynewpwd && !string.IsNullOrEmpty(newpwd))
                {
                    var updatepwdmsg = LoginModel.UpdatePassword(email, null, newpwd);
                    if (updatepwdmsg == "1")
                    {
                        message = "Your password has been updated";
                        return true;
                    }

                    //@"<h2 id='success' class='center' style='color:green'>Profile updated successfully</h2>";
                    message = "We are unable to update your password. Please try again";
                    return false; //Failed to update password
                }
                if (string.IsNullOrEmpty(newpwd))
                {
                    message = "We are unable to update your password. Please try again";
                    return true;
                }
                message = "The passwords you have entered do not match";
                return false; //The passwords do not match
            }
            catch (Exception ex)
            {
                message = "We are unable to update your password. Please try again";
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex.GetBaseException());
                return false;
            }
        }

        //Update Personal Info: FirstName, LastName, PhoenNumber, City,PostalCode, Country, State, Dob
        public static bool UpdatePersonalInfo(int memberid, string firstname, string lastname, string dob, string phone,
            string city,
            string state, string country, string postalcode, out string message)
        {
            DateTime modDate = DateTime.Now;
            string modUser = "BioUser";
            try
            {
                using (BioDataEntities db = new BioDataEntities())
                {
                    var mdb = db.Members.SingleOrDefault(m => m.MemberID == memberid);
                    mdb.MemberFirstName = firstname;
                    mdb.MemberLastName = lastname;
                    mdb.Memberdob = dob;
                    mdb.ModifiedBy = modUser;
                    mdb.ModifiedDate = modDate;

                    var pdb = mdb.MemberPhones.SingleOrDefault();
                    if (pdb == null)
                    {
                        pdb = new MemberPhone
                        {
                            MemberID = mdb.MemberID,
                            PhoneNumber = phone,
                            CreatedDate = modDate,
                            CreatedBy = modUser,
                            ModifiedDate = modDate,
                            ModifiedBy = modUser
                        };
                        db.MemberPhones.Add(pdb);
                    }
                    else
                    {
                        pdb.PhoneNumber = phone;
                        pdb.ModifiedBy = modUser;
                        pdb.ModifiedDate = modDate;
                    }

                    var adb = mdb.MemberAddresses.SingleOrDefault();

                    if (adb == null)
                    {
                        adb = new MemberAddress
                        {
                            MemberID = mdb.MemberID,
                            City = city,
                            StateProvince = state,
                            CountryRegion = country,
                            PostalCode = postalcode,
                            CreatedDate = modDate,
                            CreatedBy = modUser,
                            ModifiedDate = modDate,
                            ModifiedBy = modUser

                        };
                        db.MemberAddresses.Add(adb);
                    }
                    else
                    {
                            adb.City = city;
                            adb.StateProvince = state;
                            adb.CountryRegion = country;
                            adb.PostalCode = postalcode;
                            adb.ModifiedDate = modDate;
                            adb.ModifiedBy = modUser;
                    }
                  
                    

                    db.SaveChanges();
                }

                message = "Personal info updated successfully";
                return true;
            }
            catch (Exception ex)
            {
                message = "We are unable to update your personal information. Please try again";
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex.GetBaseException());
                return false;
            }
        }

        public static string UploadFile(HttpPostedFileBase file, string memberfirstnamelastname, out string message)
        {
            string targetFolder = HttpContext.Current.Server.MapPath("~/Content/files/");


            if (file != null && file.ContentLength > 0)
            {
                try
                {
                    string pathsuffix = memberfirstnamelastname + "_" + DateTime.Now.Minute + "_" +
                                        Path.GetFileName(file.FileName);
                    string fullpath = Path.Combine(targetFolder, pathsuffix);
                    file.SaveAs(fullpath);
                    message = null;
                    return @"../../Content/files/" + pathsuffix;
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex.GetBaseException());
                    message = "There was a problem uploading " + file.FileName;
                    return null;
                }
            }
            message = null;
            return null;
        }

        //GetTagId
        public static List<int> GetTagId(string tags)
        {
            BioDataEntities db = new BioDataEntities();
            List<int> tagId = new List<int>();
            List<string> tagdup = tags.Replace("\r\n", "").Replace("\t", "").Trim().Split(',').ToList();
            List<string> taglist = tagdup.Distinct().ToList();
            string dbUser = "BioUser";
            DateTime modDate = DateTime.Now;
            foreach (string t in taglist)
            {
                bool tagexists = db.Tags.AsEnumerable().Any(row => t.Trim() == row.Tag1);

                if (tagexists && t.Trim().Length > 0)
                {
                    int gt = (from tag in db.Tags where tag.Tag1 == t.Trim() select tag.TagID).FirstOrDefault();
                    if (!string.IsNullOrEmpty(gt.ToString()))
                    {
                        tagId.Add(gt);
                    }

                }
                else if (!tagexists && t.Trim().Length > 0)
                {
                    Tag newTag = new Tag
                    {
                        Tag1 = t.Trim(),
                        CreatedDate = modDate,
                        CreatedBy = dbUser
                    };
                    try
                    {
                        db.Tags.Add(newTag);
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex.GetBaseException());
                    }

                    tagId.Add(newTag.TagID);
                }
            }

            return tagId;
        }

        //Update Bio Info: LinkedIn, Facebook, Twitter, ProfilePhoto, Resume, ShortDesc, Tags
        public static bool UpdateBioInfo(int memid, string linkedin, string facebook, string twitter,
            HttpPostedFileBase photo, HttpPostedFileBase resume, string description, string tags, out string message)
        {
            DateTime modDate = DateTime.Now;
            string modUser = "BioUser";

            var db = new BioDataEntities();
            //Loadup the member entity, and subsequently, relevant variables
            var memberInDb = db.Members.SingleOrDefault(i => i.MemberID == memid);
            var resumeInDb = db.MemberDetails.SingleOrDefault(i => i.MemberID == memid);
            var memsocialmediaInDb = db.MemberSocialMedias.Where(i => i.MemberID == memid);
            var linkedinInDb = memsocialmediaInDb.SingleOrDefault(i => i.SocialMediaType.ToLower() == "linkedin");
            var facebookInDb = memsocialmediaInDb.SingleOrDefault(i => i.SocialMediaType.ToLower() == "facebook");
            var twitterInDb = memsocialmediaInDb.SingleOrDefault(i => i.SocialMediaType.ToLower() == "twitter");
            var memTagsInDb = db.MemberTags.Where(i => i.MemberID == memid);
            var tagid = GetTagId(tags);
            //Upload Photo and Resume
            string photoMessage;
            string resumeMessage;
            var firstlastname = memberInDb == null ? "" : memberInDb.MemberFirstName + memberInDb.MemberLastName;
            var savedPhoto = UploadFile(photo, firstlastname, out photoMessage);
            var savedResume = UploadFile(resume, firstlastname, out resumeMessage);
            if (photo != null && photoMessage != null)
            {
                message = photoMessage;
                return false;
            }

            if (resume != null && resumeMessage != null)
            {
                message = resumeMessage;
                return false;
            }
            //Map input to DB objects.
            if (savedPhoto != null && photo != null && memberInDb != null)
            {
                memberInDb.MemberProfilePhotoURL = savedPhoto;
                memberInDb.ModifiedDate = modDate;
                memberInDb.ModifiedBy = modUser;
            }
            if (resumeInDb != null && resumeMessage != null)
            {
                resumeInDb.Resume = savedResume;
                resumeInDb.ModifiedDate = modDate;
                resumeInDb.ModifiedBy = modUser;
            }
            if (memberInDb != null)
            {
                memberInDb.MemberDescription = description.Trim();
                memberInDb.ModifiedDate = modDate;
                memberInDb.ModifiedBy = modUser;
            }
            if (linkedinInDb != null)
            {
                linkedinInDb.SocialMediaURL = linkedin;
                linkedinInDb.ModifiedBy = modUser;
                linkedinInDb.ModifiedDate = modDate;
            }
            else
            {
                var addSm = new MemberSocialMedia
                {
                    Member = memberInDb,
                    CreatedBy = modUser,
                    CreatedDate = modDate,
                    SocialMediaType = "LinkedIn",
                    SocialMediaURL = linkedin
                };
                db.MemberSocialMedias.Add(addSm);
            }

            if (facebookInDb != null)
            {
                facebookInDb.SocialMediaURL = facebook;
                facebookInDb.ModifiedBy = modUser;
                facebookInDb.ModifiedDate = modDate;
            }
            else
            {
                var addSm = new MemberSocialMedia
                {
                    Member = memberInDb,
                    CreatedBy = modUser,
                    CreatedDate = modDate,
                    SocialMediaType = "Facebook",
                    SocialMediaURL = facebook
                };
                db.MemberSocialMedias.Add(addSm);
            }
            if (twitterInDb != null)
            {
                twitterInDb.SocialMediaURL = twitter;
                twitterInDb.ModifiedBy = modUser;
                twitterInDb.ModifiedDate = modDate;
            }
            else
            {
                var addSm = new MemberSocialMedia
                {
                    Member = memberInDb,
                    CreatedBy = modUser,
                    CreatedDate = modDate,
                    SocialMediaType = "Twitter",
                    SocialMediaURL = twitter
                };
                db.MemberSocialMedias.Add(addSm);
            }

            //Clear all current tags
            foreach (var memberTag in memTagsInDb)
            {
                db.MemberTags.Remove(memberTag);
            }
            //Add new tags
            foreach (var t in tagid)
            {
                MemberTag mt = new MemberTag
                {
                    MemberID = memid,
                    TagID = t,
                    CreatedDate = modDate,
                    CreatedBy = modUser,
                    ModifiedDate = modDate,
                    ModifiedBy = modUser
                };
                db.MemberTags.Add(mt);
            }

            //Save Changes
            try
            {
                db.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}",
                            validationError.PropertyName,
                            validationError.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex.GetBaseException());
                message =
                    "I am sorry, but I ran into some issues while trying to update your profile. Please try again.";
                return false;
            }
            finally
            {
                db.Dispose();
            }
            message = "Your bio information has been successfully updated.";
            return true;
        }

        //Helper method for spliting list into smaller sublists
        public static List<List<T>> ChunkList<T>(List<T> source, int chunkSize)
        {
            return source
                .Select((x, i) => new { Index = i, Value = x })
                .GroupBy(x => x.Index / chunkSize)
                .Select(x => x.Select(v => v.Value).ToList())
                .ToList();
        }


        public static bool UpdateMemberExperience(FormCollection fc, int memberId, out string message)
        {
            DateTime modDate = DateTime.Now;
            string modUser = "BioUser";
            var db = new BioDataEntities();

            var success = "Your work history was successfully updated";
            var failure = "Failed to update workhistory";
            var individualExperiences = ChunkList((from x in fc.AllKeys select x).ToList(), 9);

            //remove all memberExperiences
            var experienceToDelete = db.MemberExperiences.Where(m => m.MemberID == memberId);
            foreach (var me in experienceToDelete)
            {
                //remove Role responsibilities
                var responsibilitiesToDelete =
                    db.MemberExperienceDetails.Where(i => i.MemberExperienceID == me.MemberExperienceID);
                foreach (var r in responsibilitiesToDelete)
                {
                    db.MemberExperienceDetails.Remove(r);
                }
                //Remove Experience
                db.MemberExperiences.Remove(me);
            }
            //Add experience
            foreach (var e in individualExperiences)
            {
                MemberExperience newMemberExperience;
                try
                {
                    newMemberExperience = new MemberExperience
                    {
                        MemberID = memberId,
                        Position = fc[e.SingleOrDefault(i => i.Contains("expposition"))].Trim(),
                        ExperienceSummary = fc[e.SingleOrDefault(i => i.Contains("expsummary"))].Trim(),
                        ExperienceStartDate = Convert.ToDateTime(fc[e.SingleOrDefault(i => i.Contains("expfrom"))]),
                        CompanyName = fc[e.SingleOrDefault(i => i.Contains("expcompany["))].Trim(),
                        CompanyCity = fc[e.SingleOrDefault(i => i.Contains("expcompanycity"))].Trim(),
                        ExperienceEndDate =
                            fc[e.SingleOrDefault(i => i.Contains("expto"))].ToLower().Trim() == "present"
                                ? DateTime.Parse("12/31/9999")
                                : Convert.ToDateTime(fc[e.SingleOrDefault(i => i.Contains("expto"))]),
                        CompanyCountry = fc[e.SingleOrDefault(i => i.Contains("expcountry"))].Trim(),
                        CompanyState = fc[e.SingleOrDefault(i => i.Contains("expstate"))].Trim(),
                        CreatedBy = modUser,
                        CreatedDate = modDate
                    };
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex.GetBaseException());
                    message =
                        "Oops, I was unable to accept edits to your career experience. Please check the form and correct all errors.";
                    return false;
                }
                db.MemberExperiences.Add(newMemberExperience);
                //Save Changes so we can use the Experience
                try
                {
                    db.SaveChanges();
                }
                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            Trace.TraceInformation("Property: {0} Error: {1}",
                                validationError.PropertyName,
                                validationError.ErrorMessage);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex.GetBaseException());
                    message = failure;
                    return false;
                }
                //Add responsibilities
                List<string> responsiList = fc[e.SingleOrDefault(i => i.Contains("expresp"))].Split(';').ToList();
                foreach (var r in responsiList)
                {
                    var newResponsibilities = new MemberExperienceDetail
                    {
                        MemberExperienceID = newMemberExperience.MemberExperienceID,
                        CreatedBy = modUser,
                        CreatedDate = modDate,
                        Responsibiliti = r.Trim()
                    };
                    db.MemberExperienceDetails.Add(newResponsibilities);
                }
            }

            //Save Changes
            try
            {
                db.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}",
                            validationError.PropertyName,
                            validationError.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex.GetBaseException());
                message = failure;
                return false;
            }
            finally
            {
                db.Dispose();
            }
            message = success;
            return true;
        }

        public static bool UpdateMemberEducation(FormCollection fc, int memberId, out string message)
        {
            DateTime modDate = DateTime.Now;
            string modUser = "BioUser";
            var db = new BioDataEntities();
            var success = "Your education history was successfully updated";
            var failure = "Failed to update education history";
            var newAdd = ChunkList((from x in fc.AllKeys select x).ToList(), 7);

            //Remove
            var toDelete = db.MemberEducations.Where(m => m.MemberID == memberId);
            foreach (var d in toDelete)
            {
                db.MemberEducations.Remove(d);
            }
            //Add
            foreach (var a in newAdd)
            {
                MemberEducation newMemberEducation;
                try
                {
                    newMemberEducation = new MemberEducation
                    {
                        MemberID = memberId,
                        Institution = fc[a.SingleOrDefault(i => i.Contains("eduinstitution"))].Trim(),
                        InstitutionState = fc[a.SingleOrDefault(i => i.Contains("edustate"))].Trim(),
                        InstitutionCountry = fc[a.SingleOrDefault(i => i.Contains("educountry"))].Trim(),
                        Major = fc[a.SingleOrDefault(i => i.Contains("edumajor"))].Trim(),
                        Degree = fc[a.SingleOrDefault(i => i.Contains("edudegree"))].Trim(),
                        GraduationYear = Int32.Parse(fc[a.SingleOrDefault(i => i.Contains("edugradyear"))]),
                        InstitutionComment = fc[a.SingleOrDefault(i => i.Contains("eduschoolcomments"))].Trim(),
                        CreatedDate = modDate,
                        CreatedBy = modUser
                    };
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex.GetBaseException());
                    message = "Oops, I was unable to accept edits to your education history. Please check the form and correct all errors.";
                    return false;
                }
                db.MemberEducations.Add(newMemberEducation);
                //Save Changes
                try
                {
                    db.SaveChanges();
                }
                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            Trace.TraceInformation("Property: {0} Error: {1}",
                                validationError.PropertyName,
                                validationError.ErrorMessage);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex.GetBaseException());
                    message = failure;
                    return false;
                }
            }

            message = success;
            return true;
        }

        public static bool UpdateMemberCertification(FormCollection fc, int memberId, out string message)
        {
            DateTime modDate = DateTime.Now;
            string modUser = "BioUser";
            var db = new BioDataEntities();
            var success = "Your certification was successfully updated";
            var failure = "Failed to update your certification";
            var newAdd = ChunkList((from x in fc.AllKeys select x).ToList(), 3);

            //Remove
            var toDelete = db.MemberCertifications.Where(m => m.MemberID == memberId);
            foreach (var d in toDelete)
            {
                db.MemberCertifications.Remove(d);
            }
            //Add
            foreach (var a in newAdd)
            {
                //Check if the certification exists;
                var certname = fc[a.SingleOrDefault(i => i.Contains("certname"))];
                var certtype = fc[a.SingleOrDefault(i => i.Contains("certtype"))];
                var certdesc = fc[a.SingleOrDefault(i => i.Contains("certdesc"))];
                var exists = db.Certifications.SingleOrDefault(i => i.CertificationName.ToLower() == certname);
                try
                {
                    if (exists == null)
                    {
                        Certification newCert = new Certification
                        {
                            CertificationName = certname,
                            CertificationType = certtype,
                            CreatedBy = modUser,
                            CreatedDate = modDate
                        };
                        db.Certifications.Add(newCert);
                        db.SaveChanges();
                        MemberCertification newMemberCertification = new MemberCertification
                        {
                            MemberID = memberId,
                            CertificationID = newCert.CertificationID,
                            Description = certdesc,
                            CreatedBy = modUser,
                            CreatedDate = modDate
                        };
                        db.MemberCertifications.Add(newMemberCertification);
                    }
                    else
                    {
                        MemberCertification newMemberCertification = new MemberCertification
                        {
                            MemberID = memberId,
                            CertificationID = exists.CertificationID,
                            Description = certdesc,
                            CreatedBy = modUser,
                            CreatedDate = modDate
                        };
                        db.MemberCertifications.Add(newMemberCertification);
                    }
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex.GetBaseException());
                    message =
                        "Oops, I was unable to accept edits to your education history. Please check the form and correct all errors.";
                    return false;
                }
                try
                {
                    db.SaveChanges();
                }
                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            Trace.TraceInformation("Property: {0} Error: {1}",
                                validationError.PropertyName,
                                validationError.ErrorMessage);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex.GetBaseException());
                    message = failure;
                    return false;
                }
            }
            message = success;
            return true;
        }

        public static bool UpdateMemberAssociation(FormCollection fc, int memberId, out string message)
        {
            DateTime modDate = DateTime.Now;
            string modUser = "BioUser";
            var db = new BioDataEntities();
            var success = "Your Association was successfully updated";
            var failure = "Failed to update your Association";
            var newAdd = ChunkList((from x in fc.AllKeys select x).ToList(), 3);

            //Remove
            var toDelete = db.MemberAssociations.Where(m => m.MemberID == memberId);
            foreach (var d in toDelete)
            {
                db.MemberAssociations.Remove(d);
            }
            //Add
            foreach (var a in newAdd)
            {
                //Check if the Association exists;
                var assocname = fc[a.SingleOrDefault(i => i.Contains("association["))];
                var assoccomment = fc[a.SingleOrDefault(i => i.Contains("associationcomment"))];
                var exists = db.MemberAssociations.SingleOrDefault(i => i.Association.Association1.ToLower() == assocname.ToLower());
                var assorole = String.IsNullOrEmpty(fc[a.SingleOrDefault(i => i.Contains("associationrole"))])? null: fc[a.SingleOrDefault(i => i.Contains("associationrole"))].ToLower();
                var roleid = db.AssociationRoles.SingleOrDefault(i => i.AssociationRole1.ToLower() == assorole); 
                try
                {
                    if (exists == null)
                    {
                        Association newAssoc = new Association
                        {
                            Association1 = assocname,
                            CreatedBy = modUser,
                            CreatedDate = modDate
                        };
                        db.Associations.Add(newAssoc);
                        db.SaveChanges();

                       
                            MemberAssociation newMemberAssociation = new MemberAssociation
                            {
                                MemberID = memberId,
                                AssociationID = newAssoc.AssociationID,
                                Description = assoccomment,
                                CreatedBy = modUser,
                                CreatedDate = modDate,
                                AssociationRoleId = roleid == null? 1: roleid.AssociationRoleID
                            };
                            db.MemberAssociations.Add(newMemberAssociation);
                    }
                    else
                    {
                       
                            MemberAssociation newMemberAssociation = new MemberAssociation
                            {
                                MemberID = memberId,
                                AssociationID = exists.AssociationID,
                                Description = assoccomment,
                                CreatedBy = modUser,
                                CreatedDate = modDate,
                                AssociationRoleId = roleid == null ? 1 : roleid.AssociationRoleID
                            };
                            db.MemberAssociations.Add(newMemberAssociation);
                    }
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex.GetBaseException());
                    message = "Oops, I was unable to accept edits to your Association. Please check the form and correct all errors.";
                    return false;
                }
                try
                {
                    db.SaveChanges();
                }
                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            Trace.TraceInformation("Property: {0} Error: {1}",
                                validationError.PropertyName,
                                validationError.ErrorMessage);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex.GetBaseException());
                    message = failure;
                    return false;
                }
            }
            message = success;
            return true;
        }
    }
}