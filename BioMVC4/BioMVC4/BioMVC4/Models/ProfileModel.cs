﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity.Validation;

namespace BioMVC4.Models
{
    public class ProfileModel
    {
        public Profile ProfileBio = new Profile();

        //Using the MemberID, get the member's Bio
        public Profile GetProfile(int memberid)
        {
            using (BioDataEntities db = new BioDataEntities())
            {
                var m = db.Members.SingleOrDefault( a => a.MemberID.Equals(memberid));
                var mems = db.MemberSecurables.SingleOrDefault(ms => ms.MemberID == memberid);
                var cityState = db.MemberAddresses.FirstOrDefault(cs => cs.MemberID.Equals(memberid));
                var phone = db.MemberPhones.SingleOrDefault(mp => mp.MemberID == memberid);
                var resume = db.MemberDetails.SingleOrDefault(i => i.MemberID == memberid);
                //Check if profile exists
                if (m == null)
                {
                    return null;
                }
                else
                {
                    ProfileBio.MemberId = m.MemberID.ToString();
                    ProfileBio.FirstName = m.MemberFirstName;
                    ProfileBio.LastName = m.MemberLastName;
                    ProfileBio.MemberEmail = mems == null? null: mems.MemberEmail;
                    ProfileBio.MemberPhone = phone == null ? null: phone.PhoneNumber;
                    ProfileBio.Password = mems == null? null : mems.Password;
                    ProfileBio.Title = m.MemberTitle;
                    ProfileBio.DoB = m.Memberdob;
                    ProfileBio.Photo = m.MemberProfilePhotoURL;
                    if (m.CreatedDate != null)
                        ProfileBio.MemberSince = m.CreatedDate.Value.ToString("MMMM yyyy");
                    ProfileBio.Description = m.MemberDescription;
                    if (cityState != null)
                    {
                        ProfileBio.City = cityState.City;
                        ProfileBio.State = cityState.StateProvince;
                        ProfileBio.Country = cityState.CountryRegion;
                        ProfileBio.Postalcode = cityState.PostalCode;
                    }
                    ProfileBio.Experience = GetExperience(memberid);
                    ProfileBio.Education = GetEducation(memberid);
                    ProfileBio.Tags = GetTags(memberid);
                    ProfileBio.Certification = GetCertifications(memberid);
                    ProfileBio.Association = GetAssociations(memberid);
                    ProfileBio.Facebook = GetSocialMedia(memberid, "fb");
                    ProfileBio.Twitter = GetSocialMedia(memberid, "tw");
                    ProfileBio.LinkedIn = GetSocialMedia(memberid, "li");
                    ProfileBio.Resume = resume == null ? null: resume.Resume;

                }
            }
            return ProfileBio;
        }

        //Using the MemberID, get the member's Experience
        public List<Experience> GetExperience(int memberid)
        {
            List<Experience> experiences = new List<Experience>();
            using (BioDataEntities db = new BioDataEntities())
            {
                var exp = db.MemberExperiences
                    .Where(m => m.MemberID.Equals(memberid))
                    .OrderByDescending(b=> b.ExperienceEndDate)
                    .ToList();
                //Check if member has experience exists
                if (exp.Count  == 0)
                {
                    return null; //Member has no experience

                }
                //Add the experience to the List of ProfileExperience
                foreach (var experience in exp)
                {
                    Experience e = new Experience
                    {
                        ExperienceId = experience.MemberExperienceID.ToString(),
                        StartDate = experience.ExperienceStartDate.ToString("MMM yyyy"),
                        EndDate = experience.ExperienceEndDate == new DateTime(9999,12,31) ? "Present" : experience.ExperienceEndDate.Value.ToString("MMM yyyy"),
                        Position = experience.Position,
                        Responsibilities = GetExperienceDetails(memberid, Convert.ToInt32(experience.MemberExperienceID)),
                        ExperienceSummary = experience.ExperienceSummary,
                        Employer = experience.CompanyName,
                        EmployerCity = experience.CompanyCity,
                        EmployerState = experience.CompanyState,
                        EmployerCountry = experience.CompanyCountry
                    };
                    experiences.Add(e);
                    //Get Experience Details

                }
            }
            return experiences;
        }
        //Get the member's experience details for each job
        public List<string> GetExperienceDetails(int memberid, int memberexperienceid)
        {
            List<string> expDetails = new List<string>();
            using (BioDataEntities db = new BioDataEntities()) 
            {
                var ExpD = (from e in db.MemberExperiences
                            join ed in db.MemberExperienceDetails
                            on e.MemberExperienceID equals ed.MemberExperienceID
                            where e.MemberID.Equals(memberid) & e.MemberExperienceID.Equals(memberexperienceid)
                            select ed.Responsibiliti).ToList();
                foreach (string e in ExpD) 
                {
                    expDetails.Add(e);
                }
            }
            return expDetails;
        }

        //Using the MemberID, get the member's Education
        public List<Education> GetEducation(int memberid)
        {
            List<Education> educations = new List<Education>();
            using (BioDataEntities db = new BioDataEntities())
            {
                var edu = db.MemberEducations
                    .Where(m => m.MemberID.Equals(memberid))
                    .OrderByDescending(b=> b.GraduationYear)
                    .ToList();
                //Check if member has education listed
                if (edu.Count == 0)
                {
                    return null;
                }
                else
                {
                    //Add the Education to the List of ProfileExperience
                 
                    foreach (var education in edu)
                    {
                       
                        Education e = new Education
                        {
                            MemberEducationId = education.MemberEducationID,
                            GraduationYear = education.GraduationYear.ToString(),
                            Institution = education.Institution,
                            Degree = education.Degree,
                            Major = education.Major,
                            InstitutionState = education.InstitutionState,
                            InstitutionCountry = education.InstitutionCountry,
                            InstitutionComments = education.InstitutionComment
                        };
                        educations.Add(e);
                    }
                }
            }
            return educations;
        }

        //Using the MemberID, get the certifications
        public List<Certification> GetCertifications(int memberid)
        {
            List<Certification> certifications = new List<Certification>();
            using (BioDataEntities db = new BioDataEntities())
            {
                var cert = (from mc in db.MemberCertifications
                                            join c in db.Certifications on mc.CertificationID equals c.CertificationID
                                            where mc.MemberID == memberid
                                            select new  { mc.CertificationID, c.CertificationName, mc.Description });
                //Add the certification to the List of MemberCertifications
                foreach (var c in cert.ToList())
                {
                     Certification ce = new Certification
                        {

                            Name = c.CertificationName,
                            Description = c.Description,
                            CertificationId = c.CertificationID.ToString()
                        };
                    
                    certifications.Add(ce);
                }
            }
        return certifications;
        }

        //Using the MemberID, get the Associations
        public List<Association> GetAssociations(int memberid)
        {
            List<Association> associations = new List<Association>();
            using (BioDataEntities db = new BioDataEntities())
            {
                var asso = (from ma in db.MemberAssociations
                                     join a in db.Associations on ma.AssociationID equals a.AssociationID
                                     join r in db.AssociationRoles on ma.AssociationRoleId equals r.AssociationRoleID
                                     where ma.MemberID == memberid
                            select new { ma.AssociationID, a.Association1, ma.Description, r.AssociationRole1 });
               
                foreach (var aa in asso.ToList())
                {
                    Association ae = new Association
                    {
                        AssociationId = aa.AssociationID,
                        Name = aa.Association1,
                        Description = aa.Description,
                        YourRole = aa.AssociationRole1
                    };
                    associations.Add(ae);
                }
            }
            return associations;
        }



        //Get Tags
        public List<string> GetTags(int memberid)
        {
            List<string> listTags = new List<string>();
            using (BioDataEntities db = new BioDataEntities())
            {
                var tags = from membertag in db.MemberTags
                    where membertag.MemberID.Equals(memberid)
                    join tag in db.Tags on membertag.TagID equals tag.TagID
                    select tag.Tag1;
                foreach (var t in tags)
                {
                    listTags.Add(t);
                }

            }
            return listTags;
        }

        //GetSocialMedia
        public string GetSocialMedia(int memberid, string smt )
        {
            using (BioDataEntities db = new BioDataEntities()) 
            {
                string searchType = "";
                if (smt == "fb") { searchType = "FACEBOOK"; }
                else if (smt == "tw") { searchType = "TWITTER"; }
                else if (smt == "li") { searchType = "LINKEDIN"; }

                string smURL = (from sm in db.MemberSocialMedias
                         where sm.MemberID == memberid && sm.SocialMediaURL.ToUpper().Contains(searchType)
                             select sm.SocialMediaURL).FirstOrDefault();
                return smURL;
            } 
        }
        
        public class Profile
        {
            public string MemberId { get; set; }
            public string FirstName { get; set; }
            public string MemberEmail { get; set; }
            public string MemberPhone { get; set; }

            public string Password { get; set; }
            public string LastName { get; set; }
            public string Title { get; set; }
            public string Photo { get; set; }
            public string DoB { get; set; }
            public string MemberSince { get; set; }
            public string Description { get; set; }
            public string City { get; set; }
            public string Country { get; set; }
            public string State { get; set; }
            public string Postalcode { get; set; }
            public List<Experience> Experience { get; set; }
            public List<string> ExperienceDets { get; set; }
            public List<Education> Education { get; set; }
            public List<Certification> Certification { get; set; }
            public List<String> Tags { get; set; }
            public List<Association> Association { get; set; }
            public string Facebook { get; set; }
            public string Twitter { get; set; }
            public string LinkedIn { get; set; }
            public string Resume { get; set; }
        }

        public class Experience
        {
            public string ExperienceId { get; set; }
            public string StartDate { get; set; }
            public string EndDate { get; set; }
            public string Position { get; set; }
            public List<string> Responsibilities { get; set; }
            public string ExperienceSummary { get; set; }
            public string Employer { get; set; }
            public string EmployerCity { get; set; }
            public string EmployerState { get; set; }
            public string EmployerCountry { get; set; }
            
            //TODO: Determine the delimiter for responsibilities.

        }

        public class Education
        {
            public string GraduationYear { get; set; }
            public string Institution { get; set; }
            public string Degree { get; set; }
            public string Major { get; set; }
            public string InstitutionCountry { get; set; }
            public string InstitutionState { get; set; }
            public string InstitutionComments { get; set; }
            public int MemberEducationId { get; set; }
        }

        public class Certification
        {
            public string Name { get; set; }
            public string Description { get; set; }
            public string CertificationType { get; set; }
            public string CertificationId { get; set; }
        }
    

        public class Association
        {
            public string Name { get; set; }
            public string Description { get; set; }
            public string YourRole { get; set; }
            public int AssociationId { get; set; }
        }

        public class Tags
        {
            public string TagId { get; set; }
            public string TagName { get; set; }
        }

        /**
         * 
         * CONTACT MEMBER
         * 
         * */
        public class contactMe
        {
            int IDFrom { get; set; }
            int IDTo { get; set; }
            string EmailFrom { get; set; }
            string EmailTo { get; set; }
            string Subject { get; set; }
            string Message { get; set; }
        }
     }

}