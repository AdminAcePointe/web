﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using TestResumeParsingForms.CVParseAPI;

namespace TestResumeParsingForms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }      

        private void button1_Click(object sender, EventArgs e)
        {
            txtName.Text = "";
            txtAddress.Text = "";
            txtCity.Text = "";
            txtState.Text = "";
            txtZip.Text = "";
            txtEmail.Text = "";
            txtPhone.Text = "";
            txtResumeText.Text = "";

            Stopwatch watch = new Stopwatch();

            //Do things
            try
            {
                if (openFileDialog1.ShowDialog(this) == DialogResult.OK)
                {
                    watch.Start();
                    string strfilename = openFileDialog1.InitialDirectory + openFileDialog1.FileName;

                   
                    CVParseAPI.CVParseAPI api = new CVParseAPI.CVParseAPI();
                    FileStream fs = new FileStream(strfilename, FileMode.Open, FileAccess.Read);
                    BinaryReader br = new BinaryReader(fs);
                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                    string xmlString = api.ParseResumeFTXML(bytes, openFileDialog1.FileName, "Your SecretKey", "Your Password");
                  
                    richTextBox1.Text = xmlString;
                 
                    watch.Stop();
                    lblElapse.Text = watch.Elapsed.TotalSeconds.ToString();
                    lblElapse.Refresh();

                    string element = "";
                    XmlReader reader = XmlReader.Create(new StringReader(xmlString));
                    while (reader.Read())
                    {

                        if (reader.NodeType == XmlNodeType.Element)
                        {
                            try
                            {
                                element = reader.Name;
                            }
                            catch (Exception ex)
                            { }
                        }
                        else if (reader.NodeType == XmlNodeType.Text)
                        {
                            switch (element)
                            {
                                case "Name":
                                    try
                                    {
                                        txtName.Text = reader.Value;
                                        // Console.WriteLine("name: " + fname.Text);
                                    }
                                    catch (Exception ex)
                                    { }
                                    break;
                                case "Zipcode":
                                    try
                                    {
                                        txtZip.Text = reader.Value;
                                    }
                                    catch (Exception ex)
                                    { }
                                    // Console.WriteLine("zip: " + zip.Text);
                                    break;
                                case "Phone":
                                    try
                                    {
                                        txtPhone.Text = reader.Value;
                                    }
                                    catch (Exception ex)
                                    { }
                                    //Console.WriteLine("phone: " + phone.Text);
                                    break;
                                case "Email":
                                    try
                                    {
                                        txtEmail.Text = reader.Value;
                                    }
                                    catch (Exception ex)
                                    { }
                                    //Console.WriteLine("email: " + email.Text);
                                    break;
                                case "Address":
                                    try
                                    {
                                        txtAddress.Text = reader.Value;
                                    }
                                    catch (Exception ex)
                                    { }
                                    //Console.WriteLine("address: " + phone.Text);
                                    break;
                                case "State":
                                    try
                                    {
                                        txtState.Text = reader.Value;
                                    }
                                    catch (Exception ex)
                                    { }
                                    //Console.WriteLine("state: " + phone.Text);
                                    break;
                                case "City":
                                    try
                                    {
                                        txtCity.Text = reader.Value;
                                    }
                                    catch (Exception ex)
                                    { }
                                    //Console.WriteLine("state: " + phone.Text);
                                    break;
                                case "ResumeText":
                                    try
                                    {
                                        txtResumeText.Text = reader.Value;
                                    }
                                    catch (Exception ex)
                                    { }
                                    //Console.WriteLine("state: " + phone.Text);
                                    break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                txtErrors.Text = ex.Message;               
            }  
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
            var files = Directory.EnumerateFiles(@"C:\Resumes\Valid\", "*.*", SearchOption.TopDirectoryOnly)
            .Where(s => s.EndsWith(".doc") || s.EndsWith(".docx") || s.EndsWith(".pdf") || s.EndsWith(".xls") || s.EndsWith(".xlsx") || s.EndsWith(".html") || s.EndsWith(".htm") || s.EndsWith(".rtf"));

            //Loop through each file in directory above, write over the textbox text values each time it loops over...
            foreach (string fileName in files)
            {
                Stopwatch watch = new Stopwatch();
                watch.Start();
                //Do things
                try
                {
                    string strfilename = fileName;

                    CVParseAPI.CVParseAPI api = new CVParseAPI.CVParseAPI();
                    FileStream fs = new FileStream(strfilename, FileMode.Open, FileAccess.Read);
                    BinaryReader br = new BinaryReader(fs);
                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                    string xmlString = api.ParseResumeFTXML(bytes, Path.GetFileName(fileName), "Your SecretKey", "Your Password");
                    richTextBox1.Text = xmlString;
                    richTextBox1.Refresh();
                                    
                }
                catch (Exception ex)
                {
                   
                }
                watch.Stop();
                lblElapse.Text = watch.Elapsed.TotalSeconds.ToString();
                lblElapse.Refresh();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
    
}
