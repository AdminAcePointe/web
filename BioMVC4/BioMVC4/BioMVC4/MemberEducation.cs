//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BioMVC4
{
    using System;
    using System.Collections.Generic;
    
    public partial class MemberEducation
    {
        public int MemberEducationID { get; set; }
        public int MemberID { get; set; }
        public string Institution { get; set; }
        public Nullable<int> GraduationYear { get; set; }
        public string Degree { get; set; }
        public string Major { get; set; }
        public string InstitutionCity { get; set; }
        public string InstitutionState { get; set; }
        public string InstitutionCountry { get; set; }
        public string InstitutionComment { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
    
        public virtual Member Member { get; set; }
    }
}
