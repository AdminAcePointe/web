﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace BioMVC4
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                 name: "PrivacyPolicy",
                url: "PrivacyPolicy/{id}",
                defaults: new { controller = "Home", action = "PrivacyPolicy", id = UrlParameter.Optional }
                );

            routes.MapRoute(
                name: "Referral",
                url: "Referral/{id}",
                defaults: new { controller = "Home", action = "Referral", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Contact",
                url: "ContactUs/{id}",
                defaults: new { controller = "Home", action = "Contact", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "SetPassword",
                url: "SetPassword/{id}",
                defaults: new { controller = "Auth", action = "SetPassword", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "ValidateUser",
                url: "ValidateUser/{id}",
                defaults: new { controller = "Auth", action = "ValidateUser", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "ResetPassword",
                url: "ResetPassword/{id}",
                defaults: new { controller = "Auth", action = "ResetPassword", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "SignUp",
                url: "SignUp/{id}",
                defaults: new { controller = "SignUp", action = "SignUp", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Login",
                url: "Login/{id}",
                defaults: new { controller = "Auth", action = "Login", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Search",
                url: "Search/{id}",
                defaults: new { controller = "Search", action = "Search", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                 name: "EditProfile",
                 url: "SaveProfile/{id}",
                 defaults: new { controller = "Auth", action = "SaveProfile", id = UrlParameter.Optional }
             );
            
            routes.MapRoute(
                name: "Profile",
                url: "GetProfile/{id}",
                defaults: new { controller = "Profile", action = "GetProfile", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        
        }
    }
}