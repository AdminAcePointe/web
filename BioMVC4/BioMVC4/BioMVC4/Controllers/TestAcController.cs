﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BioMVC4.Controllers
{
    public class TestAcController : Controller
    {
        //
        // GET: /TestAC/

    public ActionResult Index()
    {
          
    return View("TestAc");
}
 
public ActionResult GetMP(string term)
{
    BioDataEntities db = new BioDataEntities();
var result = from t in  db.Tags 
        where t.Tag1.ToLower().Contains(term)
        select t.Tag1;            
    return Json(result, JsonRequestBehavior.AllowGet);
}

    }
}
