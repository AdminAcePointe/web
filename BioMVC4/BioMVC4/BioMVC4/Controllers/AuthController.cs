﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;
using BioMVC4.Models;

namespace BioMVC4.Controllers
{
    public class AuthController : Controller
    {


         private static string _result;
         private static int _memid;
        private readonly BioDataEntities _context;

        public AuthController()
        {
            _context = new BioDataEntities();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        // GET: /Auth/Login

        public ActionResult Login()
        {
            ViewBag.Title = "Member Login";
            ViewBag.ValidateResult = "1";

          

            return View();
        }


        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult ValidateUser()
        {
            var email = Request["username"];
            var pwd = Request["pwd"];


            if (ModelState.IsValid)
            {
                // Validate member
                string result = LoginModel.ValidateMember(email, pwd);
                if (result != "0")
                {
                    Session["ActiveEmail"] = email;
                    Session["ActiveMemberID"] = result;
                  //  string t = HttpContext.Current.Session["ActiveURL"].ToString();
                    // Check if it was a redirect
                    if (Convert.ToString(Session["ActiveURL"]) == "Referral")
                    {
                        
                        return RedirectToAction("Home", "Referral");
                    }
                    else
                    {
                        return RedirectToAction("GetProfile", "Profile", new { id = result });
                    }
                }
                else
                {
                    ViewBag.ValidateResult = result;
                    TempData["Result"] = @"<div class='alert alert-danger'  id='contactError' style='margin-top:10px;color:red'> <strong>Invalid Username \ Password combination</strong></div>";
                    return RedirectToAction("Login", "Auth");

                }
            }
            return Content("");
        }



        // GET: /Auth/ResetPasword
        public ActionResult ResetPassword()
        {
            ViewBag.Title = "Reset Password";
            return View("ResetPasswordEmail");
        }

        // GET: /Auth/ResetPasword
        public ActionResult SendResetPasswordEmail()
        {

            if (Request != null)
            {
                var email = Request["email"];
                var temppwd = LoginModel.CreateTempPassword();
                var instemppwd = LoginModel.InsertTempPassword(email, temppwd);
                if (instemppwd == "1")
                {
                    var result = ContactMember.SendResetPasswordEmail(temppwd, email);
                    TempData["emailResult"] = @"<div class='alert  alert-success'  id='' style='color:green'> <strong>Message Sent. Please check your email for temporary password</strong></div>";
                }
                else
                {
                    TempData["emailResult"] = @"<div class='alert alert-danger'  id='contactError' style='margin-top:10px;color:red'> <strong>Errors encountered during process. Please try again later.</strong></div>";
                    return RedirectToAction("SendResetPasswordEmail");
                }

            }

            return RedirectToAction("SetPassword");
        }

        // GET: /Auth/SetPasword
        public ActionResult SetPassword()
        {

            return View();
        }

        // GET: /Auth/SetPasword
        public ActionResult SetPasswordTemp()
        {
            if (Request != null)
            {
                var email = Request["email"];
                var temppwd = Request["temppwd"];
                var newpwd = Request["newpwd"];
                var updatePwd = LoginModel.UpdatePassword(email, temppwd, newpwd);
                if (updatePwd == "1")
                {
                    return RedirectToAction("Login");
                }
                else
                {
                    TempData["updatePWDResult"] = @"<div class='alert alert-danger'  id='contactError' style='margin-top:10px;color:red'> <strong>Errors encountered during process. Please try again later.</strong></div>";
                    return RedirectToAction("SetPassword");
                }

            }
            TempData["updatePWDResult"] = @"<div class='alert alert-danger'  id='contactError' style='margin-top:10px;color:red'> <strong>Errors encountered during process. Please try again later.</strong></div>";
            return RedirectToAction("SetPassword");
        }

        protected ActionResult ValidateSub(string result, int memid)
        {

            if (result.Contains("Email") == true)
            {
                @TempData["SignUpMessageResult"] = @"<div class='alert alert-danger col-sm-10 col-sm-offset-1'  id='contactError' style='margin-top:5px;color:red'> " + result + "</div>";
                return RedirectToAction("SignUp", "SignUp");
            }
            else if (result.Contains("Error") == true)
            {
                @TempData["SignUpMessageResult"] = "<div class='alert alert-danger col-sm-10 col-sm-offset-1' id='contactError' style='margin-top: 10px; color: red'><strong>" + result + "</strong</div>";
                return RedirectToAction("SignUp", "SignUp");
            }
            else
            {
                TempData["SignUpMessageResult"] = @"<div class='alert alert-success col-sm-10 col-sm-offset-1' id='contactSuccess' style='margin-top: 5px;background-color:#c5f3ed'><span>" + result + "</span><span><a id='member_login' style='color: black; margin-left:5px'>click here to Login</a></span></div>";
                Session["ActiveMemberID"] = memid;
                
                return RedirectToAction("EditProfile/" + memid, "Profile");
            }


           
        }



        public ActionResult SignUpNewMember(FormCollection formCollection)
        {
          
          
           
           bool result = SignupModel.AddMember(formCollection, out _memid, out _result);
           

            return ValidateSub(_result, _memid);

          
           
        }
        
// Contact us email
       
        public ActionResult ContactUs()
        {
            ContactMember cm = new ContactMember();
           string result =  cm.SendAdminNotification(Request["subject"], Request["name"], Request["emailaddress"], Request["message"]);
            TempData["ContactMessageResult"] = @"<div class='alert alert-success'  id='contactSuccess' style='margin-top:10px;color:green;background-color:orange'> <strong>" + result + "</strong></div>";

           
            return RedirectToAction("Contact", "Home");
        }

       
        public JsonResult AssociationAutoComplete(string term)
{
         
         BioDataEntities db = new BioDataEntities();
           

            var result = (from b in db.Associations
                          where b.Association1.StartsWith(term)
                          select new { label = b.Association1, value = b.Association1}).Distinct();
            return Json(result, JsonRequestBehavior.AllowGet); 
}

        public JsonResult CertificationAutoComplete(string term)
        {

            BioDataEntities db = new BioDataEntities();


            var result = (from b in db.Certifications
                          where b.CertificationName.StartsWith(term)
                          select new { label = b.CertificationName, value = b.CertificationName }).Distinct();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AssociationRoleAutoComplete(string term)
        {

            BioDataEntities db = new BioDataEntities();


            var result = (from b in db.AssociationRoles
                          where b.AssociationRole1.StartsWith(term)
                          select new { label = b.AssociationRole1, value = b.AssociationRole1 }).Distinct();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult EducationInstitutionAutoComplete(string term)
        {

            BioDataEntities db = new BioDataEntities();


            var result = (from b in db.EducationInstitutions
                          where b.EducationInstitution1.StartsWith(term)
                          select new { label = b.EducationInstitution1, value = b.EducationInstitution1 }).Distinct();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult EducationDegreeAutoComplete(string term)
        {

            BioDataEntities db = new BioDataEntities();


            var result = (from b in db.EducationDegrees
                          where b.EducationDegree1.StartsWith(term)
                          select new { label = b.EducationDegree1, value = b.EducationDegree1 }).Distinct();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult EducationMajorAutoComplete(string term)
        {

            BioDataEntities db = new BioDataEntities();


            var result = (from b in db.EducationMajors
                          where b.EducationMajor1.StartsWith(term)
                          select new { label = b.EducationMajor1, value = b.EducationMajor1 }).Distinct();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CountryAutoComplete(string term)
        {

            BioDataEntities db = new BioDataEntities();


            var result = (from b in db.Countries
                          where b.CountryName.StartsWith(term)
                          select new { label = b.CountryName, value = b.CountryName }).Distinct();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult StateAutoComplete(string term, string id)
        {
            
            //int id2 = int.Parse(id);

            BioDataEntities db = new BioDataEntities();


            var result = (from b in db.StateProvinces join c in db.Countries on b.CountryId equals c.CountryId
                          where (b.StateProvinceName.StartsWith(term) && c.CountryName.Equals(id))
                          select new { label = b.StateProvinceName, value = b.StateProvinceName }).Distinct();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

     
    }
}
