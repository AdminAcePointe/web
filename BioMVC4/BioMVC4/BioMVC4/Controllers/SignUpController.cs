﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;
using BioMVC4.Models;
using System.Configuration;
using BioMVC4.ViewModel;

namespace BioMVC4.Controllers
{
    public class SignUpController : Controller
    {
        public ActionResult SignUp()
        {
            return View("SignUp");
        }

        public JsonResult getState(string Id)
        {

            if (ModelState.IsValid)
            {
                int StateId = Int32.Parse(Id);
                BioDataEntities db = new BioDataEntities();
                var state = db.StateProvinces.Where(n => n.CountryId == StateId)
                    .Select(
                        b => new SelectListItem { Value = b.StateProvinceName, Text = b.StateProvinceName })
                    .ToList();

                return Json(state, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

     

  
    }
}
