﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using BioMVC4.Models;

namespace BioMVC4.Controllers
{
    public class SearchController : Controller
    {
        [System.Web.Http.HttpPost]
        [ValidateInput(false)]
        public ActionResult Search(string id)
        {           
            ViewBag.Id = id;
// Check if session is active
            bool Valid = LoginModel.SessionValid();
             if (Valid == true)
            {               
                return View(SearchModel.GetSearchResult(id));
            }

            return RedirectToAction("Login","Auth");
        
        }
   
       

       
    }
}
