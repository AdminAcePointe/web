﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;
using BioMVC4.Models;

namespace BioMVC4.Controllers
{
    public class ProfileController : Controller
    {
        public int profileid;

        [System.Web.Http.HttpPost]
        [ValidateInput(false)]
        public ActionResult GetProfile(int id)
        {

              Session["SearchMemberID"] = id;
   


            ProfileModel pm = new ProfileModel();
            // Check if session is active
            bool valid = LoginModel.SessionValid();
            if (valid)
            {
              
                return View(pm.GetProfile(id));
            }
            return RedirectToAction("Login", "Auth");
        }

        public ActionResult SendNotification()
        {
            ContactMember cm = new ContactMember();
          string result =  cm.SendNotification(Convert.ToInt32(Session["ActiveMemberID"]),  Convert.ToInt32(Session["ActiveMemberID"]), Request["subject"], Request["message"]);
            TempData["ContactNotification"] = @"<div class='text_center center' style='color:white'> <strong >Message Sent</strong></div>";
          
           return RedirectToAction( "GetProfile/" + Session["ActiveMemberID"]);
        }

        public ActionResult EditProfile(int id)
        {

            ProfileModel pm = new ProfileModel();

            // Check if session is active
            bool valid = LoginModel.SessionValid();
            if (valid)
            {
                return View(pm.GetProfile(id));
            }
            return RedirectToAction("Login", "Auth");
        }

    }
}
