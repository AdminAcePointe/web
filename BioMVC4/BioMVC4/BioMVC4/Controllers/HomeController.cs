﻿using BioMVC4.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BioMVC4.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
           

            return View();
        }

        public ActionResult About()
        {
          

            return View();
        }

        public ActionResult Contact()
        {

            return View();
        }
        public ActionResult Referral()
        {
          
            // Check if session is active
            bool Valid = LoginModel.SessionValid();
            if (Valid == true)
            {
                return View();
            }

            return RedirectToAction("Login", "Auth");

          
        }
        public ActionResult ReferPerson()
        {
            string result;
            ContactMember cm = new ContactMember();


           result = cm.ReferPerson(Request["fname"], Request["lname"], Session["ActiveMemberID"].ToString(), Request["email"], Request["subject"], Request["message"]);
           TempData["ReferralMessageResult"] = @"<div class='alert alert-success'  id='contactSuccess' style='margin-top:10px;color:green;background-color:orange'> <strong>" + result + "</strong></div>";


           return RedirectToAction("Referral", "Home");
        }

        public ActionResult PrivacyPolicy()
        {
            return View();
        }
    }
}
