﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BioMVC4.Models;
using System.Data.Entity.Validation;

namespace BioMVC4.Controllers
{
    public class EditController : Controller
    {
        private static string _message;
        private readonly BioDataEntities _context;

        public EditController()
        {
            _context = new BioDataEntities();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        protected ActionResult ValidateSubmission(bool result, int memid)
        {
            if (result)
            {
                TempData["ProfileSaved"] = @"<h2 id='success' class='center' style='color:green'>" + _message + "</h2>";
            }
            else
            {
                TempData["ProfileSaved"] = @"<h2 id='success' class='center' style='color:red'>" + _message + "</h2>";
            }
            return RedirectToAction("EditProfile/" + memid, "Profile");
        }

        // GET: /Edit/
        public ActionResult Index()
        {
            return RedirectToAction("ValidateUser","Auth");
        }

        [HttpPost]
        public ActionResult SaveProfileCredentials(FormCollection fc, int id){
            bool result = EditProfile.UpdatePassword(fc["email"], fc["password"], fc["passwordnew"], fc["passwordnew2"], out _message);
            return ValidateSubmission(result, id);
        }

        [HttpPost]
        public ActionResult SaveProfilePersonal(FormCollection fc, int id){
            bool result = EditProfile.UpdatePersonalInfo(id, fc["firstname"], fc["lastname"], fc["dob"], fc["phone"], fc["city"], fc["state"], fc["country"], fc["zip"], out _message);
            return ValidateSubmission(result, id);
        }

        [HttpPost]
        public ActionResult SaveProfileBio(FormCollection fc, int id){
            bool result = EditProfile.UpdateBioInfo(id, fc["linkedin"], fc["facebook"], fc["twitter"],Request.Files["photo"], Request.Files["resume"], fc["description"], fc["tags"], out _message);
            return ValidateSubmission(result, id);
        }
        [HttpPost]
        public ActionResult SaveProfileExperience(FormCollection fc, int id){
            bool result = EditProfile.UpdateMemberExperience(fc, id, out _message);
            return ValidateSubmission(result, id);
        }
        [HttpPost]
        public ActionResult SaveProfileEducation(FormCollection fd, int id)
        {
            var result = EditProfile.UpdateMemberEducation(fd, id, out _message);
            return ValidateSubmission(result, id);
        }

        [HttpPost]
        public ActionResult SaveProfileCertification(FormCollection fc, int id)
        {
            bool result = EditProfile.UpdateMemberCertification(fc, id, out _message);
            return ValidateSubmission(result, id);
        }

        [HttpPost]
        public ActionResult SaveProfileAssociation(FormCollection fc, ProfileModel pm, int id)
        {
            bool result = EditProfile.UpdateMemberAssociation(fc, id, out _message);
            return ValidateSubmission(result, id);
        }

    }
}
