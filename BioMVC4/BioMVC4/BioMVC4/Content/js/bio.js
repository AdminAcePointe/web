﻿$(document).ready(function () {


   
  

    //$.validator.setDefaults({
    //    ignore: [],
    //    // any other default options and/or rules
    //});
  
    $(document).on('focus', ".expfrom", function () {
        $(this).datepicker({
        });
    });

    $(document).on('focus', ".expto", function () {
        $(this).datepicker({
        });
    });

    $(document).on('focus',"#dob", function () {
        $(this).val("");
        $(this).datetimepicker({
            viewMode: 'months',
            format: 'MM/DD'
        });
    });


    $(document).on('focus', ".gradyear", function () {
        $(this).datetimepicker({
            viewMode: 'months',
            format: 'YYYY'
        });
    });


    // Country
    $(document).on('focus', ".country", function () {
        $(this).autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: '/Auth/CountryAutoComplete/',
                    data: "{ 'term': '" + request.term + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        
                        response($.map(data, function (item) {
                            return item;
                        }))
                    },
                    error: function (response) {
                        alert(response.responseText);
                    },
                    failure: function (response) {
                        alert(response.responseText);
                    }
                });
            },
            minLength: 1
        });
    });


    //State
    $(document).on('focus', ".state", function () {
        var country = $(this).parents(".DivMaster").find(".country").attr("name");
        //alert(country);
        //alert("[name='" + country +"']")
        $(this).autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: '/Auth/StateAutoComplete/',
                    data: "{ 'term': '" + request.term + "','id': '" + $("[name='" + country +"']").val() + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        response($.map(data, function (item) {
                            return item;
                        }))
                    },
                    error: function (response) {
                        alert(response.responseText);
                    },
                    failure: function (response) {
                        alert(response.responseText);
                    }
                });
            },
            minLength: 1
        });
    });

    $(document).on('focus', ".major", function () {
        $(this).autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: '/Auth/EducationMajorAutoComplete/',
                    data: "{ 'term': '" + request.term + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        response($.map(data, function (item) {
                            return item;
                        }))
                    },
                    error: function (response) {
                        alert(response.responseText);
                    },
                    failure: function (response) {
                        alert(response.responseText);
                    }
                });
            },
            minLength: 1
        });
    });

    $(document).on('focus', ".degree", function () {
        $(this).autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: '/Auth/EducationDegreeAutoComplete/',
                    data: "{ 'term': '" + request.term + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        response($.map(data, function (item) {
                            return item;
                        }))
                    },
                    error: function (response) {
                        alert(response.responseText);
                    },
                    failure: function (response) {
                        alert(response.responseText);
                    }
                });
            },
            minLength: 1
        });
    });
  
    $(document).on('focus', ".institution", function () {
        $(this).autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: '/Auth/EducationInstitutionAutoComplete/',
                    data: "{ 'term': '" + request.term + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        response($.map(data, function (item) {
                            return item;
                        }))
                    },
                    error: function (response) {
                        alert(response.responseText);
                    },
                    failure: function (response) {
                        alert(response.responseText);
                    }
                });
            },
            minLength: 1
        });
    });
  
    $(document).on('focus', ".certification", function () {
        $(this).autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: '/Auth/CertificationAutoComplete/',
                    data: "{ 'term': '" + request.term + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        response($.map(data, function (item) {
                            return item;
                        }))
                    },
                    error: function (response) {
                        alert(response.responseText);
                    },
                    failure: function (response) {
                        alert(response.responseText);
                    }
                });
            },
            minLength: 1
        });
    });

    $(document).on('focus', ".association", function () {
        $(this).autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: '/Auth/AssociationAutoComplete/',
                    data: "{ 'term': '" + request.term + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        response($.map(data, function (item) {
                            return item;
                        }))
                    },
                    error: function (response) {
                        alert(response.responseText);
                    },
                    failure: function (response) {
                        alert(response.responseText);
                    }
                });
            },
            minLength: 1
        });
    });

    $(document).on('focus', ".associationrole", function () {
        $(this).autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: '/Auth/AssociationRoleAutoComplete/',
                    data: "{ 'term': '" + request.term + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        response($.map(data, function (item) {
                            return item;
                        }))
                    },
                    error: function (response) {
                        alert(response.responseText);
                    },
                    failure: function (response) {
                        alert(response.responseText);
                    }
                });
            },
            minLength: 1
        });
    });

    $(".cd-side-navigation ul li a").click(function () {
        $(this).addClass("selected");
        var id = $(this).attr("id");
        //alert(id);
        sessionStorage.setItem("navselected", null);
        sessionStorage.setItem("navselected", id)
        //alert(sessionStorage.getItem("navselected"));

    });

    $(".cd-side-navigation ul li a").each(function () {
        var currid = $(this).attr('id');
        var t = sessionStorage.getItem("navselected");
        //alert("Current id :" + currid);
        //alert("Cookie : " + t);

        if (currid === sessionStorage.getItem("navselected")) {
            $(this).addClass("selected");
            sessionStorage.setItem("navselected", null)
        }
        else {
            $(this).removeClass("selected");
        }


        
    });

    if ($(".Page").height() < $(window).height()) {
        $(".footer_wrapper").addClass("fixed");
    } else {
        $(".footer_wrapper").removeClass("fixed");
    }
 
 
   
 


    $("#tabbed-nav").zozoTabs({
        orientation:"vertical",
        position: "top-left",  
        theme: "silver",
        animation: {
            duration: 800,
            effects: "slideV"
        }
    })
 
    $('#signupform').prop("disabled", true);

    $('#RememberMe').change(function () {
        if ($('#RememberMe').is(':checked')) {
          
          
            setCookie();
        }

    });
     readcookie();
    //read cookie
    function readcookie() {
        if ($.cookie.read('username') !== null) {
            var t = $.cookie.read('username').length;
            if (t > 0) {
                $('#RememberMe').attr('checked', true);

                $("#form #username").val($.cookie.read('username'));
                $("#form #pwd").val($.cookie.read('pwd'));
            }
        }
    };

// Set cookie
    function setCookie() {
        if ($('#RememberMe').is(':checked')) {
            $.cookie.write('username', $("#form #username").val(), 24 * 60 * 60);
            $.cookie.write('pwd', $("#form #pwd").val(), 24 * 60 * 60);
        }
        else {
            $.cookie.destroy('username');
            $.cookie.destroy('pwd');
        }
    };
// Enable seearch on enter -- Still pending
    $('.searchbar').bind("enterKey", function (e) {
        var search = $(".searchbar").val();
        if (search === "") {
            $("#contactError").removeClass("hidden");
        } else {
            window.location.replace('/Search' + "/" + search);
        }
    });
    $('.searchbar').keyup(function (e) {
        if (e.keyCode == 13) {
            $(this).trigger("enterKey");
        }
    });


    
$(".searchbar").on("focus", function () {
    $("#contactError").addClass("hidden");
});

$("#member_login").click(function () {
    window.location.replace('/Auth/Login');
});

$("#login-form").click(function () {
    $("#form").validate({

        //specify the validation rules
        rules: {
            username: {
                required: true,
                email: true //email is required AND must be in the form of a valid email address
            },
            pwd: {
                required: true
             
            }
        },

        //specify validation error messages
        messages: {
            username: "Please enter a valid email address",
            pwd: {
                required: "Please enter your password."
            }
           
        }

    });
 
});

$("#profilecontactform").click(function () {
    $("#contactprofileform").validate({

        //specify the validation rules
        rules: {
            subject: {
                required: true
            },
            message: {
                required: true,
                minlength: 10

            }
        },

        //specify validation error messages
        messages: {
            subject: "Please enter subject",
            message: {
                required: "Please enter your message",
                minlength: "Please enter at least 10 characters"

            }

        }

    });

});

$("#conform").click(function () {
    $("#contactform").validate({

        //specify the validation rules
        rules: {
            name: {
                required: true
            },
            emailaddress: {
                required: true,
                email:true
            },
            subject: {
                required: true
            },
            message: {
                required: true,
                minlength: 10

            }
        },

        //specify validation error messages
        messages: {
            subject: "Please enter subject",
            message: {
                required: "Please enter your message",
                minlength: "Please enter at least 10 characters"

            }

        }

    });

});

$("#refform").click(function () {
    $("#referralform").validate({

        //specify the validation rules
        rules: {
            fname: {
                required: true
            },
            lname: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            subject: {
                required: true
            },
            message: {
                required: true,
                minlength: 10

            }
        },

        //specify validation error messages
        messages: {
            subject: "Please enter subject",
            message: {
                required: "Please enter your message",
                minlength: "Please enter at least 10 characters"

            }

        }

    });

});

$("#resetpassword-form").click(function () {
   
    $("#rform").validate({
       
        //specify the validation rules
        rules: {
            email: {
                required: true,
                email: true //email is required AND must be in the form of a valid email address
            }
        },

        //specify validation error messages
        messages: {
            email: "Please enter a valid email address"
           

        }

    });

});
$("#member_signup").click(function () {
    window.location.replace('/SignUp');
});
$("#signupabout").click(function () {
    window.location.replace('/SignUp');
});

$("#setpassword-form").click(function () {
    $("#sform").validate({

        //specify the validation rules
        rules: {
            
            email: {
                required: true,
                email: true //email is required AND must be in the form of a valid email address
            },
            temppwd: {
                required: true

            },
            newpwd: {
                required: true

            },
            newpwd2: {
                required: true,
                equalTo: "#newpwd"
             
            }
        },

        //specify validation error messages
        messages: {
            temppwd: "Please enter your temporary password"
            , newpwd: "Please enter your new password"
            , newpwd2: "Please re enter your new password"
            , email: "Please enter a valid email address"

        }

    });

});
$("#bs").click(function() {
    var search = $(".searchbar").val();
    if (search === "") {
        $("#contactError").removeClass("hidden");
    } else {
        window.location.replace('/Search' + "/" + search);
    }
});
$("#sb1").click(function() {
    var search = $("#searchbar").val();
 
    if (search === "") {
        $("#contactError").removeClass("hidden");
    }
       
    else
    {
        var url = window.location.href;
        var myString = url.substr(url.indexOf("Search/") + 7);
        var nurl = ('/Search' + "/" + search);
        var ns = nurl.replace("/" + myString, "");
        window.location.replace(ns);
        if ($(".Page").height() < $(window).height()) {
            $(".footer_wrapper").addClass("fixed");
        } else {
            $(".footer_wrapper").removeClass("fixed");
        }
    }
});      
$(".profile").css("cursor", "pointer");
$(".profile").on("click", function () {
    var $this = $(this);
    var plink =  $this.find('.memberprofileid').find('[href]').attr('href');
    window.location.replace('/GetProfile' + "/" + plink);


   
});

//////////////// SignUp
// Accordion
$("#signup-accordion").zozoAccordion({
    theme: "silver",
    active: 0,
    sectionSpacing: 4
});


$('#privacy').change(function () {
    if ($('#privacy').is(':checked')) {


        $('#signupform').prop("disabled", false);
    } else {
        $('#signupform').prop("disabled", true);
    }

});

$("#signupform").click(function () {
    $("#suform").removeAttr("novalidate");
        $("#suform").validate({

            //specify the validation rules
            rules: {
                email: {
                    required: true,
                    email: true 
                },
                password: {
                    required: true,
                    minlength:8

                },
                password2: {
                    required: true,
                    minlength:8,
                    equalTo: "#password"

                },
                firstname: {
            required: true,
            minlength: 1
                    },
                lastname: {
                        required: true,
                        minlength: 1
                },
                dob: {
                    required:true
                }

              
            },

            //specify validation error messages
            messages: {
                email: "Please enter a valid email address",
           
            }

        });
    


    
          
      
    });

$(".yuert h3").addClass("hidden");

$(".editbtn").click(function (e) {
    $(".editform").removeAttr("novalidate");
    var form = "#" + $(this).parents(".editform").attr("id");

    $(form).validate({
        //ignore: [],
        invalidHandler: function (event, validator) {
            // 'this' refers to the form
            var errors = validator.numberOfInvalids();
            if (errors) {
                $(".ptb-60 h3").removeClass("hidden");
            } else {
                $(".ptb-60 h3").addClass("hidden");
            }
        },
        //specify the validation rules
        rules: {
            email: {
                required: true,
                email: true //email is required AND must be in the form of a valid email address
            },
            password: {
                required: true

            },
            passwordnew2: {
                required: true,
                equalTo: "#passwordnew",
                minlength: 6
            },
            passwordnew: {
                required: true,
                minlength: 6

            },
            firstname: {
                required: true,
                minlength: 1
            },
            lastname: {
                required: true,
                minlength: 1
            },
            city: {
                required: true
            },
            state: {
                required: true
            },
            zip: {
                required: true
            },
            country: {
                required: true
            },
            description: {
                required: true,
                minlenght: 150
            },
            tags: {
                required: true
            },
            dob: {
                required: true

            },
            resume: {
                required: true
            },
            photo: {
                required: true

            }
        },

        //specify validation error messages
        messages: {
            password: "Please enter your new password",
            password2: "Please re enter your new password",
            email: "Please enter a valid email address",
            tags: "Please enter at least one tag",
            description: "Please enter a short description about yourself",
            zip: "Please enter your zip/Postal code"
        }


    });
    });

$(".z-link").click(function () {
    $('#success').remove();
    $('#valerror').addClass("hidden");
});
$('#contactSuccess').remove();


   
});






