/*
 Theme Name: Mr.Lancer cv/resume personal template  - script.js
 Author: Phoenixcoded
 Site URL   :   http://www.phoenixcoded.com
 Follow us  :   https://themeforest.net/user/phoenixcoded

 Version: 1.0
 */

$(document).ready(function(){

    /*=======Side-bar Menu Starts ======= */
    var $window = $(window);
    $window.resize(function resize() {
        if ($window.width() <= 1340) {
            $('body nav').css('left','-94px');
        }
        else{
            $('body nav').css('left','0');
        }
        $('#toggle_icon').on('click',function(){
            $(this).toggleClass('open');
        });
    }).trigger('resize');
    /*======= Side-bar Menu Ends ======= */

    /*Preloader Starts*/
    $(window).on('load',function(){
        $('#main_loader').fadeOut('slow');
    });
    /*Preloader Ends*/

});

/*======= Toggle Button Event js Starts ======= */
var sides = ["left", "top", "right", "bottom"];

// Initialize sidebars
for (var i = 0; i < sides.length; ++i) {
    var cSide = sides[i];
    $(".sidebar." + cSide).sidebar({side: cSide});
}

// Click handlers
$("#sidebar-btn").on("click", function () {
    var $this = $(this);
    var action = $this.attr("data-action");
    var side = $this.attr("data-side");
    $(".sidebar." + side).trigger("sidebar:" + action);
    return false;
});

