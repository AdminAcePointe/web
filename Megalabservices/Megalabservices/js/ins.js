﻿
var gInsuranceObject = new Object();
var gRowData = [];
var gDisabledStates = ['OH', 'ND', 'TN', 'ID', 'NC', 'WA', 'HI', 'WY', 'OK'];
var gStateData = [];

// these are all related to the tooltip
var gTimer;
var gXCursor = 0;
var gYCursor = 0;
var gCurState;
var gCurAbbrevState;

//**************************************************************************
//
//**************************************************************************

function buildMandateLink(rowData, i) {
    var link = '';
    if (!isEmpty(rowData[i]["BeforeText"]))
        link = rowData[i]["BeforeText"] + ' ';
    link += '<a target="_blank" href="' + rowData[i]["Link"];
    link += '">' + rowData[i]["SourceText"];
    link += '</a>';
    if (!isEmpty(rowData[i]["AfterText"]))
        link += ' ' + rowData[i]["AfterText"];
    return link;
}

//**************************************************************************
//
//**************************************************************************

function populateMandateObject(rowData) {
    var state = rowData[0]["State"];
    gInsuranceObject.State = state;
    gInsuranceObject.Mandate = rowData[0]["Mandate"];
    gInsuranceObject.Links = '';
    gInsuranceObject.ContactNumbers = '';
    var holdSource = rowData[0]["SourceText"];
    for (var i = 0; i < rowData.length; i++) {
        if (i != 0 && rowData[i]["SourceText"] != rowData[i - 1]["SourceText"]) {
            gInsuranceObject.Links += "<br>";
            gInsuranceObject.Links += buildMandateLink(rowData, i);
        }
        else
            if (i == 0) {
                gInsuranceObject.Links = "<b>Source</b><br>";
                gInsuranceObject.Links += buildMandateLink(rowData, i);
            }
    }
}

//**************************************************************************
//
//**************************************************************************

function buildMandateParameters(state) {
    var qData = new Object();
    qData.State = state;
    var jsonData = JSON.stringify(qData);
    return jsonData;
}

//**************************************************************************
//
//**************************************************************************

function doMandate(state) {
  
    var jsonData;
    jsonData = buildMandateParameters(state);
    var jqxhr = $.post(queryConst.QUERY_FILE, { queryType: -1, queryID: queryConst.QUERY_MANDATES, queryData: jsonData }, function (data) {
    }, "json")
	.done(function (goodData) {
	  
	    gRowData = goodData;
	    populateMandateObject(goodData);
	    $('#insuranceModal').modal('show');
	})
  	.fail(function (badData) {
  	    var errorText = badData["responseText"];
  	    var status = badData["status"];
  	    var statusText = badData["statusText"];
  	    console.log(status + ': ' + statusText);
  	    var msg = 'Server Responded: ' + errorText;
  	   
  	});
}

//**************************************************************************
// Must have states go alphabetically in each column
//**************************************************************************

function writeStateRow(rowData, i, increment) {
    var str = "<div class='row'>";
    str += "<div class='col-xs-6 pointer'>";
    str += "<a onclick=\"doMandate('";
    str += rowData[i]["AbbrevState"];
    str += "')\">";
    str += rowData[i]["State"];
    str += "</a></div>";
    var j = i + increment;
    str += "<div class='col-xs-6 pointer'>";
    if (rowData[j]) {
        str += "<a onclick=\"doMandate('";
        str += rowData[j]["AbbrevState"];
        str += "')\">";
        str += rowData[j]["State"];
        str += "</a>";
    }
    str += "</div></div>";
    return str;
}

//**************************************************************************
//
//**************************************************************************

function populateStates(rowData) {
    var numStates = rowData.length;
    numStates += numStates % 2;  // make the number even
    var half = numStates / 2;
    var htmlStr = "";
    for (var i = 0; i < half; i++) {
        htmlStr += writeStateRow(rowData, i, half);
    }
    htmlStr += "<br>";
    $('#usmapText').html(htmlStr);
}

//**************************************************************************
//
//**************************************************************************

function populateTextDisplay() {
   
    var jqxhr = $.post(queryConst.QUERY_FILE, { queryType: -1, queryID: queryConst.QUERY_STATESWITHMANDATES }, function (data) {
    }, "json")
	.done(function (goodData) {
	    gTextData = goodData;
	    populateStates(goodData);
	   
	})
  	.fail(function (badData) {
  	    var errorText = badData["responseText"];
  	    var status = badData["status"];
  	    var statusText = badData["statusText"];
  	    console.log(status + ': ' + statusText);
  	    var msg = 'Server Responded: ' + errorText;
  	  
  	});
}


//**************************************************************************
// Show tooltip when mouse has stopped. Used globals as you cannot reliably
// pass parameters to a function used in a timer. It actually worked but
// then it wasn't taking the delay time.
//**************************************************************************  

function mouseStopped() {   // the actual function that is called
    clearTimeout(gTimer);
    if (gXCursor == 0 || gYCursor == 0)
        return;
    // don't show tooltip for states that have no info
    if (contains(gDisabledStates, gCurAbbrevState))
        return;
    $('<p class="tooltip"></p>').text(gCurState).appendTo('body').fadeIn(500);
    $('.tooltip').css({ top: gYCursor, left: gXCursor });
}

//**************************************************************************
// clearTooltip - gets rid of the state title tooltip
//**************************************************************************  

function clearTooltip() {
    $('.tooltip').remove();
    clearTimeout(gTimer);
}


//**************************************************************************
// We have three views of this page depending on the screen size; 
// Big map, small map and text display of states in two columns, 
// collapsing to one.
//**************************************************************************  

$(document).ready(function () {
    populateTextDisplay();
    $('#usmap').usmap({
        'stateSpecificLabelTextStyles': {
            'OH': { fill: '#808080' },
            'ND': { fill: '#808080' },
            'TN': { fill: '#808080' },
            'ID': { fill: '#808080' },
            'NC': { fill: '#808080' },
            'WA': { fill: '#808080' },
            'HI': { fill: '#808080' },
            'WY': { fill: '#808080' },
            'OK': { fill: '#808080' },
        },
        'stateSpecificHoverStyles': {
            'OH': { fill: '#404040', 'cursor': 'auto' },
            'ND': { fill: '#404040', 'cursor': 'auto' },
            'TN': { fill: '#404040', 'cursor': 'auto' },
            'ID': { fill: '#404040', 'cursor': 'auto' },
            'NC': { fill: '#404040', 'cursor': 'auto' },
            'WA': { fill: '#404040', 'cursor': 'auto' },
            'HI': { fill: '#404040', 'cursor': 'auto' },
            'WY': { fill: '#404040', 'cursor': 'auto' },
            'OK': { fill: '#404040', 'cursor': 'auto' }
        },
        'stateSpecificStyles': {
            'OH': { fill: '#404040', 'cursor': 'auto' },
            'ND': { fill: '#404040', 'cursor': 'auto' },
            'TN': { fill: '#404040', 'cursor': 'auto' },
            'ID': { fill: '#404040', 'cursor': 'auto' },
            'NC': { fill: '#404040', 'cursor': 'auto' },
            'WA': { fill: '#404040', 'cursor': 'auto' },
            'HI': { fill: '#404040', 'cursor': 'auto' },
            'WY': { fill: '#404040', 'cursor': 'auto' },
            'OK': { fill: '#404040', 'cursor': 'auto' }
        },
        'stateStyles': { fill: 'red' },
        'stateHoverStyles': { fill: 'blue' },
        'mouseover': function (event, data) {
            var state = data.name;
            // show tooltip when mouse has stopped.
            clearTooltip(gTimer);
            var stateName = getStateName(state);
            gCurState = stateName;
            gCurAbbrevState = state;
            $('#usmap').mousemove(function (e) {
                gXCursor = e.pageX - 10;
                gYCursor = e.pageY - 30;
            });
            // pause a bit so that cursor can catch up
            gTimer = setTimeout(mouseStopped, 800);
        },
        'mouseout': function (event, data) {
            clearTooltip();
        },

        'click': function (event, data) {
            doMandate(data.name);
        }
    });

    $('#usmap2').usmap({
        'stateSpecificLabelTextStyles': {
            'OH': { fill: '#808080' },
            'ND': { fill: '#808080' },
            'TN': { fill: '#808080' },
            'ID': { fill: '#808080' },
            'NC': { fill: '#808080' },
            'WA': { fill: '#808080' },
            'HI': { fill: '#808080' },
            'WY': { fill: '#808080' },
            'OK': { fill: '#808080' },
        },
        'stateSpecificHoverStyles': {
            'OH': { fill: '#404040', 'cursor': 'auto' },
            'ND': { fill: '#404040', 'cursor': 'auto' },
            'TN': { fill: '#404040', 'cursor': 'auto' },
            'ID': { fill: '#404040', 'cursor': 'auto' },
            'NC': { fill: '#404040', 'cursor': 'auto' },
            'WA': { fill: '#404040', 'cursor': 'auto' },
            'HI': { fill: '#404040', 'cursor': 'auto' },
            'WY': { fill: '#404040', 'cursor': 'auto' },
            'OK': { fill: '#404040', 'cursor': 'auto' }
        },
        'stateSpecificStyles': {
            'OH': { fill: '#404040', 'cursor': 'auto' },
            'ND': { fill: '#404040', 'cursor': 'auto' },
            'TN': { fill: '#404040', 'cursor': 'auto' },
            'ID': { fill: '#404040', 'cursor': 'auto' },
            'NC': { fill: '#404040', 'cursor': 'auto' },
            'WA': { fill: '#404040', 'cursor': 'auto' },
            'HI': { fill: '#404040', 'cursor': 'auto' },
            'WY': { fill: '#404040', 'cursor': 'auto' },
            'OK': { fill: '#404040', 'cursor': 'auto' }
        },
        'stateStyles': { fill: 'red' },
        'stateHoverStyles': { fill: 'blue' },

        'mouseover': function (event, data) {
            var state = data.name;
            // show tooltip when mouse has stopped.
            clearTooltip(gTimer);
            var stateName = getStateName(state);
            gCurState = stateName;
            gCurAbbrevState = state;
            $('#usmap2').mousemove(function (e) {
                gXCursor = e.pageX - 10;
                gYCursor = e.pageY - 30;
            });
            gTimer = setTimeout(mouseStopped, 800);
        },
        'mouseout': function (event, data) {
            clearTooltip();
        },

        'click': function (event, data) {
            doMandate(data.name);
        }
    });

    //**************************************************************************
    // This is fired when the Mandates Modal Dialog is called.
    // It populates the data after we have retrieved it from the database.
    // Modal should only be fired after a successful query.
    // NOTE: For this to work it must be inside of the on.ready function.
    //**************************************************************************

    $('#insuranceModal').on('show.bs.modal', function (e) {
        var stateImg = '<img class="img-responsive center-block" src="./images/states/';
        if (!gInsuranceObject)
            return;
        var state = gInsuranceObject.State;
        if (!state)
            return;
        state = removeWhitespace(state.toLowerCase());
        stateImg += state;
        stateImg += '.png">	';
        $(e.currentTarget).find('#spState').html(gInsuranceObject.State);
        $(e.currentTarget).find('#spMandateText').html(gInsuranceObject.Mandate);
        $(e.currentTarget).find('#spLinks').html(gInsuranceObject.Links);
        $(e.currentTarget).find('#spContacts').html(gInsuranceObject.ContactNumbers);
        $(e.currentTarget).find('#spStateImg').html(stateImg);
    });
});



