



jQuery(document).ready(function ($) {


    // disable all dropdowns except location
    $('#testcat').prop('disabled', 'disabled');
    $('#test').prop('disabled', 'disabled');
    $('#appointmenttime').prop('disabled', 'disabled');
    

    function getBranches() {
        var branchUrl = 'Appointment.aspx/getBranchOffices'

        $.ajax({
            type: "POST",
            url: branchUrl,
            dataType: "json",
            contentType: 'application/json; charset=utf-8',
            success: function (result) {
                $("#location").empty();
                $("#location").append("<option value='0'>--Select--</option>");
                $.each(result.d, function (key, value) {
                    $("#location").append($("<option></option>").val(value.branchID).text(value.branch));
                });
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(thrownError);
                alert("Error retrieving list of locations. Please try again.")
            }

        });
    }

    function getTestCategoryByBranchId(selectedBranchId) {
        var getTestCategoryByBranchIdUrl = 'Appointment.aspx/getTestCategoryByBranchId'
        $.ajax({
            type: "POST",
            url: getTestCategoryByBranchIdUrl,
            data: JSON.stringify({ branchId: selectedBranchId }),
            dataType: "json",
            contentType: 'application/json; charset=utf-8',
            success: function (result) {
                $("#testcat").empty();
                $("#testcat").append("<option value='0'>--Select--</option>");
                $.each(result.d, function (key, value) {
                    $("#testcat").append($("<option></option>").val(value.TestCategoryID).html(value.TestCategory));
                });
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(thrownError);
                alert("Error retrieving list of Test categories. Please try again.")
            }

        });

    }

    function getTestByCategoryId(selectedTestCategoryId) {
        var getTestByCategoryIdUrl = 'Appointment.aspx/getTestByCategoryId'
        $.ajax({
            type: "POST",
            url: getTestByCategoryIdUrl,
            data: JSON.stringify({ CategoryId: selectedTestCategoryId }),
            dataType: "json",
            contentType: 'application/json; charset=utf-8',
            success: function (result) {
                $("#test").empty();
                $("#test").append("<option value='0'>--Select--</option>");
                $.each(result.d, function (key, value) {
                    $("#test").append($("<option></option>").val(value.TestID).text(value.test));
                });
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(thrownError);
                alert("Error retrieving list of test. Please try again.")
            }

        });

    }

    function getAvailableTimeByLocationDate(selectedBranchId, selectedDate) {
        var getAvailableTimeByLocationDateUrl = 'Appointment.aspx/getAvailableTimeByLocationDate'
        $.ajax({
            type: "POST",
            url: getAvailableTimeByLocationDateUrl,
            data: JSON.stringify({ BranchId: selectedBranchId, AppointmentDate: selectedDate}),
            dataType: "json",
            contentType: 'application/json; charset=utf-8',
            success: function (result) {
                $("#appointmenttime").empty();
                $("#appointmenttime").append("<option value='0'>--Select--</option>");
                $.each(result.d, function (key, value) {
                    $("#appointmenttime").append($("<option></option>").val(value.ApptTimeID).text(value.ApptTime));
                });
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(thrownError);
                alert("Error retrieving list of available times. Please try again.")
            }

        });

    }
    
    $('#newsletterForm').removeAttr("novalidate");

    $(function () {
        $('#appointmentdate').datetimepicker(
            {
                format: 'M/DD/YYYY',
                daysOfWeekDisabled: [0],
                useCurrent: true,
                minDate: moment()

            });
    });

    $(function () {
        $('#ttdate').datetimepicker(
            {
                format: 'M/DD/YYYY',
                daysOfWeekDisabled: [0],
                useCurrent: true

            });
    });

    getBranches();



    // Branch change
    $('#location').change(function () {


        var selectedBranchId = ($('#location :selected').val()); 
        getTestCategoryByBranchId(selectedBranchId);


        // enable only when value <> 0
        if (selectedBranchId != 0) {
            $('#testcat').prop('disabled', false);
            $('#appointmenttime').prop('disabled', false);
            var value = $('#appointmentdate').data('date');
            var selectedBranchId = ($('#location :selected').val());
            var selectedDate = value
            getAvailableTimeByLocationDate(selectedBranchId, selectedDate)
        }
        else {
            $('#testcat').prop('disabled', 'disabled'); 
            $('#appointmenttime').prop('disabled', 'disabled'); 
        }
    
    });
    
   // Category change
    $('#testcat').change(function () {
        var selectedTestCategoryId = ($('#testcat :selected').val()); 
        getTestByCategoryId(selectedTestCategoryId) 

        // enable only when value <> 0
        if (selectedTestCategoryId != 0) {
            $('#test').prop('disabled', false);
        }
        else {
            $('#test').prop('disabled', 'disabled');
        }
    });

    // Appointment date change
    $("#appointmentdate").on("dp.change", function (e) {

     
        var time = moment().format('hh:mm: a');
        var value = $('#appointmentdate').data('date');
        var selectedBranchId = ($('#location :selected').val());
        var selectedDate = value

        if (selectedBranchId > 0) {
            getAvailableTimeByLocationDate(selectedBranchId, selectedDate)
            $('#appointmenttime').prop('disabled', false);
        }
        else {
            $('#appointmenttime').prop('disabled', 'disabled');
        }

    
    });

   
    // submit contact form
    $('#submitcontact').click(function () { })
    var Contact = {
        initialized: false,
        initialize: function () {

            if (this.initialized) return;
            this.initialized = true;

            this.build();
            this.events();

        },
        build: function () {

            this.validations();

        },
        events: function () {



        },
        validations: function () {
            $("#contactForm").validate({
                submitHandler: function (form) {
                    var submitButton = $(this.submitButton);
                    var sendurl = null
                    a = {};


                    sendurl = 'Contact.aspx/sendmail'
                    a.fname = $("#contactForm #fname").val()
                    a.lname = $("#contactForm #lname").val()
                    a.email = $("#contactForm #email").val()
                    a.phone = $('#contactForm #phone').val()
                    a.message = $('#contactForm #message').val();
                  



                    $.ajax({
                        type: "POST",
                        url: sendurl,
                        data: JSON.stringify(a),
                        dataType: "json",
                        contentType: 'application/json; charset=utf-8',
                        success: function (msg) {
                            var res = JSON.parse(msg.d);
                            if (res > 0) {

                                $("#contactSuccess").removeClass("hidden");
                                $("#contactError").addClass("hidden");
                            } else {

                                $("#contactError").removeClass("hidden");
                                $("#contactSuccess").addClass("hidden");
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert(xhr.status);
                            alert(thrownError);
                        }
                                , complete: function () {
                                    $('#fname').val("")
                                    $('#lname').val("")
                                    $('#phone').val("")
                                    $('#email').val("")
                                    $('#contactForm textarea').val("")


                                }
                    });
                },
                rules: {
                    fname: {
                        required: true
                    },
                    lname: {
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    message: {
                        required: true
                    }

                }
            });
        }
    };
    Contact.initialize();
        // submit appointment
    $('#submitappt').click(function () {

    })
    var Appointment = {
        initialized: false,
        initialize: function () {

            if (this.initialized) return;
            this.initialized = true;

            this.build();
            this.events();

        },
        build: function () {

            this.validations();

        },
        events: function () {



        },
        validations: function () {
            $("#ApptForm").validate({
                submitHandler: function (form) {
                    var submitButton = $(this.submitButton);
                    var sendurl = null
                    a = {};
    

                    sendurl = 'Appointment.aspx/saveapt'



                    a.fname = $("#ApptForm #fname").val()
                    a.lname = $("#ApptForm #lname").val()
                    a.email = $("#ApptForm #email").val()
                    a.phone = $('#ApptForm #phone').val()
                    a.location = $('#ApptForm #location :selected').val()
                    a.branch = $('#ApptForm #location :selected').text()
                    a.testcategory = $("#ApptForm #testcat option:selected").text();
                    a.test = $("#ApptForm #test option:selected").text();
                    a.apptdate = $("#ApptForm #appointmentdate").data('date');
                    a.appttime = $("#ApptForm #appointmenttime option:selected").text();
                    a.message = $("#ApptForm #message").val();




                    $.ajax({
                        type: "POST",
                        url: sendurl,
                        data: JSON.stringify(a),
                        dataType: "json",
                        contentType: 'application/json; charset=utf-8',
                        success: function (msg) {
                            var res = JSON.parse(msg.d);
                            if (res > 0) {

                                $("#contactSuccess").removeClass("hidden");
                                $("#contactError").addClass("hidden");
                     
                            } else {

                                $("#contactError").removeClass("hidden");
                                $("#contactSuccess").addClass("hidden");
                 
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert(xhr.status);
                            alert(thrownError);
                            alert("nah");
                        }
                                , complete: function () {
                                    var d2 = new (Date);
                                    $('#fname').val("")
                                    $('#lname').val("")
                                    $('#phone').val("")
                                    $('#email').val("")
                                    $('#ApptForm textarea').val("")
                                    $("#testcat").val("")
                                    $("#test").val("")
                                    $("#aptdate").val((d2.getMonth() + 1) + '/' + d2.getDate() + '/' + d2.getFullYear())
                                    $("#aptime").val("")
                                    $('#test').prop('disabled', 'disabled');
                                    $('#appointmenttime').prop('disabled', 'disabled');


                                }
                    });
                },
                rules: {
                    fname: {
                        required: true
                    },
                    lname: {
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    message: {
                        required: false
                    },
                    aptdate: {
                        required: true, date: true
                    },
                    aptime: {
                        required: true
                    }
                    ,
                    testcat: {
                        required: true
                    },
                    location: {
                        required: true
                    }
                    ,
                    test: {
                        required: true
                    },
                    phone: {
                        required: true
                    }


                }
            });
        }
    };
    Appointment.initialize();
    // submit newsletter
    $('#submitnewsletter').click(function () {

        var submitButton = $(this.submitButton);
        var sendurl = null
        a = {};


        sendurl = 'Locations.aspx/savenewsletter'
        a.email = $("#newsletterForm #newsletterEmail").val()

    
        $.ajax({
            type: "POST",
            url: sendurl,
            data: JSON.stringify(a),
            dataType: "json",
            contentType: 'application/json; charset=utf-8',
            success: function (msg) {
                var res = JSON.parse(msg.d);
                if (res > 0) {

                    $("#newsletterSuccess").removeClass("hidden");
                    $("#newsletterError").addClass("hidden");
                } else {

                    $("#newsletterError").removeClass("hidden");
                    $("#newsletterSuccess").addClass("hidden");
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
                               , complete: function () {

                                   $('#newsletterForm #newsletterEmail').val("")



                               }
        });
    })

    //

    var cred = 0
    
    $('input:checkbox[name="cred"]').change(function () {

        cred = $('input[name=cred]:checked', '#uForm').val();

    });

    var repcred = 0
    $('input:checkbox[name="repcred"]').change(function () {

        repcred = $('input[name=repcred]:checked', '#uForm').val();

    });

    $('#submitupload').click(function () { })
    var upload = {
        initialized: false,
        initialize: function () {

            if (this.initialized) return;
            this.initialized = true;

            this.build();
            this.events();

        },
        build: function () {

            this.validations();

        },
        events: function () {



        },
        validations: function () {
            $("#uForm").validate({
                submitHandler: function (form) {
                    var submitButton = $(this.submitButton);
                    var sendurl = null
                    a = {};


                    sendurl = 'fileupload.aspx/insertTest'

                    

                    a.accountID = sessionStorage.getItem("accountid");
                    a.testcategory = $("#uForm #testcat option:selected").val();
                    a.test = $("#uForm #test option:selected").val();
                    a.result = $("#uForm #Result option:selected").text();
                    a.testdate = $("#uForm #ttdate").data('date');
                    a.ccffilename = $("#ccffilename").val()
                    a.testfilename = $("#testfilename").val()
                    a.notify = cred
                    a.notifyrep = repcred





                    $.ajax({
                        type: "POST",
                        url: sendurl,
                        data: JSON.stringify(a),
                        dataType: "json",
                        contentType: 'application/json; charset=utf-8',
                        success: function (msg) {
                            var res = JSON.parse(msg.d);
                            if (res > 0) {

                                $("#contactSuccess").removeClass("hidden");
                                $("#contactError").addClass("hidden");
                            } else {

                                $("#contactError").removeClass("hidden");
                                $("#contactSuccess").addClass("hidden");
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert(xhr.status);
                            alert(thrownError);
                        }
                                , complete: function () {
                                    var d2 = new (Date);
                                 
                                    $('#Result').val("")
                                    $("#testcat").val("")
                                    $("#test").val("")
                                    $("#aptdate").val((d2.getMonth() + 1) + '/' + d2.getDate() + '/' + d2.getFullYear())
                                  
                                    $('#test').prop('disabled', 'disabled');
                                    $("#ccffilename").val("")
                                   $("#testfilename").val("")


                                }
                    });
                },
                rules: {
                   
                    Result: {
                        required: true
                    },
                    aptdate: {
                        required: true, date: true
                    },
                    aptime: {
                        required: true
                    }
                    ,
                    testcat: {
                        required: true
                    }
                    ,
                    test: {
                        required: true
                    }


                }
            });
        }
    };
    upload.initialize();



});
