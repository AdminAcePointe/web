﻿
    $(document).ready(function () {
       
        $('#content').css('display', "none");
 
        $(window).load(function () {
            $('#usaMapImg').mapster({
                fillOpacity: 0.25,
                render_highlight: {
                    fillColor: 'AD607C',
                    stroke: true,
                    strokeWidth: 1,
                    strokeColor: '7e0632'
                },
                fadeInterval: 500,
                mapKey: 'alt',
                onClick: function (e) {
                    $('#content').css('display', "block");
                    $('#clicked-state')
                                  .text('Network Collection Centers in ' + e.key);
                    $('#clicked-states')
                                 .text('To schedule an appointment at any of these locations, please call (866) 261-7129');
                    $('#clicked-states3')
                                .text('For cities not listed above or to find a location nearest you, please call (866) 261-7129');
                                 var sc = "." + e.key;
                                $('.tog').removeClass("show").addClass("hidden");
                                $("." + e.key).addClass("show").removeClass("hidden");
                                $('html, body').animate({ scrollTop: $("." + e.key).offset().top-250}, 500);
                 
                }
            });
            var container = document.getElementById("us-map"),
              containerSize = container.offsetWidth;

            $('#usaMapImg').mapster('resize', containerSize);

            container.style.opacity = "1";

            $(window).resize(function () {
                containerSize = document.getElementById("us-map").offsetWidth;
                $('#usaMapImg').mapster('resize', containerSize);
            })
        });

     
  

       
      
      
    
    });


