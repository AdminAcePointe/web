﻿<%@ Page Title="" Language="C#" MasterPageFile="megalabs.master" AutoEventWireup="true" CodeFile="FAQ.aspx.cs" Inherits="FAQ" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
       <title>Frequently Asked Questions | Mega Lab Services</title>	
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="container">

					<h2>Frequently Asked <strong>Questions</strong></h2>

					<div class="row">
						<div class="col-md-12">
							<p class="row mb-xl">
								Below are some of our frequently asked questions.
							</p>
						</div>
					</div>

					<hr>

					<div class="row">
						<div class="col-md-12">

							<div class="toggle toggle-primary" data-plugin-toggle>
								<section class="toggle active">
									<label>At what age should I sit down with my children to discuss the dangers of drugs?</label>
									<p>It’s never too early to share how you feel about drugs and what your expectations are in terms of zero tolerance of drug use by your children. A study showed that 74% of 4th graders wished their parents would speak with them about drugs..</p>
								</section>

								<section class="toggle">
									<label>At what age should I begin randomly drug testing my child??</label>
									<p>Every family situation is different. Some children are experimenting with drugs as young as 8 and 9 years old. We suggest that a program of random drug testing is implemented by the age of 12.</p>
								</section>

								<section class="toggle">
									<label>What should I do if my child tests positive for drugs or alcohol?</label>
									<p>First of all, stay calm. You are in control and now have the proof that your child is using drugs. Our program manual teaches you how to respond to a positive test.
								We’re with you every step of the way. Should your child need counseling or treatment, we have the resources to help you take the next steps.</p>
								</section>

                                
								<section class="toggle">
									<label>Is there a way to get a sample without my child’s knowledge?</label>
									<p>Some children may refuse to comply with taking a drug test. We address this issue in our program manual. As a last resort, we do offer parents a method to test their child without the child’s knowledge.</p>
								</section>

								<section class="toggle">
									<label>In a multi-drug test, how will I know which drug tests positive?</label>
									<p>Each panel shows the positive or negative result for a particular drug or drug category. So you know at a glance which drug you have tested positive for if there is one red line on the test strip.</p>
								</section>

								<section class="toggle">
									<label>How do you perform you drug detection assesments?</label>
									<p>  Our Drug Detection Specialists (D-Techs) are trained to use specially designed “wipe and spray” technology that allows us to detect drug residue on all types of surfaces. When an employee has handled illegal drugs, traces can be left on surfaces in your work environment by their fingers and hands.
<br />
Step 1: We follow a 25 point check system where we test the most likely surfaces where drug residues can be found in your work environment: counters, keyboards, machinery, registers, vehicles and more.
<br />
Step 2: We assess the presence and type of drugs detected and help you zero in on the locations and likely sources.</p>
								</section>

								<section class="toggle">
									<label>What are synthetic cannabinoids?</label>
									<p>Synthetic Cannabinoids, a class of designer drugs, are available in head shops, tobacco stores, and over the Internet under a variety of names, such as, “K2”, “Spice”, “Tucatan Fire”, “Skunk”, “Moon Rocks”, “Black Mamba”, and “Bombay Blue”. They are often labeled “not for human consumption” and common routes of administration include inhalation and oral ingestion.

When synthetic cannabis blends first went on sale in the early 2000s, it was thought that they achieved an effect through a mixture of natural herbs. Laboratory analysis in 2008 showed that this was not the case, and that they in fact contain synthetic cannabinoids that act on the body in a similar way to cannabinoids naturally found in cannabis, such as THC. A large and complex variety of synthetic cannabinoids, most often cannabicyclohexanol, JWH-018, JWH-073, or HU-210, are used in an attempt to avoid the laws that make cannabis illegal, making synthetic cannabis a designer drug. It has been sold under various brand names, online, in head shops, and other stores.

It is often marketed as “herbal incense“; however, some brands market their products as “herbal smoking blends”. In either case, the products are usually smoked by users. Although synthetic cannabis does not produce positive results in drug tests for cannabis, it is possible to detect its metabolites in human urine. The synthetic cannabinoids contained in synthetic cannabis products have been made illegal in many European countries. Regular users of Synthetic Cannabinoids may experience withdrawal and addiction symptoms.2 Tolerance to these agents may develop rapidly, which might be associated with dependence.3 The compounds are stored in the body for a long period of time and long-term effects on humans are not known.

In 2010, the American Association of Poison Control Centers received 2,906 calls related to synthetic cannabinoids. In 2011, the number of calls received increased to 6,959.4 On July 9, 2012, President Obama signed into law the Synthetic Drug Abuse Prevention Act banning synthetic compounds commonly found in synthetic marijuana, by placing them under Schedule I of the Controlled Substance Act..</p>
								</section>

								<section class="toggle">
									<label>Drug Names, Abbreviations, Cut-off Levels & Windows of Detection Table?</label>
									<p></p>
								</section>

								<section class="toggle">
									<label> What are Drug Test Panels</label>
									<p>A particular drug panel indicates the number and types of drugs tested.

5 Panel:   THC/COC/OPI/AMP/PC

6 Panel:  THC/COC/OPI/AMP/MET/BZO

8 Panel:   AMP/BUP/BZO/THC/MOP/MTD/OXY/COC

10 Panel:  COC/THC/OPI/AMP/MET/PCP/BZO/BAR/MTD/MDMA

12 Panel:  COC/MET/AMP/THC/MOP/OXY/MTD/BAR/BZO/MDMA/PPX/BUP</p>
								</section>
							</div>

						</div>

					</div>

				</div>
</asp:Content>

