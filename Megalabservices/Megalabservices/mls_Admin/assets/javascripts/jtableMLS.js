﻿
     
    
     
$(document).ready(function () {
    var cachedCityOptions = null;

    $('#StudentTableContainer').jtable({
        title: 'Company List',
        paging: false,
        pageSize: 10,
        sorting: true,
        multiSorting: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/TestResult.aspx/CompanyList'
        },
        fields: {
            CompanyId: {
                key: true,
                create: false,
                edit: false,
                list: false
            },
            Name: {
                title: 'Name',
                width: '23%'
            }
        }
    });
    //Load student list from server
    $('#StudentTableContainer').jtable('load');
});
