/* Add here all your JS customizations */


$(document).ready(function () {

    sessionStorage.removeItem("rid")
  
    var pathname = window.location.pathname; 
    var url = window.location.href;

    sessionStorage.setItem("curl",url)

    $('#profileName').text(sessionStorage.getItem("FullName"))

    $(function () {
        $('#testreportedto1').datetimepicker(
            {
                format: 'M/DD/YYYY',
                daysOfWeekDisabled: [0],
                useCurrent: false

            });
    });

    $(function () {
        $('#testreportedfrom1').datetimepicker(
            {
                format: 'M/DD/YYYY',
                daysOfWeekDisabled: [0],
                useCurrent: false

            });
    });



    $('#Search').click(function () {
     
        var sendurl = null
        var a = {};
        sendurl = 'TestResultConfig.aspx/searchcustomer'
        a.fname = $("#fname").val()
        a.lname = $("#lname").val()
        a.ssn = $("#ssn").val()
        $.ajax({
            type: "POST",
            url: sendurl,
            data: JSON.stringify(a),
            dataType: "json",
            contentType: 'application/json; charset=utf-8',
            success: function (msg) {
                var res = JSON.parse(msg.d);
                $("#sr").html("")
                
                $.each($.parseJSON(msg.d), function () {
                  
                    var test = "<tr class=\"gradeC\"><td id= \"accountid\" class=\"hidden\">" + this.AccountID + "</td><td> <div class=\"cbp0\" style=\"margin:0\"> <label> <input type=\"checkbox\" value=\"\"> </label> </div></td><td>" + this.LastName + "</td><td>" + this.FirstName + "</td><td class=\"hidden-phone\">" + this.SSN + "</td><td><div><a ID=\"uploadx\" class=\"btn btn-small btn-primary\" href=\"fileupload.aspx#" + this.AccountID + "\">Upload</a></div></td></tr>"
               
                    $("#sr").append(test)
                  
                   
                })
              
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
            

        });

        $('#Results').css("display", "inline-block")
        id = "datatable-tabletools"
        $('#datatable-tabletools').css("width", "100%")
    });

    $('#empsearchback').click(function () {
     
        window.location.href = "EmployeeSearch.aspx";

    
   
    })

    //
    $('#companyempsearch').click(function () {

      
        $('#companyempsearch').hide();
        $("#loader").removeClass("hidden");
        var sendurl = null
        var a = {};
        sendurl = 'EmployeeSearch.aspx/searchemployee'

        a.username = sessionStorage.getItem("username")
        a.firstname = $("#fname").val()
        a.lastname = $("#lname").val()
      
        a.testdatefrom = $("#testreportedfrom").val()
        a.testdateto = $("#testreportedto").val()
        a.locationname = $("#location").val()
        a.sitecode = $("#sitecode").val()
        $.ajax({
            type: "POST",
            url: sendurl,
            data: JSON.stringify(a),
            dataType: "json",
            contentType: 'application/json; charset=utf-8',
            success: function (msg) {
                var res = JSON.parse(msg.d);
                $("#sr").html("")
                $('#Results').hide();
                $('#Results').css("display", "none");

                $.each($.parseJSON(msg.d), function () {

                    var test = "<tr class=\"gradeC\"><td id= \"accountid\" class=\"hidden\">" + this.AccountID + "</td><td> <div class=\"cbp0\" style=\"margin:0\"> <label> <input type=\"checkbox\" value=\"\"> </label> </div></td><td>" + this.LastName + "</td><td>" + this.FirstName + "</td><td class=\"hidden-phone\">" + this.SSN + "</td><td class=\"hidden-phone\">" + this.TestDate + "</td><td class=\"hidden-phone\">" + this.TestCategory + "</td><td class=\"hidden-phone\">" + this.Test + "</td><td class=\"hidden-phone\">" + this.TestResult + "</td><td><div><a ID=\"uploadx\" class=\"\" href=\"Tests.aspx#" + this.AccountID + "\">View</a></div></td></tr>"

                    $("#sr").append(test)


                })
                $("#loader").addClass("hidden");
               
                $('#Results').show();
                $('#Results').css("display", "inline-block !important")
                $('#companyempsearch').show();
               
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
           
             
        });
        $('#empsearchtable').DataTable({
            destroy: true
        });

       
        //id = "datatable-tabletools"
        //$('#datatable-tabletools').css("width", "100%")
    });





  

    var n = pathname.indexOf("Tests.aspx");
    if (n > 0) {

        var aid = url.substring(url.indexOf('#') + 1)
     

        var un = sessionStorage.getItem("username")
        if (un === null) {
            window.location.href = "clientlogin.aspx";
        }
        else {

            if (aid > 0) {
                un = "aid="+aid
               
            }
            var sendurl = null
            var a = {};
            sendurl = 'Tests.aspx/getAccountInfo'
            a.username = un
            $.ajax({
                type: "POST",
                url: sendurl,
                data: JSON.stringify(a),
                dataType: "json",
                contentType: 'application/json; charset=utf-8',
                success: function (msg) {
                    var res = JSON.parse(msg.d);

                    $.each($.parseJSON(msg.d), function () {

                        $('#fname').text(this.FirstName)
                        $('#lname').text(this.LastName)
                        $('#ssn').text(this.SSN)
                        $('#setupdate').text(this.SetupDate1)
                        $('#phone').text(this.Phone)
                        $('#email').text(this.Email)
                        $('#address').text(this.Address)
                        $('#LastTestDate').text(this.LastTestDate)
                        $('#PendingTestResult').text(this.PendingTestResult)
                        sessionStorage.setItem("RoleID",this.RoleID)
                    })

                
                  
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(thrownError);
                }

            });

            sendurl = 'Tests.aspx/getTestInfo'


            $.ajax({
                type: "POST",
                url: sendurl,
                data: JSON.stringify(a),
                dataType: "json",
                contentType: 'application/json; charset=utf-8',
                success: function (msg) {
                    var res = JSON.parse(msg.d);
                    $("#sr").html("")
                    $("#hr").html("")
                    var hideccf
                    var c = 0

                    $.each($.parseJSON(msg.d), function () {
                        
                        hideccf = this.CCFFileName

                        if (hideccf === "NULL" && c == 0) {
                          
                            var testh = "<tr><th>Test ID</th><th>Test</th><th class=\"hidden-phone\">Category</th><th class=\"hidden-phone\">Test Date</th><th class=\"hidden-phone\">Result</th><th class=\"hidden-phone\">Test Result</th></tr>"
                            $("#hr").append(testh)
                        }
                        else if (hideccf !== "NULL" && c == 0)
                        {
                      
                            var testh = "<tr><th>Test ID</th><th>Test</th><th class=\"hidden-phone\">Category</th><th class=\"hidden-phone\">Test Date</th><th class=\"hidden-phone\">Result</th><th id=\"hccf\"class=\"hidden-phone\">CCF</th><th class=\"hidden-phone\">Test Result</th></tr>"
                            $("#hr").append(testh)
                        }
                        c = 1;
                      
                        if (hideccf === "NULL") {
                           
                         
                            var test = "<tr class=\"gradeC\"><td id=\"TestID\">" + this.TestID + "</td><td id=\"Test\">" + this.Test + "</td><td id=\"TestCategory\" >" + this.TestCategory + "</td><td id=\"TestDate\" class=\"hidden-phone\">" + this.TestDate + "</td><td id=\"Result\" class=\" hidden-phone\">" + this.Result + "</td><td class=\" hidden-phone\"><span id=\"testresult\"><a target=\"_blank\" href=\"uploads\\" + this.TestFileName + "\">View</a></span></td></tr>"
                            $("#sr").append(test)
                        }
                        else {
                           
                            var test = "<tr class=\"gradeC\"><td id=\"TestID\">" + this.TestID + "</td><td id=\"Test\">" + this.Test + "</td><td id=\"TestCategory\" >" + this.TestCategory + "</td><td id=\"TestDate\" class=\"hidden-phone\">" + this.TestDate + "</td><td id=\"Result\" class=\" hidden-phone\">" + this.Result + "</td><td class=\" hidden-phone\"><span id=\"ccf\"><a target=\"_blank\" href=\"uploads\\" + this.CCFFileName + "\">View</a></span></td> <td class=\" hidden-phone\"><span id=\"testresult\"><a target=\"_blank\" href=\"uploads\\" + this.TestFileName + "\">View</a></span></td></tr>"
                            $("#sr").append(test)
                        }
                    });
                    $('#testtable').DataTable({
                        "order": [[3, 'desc']]
                    });
                    
                    
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(thrownError);
                }

            });


            $.ajax({
                type: "POST",
                url: 'clientlogin.aspx/getUserName',
                data: JSON.stringify(a),
                dataType: "json",
                contentType: 'application/json; charset=utf-8',
                success: function (msg) {
                    var res2 = msg.d;


                    sessionStorage.setItem("FullName", res2)

                    $('#profileName1').text(res2)

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(thrownError);
                }

            });
        }
    }
 
 

    var l = url.indexOf("clientlogin.aspx");

    if (l > 0) {
     sessionStorage.clear();
    }

    var z = url.indexOf("adminlogin.aspx");

    if (z > 0) {
        sessionStorage.clear();
    }



    if ($.cookie.read('mls_username') !== null) {
        var t = $.cookie.read('mls_username').length


        if (t > 0) {
            $('#RememberMe').attr('checked', true);

            $("#form #username").val($.cookie.read('mls_username'))
            $("#form #pwd").val($.cookie.read('mls_pwd'))

        }
    }

   
       

 

    $('#login').click(function () { })
    var login = {
        initialized: false,
        initialize: function () {

            if (this.initialized) return;
            this.initialized = true;

            this.build();
            this.events();

        },
        build: function () {

            this.validations();

        },
        events: function () {



        },
        validations: function () {
            $("#form").validate({
                submitHandler: function (form) {
                    var submitButton = $(this.submitButton);
                    var sendurl = null
                    a = {};


                    sendurl = 'clientlogin.aspx/login'
                    sessionStorage.setItem("username",null)
                    sessionStorage.setItem("username", $("#form #username").val());

                    a.username = $("#form #username").val()
                    a.pwd = $("#form #pwd").val()
                   


                    $.ajax({
                        type: "POST",
                        url: 'clientlogin.aspx/roleid',
                        data: JSON.stringify(a),
                        dataType: "json",
                        contentType: 'application/json; charset=utf-8',
                        success: function (msg) {
                            var res = JSON.parse(msg.d);
                            sessionStorage.removeItem("rid")
                            sessionStorage.setItem("rid", res)
                            //alert(sessionStorage.getItem("rid"))
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert(xhr.status);
                            alert(thrownError);
                        }

                    });


                    $.ajax({
                        type: "POST",
                        url: sendurl,
                        data: JSON.stringify(a),
                        dataType: "json",
                        contentType: 'application/json; charset=utf-8',
                        //beforeSend: function () {

                        //    if ($('#RememberMe').is(':checked')) {
                        //        $.cookie.write('mls_username', $("#form #username").val(), 24 * 60 * 60);
                        //        $.cookie.write('mls_pwd', $("#form #pwd").val(), 24 * 60 * 60);
                        //    }
                        //    else {
                        //        $.cookie.destroy('mls_username');
                        //        $.cookie.destroy('mls_pwd');
                        //    }


                           
                         
                        //},
                        success: function (msg) {
                            var res = JSON.parse(msg.d);
                            if (res > 0) {
                                sessionStorage.setItem("token", res);
                             
                                var rid = sessionStorage.getItem("rid")
                                if (rid > 0) {
                                    window.location.href = "EmployeeSearch.aspx";
                                }
                                //else {
                                //    window.location.href = "Tests.aspx";
                                //}
                            } else {
                                $("#contactError").removeClass("hidden");
                                $("#contactSuccess").addClass("hidden");
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                         
                            alert("Error authenticating against the server. Please try later")
                        }
                                , complete: function () {
                           

                                }
                    });
                },
                rules: {
                    username: {
                        required: true
                    },
                    pwd: {
                        required: true
                    }

                }
            });
        }
    };
    login.initialize();



    $('#resetpw').click(function () {})
    var resetpw = {
        initialized: false,
        initialize: function () {

            if (this.initialized) return;
            this.initialized = true;

            this.build();
            this.events();

        },
        build: function () {
            this.validations();
        },
        events: function () {



        },
        validations: function () {
            $("#rform").validate({
                submitHandler: function (form) {
                    var submitButton = $(this.submitButton);
                    var sendurl = null
                    a = {};
                    sendurl = 'clientresetpassword.aspx/sendmail'
                    a.email = $("#rform #email").val()
                    $("#contactError").addClass("hidden");
                    $("#contactSuccess").addClass("hidden");
                    $.ajax({
                        type: "POST",
                        url: 'clientresetpassword.aspx/sendmail',
                        data: JSON.stringify(a),
                        dataType: "json",
                        contentType: 'application/json; charset=utf-8',
                        success: function (msg) {
                            var res = JSON.parse(msg.d);
                            
                             if (res === -2) {
                                $("#contactError").text("The supplied email address is not found in our system. Please try using another email address").removeClass("hidden");
                                $("#contactSuccess").addClass("hidden");

                            }
                             if (res === -1) {
                                $("#contactError").text("No matching name found in our system. Please contact the administrator").removeClass("hidden");
                                $("#contactSuccess").addClass("hidden");
                             }
                             if (res === 1 ) {
                                 $("#contactError").addClass("hidden");
                                 $("#contactSuccess").removeClass("hidden");

                             }
                             if (res === 0) {
                                 
                                {
                                    $("#contactError").text("There was an error sending your message.Please contact the administrator").removeClass("hidden");

                                    $("#contactSuccess").addClass("hidden");
                                }
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            //alert(xhr.status);
                            //alert(thrownError);
                            alert("Error authenticating against the server. Please try later")
                        }
                                , complete: function () {
                                    $("#rform #email").val("")

                                }
                    });
                },
                rules: {
                    email: {
                        required: true
                        ,email:true
                    }

                }
            });
        }
    };
    resetpw.initialize();

    $('#setpw').click(function () { })
    var setpw = {
        initialized: false,
        initialize: function () {

            if (this.initialized) return;
            this.initialized = true;

            this.build();
            this.events();

        },
        build: function () {
            this.validations();
        },
        events: function () {
        },
        validations: function () {
            $("#reform").validate({
                submitHandler: function (form) {
                    var submitButton = $(this.submitButton);
                    var sendurl = null
                    a = {};
                    sendurl = 'clientsetpassword.aspx/resetpassword'
                    a.oldpw = $("#reform #tempw").val()
                    a.newpw = $("#reform #pwd1").val()
                    a.url = window.location.href
                    $("#contactError").addClass("hidden");
                    $("#contactSuccess").addClass("hidden");
                    $.ajax({
                        type: "POST",
                        url: sendurl,
                        data: JSON.stringify(a),
                        dataType: "json",
                        contentType: 'application/json; charset=utf-8',
                        success: function (msg) {
                            var res = JSON.parse(msg.d);
                            if (res === -1) {

                                {
                                    $("#contactError").text("Invalid temporary password").removeClass("hidden");

                                    $("#contactSuccess").addClass("hidden");
                                }
                            }
                           
                            if (res === 0) {

                                {
                                    $("#contactError").text("Error resetting password. Please contact Administrator").removeClass("hidden");

                                    $("#contactSuccess").addClass("hidden");
                                }
                            }
                            if (res >  0) {

                                {
                                    $("#contactError").addClass("hidden");

                                    $("#contactSuccess").removeClass("hidden");
                                }
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            //alert(xhr.status);
                            //alert(thrownError);
                            alert("Error authenticating against the server. Please try later")
                        }
                                , complete: function () {
                                    $("#reform #tempw").val("")
                                     $("#reform #pwd1").val("")
                                     $("#reform #pwd2").val("")
                                }
                    });
                },
                rules: {
                    tempw: {
                        required: true
                        
                    },
                    pwd1: {
                        required: true

                    }
                    , pwd2: {
                        required: true,
                        equalTo: "#pwd1"

                    }

                }
            });
        }
    };
    setpw.initialize();

    $('#logout').click(function () {
        var logout = sessionStorage.getItem("RoleID")
    
        if (logout == "1") {
            window.location.href = "adminlogin.aspx"

        }
        else {
            window.location.href = "clientlogin.aspx"
        }
    });

    $('#SearchCompany').click(function () {

        var sendurl = null
        var a = {};
        sendurl = 'AddCompanyRep.aspx/searchCompany'
        a.CompanyName = $("#compname").val()
        $.ajax({
            type: "POST",
            url: sendurl,
            data: JSON.stringify(a),
            dataType: "json",
            contentType: 'application/json; charset=utf-8',
            success: function (msg) {
                var res = JSON.parse(msg.d);
                $("#sr").html("")

                $.each($.parseJSON(msg.d), function () {

                    var test = "<tr class=\"gradeC\"><td>" + this.CompanyID + "</td><td class=\"hidden-phone\">" + this.CompanyName + "</td><td class=\"hidden-phone\">" + this.CompanyAddress + "</td><td><div><a ID=\"AddRep\"  class=\"btn btn-small btn-primary\" href=\"AddLocationRep.aspx#" + this.CompanyID + "\">Add Rep</a></div></td></tr>"
                   
                    $("#sr").append(test)


                })
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }

        });

        $('#Results').css("display", "inline-block")
        id = "datatable-tabletools"
        $('#datatable-tabletools').css("width", "100%")
    });

   

    //
 

    $('#SearchCompany2').click(function () {

        var sendurl = null
        var a = {};
        sendurl = 'AddCompanyEmployee.aspx/searchCompany'
        a.CompanyName = $("#compname").val()
        $.ajax({
            type: "POST",
            url: sendurl,
            data: JSON.stringify(a),
            dataType: "json",
            contentType: 'application/json; charset=utf-8',
            success: function (msg) {
                var res = JSON.parse(msg.d);
                $("#sr").html("")

                $.each($.parseJSON(msg.d), function () {

                    var test = "<tr class=\"gradeC\"><td>" + this.CompanyID + "</td><td class=\"hidden-phone\">" + this.CompanyName + "</td><td class=\"hidden-phone\">" + this.CompanyAddress + "</td><td><div><a ID=\"AddRep\"  class=\"btn btn-small btn-primary\" href=\"AddLocationEmployee.aspx#" + this.CompanyID + "\">Add Employee</a></div></td></tr>"

                    $("#sr").append(test)


                })
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }

        });

        $('#Results').css("display", "inline-block")
        id = "datatable-tabletools"
        $('#datatable-tabletools').css("width", "100%")
    });

   
    
    

//
})