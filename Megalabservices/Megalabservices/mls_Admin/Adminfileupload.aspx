﻿<%@ Page Title="" Language="C#" MasterPageFile="megalabs.master" AutoEventWireup="true" CodeFile="Adminfileupload.aspx.cs" Inherits="fileupload" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <section class="body">
        <section style="margin-top:40px">
  <section class="panel">
		<header class="panel-heading" style="background:#ecedf0">
			<h3 class="" style="color:#0077b3; text-align:center"><strong>Client (Secure) file upload</strong></h3>
          
		</header>
        <div class="panel-body" style="">
				
             <div class="container">

					<div class="row">
                       
                     
						<div class="col-md-12" >

							

							<form id="uFormClient"  method="POST" Runat="Server">
								
                             <div  class="col-md-5 col-md-offset-3" style="border:1px solid white;padding-top:30px; background:#ecedf0 !important">
                                 
                            <div Runat="Server" class="alert alert-success" id="contactSuccess">
                                 
								<strong>Success!</strong> <asp:Label runat="server" id="StatusLabel"/>
							</div>

							<div Runat="Server" class="alert alert-danger" id="contactError">
								<strong>Error!</strong> <asp:Label runat="server" id="StatusLabelError"/>
							</div>
    <div class="clearfix"><br /></div>

                                 <div class="form-group">
                                  
												<div class="col-md-12">
													
																<input id="From"  name="From" class="form-control" data-msg-required="Please enter name" placeholder="From" type="text" />
													
														
												</div>
                                     </div>
                            
                                  <div class="form-group">
                                       <label style="text-align:left"></label>
												<div class="col-md-12">
													
																<input id="Subject" name="Subject" class="form-control"data-msg-required="Please enter subject"  placeholder="Subject" type="text"/>
													
														
												</div>
                                     </div>

                                  <div class="form-group">
                                 
												<div class="col-md-12">
													
																<textarea id="Note" rows="6" name="Note" class="form-control" placeholder="Notes"></textarea>
													
														
												</div>
                                     </div>
                              
							
									<div class="form-group center">
							
												<div class="col-md-12">
  
                                         <asp:FileUpload id="FileUploadControl" runat="server" />
                                                    <br />
                                         <asp:Button runat="server" class="btn btn-primary btn-sm mb-xlg center" id="UploadButton" text="Upload file" onclick="UploadButton_Click" />
                                                          
														</div>
                                         
												

                                    </div>
								
				
                                
 </div>
							</form>

						</div>
						

					</div>

				</div>
					</div>
	
	</section>



</section>
        </section>
    
   
</asp:Content>

