﻿<%@ Page Title="" Language="C#" MasterPageFile="mlsplain.master" AutoEventWireup="true" CodeFile="AddCompanyRep.aspx.cs" Inherits="AddCompanyRep" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
      <script src="../js/json2.js"></script>
    <script src="../js/json2.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <section class="body whitebody">
        <section style="margin-top:80px">
           <div class="container">
							<div class="col-md-12">
                                <h2 class="panel-title" style="font-size:28px;padding:20px 0">Search Criteria</h2>
                                <h4>Please enter Company</h4>
								<section class="panel">
									<form class="form-horizontal" novalidate="novalidate">
											<div class="tab-content">
												<div id="w4-account" class="tab-pane active">
													<div class="form-group">
														<label class="col-sm-3 control-label" for="w4-username">Company Name</label>
														<div class="col-sm-3">
															<input type="text" class="form-control" name="compname" id="compname" required>
														</div>
													</div>
													

                                                    <div>
                                                        <a ID="SearchCompany" class="btn btn-small btn-primary" href="#">Search</a></div>
												</div>
											</div>
										</form>
                                    
								</section>
                                  
							</div>
						
						</div>
        </section>
      
    	<hr />	
<section  class="panel">
<div id="Results" class="container" style="display:none">
     <h2 class="panel-title" style="font-size:28px;padding:20px 0">Search Results</h2>
<div class="panel-body" style="">
<table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">
<thead>
	<tr>

		<th>Company ID</th>
		<th>Company Name</th>
		<th class="hidden-phone">Company Address</th>
        <th class="hidden-phone">Action</th>
	</tr>
</thead>
<tbody id="sr">

</tbody>
</table>
</div>
</div>
</section>
						

    </section>
</asp:Content>
