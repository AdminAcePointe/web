﻿<%@ Page Title="" Language="C#" MasterPageFile="megalabs.master" AutoEventWireup="true" CodeFile="AddLocationEmployee.aspx.cs" Inherits="AddLocationEmployee" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <section class="body">
        <section style="margin-top:80px">
  <section class="panel">
		<header class="panel-heading" style="background:#ecedf0">
			<h3 class="panel-title" style="color:#0077b3; text-align:center">Locations</h3>
            <a class="text-center" href="Portal.aspx">Back to Portal </a>
                    <span>|</span>
                    <a class="text-center" href="AddLocationEmployee.aspx">Back to Search</a>
		</header>
  
	</section><section  class="panel">
<div id="Results" class="container">
     <h2 class="panel-title" style="font-size:28px;padding:20px 0">Search Results - Locations for Company : </h2>
<div class="panel-body" style="">
    <form id="repForm"  method="POST">
<table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">
<thead>
	<tr>

		<th class="hidden">Location ID</th>
		<th>Site Code</th>
		<th class="hidden-phone">Location Name</th>
        <th class="hidden-phone">Location Address</th>
        <th class="hidden-phone">Choose Employee</th>
	</tr>
</thead>
<tbody id="sr">

</tbody>
</table>
    		<div class="row" style="padding-top:30px">
									<div class="col-md-12">
										<input id="saveemp" type="submit" value="Assign Employee" class="btn btn-primary btn-sm mb-xlg" data-loading-text="Loading...">
									</div>
								</div>


							</form>
</div>
</div>
</section>


</section>
        </section>
    
   
</asp:Content>

