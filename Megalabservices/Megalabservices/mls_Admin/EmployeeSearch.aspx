﻿<%@ Page Title="" Language="C#" MasterPageFile="mlsplain.master" AutoEventWireup="true" CodeFile="EmployeeSearch.aspx.cs" Inherits="EmployeeSearch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <section class="body whitebody dtc">
        <section style="margin-top:80px">
           <div class="container">
							<div class="col-md-12">
                                <h2 class="panel-title" style="font-size:28px;padding:20px 0; color:#0077b3">Search Criteria</h2>
                                <h4>Please search by donor name, location or site code to locate donor(s) for which you want to view test results.</h4>
								<section class="panel">
									<form class="form-horizontal" novalidate="novalidate">
											<div class="tab-content">
												<div id="w4-account" class="tab-pane active">
													<div class="form-group">
														<label class="col-sm-2 control-label" for="w4-username">Donor First Name</label>
														<div class="col-sm-3">
															<input type="text" class="form-control" name="username" id="fname">
														</div>
                                                        <label class="col-sm-2 control-label" for="w4-username">Donor Last Name</label>
														<div class="col-sm-3">
															<input type="text" class="form-control" name="username" id="lname">
														</div>
													</div>
                                              
													<div class="form-group">
														<label class="col-sm-2 control-label" for="w4-password">Location</label>
														<div class="col-sm-3">
															<input type="text" class="form-control" name="location" id="location">
														</div>
                                                        <label class="col-sm-2 control-label" for="w4-password">Site Code</label>
														<div class="col-sm-3">
															<input type="text" class="form-control" name="password" id="sitecode">
														</div>
													</div>
                                                    
                                                     <div class="form-group">
														<label class="col-sm-2 control-label" for="w4-password">Test Reported From</label>
														<div class="col-sm-3">
															 <div class='input-group date' id="testreportedfrom1" >
                                                <input id="testreportedfrom" type='text' class="form-control" data-msg-required="Please choose date." name="aptdate"/>
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
														</div>
                                                         <label class="col-sm-2 control-label" for="w4-password">Test Reported To</label>
														<div class="col-sm-3">
															 <div class='input-group date' id="testreportedto1" >
                                                <input id="testreportedto" type='text' class="form-control" data-msg-required="Please choose date." name="aptdate"/>
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
														</div>
													</div>
                                                     
                                                    <div><p>
                                                <span><a ID="companyempsearch" class="btn btn-lg btn-primary center" href="#">Search</a></span>
												<span ><img id="loader" class="hidden" style="max-width:80px; max-height:80px;" src="assets/images/loader.gif" /></span>
                                                    </p>
                                                        </div>
                                                </div>
                                                </div>
                                        </form>
                                  
                                   
								</section>
                                  
							</div>
						
						</div>
        </section>
      
    	<hr />	
<section  class="panel">
    <div class="container">
<div id="Results" class="container" style="display:none">
     <h2 class="panel-title" style="font-size:28px;padding:20px 0; color:#0077b3">Search Results</h2>
<div class="panel-body" style="">
<table class="table table-bordered table-striped mb-none" id="empsearchtable" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">

<thead>
	<tr>
		<th>#</th>
		<th>Last Name</th>
		<th>First Name </th>
		<th class="hidden-phone">Donor ID</th>      
        <th class="hidden-phone">Test Date</th>
        <th class="hidden-phone">Test Category</th>
        <th class="hidden-phone">Test</th>
        <th class="hidden-phone">Test Result</th>
        <th class="hidden-phone">Action</th>
	</tr>
</thead>
<tbody id="sr">
<tr class="gradeC hidden">
		<td>
            <div class="cbp0" style="margin:0">
		    <label>
			    <input type="checkbox" value="">			  
		    </label>
	    </div></td>
    <td>
         </td>
		<td>
		</td>
    <td>
		</td>
    <td>
		</td>
		<td>
		</td>
		<td class="hidden-phone"></td>
		
        <td class=" hidden-phone"><a class="simple-ajax-modal" href="fileuploads.html"></a></td>
		                       
	</tr>
	
</tbody>
</table>
</div>
</div>
        </div>
</section>
						

    </section>
</asp:Content>


