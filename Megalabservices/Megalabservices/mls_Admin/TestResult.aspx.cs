﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Mail;

public partial class mls_Admin_TestResult : System.Web.UI.Page
{

    static List<Comp> Company; // Static List instance
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod]
    public static object CompanyList()
    {
        try
        {
           // Get data from database
            int studentCount = GetCompanyCount();
            List<Comp> Company = GetCompany();

           // Return result to jTable
            return new { Result = "OK", Records = Company, TotalRecordCount = studentCount };
        }
        catch (Exception ex)
        {
            return new { Result = "ERROR", Message = ex.Message };
        }
    }

    //CompanyList
    private static int GetCompanyCount()
    {
        int cnt;

        string cs = ConfigurationManager.ConnectionStrings["eklipseconsult"].ConnectionString;
        using (SqlConnection con = new SqlConnection(cs))
        {
            using (SqlCommand cmd = new SqlCommand("dbo.getCompanyCount", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                cnt = Convert.ToInt32(cmd.ExecuteScalar());

                con.Close();

            }

        }
      
        return cnt;
    }

    private static List<Comp> GetCompany()
    {
       

        Company = new List<Comp>();

        string cs = ConfigurationManager.ConnectionStrings["eklipseconsult"].ConnectionString;
        DataTable dt = new DataTable();
        using (SqlConnection con = new SqlConnection(cs))
        {
            using (SqlCommand cmd = new SqlCommand("EXEC getCompanyList", con))
            {
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row;
                foreach (DataRow dr in dt.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        row.Add(col.ColumnName, dr[col]);
                    }
                    rows.Add(row);
                }

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Comp c = new Comp();
                    c.CompanyId = Convert.ToInt32(dt.Rows[i]["CompanyID"]);
                    c.Name = dt.Rows[i]["CompanyName"].ToString();

                    Company.Add(c);
                }
                return Company;
            }
        }

        
    }
  

   
}