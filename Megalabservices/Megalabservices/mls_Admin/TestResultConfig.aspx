﻿<%@ Page Title="" Language="C#" MasterPageFile="mlsplain.master" AutoEventWireup="true" CodeFile="TestResultConfig.aspx.cs" Inherits="TestResultConfig" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
      <script src="../js/json2.js"></script>
    <script src="../js/json2.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <section class="body whitebody">
        <section style="margin-top:80px">
           <div class="container">
							<div class="col-md-12">
                                <h2 class="panel-title" style="font-size:28px;padding:20px 0">Search Criteria</h2>
                                <h4>Please enter customer's first name and last name and/or social security number to locate customer's account for results upload.</h4>
								<section class="panel">
									<form class="form-horizontal" novalidate="novalidate">
											<div class="tab-content">
												<div id="w4-account" class="tab-pane active">
													<div class="form-group">
														<label class="col-sm-3 control-label" for="w4-username">First Name</label>
														<div class="col-sm-3">
															<input type="text" class="form-control" name="username" id="fname">
														</div>
													</div>
													<div class="form-group">
														<label class="col-sm-3 control-label" for="w4-password">Last Name</label>
														<div class="col-sm-3">
															<input type="text" class="form-control" name="password" id="lname">
														</div>
													</div>
                                                    <div class="form-group">
														<label class="col-sm-3 control-label" for="w4-last-name">SSN/Tax-ID</label>
														<div class="col-sm-3">
															<input type="text" class="form-control" name="last-name" id="ssn" required>
														</div>
													</div>

                                                    <div>
                                                        <a ID="Search" class="btn btn-small btn-primary" href="#">Search</a></div>
												</div>
											</div>
										</form>
                                    
								</section>
                                  
							</div>
						
						</div>
        </section>
      
    	<hr />	
<section  class="panel">
<div id="Results" class="container" style="display:none">
     <h2 class="panel-title" style="font-size:28px;padding:20px 0">Search Results</h2>
<div class="panel-body" style="">
<table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">
<thead>
	<tr>
		<th>#</th>
		<th>Last Name</th>
		<th>First Name </th>
		<th class="hidden-phone">SSN/TaxID(Last 4)</th>
        <th class="hidden-phone">Action</th>
	</tr>
</thead>
<tbody id="sr">
<tr class="gradeC hidden">
		<td>
            <div class="cbp0" style="margin:0">
		    <label>
			    <input type="checkbox" value="">			  
		    </label>
	    </div></td>
		<td>
		</td>
		<td>
		</td>
		<td class="hidden-phone"></td>
		
        <td class=" hidden-phone"><a class="simple-ajax-modal" href="fileuploads.html"></a></td>
		                       
	</tr>
	
</tbody>
</table>
</div>
</div>
</section>
						

    </section>
</asp:Content>
