﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Mail;
using System.IO;
using System.Drawing;


public partial class fileupload : System.Web.UI.Page
{


    protected void Page_Load(object sender, EventArgs e)
    {

        contactSuccess.Visible = false;
        contactError.Visible = false;

    }

    protected void UploadButton_Click(object sender, EventArgs e)
    {
        int res;
        string From = Page.Request.Form["From"].ToString(); ;
        string Subject = Page.Request.Form["Subject"].ToString(); ;
        string Note = Page.Request.Form["Note"].ToString(); ;

        if (FileUploadControl.HasFile)
        {
            try
            {
                string filename = Path.GetFileName(FileUploadControl.FileName);
                FileUploadControl.SaveAs(Server.MapPath("~/mls_Admin//uploads/") + filename);
             
                StatusLabel.Text = "Your file has been uploaded successfully!";
                contactSuccess.Visible = true;





                res = send(From,Subject, Note);
            }
            catch (Exception ex)
            {
                StatusLabelError.Text = "There was an error uploading your file. Please try again later or call our office at (866) 261-7129 or (571) 285-1857 for assistance.";
               
                contactError.Visible = true; ;
            
            }
        }
    }

      
 
    private static int send(string From, string Subject, string Note)
    {
        int res = 0;

        try
        {
                StringBuilder emailstring = new StringBuilder();
                emailstring.AppendLine("<p><span>New client file was uploaded</span></p>");
                emailstring.AppendLine("<p><span>From : " + From + "</span></p>");
                emailstring.AppendLine("<p><span>Subject : " + Subject + "</span></p>");
                emailstring.AppendLine("<p><span>Note : " + Note + "</span></p>");
                emailstring.AppendLine("<p><span>Sincerely,</span></p>");
                emailstring.AppendLine("<p><span>Megalab Services</span></p>");

              
                const string SERVER = "relay-hosting.secureserver.net";
                MailMessage newmessage = new System.Web.Mail.MailMessage();
               
                newmessage.From = "info@megalabservices.com";
                newmessage.To = "info@megalabservices.com";
                newmessage.Bcc = "ayodele.olatunji@eklipseconsult.com";
                newmessage.BodyFormat = MailFormat.Html;
                newmessage.BodyEncoding = Encoding.UTF8;
                newmessage.Subject = "Megalabservices - Notification Of Client file upload";
                newmessage.Body = emailstring.ToString();
                SmtpMail.SmtpServer = SERVER;
                SmtpMail.Send(newmessage);

                res = 1;
            }
            catch
            {
                res = 0;
            }
            return res;
        }
    }
 
