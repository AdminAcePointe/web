﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Mail;


public partial class fileupload : System.Web.UI.Page
{
    static List<replist> rep;
    static List<replist> replist; 


    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod]
    public static int insertTest(
        string accountID, string testcategory
        , string test, string testdate, string result, string ccffilename
        , string testfilename, string notify, string notifyrep
        )
    {

        int account = 0;
        try
        {


            string cs = ConfigurationManager.ConnectionStrings["eklipseconsult"].ConnectionString;
            using (SqlConnection con = new SqlConnection(cs))
            {
                using (SqlCommand cmd = new SqlCommand("dbo.insertTest", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@accountID", accountID);
                    cmd.Parameters.AddWithValue("@testcategory", testcategory);
                    cmd.Parameters.AddWithValue("@testdate", testdate);
                    cmd.Parameters.AddWithValue("@test", test);
                    cmd.Parameters.AddWithValue("@result", result);
                    cmd.Parameters.AddWithValue("@ccffilename", ccffilename);
                    cmd.Parameters.AddWithValue("@testfilename", testfilename);





                    con.Open();
                    account = Convert.ToInt32(cmd.ExecuteScalar());

                    con.Close();

                }

            }
            account = 1;

            if (notify == "1" || notifyrep == "1")
            {
                int em;
                em = sendmail(accountID, test, testdate, notify, notifyrep);
            }
        }
        catch
        {
            account = 0;
        }

        return account;
    }

    [WebMethod]
    public static int sendmail(string accountid, string test, string testdate, string notify, string notifyrep)
    {
        int res = 0;
        string name = "z";
        string email = "z";
        string url = "https://megalabservices.com/mls_Admin/clientlogin.aspx";


        if (notify == "1")
        {
            name = getcustomername(accountid);
            email = getcustomeremail(accountid);

            if (name != "z")
            {
                int n = name.IndexOf("?");
                string z = name.Substring(n);
                string urlstring = url + z;
                string[] subname = name.Split('?');
                string subname2 = subname[0];

                res = send(accountid, test, testdate, subname2, url, email);
            }
            else
            {
                res = -1;
            }
        }

        if (notifyrep == "1")
        {
            List<replist> replist = getrep(accountid);
            
            foreach(replist item in replist){
                 res = send(accountid, test, testdate, item.repname, url, item.repemail);
            }
           
        }
         
        return res;
            
        }
      
    private static string getcustomername(string accountid)
    {
        string name = String.Empty;


        string cs = ConfigurationManager.ConnectionStrings["eklipseconsult"].ConnectionString;
        using (SqlConnection con = new SqlConnection(cs))
        {
            using (SqlCommand cmd = new SqlCommand("dbo.sp_getcustomerNamebyAccountID", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@AccountID", accountid);
                con.Open();
                name = (cmd.ExecuteScalar().ToString());
                con.Close();


            }
        }
        return name;
    }
    private static string getcustomeremail(string accountid)
    {
        string name = String.Empty;


        string cs = ConfigurationManager.ConnectionStrings["eklipseconsult"].ConnectionString;
        using (SqlConnection con = new SqlConnection(cs))
        {
            using (SqlCommand cmd = new SqlCommand("dbo.sp_getcustomerEmailByAccountID", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@AccountID", accountid);
                con.Open();
                name = (cmd.ExecuteScalar().ToString());
                con.Close();


            }
        }
        return name;
    }
    private  static List<replist> getrep(string accountid)
    {
        string name = String.Empty;

        rep = new List<replist>();

        string cs = ConfigurationManager.ConnectionStrings["eklipseconsult"].ConnectionString;

        DataTable dt = new DataTable();

        using (SqlConnection con = new SqlConnection(cs))
        {
            using (SqlCommand cmd = new SqlCommand("dbo.sp_getrepNamebyAccountID2", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@AccountID", accountid);
                con.Open();

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row;
                foreach (DataRow dr in dt.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        row.Add(col.ColumnName, dr[col]);
                    }
                    rows.Add(row);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    replist c = new replist();
                    c.repname =  dt.Rows[i]["repname"].ToString();
                    c.repemail = dt.Rows[i]["repemail"].ToString();


                    rep.Add(c);
                }

                con.Close();


            }
        }
        return rep;
    }
    private static int send(string accountid, string test, string testdate, string subname2 , string url , string email)
    {
        int res = 0;
        
            try
            {
                StringBuilder emailstring = new StringBuilder();
                emailstring.AppendLine("<p><span>Dear </span><span>" + subname2 + "</span></p>");
                emailstring.AppendLine("<p><span>The results of this test is available for your review:</span></p>");
                emailstring.AppendLine("<p><span>Test ID: " + test + "</span></p>");
                emailstring.AppendLine("<p><span>Test Date: " + testdate + "</span></p><br><br>");
                emailstring.AppendLine("<p><span>Please log into your account - <a href=" + url + ">Megalabs Services Account Log In</a> - to view, print or download your test results.</span></p>");

                emailstring.AppendLine("<p><span>If you have any problems logging into your account,  please contact a Megalab Services  Customer Care Representative at (866) 261-7129 or (571) 285-1857, Monday - Friday, 8 a.m. to 6 p.m. Eastern.</span></p>");
                emailstring.AppendLine("<p><span>Sincerely,</span></p>");
                emailstring.AppendLine("<p><span>Megalab Services</span></p>");

                //SmtpClient smtp = new SmtpClient();
                const string SERVER = "relay-hosting.secureserver.net";
                MailMessage newmessage = new System.Web.Mail.MailMessage();
                // newmessage.Cc = "";
                //newmessage.Bcc = "";
                newmessage.From = "info@megalabservices.com";
                newmessage.To = email;
                newmessage.Bcc = "info@megalabservices.com";
              
                newmessage.BodyFormat = MailFormat.Html;
                newmessage.BodyEncoding = Encoding.UTF8;
                newmessage.Subject = "Megalabservices - Notification Of Test Results";
                newmessage.Body = emailstring.ToString();
                SmtpMail.SmtpServer = SERVER;
                SmtpMail.Send(newmessage);

                //smtp.Send(newmessage);
                res = 1;
            }
            catch
            {
                res = 0;
            }
            return res;
        }
    }
 
