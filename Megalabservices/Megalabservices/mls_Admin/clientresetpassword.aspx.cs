﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Mail;


public partial class mls_Admin_clientresetpassword : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod]
    public static int sendmail(string email)
    {
        int res = 0;
        string temppw = generatetemppassword();
        int successtemppw = savetemporarypassword(email, temppw);
        string name = "z";
        string url = "http://megalabservices.com/mls_Admin/clientsetpassword.aspx";
    
      

       

        if (successtemppw > 0)
        {
            name = getcustomerdetail(email);
           

          if (name != "z")
        {
            int n = name.IndexOf("?");
            string z = name.Substring(n);
            string urlstring = url + z;
              string[] subname = name.Split('?');
              string subname2 = subname[0];
            try
            {
                StringBuilder emailstring = new StringBuilder();
                emailstring.AppendLine("<p><span>Dear </span><span>" + subname2 + "</span></p>");
                emailstring.AppendLine("<p><span>You recently requested your Megalabs service client portal password changed.</span></p>");
                emailstring.AppendLine("<p><span>Your temporary password is " + temppw +"</span></p>");
                emailstring.AppendLine("<p><span>For security purposes, this password will expire in 24 hours (from the time this email was sent). Please return to the Megalab Services login page and enter your Username and this temporary password.</span></p>");
                emailstring.AppendLine("<p><span><a href=" + urlstring + ">Megalabs Services Account Log In</a></span></p>");
                emailstring.AppendLine("<p><span>Once you’ve logged in, you’ll be able to establish a permanent password.</span></p>");
                emailstring.AppendLine("<p><span>If you have any problems logging into your account, or you did not request this change, please contact a Megalabservices  Customer Care Representative at (866) 261-7129 or (571) 285-1857, Monday - Friday, 8 a.m. to 6 p.m. Eastern.</span></p>");
                emailstring.AppendLine("<p><span>Sincerely,</span></p>");
                emailstring.AppendLine("<p><span>Megalab Services</span></p>");

                //SmtpClient smtp = new SmtpClient();
                const string SERVER = "relay-hosting.secureserver.net";
                MailMessage newmessage = new System.Web.Mail.MailMessage();
                newmessage.From = "info@megalabservices.com";
                newmessage.To = email;
                newmessage.Bcc = "info@megalabservices.com";
                newmessage.BodyFormat = MailFormat.Html;
                newmessage.BodyEncoding = Encoding.UTF8;
                newmessage.Subject = "Megalabservices - Password reset Instructions";
                newmessage.Body = emailstring.ToString();
                // newmessage.Cc = "";
                //newmessage.Bcc = "";
                SmtpMail.SmtpServer = SERVER;
                SmtpMail.Send(newmessage);

                //smtp.Send(newmessage);
                res = 1;
            }
            catch
            {
                res = 0;
            }
        }
        else
        {
            res = -1; // No name match
        }
        }
        else
        {
            res = -2; // no email match

        }
        return res;
    }

   
    private static string getcustomerdetail(string email){
        string name= String.Empty;


        string cs = ConfigurationManager.ConnectionStrings["eklipseconsult"].ConnectionString;
    using (SqlConnection con = new SqlConnection(cs))
    {
        using (SqlCommand cmd = new SqlCommand("dbo.sp_getcustomerNamebyEmail", con))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@email", email);
            con.Open();
            name = (cmd.ExecuteScalar().ToString());
            con.Close();

          
        }
    }
    return name;
    }

    private static string generatetemppassword()
    {


        string temppwd = System.Web.Security.Membership.GeneratePassword(15, 5);
        return temppwd;

    }


    private static int savetemporarypassword(string email, string @temppwd)
    {
        int res = 0;


        string cs = ConfigurationManager.ConnectionStrings["eklipseconsult"].ConnectionString;
        using (SqlConnection con = new SqlConnection(cs))
        {
            using (SqlCommand cmd = new SqlCommand("dbo.sp_insertTempPasswordByemail", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@email", email);
                cmd.Parameters.AddWithValue("@temppwd", temppwd);
                con.Open();
                res = Convert.ToInt16(cmd.ExecuteScalar());
                con.Close();


            }
        }
        return res;
    }
}
