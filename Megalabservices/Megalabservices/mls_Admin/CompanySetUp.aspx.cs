﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Mail;

public partial class CompanySetUp : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod]
    public static int insertCompany( string companyname, string companytaxid,string companyemail,string companyphone
                ,string companywebsite,string companyaddress,string companycity,string companystate,string companyzip
                ,string location1sitecode,string location1name,string location1address
                ,string location1city,string location1state,string location1zip
                ,string location2sitecode
                ,string location2name,string location2address
                ,string location2city,string location2state,string location2zip
        )
    {

        int company = 0;
        try
        {
         

            string cs = ConfigurationManager.ConnectionStrings["eklipseconsult"].ConnectionString;
            using (SqlConnection con = new SqlConnection(cs))
            {
                using (SqlCommand cmd = new SqlCommand("dbo.insertCompany", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@companyname", companyname);
                    cmd.Parameters.AddWithValue("@companytaxid", companytaxid);
                    cmd.Parameters.AddWithValue("@companyemail", companyemail);
                    cmd.Parameters.AddWithValue("@companyphone", companyphone);
                    cmd.Parameters.AddWithValue("@companywebsite", companywebsite);
                    cmd.Parameters.AddWithValue("@companyaddress", companyaddress);
                    cmd.Parameters.AddWithValue("@companycity", companycity);
                    cmd.Parameters.AddWithValue("@companystate", companystate);
                    cmd.Parameters.AddWithValue("@companyzip", companyzip);

                    cmd.Parameters.AddWithValue("@location1sitecode", location1sitecode);
                    cmd.Parameters.AddWithValue("@location1name", location1name);
                    cmd.Parameters.AddWithValue("@location1address", location1address);
                    cmd.Parameters.AddWithValue("@location1city", location1city);
                    cmd.Parameters.AddWithValue("@location1state", location1state);
                    cmd.Parameters.AddWithValue("@location1zip", location1zip);

                    cmd.Parameters.AddWithValue("@location2sitecode", location2sitecode);
                    cmd.Parameters.AddWithValue("@location2name", location2name);
                    cmd.Parameters.AddWithValue("@location2address", location2address);
                    cmd.Parameters.AddWithValue("@location2city", location2city);
                    cmd.Parameters.AddWithValue("@location2state", location2state);
                    cmd.Parameters.AddWithValue("@location2zip", location2zip);



                    con.Open();
                    company = Convert.ToInt32(cmd.ExecuteScalar());

                    con.Close();

                }

            }
      
        }
        catch
        {
            company = 0;
        }

        return company;
    }
}