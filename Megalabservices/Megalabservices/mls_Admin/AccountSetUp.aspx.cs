﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Mail;

public partial class AccountSetUp : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod]
    public static int insertAccount(string fname, string lname
        , string address1
        , string address2, string city, string state
        ,string zip , string ssn, string email,
        string homephone, string username, string password, string roleID, string credID
        )
    {

        int account = 0;

        string temppwd = generatetemppassword();
        try
        {
            string enpwd = encryptpw(password);

            string cs = ConfigurationManager.ConnectionStrings["eklipseconsult"].ConnectionString;
            using (SqlConnection con = new SqlConnection(cs))
            {
                using (SqlCommand cmd = new SqlCommand("dbo.insertAccount", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@fname", fname);
                    cmd.Parameters.AddWithValue("@lname", lname);
                    cmd.Parameters.AddWithValue("@address1", address1);
                    cmd.Parameters.AddWithValue("@address2", address2);
                    cmd.Parameters.AddWithValue("@city", city);
                    cmd.Parameters.AddWithValue("@state", state);

                    cmd.Parameters.AddWithValue("@zip", zip);
                    cmd.Parameters.AddWithValue("@ssn", ssn);
                    cmd.Parameters.AddWithValue("@email", email);
                    cmd.Parameters.AddWithValue("@homephone", homephone);
                    cmd.Parameters.AddWithValue("@username", username);

                    cmd.Parameters.AddWithValue("@password", enpwd);

                    cmd.Parameters.AddWithValue("@roleID", roleID);
                    cmd.Parameters.AddWithValue("@credID", credID);
                    cmd.Parameters.AddWithValue("@temppwd", temppwd);
                    
                    cmd.Parameters.AddWithValue("@AccountTypeID", 1);
                    


                    con.Open();
                    account = Convert.ToInt32(cmd.ExecuteScalar());

                    con.Close();

                }

            }
            account = 1;
            if (credID == "1")
            {
                int em;
                em = sendmail(email, username, temppwd);
            }
        }
        catch
        {
            account = 0;
        }

        return account;
    }
    private static string encryptpw(string pwd)
    {
        byte[] bytes = System.Text.Encoding.Unicode.GetBytes(pwd);
        string encrytpwd = Convert.ToBase64String(bytes);
        return encrytpwd;
    }

    private static string generatetemppassword()
    {


        string temppwd = System.Web.Security.Membership.GeneratePassword(15, 5);
        return temppwd;

    }

    [WebMethod]
    public static int sendmail(string email,string username, string temppw)
    {
        int res = 0;
        string name = "z";
        string url = "https://megalabservices.com/mls_Admin/clientsetpassword.aspx";


            name = getcustomerdetail(email);


            if (name != "z")
            {
                int n = name.IndexOf("?");
                string z = name.Substring(n);
                string urlstring = url + z;
                string[] subname = name.Split('?');
                string subname2 = subname[0];
                try
                {
                    StringBuilder emailstring = new StringBuilder();
                    emailstring.AppendLine("<p><span>Dear </span><span>" + subname2 + "</span></p><br>");

                    emailstring.AppendLine("<p><span>Your Megalab Services account has been created.</span></p><br>");
                    emailstring.AppendLine("<p><span>Your username is " + username + "</span></p>");
                    emailstring.AppendLine("<p><span>Your temporary password is " + temppw + "</span></p><br>");


                    emailstring.AppendLine("<p><span>For security reasons, this password will expire 24 hours from the time this email was sent. Please log into your Megalab Services account through the link below to establish your permanent password and access your account.</span></p><br>");
                    emailstring.AppendLine("<p><span><a href=" + urlstring + ">Megalabs Services Account Log In</a></span></p><br>");

                    emailstring.AppendLine("<p><span>If you have any problems logging into your account, please contact a Megalabservices Customer Care Representative at (866) 261-7129 or (571) 285-1857, Monday - Friday, 8 a.m. to 6 p.m. Eastern.</span></p>");
                   
                    emailstring.AppendLine("<p><span>Sincerely,</span></p>");
                    emailstring.AppendLine("<p><span>Megalab Services</span></p>");

                    //SmtpClient smtp = new SmtpClient();
                    const string SERVER = "relay-hosting.secureserver.net";
                    MailMessage newmessage = new System.Web.Mail.MailMessage();
                    newmessage.From = "info@megalabservices.com";
                    newmessage.To = email;
                    newmessage.BodyFormat = MailFormat.Html;
                    newmessage.BodyEncoding = Encoding.UTF8;
                    newmessage.Subject = "Megalabservices - Login information";
                    newmessage.Body = emailstring.ToString();
                    // newmessage.Cc = "";
                    //newmessage.Bcc = "";
                    SmtpMail.SmtpServer = SERVER;
                    SmtpMail.Send(newmessage);

                    //smtp.Send(newmessage);
                    res = 1;
                }
                catch
                {
                    res = 0;
                }
            }
            else
            {
                res = -1; // No name match
            }
      
        return res;
    }

    private static string getcustomerdetail(string email)
    {
        string name = String.Empty;


        string cs = ConfigurationManager.ConnectionStrings["eklipseconsult"].ConnectionString;
        using (SqlConnection con = new SqlConnection(cs))
        {
            using (SqlCommand cmd = new SqlCommand("dbo.sp_getcustomerNamebyEmail", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@email", email);
                con.Open();
                name = (cmd.ExecuteScalar().ToString());
                con.Close();


            }
        }
        return name;
    }

}