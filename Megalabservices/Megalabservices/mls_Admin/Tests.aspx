﻿<%@ Page Title="" Language="C#" MasterPageFile="mlsplain.master" AutoEventWireup="true" CodeFile="Tests.aspx.cs" Inherits="Tests" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
           <script src="../js/json2.js"></script>
    <script src="../js/json2.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <section class="body whitebody dtc">
    
        <section style="margin-top:80px">
                <div id="es" class="container" style="padding-top:10px">
        <p><a id="empsearchback" style="cursor:pointer">Back to Search page</a></p>
    </div>
           <div class="container">
							<div class="col-md-12">
                                <h2 class="panel-title" style="font-size:28px;color:#0077b3; padding:20px 0">Personal Information</h2>
                                
								<section class="panel">
									
                                    <div class="table-responsive">
											<table class="table mb-none noborder">
											
												<tbody>
													<tr>
														
														<td><span class="tlabel">First Name :</span><span id ="fname"></span></td>
														<td><span class="tlabel2">Phone :</span><span id="phone"></span></td>
                                                        
                                                        <td><span class="tlabel2">First test date:</span><span id="setupdate"></span></td>
                                                     
													</tr>
													<tr>
														<td><span class="tlabel">Last Name :</span><span id="lname"></span></td>
                                                        <td><span class="tlabel2">Email :</span><span id="email"></span></td>
                                                        <td><span class="tlabel2">Last test date :</span><span id="LastTestDate"></span></td>
                                                        
														
													</tr>
													<tr>
														<td><span class="tlabel">SSN/Emp ID: </span><span id="ssn"></span></td>
                                                        <td><span class="tlabel2">Address :</span><span id="address"></span></td>
                                                        <td><span class="tlabel2">Pending Test results? :</span><span id="PendingTestResult"></span></td>
														
														
													</tr>

												</tbody>
											</table>
										</div>
									
								</section>
                                  
							</div>
						
						</div>
        </section>
      
    	<hr />	
<section class="panel" >
<div class="container" >
     <h2 class="panel-title" style="font-size:28px;color:#0077b3;padding:20px 0">Test Information</h2>
<div class="panel-body" style="">
<table class="table table-bordered table-striped mb-none" id="testtable" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">
<thead id="hr">
	
</thead>
<tbody id="sr">   
</tbody>
</table>
</div>
</div>
</section>
						

    </section>
</asp:Content>

