﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Mail;
using System.Web.Script.Serialization;


public partial class oldfileupload : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {


    }
    [WebMethod]
    public static int insertTest(
        string accountID, string testcategory
        , string test, string testdate, string result, string ccffilename
        , string testfilename
        )
    {

        int account = 0;
        try
        {
         

            string cs = ConfigurationManager.ConnectionStrings["eklipseconsult"].ConnectionString;
            using (SqlConnection con = new SqlConnection(cs))
            {
                using (SqlCommand cmd = new SqlCommand("dbo.insertTest", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@accountID", accountID);
                    cmd.Parameters.AddWithValue("@testcategory", testcategory);
                    cmd.Parameters.AddWithValue("@testdate", testdate);
                    cmd.Parameters.AddWithValue("@test", test);
                    cmd.Parameters.AddWithValue("@result", result);
                    cmd.Parameters.AddWithValue("@ccffilename", ccffilename);
                    cmd.Parameters.AddWithValue("@testfilename", testfilename);

              



                    con.Open();
                    account = Convert.ToInt32(cmd.ExecuteScalar());

                    con.Close();

                }

            }
            account = 1;
        }
        catch
        {
            account = 0;
        }

        return account;
    }
}