﻿<%@ Page Title="" Language="C#" MasterPageFile="mlsAdmin.master" AutoEventWireup="true" CodeFile="Portal.aspx.cs" Inherits="Portal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <div style="margin-top: 80px"></div>


    <section style="background: white">
        <div class="container">
            <h2 class="text-center">Account Configuration</h2>
  <div class="row">
            <div class="col-lg-4 col-sm-4">


                <a class="btn btn-lg" href="AccountSetUp.aspx">
                    <div class="plan">
                        <h5 class="text-center">Add New Account</h5>


                    </div>

                </a>

            </div>
            <div class="col-lg-4 col-sm-4 text-center">
                <a class="btn btn-lg" href="#">
                    <div class="plan text-center">
                        <h5 class="text-center">View / Modify Account</h5>


                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-4">
                <a class="btn btn-lg" href="TestResultConfig.aspx">
                    <div class="plan">
                        <h5 class="text-center">Upload Test Results</h5>


                    </div>
                </a>
            </div>

      </div>
              <div class="row">
                  <div class="col-lg-4 col-sm-4">


                <a class="btn btn-lg" href="AddCompanyEmployee.aspx">
                    <div class="plan">
                        <h5 class="text-center">Assign Employee to Location</h5>


                    </div>

                </a>

            </div>
                  </div>
        </div>
    </section>
    <section>
        <div class="container">
            <h2 class="text-center">Company Configuration</h2>
            <div class="row">
                <div class="col-lg-4 col-sm-4">


                    <a class="btn btn-lg" href="CompanySetUp.aspx">
                        <div class="plan">
                            <h5 class="text-center">Add New Company</h5>


                        </div>

                    </a>

                </div>
                <div class="col-lg-4 col-sm-4 text-center">
                    <a class="btn btn-lg" href="#">
                        <div class="plan">
                            <h5 class="text-center">Add New location</h5>


                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-4">
                    <a class="btn btn-lg" href="AddCompanyRep.aspx">
                        <div class="plan">
                            <h5 class="text-center">Assign Company Rep to location</h5>


                        </div>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-sm-4">


                    <a class="btn btn-lg text-center" href="CompanySetUp.aspx">
                        <div class="plan text-center">
                            <h5 class="text-center">View \ Modify Company</h5>


                        </div>

                    </a>

                </div>
                <div class="col-lg-4 col-sm-4 text-center">
                    <a class="btn btn-lg" href="#">
                        <div class="plan">
                            <h5 class="text-center">View \ Modify location</h5>


                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-4">
                    <a class="btn btn-lg" href="#">
                        <div class="plan">
                            <h5 class="text-center">View\ Modify Company Rep</h5>


                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <section style="background: white">
        <div class="container">
            <h2 class="text-center">Alerts Configuration</h2>

            <div class="col-lg-4 col-sm-4">


                <a class="btn btn-lg" href="#">
                    <div class="plan">
                        <h5 class="text-center">Configure Test Result Alerts</h5>


                    </div>

                </a>

            </div>
            <div class="col-lg-4 col-sm-4 text-center">
                <a class="btn btn-lg text-center" href="#">
                    <div class="plan text-center">
                        <h5 class="text-center">Configure Newsletter Alerts</h5>


                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-4">
                <a class="btn btn-lg" href="#">
                    <div class="plan">
                        <h5 class="text-center">Configure Appointment Alerts</h5>


                    </div>
                </a>
            </div>
        </div>
    </section>
</asp:Content>

