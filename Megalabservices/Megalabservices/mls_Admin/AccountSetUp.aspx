﻿<%@ Page Title="" Language="C#" MasterPageFile="mlsAdmin.master" AutoEventWireup="true" CodeFile="AccountSetUp.aspx.cs" Inherits="AccountSetUp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="body" style="background: #e8e8e8; padding: 20px; margin-top: 50px;">
   
        
        <section id="Individual" role="main" class="content-body">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <section class="panel form-wizard" id="w4">
                            <header class="panel-heading">
                                <div class="panel-actions">
                                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                                    <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                                </div>

                                <h2 class="panel-title">Individual Account Set up</h2>
                            </header>
                            <div class="panel-body">
                                <div class="wizard-progress wizard-progress-lg">
                                    <div class="steps-progress">
                                        <div class="progress-indicator"></div>
                                    </div>
                                    <ul class="wizard-steps">
                                        <li class="active">
                                            <a href="#w4-personal" data-toggle="tab"><span>1</span>Personal Info</a>
                                        </li>
                                        <li>
                                            <a href="#w4-profile" data-toggle="tab"><span>2</span>Login Info</a>
                                        </li>
                                        <li id="con">
                                            <a href="#w4-confirm" data-toggle="tab"><span>3</span>Confirmation</a>
                                        </li>
                                    </ul>
                                </div>

                                <form id="AccountForm" class="form-horizontal">
                                    <div class="tab-content">
                                        <div id="w4-personal" class="tab-pane active">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label " for="w4-first-name">First Name</label>
                                                    <div class="col-sm-9">
                                                        <input id="fn0" type="text" class="form-control t" name="first-name" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label " for="w4-last-name">Last Name</label>
                                                    <div class="col-sm-9">
                                                        <input id="ln0" type="text" class="form-control t" name="last-name" id="w4-last-name" required>
                                                    </div>
                                                </div>
                                               
                                                  <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="w4-first-name">SSN</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" name="ssn" id="sn0" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="email">Email </label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" name="email" id="em0" required >
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="w4-mobilephone">Phone </label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" name="mobilephone" id="mp0" required>
                                                    </div>
                                                </div>
                                             
                                            </div>
                                            <div class="col-lg-6">
                                               <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="w4-address-name">Address</label>
                                                    <div class="col-sm-9">
                                                        <input id="add0" type="text" class="form-control" name="address-name"  required>
                                                    </div>
                                                </div>

                                                   <div class="form-group">
                                                       <label class="col-sm-3 control-label" for="w4-address-name">ste\apt #</label>
                                                    <label class="col-sm-3 control-label" for="w4-address-name_2"></label>
                                                    <div class="col-sm-9">
                                                        <input id="add22" type="text" class="form-control" name="address-name_2">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="w4-city">City</label>
                                                    <div class="col-sm-9">
                                                        <input id="c0" type="text" class="form-control" name="city" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">State</label>
                                                    <div class="col-sm-9">
                                                        <select id="st0" name="states" class="form-control populate" title="Please select at least one state" required>
                                                            <option value="">Choose a State</option>
                                                            <option value="AK">Alaska</option>
                                                            <option value="AL">Alabama</option>
                                                            <option value="AR">Arkansas</option>
                                                            <option value="AZ">Arizona</option>
                                                            <option value="CA">California</option>
                                                            <option value="CO">Colorado</option>
                                                            <option value="CT">Connecticut</option>
                                                            <option value="DE">Delaware</option>
                                                            <option value="FL">Florida</option>
                                                            <option value="GA">Georgia</option>
                                                            <option value="HI">Hawaii</option>
                                                            <option value="IL">Illinois</option>
                                                            <option value="IA">Iowa</option>
                                                             <option value="ID">Idaho</option>
                                                            <option value="IN">Indiana</option>
                                                            <option value="KS">Kansas</option>
                                                            <option value="KY">Kentucky</option>
                                                            <option value="LA">Louisiana</option>
                                                            <option value="MN">Minnesota</option>
                                                            <option value="MS">Mississippi</option>
                                                            <option value="MO">Missouri</option>
                                                            <option value="MT">Montana</option>
                                                            <option value="ME">Maine</option>
                                                            <option value="MD">Maryland</option>
                                                            <option value="MA">Massachusetts</option>
                                                            <option value="MI">Michigan</option>
                                                            <option value="NH">New Hampshire</option>
                                                            <option value="NJ">New Jersey</option>
                                                            <option value="NY">New York</option>
                                                            <option value="NM">New Mexico</option>
                                                            <option value="NE">Nebraska</option>
                                                            <option value="NV">Nevada</option>
                                                            <option value="ND">North Dakota</option>
                                                            <option value="NC">North Carolina</option>
                                                            <option value="OK">Oklahoma</option>
                                                            <option value="OH">Ohio</option>
                                                            <option value="OR">Oregon</option>
                                                            <option value="PA">Pennsylvania</option>
                                                            <option value="RI">Rhode Island</option>
                                                            <option value="SC">South Carolina</option>
                                                            <option value="SD">South Dakota</option>
                                                            <option value="TX">Texas</option>
                                                            <option value="TN">Tennessee</option>
                                                            
                                                            <option value="UT">Utah</option>
                                                            <option value="VT">Vermont</option>
                                                            <option value="VA">Virginia</option>

                                                            <option value="WA">Washington</option>
                                                            <option value="WI">Wisconsin</option>
                                                            <option value="WV">West Virginia</option>
                                                            <option value="WY">Wyoming</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label " for="w4-zip">Zip Code</label>
                                                    <div class="col-sm-9">
                                                        <input id="zip0" type="text" class="form-control" name="Zip" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="w4-profile" class="tab-pane">
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label" for="w4-username">Username</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="username" id="us0" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label" for="w4-password">Password</label>
                                                <div class="col-sm-4">
                                                    <input type="password" class="form-control" name="password" id="pw0" required minlength="6">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label" for="w4-password">Confirm Password</label>
                                                <div class="col-sm-4">
                                                    <input type="password" class="form-control" name="password2" id="pw00" required minlength="6">
                                                </div>
                                            </div>

                                            <div class="form-group">
												<label for="inputSuccess" class="col-md-3 control-label">Send credentials to accounts?</label>
												<div class="col-md-6">
													<label class="checkbox-inline">
														<input type="checkbox" name="cred" value="1" id="cred"> (Email will be sent using the email address entered in the previous page)
													</label>
													
												</div>
											</div>
                                          <div class="form-group">
											<label class="col-sm-3 control-label">Account is for <span class="required" aria-required="true">*</span></label>
											<div class="col-sm-9">
												<div class="radio-custom radio-primary">
													<input type="radio" required="" value="4" name="rb" id="emp" aria-required="true">
													<label for="awesome">Employee</label>
												</div>
												<div class="radio-custom radio-primary">
													<input type="radio" value="2" name="rb" id="rep">
													<label for="very-awesome">Company Representative</label>
												</div>
												<div class="radio-custom radio-primary">
													<input type="radio" value="1" name="rb" id="admin">
													<label for="ultra-awesome">Admin</label>
												</div>
                                                <div class="radio-custom radio-primary">
													<input type="radio" value="3" name="rb" id="none">
													<label for="ultra-awesome">Individuals (walk ins)</label>
												</div>
												<label for="porto_is" class="error"></label>
											</div>
										</div>
                                          <div class="form-group hidden">
										<div class="col-md-12">
											<label>Choose Company <span class="required">*</span></label>
                                            <select id="comp" class="form-control" data-msg-required="Please choose company." name="company">
                                                <option value="">(Choose option)</option>
                                                <option value="1">Jiffy Lube</option>
                                                <option value="2">DOT</option>
                                                
                                            </select>
										</div>
									
										<div class="col-md-12">
											<label>Choose location <span class="required">*</span></label>
                                            <select id="loc" class="form-control" data-msg-required="Please location." name="location">
                                                <option value="">(Choose one)</option>

                                                <option value="11">123 w east street  gilbert az</option>
                                                <option value="12">345 e west street chandler az </option>
                                             
                                           </select>
										</div>
                                        </div>

                                          
                                        </div>

                                        <div id="w4-confirm" class="tab-pane">
                                            <h2 class="panel-title center">Please confirm your entries</h2>
                                            <hr />
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label class="col-sm-5 control-label" for="w4-first-name">First Name</label>
                                                    <div class="col-sm-7">
                                                        <label id="fn1" class="control-label"></label>

                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-5 control-label" for="w4-last-name">Last Name</label>
                                                    <div class="col-sm-7">
                                                        <label id="ln1" class="control-label"></label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-5 control-label" for="w4-address-name">Address</label>
                                                    <div class="col-sm-7">
                                                        <label id="add1" class="control-label"></label>

                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-5 control-label" for="w4-address-name_2">Address 2</label>
                                                    <div class="col-sm-7">
                                                        <label id="add2" class="control-label"></label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-5 control-label" for="w4-city">City</label>
                                                    <div class="col-sm-7">
                                                        <label id="c1" class="control-label"></label>

                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-5 control-label">State</label>
                                                    <div class="col-sm-7">
                                                        <label id="st1" class="control-label"></label>

                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-5 control-label">Zip</label>
                                                    <div class="col-sm-7">
                                                        <label id="zip1" class="control-label"></label>

                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label class="col-sm-5 control-label" for="w4-first-name">SSN</label>
                                                    <div class="col-sm-7">
                                                        <label id="sn1" class="control-label"></label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-5 control-label" for="w4-last-name">Email</label>
                                                    <div class="col-sm-7">
                                                        <label id="em1" class="control-label"></label>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label class="col-sm-5 control-label" for="w4-mobilephone">Phone</label>
                                                    <div class="col-sm-7">
                                                        <label id="mp1" class="control-label"></label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-5 control-label" for="w4-username">Username</label>
                                                    <div class="col-sm-4">
                                                        <label id="us1" class="control-label"></label>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-5 control-label" for="Role">Role ID</label>
                                                    <div class="col-sm-7">
                                                        <label id="rid1" class="control-label"></label>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-5 control-label" for="cred">Send Credentials?</label>
                                                    <div class="col-sm-7">
                                                        <label id="cred1" class="control-label"></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="panel-footer">
                                <ul class="pager">
                                    <li class="previous disabled">
                                        <a><i class="fa fa-angle-left"></i>Previous</a>
                                    </li>
                                    <li class="finish hidden pull-right">
                                        <a id="submitAccount">Finish</a>
                                    </li>
                                    <li class="next">
                                        <a>Next <i class="fa fa-angle-right"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </section>
                    </div>
                </div>

            </div>
        </section>


    </section>
</asp:Content>

