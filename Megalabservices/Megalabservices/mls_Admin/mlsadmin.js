﻿
jQuery(document).ready(function ($) {
 
    var pathname = window.location.pathname;
    var url = window.location.href;

    var o = url.indexOf("fileupload.aspx");

    if (o > 0) {

        var accountid = url.substring(url.indexOf('#') + 1)
        sessionStorage.setItem("accountid", accountid);
       

    }

    var oo = url.indexOf("AddLocationRep.aspx");

    if (oo > 0) {

        var companyid = url.substring(url.indexOf('#') + 1)
        sessionStorage.setItem("companyid", companyid);
       
        var finalopt

        if (companyid > 0) {



            var sendurl = null
            var a = {};
          
            a.CompanyID = companyid




            sendurl = 'AddLocationRep.aspx/getRep'


            $.ajax({
                type: "POST",
                url: sendurl,
                data: JSON.stringify(a),
                dataType: "json",
                contentType: 'application/json; charset=utf-8',
                success: function (msg) {
                    var res = JSON.parse(msg.d);

                    //$(".reps").html("");

                    finalopt = "<option value=\"\">Choose a Rep</option>"

                    //$(".reps").append("<option value=\"\">Choose a Rep</option>")

                    $.each($.parseJSON(msg.d), function () {

                        var opt = "<option value=\" " + this.AccountID + "\">" + this.FullName + "</option>"

                        finalopt = finalopt + opt;
                        //$(".reps").append(opt)


                    })
                
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(thrownError);
                }

            });

            sendurl = 'AddLocationRep.aspx/getLocations'

            $.ajax({
                type: "POST",
                url: sendurl,
                data: JSON.stringify(a),
                dataType: "json",
                contentType: 'application/json; charset=utf-8',
                success: function (msg) {
                    var res = JSON.parse(msg.d);

                    $.each($.parseJSON(msg.d), function () {

                        var loc = "<tr class=\"gradeC\"><td class=\"LocationID hidden\">" + this.LocationID + "</td><td id=\"SiteCode\">" + this.SiteCode + "</td><td id=\"LocationName\" >" + this.LocationName + "</td><td id=\"LocationAddress\" class=\"hidden-phone\">" + this.LocationAddress + "</td><td id=\"rep2\" class=\" hidden-phone \">  <select class=\"reps\" name=\"reps\"></select></td></tr>"
                        $("#sr").append(loc)
                       
                    })

                    $(".reps").append(finalopt)

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(thrownError);
                }

            });



         
        }

    }

    //
    var emp = url.indexOf("AddLocationEmployee.aspx");

    if (emp > 0) {

        var companyid = url.substring(url.indexOf('#') + 1)
        sessionStorage.setItem("companyid", companyid);


        if (companyid > 0) {



            var sendurl = null
            var a = {};
            sendurl = 'AddLocationEmployee.aspx/getLocations'
            a.CompanyID = companyid
            $.ajax({
                type: "POST",
                url: sendurl,
                data: JSON.stringify(a),
                dataType: "json",
                contentType: 'application/json; charset=utf-8',
                success: function (msg) {
                    var res = JSON.parse(msg.d);

                    $.each($.parseJSON(msg.d), function () {

                        var loc = "<tr class=\"gradeC\"><td class=\"LocationID hidden\">" + this.LocationID + "</td><td id=\"SiteCode\">" + this.SiteCode + "</td><td id=\"LocationName\" >" + this.LocationName + "</td><td id=\"LocationAddress\" class=\"hidden-phone\">" + this.LocationAddress + "</td><td id=\"emp2\" class=\" hidden-phone \">  <select class=\"emps\" name=\"emps\"></select></td></tr>"
                        $("#sr").append(loc)
                    })



                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(thrownError);
                }

            });

            sendurl = 'AddLocationEmployee.aspx/getEmp'


            $.ajax({
                type: "POST",
                url: sendurl,
                data: JSON.stringify(a),
                dataType: "json",
                contentType: 'application/json; charset=utf-8',
                success: function (msg) {
                    var res = JSON.parse(msg.d);

                    //$(".reps").html("");

                    $(".emps").append("<option value=\"\">Choose an Employee</option>")

                    $.each($.parseJSON(msg.d), function () {

                        var opt = "<option value=\" " + this.AccountID + "\">" + this.FullName + "</option>"

                        $(".emps").append(opt)


                    })
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(thrownError);
                }

            });



        }

    }


    var roleid
    $('input:radio[name="rb"]').change(function () {

       roleid = $('input[name=rb]:checked', '#AccountForm').val();
     
    });


    var cred
    $('input:checkbox[name="cred"]').change(function () {

        cred = $('input[name=cred]:checked', '#AccountForm').val();

    });

 
    $('ul.pager li.next').click(function () {

        //Individual
        $('#fn1').text($('#fn0').val());
        $('#ln1').text($('#ln0').val());
        $('#add1').text($('#add0').val());
        $('#add2').text($('#add22').val()); 
        $('#c1').text($('#c0').val());
        $('#st1').text($('#st0').val());
        $('#zip1').text($('#zip0').val());
        $('#sn1').text($('#sn0').val());
        $('#em1').text($('#em0').val());
        $('#hp1').text($('#hp0').val());
        $('#mp1').text($('#mp0').val());
        $('#us1').text($('#us0').val());
        $('#rid1').text(roleid);
        $('#cred1').text(cred);

        //Company
        $('#compname1').text($('#compname').val());
        $('#comptaxid1').text($('#comptaxid').val());
        $('#compemail1').text($('#compemail').val());
        $('#compphone1').text($('#compphone').val());
        $('#compwebsite1').text($('#compwebsite').val());
        $('#compaddress1').text($('#compaddress').val());
        $('#compaddress22').text($('#compaddress2').val());
        $('#compcity1').text($('#compcity').val());
        $('#compstate1').text($('#compstate').val());
        $('#compzip1').text($('#compzip').val());
   
        //location
        $('#locsitecode1').text($('#locsitecode').val());
        $('#locname1').text($('#locname').val());
        $('#locaddress1').text($('#locaddress').val());
        $('#loccity1').text($('#loccity').val());
        $('#locstate1').text($('#locstate').val());
        $('#loczip1').text($('#loczip').val());
        $('#locrep1').text($('#locrep').val());
 
        $('#locsitecode3').text($('#locsitecode2').val());
        $('#locname3').text($('#locname2').val());
        $('#locaddress3').text($('#locaddress2').val());
        $('#loccity3').text($('#loccity2').val());
        $('#locstate3').text($('#locstate2').val());
        $('#loczip3').text($('#loczip2').val());
        $('#locrep3').text($('#locrep2').val());
        
    });
   
    $('#AdminLogin').click(function (e) {
        e.preventDefault();
   
    var sendurl = null
    var a = {};


    sendurl = 'AdminLogin.aspx/login'

    a.username = $('#username').val()
    a.password = $('#password').val()
    a.role = 1
    
    $.ajax({
        type: "POST",
        url: sendurl,
        data: JSON.stringify(a),
        dataType: "json",
        contentType: 'application/json; charset=utf-8',
        success: function (msg) {
            var res = JSON.parse(msg.d);
            if (res > 0) {
               
                window.location.href = 'Portal.aspx';
            } else {
                alert("Invalid Login\Password")
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            alert(thrownError);
        }
    })
    });
    //

    $('#saverep').click(function (e) {
        e.preventDefault();

            var sendurl = null
            var a = {};


            sendurl = 'AddLocationRep.aspx/insertLocationRep'
            var repadded = 0
          

        $('.gradeC').each(function () {
      
            a.accountid = $(this).find('select > option:selected').val()
            a.locationid = $(this).find('.LocationID').text()
     

        $.ajax({
            type: "POST",
            url: sendurl,
            data: JSON.stringify(a),
            dataType: "json",
            contentType: 'application/json; charset=utf-8',
            success: function (msg) {
                var res = JSON.parse(msg.d);
                if (res > 0) {

                    repadded = repadded + 0
                } else {
                    repadded = repadded + 1
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            },
            complete: function () {
               
            }
                })

        });

        if (repadded > 0) {
            alert("Some of the selected reps have been added previously.. No action needed")
        }
        else {
            alert("Reps assigned")
        }
    });

    $('#saveemp').click(function (e) {
        e.preventDefault();

        var sendurl = null
        var a = {};


        sendurl = 'AddLocationEmployee.aspx/insertLocationEmployee'
        var repadded = 0


        $('.gradeC').each(function () {

            a.accountid = $(this).find('select > option:selected').val()
            a.locationid = $(this).find('.LocationID').text()


            $.ajax({
                type: "POST",
                url: sendurl,
                data: JSON.stringify(a),
                dataType: "json",
                contentType: 'application/json; charset=utf-8',
                success: function (msg) {
                    var res = JSON.parse(msg.d);
                    if (res > 0) {

                        repadded = repadded + 0
                    } else {
                        repadded = repadded + 1
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(thrownError);
                },
                complete: function () {

                }
            })

        });

        if (repadded > 0) {
            alert("Some of the selected employees have been added previously.. No action needed")
        }
        else {
            alert("Employees assigned")
        }
    });


});