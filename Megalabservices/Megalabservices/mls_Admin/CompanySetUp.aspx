﻿<%@ Page Title="" Language="C#" MasterPageFile="mlsAdmin.master" AutoEventWireup="true" CodeFile="CompanySetUp.aspx.cs" Inherits="CompanySetUp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="body" style="background: #e8e8e8; padding: 20px; margin-top: 50px;">


        <section id="Individual" role="main" class="content-body">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <section class="panel form-wizard" id="w4">
                            <header class="panel-heading">
                                <div class="panel-actions">
                                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                                    <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                                </div>

                                <h2 class="panel-title">Company Account Setup</h2>
                            </header>
                            <div class="panel-body">
                                <div class="wizard-progress wizard-progress-lg">
                                    <div class="steps-progress">
                                        <div class="progress-indicator"></div>
                                    </div>
                                    <ul class="wizard-steps">
                                        <li class="active">
                                            <a href="#w4-personal" data-toggle="tab"><span>1</span>Company Info</a>
                                        </li>
                                        <li>
                                            <a href="#w4-profile" data-toggle="tab"><span>2</span>Location Info </a>
                                        </li>
                                        <li id="con">
                                            <a href="#w4-confirm" data-toggle="tab"><span>3</span>Confirmation</a>
                                        </li>
                                    </ul>
                                </div>

                                <form id="CompanyForm" class="form-horizontal" novalidate="novalidate">
                                    <div class="tab-content">
                                        <div id="w4-personal" class="tab-pane active">
                                            <div class="col-md-12">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label " for="w4-first-name">Company Name</label>
                                                        <div class="col-sm-9">
                                                            <input id="compname" type="text" class="form-control t" name="comp-name" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label" for="w4-first-name">TaxID</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control" name="taxid" id="comptaxid">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label" for="w4-last-name">Email </label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control" name="compemail" id="compemail" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label" for="w4-companyphone">Phone </label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control" name="comapnyphone" id="compphone" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label " for="w4-website">Website</label>
                                                        <div class="col-sm-9">
                                                            <input id="compwebsite" type="text" class="form-control t" name="compwebsite">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label" for="w4-address-name">Address</label>
                                                        <div class="col-sm-9">
                                                            <input id="compaddress" type="text" class="form-control" name="compaddress" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label" for="w4-address-name_2"></label>
                                                        <div class="col-sm-9">
                                                            <input id="compadress22" type="text" class="form-control" name="compaddress22">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label" for="w4-city">City</label>
                                                        <div class="col-sm-9">
                                                            <input id="compcity" type="text" class="form-control" name="compcity" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">State</label>
                                                        <div class="col-sm-9">
                                                            <select id="compstate" name="states" class="form-control populate" title="Please select at least one state" required>
                                                                <option value="">Choose a State</option>
                                                                <option value="AK">Alaska</option>
                                                                <option value="AL">Alabama</option>
                                                                <option value="AR">Arkansas</option>
                                                                <option value="AZ">Arizona</option>
                                                                <option value="CA">California</option>
                                                                <option value="CO">Colorado</option>
                                                                <option value="CT">Connecticut</option>
                                                                <option value="DE">Delaware</option>
                                                                <option value="FL">Florida</option>
                                                                <option value="GA">Georgia</option>
                                                                <option value="HI">Hawaii</option>
                                                                <option value="IL">Illinois</option>
                                                                <option value="IA">Iowa</option>
                                                                <option value="ID">Idaho</option>
                                                                <option value="IN">Indiana</option>
                                                                <option value="KS">Kansas</option>
                                                                <option value="KY">Kentucky</option>
                                                                <option value="LA">Louisiana</option>
                                                                <option value="MN">Minnesota</option>
                                                                <option value="MS">Mississippi</option>
                                                                <option value="MO">Missouri</option>
                                                                <option value="MT">Montana</option>
                                                                <option value="ME">Maine</option>
                                                                <option value="MD">Maryland</option>
                                                                <option value="MA">Massachusetts</option>
                                                                <option value="MI">Michigan</option>
                                                                <option value="NH">New Hampshire</option>
                                                                <option value="NJ">New Jersey</option>
                                                                <option value="NY">New York</option>
                                                                <option value="NM">New Mexico</option>
                                                                <option value="NE">Nebraska</option>
                                                                <option value="NV">Nevada</option>
                                                                <option value="ND">North Dakota</option>
                                                                <option value="NC">North Carolina</option>
                                                                <option value="OK">Oklahoma</option>
                                                                <option value="OH">Ohio</option>
                                                                <option value="OR">Oregon</option>
                                                                <option value="PA">Pennsylvania</option>
                                                                <option value="RI">Rhode Island</option>
                                                                <option value="SC">South Carolina</option>
                                                                <option value="SD">South Dakota</option>
                                                                <option value="TX">Texas</option>
                                                                <option value="TN">Tennessee</option>

                                                                <option value="UT">Utah</option>
                                                                <option value="VT">Vermont</option>
                                                                <option value="VA">Virginia</option>

                                                                <option value="WA">Washington</option>
                                                                <option value="WI">Wisconsin</option>
                                                                <option value="WV">West Virginia</option>
                                                                <option value="WY">Wyoming</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label " for="w4-zip">Zip Code</label>
                                                        <div class="col-sm-9">
                                                            <input id="compzip" type="text" class="form-control" name="compZip" required>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div id="w4-profile" class="tab-pane">
                                            <div class="col-md-6">
                                                <h4 class="text-weight-bold">Location 1</h4>
                                                <hr />
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="w4-username">Site Code</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" name="sitecode" id="locsitecode">
                                                    </div>

                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="w4-location">Location Name</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" name="Locationname" id="locname">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="w4-address">Address</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" name="address" id="locaddress">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="w4-city">City</label>
                                                    <div class="col-sm-9">
                                                        <input id="loccity" type="text" class="form-control" name="loccity">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">State</label>
                                                    <div class="col-sm-9">
                                                        <select id="locstate" name="states" class="form-control populate" title="Please select at least one state">
                                                            <option value="">Choose a State</option>
                                                            <option value="AK">Alaska</option>
                                                            <option value="AL">Alabama</option>
                                                            <option value="AR">Arkansas</option>
                                                            <option value="AZ">Arizona</option>
                                                            <option value="CA">California</option>
                                                            <option value="CO">Colorado</option>
                                                            <option value="CT">Connecticut</option>
                                                            <option value="DE">Delaware</option>
                                                            <option value="FL">Florida</option>
                                                            <option value="GA">Georgia</option>
                                                            <option value="HI">Hawaii</option>
                                                            <option value="IL">Illinois</option>
                                                            <option value="IA">Iowa</option>
                                                            <option value="ID">Idaho</option>
                                                            <option value="IN">Indiana</option>
                                                            <option value="KS">Kansas</option>
                                                            <option value="KY">Kentucky</option>
                                                            <option value="LA">Louisiana</option>
                                                            <option value="MN">Minnesota</option>
                                                            <option value="MS">Mississippi</option>
                                                            <option value="MO">Missouri</option>
                                                            <option value="MT">Montana</option>
                                                            <option value="ME">Maine</option>
                                                            <option value="MD">Maryland</option>
                                                            <option value="MA">Massachusetts</option>
                                                            <option value="MI">Michigan</option>
                                                            <option value="NH">New Hampshire</option>
                                                            <option value="NJ">New Jersey</option>
                                                            <option value="NY">New York</option>
                                                            <option value="NM">New Mexico</option>
                                                            <option value="NE">Nebraska</option>
                                                            <option value="NV">Nevada</option>
                                                            <option value="ND">North Dakota</option>
                                                            <option value="NC">North Carolina</option>
                                                            <option value="OK">Oklahoma</option>
                                                            <option value="OH">Ohio</option>
                                                            <option value="OR">Oregon</option>
                                                            <option value="PA">Pennsylvania</option>
                                                            <option value="RI">Rhode Island</option>
                                                            <option value="SC">South Carolina</option>
                                                            <option value="SD">South Dakota</option>
                                                            <option value="TX">Texas</option>
                                                            <option value="TN">Tennessee</option>

                                                            <option value="UT">Utah</option>
                                                            <option value="VT">Vermont</option>
                                                            <option value="VA">Virginia</option>

                                                            <option value="WA">Washington</option>
                                                            <option value="WI">Wisconsin</option>
                                                            <option value="WV">West Virginia</option>
                                                            <option value="WY">Wyoming</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label " for="w4-zip">Zip Code</label>
                                                    <div class="col-sm-9">
                                                        <input id="loczip" type="text" class="form-control" name="Zip">
                                                    </div>
                                                </div>
                                                <%--   <div class="form-group">
                                                    <label class="col-sm-3 control-label " for="w4-zip">Company Rep<span class="required">*</span></label>
                                                    <div class="col-sm-9">
                                            <select id="locrep" class="form-control" data-msg-required="Please choose company rep." name="rep">
                                                <option value="">(Select from List)</option>
                                                <option value="1">Esi </option>
                                                <option value="2">Ayo</option>
                                                
                                            </select>
                                                       
                                                    </div>
                                                </div>      --%>
                                            </div>
                                            <div class="col-md-6">
                                                <h4 class="text-weight-bold">Location 2</h4>
                                                <hr />
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="w4-sitecode">Site Code</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" name="sitecode2" id="locsitecode2">
                                                    </div>

                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="w4-location">Location Name</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" name="Locationname" id="locname2">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="w4-address">Address</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" name="address2" id="locaddress2">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="w4-city">City</label>
                                                    <div class="col-sm-9">
                                                        <input id="loccity2" type="text" class="form-control" name="loccity2">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">State</label>
                                                    <div class="col-sm-9">
                                                        <select id="locstate2" name="states" class="form-control populate" title="Please select at least one state">
                                                            <option value="">Choose a State</option>
                                                            <option value="AK">Alaska</option>
                                                            <option value="AL">Alabama</option>
                                                            <option value="AR">Arkansas</option>
                                                            <option value="AZ">Arizona</option>
                                                            <option value="CA">California</option>
                                                            <option value="CO">Colorado</option>
                                                            <option value="CT">Connecticut</option>
                                                            <option value="DE">Delaware</option>
                                                            <option value="FL">Florida</option>
                                                            <option value="GA">Georgia</option>
                                                            <option value="HI">Hawaii</option>
                                                            <option value="IL">Illinois</option>
                                                            <option value="IA">Iowa</option>
                                                            <option value="ID">Idaho</option>
                                                            <option value="IN">Indiana</option>
                                                            <option value="KS">Kansas</option>
                                                            <option value="KY">Kentucky</option>
                                                            <option value="LA">Louisiana</option>
                                                            <option value="MN">Minnesota</option>
                                                            <option value="MS">Mississippi</option>
                                                            <option value="MO">Missouri</option>
                                                            <option value="MT">Montana</option>
                                                            <option value="ME">Maine</option>
                                                            <option value="MD">Maryland</option>
                                                            <option value="MA">Massachusetts</option>
                                                            <option value="MI">Michigan</option>
                                                            <option value="NH">New Hampshire</option>
                                                            <option value="NJ">New Jersey</option>
                                                            <option value="NY">New York</option>
                                                            <option value="NM">New Mexico</option>
                                                            <option value="NE">Nebraska</option>
                                                            <option value="NV">Nevada</option>
                                                            <option value="ND">North Dakota</option>
                                                            <option value="NC">North Carolina</option>
                                                            <option value="OK">Oklahoma</option>
                                                            <option value="OH">Ohio</option>
                                                            <option value="OR">Oregon</option>
                                                            <option value="PA">Pennsylvania</option>
                                                            <option value="RI">Rhode Island</option>
                                                            <option value="SC">South Carolina</option>
                                                            <option value="SD">South Dakota</option>
                                                            <option value="TX">Texas</option>
                                                            <option value="TN">Tennessee</option>

                                                            <option value="UT">Utah</option>
                                                            <option value="VT">Vermont</option>
                                                            <option value="VA">Virginia</option>

                                                            <option value="WA">Washington</option>
                                                            <option value="WI">Wisconsin</option>
                                                            <option value="WV">West Virginia</option>
                                                            <option value="WY">Wyoming</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label " for="w4-zip">Zip Code</label>
                                                    <div class="col-sm-9">
                                                        <input id="loczip2" type="text" class="form-control" name="Zip">
                                                    </div>
                                                </div>
                                                <%--     <div class="form-group">
                                                    <label class="col-sm-3 control-label " for="w4-zip">Company Rep<span class="required">*</span></label>
                                                    <div class="col-sm-9">
                                            <select id="locrep2" class="form-control" data-msg-required="Please choose company rep." name="rep">
                                                <option value="">(Select from List)</option>
                                                <option value="1">Esi </option>
                                                <option value="2">Ayo</option>
                                                
                                            </select>
                                                       
                                                    </div>
                                                </div>    --%>
                                            </div>
                                            <div class="clearfix">
                                                <h4 class="text-center" style="margin-top: 40px !important">To add more locations, please navigate to the portal and select "Add locations"</h4>
                                            </div>
                                        </div>

                                        <div id="w4-confirm" class="tab-pane">
                                            <h2 class="panel-title center">Please confirm your entries</h2>
                                            <hr />
                                            <div class="col-lg-4">
                                                <h5>Company Info</h5>
                                                <hr />
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="w4-company-name">Company</label>
                                                    <div class="col-sm-9">
                                                        <label id="compname1" class="control-label"></label>

                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="w4-taxid">TaxID</label>
                                                    <div class="col-sm-9">
                                                        <label id="comptaxid1" class="control-label"></label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="w4-address-name">Email</label>
                                                    <div class="col-sm-9">
                                                        <label id="compemail1" class="control-label"></label>

                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="w4-phone">Phone</label>
                                                    <div class="col-sm-9">
                                                        <label id="compphone1" class="control-label"></label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="w4-city">Website</label>
                                                    <div class="col-sm-9">
                                                        <label id="compwebsite1" class="control-label"></label>

                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="w4-last-name">Address</label>
                                                    <div class="col-sm-9">
                                                        <label id="compaddress1" class="control-label"></label>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="w4-mobilephone">City</label>
                                                    <div class="col-sm-9">
                                                        <label id="compcity1" class="control-label"></label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="w4-username">State</label>
                                                    <div class="col-sm-4">
                                                        <label id="compstate1" class="control-label"></label>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Zip</label>
                                                    <div class="col-sm-9">
                                                        <label id="compzip1" class="control-label"></label>

                                                    </div>
                                                </div>


                                            </div>
                                            <div class="col-lg-4">
                                                <h5>Location 1</h5>
                                                <hr />
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="w4-first-name">Site Code</label>
                                                    <div class="col-sm-9">
                                                        <label id="locsitecode1" class="control-label"></label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="w4-first-name">Location</label>
                                                    <div class="col-sm-9">
                                                        <label id="locname1" class="control-label"></label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="w4-last-name">Address</label>
                                                    <div class="col-sm-9">
                                                        <label id="locaddress1" class="control-label"></label>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="w4-mobilephone">City</label>
                                                    <div class="col-sm-9">
                                                        <label id="loccity1" class="control-label"></label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="w4-username">State</label>
                                                    <div class="col-sm-4">
                                                        <label id="locstate1" class="control-label"></label>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Zip</label>
                                                    <div class="col-sm-9">
                                                        <label id="loczip1" class="control-label"></label>

                                                    </div>
                                                </div>

                                                <%--                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="rep">Rep</label>
                                                    <div class="col-sm-9">
                                                        <label id="locrep1" class="control-label"></label>
                                                    </div>
                                                </div>--%>
                                            </div>
                                            <div class="col-lg-4">
                                                <h5>Location 2</h5>
                                                <hr />
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="w4-first-name">Site Code</label>
                                                    <div class="col-sm-9">
                                                        <label id="locsitecode3" class="control-label"></label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="w4-first-name">Location</label>
                                                    <div class="col-sm-9">
                                                        <label id="locname3" class="control-label"></label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="w4-last-name">Address</label>
                                                    <div class="col-sm-9">
                                                        <label id="locaddress3" class="control-label"></label>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="w4-mobilephone">City</label>
                                                    <div class="col-sm-9">
                                                        <label id="loccity3" class="control-label"></label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="w4-username">State</label>
                                                    <div class="col-sm-4">
                                                        <label id="locstate3" class="control-label"></label>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Zip</label>
                                                    <div class="col-sm-9">
                                                        <label id="loczip3" class="control-label"></label>

                                                    </div>
                                                </div>

                                                <%--      <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="rep">Rep</label>
                                                    <div class="col-sm-9">
                                                        <label id="locrep3" class="control-label"></label>
                                                    </div>
                                                </div>--%>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="panel-footer">
                                <ul class="pager">
                                    <li class="previous disabled">
                                        <a><i class="fa fa-angle-left"></i>Previous</a>
                                    </li>
                                    <li class="finish hidden pull-right">
                                        <a id="submitAccount">Finish</a>
                                    </li>
                                    <li class="next">
                                        <a>Next <i class="fa fa-angle-right"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </section>
                    </div>
                </div>

            </div>
        </section>


    </section>
</asp:Content>

