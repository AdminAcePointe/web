﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AdminLogin.aspx.cs" Inherits="Admin" %>

<!DOCTYPE html>
<html>
    <head>
	<!-- Basic -->
		<meta charset="UTF-8">

		<title>Megalab Services | Admin Portal</title>
		<meta name="keywords" content="MLS Admin " />
		<meta name="description" content="Megalab Services- Admin Portal<">
		<meta name="author" content="acepointe.com">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.css" />

		<link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.css" />
		<link rel="stylesheet" href="assets/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css" />

		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.css" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="assets/stylesheets/theme.css" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="assets/stylesheets/skins/default.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="assets/stylesheets/theme-custom.css">

		<!-- Head Libs -->
		<script src="assets/vendor/modernizr/modernizr.js"></script>
    </head>
<body class="fixed">
    <section class="body">


<section style="clear:both; margin-top: 150px;">
         <div class="wrapper" >
             <div style="margin-bottom:30px;">
        <img class="center-block" src="assets/images/mls/logo_mls.png" alt="Mls Admin"/>
                 </div>
          
        <div class="row">
						<div class="col-md-5 col-md-offset-3">
							<form id="summary-form"  class="form-horizontal">
								<section class="panel">
									<div class="panel-body" style="background:transparent;border:none;">
										<div class="validation-message">
											<ul></ul>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label">User Name <span class="required">*</span></label>
											<div class="col-sm-9">
												<input id="username" type="text" name="fullname" class="form-control" title="Plase enter User name." required/>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label">Password<span class="required">*</span></label>
											<div class="col-sm-9">
												<input id="password" type="password" name="password" class="form-control" title="Please enter password."  required/>
											</div>
										</div>
										
									</div>
									<footer class="panel-footer"  style="background:transparent">
										<div class="row">
											<div class="col-sm-8 col-sm-offset-4">
												<button id= "AdminLogin" class="btn btn-primary">Login</button>
												<a href="ResetPassword.aspx">Forgot Password</a>
											</div>
										</div>
									</footer>
								</section>
							</form>
						</div>
					
					</div>
                </div>
    </section>
</section>

    <!-- Vendor -->
		<script src="assets/vendor/jquery/jquery.js"></script>
		<script src="assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="assets/vendor/bootstrap/js/bootstrap.js"></script>
		<script src="assets/vendor/nanoscroller/nanoscroller.js"></script>
		<script src="assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="assets/vendor/magnific-popup/jquery.magnific-popup.js"></script>
		<script src="assets/vendor/jquery-placeholder/jquery-placeholder.js"></script>
		
		<!-- Specific Page Vendor -->
		<script src="assets/vendor/jquery-validation/jquery.validate.js"></script>
		<script src="assets/vendor/select2/js/select2.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="assets/javascripts/theme.js"></script>

        <script src="mlsadmin.js"></script>
		
		<!-- Theme Custom -->
		<script src="assets/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="assets/javascripts/theme.init.js"></script>

		<!-- Examples -->
		<%--<script src="assets/javascripts/forms/examples.validation.js"></script>--%>
</body>
</html>
