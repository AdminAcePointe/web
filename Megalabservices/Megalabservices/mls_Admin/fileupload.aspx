﻿<%@ Page Title="" Language="C#" MasterPageFile="megalabs.master" AutoEventWireup="true" CodeFile="fileupload.aspx.cs" Inherits="fileupload" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <section class="body">
        <section style="margin-top:80px">
  <section class="panel">
		<header class="panel-heading" style="background:#ecedf0">
			<h3 class="panel-title" style="color:#0077b3; text-align:center">Test Information & Results Upload</h3>
            <a class="text-center" href="Portal.aspx">Back to Portal </a>
                    <span>|</span>
                    <a class="text-center" href="TestResultConfig.aspx">Back to Search</a>
		</header>
        <div class="panel-body" style="background:whitesmoke">
				
             <div class="container">

					<div class="row">
						<div class="col-md-8 col-md-offset-2" >

							

							<form id="uForm"  method="POST">
								
                                <div class="row" style="padding-top:20px">
                                    	<div class="form-group">
										<div class="col-md-6">
											<label>Choose Test Category <span class="required">*</span></label>
                                            <select id="testcat" class="form-control" data-msg-required="Please choose test category." name="testcat">
                                                <option value="">(Choose option)</option>
                                                <option value="1">Alcohol Test</option>
                                                <option value="2">Background Check</option>
                                                <option value="3">DNA Test</option>
                                                <option value="4">Drug Test</option>
                                                <option value="5">Immigration Test</option>
                                                <option value="6">Physical Examination</option>
                                                <option value="7">Physical Examination  + Drug Test</option>
                                                 
                                            </select>
										</div>
									
										<div class="col-md-6">
											<label>Choose Test <span class="required">*</span></label>
                                            <select id="test" class="form-control" data-msg-required="Please choose test type." name="test">
                                                <option value="">(Choose one)</option>

                                                <option value="11">Breath Alcohol Test</option>
                                                <option value="12">EtG Hair Alcohol Test</option>
                                                <option value="13">EtG Urine Alcohol Test</option>
                                                <option value="14">Urine Alcohol Test</option>
                                                <option value="15">None</option>

                                                <option value="21">Background Check</option>
                                                <option value="22">None</option>

                                                <option value="31">Ancestry DNA Test</option>
                                                <option value="32">Child Safety ID DNA</option>
                                                <option value="33">Infidelity DNA</option>
                                                <option value="34">Informational DNA Test</option>
                                                <option value="35">Legal Relationship DNA Test</option>
                                                <option value="36">PharmacoGenetic DNA</option>
                                                <option value="37">None</option>

                                                <option value="41">Athletics/Academics Drug Testing</option>
                                                <option value="42">Court Order</option>
                                                <option value="43">DOT</option>
                                           
                                                <option value="44">Instant</option>
                                                     <option value="45">On-site</option>
                                                <option value="46">Informational/Investigative Drug Testing</option>
                                                <option value="47">Work-place</option>
                                                <option value="428">Hair Test</option>
                                                <option value="429">Lab based</option>
                                                <option value="48">None</option>
                                                <option value="49">Pre-Employment</option>
                                                <option value="411">Drug Test and Pre-Employment Physical</option>
                                                <option value="412">5-Panel DOT</option>
                                                <option value="413">Non-DOT</option>
                                                <option value="414">5-Panel</option>
                                                <option value="415">5-Panel + Oxy</option>
                                                <option value="416">9-Panel</option>
                                                <option value="417">10-Panel</option>
                                                <option value="418">K2 + Bath Salts</option>
                                                <option value="419">Bath Salts</option>
                                                <option value="421">K2</option>
                                                <option value="422">5-Panel + Oxy + K2 + Bath Salt</option>                                                                                                
                                                <option value="423">Random Test</option>
                                                <option value="424">Post-Accident Test</option>
                                                <option value="425">Reasonable Suspicion Test</option>
                                                <option value="426">Re-Test</option>
                                                <option value="427">Return to Duty Test</option>
                                                <option value="430">MRO Confirmation</option>

                                                <option value="51">Immigration Test</option>
                                                <option value="52">None</option>

                                               <option value="61">Random Test</option>
                                               <option value="62">Post-Accident Test</option>
                                               <option value="63">Reasonable Suspicion Test</option>
                                                <option value="64">Re-Test</option>
                                                <option value="65">Return to Duty Test</option>
                                                <option value="66">Pre-Employment</option>
                                                <option value="67">Drug Test and Pre-Employment Physical</option>

                                                 <option value="71">Random Test</option>
                                                <option value="72">Post-Accident Test</option>
                                                <option value="73">Reasonable Suspicion Test</option>
                                                <option value="74">Re-Test</option>
                                                <option value="75">Return to Duty Test</option>
                                                <option value="76">Pre-Employment</option>
                                                <option value="77">Drug Test and Pre-Employment Physical</option>

                   
                                           </select>
										</div>
                                        </div>
									
								</div>
                                <div class="row" style="padding-top:20px">
                                 <div class="form-group">
										<div class="col-md-6">
											<label>Choose Test Date</label>
                                            <div class='input-group date' id="ttdate" >
                                                <input id="aptdate" type='text' class="form-control" data-msg-required="Please choose date." name="aptdate"/>
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
										</div>
									<div class="col-md-6">
											<label>Choose Result <span class="required">*</span></label>
                                            <select id="Result" class="form-control" data-msg-required="Please choose result." name="Result">
                                                <option value="">(Choose option)</option>
                                                <option value="1">Positive</option>
                                                <option value="2">Negative</option>
                                                <option value="3">Pass</option>
                                                <option value="4">Fail</option>
                                                <option value="5">N\A</option>
                                                <option value="6">Negative Dilute</option>
                                                <option value="10">Positive Dilute</option>
                                                <option value="7">Dilute</option>
                                                <option value="8">Neutral</option>
                                                <option value="9">Invalid Result</option>
                                                
                                        </select>
										</div>
										
                                        </div>
                                 </div>
								<div class="row" style="padding-top:20px">
									<div class="form-group">
							
                                    <div class="col-md-6">
												<label> Test Results File Upload</label>
												<div class="col-md-12" style="padding-left:0px;">
													<div class="fileupload fileupload-new" data-provides="fileupload">
														<div class="input-append">
															<div class="uneditable-input" style="width:160px !important"> 
																<i class="fa fa-file fileupload-exists"></i>
																<span class="fileupload-preview"></span>
															</div>
															<span class="btn btn-default btn-file">
																<span class="fileupload-exists">Change</span>
																<span class="fileupload-new">Browse</span>
																<input id="testfilename" type="file" />
															</span>
															<a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
														</div>
													</div>
												</div>
											</div>
                                    <div class="col-md-6">
                                        <label>CCF File Upload</label>
												<div class="col-md-12" style="padding-left:0px;">
													<div class="fileupload fileupload-new" data-provides="fileupload">
														<div class="input-append">
															<div class="uneditable-input" style="width:160px !important">
																<i class="fa fa-file fileupload-exists"></i>
																<span class="fileupload-preview"></span>
															</div>
															<span class="btn btn-default btn-file">
																<span class="fileupload-exists">Change</span>
																<span class="fileupload-new">Browse</span>
																<input id="ccffilename" type="file"/>
															</span>
															<a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
														</div>
													</div>
												</div>

                                    </div>
                                    </div>
								
								</div>
                                <div class="row" style="padding-top:20px">
                                    <div class="col-md-12">
                                    <div class="form-group">
												
												<div class="col-md-12 text-center">
                                                    
													<label class="checkbox-inline">
														<input type="checkbox" name="repcred" value="1" id="notifyrep"> Notify Company Rep of test result availability (Rep Email on file will be used)
													</label>
													
												</div>
											</div>
                                        </div>
                                </div>
                                <div class="row" style="padding-top:20px">
                                    <div class="col-md-12">
                                    <div class="form-group">
												
												<div class="col-md-12 text-center">
                                                    
													<label class="checkbox-inline">
														<input type="checkbox" name="cred" value="1" id="notify"> Notify client of test result availability (Email on file will be used)
													</label>
													
												</div>
											</div>
                                        </div>
                                </div>

                                     <div class="row" style="padding-top:20px">
                                    <div class="col-md-12">
                                    <div class="form-group">
												
												<div class="col-md-12 text-center">
                                                    
													<label class="checkbox-inline">
														<input type="checkbox" name="cred" value="1" id="Checkbox3"> Choose representative (First and last Name)
													</label>
													
												</div>
											</div>
                                        </div>
                                </div>
                             
								<div class="row" style="padding-top:30px">
									<div class="col-md-12 text-center">
										<input id="submitupload" type="submit" value="Upload test results" class="btn btn-primary btn-sm mb-xlg" data-loading-text="Loading...">
									</div>
								</div>
                                

							</form>

                            <div class="alert alert-success hidden" id="contactSuccess">
								<strong>Success!</strong> Your test results  have been set up successfully. 
							</div>

							<div class="alert alert-danger hidden" id="contactError">
								<strong>Error!</strong> There was an error uploading your test result. Please try again later or call our office at (866) 261-7129 or (571) 285-1857 for scheduling assistance.
							</div>
						</div>
						

					</div>

				</div>
					</div>
	\
	</section>



</section>
        </section>
    
   
</asp:Content>

