﻿<%@ Page Title="" Language="C#" MasterPageFile="mlsplain.master" AutoEventWireup="true" CodeFile="oldfileupload.aspx.cs" Inherits="oldfileupload" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <section class="body">
        <section style="margin-top:80px">
  <section class="panel">
		<header class="panel-heading">
			<h3 class="panel-title" style="color:#0077b3; text-align:center">Test Information & Results Upload</h3>
		</header>
        <div class="panel-body">
		<div class="container">
						<div class="col-md-10 col-md-offset-1">
							<form id="form" class="form-horizonal" method="post">
                                <div style="background-color:white;margin:10px">
                                <div class="row">
                                    	<div class="form-group">
										<div class="col-md-6">
											<label>Choose Test Category <span class="required">*</span></label>
                                            <select id="testcat" class="form-control" required name="testcat">
                                                <option value="">(Choose one)</option>
                                                <option value="1">Alcohol Test</option>
                                                <option value="2">Background Check</option>
                                                <option value="3">DNA Test</option>
                                                <option value="4">Drug Test</option>
                                                <option value="5">Immigration Test</option>
                                            </select>
										</div>
									
										<div class="col-md-6">
											<label>Choose Test <span class="required">*</span></label>
                                            <select id="test" class="form-control" data-msg-required="Please choose test type." name="test">
                                                <option value="">(Choose one)</option>

                                                <option value="11">Breath Alcohol Test</option>
                                                <option value="12">EtG Hair Alcohol Test</option>
                                                <option value="13">EtG Urine Alcohol Test</option>
                                                <option value="14">Urine Alcohol Test</option>
                                                <option value="15">None</option>

                                                <option value="21">Background Check</option>
                                                <option value="22">None</option>

                                                <option value="31">Ancestry DNA Test</option>
                                                <option value="32">Child Safety ID DNA</option>
                                                <option value="33">Infidelity DNA</option>
                                                <option value="34">Informational DNA Test</option>
                                                <option value="35">Legal Relationship DNA Test</option>
                                                <option value="36">PharmacoGenetic DNA</option>
                                                <option value="37">None</option>

                                                <option value="41">Athletics/Academics Drug Testing</option>
                                                <option value="42">Court Order</option>
                                                <option value="43">DOT</option>
                                                <option value="44">On-site</option>
                                                <option value="45">Instant</option>
                                                <option value="46">Informational/Investigative Drug Testing</option>
                                                <option value="47">Work-place</option>
                                                <option value="48">None</option>

                                                <option value="51">Immigration Test</option>
                                                <option value="52">None</option>
                                           </select>
										</div>
                                        </div>
									
								</div>
                                <div class="row">
                                 <div class="form-group">
										<div class="col-md-6">
											<label>Enter Test Date (MM/DD/YYYY)<span class="required">*</span></label>
                                            <div class='input-group date' id="appointmentdate" >
                                                <input id="testdate" type='text' class="form-control" required name="testdate"/>
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
										</div>
									
										
                                        </div>
                                 </div>
                                <br>
								<div class="row">
									<div class="form-group">
										<div class="col-md-12">
											<label>Results</label>
											<textarea maxlength="500" rows="4" class="form-control" name="result" id="result"></textarea>
										</div>
									</div>
								</div>
                               <br>
                                <div class="row">
                                <div class="form-group">
                                    <div class="col-md-6">
												<label> Test Results File Upload</label>
												<div class="col-md-12">
													<div class="fileupload fileupload-new" data-provides="fileupload">
														<div class="input-append">
															<div class="uneditable-input">
																<i class="fa fa-file fileupload-exists"></i>
																<span class="fileupload-preview"></span>
															</div>
															<span class="btn btn-default btn-file">
																<span class="fileupload-exists">Change</span>
																<span class="fileupload-new">Browse</span>
																<input id="testfilename" type="file" />
															</span>
															<a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
														</div>
													</div>
												</div>
											</div>
                                    <div class="col-md-6">
                                        <label>CCF File Upload</label>
												<div class="col-md-12">
													<div class="fileupload fileupload-new" data-provides="fileupload">
														<div class="input-append">
															<div class="uneditable-input">
																<i class="fa fa-file fileupload-exists"></i>
																<span class="fileupload-preview"></span>
															</div>
															<span class="btn btn-default btn-file">
																<span class="fileupload-exists">Change</span>
																<span class="fileupload-new">Browse</span>
																<input id="ccffilename" type="file"/>
															</span>
															<a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
														</div>
													</div>
												</div>

                                    </div>
                                    </div>
                                   </div> 
                              </div>
                                <br />
								<div class="row">
									<div class="col-md-6">
										<input id="upload" type="button" value="Upload Test Results" class="btn btn-primary btn-sm mb-xlg">
									</div>
								</div>    
                                                      
							</form>

                            <div class="alert alert-success hidden" id="contactSuccess">
								<strong>Success!</strong> Test information successfully uploaded
							</div>

							<div class="alert alert-danger hidden" id="contactError">
								<strong>Error!</strong> There was an error uploading the information. Please try again.
							</div>
						</div>
				</div>		

					</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-md-12 " style="text-align:center">
					<a class="text-center" href="Portal.aspx">Back to Portal< </a>
                    
                    <a class="text-center" href="TestResultConfig.aspx">>Back to Search</a>

				</div>
			</div>
		</footer>
	</section>



</section>
        </section>

</asp:Content>

