﻿using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Mail;
using System.Configuration;
using System;

public partial class Admin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod]
    public static int login(string username, string password, string role)
    {

        //string enpwd =  encryptpw(pwd);



        string dbpwd = String.Empty;

        int valid = 0;
        try
        {

            string cs = ConfigurationManager.ConnectionStrings["eklipseconsult"].ConnectionString;
            using (SqlConnection con = new SqlConnection(cs))
            {
                using (SqlCommand cmd = new SqlCommand("dbo.sp_ValidateCredentials", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@username", username);
                    cmd.Parameters.AddWithValue("@roleid", role);
                    con.Open();
                    dbpwd = (cmd.ExecuteScalar().ToString());
                    con.Close();
                    if (password == decryptpw(dbpwd))
                    {
                        valid = 1;
                    }
                    else
                    {
                        valid = 0;
                    }

                }

            }
        }
        catch
        {
            valid = 0;
        }

        return valid;
    }

    private static string encryptpw(string pwd)
    {
        byte[] bytes = System.Text.Encoding.Unicode.GetBytes(pwd);
        string encrytpwd = Convert.ToBase64String(bytes);
        return encrytpwd;
    }

    private static string decryptpw(string dbpwd)
    {
        byte[] bytes = Convert.FromBase64String(dbpwd);
        string decrytpwd = System.Text.Encoding.Unicode.GetString(bytes);
        return decrytpwd;
    }
}