﻿<%@ Page Title="" Language="C#" MasterPageFile="megalabs.master" AutoEventWireup="true" CodeFile="Contact.aspx.cs" Inherits="Contact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title>Contact Us | Mega Lab Services</title>	
     <script src="js/json2.js"></script>
    <script src="js/json2.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div class="container">

					<div class="row">
						<div class="col-md-7">

							

							<h2 class="mb-sm mt-sm"><strong>Contact</strong> Us</h2>
							<form id="contactForm"  method="POST">
								<div class="row">
									<div class="form-group">
										<div class="col-md-6">
											<label> First Name <span class="required">*</span></label>
											<input type="text" value="" data-msg-required="Please enter your first name." maxlength="100" class="form-control" name="fname" id="fname" >
										</div>
										<div class="col-md-6">
											<label> Last Name <span class="required">*</span></label>
											<input type="text" value="" data-msg-required="Please enter your last name." maxlength="100" class="form-control" name="lname" id="lname" >
										</div>
									</div>
								</div>
								<div class="row">
                                    	<div class="form-group">
										<div class="col-md-6">
											<label>Phone number</label>
											<input type="text" value=""  maxlength="100" class="form-control" name="phone" id="phone">
										</div>
									
										<div class="col-md-6">
											<label>Email Address <span class="required">*</span></label>
											<input type="text" value="" data-msg-required="Please enter your email address." maxlength="100" class="form-control" name="email" id="email" >
										</div>
                                        </div>
									
								</div>
								<div class="row">
									<div class="form-group">
										<div class="col-md-12">
											<label>Message<span class="required">*</span></label>
											<textarea maxlength="5000" data-msg-required="Please enter your message." rows="10" class="form-control" name="message" id="message"></textarea>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<input id="submitcontact" type="submit" value="Send Message" class="btn btn-primary btn-lg mb-xlg" data-loading-text="Loading...">
									</div>
								</div>
							</form>

                            <div class="alert alert-success hidden" id="contactSuccess">
								<strong>Success!</strong> Your message has been successfully sent.
							</div>

							<div class="alert alert-danger hidden" id="contactError">
								<strong>Error!</strong> There was an error sending your message.
							</div>
						</div>
						<div class="col-md-4">

							

							<hr>

							<h4 class="heading-primary"> <strong> Virginia Office</strong> </h4>
							<ul class="list list-icons list-icons-style-3 mt-xlg">
								<li><i class="fa fa-map-marker"></i> <strong>Address:</strong> 1950 Optiz Boulevard<br />        Woodbridge, Virginia 22191 </li>
								<li><i class="fa fa-phone"></i> <strong>Phone:</strong> (866) 261-7129 / (571) 285-1857</li>
                                <li><i class="fa fa-fax"></i> <strong>Fax:</strong>  (703) 986-3139  </li>
								<li><i class="fa fa-envelope"></i> <strong>Email:</strong> <a href="info@megalabservices.com">info@megalabservices.com</a></li>
							</ul>
                            <h6  style="margin-bottom:0px" class="heading-primary"><strong> Business Hours</strong></h6>
                            <ul  class="list list-icons list-dark mt-xlg" style="margin-top:0px !important" >
								<li><i class="fa fa-clock-o"></i> Monday - Friday 9am to 5pm</li>
								<li><i class="fa fa-clock-o"></i> Saturday - By Appointment Only</li>
								<li><i class="fa fa-clock-o"></i> Sunday - Closed</li>
							</ul>
                            <hr>

							<h4 class="heading-primary"> <strong> California Office </strong> </h4>
							<ul class="list list-icons list-icons-style-3 mt-xlg">
								<li><i class="fa fa-map-marker"></i> <strong>Address:</strong>  1321 Evans Avenue Ste. E, <br />        San Francisco, California 94124 </li>
								<li><i class="fa fa-phone"></i> <strong>Phone:</strong> (415) 872 9284</li>
                                <li><i class="fa fa-fax"></i> <strong>Fax:</strong> (415) 647 7316 </li>
								<li><i class="fa fa-envelope"></i> <strong>Email:</strong> <a href="hsa@megalabservices.com">hsa@megalabservices.com</a></li>
							</ul>

							<h6 style="margin-bottom:0px" class="heading-primary"><strong> Business Hours</strong></h6>
							<ul class="list list-icons list-dark mt-xlg" style="margin-top:0px !important" >
								<li><i class="fa fa-clock-o"></i> Monday - Friday 8.30am to 5pm</li>
								<li><i class="fa fa-clock-o"></i> Saturday - By Appointment Only</li>
								<li><i class="fa fa-clock-o"></i> Sunday - Closed</li>
							</ul>

						</div>

					</div>

				</div>
    <section class="call-to-action call-to-action-default with-button-arrow call-to-action-in-footer">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="">
								<h3 style=" text-align:center"> Mega Lab Services, your  <strong> # 1 DNA, Drug & Alcohol testing center</strong></h3>
							</div>
                            <div class="clearfix"></div>
							<div class="center" style="padding:30px">
								<a class="btn btn-lg btn-primary" href="Appointment.aspx">Schedule Appointment</a>
							
                               
						</div>
					</div>
				</div>
                    </div>
			</section>
</asp:Content>

