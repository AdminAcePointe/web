﻿<%@ Page Title="" Language="C#" MasterPageFile="megalabs.master" AutoEventWireup="true" CodeFile="aboutus.aspx.cs" Inherits="aboutus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>About Us | Mega Lab Services</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
 
    <section class="parallax section section-text-light section-parallax section-center mt-none" data-stellar-background-ratio="0.5" 
        style="background-image: url(img/MLS/about.jpg);">
					<div class="container test" >
						<h1 class="test"style="padding:100px; text-align:center;"><strong>About Us</strong></h1>
					</div>
				</section>

    <section>
        <div class="container">
            <div class="row" style="color: white">
                <h2 class="mb-xl text-center" style="padding-top: 20px;"><strong>Who</strong> We Are</h2>
                <p class="lead text-color-dark">
                    <strong>Mega Lab Services</strong>
                is a third Party administrator (TPA) for Drug/Alcohol, DNA Testing, and Background Screening services.
                             We have been implementing analytical lab solutions using its core organizational values since its launch.
                    <br />
                                 We create a Turnkey Program for your organization to become a drug free workplace. 
                                 A policy is written on your behalf, customized to your needs and specifics of your business.             
                 <br />
                     <br />
                    This policy will comply with all federal and state regulations for regulated and non-regulated drug testing. 
                     Our experienced consultants will meet your management staff to implement the policy, answering questions, 
                    and addressing concerns. 
                    <br />
                    <br />
                    Our Lab Services can test for 5 different types of drugs (5-panel), up to 12 drugs (12-panel).
                   With our expertise, MLS has affiliations with SAMHSA certified, AABB, and ISO 17025 accredited laboratories. 
                   Mega Lab Services utilize FDA-cleared testing kits to help our clients manage their drug-free 
                   work place programs and enhance business processes that comply with all appropriate standards.
                </p>
                    
						</div>
					</div>	
    </section>
    

    <section class="section m-none">
        <div class="container">
            <div class="row">
                <div class="col-md-12 center">
                    <h1>What we <strong>do</strong></h1>
                </div>
            </div>
            <div class="row mt-lg">
                
                <div class="col-xs-6">
                    <h4 class="mb-xs text-center bold">Drug/Alcohol Testing & Employer Services</h4>
                    <div class="row">
                        <div class="col-md-2">

                            <img class="img-responsive mb-lg" src="img/mls/drug.png" alt="">
                        </div>
                        <div class="col-md-10">

                            <p>
                               We provide drug and alcohol testing to individuals and groups. Our drug and alcohol testing services includes Workplace Testing, Hair Follicle Testing, Steroid Testing,
                                Transportation Drug and Alcohol Testing, In-Home Drug Testing ......
                           <span><a href="services.aspx" class="lnk-tertiary learn-more">Learn More</a></span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6">
                    <h4 class="mb-xs text-center bold">DNA Testing</h4>
                    <div class="row">
                        <div class="col-md-2" >

                            <img class="img-responsive mb-lg"  src="img/mls/dna.png" alt="">
                        </div>
                        <div class="col-md-10">

                            <p>
                                We offer a wide range of DNA testing services including Informational DNA Testing
                               , Legal DNA Paternity Testing ,Home DNA Testing
                                 Full and Half Siblingship Testing, Ancestry Testing, Child Safety Identification Certificate and ISO 17025 Quality Assurance......
                           <span><a href="services.aspx" class="lnk-secondary learn-more">Learn More</a></span>
                                 </p>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-lg">
                <div class="col-xs-6">
                    <h4 class="mb-xs text-center bold">Workplace Safety and Medical Supplies</h4>
                    <div class="row">
                        <div class="col-md-2">

                            <img class="img-responsive mb-lg" src="img/mls/wp.png" alt="">
                        </div>
                        <div class="col-md-10">
                           <p>
                                     Under federal law, employees are entitled to a safe workplace. Employer must provide a workplace free
of known health and safety hazards. The Occupational Safety and Health Act of 1970 created OSHA.......
                           <span><a href="services.aspx" class="lnk-quaternary learn-more">Learn More</a></span>
                                </p>
                                
                        </div>
                    </div>
                </div>
                <div class="col-xs-6">
                    <h4 class="mb-xs text-center bold">Immigration DNA Testing</h4>
                    <div class="row">
                        <div class="col-md-2">

                            <img class="img-responsive mb-lg" src="img/mls/immigration.png" alt="">
                        </div>
                        <div class="col-md-10">

                            <p>
                                    Our immigration testing services include testing for Naturalization Services, Department of Health &
                                 Human Services Supplier, DNA Testing for local Embassies
                                    DNA Testing for Immigration clients, Immigration DNA Testing Consultation.......
                           <span><a href="services.aspx" class="lnk-primary learn-more">Learn More</a></span>
                                </p>
                                
                        </div>
                    </div>
                </div>
            </div>
             <div class="row mt-lg">
                <div class="col-xs-12">
                    <h4 class="mb-xs text-center bold">Background Checks</h4>
                    <div class="row">
                        <div class="col-md-2">

                            <img class="img-responsive mb-lg" src="img/mls/bg.png" alt="">
                        </div>
                        <div class="col-md-10">
                           <p>
                                                                                We offer background screening services that are ideal for private-owned companies, non-profit organizations, publicly owned for-profit organizations, and government sectors.

Mega Lab Services delivers the best and most reliable background checks and has the flexibility to adapt to your changing needs over time. 
                                            <br />
                                            <br />
                                            By establishing ourselves as your trusted partner, we deliver reliable, complete, and current information, legal compliance with all applicable federal/state/local laws, unparalleled customer service, and the best overall value.

                           <span><a href="services.aspx" class="lnk-quaternary learn-more">Learn More</a></span>
                                </p>
                                
                        </div>
                    </div>
                </div>
              
            </div>
        </div>
    </section>
    <!--Accreditations -->
    <section class="section mt-xl pb-none whitebg Nomargintop">
        <div class="container">
            <div class="row">
                <div class="col-md-12 center">
                    <h2 class="mb-xl"><strong>Partnerships</strong> &amp; <strong>Accreditations</strong></h2>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 center">
                    <div class="owl-carousel owl-theme mt-xl" data-plugin-options='{"items": 5, "autoplay": false,"loop": true, "autoplayTimeout": 3000}'>
                         <%--<div>
                            <img class="img-responsive" src="img/MLS/AABB_Logo.png" alt="">
                        </div>--%>
                        <div>
                            <img class="img-responsive" style="height:80px;"  src="img/MLS/datia_logo_800x150.png" >
                        </div>
                        <div>
                            <img class="img-responsive" src="img/MLS/quest-diagnostics-logo-2014.png" alt="">
                        </div>
                                               
                       
                   <div>
                               <!-- <img class="img-responsive" src="img/MLS/aabb_logo.png">-->
                                <img class="img-responsive" src="img/MLS/CDOT_Sample-C.png" />
                            </div>

                             <div>
                               <!-- <img class="img-responsive" src="img/MLS/aabb_logo.png">-->
                                 <img class="img-responsive" src="img/MLS/CBAlcoholTechs1.png" />
                            </div>
                        
                        

                    </div>
                </div>
            </div>

        </div>
    </section>
    <!-- Testimonials-->
    <section>
    <div class="row" style="background:url(img/MLS/greybg.jpg)">
        <div class="col-md-12">
            <div class="owl-carousel owl-theme nav-bottom rounded-nav mt-lg mb-none" data-plugin-options='{"items": 1, "loop": true,"autoplay": true,"autoplayTimeout": 5000}'>
                <div>
                    <div class="col-md-12">
                        <div class="testimonial testimonial-style-6 testimonial-with-quotes mb-none">
                            <blockquote>
                                <p> My entire experience at MegaLabs was awesome, testing process was quick and efficient and my results came quickly! Couldn't ask for more. Thank you MegaLab! .</p>
                            </blockquote>
                            <div class="testimonial-author">
                                <p><strong> Amy N.</strong><span> Alexandria, VA</span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="col-md-12">
                        <div class="testimonial testimonial-style-6 testimonial-with-quotes mb-none">
                            <blockquote>
                                <p> Thank you for the excellent service. I would recommend MegaLab any day!</p>
                            </blockquote>
                            <div class="testimonial-author">
                                <p><strong> Aziz Carl </strong><span> Washington DC</span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="col-md-12">
                        <div class="testimonial testimonial-style-6 testimonial-with-quotes mb-none">
                            <blockquote>
                                <p> Excellent customer service. The technician that took my samples was very informative, patient and answered all my questions!</p>
                            </blockquote>
                            <div class="testimonial-author">
                                <p><strong> John Mason </strong><span> San Jose, CA</span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="col-md-12">
                        <div class="testimonial testimonial-style-6 testimonial-with-quotes mb-none">
                            <blockquote>
                                <p> When I walked into Megalab, I was so pressed for time.The turnaround time for the results was just as they told me.I got my results fast! I will recommend Megalab without reservations.</p>
                            </blockquote>
                            <div class="testimonial-author"> 
                                <p><strong> Aminata W. </strong><span> Palm Springs, FL</span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="col-md-12">
                        <div class="testimonial testimonial-style-6 testimonial-with-quotes mb-none">
                            <blockquote>
                                <p> My mom and I had a lot of questions about ancestry testing when we first reached out to Megalabs. They were very informative and answered all our questions. Great customer service!</p>
                            </blockquote>
                            <div class="testimonial-author">
                                <p><strong> Pam & Vanessa H.</strong><span>Chicago, IL</span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        </section>


</asp:Content>

