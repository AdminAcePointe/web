﻿using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Mail;
using System.Configuration;
using System;

public partial class Locations : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod]
    public static int savenewsletter(string email)
    {


        int res = 0;
        string insertnews = String.Empty;
        int insertnote = 0;

        try
        {
            insertnews = insertnewsletter(email);

            if (insertnews != "0")
            {
                StringBuilder emailstring = new StringBuilder();
                emailstring.AppendLine("<p><span></span><span>Dear Subscriber</span></p>");

                emailstring.AppendLine("<p>Thanks for your subscription to megalabservices newsletter");

                emailstring.AppendLine("<p><span>In a short while , you will begin to recieve useful information about the clinical labs industry as well as Megalab offerings</span></p>");


                emailstring.AppendLine("<br/><p><span>Thanks, Megalabs <br/info@megalabservices.com <br/>www.megalabservices.com</span></p>");

                emailstring.AppendLine("<br/><p style=\"font-size:10px\"><span><a href=\"www.megalabservices.com/newsletterunsubscribe.aspx?subscriberid=" + insertnews.ToString() + "\">Click here to unsubscribe</a></span></p>");


                //SmtpClient smtp = new SmtpClient();
                const string SERVER = "relay-hosting.secureserver.net";
                MailMessage newmessage = new System.Web.Mail.MailMessage();
                newmessage.From = "info@megalabservices.com";
                newmessage.To = email;
                newmessage.BodyFormat = MailFormat.Html;
                newmessage.BodyEncoding = Encoding.UTF8;
                newmessage.Subject = "Megalabservices Newsletter subscription";
                newmessage.Body = emailstring.ToString();
                //newmessage.Cc = "";
                newmessage.Bcc = "info@acepointe.com";
                SmtpMail.SmtpServer = SERVER;
                SmtpMail.Send(newmessage);

                StringBuilder emailstring2 = new StringBuilder();

                emailstring2.AppendLine("<p>Newsletter subscription notification");

                emailstring2.AppendLine("<p><span>A new client has just subscribed for your newsletter</span></p>");
                emailstring2.AppendLine("<p><span>Email :</span><span>" + email +"</span></p>");
                 

                emailstring2.AppendLine("<p><span>Thanks , Megalabservices </p>");


                MailMessage newmessage2 = new System.Web.Mail.MailMessage();
                newmessage2.From = "info@megalabservices.com";
                newmessage2.To = "info@megalabservices.com";
                newmessage2.BodyFormat = MailFormat.Html;
                newmessage2.BodyEncoding = Encoding.UTF8;
                newmessage2.Subject = "Megalabservices  -  Newsletter subscription";
                newmessage2.Body = emailstring2.ToString();
                newmessage2.Cc = "";
                newmessage2.Bcc = "info@acepointe.com";
                SmtpMail.SmtpServer = SERVER;
                SmtpMail.Send(newmessage2);



                res = 1;
                if (res == 1)
                {
                    insertnote = insertNotification(newmessage2.From, newmessage2.To, newmessage2.Cc, newmessage2.Bcc, newmessage2.Subject, newmessage2.Body);
                }
                if (insertnote != 1)
                {
                    res = 0;
                }

            }
            else res = 0;

        }
        catch
        {
            res = 0;
        }

        return res;
    }
    [WebMethod]
    public static string insertnewsletter(string email)
    {


        string guid = string.Empty;
        //try
        //{

            string cs = ConfigurationManager.ConnectionStrings["eklipseconsult"].ConnectionString;
            using (SqlConnection con = new SqlConnection(cs))
            {
                using (SqlCommand cmd = new SqlCommand("dbo.insertsubscription", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@email", email);
                    con.Open();
                    guid = cmd.ExecuteScalar().ToString();

                    con.Close();

                }

            }
        //}
        //catch
        //{
        //    guid = "0";
        //}

        return guid;
    }
    [WebMethod]
    public static int insertNotification(string from, string to, string cc, string bcc, string subject
        , string body)
    {

        int notres = 0;
        try
        {

            string cs = ConfigurationManager.ConnectionStrings["eklipseconsult"].ConnectionString;
            using (SqlConnection con = new SqlConnection(cs))
            {
                using (SqlCommand cmd = new SqlCommand("dbo.insertNotification", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@from", from);
                    cmd.Parameters.AddWithValue("@to", to);
                    cmd.Parameters.AddWithValue("@cc", cc);
                    cmd.Parameters.AddWithValue("@bcc", bcc);
                    cmd.Parameters.AddWithValue("@subject", subject);
                    cmd.Parameters.AddWithValue("@body", body);


                    con.Open();
                    notres = Convert.ToInt32(cmd.ExecuteScalar());

                    con.Close();

                }

            }
            notres = 1;
        }
        catch
        {
            notres = 0;
        }

        return notres;
    }
}