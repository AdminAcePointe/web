﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Mail;


public partial class Contact : System.Web.UI.Page
{


        protected void Page_Load(object sender, EventArgs e)
        {

        }
      
        [WebMethod]
        public static int sendmail(string fname, string lname, string email, string phone, string message)
        {
       
            int res = 0;

            try
            {
                StringBuilder emailstring = new StringBuilder();
                emailstring.AppendLine("<p><span>Name : </span><span>" + fname + " " + lname + "</span></p>");
                emailstring.AppendLine("<p><span>Email : </span><span>" + email + "</span></p>");
                emailstring.AppendLine("<p><span>Phone : </span><span>" + phone + "</span></p>");
                emailstring.AppendLine("<p><span>Message : </span><span>" + message + "</span></p>");

                //SmtpClient smtp = new SmtpClient();
                const string SERVER = "relay-hosting.secureserver.net";
                MailMessage newmessage = new System.Web.Mail.MailMessage();
                newmessage.From = "info@megalabservices.com";
                newmessage.To = "info@megalabservices.com";
                newmessage.BodyFormat = MailFormat.Html;
                newmessage.BodyEncoding = Encoding.UTF8;
                newmessage.Subject = "Megalabservices : New message from " + fname + " " + lname;
                newmessage.Body = emailstring.ToString();
               // newmessage.Cc = "";
               //newmessage.Bcc = "";
                SmtpMail.SmtpServer = SERVER;
                SmtpMail.Send(newmessage);

                //smtp.Send(newmessage);
                res = 1;
            }
            catch
            {
                res = 0;
            }

            return res;
        }
    }
