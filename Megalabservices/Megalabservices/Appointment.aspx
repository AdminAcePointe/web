﻿<%@ Page Title="" Language="C#" MasterPageFile="megalabs.master" AutoEventWireup="true" CodeFile="Appointment.aspx.cs" Inherits="Appointment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title>Appointments | Mega Lab Services</title>	
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div class="container">

					<div class="row">
						<div class="col-md-7">

							

							<h2 class="mb-sm mt-sm"><strong>Appointment</strong></h2> 
                            <%--<h5>(for Woodbridge,VA location only)</h5>--%>
							<form id="ApptForm"  method="POST">
								<div class="row">
									<div class="form-group">
										<div class="col-md-6">
											<label> First Name <span class="required">*</span></label>
											<input type="text" value="" data-msg-required="Please enter your first name." maxlength="100" class="form-control" name="fname" id="fname" >
										</div>
										<div class="col-md-6">
											<label> Last Name <span class="required">*</span></label>
											<input type="text" value="" data-msg-required="Please enter your last name." maxlength="100" class="form-control" name="lname" id="lname" >
										</div>
									</div>
								</div>
								<div class="row">
                                    	<div class="form-group">
										<div class="col-md-6">
											<label>Phone number</label>
											<input type="text" value=""  maxlength="100" class="form-control" name="phone" id="phone">
										</div>
									
										<div class="col-md-6">
											<label>Email Address <span class="required">*</span></label>
											<input type="text" value="" data-msg-required="Please enter your email address." maxlength="100" class="form-control" name="email" id="email" >
										</div>
                                        </div>
									
								</div>

                                <div class="row">
                                    	<div class="form-group">
                                        <div class="col-md-6">
											<label>Choose A Location <span class="required">*</span></label>
                                            <select id="location" class="form-control" data-msg-required="Please a location." name="location">
                                            </select>
										</div>
										<div class="col-md-6">
											<label>Choose Test Category <span class="required">*</span></label>
                                            <select id="testcat" class="form-control" data-msg-required="Please choose test category." name="testcat">
                                            </select>
										</div>
									
										<div class="col-md-12">
                                            <br />
											<label>Choose Test <span class="required">*</span></label>
                                            <select id="test" class="form-control" data-msg-required="Please choose test type." name="test"> 
                                           </select>
										</div>
                                        </div>
									
								</div>
                                <div class="row">
                                 <div class="form-group">
										<div class="col-md-6">
											<label>Choose Appointment Date</label>
                                            <div class='input-group date' id="appointmentdate" >
                                                <input id="aptdate" type='text' class="form-control" data-msg-required="Please appointment date." name="aptdate"/>
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
										</div>
									
										<div class="col-md-6">
											<label>Choose Appointment Time<span class="required">*</span></label>
                                            
                                                 <select id="appointmenttime" class="form-control" data-msg-required="Please choose appointment time." name="aptime">
                                       
                                            </select>
                                        </div>
                                        </div>
                                 </div>
								<div class="row">
									<div class="form-group">
										<div class="col-md-12">
                                            <br />
                                            <br />

											<label>Comments</label>
											<textarea maxlength="500" rows="5" class="form-control" name="message" id="message"></textarea>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<input id="submitappt" type="submit" value="Submit" class="btn btn-primary btn-lg mb-xlg" data-loading-text="Loading...">
									</div>
								</div>
							</form>

                            <div class="alert alert-success hidden" id="contactSuccess">
								<strong>Success!</strong> Your appointment has been set up successfully. An email will be sent to you.
							</div>

							<div class="alert alert-danger hidden" id="contactError">
								<strong>Error!</strong> There was an error setting up your appointment. Please try again later or call our office at (866) 261-7129 or (571) 285-1857 for scheduling assistance.
							</div>
						</div>
						<div class="col-md-5" style="margin-top:60px;color:#ffffff !important">
                            <div class="featured-boxes mt-none mb-none tblack">
										
											<h4 class="text-center bold" style="padding-top:20px">Schedule your appointment online !</h4>
                                	<hr>
												<p>Please use this appointment scheduler to schedule an appointment. </p>
                                                   
                                <%--<p>To schedule appointments at a partner location near you, please visit our  <a href="locations.aspx"><strong>TESTING CENTERS</strong></a> page to locate a center.</p>--%>
												
                                <p> To cancel or reschedule an existing appointment, <br />please call  <strong>(866) 261-7129</strong>
                                                     or  <strong>(571) 285-1857</strong> or send us an email at <a href="mailto:info@megalabservices.com ">info@megalabservices.com </a></p>
												<hr>
										<p><strong style="">Do you know </strong> that you can access your test results online.We can also also send you notifications when your results are available.
                                            Sign up for a mls account and you will have access to all your  test results .<br />
                                            Please contact one of our staff to get you going right away. 
										</p>
									</div>
						</div>

					</div>

				</div>
   
</asp:Content>

