﻿<%@ Page Title="" Language="C#" MasterPageFile="megalabs.master" AutoEventWireup="true" CodeFile="Services.aspx.cs" Inherits="Services" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Services | Mega Lab Services</title>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <section class="parallax section section-text-light section-parallax section-center mt-none" data-stellar-background-ratio="0.8"
        style="background-image: url(img/MLS/servicesnew.jpg); margin-bottom: 0;">
        <div class="container">
            <div class="row">
                <h1 class="text-center test" style="padding: 100px;">Services</h1>

            </div>
        </div>
    </section>

    <section class="call-to-action mb-xl" style="min-height: 90px; margin-bottom: 0 !important; background: url(img/MLS/greybg.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <h3 class="center" style="padding-top: 30px;">Megalab Services  provides
                                        <strong>high-quality laboratory tests </strong>in a timely manner
                    </h3>



                </div>
            </div>
        </div>
    </section>


    <section class="section" style="background: whitesmoke; margin-top: 0; margin-bottom: 0;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="tabs tabs-bottom tabs-center tabs-simple">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tabsNavigationSimpleIcons1" data-toggle="tab">
                                    <span class="featured-boxes featured-boxes-style-6 p-none m-none">
                                        <span class="featured-box featured-box-primary featured-box-effect-6 p-none m-none">
                                            <span class="box-content p-none m-none">
                                                <img style="height: 80px; width: 80px;" src="img/MLS/drug.PNG" />
                                            </span>
                                        </span>
                                    </span>
                                    <p class="mb-none pb-none">Drug/Alcohol Testing
                                       </p>
                                </a>
                            </li>
                            <li>
                                <a href="#tabsNavigationSimpleIcons2" data-toggle="tab">
                                    <span class="featured-boxes featured-boxes-style-6 p-none m-none">
                                        <span class="featured-box featured-box-primary featured-box-effect-6 p-none m-none">
                                            <span class="box-content p-none m-none">
                                                <img style="height: 80px; width: 80px;" src="img/MLS/dna.PNG" />
                                            </span>
                                        </span>
                                    </span>
                                    <p class="mb-none pb-none">DNA Testing</p>
                                </a>
                            </li>
                            <li>
                                <a href="#tabsNavigationSimpleIcons3" data-toggle="tab">
                                    <span class="featured-boxes featured-boxes-style-6 p-none m-none">
                                        <span class="featured-box featured-box-primary featured-box-effect-6 p-none m-none">
                                            <span class="box-content p-none m-none">
                                                <img style="height: 80px; width: 80px;" src="img/MLS/immigration.PNG" />
                                            </span>
                                        </span>
                                    </span>
                                    <p class="mb-none pb-none">Immigration DNA Testing</p>
                                </a>
                            </li>
                            <li>
                                <a href="#tabsNavigationSimpleIcons5" data-toggle="tab">
                                    <span class="featured-boxes featured-boxes-style-6 p-none m-none">
                                        <span class="featured-box featured-box-primary featured-box-effect-6 p-none m-none">
                                            <span class="box-content p-none m-none">
                                                <img style="height: 80px; width: 80px;" src="img/MLS/wp.PNG" />
                                            </span>
                                        </span>
                                    </span>
                                    <p class="mb-none pb-none">Workplace Safety
                                        Supplies</p>
                                </a>
                            </li>
                            <li>
                                <a href="#tabsNavigationSimpleIcons4" data-toggle="tab">
                                    <span class="featured-boxes featured-boxes-style-6 p-none m-none">
                                        <span class="featured-box featured-box-primary featured-box-effect-6 p-none m-none">
                                            <span class="box-content p-none m-none">
                                                <img style="height: 80px; width: 80px;" src="img/MLS/bg.PNG" />
                                            </span>
                                        </span>
                                    </span>
                                    <p class="mb-none pb-none">Background Checks</p>
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content" style="background: whitesmoke">
                            <div class="tab-pane active" id="tabsNavigationSimpleIcons1">
                                <div class="center">
                                    <h4 class="text-left heading"><strong>Drug/Alcohol Testing & Employer Services</strong></h4>
                                    <div class="col-md-8">
                                        <p class="text-color-dark text-left">
                                            Wouldn’t you like the confidence of knowing your workplace is drug free? Don’t assume anyone in your organization is 
                                        immune to the problem of drug and alcohol abuse. Drug use affects workplace whether the employer sees it or not. 
                                        <br />
                                            <br />
                                            The White House Office of National Drug Control Policy points out that large companies often have drug-free 
                                        workplace programs which result in 50% lower positive drug test rates, and 75% fewer self-reports of current
                                         drug use among workers compared to smaller worksites.
                                        <br />
                                            <br />
                                            A business owner or an employer may find out if drug abuse is 
                                        threatening his or her business by implementing a drug-free workplace program. Knowing what threats exist and how to
                                         neutralize them will reduce the risk to your company’s resources and profits. 
                                        <br />
                                            <br />
                                            How Mega Lab can help: We can help your 
                                        company set up a drug-free workplace program, we work with you to provide a written policy up to the stage of implementation. 

                                        </p>

                                    </div>
                                    <div class="col-md-4" style="background: #e9e9e9">
                                        <br />
                                        <p class="text-color-dark ">Our Drug/Alcohol testing & Employer services</p>

                                        <hr />

                                        <ul class="list list-icons list-icons-style-3 mt-xlg" style="text-align: left; color: black">
                                            <li><i class="fa fa-caret-right"></i><a class="text-color-dark" href="#" data-toggle="modal" data-target="#workplaceModal">Workplace Testing</a>
                                                <div class="modal fade" id="workplaceModal" tabindex="-1" role="dialog" aria-labelledby="workplaceModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                <h4 class="modal-title" id="workplaceModalLabel">Workplace Testing</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>
                                                                    Mega Lab can help implement an ongoing drug testing program at your workplace in the areas of:
Pre-Employment Drug Testing
On-going Random Drug Testing
Post-Accident Drug Testing
Reasonable Suspicion/Cause Drug Testing
Follow-Up Drug Testing
Return to Work/Duty Drug Testing
Drug Detection Surface Assessments.
                                                                </p>
                                                                <p>
                                                                    Benefits of a Drug-free Workplace:
Improved productivity,
Less theft,
Fewer drug-related accidents,
Reduced time away from work (Tardiness),
Low medical cost,
Reduced workers’ compensation insurance bills,
5% discount on workers’ compensation insurance (Not applicable in some states)
                                                                </p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li><i class="fa fa-caret-right"></i><a class="text-color-dark" href="#" data-toggle="modal" data-target="#HairFollicleModal">Instant Drug Testing</a>
                                                <div class="modal fade" id="HairFollicleModal" tabindex="-1" role="dialog" aria-labelledby="HairFollicleLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                <h4 class="modal-title" id="HairFollicleLabel">Instant Drug Testing</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>
                                                                    Instant or rapid testing for drugs of abuse yields rapid, reliable results. Compared with lab-based testing which can take hours or even days to provide a result, instant testing takes only minutes and enables faster decision making and greater program efficiency. 
 U.S. Food and Drug Administration (FDA) cleared testing kits deliver consistent, accurate results. With results available only minutes after the collection, instant or rapid testing eliminates the logistics, testing and reporting delays that can be associated with lab-based drug testing.
Results are available for employers within minutes of initiating the test and will remain stable for 60 minutes.

Laboratory Support
Instant tests that screen non-negative for illicit drugs should be confirmed in a laboratory. Within our network of four SAMHSA-certified drug-testing laboratories, we will retest the rapid test specimen to determine if drugs are present.  This additional layer of support and assurance can help to improve the quality and reliability of any testing program.

All testing kits are FDA cleared.
Instant test results.
Lower cost per test.
All presumptive positive test results sent to our SAMHSA certified lab overnight for confirmation by using a sophisticated, accurate and precision analytical equipment-Gas Chromatograph/Mass Spectrometer (GC/MS).
Mega Lab Services offers a complete line of instant on-site drug test kits from one of the worlds’ leading medical equipment manufacturers. We carry different types of drug test kits with different combination of drug panels. Our kits can test for (1) individual drug or a combination of up to (12) drugs.

Please allow us to put together an instant on-site testing program for your company.
                                                                </p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li><i class="fa fa-caret-right"></i><a class="text-color-dark" href="#" data-toggle="modal" data-target="#SteroidTestingModal">Steroid Testing</a>
                                                <div class="modal fade" id="SteroidTestingModal" tabindex="-1" role="dialog" aria-labelledby="SteroidTestingLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                <h4 class="modal-title" id="SteroidTestingLabel">Steroid Testing</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>
                                                                    Steroid use in sports is unfortunately becoming very rampant in our society. It is so prevalent that when an athlete accomplishes something truly amazing, the first reaction of many people is to suspect drug use instead of celebrating their achievement.

Adults, teenagers and professional athletes abuse these dangerous drugs; National Institute of Drug Abuse found that 0.5% of 8th graders, 1.0% of 10th graders and 1.5% of 12th graders had abused anabolic steroids at least once in the year prior to being surveyed in 2010. These are teenagers abusing these dangerous drugs.

Anabolic steroids help in increasing the muscle mass and physical strength, and are consequently used in sport and bodybuilding to enhance strength or physique. So, other than prescribed medicinal users, there are many user groups who look towards steroids to assist them in attaining the physical attributes they crave for. Such a misuse of steroids is prohibited under anti-doping policy by many sports organizations across the globe. So, a wide range of tests have been designed & are under consistent evolution to increase specificity and sensitivity of detecting and deterring performance-enhancing drugs abuse by competitors in various sports. There are many laboratory based as well as kit based methodologies for detecting testosterone or any of its known metabolites. Laboratory based techniques may involve, amongst others, antibody based screening, Liquid/GC Chromatography etc. The results obtained by Chromatographic analysis may be coupled with Mass Spectrometry (MS) for sensitive as well as specific diagnosis and for validation purposes. The nature of samples required for such tests is conciliatory as blood, urine or even hair follicle samples may be used.

Mega Lab Services offers a comprehensive and affordable steroid panel. Our steroid test is ideal for sports organizations, colleges and high schools. Corrections, law enforcement and occupational health agencies are using this panel too.

Benefits Include:

Comprehensive laboratory test detects full list of prescription androgenic anabolic steroids, prohormones, designer anabolic steroids, essential masking and anti-estrogen agents
All laboratory tests performed on state of the art GC/MS equipment
Affordable Pricing (ask us about combining your drug testing panels with steroid tests
Fast turn-around time from time of receipt (48 hours negative, 72 hours Positive)
Includes chain of custody, collection & Shipping supplies at no additional charge
Free next day shipping to lab
This test is comparable to WADA (World Anti-Doping Agency) Tests

Examples of steroids are: Ethylstrenol, Oxymesterone, Methandrostenolone, Fluoxymesterone, Teststerone, Methandriol, Bolasterone,,Glenbuteron (anabolic agent), Norbolethone
                                                                </p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li><i class="fa fa-caret-right"></i><a class="text-color-dark" href="#" data-toggle="modal" data-target="#TransDALModal">Transportation Drug and Alcohol Testing</a>
                                                <div class="modal fade" id="TransDALModal" tabindex="-1" role="dialog" aria-labelledby="TransDALModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                <h4 class="modal-title" id="TransDALModalLabel">Transportation Drug and Alcohol Testing</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>
                                                                    According to the National Drug-Free Workplace Alliance, companies are subject to DOT-regulated drug testing if:
Your company possesses vehicles having gross weight rating (GWR) of 26,001 Lbs. and over
Have Employees occupying certain safety sensitive positions, examples are: employees driving trucks with CDL licenses, operating heavy machinery, working on pipelines, operating a ferry, school bus driver, employees under FAA, FHWA, FMCSA, FRA, FTA, MARAD,HNTSA, PHMSA, SLSDC,STB,
                                                                </p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li><i class="fa fa-caret-right"></i><a class="text-color-dark" href="#" data-toggle="modal" data-target="#IHGModal">Court Ordered Drug Testing</a>
                                                <div class="modal fade" id="IHGModal" tabindex="-1" role="dialog" aria-labelledby="IHGModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                <h4 class="modal-title" id="IHGModalLabel">Court Ordered Drug Testing</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>
                                                                    Court-ordered drug tests are required to be administered by certified professionals. Since court-ordered drug tests are needed in many situations, including arrests for DUI or DWI, probation, and child custody arrangements, the administrator must strictly follow the guidelines on the court order or legal agreement. Often, the court order or agreement will determine the drug and alcohol testing frequency and guidelines. Court ordered drug tests are common for violations like driving while intoxicated or under the influence of drugs or alcohol.
Legal battles involving child custody may include issues of drug or alcohol use or abuse among one (or both) of the former spouses. In order to protect the children, an ongoing program of random drug testing is often agreed upon or ordered by the court. Alternatively, the program may include testing of either spouse at the time of visitation or custody.
                                                                </p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li><i class="fa fa-caret-right"></i><a class="text-color-dark" href="#" data-toggle="modal" data-target="#DFWModal">Oral Fluid Drug Testing</a>
                                                <div class="modal fade" id="DFWModal" tabindex="-1" role="dialog" aria-labelledby="DFWModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                <h4 class="modal-title" id="DFWModalLabel">Oral Fluid Drug Testing</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>
                                                                    Oral Fluid Testing is an innovative testing method that is quicker, more reliable, and less intrusive than urine testing methods. Testing 15-20 employees can take approximately an hour with urine drug testing methods, while that same number of employees can be done in 25 – 30 minutes using Oral Fluid. Since the collection of oral fluid specimen can be viewed by a second person without infringing privacy it does not suffer from the same issues regarding possible adulteration or substitution as for urine. The procedure for collecting saliva is non-invasive and you don’t need to worry about things like shy bladder. Oral fluid is readily accessible and gathering a sample for oral fluid testing does not require the use of same-sex collectors nor does it require special collection facilities. It merely requires a mouth swab to test.
Mega Lab can save you time and money by coming directly to your place of business, and it’s great for streamlining the hiring process. Our sample collectors are trained and certified, sample analysis performed by SAMSHA approved laboratories.
Our ability to collect on site nationally delivers a convenient, consistent and professional collection experience.
Call Mega Lab for more information on Oral Fluid Drug Testing program.
                                                                </p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>


                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tabsNavigationSimpleIcons2">
                                <div class="center">
                                    <h4 class="text-left heading"><strong>DNA Testing</strong></h4>
                                    <div class="col-md-8">
                                        <p class="text-color-dark text-left">
                                            Got some questions you need answers to? Is the unknown tearing your familiy apart? Are you losing sleep due to fear of the outcome? Or maybe you are just curious.
                                    Whatever the case may be, if you're ready for some answers, Mega Lab can help.
                                             <br />
                                            <br />
                                            Mega Lab offers a wide range of DNA testing options including paternity testing, maternity testing, siblingship testing, avuncular Aunt/Uncle DNA Test,
                                            grand parentage, twin zygosity Testing, infidelity testing, identity DNA testing, pharmacogenetic  DNA  testing and lots more. 


                                        <br />
                                            <br />
                                            We understand how stressful and difficult some of these situations can be and we do our best to make the process of getting your questions answered as easy and stress-free as possible. Our 
                                           lab tests are conducted professionally with state of the art technology, ensuring that you get accurate result of your testing.  In addition, we take your privacy matters seriously and we ensure your privacy is always  protected throughout the process.

                                        </p>

                                    </div>
                                    <div class="col-md-4" style="background: #e9e9e9">
                                        <br />
                                        <p class="text-color-dark ">Our DNA testing services</p>


                                        <hr />

                                        <ul class="list list-icons list-icons-style-3 mt-xlg" style="text-align: left; color: black">
                                            <li><i class="fa fa-caret-right"></i><a class="text-color-dark" href="#" data-toggle="modal" data-target="#RDNAModal">Relationship DNA Testing</a>
                                                <div class="modal fade" id="RDNAModal" tabindex="-1" role="dialog" aria-labelledby="RDNAModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                <h4 class="modal-title" id="RDNAModalLabel">Relationship DNA Testing</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>
                                                                    Paternity Testing:

Personal Prenatal DNA Paternity Testing – Used to determine paternity of a child before the child is born. Includes one alleged father, one child (amniotic fluid), and biological mother.
                                                                    <br />
                                                                    <br />
                                                                    Maternity Testing:

Legal Prenatal DNA Paternity Testing – Used to determine paternity of a child before the child is born for LEGAL purposes. Includes one alleged father, one child (amniotic fluid), and biological mother.
                                                                    <br />
                                                                    <br />
                                                                    Grand Parentage:

Duo Grandparentage DNA Test – Determine Grandparentage for grandfather, grandmother and grandchild.
Single Grandparentage Test – Determine Grandparentage for a single grandparent and grandchild.
                                                                    <br />
                                                                    <br />
                                                                    Twin Zygosity Testing:

Twin Zygosity Test – Determine whether twins (or triplets, etc.) are identical or fraternal..
                                                                </p>
                                                                <p>
                                                                    Siblingship Testing:

Full Sibling DNA Test – Determine whether two (or more) individuals share both parents in common.
Half Sibling DNA Test – Determine whether two (or more) individuals share one parent in common.
                                                                    <br />
                                                                    <br />
                                                                    Avuncular Aunt/Uncle DNA Test:

Determine if a man or woman is a biological aunt or uncle to a child.
The DNA Aunt / Uncle Test will determine if a man / woman is the biological aunt / uncle of a child,
who shares a quarter of their DNA with the Aunt or Uncle.
This DNA Test can be useful if one of the parents is unavailable for testing.
Mega Lab’s Avuncular DNA Test (Aunt / Uncle) is the best method in determining, with indisputable accuracy,
the biological relationship between a potential aunt or uncle and child.
Following our easy-to-use instructions, you can collect DNA samples using normal cotton swabs in less than 5 minutes.
                                                                </p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li><i class="fa fa-caret-right"></i><a class="text-color-dark" href="#" data-toggle="modal" data-target="#ADNAModal">Ancestry DNA Testing</a>
                                                <div class="modal fade" id="ADNAModal" tabindex="-1" role="dialog" aria-labelledby="HairFollicleLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                <h4 class="modal-title" id="ADNAModalLabel">Ancestry DNA Testing</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>
                                                                    The analysis is performed after DNA Spectrum receives the DNA Profile from DDC.

The analysis matches your DNA with 115,000 individual forensic records in over 400 known world populations and reveals the detailed results, including your top 50 individual rankings and your deep ancestry results.

These populations account for much of the known historic migration of people.

Through our advanced statistical analysis, this can truly answer the question, “Where am I from?”.
                                                                </p>
                                                                <p>
                                                                    DNA Ancestry
This report displays your unique autosomal genetic profile, which is 99.9999999% exclusive to you.

This is the same profile that universities, courts, and law enforcement agencies use to identify individuals in the United States

We use the highest level of security to protect your results. Please safely store this document in a secure location.

Premium Atlas of Ancestry
This beautiful world atlas is a unique visual way to understand your connections throughout history to different parts of the world and to diverse populations of individuals

Through varying colors and locations, you can see your ties to different groups of people, the strength of those ties, and how your ancestors have migrated through time..
                                                                </p>
                                                                <p>
                                                                    Framed Deluxe Certificate of Ethnicity
The framed deluxe certificate show your name and ID number above your Predominant, Secondary, and Minority matches.

It is authenticated by DNA Spectrum’s principal investigator, and is great to hang on your wall or show your family.

This certificate authenticates your heritage for all to see.
                                                                </p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li><i class="fa fa-caret-right"></i><a class="text-color-dark" href="#" data-toggle="modal" data-target="#CAPModal">Court Admissible/Professional DNA Testing</a>
                                                <div class="modal fade" id="CAPModal" tabindex="-1" role="dialog" aria-labelledby="CAPModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                <h4 class="modal-title" id="CAPModalLabel">Court Admissible/Professional DNA Testing</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>Court admissible DNA testing is the only type of DNA testing that can be brought forth as evidence in legal cases. All judicial courts require chain of custody to determine the validity of the court admissible DNA testing results, this ensures that all DNA specimens collected are thoroughly documented and well-handled, preventing slip-ups that could jeopardize judicial proceedings..</p>
                                                                <p>
                                                                    Chain of Custody DNA collection requires:
Photo IDs of participants aged 18 and older
Photos of everyone involved
Consent from adults & legal guardians for minors
Specimen collection (buccal swab/cheek swab)
Legal labeling and packaging
Specimens ship to testing laboratory directly through approved route
Proper shipping, storage and results communication to and from the lab
Paperwork and documents with DNA test results properly filled out
DNA specimen processed by American Association of Blood Banks (AABB) certified laboratory.
                                                                </p>
                                                                <p>
                                                                    Mega Lab Services has certified AABB Relationship DNA Sample Collectors, we follow all legal guidelines, we protect our client’s privacy, DNA test results are 99.999% accurate and court admissible.

DNA test are court admissible for any purpose, including:

changing a birth certificate
petitioning for child custody
Child Adoption
establishing paternity for child support purposes
changing wills and estates
proving right of inheritance
Survivor’s SSI or Social Security benefits
                                                                </p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li><i class="fa fa-caret-right"></i><a class="text-color-dark" href="#" data-toggle="modal" data-target="#FDNAModal">Forensic DNA Testing</a>
                                                <div class="modal fade" id="FDNAModal" tabindex="-1" role="dialog" aria-labelledby="FDNAModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                <h4 class="modal-title" id="FDNAModalLabel">Forensic DNA Testing</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>
                                                                    DNA forensics is a branch of forensic science that focuses on the use of genetic material in criminal investigation to answer questions pertaining to legal situations, including criminal and civil cases.
DNA forensics is being used in the courtroom to investigate violent crimes, paternity, social security, and insurance claims, and to track food-borne epidemics, among other things.
Recent advancements in DNA technology have improved law enforcement’s ability to use DNA to solve old cases. Original forensic applications of DNA analysis were developed using a technology called restriction fragment length polymorphism (RFLP). Although very old cases (more than 10 years) may not have had RFLP analysis done, this kind of DNA testing may have been attempted on more recent unsolved cases. However, because RFLP analysis required a relatively large quantity of DNA, testing may not have been successful. Similarly, biological evidence deemed insufficient in size for testing may not have been previously submitted for testing. Also, if a biological sample was degraded by environmental factors such as dirt or mold, RFLP analysis may have been unsuccessful at yielding a result. Newer technologies could now be successful in obtaining results. Newer DNA analysis techniques enable laboratories to develop profiles from biological evidence invisible to the naked eye, such as skin cells left on ligatures or weapons. Unsolved cases should be evaluated by investigating both traditional and nontraditional sources of DNA. Valuable DNA evidence might be available that previously went undetected in the original investigation. If biological evidence is available for test
DNA is the fundamental building block for an individual’s entire genetic makeup. It is a component of virtually every cell in the human body, and a person’s DNA is the same in every cell. That is, the DNA in a person’s blood is the same as the DNA in his skin cells, saliva, and other biological material. DNA analysis is a powerful tool because each person’s DNA is unique (with the exception of identical twins). Therefore, DNA evidence collected from a crime scene can implicate or eliminate a suspect, similar to the use of fingerprints. It also can analyze unidentified remains through comparisons with DNA from relatives. Additionally, when evidence from one crime scene is compared with evidence from another using Combined DNA Index System (CODIS), those crime scenes can be linked to the same perpetrator locally, statewide, and nationally. DNA is also a powerful tool because when biological evidence from crime scenes is collected and stored properly, forensically valuable DNA can be found on evidence that may be decades old. Therefore, old cases that were previously thought unsolvable may contain valuable DNA evidence capable of identifying the perpetrator.
                                                                </p>

                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li><i class="fa fa-caret-right"></i><a class="text-color-dark" href="#" data-toggle="modal" data-target="#PHModal">Pharmacogenetics DNA</a>
                                                <div class="modal fade" id="PHModal" tabindex="-1" role="dialog" aria-labelledby="PHModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                <h4 class="modal-title" id="PHModalLabel">Pharmacogenetics DNA</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>
                                                                    How Does Pharmacogenetics Testing Work?

An individual’s responses to drugs are determined by analysis of the inherited genetic differences in drug metabolic pathways. These differences can affect how effective a prescriptions will be and whether the drugs will affect an individual adversely.

Though the technology is advanced, the pharmacogenetics testing process is relatively simple, saliva sample (mouth swab) is collected and   a personal genetic DNA is created profile for the tested individual. This information is then run against a comprehensive list of medications to determine the following:

What prescriptions will work best
What prescriptions should be avoided
What medications can be prescribed with the usual precautions
What dosage for selected medications
When the analysis is complete, you can send your pharmacogenetics testing results to any and all physicians of your choice so that they can make informed decisions regarding what medications are right for you..
                                                                </p>
                                                                <p>
                                                                    Pharmacogenetics testing has proven useful in developing more effective treatment plans for a number of medical conditions.

Benefits of Pharmacogenetics Testing for Patients

There are many advantages to pursuing pharmacogenetics testing as a patient, including:

One-time test: Since pharmacogenetics testing involves your unchanging DNA, it can be completed once and updated when necessary.
Lower costs: When the trial and error prescription process is eliminated, co-pays and out-of-pocket costs go down; insurance premiums may also decrease.
Get well sooner: If the right medication and dosage is prescribed first, the patient’s condition will improve sooner. Doctors are further able to avoid treatment plans that include prescriptions with possible adverse effects.
Fewer doctor’s visits: With the right prescription in hand, multiple trips to the doctor’s office may not be necessary; less wait time, less exposure to germs, and lower costs.
                                                                </p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li><i class="fa fa-caret-right"></i><a class="text-color-dark" href="#" data-toggle="modal" data-target="#IDModal">Identity DNA Testing</a>
                                                <div class="modal fade" id="IDModal" tabindex="-1" role="dialog" aria-labelledby="IDModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                <h4 class="modal-title" id="IDModalLabel">Identity DNA Testing</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>Child Identification DNA</p>

                                                                <p>DNA Profiling</p>

                                                                <p>DNA Banking</p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li><i class="fa fa-caret-right"></i><a class="text-color-dark" href="#" data-toggle="modal" data-target="#IFModal">Infidelity Testing</a>
                                                <div class="modal fade" id="IFModal" tabindex="-1" role="dialog" aria-labelledby="IFModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                <h4 class="modal-title" id="IFModal">Infidelity Testing</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>
                                                                    Is your lover having sex outside the relationship? You need to establish the truth.

Statistically, approximately 60% of husbands and 40% of wives will have an affair at some point in their marriage or relationship.

Testing begins with DNA detection, the infidelity evidence is analyzed for male DNA. The second step is comparison, your DNA will be required as a control in order to determine if the DNA obtained from the submitted sample belongs to a male or female. If the DNA profile does not match yours, you will have indisputable scientific confirmation.

STR:

The human genome is full of repeated DNA sequences. These repeated sequences come in various sizes and are classified according to the length of the core repeat units, the number of contiguous repeat units, and/or the overall length of the repeat region. DNA regions with short repeat units (usually 2-6 bp in length) are called Short Tandem Repeats (STR). STRs are found surrounding the chromosomal centromere (the structural center of the chromosomes). STRs have proven to have several benefits that make them especially suitable for human identification.
                                                                </p>

                                                                <p>
                                                                    Y-STR:

Y- STRs are Short Tandem Repeat found on the male-specific Y- Chromosome. The coding genes, mostly found on the short arm of the Y- Chromosome, are vital to male sex determination, spermatogenesis and other male related functions. The Y-STRs are polymorphic among unrelated males and are inherited through the paternal line with little change through generations.
                                                                </p>
                                                                <p>
                                                                    Telomeres Testing:

TelemeresTelomeres act as a cap on your chromosomes. Each time a cell replicates, its telomere will become shorter. A shorter telomere equals a shorter life span for a cell. Once the telomere is gone, the chromosome is damaged and the cell dies.

The length of your telomeres can be determined through a DNA test. Telomere length is the best method to date to assess biological age. While you might be 35 years old according to your birthdate, your biological age might actually be 55, depending on the length of your telomeres.

Studies have shown that poor diet, lack of exercise, stress and tobacco use can affect telomere length, causing them to shorten faster and that healthy lifestyle changes can help slow down the rate of telomere shortening.
                                                                </p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>


                                    </div>

                                </div>
                            </div>
                            <div class="tab-pane" id="tabsNavigationSimpleIcons3">
                                <div class="center">

                                    <div class="col-md-10 col-md-offset-1">
                                        <h4 class="text-left heading"><strong>Immigration DNA Testing</strong></h4>
                                        <p class="text-color-dark text-left">
                                            It is required to prove a biological relationship between a petitioner and a beneficiary, using an AABB accredited immigration DNA testing lab, especially when the supporting documents and evidence is deemed insufficient to prove the relationship. The case becomes inconclusive by the USCIS or U.S. embassy and the petitioner’s request may be denied. In most cases, the USCIS or U.S Embassy may advise the petitioner to proof biological family relationship with DNA, using an AABB accredited immigration DNA Laboratory.

                                           <br />
                                            <br />
                                            Examples of DNA tests used for immigration purposes include paternity tests, maternity tests, siblingship tests, and grand-parentage tests, all of which must utilize AABB accredited testing facilities.

                                        </p>
                                        <hr />

                                        <h4 class="text-left heading">Frequently Asked Questions</h4>

                                        <h4 class="text-left heading">How does it work?</h4>
                                        <p class="text-color-dark text-left heading">
                                            Mega Lab Services, a Third Party Administrator, specializes in collecting DNA specimens. Our partner AABB Accredited Laboratories must initiate the immigration DNA legal and Chain of Custody processes prior to specimen collection by Mega Lab Services. Our partner AABB Accredited Laboratories work with U.S Consulate offices in over 170 Countries around the world.                                            
                                        </p>
                                        <br />
                                        <h4 class="text-left heading">What are the Steps To completing Immigration DNA Test</h4>
                                        <p class="text-color-dark text-left heading">
                                            Our partner AABB Accredited Laboratories, using Mega Lab Services, will arrange to collect your DNA specimen. You, the petitioner will come in for your appointment once the specimen is collected, we will ship your specimen to our partner laboratory and they will ship specimen collection kit to the U.S Consulate in the applicant’s home country. U.S Consulate will arrange for the specimen collection of your relative(s).
                                        </p>
                                        <p class="text-color-dark text-left heading">
                                            To get immigration DNA processed by our partner AABB Accredited Laboratories, you need to submit a verified copy of your photo identification and request letter (from USCIS) to our partner laboratories that we work with.  Once the test is completed, our partner AABB Accredited Laboratories will submit the reports directly to USCIS on your behalf as they require. Note, these report can also be used to establish legal proof of relationship for the purpose of obtaining a U.S Passport.                                       
                                        </p>
                                        <br />
                                        <h4 class="text-left heading">Quality Service</h4>
                                        <p class="text-color-dark text-left heading">
                                            Mega Lab Services is a trusted and respected partner and distributor for reputable AABB Accredited Laboratories nationwide.
                                           <br />
                                            For more information on immigration DNA sample collection, please contact us at (866) 261-7129 or (571) 285-1857.

                                        </p>
                                        <br />
                                        <%-- <h4 class="text-left heading"> Immigration Embassy Addresses</h4>
                                       <p class="text-color-dark text-left heading">
                                            As a service to our customers, Mega Lab provides the addresses to 16 of the most commonly petitioned embassies.
                                           Mega Lab Services is a trusted and respected partner for immigration DNA testing. Our large network of locations allows us to offer best-of-class services for anyone in need of immigration DNA testing across the nation.
                                           For more information on immigration DNA testing, contact: (866) 261-7129 / (571) 285-1857.
                                        </p>--%>
                                    </div>


                                </div>
                            </div>
                            <div class="tab-pane" id="tabsNavigationSimpleIcons4">
                                <div class="center">
                                    <h4 class="text-left heading"><strong>Background Checks</strong></h4>
                                    <div class="col-md-8">
                                        <p class="text-color-dark text-left">
                                            We offer background screening services that are ideal for private-owned companies, non-profit organizations, publicly owned for-profit organizations, and government sectors.

Mega Lab Services delivers the best and most reliable background checks and has the flexibility to adapt to your changing needs over time. 
                                            <br />
                                            <br />
                                            By establishing ourselves as your trusted partner, we deliver reliable, complete, and current information, legal compliance with all applicable federal/state/local laws, unparalleled customer service, and the best overall value.

We empower employers to make informed hiring decisions by providing timely, accurate & complete employment background checks. Mega Lab Services conducts criminal record searches directly at each of our nation’s 3,500 plus county courthouses, covering the entire United States.

  <br />
                                            <br />
                                            We work directly with your organization to develop an effective pre-employment screening program that addresses your unique needs. Our background check solutions can include any combination of the following pre-employment screening and background check services
                                        </p>

                                    </div>
                                    <div class="col-md-4" style="background: #e9e9e9">
                                        <br />
                                        <p class="text-color-dark ">Our Background Checks services</p>

                                        <hr />

                                        <ul class="list list-icons list-icons-style-3 mt-xlg" style="text-align: left; color: black">
                                            <li><i class="fa fa-caret-right"></i><a class="text-color-dark" href="#" data-toggle="modal" data-target="#chModal">Criminal History</a>
                                                <div class="modal fade" id="chModal" tabindex="-1" role="dialog" aria-labelledby="chModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                <h4 class="modal-title" id="chModalLabel">Criminal History</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>
                                                                    Criminal History:
                                                                   Reveals criminal record information and is a key to offsetting the liabilities of 
                                                                    negligent hiring. Using criteria established by your corporate guidelines, 
                                                                    we search county, state (where available) and federal criminal court records.
                                                                </p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li><i class="fa fa-caret-right"></i><a class="text-color-dark" href="#" data-toggle="modal" data-target="#dhModal">Driving History</a>
                                                <div class="modal fade" id="dhModal" tabindex="-1" role="dialog" aria-labelledby="dhModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                <h4 class="modal-title" id="dhModalLabel">Driving History</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>Reveals valid license information, negligent / reckless operators and individuals operating a vehicle with an invalid or suspended license. An important search for any company hiring drivers of company vehicles, or for personnel operating personal vehicles for company business.</p>

                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li><i class="fa fa-caret-right"></i><a class="text-color-dark" href="#" data-toggle="modal" data-target="#ceiModal">Credit Employment Insight</a>
                                                <div class="modal fade" id="ceiModal" tabindex="-1" role="dialog" aria-labelledby="ceiModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                <h4 class="modal-title" id="ceiModalLabel">Credit Employment Insight</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>
                                                                    Shows debt load, payment history and public record information of a civil nature (bankruptcy, lien, judgment) and more.
                                                                </p>

                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li><i class="fa fa-caret-right"></i><a class="text-color-dark" href="#" data-toggle="modal" data-target="#eplvLModal">Education/Professional License Verification</a>
                                                <div class="modal fade" id="eplvLModal" tabindex="-1" role="dialog" aria-labelledby="eplvLModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                <h4 class="modal-title" id="eplvLModalLabel">Education/Professional License Verification</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>
                                                                    Education Verification:
                                                                     Confirms through the given educational institution the years of 
                                                                    attendance and/ or graduation and degree(s) obtained.
                                                                </p>
                                                                <p>
                                                                    Professional License Verification:
                                                                Indicates to whom the license is issued, issuing agency, validity of issue, date of expiration 
                                                                    and, if available, history of disciplinary action taken on the license.
                                                                </p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li><i class="fa fa-caret-right"></i><a class="text-color-dark" href="#" data-toggle="modal" data-target="#crccModal">Civil Records/Criminal Court Documents</a>
                                                <div class="modal fade" id="crccModal" tabindex="-1" role="dialog" aria-labelledby="crccModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                <h4 class="modal-title" id="crccModalLabel">Civil Records/Criminal Court Document Copies</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>
                                                                    Civil Records:
                                                                    (Upper): The upper and lower civil courts are distinguished based upon financial value of a claim.                                                                   
                                                                     Higher value cases such as divorce are handled at the upper level.
                                                                    (Lower): Lower value cases such as eviction, small claims and minor disputes are included in the lower level civil record.
                                                                </p>
                                                                <p>
                                                                    Criminal Court Document Copies:
                                                                  Copies of the original court documents.
                                                                </p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li><i class="fa fa-caret-right"></i><a class="text-color-dark" href="#" data-toggle="modal" data-target="#dsModal">Disposition Searches</a>
                                                <div class="modal fade" id="dsModal" tabindex="-1" role="dialog" aria-labelledby="dsModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                <h4 class="modal-title" id="dsModalLabel">Disposition Searches</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>A search of the county court records to obtain any and all information regarding a case, which may be missing on the original report.</p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                           
                                        </ul>


                                    </div>

                                </div>
                            </div>
                            <div class="tab-pane" id="tabsNavigationSimpleIcons5">
                                <div class="center">
                                    <h4 class="text-left heading"><strong>Workplace Safety and Medical Supplies</strong></h4>
                                    <div class="col-md-12">
                                        <p class="text-color-dark text-left">
                                         Under federal law, employees are entitled to a safe workplace. Employer must provide a workplace free
of known health and safety hazards. The Occupational Safety and Health Act of 1970 created OSHA,
which sets and enforces protective workplace safety and health standards. Employers also must comply
with the General Duty Clause of the OSH Act, which requires them to keep their workplaces free of
serious recognized hazards.
                                            <br />
                                            <br />
Mega Lab Service Workplace Safety supplies and protective equipment keeps your employees healthy
and free of safety hazards. Mega Lab Services help employers ensure full compliance with safety
regulations by providing Protective clothing, helmets, face shields, goggles, respirators, gloves, traffic
safety vests, hard hats, fall protection harnesses, and much more. Mega Lab Services will help maintain
OSHA safety compliance at your facility with fire extinguishers, LOTO, safety cans and cabinets, sorbents,
training programs, and matting.
  <br />
                                            <br />
You can count on Mega Lab Services for all the safety supplies you need to keep your employees safe
from injury or infection. Mega Lab Services has all the personal and facility safety equipment you need
at the most affordable prices. Our employees are committed to being knowledgeable, professional, and
ethical in order to provide the best service to our clients. We are constantly in search of discovering the
most modern, innovative products in the market and making them accessible to our clients.                                        </p>

                                    </div>
                            

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="call-to-action call-to-action-default" style="background: #e9e9e9">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="center">
                        <h2 style="text-align: center; padding-top: 30px;"><strong>Schedule your next lab tests with us</strong></h2>
                        <h4 style="text-align: center; padding-bottom: 30px;">Call us @<strong> (866) 261-7129 / (571) 285-1857</strong> or <a class="btn btn-small btn-primary" href="Appointment.aspx">Schedule</a> your appointment online</h4>



                    </div>

                    <div class="clearfix"></div>

                </div>
            </div>
        </div>
    </section>


</asp:Content>

