﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Mail;


public partial class Appointment : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    { }
   
    [WebMethod]
    public static int saveapt(string fname, string lname, string email, string phone, string testcategory 
        , string test , string apptdate, string appttime , string message, string location)
    {
   

        int res = 0;
        int insertapt = 0;
        int insertnote = 0;

        try
        {
            insertapt = insertappointment(fname, lname, email, phone, testcategory, test, apptdate, appttime, message, location);
            if (insertapt == 1)
            {
                StringBuilder emailstring = new StringBuilder();
                emailstring.AppendLine("<p><span>Name : </span><span>" + fname + " " + lname + "</span></p>");
                emailstring.AppendLine("<p><span>Email : </span><span>" + email + "</span></p>");
                emailstring.AppendLine("<p><span>Phone : </span><span>" + phone + "</span></p>");

                emailstring.AppendLine("<p><span>Test Category : </span>" + testcategory + "</span></p>");
                emailstring.AppendLine("<p><span>Test : </span>" + test + "</span></p>");
                emailstring.AppendLine("<p><span>Appointment Date : </span>" + apptdate + "</span></p>");
                emailstring.AppendLine("<p><span>Appointment Time : </span>" + appttime + "</span></p>");

                emailstring.AppendLine("<p><span>Message : </span>" + message + "</span></p>");

                //SmtpClient smtp = new SmtpClient();
                const string SERVER = "relay-hosting.secureserver.net";
                MailMessage newmessage = new System.Web.Mail.MailMessage();
                newmessage.From = "info@megalabservices.com";
                newmessage.To = "info@megalabservices.com";
                newmessage.BodyFormat = MailFormat.Html;
                newmessage.BodyEncoding = Encoding.UTF8;
                newmessage.Subject = "Megalabservices : New appointment for " + fname + " " + lname;
                newmessage.Body = emailstring.ToString();
                //newmessage.Cc = "";
                //newmessage.Bcc = "";
                SmtpMail.SmtpServer = SERVER;
                SmtpMail.Send(newmessage);

                if (testcategory != "Drug & Background Check (ST1701)")
                {
                    StringBuilder emailstring2 = new StringBuilder();
                    emailstring2.AppendLine("<p><span>Thank you </span><span>" + fname + " " + lname + " for setting up an appointment with Megalabservices. A representative will contact you shortly.</span></p>");

                    emailstring2.AppendLine("<br></br>");

                            
                    emailstring2.AppendLine("<p>Please view the details of your upcoming tests below, visit the chosen location at your scheduled date and time:</p>");
                    emailstring2.AppendLine("<p><span>Scheduled Test : </span>" + test + "</span><br></br><span>Appointment Date : </span>" + apptdate + "</span><br></br><span>Appointment Time : </span>" + appttime + "</span></p>");
                    
                    emailstring2.AppendLine("<br></br>");


                    emailstring2.AppendLine("<p><strong>Woodbridge Location</strong></p>");
                    emailstring2.AppendLine("<p><span>Mega Lab Services<br></br>1950 Optiz Boulevard<br></br>Woodbridge, Virginia 22191<span></p>");
                    


                    emailstring2.AppendLine("<br></br>");


                    emailstring2.AppendLine("<p><strong>San Fransisco Location</strong></p>");
                    emailstring2.AppendLine("<p><span>Mega Lab Services<br></br>1321 Evans Avenue Ste. E<br></br>San Francisco, California 94124<span></p>");

                     


                    emailstring2.AppendLine("<br></br>");
                   
                    emailstring2.AppendLine("<p>Sincerely,</p>");
                    emailstring2.AppendLine("<p>Mega Lab Services</p>");
                    emailstring2.AppendLine("<p>******************************************************************************************</p>");
                    emailstring2.AppendLine("<p>This e-mail and any attachments may contain CONFIDENTIAL information, including PROTECTED HEALTH information. If you are not the intended recipient, any use or disclosure of this information is STRICTLY PROHIBITED; you are requested to delete this e-mail and any attachments, notify the sender immediately, and notify Mega Lab Services or Call (866) 261-7129</p>");



                    MailMessage newmessage2 = new System.Web.Mail.MailMessage();
                    newmessage2.From = "info@megalabservices.com";
                    newmessage2.To = email;
                    newmessage2.BodyFormat = MailFormat.Html;
                    newmessage2.BodyEncoding = Encoding.UTF8;
                    newmessage2.Subject = "Megalabservices Lab testing -  Your appointment details";
                    newmessage2.Body = emailstring2.ToString();
                    //newmessage2.Cc = "";
                    //newmessage2.Bcc = "";
                    SmtpMail.SmtpServer = SERVER;
                    SmtpMail.Send(newmessage2);
                    res = 1;
                    if (res == 1)
                    {
                        insertnote = insertNotification(newmessage2.From, newmessage2.To, newmessage2.Cc, newmessage2.Bcc, newmessage2.Subject, newmessage2.Body);
                    }
                }
                if (testcategory == "Drug & Background Check (ST1701)")
                {
                StringBuilder emailstring3 = new StringBuilder();
                emailstring3.AppendLine("<p><span>Thank you </span><span>" + fname + " " + lname + " for setting up an appointment for drug test and background check. Please follow the two steps to start the process.</span></p>");

                emailstring3.AppendLine("<p><strong>Step one: Background Check</strong></p>");
                emailstring3.AppendLine("<p>Please click on the link below to begin your Background check process</p>");
                emailstring3.AppendLine("<p>https://megalab.quickapp.pro</p>");

                emailstring3.AppendLine("<br></br>");
 

                emailstring3.AppendLine("<p><strong>Step Two: Drug Test</strong></p>");
                emailstring3.AppendLine("<p>Please click " + "<a href=" + "https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&amp;hosted_button_id=QJEDLBHYVLL8Y target="  + "_blank>HERE</a>" + " to pay for your drug test.</p>");
                emailstring3.AppendLine("You will receive a confirmation email with our nearest drug test center to you and an Authorization Form to bring with you to the testing center within 24 hours of receipt of payment.");
                emailstring3.AppendLine("<strong>Note:</strong> You must bring a valid ID and the authorization form with you to the testing center for testing to be performed.");
                emailstring3.AppendLine("<p>Please view the details of your upcoming drug tests below, visit the chosen location at your scheduled date and time:</p>");
                emailstring3.AppendLine("<p><span>Appointment Date : </span>" + apptdate + "</span><br></br><span>Appointment Time : </span>" + appttime + "</span></p>");
                emailstring3.AppendLine("<br></br>");
                emailstring3.AppendLine("<p><strong>Drug Test Location</strong></p>");
                emailstring3.AppendLine("<p><span>Mega Lab Services<br><span>Mega Lab Services<br></br>1950 Optiz Boulevard<br></br>Woodbridge, Virginia 22191<span><span></p>");
                emailstring3.AppendLine("<br></br>");
                emailstring3.AppendLine("<p><span>If you have any problems logging into your account, please contact a Mega Lab Services Customer Care Representative at (866) 261-7129 or (571) 285-1857, Monday - Friday, 8 a.m. to 6 p.m. Eastern.</p>");
                emailstring3.AppendLine("<p>Sincerely,</p>");
                emailstring3.AppendLine("<p>Mega Lab Services</p>");
                emailstring3.AppendLine("<p>******************************************************************************************</p>");
                emailstring3.AppendLine("<p>This e-mail and any attachments may contain CONFIDENTIAL information, including PROTECTED HEALTH information. If you are not the intended recipient, any use or disclosure of this information is STRICTLY PROHIBITED; you are requested to delete this e-mail and any attachments, notify the sender immediately, and notify Mega Lab Services or Call (866) 261-7129</p>");


                MailMessage newmessage3 = new System.Web.Mail.MailMessage();
                newmessage3.From = "info@megalabservices.com";
                newmessage3.To = email;
                newmessage3.BodyFormat = MailFormat.Html;
                newmessage3.BodyEncoding = Encoding.UTF8;
                newmessage3.Subject = "Drug test and background check Email Notification";
                newmessage3.Body = emailstring3.ToString();
                //newmessage3.Cc = "";
                //newmessage3.Bcc = "";
                SmtpMail.SmtpServer = SERVER;
                SmtpMail.Send(newmessage3);
                res = 1;
                if (res == 1)
                {
                    insertnote = insertNotification(newmessage3.From, newmessage3.To, newmessage3.Cc, newmessage3.Bcc, newmessage3.Subject, newmessage3.Body);
                }
                }
               
               
                if (insertnote != 1)
                {
                    res = 0;
                }

            }
            else res = 0;

        }
        catch
        {
            res = 0;
        }

        return res;
    }
    [WebMethod]
    public static int insertappointment(string fname, string lname, string email, string phone, string testcategory
        , string test, string apptdate, string appttime, string message , string location)
    {
     
        int emailres = 0;
        try
        {

            string cs = ConfigurationManager.ConnectionStrings["eklipseconsult"].ConnectionString;
            using (SqlConnection con = new SqlConnection(cs))
            {
                using (SqlCommand cmd = new SqlCommand("dbo.insertAppointment", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@fname", fname);
                    cmd.Parameters.AddWithValue("@lname", lname);
                    cmd.Parameters.AddWithValue("@email", email);
                    cmd.Parameters.AddWithValue("@phone", phone);
                    cmd.Parameters.AddWithValue("@testcategory", testcategory);
                    cmd.Parameters.AddWithValue("@test", test);
                    cmd.Parameters.AddWithValue("@apptdate", apptdate);
                    cmd.Parameters.AddWithValue("@appttime", appttime);
                    cmd.Parameters.AddWithValue("@message", message);
                    cmd.Parameters.AddWithValue("@branchid", location);

                    con.Open();
                    email = cmd.ExecuteScalar().ToString();
                  
                    con.Close();
                  
                }

            }
            emailres = 1;
        }
        catch
        {
            emailres = 0;
        }

        return emailres;
    }

    [WebMethod]
    public static int insertNotification(string from, string to, string cc, string bcc, string subject
        , string body)
    {

        int notres = 0;
        try
        {

            string cs = ConfigurationManager.ConnectionStrings["eklipseconsult"].ConnectionString;
            using (SqlConnection con = new SqlConnection(cs))
            {
                using (SqlCommand cmd = new SqlCommand("dbo.insertNotification", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@from", from);
                    cmd.Parameters.AddWithValue("@to", to);
                    cmd.Parameters.AddWithValue("@cc", cc);
                    cmd.Parameters.AddWithValue("@bcc", bcc);
                    cmd.Parameters.AddWithValue("@subject", subject);
                    cmd.Parameters.AddWithValue("@body", body);
           

                    con.Open();
                    notres = Convert.ToInt32(cmd.ExecuteScalar());

                    con.Close();

                }

            }
            notres = 1;
        }
        catch
        {
            notres = 0;
        }

        return notres;
    }


    [WebMethod]
    public static List<TestCategories> getTestCategoryByBranchId(string branchId)
    {
        List<TestCategories> TestCategories = new List<TestCategories>();
        string cs = ConfigurationManager.ConnectionStrings["eklipseconsult"].ConnectionString;
        DataTable dt = new DataTable();
        using (SqlConnection con = new SqlConnection(cs))
        {
            using (SqlCommand cmd = new SqlCommand("EXEC dbo.GetCategoryByBranchId " + Convert.ToUInt32(branchId) , con))
            {
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        TestCategories.Add(new TestCategories
                        {
                            TestCategoryID = Convert.ToInt32(dt.Rows[i]["TestCategoryID"]),
                            TestCategory = dt.Rows[i]["TestCategory"].ToString()
                        });
                    }

                }
            }
        }
        return TestCategories;
    }


    [WebMethod]
    public static List<Test> getTestByCategoryId(int CategoryId)
    {
        List<Test> Test = new List<Test>();
        string cs = ConfigurationManager.ConnectionStrings["eklipseconsult"].ConnectionString;
        DataTable dt = new DataTable();
        using (SqlConnection con = new SqlConnection(cs))
        {
            using (SqlCommand cmd = new SqlCommand("EXEC dbo.GetTestByCategoryId " + CategoryId, con))
            {
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        Test.Add(new Test
                        {
                            TestID = Convert.ToInt32(dt.Rows[i]["TestID"]),
                            test = dt.Rows[i]["Test"].ToString()
                        });
                    }

                }
            }
        }
        return Test;
    }


    [WebMethod]
    public static List<Branches> getBranchOffices()
    {
        string cs = ConfigurationManager.ConnectionStrings["eklipseconsult"].ConnectionString;
        DataTable dt = new DataTable();
        List<Branches> branches = new List<Branches>();
        using (SqlConnection con = new SqlConnection(cs))
        {
            using (SqlCommand cmd = new SqlCommand("EXEC dbo.getBranchNames" , con))
            {
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        branches.Add(new Branches
                        {
                            branchID = Convert.ToInt32(dt.Rows[i]["branchid"]),
                            branch = dt.Rows[i]["branch"].ToString()
                        });
                    }

                }
            }
        }
        return branches;
    }


    [WebMethod]
    public static List<AppointmentTime> getAvailableTimeByLocationDate(string BranchId, string AppointmentDate)
    {
        List<AppointmentTime> AppointmentTime = new List<AppointmentTime>();
        string cs = ConfigurationManager.ConnectionStrings["eklipseconsult"].ConnectionString;
        DataTable dt = new DataTable();
        using (SqlConnection con = new SqlConnection(cs))
        {
            using (SqlCommand cmd = new SqlCommand("EXEC dbo.GetAvailableTimeByBranchIdAppointmentDate " + Convert.ToUInt32(BranchId)+ ", " + "'" + AppointmentDate + "'",  con))
            {
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        AppointmentTime.Add(new AppointmentTime
                        {
                            ApptTimeID = Convert.ToInt32(dt.Rows[i]["ApptTimeID"]),
                            ApptTime = dt.Rows[i]["ApptTime"].ToString()
                        });
                    }

                }
            }
        }
        return AppointmentTime;
    }

}