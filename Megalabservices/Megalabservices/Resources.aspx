﻿<%@ Page Title="" Language="C#" MasterPageFile="megalabs.master"  AutoEventWireup="true" CodeFile="Resources.aspx.cs" Inherits="Resources" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title>Resources | Mega Lab Services</title>	
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<section class="call-to-action mb-xl" style="min-height:90px;margin-bottom:0 !important; background-color:#0088cc">

    <div class="container">
            <div class="row">
                <h1 class="text-center test2" style="padding:30px;">Resources</h1>
                
              
            </div>
        </div>
					
				</section>
   <section style="min-height:550px;">

<div class="container" data-spy="scroll" data-target="#sidebar" data-offset="120" style="margin-top:20px;">
     <h4 class="center" style="padding-bottom:30px;">Mega Lab Services provide its clients with all necessary tools to implement drug and alcohol testing services. 
                    Below are some of the needed resources to perform your rapid drug and alcohol tests.
									</h4>

					<div class="row">
						<div class="col-md-3">
							<aside class="sidebar" id="sidebar" data-plugin-sticky data-plugin-options='{"minWidth": 991, "containerSelector": ".container", "padding": {"top": 110}}'>

								
								<ul class="nav nav-list mb-xl show-bg-active">
                                    <li><a data-hash data-hash-offset="85" href="#second">General</a></li>
									<li class="active"><a data-hash data-hash-offset="85" href="#first">Preliminary Result Forms</a></li>
									<%--<li>
										<a data-hash data-hash-offset="85" href="#second">Services</a>
										<ul>
											<li><a data-hash data-hash-offset="85" href="#sub-second-1">Background checks</a></li>
											<li><a data-hash data-hash-offset="85" href="#sub-second-2">DNA</a></li>
										</ul>
									</li>
                                        --%>
									
								</ul>
                           
							</aside>
						</div>
						<div class="col-md-9" style="background-color:whitesmoke;min-height:500px;">
                            <h3 class="heading-primary" style="background:whitesmoke"></h3>
                            <p id="first" class=""><strong>Preliminary Result Forms</strong></p>
                            <ul class="nav nav-list nav nav-divider">
                              <li><a target="_blank" href="assets/forms/urine-prelim_result_form.pdf">Urine Rapid Drug Screen – Preliminary Test Result Form   (downloadable PDF)</a></li>
                            <li><a target="_blank"  href="assets/forms/alcohol-prelim_result_form.pdf">Alcohol Rapid Screen – Preliminary Test Result Form (downloadable PDF)</a></li>
                           <li> <a  target="_blank" href="assets/forms/saliva-prelim_result_form.pdf">Oral Fluid Rapid Drug Screen – Preliminary Test Result Form (downloadable PDF)</a></li>
							</ul>
						
<%--							<h3 class="mb-sm" id="second">Services</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam posuere porta.</p>

							
							<h4 class="mb-sm" id="sub-second-1">Background Check</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam posuere porta. </p>

							<h4 class="mb-sm" id="sub-second-2">DNA</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>

							<h3 class="mb-sm" id="third">Whatever</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam posuere porta. --%>

						</div>

					</div>

				</div>

       </section>
</asp:Content>

