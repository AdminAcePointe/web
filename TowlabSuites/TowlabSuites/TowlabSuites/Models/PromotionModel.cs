﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Management.Instrumentation;
using System.Web;
using System.Web.Mvc;

namespace TowlabSuites.Models
{
    public class PromotionModel
    {
        public string Email;
        public const string User = "AppUser";
        public string Message;

        public PromotionModel()
        {
            
        }
        public PromotionModel(string email)
        {
            Email = email;
        }


        public void Subscribe()
        {
            try
            {
            Dbio db = new Dbio();
            ManageGuest manageGuest = new ManageGuest();
            manageGuest.CreateGuest(Email);
            var alreadySubscribed = db.Db.Promotions.FirstOrDefault(i => i.EmailAddress.ToLower() == Email.ToLower());

            //EmailParms emailParms = new EmailParms();

            if (alreadySubscribed == null)
            {
                Promotion promotion = new Promotion
                {
                    GuestID = manageGuest.GuestId,
                    EmailAddress = Email,
                    IsActive = true,
                    DeactivationDate = null,
                    CreatedDate = DateTime.Today,
                    CreatedBy = User
                };
                db.Db.Promotions.Add(promotion);
                db.Db.SaveChanges();
                Message = Email.ToLower() + " is now subscribed for promotional content";
                SendEmail();
                return;
            }

            if (alreadySubscribed.IsActive == true)
            {
                Message = "Looks like we already got you covered via " + Email.ToLower() + ". Thank you!";
                SendEmail();
                return;
            }
            if (alreadySubscribed.IsActive == false)
            {
                alreadySubscribed.IsActive = true;
                alreadySubscribed.LastModifiedBy = User;
                alreadySubscribed.LastModifiedDate = DateTime.Now;
                Message = "Welcome back! " + Email.ToLower() + " has been reactivated for promotional content.";
                db.Db.SaveChanges();
                SendEmail();
            }
        }
            catch (Exception ex)
            {
                
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }

        public void SendEmail()
            {
                try
                {
                Dbio db = new Dbio();
                EmailParms emailParms = new EmailParms();
                
                var template = db.Db.Templates.Find(1);
                string body = "";
                string subject = "";

                string httpBase = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);

                if (template != null)
                {
                    subject = template.TemplateSubject.Replace("Promotion Subscription",
                        "Promotion subscription for " + Email);

                    body = template.TemplateBody
                        .Replace(
                            "Your subscription request has been processed, you’re now all set to receive our latest news and offers.",
                            Message)
                            .Replace("[[email]]",Email);
                       
                }
                emailParms.From = "info@towlabSuites.com";
                emailParms.To = Email;
                emailParms.Body = body;
                emailParms.Subject = subject;

                Email email = new Email();
                email.SendEmail(emailParms);
            }
                catch (Exception ex)
                {
                    
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                }
            }
        public void Unsubscribe(string email)
        {
            try
            {
            var db = new Dbio();
            var user = db.Db.Promotions.FirstOrDefault(i => i.EmailAddress.ToLower() == email.ToLower());
            if (user != null)
            {
                user.IsActive = false;
                user.LastModifiedBy = User;
                user.LastModifiedDate = DateTime.Now;
                    user.DeactivationDate = DateTime.Now;
                db.Db.SaveChanges();
                Message = "Success! You have been unsubscribed and will no longer receive emails via " + email;
            }
            else
            {
                Message = "Oops... this is embrassing, but I don't seem to have " + email + " in my system.\n"
                          + "Well on the bright side, you will not receive promotional content.";
            }
            }
            catch (Exception ex)
            {
                
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

        }
    }
}