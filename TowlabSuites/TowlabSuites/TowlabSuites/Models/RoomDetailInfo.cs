﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TowlabSuites.Models
{
    public class RoomDetailInfo
    {

        public static List<roomdetailinfo> getroomdetailinformation( int roomtypename)
        {


            try
            {
                List<roomdetailinfo> rti = new List<roomdetailinfo>();
                Dbio database = new Dbio();
                // get roomtype info
                var roomtype = (from rt in database.Db.RoomTypes
                                join tf in database.Db.RoomRates on rt.RoomTypeID equals tf.RoomTypeID
                                join ph in database.Db.Photos on rt.RoomTypeID equals ph.RoomTypeID
                                where ph.ShortDescription.Contains("Primary Photo")
                                where rt.RoomTypeID == roomtypename
                                select new
                                {
                                  roomtypeid = rt.RoomTypeID
                                 ,roomtype = rt.ShortDescription
                                 ,roomdesc = rt.LongDescription
                                 ,roomrate = tf.Rate
                                 ,detaildesc = rt.DetailDescription
                                 ,roombannerphoto  = ph.PhotoURL
                                }
                               );

                // get photos
                var photos = (from rt in roomtype.AsEnumerable()
                              join ph in database.Db.Photos on rt.roomtypeid equals ph.RoomTypeID
                              
                              group ph.PhotoURL by rt.roomtypeid into g
                             select new { roomtypeid = g.Key, roomphotoURLlist = string.Join(";", g.ToArray()) }
                             );


                // get room features 
                var roomfeatures = (from rt in roomtype.AsEnumerable()
                                    join tyf in database.Db.TypeFeatures on rt.roomtypeid equals tyf.RoomTypeID
                                    join rf in database.Db.RoomFeatures on tyf.RoomFeatureID equals rf.RoomFeatureID
                                   
                                    group rf.RoomFeature1 by rt.roomtypeid into g
                                    select new { roomtypeid = g.Key, roomfeaturelist = string.Join(";", g.ToArray()) });

                // combine the 3 queries
                var final = (from rt in roomtype.AsEnumerable()
                             join f in roomfeatures.AsEnumerable() on rt.roomtypeid equals f.roomtypeid
                             join ph in photos.AsEnumerable() on rt.roomtypeid equals ph.roomtypeid

                             select new roomdetailinfo
                             {
                                 roomtypeid = rt.roomtypeid
                                 , shortdesc = rt.roomtype
                                 ,longdesc = rt.roomdesc
                                 ,detaildesc = rt.detaildesc
                                 ,roomrate = "₦" + Convert.ToInt32(rt.roomrate).ToString()
                                 ,roomphotolist = ph.roomphotoURLlist
                                 ,roomfeaturelist = f.roomfeaturelist
                                 ,roomdetailurl = "/roomdetail/" + rt.roomtypeid.ToString()
                                 ,roombannerphoto = rt.roombannerphoto
                             }
                                              );

                foreach (var a in final)
                {
                    rti.Add(new roomdetailinfo
                    {
                        roomtypeid = a.roomtypeid
                       ,shortdesc = a.shortdesc
                       ,longdesc = a.longdesc
                       ,roomrate = a.roomrate
                       ,roomphotolist = a.roomphotolist
                       ,roomdetailurl = a.roomdetailurl
                      ,roomfeaturelist = a.roomfeaturelist
                      ,roombannerphoto = a.roombannerphoto
                      ,detaildesc = a.detaildesc
                    });
                }
                return rti;
            }
            catch (Exception ex)
            {
                
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return null;
            }
           
        }

    }

    public struct roomdetailinfo{

        public int roomtypeid { get; set; }
        public string shortdesc { get; set; }
        public string longdesc { get; set; }
        public string detaildesc { get; set; }
        public string roomrate { get; set; }
        public string roombannerphoto { get; set; }
        public string roomphotolist { get; set; }
        public string roomdetailurl { get; set; }
        public string roomfeaturelist { get; set; }
    }

   
 


}