﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TowlabSuites.Controllers;

namespace TowlabSuites.Models
{
    public class ReservationModel
    {
        private const string UserName = "AppUser";
        public string Message;
        public string ReservationComplete;
        public string ReservationCompleteFontColor;

        public DateTime CheckIn { get; set; }
        public DateTime CheckOut { get; set; }
        public byte NumOfGuests { get; set; }
        public string TypeOfRoom { get; set; }
        public byte NumOfNights { get; set; }

        public Reservation Reservation { get; set; }
        public RoomType RoomType { get; set; }

        public void Create(FormCollection fc)
        {

            try
            {
                Dbio db = new Dbio();
                Reservation = new Reservation();
                RoomType = new RoomType();

                RoomType = db.Db.RoomTypes.FirstOrDefault(i => i.ShortDescription == TypeOfRoom);
                if (RoomType != null)
                {
                    var spCheckRoom = db.Db.GetRoomStatus(CheckIn, CheckOut).FirstOrDefault(i => i.RoomType == RoomType.ShortDescription);
                    var isRoomAvailabe = spCheckRoom == null ? 0 : Convert.ToByte(spCheckRoom.IsAvailable);
                    if (isRoomAvailabe == 0)
                    {
                        Message = "The " + RoomType.ShortDescription + " room is not availabe for the dates selected.";
                        ReservationComplete = "Reservation Failed!";
                        ReservationCompleteFontColor = "danger";
                        return;
                    }
                }
                else if (RoomType == null)
                {
                    ReservationComplete = "Oops... this is Embrassing! :(";
                    ReservationCompleteFontColor = "danger";
                    Message = "The selected room type does not exist.";
                    return;
                }
                var firstName = fc["form-name"];
                var lastName = fc["form-surname"];
                var email = fc["form-email"];
                var phone = fc["form-phone"];
                var specialRequirements = fc["form-requirements"];

                Reservation.Room = RoomType.Rooms.First();
                Reservation.RoomType = RoomType;

                ManageGuest manageGuest = new ManageGuest();
                manageGuest.CreateGuest(email, firstName, lastName, phone);
                Reservation.GuestID = manageGuest.GuestId;

                Reservation.ArrivalDate = CheckIn;
                Reservation.DepartureDate = CheckOut;
                Reservation.NumberOfGuests = NumOfGuests;
                Reservation.SpecialRequirements = specialRequirements;

                var genCode = new ConfirmationNumber();
                var code = genCode.Generate(RoomType, Reservation.Room);
                Reservation.ConfirmationNumber = code;

                Reservation.ConfirmationDate = DateTime.Now.Date;
                Reservation.CreatedBy = UserName;
                Reservation.CreatedDate = DateTime.Now;
                Reservation.SpecialRequirements = String.IsNullOrWhiteSpace(specialRequirements) ? null : specialRequirements;

                ReservationComplete = "Reservation Successful!";
                ReservationCompleteFontColor = "success";
                Message = "Your booking confirmation code is " + Reservation.ConfirmationNumber;

                db.Db.Reservations.Add(Reservation);
                db.Db.SaveChanges();

                //Send Email
                NumOfNights = Convert.ToByte((CheckOut - CheckIn).Days);
                var template = db.Db.Templates.Find(2);
                var roomRateData = db.Db.RoomRates.First(i => i.RoomType.ShortDescription == TypeOfRoom);
                decimal totalPrice = Convert.ToDecimal(NumOfNights * (RoomType == null ? 0 : roomRateData.Rate));
                // write code to return total price
                var priceFormat = totalPrice.ToString("#,#", CultureInfo.InvariantCulture);
                var totalcharges = "₦" + priceFormat;
                var pricePerNight = "₦" + Convert.ToDecimal(roomRateData.Rate).ToString("#,#", CultureInfo.InvariantCulture);
                if (template != null)
                {
                    string httpBase = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
                    var body = template.TemplateBody.Replace("[[firstname]]", firstName)
                        .Replace("[[confimationnumber]]", " " + code)
                        .Replace("[[fullname]]", " " + lastName + ", " + firstName)
                        .Replace("[[arrivaldate]]", " " + CheckIn.ToString("D"))
                        .Replace("[[departuredate]]", " " + CheckOut.ToString("D"))
                        .Replace("[[checkin]]", " 4:00 pm")
                        .Replace("[[checkout]]", " 12:00 noon")
                        .Replace("[[numnights]]", " " + NumOfNights)
                        .Replace("[[roomtype]]", " " + RoomType.ShortDescription)
                        .Replace("[[price]]", " " + pricePerNight)
                        .Replace("[[totalprice]]", " " + totalcharges)
                        .Replace("[signUpURL]", httpBase + "/Base/Promotion?email=" + email);

                    var parms = new EmailParms
                    {
                        Body = body,
                        Subject = template.TemplateSubject,
                        From = "reservations@towlabsuites.com",
                        To = email
                    };

                    var sendMail = new Email();
                    sendMail.SendEmail(parms);
                }
            }
            catch (Exception ex)
            {
                
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }

        public static List<object> ConfirmReservation(string confirmationNumber, string lastName)
        {

            try
            {
                Dbio db = new Dbio();
                var r = db.Db.Reservations.FirstOrDefault(
                    i => i.ConfirmationNumber == confirmationNumber && i.Guest.GuestLastName == lastName);
                if (r != null)
                {
                    // write code to return total price
                    var numofNights = (r.DepartureDate - r.ArrivalDate).Days;
                    var rate = db.Db.RoomRates.FirstOrDefault(i => i.RoomType.ShortDescription == r.RoomType.ShortDescription);
                    var totalPrice = rate == null ? 0 : Convert.ToDecimal(rate.Rate * numofNights);
                    var priceFormat = totalPrice.ToString("#,#", CultureInfo.InvariantCulture);
                    var totalcharges = "₦" + priceFormat;


                    return new List<object>
                {
                    new
                    {
                        r.RoomType.RoomTypeID,
                        RoomType = r.RoomType.ShortDescription,
                        checkin = r.ArrivalDate.Date.ToString("D"),
                        checkout = r.DepartureDate.ToString("D"),
                        totalprice = totalcharges,
                        guest = r.NumberOfGuests,
                        night = numofNights
                    }
                };
                }

            }
            catch (Exception ex)
            {
               Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return null;
        }

        //Extract the number of nights
        public static string ExtractNumber(string original)
        {
            return new string(original.Where(Char.IsDigit).ToArray());
        }

    }
    
    
}