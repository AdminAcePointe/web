﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TowlabSuites.Models
{
    public class GalleryInfo
    {
        public static List<photo> getphotoinformation()
        {


            List<photo> rti = new List<photo>();
            Dbio database = new Dbio();
            // get photo info
            var photo = (from ph in database.Db.Photos
                          where !ph.ShortDescription.Contains("Primary Photo")
                            select new
                            {
                               photoid = ph.PhotoID
                             ,photourl = ph.PhotoURL
                             ,photodesc = ph.ShortDescription                           
                            }
                         );
          
            foreach (var ph in photo)
            {
                rti.Add(new photo
                {
                    photoid = ph.photoid,
                    photourl = ph.photourl,
                    photodesc = ph.photodesc  
                });
            }
            return rti;

        }

    }

    public struct photo
    {

        public int photoid { get; set; }
        public string photourl { get; set; }
        public string photodesc { get; set; }
    }
}