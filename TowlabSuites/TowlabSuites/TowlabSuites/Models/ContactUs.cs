﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TowlabSuites.Models
{
    
    public class ContactUs
    {
        public string FirstName;
        public string LastName;
        public string Email;
        public string Phone;
        public string Message;
        private const int NotificationTypeId = 2; // Contact email 
        private const string User = "AppUser";
        public EmailParms EmailParms;

        public ContactUs()
        {
            
        }
        public ContactUs(FormCollection fc)
        {
            FirstName = fc["form-firstname"];
            LastName = fc["form-lastname"];
            Email = fc["form2-email"];
            Phone = fc["form-phone"];
            Message = fc["form-message"];
        }

        public void SendMessage()
        {
            try
            {
                Dbio db = new Dbio();
                ManageGuest manageGuest = new ManageGuest();
                manageGuest.CreateGuest(Email, FirstName, LastName, Phone);

                var notification = new Notification
                {
                    NotificationTypeID = NotificationTypeId,
                    GuestID = manageGuest.GuestId,
                    NotificationDate = DateTime.Today,
                    Subject = null,
                    Body = Message,
                    CreatedBy = User,
                    CreatedDate = DateTime.Today
                };
                db.Db.Notifications.Add(notification);
                db.Db.SaveChanges();
                SendEmail();
            }
            catch (Exception ex)
            {
                
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

        }

        public void SendEmail()
        {

            try
            {
                Dbio db = new Dbio();
                var template = db.Db.Templates.Find(3);
                string customizedBody = "";
                string customizedSubject = "";

                if (template != null)
                {
                    customizedBody = template.TemplateBody.Replace("[[confimationnumber]]", "")
                        .Replace("New Message", "Message from " + Email)
                        .Replace("First Name:", "First Name: " + FirstName)
                        .Replace("Last Name:", "Last Name: " + LastName)
                        .Replace("Email:", "Email: " + Email)
                        .Replace("Phone:", "Phone: " + Phone)
                        .Replace("Message:", "Message: " + Message);
                    customizedSubject = template.TemplateSubject.Replace("New Contact Notification - Towlab Suites",
                        "Towlab Suites Notification Message from " + Email);
                }


                var distributionDb = db.Db.NotificationDistributionLists.Where(i => i.NotificationTypeID == NotificationTypeId).ToList();

                foreach (var i in distributionDb)
                {
                    EmailParms.From = "notification@towlabSuites.com";
                    EmailParms.To = i.EmailAddress;
                    EmailParms.Body = template == null ? null : customizedBody;
                    EmailParms.Subject = template == null ? null : customizedSubject;
                    Email email = new Email();
                    email.SendEmail(EmailParms);
                }
            }
            catch (Exception ex)
            {
                
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }


        }
    }
}