﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TowlabSuites.Models
{
    public class RoomsInfo
    {

        public static List<roominfo> getroomtypeinformation()
        {


            try
            {
                List<roominfo> rti = new List<roominfo>();
                Dbio database = new Dbio();
                // get roomtype info
                var roomtype = (from rt in database.Db.RoomTypes
                                join tf in database.Db.RoomRates on rt.RoomTypeID equals tf.RoomTypeID
                                join ph in database.Db.Photos on rt.RoomTypeID equals ph.RoomTypeID
                                where ph.ShortDescription.Contains("Primary Photo")

                                select new
                                {
                                    roomtypeid = rt.RoomTypeID
                                 ,
                                    roomtype = rt.ShortDescription
                                 ,
                                    roomdesc = rt.LongDescription
                                 ,
                                    roomrate = tf.Rate
                                 ,
                                    roomphoto = ph.PhotoURL
                                }
                                            );
                // get room features  for each type
                var roomfeatures = (from rt in roomtype.AsEnumerable()
                                    join tyf in database.Db.TypeFeatures on rt.roomtypeid equals tyf.RoomTypeID
                                    join rf in database.Db.RoomFeatures on tyf.RoomFeatureID equals rf.RoomFeatureID
                                    group rf.RoomFeature1 by rt.roomtypeid into g
                                    select new { roomtypeid = g.Key, roomfeaturelist = string.Join(";", g.ToArray()) });

                // combine the 2 queries
                var final = (from rt in roomtype.AsEnumerable()
                             join f in roomfeatures.AsEnumerable() on rt.roomtypeid equals f.roomtypeid

                             select new roominfo
                             {
                                 roomtypeid = rt.roomtypeid
                                 ,
                                 shortdesc = rt.roomtype
                                 ,
                                 longdesc = rt.roomdesc
                                 ,
                                 roomrate = "₦" + Convert.ToInt32(rt.roomrate).ToString()
                                 ,
                                 roomphoto = rt.roomphoto
                                 ,
                                 roomfeaturelist = f.roomfeaturelist
                                 ,
                                 roomdetailurl = "/roomdetail/" + rt.roomtypeid.ToString()
                             }
                                              );

                foreach (var a in final)
                {
                    rti.Add(new roominfo
                    {
                        roomtypeid = a.roomtypeid
                       ,
                        shortdesc = a.shortdesc
                       ,
                        longdesc = a.longdesc
                       ,
                        roomrate = a.roomrate
                       ,
                        roomphoto = a.roomphoto
                       ,
                        roomdetailurl = a.roomdetailurl
                      ,
                        roomfeaturelist = a.roomfeaturelist
                    });
                }
                return rti;
            }
            catch (Exception ex)
            {
                
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return null;
        }

    }

    public struct roominfo{

        public int roomtypeid { get; set; }
        public string shortdesc { get; set; }
        public string longdesc { get; set; }
        public string roomrate { get; set; }
        public string roomphoto { get; set; }
        public string roomdetailurl { get; set; }
        public string roomfeaturelist { get; set; }
    }

   
 


}