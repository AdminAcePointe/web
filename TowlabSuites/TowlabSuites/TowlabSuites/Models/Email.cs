﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace TowlabSuites.Models
{
    public struct Email
    {
        public bool SendEmail(EmailParms emailParmObj)
        {
            try
            {
                using (MailMessage mail = new MailMessage(emailParmObj.From, emailParmObj.To))
                {
                    mail.Subject = emailParmObj.Subject;
                    mail.Body = emailParmObj.Body;
                    mail.IsBodyHtml = true;
                    SmtpClient smtpClient = new SmtpClient();
                    smtpClient.Send(mail);
                }
                return true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return false;
            }
        }
    }

    public struct EmailParms
    {
        public string From { get; set; }
        public string To { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string Attachment { get; set; }
        public string Emailtype { get; set; }
    }

}