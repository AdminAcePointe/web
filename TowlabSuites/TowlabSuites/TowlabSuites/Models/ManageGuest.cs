﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;

namespace TowlabSuites.Models
{
    public struct ManageGuest
    {
        private const string User = "AppUser";
        public  int GuestId;
        public int GuestAddressId;
        public int GuestPhoneId;
        public int GuestEmailId;
        
        public void CreateGuest(  string email, string firstName = null, string lastName = null, string phone = null)
        {
            try
            {
                Dbio db = new Dbio();
                var guest = new Guest
                {
                    CreatedBy = User,
                    CreatedDate = DateTime.Now,
                    GuestAddresses = null,
                    GuestEmailID = null,
                    GuestFirstName = firstName,
                    GuestLastName = lastName,
                    GuestMiddleName = null,
                    GuestPhones = null,
                    LastModifiedBy = User
                };
                db.Db.Guests.Add(guest);
                db.Db.SaveChanges();
                GuestId = guest.GuestID;

                CreateEmail(email);
                CreatePhone(phone);
                UpdateGuest(GuestId);
            }
            catch (Exception ex)
            {
                
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }
        public void CreatePhone(string phone)
        {
            try
            {
                Dbio db = new Dbio();
                var p = new GuestPhone
                {
                    CreatedBy = User,
                    CreatedDate = DateTime.Now,
                    PhoneNumber = phone,
                    GuestID = GuestId
                };
                db.Db.GuestPhones.Add(p);
                db.Db.SaveChanges();
                GuestPhoneId = p.GuestPhoneID;
            }
            catch (Exception ex)
            {
                
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }

        public void CreateEmail(string email)
        {
            try
            {
                Dbio db = new Dbio();
                var e = new GuestEmail
                {
                    CreatedBy = User,
                    CreatedDate = DateTime.Now,
                    Email = email,
                    GuestID = GuestId
                };
                db.Db.GuestEmails.Add(e);
                db.Db.SaveChanges();
                GuestEmailId = e.GuestEmailID;
            }
            catch (Exception ex)
            {
                
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }

        public void CreateAddress(string addressLine1, string addressLine2 , string city, string countryname,string zip, string state)
        {
            try
            {
                Dbio db = new Dbio();
                var country = db.Db.Countries.SingleOrDefault(i => String.Equals(i.Name, countryname, StringComparison.CurrentCultureIgnoreCase));
                var address = new GuestAddress
                {
                    GuestID = GuestId,
                    Address1 = addressLine1,
                    Address2 = addressLine2,
                    City = city,
                    Country = country,
                    CreatedBy = User,
                    CreatedDate = DateTime.Now
                };
                db.Db.GuestAddresses.Add(address);
                db.Db.SaveChanges();
                GuestAddressId = address.GuestAddressID;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }

        public void UpdateGuest(int guestId)
        {
            try
            {
                Dbio db = new Dbio();
                Guest guest = db.Db.Guests.Find(guestId);
                if (guest != null)
                {
                    if (GuestEmailId > 0) { guest.GuestEmailID = GuestEmailId; }
                    if (GuestPhoneId > 0) { guest.GuestPhoneID = GuestPhoneId; }
                    if (GuestAddressId > 0) { guest.GuestAddressID = GuestAddressId; }
                    db.Db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            
        }
    }
}