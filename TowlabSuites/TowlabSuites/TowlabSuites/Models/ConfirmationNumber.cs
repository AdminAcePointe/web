﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TowlabSuites.Models
{
    public struct ConfirmationNumber
    {
        public string Generate(RoomType roomType, Room room)
        {
            try
            {
                var rand = new Random();
                var confirmationCode = roomType.RoomType1
                                        + rand.Next(300, 999)
                                        + room.RoomID
                                        + DateTime.Today.Millisecond;
                return confirmationCode.Substring(0, 8).ToUpper();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return "unable to generate code";
        }
    }
}