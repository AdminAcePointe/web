﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TowlabSuites.Models
{
    public class Dbio : Controller
    {
            public TowlabSuitesEntities Db;

            public Dbio()
            {
                Db = new TowlabSuitesEntities();
            }

            protected override void Dispose(bool disposing)
            {
                Db.Dispose();
            }

    }
}