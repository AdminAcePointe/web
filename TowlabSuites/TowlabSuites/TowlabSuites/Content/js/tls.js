﻿(function ($, window, document, undefined) {
    'use strict';

    checkdates()

   

    // adjust reservation dates based on check in date selection 
    $("#checkin").on("change", function () {
  
        checkdates()
    })

    // Check availability when dates change
    $(".avail").on("change", function () {
        //$(".price-inner .abc").removeClass("gg").removeClass("adisabled")
        checkavailability();

    })

    // reservation step 1 -  post to choose room form
    $("#initreservebtn").on("click", function () {

        var res = formvalidate();
        if (res == 0) {
            $.alert({
                title: 'Error',
                //theme: 'supervan',
                type: 'red',
                typeAnimated: true,

                content: 'Please fill out the required fields'
            })
            return false
        }

        var guesttext = $("#guest option:selected").text();
    
        var RoomData = {
            '_checkin': $('#checkin').val(),
            '_checkout': $("#checkout").val(),
            '_guest': $('#guest').val(),
            '_guesttext': guesttext
        };
        
            // process the form
        $.ajax({
            url: '/Reservation/NavChooseroom/',
            data: JSON.stringify(RoomData),
            dataType: "json",
            type: "POST",
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data != null) {
                    window.location = data;
                }
                else {
                    $.alert({
                        title: 'Error',
                        //theme: 'supervan',
                        type: 'red',
                        typeAnimated: true,

                        content: 'Error occured. Please try again'
                    })
                }

            }
        });
    })

    // reservation step 2 - post to make reseservation
    $(".abc").click(function () {

     
        var price = $(this).parents(".price-inner").find(".price").text();
        
        if (price.length == 0)
        {
            price = $("#roomprice").text();
        }

        var roomtype = $(this).parents(".rooms-item").find(".roomtype").text();

        if (roomtype.length == 0) {
            roomtype = $("#roomtype").text();
        }

        var res = formvalidate();
        if (res == 0) {
            $.alert({
                title: 'Error',
                //theme: 'supervan',
                type: 'red',
                typeAnimated: true,

                content: 'Please fill out the required fields'
            });
            return false
        }
       
        var RoomData = {
            '_checkin': $('input[name="checkin"]').val(),
            '_checkout': $('input[name="checkout"]').val(),
            '_guest': $('select[name="guest"]').val(),
            '_guesttext': $('select[name="guest"]').text(),
            '_roomtype': roomtype,
            '_price': price
        };

      
  
        $.ajax({
            url: '/Reservation/MakeReservation/',
            data: JSON.stringify(RoomData),
            dataType: "json",
            type: "POST",
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data != null) {
                    window.location = data;
                }
                else {
                    $.alert({
                        title: 'Error',
                        //theme: 'supervan',
                        type: 'red',
                        typeAnimated: true,

                        content: 'Error completing reservation. Please try again later'
                    })
                }

            }
        });


    });


    // postback to choose room - edits
    $("#pbchoosefrm").click(function () {

      

        var RoomData = {
            '_checkin': $('#checkin').text(),
            '_checkout': $("#checkout").text(),
            '_guest': $('#guest').text(),
            '_guesttext': $('#guest').text() + " guests",
        };

        $.ajax({
            url: '/Reservation/NavChooseroom/',
            data: JSON.stringify(RoomData),
            dataType: "json",
            type: "POST",
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data != null) {
                    window.location = data;
                }
                else {
                  
                    $.alert({
                        title: 'Error',
                        //theme: 'supervan',
                        type: 'red',
                        typeAnimated: true,

                        content: 'Error navigating back to Room Selection page. Please try again'
                    })
                }

            }
        });


    });

    // submit promotion email
    $("#promoemailsubmit").click(function () {

        

        var email = $('#form-email').val()
        var e = {
            'email': email
        };
        if (!validateEmail(email)) {
            $.alert({
                title: 'Error',
                //theme: 'supervan',
                type:'red',
                typeAnimated: true,
                
                content:'Please enter valid email address to continue'
            })
            return false;
        }
        $.confirm({
            title: 'Subscription Status',
            theme: 'supervan',
            typeAnimated: true,
            buttons: {
                
                close: function () {
                    $('#form-email').val("");
                }
            },
            content: function () {
                var self = this;                            
                return $.ajax({
                    url: '/Base/Promotion/',
                    data: JSON.stringify(e),
                    dataType: "json",
                    type: "POST",
                    crossDomain: true,
                    contentType: "application/json; charset=utf-8",
                }).done(function (response) {
                    self.setContentAppend(response);
                });
            }
        });

       


    });


    // unsubscribe guest
    $("#b").click(function () {

        var emailraw = getUrlVars()["email"];

        var email = {
            'email': emailraw
        };
        $.ajax({
            url: '/Reservation/Unsubscribe/',
            data: JSON.stringify(email),
            dataType: "json",
            type: "POST",
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            success: function (data) {

                if (data != null) {
                 
                    $('#message').html(data);

                }
                else {
                    alert("Error unsubscribing. Please try again");
                }

            },
            error: function (response) {
                alert(response.responseText);
            },
            failure: function (response) {
                alert(response.responseText);
            }
        });


    });


    // check reservation
    $("#chkres").click(function () {
        $("#roomtype").text("")
        $("#totalprice").text("")
        $("#checkin").text("")
        $("#checkout").text("")
        $("#guest").text("")
        $("#night").text("")

        if ($("#lastname").val().length == 0 || $("#confirmationnumber").val().length == 0)
        {
            $.alert({
                title: 'Error',
                //theme: 'supervan',
                type: 'red',
                typeAnimated: true,

                content: 'Please enter the required fields'
            })
            return false
        }
        

        var resdata = {
            'LastName': $("#lastname").val(),
            'ConfirmationNumber': $("#confirmationnumber").val()
        };

        $.confirm({
            title: 'Confirm Reservation',
            type:"red",
            typeAnimated: true,
            buttons: {

                close: function () {
                   
                }
            },
            content: function () {
                var self = this;
                return  $.ajax({
                    url: '/Base/confirmReservation/',
                    data: JSON.stringify(resdata),
                    dataType: "json",
                    type: "POST",
                    crossDomain: true,
                    contentType: "application/json; charset=utf-8"
                }).done(function (data) {

                    $("#roomtype").text(data[0].RoomType + "- ")
                    $("#roomtype").attr("href", "/roomdetail/" + data[0].RoomTypeID)
                    $("#totalprice").text("Price:" + data[0].totalprice)
                    $("#checkin").text(data[0].checkin)
                    $("#checkout").text(data[0].checkout)
                    $("#guest").text(data[0].guest)
                    $("#night").text(data[0].night)
                    self.close()

                }).fail(function () {
                    self.setContentAppend('No Match found. Please try again');
                });
            }
        
        });
      
       


    });
 //functions
    // check reservation dates - throw error if check out is less than check in 
    function checkdates() {
        
        var checkindate = $("#checkin").val();
    
            $("#checkout").datepicker('option', 'minDate', checkindate);
        
    }
    // validate email
    function validateEmail($email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return ($email.length > 0 && emailReg.test($email));
    }

    function formvalidate() {

        if(
            $('input[name="checkin"]').val().length == 0 
            || $('input[name="checkout"]').val().length == 0
            || $('select[name="guest"]').val().length == 0
            //|| $('select[name="guest"]').text().length == 0
            //|| $('input[name="checkin"]').text().length == 0 
            //|| $('input[name="checkout"]').text().length == 0
            //|| $('select[name="guest"]').text().length == 0
       )
        {
            var res = 0
            return res
        }

    }

    function formvalidate2() {

        if (
           
             $('select[name="guest"]').text().length == 0
            || $('input[name="checkin"]').text().length == 0 
            || $('input[name="checkout"]').text().length == 0
 
       ) {
            var res = 0
            return res
        }

    }

    // get availability
    function checkavailability(request, response) {
        // reset form 
       
        // get the form data
        var formData = {
            '_checkin': $('input[name="checkin"]').val(),
            '_checkout': $('input[name="checkout"]').val(),
            '_guest': $('select[name="guest"]').val(),
            '_guesttext': $('select[name="guest"]').val(),
            
        };
        $.ajax({
            url: '/Reservation/CheckRoomAvailability/',
            data: JSON.stringify(formData),
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                
                    if (data != null) {
                        for (var i = 0; i < data.length; i++) {
                            $(".roomtype a").each(function () {
                                if ($(this).text() == data[i].RoomType) {
                                    $(this).removeClass("avail");
                                    $(this).append("<span class='avail' style='font-size:16px; color:red !important'> * " + data[i].Status + "</span>");
                                    $(this).parents(".rooms-item").find(".price-inner .abc").addClass("adisabled");
                                }
                               
                            });
                        }
                    }

                }
        });
    }

    // Read a page's GET URL variables and return them as an associative array.
    function getUrlVars() {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }

}(jQuery, window, document));
