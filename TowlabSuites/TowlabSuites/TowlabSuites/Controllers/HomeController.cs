﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DotNetOpenAuth.Messaging;
using TowlabSuites.Models;

namespace TowlabSuites.Controllers
{
    public class HomeController : Controller
    {
        private readonly TowlabSuitesEntities _context;

        public HomeController()
        {
            _context = new TowlabSuitesEntities();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        //
        // GET: /Home/

        public ActionResult getRoomView()

        {
            List<roominfo> rti = new List<roominfo>();

            rti = RoomsInfo.getroomtypeinformation();

            return View("Room",rti);
        }

        public ActionResult Index()
        {
            return View();
        }



        public ActionResult getRoomDetailView(int id)
        {
            List<roomdetailinfo> rd = new List<roomdetailinfo>();

            rd = RoomDetailInfo.getroomdetailinformation(id);
            
            return View("RoomDetail",rd);
        }

       

        public ActionResult Services()
        {
            return View();
        }

        public ActionResult Gallery()
        {
            List<photo> ph = new List<photo>();

            ph = GalleryInfo.getphotoinformation();
            return View(ph);
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

        public ActionResult submitPromotion(string email)
        {
      
            string message = "Email submitted successfully";

            //Write to database and send email to subscriber

            return Json(message, JsonRequestBehavior.AllowGet);
        }

    }
}
