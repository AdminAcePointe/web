﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using TowlabSuites.Models;

namespace TowlabSuites.Controllers
{
    public class ReservationController : Controller
    {
        private readonly TowlabSuitesEntities _context;

        public ReservationController()
        {
            _context = new TowlabSuitesEntities();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        // GET: /Reservation/

        public ActionResult CompleteReservation()
        {
            return View();
        }


        public JsonResult NavChooseRoom(string _checkin, string _checkout, string _guest, string _guesttext)
        {
            string checkin = _checkin;
            var checkout = _checkout;
            var guest = _guest;
            var guesttext = _guesttext;

            // set variables
            SetSessions(checkin, checkout, guest, guesttext);
            List<roominfo> rti = new List<roominfo>();

            rti = RoomsInfo.getroomtypeinformation();

            return Json(Url.Action("ChooseRoom", "Reservation"), JsonRequestBehavior.AllowGet); 
        }

        public ActionResult ChooseRoom()
        {
          

            return View();
        }

        public JsonResult MakeReservation(FormCollection fc, string _checkin, string _checkout, string _guest, string _guesttext, string _roomtype , string _price)

        {
            string checkin = _checkin;
            var checkout = _checkout;
            var guest = _guest;
            var guesttext = _guesttext;
            var roomtype = _roomtype;
            var price = _price;
            //get nights
            var nights = GetNumberOfNights(checkin,checkout);

            // get total price 
            var totalprice = GetTotalPrice(nights, price);


            // set variables
            SetSessions(checkin, checkout, guest, guesttext , roomtype, price,nights,totalprice);

      
            return Json(Url.Action("CompleteReservation", "Reservation"), JsonRequestBehavior.AllowGet); 
        }


        public ActionResult SubmitReservation(FormCollection fc)
        {
            ReservationModel reservationModel = new ReservationModel
            {
                CheckIn = Convert.ToDateTime(Session["checkin"]),
                CheckOut = Convert.ToDateTime(Session["checkout"])
            };
            var numGuests = Session["guest"].ToString();
            reservationModel.NumOfGuests = Convert.ToByte(numGuests.Substring(0,1));
            reservationModel.TypeOfRoom = Session["roomtype"].ToString();
            reservationModel.Create(fc);
            TempData["Reservation"] = reservationModel;
            return RedirectToAction("Confirmation","Base");
        }

        public JsonResult CheckRoomAvailability(FormCollection fc, string _checkin, string _checkout, string _guest, string _guesttext)
        {

            string checkin = _checkin;
            var checkout = _checkout;
            var guest = _guest;
            var guesttext = _guesttext;
            // set variables
            SetSessions(checkin,checkout,guest,guesttext);
              
            var roomList = _context.GetRoomStatus(Convert.ToDateTime(checkin), Convert.ToDateTime(checkout));
            var avail = new List<object>();
            foreach (var room in roomList)
            {
                if (room.IsAvailable == 0)
                {
                    avail.Add(new {RoomType = room.RoomType, Status = "Unavailable for selected dates"});
                }
            }
            // TODO : Ayo: view is not using json result properly
            return Json(avail,JsonRequestBehavior.AllowGet);
        }

            public void  SetSessions(string checkin, string checkout, string guest, string guesttext , string roomtype = null , string price = null, string nights = null , string totalprice = null) {
            Session["checkin"] = checkin;
            Session["checkout"] = checkout;
            Session["guest"] = guest;
            Session["guesttext"] = guesttext;
            Session["roomtype"] = roomtype;
            Session["price"] = price;
            Session["nights"] = nights;
            Session["totalprice"] = totalprice;
        }

            public string GetNumberOfNights(string checkin, string checkout)
            {
                var checkIn = Convert.ToDateTime(checkin);
                var checkOut = Convert.ToDateTime(checkout);
                int duration =  (checkOut - checkIn).Days;
                // write code to return number of nights
                var nightString = duration < 2 ? " Night" : " Nights";
                return duration + nightString;
            }

            public string GetTotalPrice(string nights, string pricepernight)
            {
                var intNights = Int32.Parse(ReservationModel.ExtractNumber(nights)); //Convert.ToInt16(nights.Substring(0, 1));
                var priceString =  Regex.Replace(pricepernight, @"₦", string.Empty);
                int price = int.Parse(priceString, NumberStyles.Currency);
                var totalPrice = intNights * price;
                // write code to return total price
                var priceFormat = totalPrice.ToString("#,#",CultureInfo.InvariantCulture);

                return "₦" + priceFormat;
            }

        public ActionResult Unsubscribe(string email)
        {
            var promotionModel = new PromotionModel();
            promotionModel.Unsubscribe(email);
            TempData["unsubscribeMessage"] = "<h5 class='' style='background-color:#04B404  !important; color:#fff;text-align : center; padding: 15px;'> " + promotionModel.Message+"</h5>";

            return Json(TempData["unsubscribeMessage"], JsonRequestBehavior.AllowGet);
        }

        public ActionResult unsubscribeguest(string id) {

            return View();
        }

    }
}
