﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using TowlabSuites.Models;

namespace TowlabSuites.Controllers
{
    public class BaseController : Controller
    {
        private readonly TowlabSuitesEntities _context;

        public BaseController()
        {
            _context = new TowlabSuitesEntities();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        //
        // GET: /Base/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Confirmation()
        {
            var reservation = (ReservationModel) TempData["Reservation"];
            return View(reservation);
        }

        public ActionResult EmailTemplate(){
            return View();
        }
        

        public ActionResult ContactUs(FormCollection fc)
        {
            ContactUs contactUs = new ContactUs(fc);
            contactUs.SendMessage();
            TempData["ContactMessage"] = "<h5 class='' style='background-color:#04B404  !important; color:#fff;text-align : center; padding: 15px;'>Your message has been sent.<br /><br /> Our reservations specialist will be in touch soon.</h5>";

            return RedirectToAction("Contact","Home");
            
        }

        public ActionResult Promotion(string email)
        {
            PromotionModel promotionModel = new PromotionModel(email);
            promotionModel.Subscribe();
            return Json(promotionModel.Message,JsonRequestBehavior.AllowGet);
        }
        public ActionResult checkreservation()
        {
            return View();
        }

        public ActionResult ConfirmReservation(string confirmationNumber, string lastName)
        {
            return Json(ReservationModel.ConfirmReservation(confirmationNumber,lastName), JsonRequestBehavior.AllowGet);
        }

    
    }
}
