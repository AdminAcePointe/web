﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace TowlabSuites
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");


            routes.MapRoute(
                 name: "Unsubscribeguest",
                 url: "unsubscribeguest/{id}",
                 defaults: new { controller = "Reservation", action = "unsubscribeguest", id = UrlParameter.Optional }
             );
            routes.MapRoute(
            name: "confirmation",
            url: "Confirmation/{id}",
            defaults: new { controller = "Base", action = "Confirmation", id = UrlParameter.Optional }
        );

            routes.MapRoute(
        name: "Email",
        url: "EmailTemplate/{id}",
        defaults: new { controller = "Base", action = "EmailTemplate", id = UrlParameter.Optional }
    );

            routes.MapRoute(
             name: "ReservationModel",
             url: "ReservationModel/{id}",
             defaults: new { controller = "Reservation", action = "ReservationModel", id = UrlParameter.Optional }
         );

            routes.MapRoute(
              name: "ChooseRoom",
              url: "chooseroom/{id}",
              defaults: new { controller = "Reservation", action = "ChooseRoom", id = UrlParameter.Optional }
          );
              routes.MapRoute(
            name: "Reservation",
            url: "CompleteReservation/{id}",
            defaults: new { controller = "Reservation", action = "Reservation", id = UrlParameter.Optional }
        );
            routes.MapRoute(
              name: "Services",
              url: "services/{id}",
              defaults: new { controller = "Home", action = "services", id = UrlParameter.Optional }
          );

            routes.MapRoute(
               name: "RoomDetail",
               url: "roomdetail/{id}",
               defaults: new { controller = "Home", action = "getRoomDetailView", id = UrlParameter.Optional }
           );
            routes.MapRoute(
               name: "Room",
               url: "room/{id}",
               defaults: new { controller = "Home", action = "getRoomView", id = UrlParameter.Optional }
           );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}